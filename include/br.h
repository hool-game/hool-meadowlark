/* Versions
210209 No changes from windows
*/

#ifndef __BR_H
#define __BR_H
#include "ring.h"
#ifndef WIN32
// #include "hoffer.h"
#endif
#ifndef ERROR
#define ERROR -1
#endif
#define TRUE 1
#define FALSE 0
#define UP 1
#define DOWN 0
#define DISTMAX 240

struct qa {
	char question[80];
	char answer[80];
	struct qa *next;
};

struct bid {
	int abid;
	char comment[80];
	struct bid *next;
	};

struct hand_type {
	int type;
	char directions[80];
	};

#undef USE_INFO_STRUCT
#if defined(USE_INFO_STRUCT)
static struct info {
	int vulnerability;		/* 0 None 1 NS 2 EW 4 NSEW */
	int dealer;			/* 0 North 1 East 2 South 3 West */
	int deck[52];			/* deck 0=2C, 1st 13=North */
	char title[80];			/* title of the hand */
	char event[255];
	char site[255];
	char pref_leads[40];
	char pref_suits[20];
	struct qa *question;
	char statistics[4][D_LEN];
	int played[52];			/* 1st=dealer's card, 2nd next, */
	struct hand_type *hand_type;
	int game_type;
	struct bid *bids;
	int hand_num;
	char contract[6];
	int declarer;
	int result;
	int score;
	char auction[255];
	char play[255];
	char north[60];
	char east[60];
	char south[60];
	char west[60];
	char date[11];
	char home_team[60];
	char visit_team[60];
	char room[60];
	char stage[24];
	int round;
	} rec;
#endif

#define VISUAL_C
struct hands {
	char deck[52];
#ifndef VISUAL_C
	struct hands huge *next;
	struct hands huge *prev;
#else
	struct hands *next;
	struct hands *prev;
#endif
	};


struct nlist {
		int handdist[4];
		int key;
		double chance1;
		double chance2;
		struct nlist *permptr;
		struct nlist *combptr;
		};



struct HCP {
		float count;
		float percent;
		};

struct honor_set {
		int HCPs;
		float count;
		float percent;
		};

#endif
