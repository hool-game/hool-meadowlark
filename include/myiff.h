/* Versions
210209 No changes from windows version
*/

#ifndef WIN32
#include "hoffer.h"
#endif
/*  :ts=8 bk=0
 *
 * myiff.h:	Definitions for my IFF reader.
 *
 * Leo L. Schwab			8705.11

 */

/*  Masking techniques  */
#define	mskNone			0
#define	mskHasMask		1
#define	mskHasTransparentColor	2
#define	mskLasso		3

/*  Compression techniques  */
#define	cmpNone			0
#define	cmpByteRun1		1

#ifdef AMIGA
/*  Bitmap header (BMHD) structure  */
struct BitMapHeader {
	UWORD	w, h;		/*  Width, height in pixels */
	WORD	x, y;		/*  x, y position for this bitmap  */
	UBYTE	nplanes;	/*  # of planes  */
	UBYTE	Masking;
	UBYTE	Compression;
	UBYTE	pad1;
	UWORD	TransparentColor;
	UWORD	XAspect, YAspect;
	WORD	PageWidth, PageHeight;
};

/*  Color register structure (not really used)  */
struct ColorRegister {
	UBYTE red, green, blue;
};
#endif

/*  Makes my life easier.  */
union typekludge {
	char type_str[4];
	long type_long;
};

struct ChunkHeader {
	union typekludge chunktype;
	long chunksize;
};
#define	TYPE		chunktype.type_long
#define	STRTYPE		chunktype.type_str


/*  Useful macro from EA (the only useful thing they ever made)  */
#ifdef AMIGA
#define MAKE_ID(a, b, c, d)\
	( ((long)(a)<<24) + ((long)(b)<<16) + ((long)(c)<<8) + (long)(d) )
#else
#define MAKE_ID(a,b,c,d)\
	( ((long)(d)<<24) + ((long)(c)<<16) + ((long)(b)<<8) + (long)(a) )
#endif

/*  IFF types we may encounter  */
#define	CATS	MAKE_ID('C', 'A', 'T', 'S')	
#define	FORM	MAKE_ID('F', 'O', 'R', 'M')
#define	ILBM	MAKE_ID('I', 'L', 'B', 'M')
#define	BMHD	MAKE_ID('B', 'M', 'H', 'D')
#define	CMAP	MAKE_ID('C', 'M', 'A', 'P')
#define BODY    MAKE_ID('B', 'O', 'D', 'Y')
#define FTXT    MAKE_ID('F', 'T', 'X', 'T')
#define CHRS    MAKE_ID('C', 'H', 'R', 'S')

#define BIDS    MAKE_ID('B', 'I', 'D', 'S')
#define DECK    MAKE_ID('D', 'E', 'C', 'K')
#define PLAY    MAKE_ID('P', 'L', 'A', 'Y')
#define BTXT    MAKE_ID('B', 'T', 'X', 'T')
#define INIT    MAKE_ID('I', 'N', 'I', 'T')
#define STAT	MAKE_ID('S', 'T', 'A', 'T')
#define HAND	MAKE_ID('H', 'A', 'N', 'D')
#define COMM	MAKE_ID('C', 'O', 'M', 'M')
#define LEAD	MAKE_ID('L', 'E', 'A', 'D')
#define SUIT	MAKE_ID('S', 'U', 'I', 'T')

#define	GRAB	MAKE_ID('G', 'R', 'A', 'B')
#define	DEST	MAKE_ID('D', 'E', 'S', 'T')
#define	SPRT	MAKE_ID('S', 'P', 'R', 'T')
#define	CAMG	MAKE_ID('C', 'A', 'M', 'G')
#define	CRNG	MAKE_ID('C', 'R', 'N', 'G')
#define	CCRT	MAKE_ID('C', 'C', 'R', 'T')
#define	DPPV	MAKE_ID('D', 'P', 'P', 'V')


#define TORN	MAKE_ID('T', 'O', 'R', 'N')
#define DATE	MAKE_ID('D', 'A', 'T', 'E')
#define ADDR	MAKE_ID('A', 'D', 'D', 'R')
#define CITY	MAKE_ID('C', 'I', 'T', 'Y')
#define STTE	MAKE_ID('S', 'T', 'T', 'E')
#define CTRY	MAKE_ID('C', 'T', 'R', 'Y')
#define ZIPC	MAKE_ID('Z', 'I', 'P', 'C')
#define FNAM	MAKE_ID('F', 'N', 'A', 'M')
#define LNAM	MAKE_ID('L', 'N', 'A', 'M')
#define PNTS	MAKE_ID('P', 'N', 'T', 'S')
#define OBJECT	MAKE_ID('O', 'B', 'J', 'T')
#define ATTRIBUTE MAKE_ID('A', 'T', 'T', 'R')
#define VALUE	MAKE_ID('V', 'A', 'L', 'U')




/*  Other useful things.  */
#define	CHUNKHEADERSIZE		sizeof (struct ChunkHeader)
#define	SUBTYPESIZE		sizeof (long)


/*  What functions return  */
extern void	*OpenLibrary(void), *AllocMem(void), *AllocRaster(void), *GetColorMap(void);

