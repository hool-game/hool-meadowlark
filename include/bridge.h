/* Versions
210209 No changes from windows version
*/

#ifndef __BRIDGE_H
#define __BRIDGE_H
#ifndef WIN32
// #include "hoffer.h"
#endif
#define EOS '\0'
#define ON 1
#define OFF 0
#define OK 1
#define FALSE 0
#define TRUE 1
#define ERR (-1)
#define PERMUTE 2
#define ONE 1
#define UP 1
#define DOWN 0
#define DONE 1
#define NOTDONE 0
#define CLUBS 0
#define DIAMONDS 1
#define HEARTS 2
#define SPADES 3
#define NT 4
#define PASS 5
#define DOUBLE 6
#define DBL 6
#define RDBL 7
#define REDOUBLE 7
#define NORTH 0
#define EAST 1
#define SOUTH 2
#define WEST 3
#define ONECLUB 10
#define ONEDIAMOND 11
#define ONEHEART 12
#define ONESPADE 13
#define ONENT 14
#define TWOCLUBS 20
#define TWODIAMONDS 21
#define TWOHEARTS 22
#define TWOSPADES 23
#define TWONT 24
#define THREECLUBS 30
#define THREEDIAMONDS 31
#define THREEHEARTS 32
#define THREESPADES 33
#define THREENT 34
#define FOURCLUBS 40
#define FOURDIAMONDS 41
#define FOURHEARTS 42
#define FOURSPADES 43
#define FOURNT 44
#define FIVECLUBS 50
#define FIVEDIAMONDS 51
#define FIVEHEARTS 52
#define FIVESPADES 53
#define FIVENT 54
#define SIXCLUBS 60
#define SIXDIAMONDS 61
#define SIXHEARTS 62
#define SIXSPADES 63
#define SIXNT 64
#define SEVENCLUBS 70
#define SEVENDIAMONDS 71
#define SEVENHEARTS 72
#define SEVENSPADES 73
#define SEVENNT 74
#define section1 1
#define section2 2
#define section3 4
#define section4 8
#define section5 16
#define section6 32
#define section7 64
#define section8 128
#define DECKSIZE 52
#define PRODUCTION_VERSION 1
#define DEXTRO_ROTATE
#endif

