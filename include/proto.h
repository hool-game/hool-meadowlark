/* Versions
210209 Nothing brought over from windows
*/

//@+leo
//@+node:0::@file ..\include\Proto.h
//@+body
//@@language c
#ifndef __PROTO_H
#define __PROTO_H
#ifndef WIN32
// #include "hoffer.h"
#undef HUGE
#endif

// #include "hoffer.h"
#if !defined(LINUX)
#include "br.h"
#endif

#ifdef DLL
typedef void (FAR PASCAL _export * FPSTRCB) (int) ;
typedef void (FAR PASCAL _export * FPSTRPR) (LPSTR);
int do_gen(FPSTRCB lpfnSPL);
#else
int do_gen(void);
#endif

#define GIB

#ifdef HOFFER
int process(char *);
#else
#ifdef DLL
#ifdef GEN32
#include "windef.h"
void process(LPSTR);
#else
void FAR PASCAL process(FPSTRCB,LPSTR);
#ifdef GIB
void FAR PASCAL gib_process(FPSTRPR, LPSTR);
void FAR _export gib_process2(FPSTRPR,LPSTR);
#endif
#endif
#endif
#endif

int gen_proc(int command, char * file);
void all_out(char *p1, char *p2, int);
void print_hand_out(char *p1, int);
void do_get_des(char *,int, char *);
void eval_command(char *);
void hand_print_setup(int,int);
int get_matt_string(char *,char *);
char *get_print_hand(char *p1 ,int);
char *get_print_all(char *p1);

void restrict_available(int [52], int, int);
void restrict_range(int,int);
void restrict_dist(int [4]);
int get_hcp(void);
void reset_hcps(void);
int h_and_d_shuffle(int [52],int,int,unsigned int,int [4]);
/* void get_description(int [52],int i,char far *); */
#ifdef HUGE
int iff_out(struct hands huge *,int , struct hands huge *);
#else
int iff_out(struct hands *,int , struct hands *);
#endif
void hand_and_bids(int FAR [52], char *, int );
// #include "..\include\proto2.h"
void do_dextro(void);
void do_debug(char c);
int gen32_getQuicks(int deckp[52], int first, int number, int quicks[4]);
void getargs(int, char **);
void setperms(void);
int getperms(int far [4], int [24][4]);
unsigned long getcomtot(int [4], int [4]);
int reset_distmem(struct nlist *, int  [DISTMAX][4], int, int);
struct nlist *buildtree(int  [24][4], int, int  [4],int);
int gethand(int [52], int);
void treefree(struct nlist *);
int errorcheck(int *);
int getdist(struct nlist *, int, int  *);
int distdeal(int  [52], int, int [4]);
void gen32_shuffle(int *, int, int);
void hcp_range_shuffle(int [52], int, int);
int gen32_get_hcps(int *, int, int);
extern "C" void gen32_distro(int [52], int, int, int  [4]);
char *gen32_get_description2(int,int);
char *get_constraint_message(int,int,int,int,int);
int gen32_get_deck(int);
void gen32_set_deck(int,int);
void sort_by_length(int *);
void handprint(int [52]);
void pageprint(int [52],int);
void pairprint(int [52], int,int,int);
void printout(int [52], int);
void do_select(int, int, int, int, int [4][4], int [4]);
void rearrange(int [52], int, int, int, int, int, int [4][4], int [4]);
void get_selections(int *, int, int);
void exchange(int *, int, int);
void rearrange2(int [52], int, int, int [4], int);
double combination(int,int);
double permutation(int, int);
double factorial(int);
int sort_function(const void *a, const void *b);
void initialize(void);
char *get_first(void);
int do_print(void);
int gen32_getLTC(int [52],int,int);
void output(char *,...);
int get_range(int *l, int *h, int dl, int dh, int ml, int mh, char *s);
void explain(void);
int set_print_flags(char c);
int get_shape(char *s);
int set_validate_flag(char *s);
int get_string(char *out, char *s);
int get_string(char *out, char *in);
int get_name(char *out, char *s);
int get_setting(int *, int, int, char *);
int get_ms(int [52], char *);
void hand_free(void);
void set_print_range(long);
long get_print_count(void);
int gen32_roth(int deckp[52],int ,int,int,int);
void debug_dist(struct nlist *);
void treeprint(struct nlist *);
void gen_err(char *);
void sort(int *,int,int,int);
#endif

//@-body
//@-node:0::@file ..\include\Proto.h
//@-leo
