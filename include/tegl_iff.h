/* Versions
210209 No changes from windows
*/

#ifndef WIN32
#include "hoffer.h"
#endif
int out_body(int, struct info *);
int iffreadnext(int,struct info *);
int iffread(int,int,struct info *);
int iffreadnext(int,struct info *);
int iffheader(int);
unsigned long iffout(int,struct info *);
unsigned long iffappend(int, struct info *);
void handset(int *);
// void exit(int);
void update(void);
int out_long(int, unsigned long);
void openstuff(void);
void cleanup(void);
void closestuff(void);
void fail(char *);
int getchunkheader(int, struct ChunkHeader *);
void ackphft(char *);
int getsubtype(int, unsigned long *);
void skipchunk(int, unsigned long);
// void err(int);
void print_state(struct info *state);
int set_hand_type(struct info *state, struct hand_type *ht);
int set_title(struct info *state, char *title);
int set_dealer(struct info *state, int dealer);
int set_vulnerability(struct info *state, int vulnerability);
int set_game_type(struct info *state, int game_type);
int add_bid(struct info *state, struct bid *bid);
int add_question(struct info *state, struct qa *q);
