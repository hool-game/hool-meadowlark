#ifndef __RING
#define __RING
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>

#define TRUE 1
#define FALSE 0
#define ON 1
#define OFF 0
#define DECKSIZE 52
#define UP 1
#define DOWN 0
#define ERR (-1)
#define OK 1
#define YES 1
#define NO 0
#define NO_LOGGING
#define D_LEN 105

#define DECKNUM 10001
// #include "constraint_ids.h"
//#ifdef PUT_GLOBALS_HERE
//int gen32_deck[52];
//int gen32_dist[4];
//#else
//extern int gen32_deck[52];
//extern int gen32_dist[4];
//#endif

#if !defined(LINUX) && !defined(linux)
#include "C:\Users\rod\projects\godot\modules\gbridge\bidlibc_new\gen\include\bridge.h"
#include "C:\Users\rod\projects\godot\modules\gbridge\bidlibc_new\gen\include\br.h"
#else
#include "../include/bridge.h"
#include "../include/br.h"
#endif

#include "cswitch.h"
#include "hoffer.h"

#define RIGID
#undef SHOW_CONSTRAINTS_AT_RESET
#define BEST_HOFFER

#define UNITY_TESTING
#define FIX_LOSERS // there were two different functions get_losers, and getLTC - consolidated 200120
#define FIX_SUIT_STRENGTH // same as above for FIX_LOSERS

// char *get_description2(int i, int v);
void gen_err(char *);
void gen32_shuffle(int *deck, int first,int number);
#define LOOP_MAX 10000

#define USE_BERGEN
#define CLEAN_UP // eliminate old functions not necessary for IOS in particular


#ifndef AMZI
#undef AMZI_GEN
#else
#define AMZI_GEN
#endif

#ifndef WIN32
#define EXTERN_HOFFER_C extern
#define HOFFER
// #pragma message("HOFFER defined in ring.h because of WIN32 not being defined")
#else
#pragma message("HOFFER is not defined in ring.h")
#endif
#define USE_HOFFER_EVALUATION


#undef DIVORCE
#define DIVORCE2
#undef GENERATE_ALONE

#ifndef DIVORCE
#else
#pragma message("DIVORCED")
#endif

#ifndef DIVORCE2
#pragma message("NOT DIVORCED2")
#else
#endif

// #include "bergen.h"
#endif

