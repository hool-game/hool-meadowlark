const { HoolClient } = require('hool-client')
const { jid } = require('@xmpp/client')
const readline = require('readline')
const { waitFor, waitForAll } = require('wait-for-event')

require('dotenv').config()

const hoolWebService = process.env.HOOLBOT_WEB_SERVICE || 'wss://beta.hool.org:443/xmpp-websocket'
const hoolTableService = process.env.HOOLBOT_TABLE_SERVICE || 'tables.hool.org'
const hoolTableName = process.env.HOOLBOT_TABLE_NAME || 'automaton@tables.hool.org'

let hooligans = {}

for (let side of ['north', 'east', 'west']) {
  hooligans[side] = new HoolClient({
    service: hoolWebService,
    resource: 'automaton',
  })
  hooligans[side].tableService = hoolTableService
}

function pause(question=null) {
  let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  })

  if (!question) question = 'Pausing for input...'

  return new Promise(resolve => rl.question(question, ans => {
    rl.close()
    resolve(ans)
  }))
}

function wait(milliseconds=1000){
    return new Promise(resolve => {
        setTimeout(resolve, milliseconds);
    });
}

async function main() {
  // connect in order
  hooligans.north.xmppConnect('chikorita@hool.org', 'chikorita')
  hooligans.east.xmppConnect('cyndaquil@hool.org', 'cyndaquil')
  hooligans.west.xmppConnect('totodile@hool.org', 'totodile')

  // exit in order
  process.once('SIGINT', async (code) => {

      console.log("Games over. Let's go, folks")
      for (let h of [hooligans.north, hooligans.east, hooligans.west]) {
        await h.leaveTable(hoolTableName)
        await wait(1000)
        await h.xmpp.stop()
      }

      console.log("We've gone.")
  })

  // wait in order
  await waitForAll('connected', [hooligans.north, hooligans.east, hooligans.west])
  console.log('All hooligans connected!')

  await wait(1500)

  // join in order
  await hooligans.north.joinTable(hoolTableName, 'N')
  await wait(1000)
  await hooligans.east.joinTable(hoolTableName, 'E')
  await wait(1000)
  await hooligans.west.joinTable(hoolTableName, 'W')
  await wait(1000)

  await pause("We're in the table! [ENTER]")

  // share in order
  hooligans.north.shareInfo(hoolTableName, 'N', 'HCP')
  await wait(1000)
  hooligans.east.shareInfo(hoolTableName, 'E', 'pattern')
  await pause('Waiting for South [ENTER]')
  hooligans.west.shareInfo(hoolTableName, 'W', 'C')
  await wait(1000)

  hooligans.north.shareInfo(hoolTableName, 'N', 'D')
  await wait(1000)
  hooligans.east.shareInfo(hoolTableName, 'E', 'HCP')
  await pause('Waiting for South [ENTER]')
  hooligans.west.shareInfo(hoolTableName, 'W', 'S')
  await wait(1000)

  await pause('Time to bid? [ENTER]')

  // bid in order
  hooligans.north.makeBid(hoolTableName, 'N', 'level', 1, 'D')
  await wait(1000)
  hooligans.east.makeBid(hoolTableName, 'E', 'level', 2, 'S')
  await wait(1000)
  hooligans.west.makeBid(hoolTableName, 'W', 'pass')

  await pause('Ready for second round [ENTER]')

  hooligans.east.makeBid(hoolTableName, 'E', 'pass')
  await wait(1000)
  hooligans.west.makeBid(hoolTableName, 'W', 'pass')
  await wait(1000)

  await pause('Time for cardplay? [ENTER]')

  let allRanks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
  let allSuits = ['S', 'H', 'D', 'C']

  for (i=0; i<14; i++) {
    for (let side of ['W', 'N', 'E', 'W', 'N', 'E']) {
      let player
      if (side == 'N')  {
        player = 'north'
      } else if (side == 'W') {
        player = 'west'
      } else {
        player = 'east'
      }

      if (side == 'W') await pause('Waiting for South [ENTER]')
      else await wait(500)

      for (let suit of allSuits) {
        for (let rank of allRanks) {
          hooligans[player].playCard(hoolTableName, side, rank, suit)
        }
      }
    }
  }

  // all done (hopefully)
}

if (!module.parent) {
  main()
}
