# HOOL: Library interface. #

#### 1. A library will available for the the card game Hool. 
This is based on a bridge library. It will be available for Windows, IOS, OSX (so also Linux), and android operating systems.

It is written in C++ which also has an embedded prolog language (AMZI! prolog).

Original document date: 2021-1-26.

This proposal documents the interface to the libary.

The signature for all functions is: 5 integer input parameters returning a string using a C calling convention.

char *command(int who, int what, int where, int lo, int hi); 

#### 2. The integer parameters (five of them) ####

1. **WHO** This is always the direction (0,1,2,3) for North, East, South or West.
2. **WHAT** This is the commmand: for example, 70 requests the first information sharing
3. **WHERE** This is usually a subcommand: for example, 0 on 70 is the basic information sharing system.
4. **LO** a value
5. **HI** a value

#### 3. The return value is always a string. ####

1. Information sharing. After the information all text after a colon is a comment.
	
- **HCPs**: return your dir, your hcps, and HCP. For example: "0 12 HCP" indicating North showing hcps of 12 points
- **Pattern**: return your dir, your distribution is descending order and PATTERN. For example: "2 5431 PATTERN" indicating South has a longest suit of 5 cards.
- **Spade length**: return your dir, your length in spades, and SPADES.
- **Heart length**: return your dir, your length in hearts, and HEARTS.
- **Diamond length**: return your dir, your length in diamonds, and DIAMONDS.
- **Club length**: return your dir, your length in clubs, and CLUBS.

2. Bidding: return your dir,  PASS, DOUBLE, REDOUBLE, 1H, 2N etc, then BID.
3. Playing: return your dir, 2C 7S etc., then PLAY.

Comments can be appended and used for debugging. For example, if you are using a bidding system which might show extra information such as opening count, you could return messages such as:

	
- 0 1H BID : showing 5+ hearts and opening count.
- 2 3H BID : showing 7+ hearts; pre-emptive; less than 10 high card points.

or for play of the hand

- 2 AS PLAY : ace from ace-king
- 0 2S PLAY : 4th best

#### 4. Examples ####

1. result = command(0,70,0,0,0) - requests the first information sharing item. The first parameter is North, the second is the request, the third parameter is the information sharing system. Parameters 4 and 5 can be used to qualify the system, but are unused at this time.

2. result = command(2,71,2,0,0) - requests the second information sharing item. The first parameter is 2 for South, the second is the request. The third parameter is the information sharing system.

3. result = command(2,72,contract,declarer,doubled) - requests a bid.
4. result = command(2,75,bid,0,0) - notification of a bid of another player.

4. result = command(2,9,card,0,0) - sends the information of a card. You receive your 13 cards from the server. Also, after the dummy is shown, you would receive the cards for the dummy. If the declarer is South and you are West, you would receive all of your cards with result = command(3,73, card,0,0) and the cards once revealed for dummy as result = command(1,73,card,0,0) for the dummy's cards in the North hand.

5. result = command(2,74,signaling,0,0) - requests a play where parameter 3 is the signaling system being used. The 3rd parameter would be 0 = no signals. 1 = standard count and attitude signals. 2 = upside down count and attitude signals. 4 = SIT signals (switch in time).
6. result = command(2,76,signaling,0,0) - notification of a play by another player.
2. command(0,86,contract,declarer,doubled) notification of contract, where contract is a integer of the level and the denomination such as 10 is 1C, 74 is 7N. declarer is 0,1,2,3. doubled is 0,1,2 for undoubled, doubled, redoubled. 

#### 5. Addenda ####

1. command(0,99,0,0,0) does a reset.
2. Requests of information can be made to the library:

- command(0,90,0,0,0) requests the library version.
- command(0,94,0,0,0) requests a string of the hands that have been generated.
- command(0,92,0,0,0) requests a string of the hands that are being used during the play.
- command(0,89,0,0,0) requests a string of the constraints being used.

#### 6. Version, status, and update information ####

2021-01-26 Original document.

2021-01-26 Received HOOL flutter code.

- Android Studio set up on Windows and IOS. XCode set up on OSX. Flutter: "flutter doctor" without problems on all environments, and "flutter run" brings up the IOS simulator without problems. XCode does not compile directly, only through flutter run.

#### 7. How does the library work?
	1. The play of the hand uses DDS, a double dummy solver found on the internet by Bo Hagland. One hundred hands are generated from the information of the 2 rounds of shows, and the cards that have been played. No signals as yet. Each possible play is considered, and the total number of tricks determined. The card with the highest trick total for 100 generate hands is selected.
	2. The bidding. Hands are generated from the shows. The strength of the hand and of partners hand in each of these generated hands are calculated for each denomination. The denomination having the highest score is chosen. It it will be strong enough to bid a game or slam it will do so.
	3. The shows. There is more than just picking one of the three general items to show. There is som hidden information. For instance, on the first show, if a robot shows the length of one of the suits, it also has 13+ points. Also, another is that on first show if it is a short suit, then it also has a three suited hand. There are many ways to do shows and bidding. Having the robots play against each other can refine the revealing and bidding systems.

### 8. Changes in the library interface.

#### Shows

	1. Previously the shows were not processed. Now that information is used to constraint the hands for the hand generation routines.

To inform the robot of what has been shown, the standard 5 integer function is called.

hool(who,ID,where,low, high)
#define CHECK1_HCPS 64
#define CHECK1_LEN 65
#define CHECK1_DIST 66
#define CHECK2_HCPS 67
#define CHECK2_LEN 68
#define CHECK2_DIST 69

For hcps. To inform the robot of a player showing HCPS, you would send hool(2,64,10,0,0), which says that player 2 (south) has shown 10 high card points on the first show. Similarly, hool(3,67,5,0,0) informs that west has shown 5 high card points on the second show.

For length of a suit. To inform the robot of a player showing suit length, you would send hool(1,65,0,5,5) which says that player 1 (east) has show suit 0 (clubs) has 5 cards. Similarly, hool(0,68,3,2,2) shows that north hand 2 spades and that it was the 2nd show.

For distribution. To inform the robot of a player showing a distribution, such as 5332, you would send hool(3,66,5,3,3) which shows that west has 5332 distribution on the first show. Note that the length of the shortest suit is calculated from the given lengths. Similarly, hool(1,69,4,4,4) shows that east has 4441 distribution.

#### Bidding
The bidding calls have not changes, but now the robot actually uses the infomation to make an educated guess at a bid.

#### Cards. 
notification of cards to the robot is unchanged. It is the command.what of 9 to send the card.

#### Play of the hand. 

**The call to play of the hand has changed**. It now requires the round, and the seat information for both checking the play and requesting the play. Adding the round and seat infomation allows better verification.

	1. To get a play the command is hool(dir, ID_GET_PLAY,0,round, seat). So, hool(2,74,0,0,0) is a request for a play of the opening lead. Similarly, hool(3,74,0,1,3) would be asking for a play for west of round one and the last card. Note that, as in C, the round and the seat information is 0 based.
	2. Similary, to send the play made by another player, hool(0,ID_CHECK_PLAY,51,0,0) would inform the robot that the opening lead, made by north, was the spade ace (51). Similarly, hool(1,76,1,1,3) would show that east played the Club 3 (1), for the 2nd round in the last seat (3).

#### Other changes to the library -

the DDS double dummy library is in DDS.lib and DDS. dll and must be in the active directory of the program. **This is new**. It is multi-threaded and will use 4 cores if available.

The full directory is zipped together.

Other items of interest.

1. There is a program called mscript2.exe, included. Check the internet for mscript. It can be used to create a test for the program, which will reside in the meadowlark directory. The input is a set of lines of 5 integers (the commands as they would be sent to the robot). The command:
		mscript2 make_demo.ms check.out
	will use the script make_demo.ms and input file of check.out to write a file just the same as ou have in the demo directory of demo.js. This makes testing of the program much easier from the server side.

2. The file hello.cpp is actually generated from hello.exp. It is macro processed. I would not change that, I would change hello.cpp and I can incorporate any changes to the hello.exp file.
3. The C++ files could be compiled to wasm, and then run on the mobile browser rather than the server, but I will leave that for another day and another discussion.

Good luck. It might work, but more likely we will need to go over the interface in a few days.
Rod Ludwig 220501.

[[use meadowlark]]
#hool 








