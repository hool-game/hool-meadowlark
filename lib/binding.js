

function Hool(who,what,where,lo,high) {
    console.debug('hool -> (', who, what, where, lo, high, ')');
    const addon = require('../build/Release/hool-native');
    this.play = function() {
        let result = _addonInstance.play();
        console.debug('hool <- (', result, ')');
        return result;
    }

    var _addonInstance = new addon.Hool(who,what,where,lo,high);
}

module.exports = Hool;
