//@+leo-ver=4
//@+node:@file C:/html/bid20/bridge2.cpp
//@@language c
// bridge2.c - a macro processor with parameters modified for my bridge program
//@<< bridge2 #includes >>
//@+node:<< bridge2 #includes >>
// #include "stdafx.h"
#include "hoffer.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
// release #include "assert.h"
#ifdef _WIN32
#include "malloc.h"
#endif
#include "bridge.h"
//@-node:<< bridge2 #includes >>
//@nl
//@<< bridge2 declarations >>
//@+node:<< bridge2 declarations >>
#undef VERSION2
#define ARITH_GRAB

#ifndef HOFFER
#define RXTRNL
#endif
#include <string>
#include "typedef.hpp"
#include "rx.hpp"
// #include "c:\html\node.hpp"
// #include "header.hpp"
// #include "c:\html\myfile.hpp"
// #include "c:\html\bidstate.hpp"
// #include "c:\html\select.hpp"
// #include "cswitch.h"
#include "bid20.h"
#include "non_exportables.h"
#include "mroff.h"
#include "my_globals.h"
#include "constraint_ids.h"

#if defined(linux)
#define NO_CSW
#include "cswitch.h"
#include "assert.h"
#endif


#undef MACRO_DEBUGGING
void setAssoc(const char *, const char *, int);
char *getAssoc(const char *,int);
void clearAssoc(const char *,int);

// char *lookup(char *,int);
char *value;
#define lookup(x,y) getAssoc((char *)x,y)
#define install(x,y) setAssoc((char *)x,(char *)y,7)
void domacro(const char *,int);

FILE *FPTR,*FPTR2;
struct flist {
	struct flist *next;
	FILE *fptr;
	}  *fheader=NULL;
int  roff_index;
int  wrapper;
// changed to unsigned char 210405
unsigned char  is_type[256];
int  macro_init_flag = 0;
/* extern "C" {
void macro_init(void);
int getargs2(const char *);
} */

void pbnum(int n);
#ifdef FLOAT
void pbfnum(float x);
#endif
void puttok(char *str);
int push(int ep, int *argstk, int ap);
void eval(int *argstk, int i, int j, int section);
int gettok(char *m_token);
void error(char *msg);
void putbak(char c);
void putchr(char c);
int ngetc(void);
void outputs(char *str);
void pbstr(char str[]);
void dodef(int *argstk, int i, int j, int section);
void doundef(int *argstk, int i, int j, int section);
void doinclude(int *argstk, int i, int j);
FILE *fpop(void);
struct flist *fpush(FILE *fptr);
#ifndef PRODUCTION_VERSION
void doif(int *argstk, int i, int j);
void doincr(int *argstk, int i, int j);
void dolen(int *argstk, int i, int j);
void doset(int *argstk, int i, int j, int section);
#endif
void doifdef(int *argstk, int i, int j, int section);
void doarith(int *argstk, int i, int j);
#ifdef FLOAT
void dofloat(int *argstk, int i, int j);
#endif
#ifndef PRODUCTION_VERSION
void dosub(int *argstk, int i, int j);
void docombine(int *argstk, int i, int j);
#endif
void roff(char c);
void entryreset(struct nlist *np);
void tablereset();
int mprint(int i);


	int  macro_debug;

	char  def[MAXDEF];       /* the definition */
	char  m_token[MAXTOK];
	char  defnam[12];
	char  undefnam[12];
	char  incrnam[12];
	char  ifelsenam[12];
	char  substrnam[12];
	char  ifdefnam[12];
	char  lennam[12];
	char  arithnam[12];
#ifdef FLOAT
	char  floatnam[12];
#endif
	char  setnam[12];
	char  enddefnam[12];
	char  def2nam[12];
	char  inclnam[12];
	char  combnam[12];

	char  incrtyp[2];
	char  ifelsetyp[2];
	char  substrtyp[2];
	char  undeftyp[2];
	char  deftyp[2];
	char  ifdeftyp[2];
	char  lentyp[2];
	char  arithtyp[2];
#ifdef FLOAT
	char  floattyp[2];
#endif
	char  settyp[2];
	char  enddeftyp[2];
	char  def2typ[2];
	char  incltyp[2];
	char  combtyp[2];
	static  int i;
	int  enflag,lineflag,margsflag,defflag;
	int  ap,*callst,nlb,*plev;
	int  *argstk;
	char  balp[3];

//	struct nlist *listptr;
	char  ans;
	int  hashi;

	int  file_input_flag=OFF;
//@-node:<< bridge2 declarations >>
//@nl
//@+others
//@+node:macro_verify
#if defined(linux)
#define EXTERN_HOFFER_C
#endif

	EXTERN_HOFFER_C int verify(int section) {
		int result =0;
		if (section & 1) result |= macro_verify();
		return result;
	}


EXTERN_HOFFER_C int macro_verify(void) {
	if (!macro_init_flag) return 0;
	if (macro_temp[M_MAX-1] != (char)255) return 0;
//	const char *temp = getAssoc("@define",2);
	domacro((const char *)"@define(MACROTEST,OK)",2);
	char *temp2 = getAssoc((const char *)"MACROTEST",2);
	if (temp2 == NULL) return 0;
	if (strcmp(temp2,"OK") != 0) return 0;
    domacro((const char *)"12 is not 15",2);
	// fprintf(stderr,"macrotemp:%s",macro_temp);
	if (strcmp(macro_temp,"12 is not 15") != 0) {
		fprintf(stderr,"bad macros again1:%s",macro_temp);
		return 0;
		}
    domacro((const char *)"12 is not 15",1);
	// fprintf(stderr,"macrotemp:%s",macro_temp);
	if (strcmp(macro_temp,"12 is not 15") != 0) {
		fprintf(stderr,"bad macros again2:%s",macro_temp);
		return 0;
		}

	return 1;
}
//@-node:macro_verify
//@+node:macro_init




EXTERN_HOFFER_C void macro_init(int setup)
{

	if (macro_init_flag) return;
	callst = new  int[CALLSIZE];
	plev = new  int[CALLSIZE];
	argstk= new  int[ARGSIZE];

    macro_init_flag = 1;
	myline[0]='\0';
	macro_temp[M_MAX-1]=(char)255;
	macro_temp[M_MAX-2]='\0';
	for (i=0; i<128; i++) is_type[i]=i;
	for (i=128; i<256; i++) is_type[i]=127; // ASCII only goes to 127
	for (i=48; i<58; i++) is_type[i]=DIGIT;
	for (i=65; i<91; i++) is_type[i]=LETTER;
	for (i=97; i<123; i++) is_type[i]=LETTER;
	is_type[95]=LETTER; is_type[64]=LETTER;
		macro_debug=0;
#ifndef PRODUCTION_VERSION
		strcpy(incrnam,"@incr");
		strcpy(ifelsenam,"@ifelse");
		strcpy(substrnam,"@substr");
		strcpy(lennam,"@len");
		strcpy(setnam,"@set");
        strcpy(enddefnam,".en");
		strcpy(def2nam,".de");
		strcpy(combnam,"@combine");
#endif
		strcpy(undefnam,"@undefine");
		strcpy(defnam,"@define");
		strcpy(ifdefnam,"@ifdef");
		if (setup != 1) strcpy(arithnam,"@arith");
#ifdef FLOAT
		strcpy(floatnam,"@float");
#endif
	strcpy(inclnam,"@include");

        deftyp[0]= (char)DEFTYP;
		deftyp[1]= (char)EOS;
        undeftyp[0]= (char)UNDEFTYP;
		undeftyp[1]= (char)EOS;
#ifndef PRODUCTION_VERSION
		incrtyp[0]= (char)INCTYPE;
		incrtyp[1]= (char)EOS;
		ifelsetyp[0]= (char)IFTYPE;
		ifelsetyp[1]= (char)EOS;
		substrtyp[0]= (char)SUBTYPE;
		substrtyp[1]= (char)EOS;
#endif
		ifdeftyp[0]= (char)IFDEFTYPE;
		ifdeftyp[1]= (char)EOS;
#ifndef PRODUCTION_VERSION
		lentyp[0]= (char)LENTYPE;
		lentyp[1]= (char)EOS;
#endif
		arithtyp[0]= (char)ARITHTYPE;
		arithtyp[1]= (char)EOS;
#ifdef FLOAT
		floattyp[0]= (char)FLOATTYPE;
		floattyp[1]= (char)EOS;
#endif
#ifndef PRODUCTION_VERSION
		settyp[0]= (char)SETTYPE;
		settyp[1]= (char)EOS;
		enddeftyp[0]= (char)ENDDEFTYPE;
		enddeftyp[1]= (char)EOS;
		def2typ[0]= (char)DEFTYP;
		def2typ[1]= (char)EOS;
#endif
	incltyp[0]= (char)INCLUDETYPE;
	incltyp[1]= (char)EOS;
#ifndef PRODUCTION_VERSION
	combtyp[0]= (char)COMBINETYPE;
	combtyp[1]= (char)EOS;
#endif

	bp= INITBP;             /* initialize the input buffer index */


		strcpy(balp,"()");

        TRIGGER(1,BidMessage((char *)"installing first\n"));

		install(defnam,deftyp);
        TRIGGER(1,BidMessage((char *)"first installed\n"));
		install(undefnam,undeftyp);
#ifndef PRODUCTION_VERSION
		install(incrnam,incrtyp);
		install(substrnam,substrtyp);
		install(ifelsenam,ifelsetyp);
#endif
		install(ifdefnam,ifdeftyp);
#ifndef PRODUCTION_VERSION
		install(lennam,lentyp);
#endif
		install(arithnam,arithtyp);
#ifndef PRODUCTION_VERSION
		install(setnam,settyp);
		install(enddefnam,enddeftyp);
		install(def2nam,def2typ);
#endif
#ifdef FLOAT
		install(floatnam,floattyp);
#endif
		install(inclnam,incltyp);
#ifndef PRODUCTION_VERSION
		install(combnam,combtyp);
#endif

		defflag=OFF;
		lineflag=OFF;
		margsflag=OFF;
		enflag=OFF;             /* after .en used to push rparen only */
		cp=-1;
		ap=0;
		ep=0;
		macro_init_flag = ON;

}
//@-node:macro_init
//@+node:clear_value_macros

void clear_value_macros(void) {
	int i;

#ifndef NO_MACROS_VERSION
	for (i=0; i<7; i++) {
		clearAssoc((char  *)"MYTOP",i);
		clearAssoc((char  *)"TOP",i);
		clearAssoc((char  *)"MYBOTTOM",i);
		clearAssoc((char  *)"BOTTOM",i);
		}
#else
   char forth_string[8];
   ONE_SHOT(241,do_log(1,10,(char *)"in Clearing value macros"));
   for (i=0; i<4; i++) {
       sprintf(forth_string,(char *)"%u %uG",40,70+i*2);
       ONE_SHOT(241,do_log(1,10,(char *)"call forth: %s",forth_string));
       f_eval(forth_string);
       sprintf(forth_string,(char *)"%u %uG",0,71+i*2);
       ONE_SHOT(241,do_log(1,10,(char *)"call forth: %s",forth_string));
       f_eval(forth_string);
       }
 //  pop_csw();

#endif  // NO_MACROS_VERSION

#ifdef HIGH_LOW
	for (i=0; i<4; i++)	{
		   high_low[i][0]=0;
		   high_low[i][1]=37;
		   }
#endif
	}
//@-node:clear_value_macros
//@+node:domacro
//@+at 
//@nonl
// #define MACRO_DEBUGGING
//@-at
//@@c

EXTERN_HOFFER_C char *my_domacro(const char *s, int section) {
    domacro(s,section);
    return macro_temp;
}

void domacro(const char *s,int section)
{
	bp= -1;
	roff_index = -1;
    int t;

    static int count = 0;
    // ONE_SHOT(201,do_log(1,20,"hoffer:domacro entered for first time"));

    if (count < 5) {
        // set_trigger("240");
#ifdef USE_ALFT
        // ALOG("\n<hoffer:%s>",s);
        // ALOG("%s","hoffer:calling do_log without using CSW_EXEC");
#endif
		TRIGGER(ID_MACROS,do_log(1,20,"hoffer:5 loop domacro entered; CSW_EXEC to follow"));
        // CSW_EXEC(do_log(1,20,"hoffer:domacro entered"));
		// CSW_EXEC(do_log(1,20,"hoffer:domacro entered CSW_EXEC; no parameters"));
        TRIGGER(ID_MACROS,do_log(1,20,"hoffer:testing first 5 macros:entered with:%s",s));
#ifdef USE_ALFT
        // ALOG("back from using CSW_EXEC");
#endif
        count++;
    }

#ifdef USE_MACRO_TEMP_ERROR
    macro_temp_error[0]='\0';
#endif

#ifndef PRODUCTION_VERSION
	assert(macro_init_flag == ON);
#endif

#ifdef WINSIGHT
	pbstr((char *)s);
#else
	pbstr((char  *)s);
#endif


	for (t=gettok(m_token); t != EOF; t=gettok(m_token)) {
#ifdef MACRO_DEBUGGING
				TRIGGER(240,BidMessage("\n<%s>",m_token));
#endif
#ifndef PRODUCTION_VERSION
		if (t==CONTROL_C) exit(10);
				else
#endif
					 if (t==ALPHA) {
#ifdef MACRO_DEBUGGING
                         TRIGGER(240,BidMessage("\nlookup: %s %u",m_token,section));
#endif
						 value = lookup(m_token,section);
						if (strcmp(value,"@@") == 0) {
#ifdef MACRO_DEBUGGING
                            TRIGGER(240,BidMessage("\noutputting token:%s",m_token));
#endif
							puttok(m_token);
                            }
						else   {
#ifdef MACRO_DEBUGGING
                                TRIGGER(240,BidMessage("\nprocessing token"));
#endif
#ifdef _WIN32
								if (value[0] == DEFTYP && strcmp(".de",m_token) == 0)
#else
								if (1 == DEFTYP && strcmp(".de",m_token) == 0)
#endif
                                        {
#ifdef MACRO_DEBUGGING
										TRIGGER(240,BidMessage("\nlineflag on"));
                                        TRIGGER(240,BidMessage("\ndefflag on"));
#endif
                                        lineflag=ON; 
                                        defflag=ON;
                                        }

                                                /* treat define a bit
                                        differently to allow the newline
                                        as a replacement for the comma in
                                        detecting new arguments        */
								if (strcmp(".en",m_token) == 0) {
                                        defflag=OFF;
                                        enflag=ON;
#ifdef MACRO_DEBUGGING
										if (macro_debug & 1) printf("\ndefflag off");
#endif
                                        }
								if (macro_debug & 1)
										printf("\nlineflag = %u",lineflag);

								cp++;
#ifndef PRODUCTION_VERSION
								if (cp >= CALLSIZE)
                                        error((char *)"call stack overflow");
#endif
								callst[cp]=ap;
								ap=push(ep,argstk,ap);
								puttok(value);
								putchr(EOS);
								ap=push(ep,argstk,ap);
								puttok(m_token);  /* stack name */
								putchr(EOS);
								ap=push(ep,argstk,ap);
								t=gettok(m_token);        /* peek at next */
								pbstr(m_token);
#ifdef KRTYPE

								if (enflag) {
                                        enflag=OFF;
                                        pbstr(")");
                                        }
								else if (defflag && margsflag==OFF) {
										t=gettok(m_token); /* strip ' ' */
                                        pbstr("(");
                                        margsflag=ON;
                                        }
								else if (t != LPAREN)  pbstr(balp);
#else 
                                if (t != LPAREN) pbstr(balp);
#endif
								plev[cp]=0;
								}
						}
				else if (t== LBRACK) {  /* strip one level of [] */
                        nlb=1;
						do {
                        	t=ngetc(); /* used to be gettok(m_token); */

							m_token[0]=t; m_token[1]=EOS;
							if (t==LBRACK) nlb++;
                            else if (t==RBRACK) nlb--;
							else if (t==EOF)
								error((char *)"EOF in brackets");
							if (nlb != 0) puttok(m_token);
							} while (nlb>0);
                        }
				else if (cp==-1) puttok(m_token);
                else if (t==LPAREN) {
						if (plev[cp] > 0) puttok(m_token);
                        plev[cp]++;
                        }
                else if (t==RPAREN) {
                        plev[cp]--;
						if (plev[cp] > 0) puttok(m_token);
                        else   {
                                putchr(EOS);
                                eval(argstk,callst[cp],ap-1,section);
								ap=callst[cp]; /* pop eval stack */
								ep=argstk[ap];
								cp--;
								t=gettok(m_token); /* get rid of newline */
								if ( t != NEWLINE) pbstr(m_token);
                                }
						}
                else if (t==NEWLINE && plev[cp] ==1 && margsflag==ON
                                        && defflag==OFF && lineflag==OFF) {
                                /* in argument collect for the line
                                   oriented macros...therefore the
                                   newline becomes the end of the def */
                        margsflag=OFF;
#ifdef MACRO_DEBUGGING
						if (macro_debug & 1) printf("\nend of macro args input");
						if (macro_debug & 1) printf("\npushing back a r paren");
#endif
                        putbak(')');
                        }
                else if (t==NEWLINE && plev[cp] == 1 && defflag==ON) {
                                                /* .de newline replaces , */
                        if (lineflag == ON) {
#ifdef MACRO_DEBUGGING
								if (macro_debug & 1)
                                        printf("\nnewline changed to comma");
#endif
                                lineflag=OFF;
                                putbak(',');
                                }
                                                /* looking to see if the
                                                newline directly precedes the
                                                .en, if so it will not be
												put into the m_token processing
																		*/
                        else    {
								t=gettok(m_token);
								pbstr(m_token);
								if (strcmp(".en",m_token) != 0)
                                        puttok((char *)"\n");
                                }
                        }
                else if (t==COMMA && plev[cp]==1) { /*new argument*/
#ifdef MACRO_DEBUGGING
						if (macro_debug & 1) printf("\nstarting to get new arg");
#endif
                        putchr(EOS);
                        ap=push(ep,argstk,ap);
                        }
				else puttok(m_token);
                }
#ifndef PRODUCTION_VERSION
		if (cp != -1)
			 error((char *)"unexpected EOF");
		assert(roff_index < M_MAX -2);
		assert(macro_temp[M_MAX-1]==(char)255);
#endif
		macro_temp[++roff_index]=EOS;
}
//@-node:domacro
//@+node:pbnum

void pbnum(int n)
{
        int i,j,sign;
        char s[7];


#ifdef MACRO_DEBUGGING
		if (macro_debug & 2) printf("\npbnum pushes back %d",n);
#endif
        if ((sign=n) < 0) n=-n;
        i=0;

        do {
                s[i++] = (n % 10) + '0';
        } while ((n /= 10) > 0);

        for (j=0; j<i; j++) putbak(s[j]);
        if (sign < 0) putbak('-');
}
//@-node:pbnum
//@+node:pbfnum

#ifdef FLOAT
void pbfnum(float x)
{
	char temp[20];
	int i;

	sprintf(temp,"%.2f",x);
	for (i=strlen(temp)-1; i>=0; i--) putbak(temp[i]);
}
//@-node:pbfnum
//@+node:puttok
#endif

/* PUTTOK - put a m_token either on output or into
        the evaluation stack */

void puttok(char *str)
{
	if (cp == -1) while (*str != EOS) roff(*str++);
	else {
#ifndef PRODUCTION_VERSION
		if (ep + strlen(str) >= EVALSIZE)
			error((char *)"eval stack overflow");
#endif
		while (*str != EOS) evalst[ep++]= *str++;
                }
}
//@-node:puttok
//@+node:putchr

void putchr(char c)
{
	if (cp== -1) roff(c);
	else  {
#ifndef PRODUCTION_VERSION
				if (ep >= EVALSIZE) error((char *)"eval stack overflow");
#endif
				evalst[ep]=c;
				ep++;
		}
/* see puttok as this is not the only funnel for output...
done for speedup.
*/
}
//@-node:putchr
//@+node:push


int push(int ep, int *argstk, int ap)
{
#ifndef PRODUCTION_VERSION
        if (ap >= ARGSIZE) error((char *)"arg stack overflow");
#endif
        argstk[ap]=ep;
        return(ap+1);
}
//@-node:push
//@+node:eval


void eval(int *argstk, int i, int j, int section)
{
	int argno,k,m,n,t;
	unsigned char td;
#ifdef MACRO_DEBUGGING
	int q;

	if (macro_debug & 2) fprintf(stderr,"\nI've entered eval ++++++++++++++");
		if (macro_debug & 2) {
                printf("\ncallst[cp]=%u ap-1=%u",i,j);
                }
#endif
        t=argstk[i];    /* points to 1st part of evstk for this set */

        td=evalst[t];   /* the first character of the defn              */

#ifdef MACRO_DEBUGGING
		if (macro_debug & 2) {
                printf("\nargstk=");
                for (q=0; q<j+1; q++) printf("%u-",argstk[q]);
                }
		if (macro_debug & 2) printf("\nargstk[callst[cp]]=%u",t);
		if (macro_debug & 2) printf("\nevalst[argstk[callst[cp]]]=%u",td);
		if (macro_debug & 2) {
                printf("\n");
                printf("ep=%u, evalst=\n",ep);
                for (q=0; q<ep; q++) 
                        if (evalst[q] >= 32) printf("%c-",evalst[q]);
                        else if (evalst[q] == 0) printf("EOS-");
                        else printf("*%u-",evalst[q]);
                }
#endif
		if (td==DEFTYP) dodef(argstk,i,j,section);
        else if (td==UNDEFTYP) doundef(argstk,i,j,section);
#ifndef PRODUCTION_VERSION
        else if (td==INCTYPE) doincr(argstk,i,j);
        else if (td==SUBTYPE) dosub(argstk,i,j);
        else if (td==IFTYPE) doif(argstk,i,j);
#endif
        else if (td==IFDEFTYPE) doifdef(argstk,i,j,section);
#ifndef PRODUCTION_VERSION
        else if (td==LENTYPE) dolen(argstk,i,j);
#endif
        else if (td==ARITHTYPE) doarith(argstk,i,j);
#ifdef FLOAT
	else if (td==FLOATTYPE) dofloat(argstk,i,j);
#endif
#ifndef	PRODUCTION_VERSION
	else if (td==SETTYPE) doset(argstk,i,j,section);
	else if (td==INCLUDETYPE) doinclude(argstk,i,j);
		else if (td==COMBINETYPE) docombine(argstk,i,j);
#endif
        else if (td==ENDDEFTYPE) {
                putbak(')');
#ifdef MACRO_DEBUGGING
				if (macro_debug & 1)
                        printf("\nend of define...pushing back a r paren");
#endif
                }
        else   {
#ifdef MACRO_DEBUGGING
		if (macro_debug & 2) printf("\nnon define evaluation entered");
#endif
                for (k=t +  (int)strlen(&evalst[t])  -1; k>=t; k--)
                        if (evalst[k-1] != ARGFLAG) putbak(evalst[k]);
                        else {
                                if (evalst[k]>= '0' && evalst[k] <='9')
                                        argno=evalst[k]-'0';
                                else argno= -1;
                                if (argno>= 0 && argno < j-i) {
                                        n=i+argno+1;
                                        m=argstk[n];
#ifdef MACRO_DEBUGGING
						if (macro_debug & 2) printf("\npushing back %s",&evalst[m]);
#endif
                                        pbstr(&evalst[m]);
                                        }
                                k--;
                                }
                if (k==t) putbak(evalst[k]);
                }
}
//@-node:eval
//@+node:gettok
//@+at 
//@nonl
// GETTOK - this gets a character from input and returns the character if it 
// is non-alphanumeric, otherwise it collects a string of alphanumeric 
// characters into m_token and its return value is then ALPHA
//@-at
//@@c

int gettok(char *m_token)
{
        int i,ans=EOS;

		i=0;
		while ( i < MAXTOK) {
				m_token[i]=ngetc();
				TRIGGER(ID_MACROS,do_log(1,10,"hoffer:m_token:i:<%d> m_token[%d]:c<%c> d<%d>\n",i,i,m_token[i],m_token[i]));
            if (m_token[i] >= 0 && m_token[i] < 127) ans=is_type[(unsigned int) m_token[i]];
            else ans=EOF;
            TRIGGER(ID_MACROS,do_log(1,10,"hoffer:is_type:ans:%d\n",ans,EOF));
				if (ans != LETTER && ans != DIGIT) {
                    // ALOG("hoffer:gettok:is_type of m_token[i]: %d", ans);
                    break;
                    }
				i++;
                }

#ifndef PRODUCTION_VERSION
		if (i >= MAXTOK) error((char *)"m_token too long");
#endif

        if (i > 0) {
				putbak(m_token[i]);       /* push back after going one too far */
                i--;                    /* point to last char in string      */
                ans=ALPHA;
                }

		m_token[i+1]=EOS;
        return(ans);
}
//@-node:gettok
//@+node:error

void error(char *msg)
{
	fprintf(stderr,"ERROR:%s:",msg);
	exit(10);
}
//@-node:error
//@+node:putbak
//@+at 
//@nonl
// type(c) char c; {     if (isalpha(c)) return(LETTER);     else if 
// (isdigit(c)) return(DIGIT);     else if (c=='@') return(LETTER); else if 
// (c=='_') return(LETTER); else return(c); }
//@-at
//@@c

void putbak(char c)
{
/*
		if (macro_debug & 2) printf("\nputting back >%c<",c);
*/
        if (c == 13) return;
        buf[++bp]=c;
#ifndef PRODUCTION_VERSION
	if (bp > (BSIZE-2)) error((char *)"too many characters pushed back");
#endif        
}
//@-node:putbak
//@+node:ngetc

int ngetc(void)
{
        int c;

		if (bp >= 0) c=buf[bp];
        else {
			bp=0;
			if (FPTR != NULL) {
				c=getc(FPTR);
				if (c==13) c=getc(FPTR);
				}
			else c=EOF;
			if (c == EOF && FPTR != NULL) {
					fclose(FPTR);
					FPTR = fpop();
					if (FPTR != NULL)
						c=getc(FPTR);
					}
			buf[bp]=c;
			}

	if (c != EOF) bp--;
	return(c);
}
//@-node:ngetc
//@+node:outputs
//@+at 
//@nonl
// OUTPUTS - This outputs the given string to the output file
//@-at
//@@c

void outputs(char *str)
{       int i=0;
	while (str[i] != '\0') roff(str[i++]);
}
//@-node:outputs
//@+node:pbstr
//@+at 
//@nonl
// PBSTR - push the given string back onto the input area (done in     reverse 
// order so that it may be reread
//@-at
//@@c

void pbstr(char str[])
{
		int i;
		if (str == NULL) return;
		for (i=(int)strlen(str)-1; i>=0; i--)
				putbak(str[i]);
}
//@-node:pbstr
//@+node:dodef

void dodef(int *argstk, int i, int j, int section)
{
        int a2,a3;

        if (j-i > 2) {
                a2=argstk[i+2];
                a3=argstk[i+3];
		if (macro_debug & 2) fprintf(stderr,"\nIn dodef");
				setAssoc(&evalst[a2],&evalst[a3],section);
                }
}
//@-node:dodef
//@+node:doundef

void doundef(int *argstk, int i, int j, int section)
{
        int a2;

        if (j-i > 1) {
                a2=argstk[i+2];
		if (macro_debug & 2) fprintf(stderr,"\nIn doundef");
				clearAssoc(&evalst[a2],section);
                }
}
//@-node:doundef
//@+node:doinclude

void doinclude(int *argstk, int i, int j)
{
	int a2;

	if (j-i > 1) {
		a2=argstk[i+2];
        printf("include: %s",&evalst[a2]);
		if ((FPTR2=fopen(&evalst[a2],"r")) == NULL) {
			fprintf(stderr,"Can't open %s",&evalst[a2]);
			return;
			}
		if (FPTR != NULL) {
			fpush(FPTR);
			FPTR = FPTR2;
			}
		else FPTR=FPTR2;
		}
}
//@-node:doinclude
//@+node:fpop


FILE *fpop(void)
{
	FILE *ptr;
	struct flist *temp;

	if (fheader == NULL) return(NULL);
	else {
		temp=fheader;
		ptr=fheader->fptr;
		fheader=fheader->next;
		free(temp);
		return(ptr);
		}
}
//@-node:fpop
//@+node:fpush

struct flist *fpush(FILE *fptr)
{
	struct flist *temp;

	if (fptr == NULL) return(NULL);

#ifdef _WIN32
	if ((temp = (struct flist *) _malloc_dbg(sizeof(temp), _NORMAL_BLOCK, __FILE__, __LINE__ )) == NULL){
    	my_error((char *)"Can't allocate in fpush");
		return(NULL);
		}
#else
	if ((temp = (struct flist *) malloc(sizeof(temp))) == NULL){
    	my_error((char *)"Can't allocate in fpush");
		return(NULL);
		}
#endif

	if (fheader==NULL) {
		fheader=temp;
		fheader->next=NULL;
		}
	else {
		temp->next = fheader;
		fheader = temp;
		}
	fheader->fptr = fptr;
	return(fheader);                        
}
//@-node:fpush
//@+node:doif


#ifndef PRODUCTION_VERSION
void doif(int *argstk, int i, int j)
{
        int a2,a3,a4,a5;

		if (macro_debug & 2) printf("\nIn doif");
        if (j-i<5) return;
        a2=argstk[i+2];
        a3=argstk[i+3];
        a4=argstk[i+4];
        a5=argstk[i+5];

        if (strcmp(&evalst[a2],&evalst[a3])==0)
                pbstr(&evalst[a4]);
        else pbstr(&evalst[a5]);
}
//@-node:doif
//@+node:docombine

void docombine(int *argstk, int i, int j)
{
		int a2,a3;

		a2 = argstk[i+2];
		a3 = argstk[i+3];

		pbstr(&evalst[a3]);
		pbstr(&evalst[a2]);
}
//@-node:docombine
//@+node:doincr


void doincr(int *argstk, int i, int j)
{
        int k,l;

		if (macro_debug & 2) printf("\nin doincr");
        k=argstk[i+2];
        l=atoi(&evalst[k]) + 1;
        pbnum(l);
}
//@-node:doincr
//@+node:dolen

void dolen(int *argstk, int i, int j)
{
        int k,l;

        k=argstk[i+2];
        l=(int)strlen(&evalst[k]);
		if (macro_debug & 2)
                printf("\n*****dolen sees %s and pushes %d",&evalst[k],l);
        pbnum(l);
}
//@-node:dolen
//@+node:doifdef
#endif

void doifdef(int *argstk, int i, int j, int section)
{
        int a2,a3;

		if (macro_debug & 2) printf("\ndoifdef entered");
        if (j-i < 3) return;

        a2=argstk[i+2];
        a3=argstk[i+3];
		if (getAssoc(&evalst[a2],section) != NULL)
			pbstr(&evalst[a3]);
}
//@-node:doifdef
//@+node:doset

#ifndef PRODUCTION_VERSION
void doset(int *argstk, int i, int j, int section)
{
		int a2,a3;

		if (macro_debug & 2) printf("\nIn doset");
        if (j-i < 3) return;

        a2=argstk[i+2];
        a3=argstk[i+3];

		if (lookup(&evalst[a2],section) != NULL) pbstr(&evalst[a3]);

		if (lookup(&evalst[a2],section) != NULL) {
	//		clearAssociation(&evalst[a2],section);
			setAssoc(&evalst[a2],&evalst[a3],section);
            }
}
//@-node:doset
//@+node:doarith

#endif

void doarith(int *argstk, int i, int j)
{
        char op;
        int op1,op2;
        int a2,a3,a4;
        int index = -1;

#ifdef MACRO_DEBUGGING
		if (macro_debug & 2) printf("\nArith entered");
#endif
        if (j-i < 4) return;

        a2=argstk[i+2];
        a3=argstk[i+3];
        a4=argstk[i+4];
		op1=atoi(&evalst[a2]);
		op2=atoi(&evalst[a4]);

#ifdef ARITH_GRAB
		if (evalst[a2]== '#')
			index=evalst[a2+1]-97;
		if (index >= 0)
#ifndef ARITH_VERSION2
			op1 = (int) grab[index].c - '0';
#else
			op1 = (int) grab[index] - '0';
#endif
#endif

		index = -1;
#ifdef ARITH_GRAB
		if (evalst[a4] == '#')
			index=evalst[a4+1]-97;
		if (index >= 0)
#ifndef ARITH_VERSION2
			op2 = (int) grab[index].c - '0';
#else
			op2 = (int) grab[index] - '0';
#endif
#endif			

        op=evalst[a3];

#ifdef MACRO_DEBUGGING
		if (macro_debug & 2) printf("\nop1 = %d, op2 = %d, op = %c",op1,op2,op);
#endif
        switch (op) {
                case '+': pbnum(op1 + op2); break;
                case '-': pbnum(op1 - op2); break;
                case '*': pbnum(op1 * op2); break;
                case '/': pbnum(op1 / op2); break;
                case '%': pbnum(op1 % op2); break;
                case '=': pbnum((op1 == op2) ? 1 : 0); break;
                case '>': pbnum((op1 > op2) ? 1 : 0); break;
                case '<': pbnum((op1 < op2) ? 1 : 0); break;
                case '|': pbnum((op1 | op2) ? 1 : 0); break;
                case '&': pbnum((op1 & op2) ? 1 : 0); break;
                case '!': if (evalst[a3 + 1] == '=') pbnum((op1 != op2) ? 1 : 0); break;
                default: break;
                }
}
//@-node:doarith
//@+node:dofloat


#ifdef FLOAT
void dofloat(int *argstk, int i, int j)
{
		char op;
	int a2,a3,a4;
	float op1,op2;

		if (macro_debug & 2) printf("\nFloat entered");
        if (j-i < 4) return;

        a2=argstk[i+2];
        a3=argstk[i+3];
        a4=argstk[i+4];

/*

	if (isalpha(evalst[a2])) {
                if (strcmp("bun",&evalst[a2])==0) strcpy(op1,bun);
                else if (strcmp("creat",&evalst[a2])==0) strcpy(op1,creat);
                else if (strcmp("sodium",&evalst[a2])==0) strcpy(op1,creat);
                else if (strcmp("potassium",&evalst[a2])==0)
                        strcpy(op1,potassium);
                else if (strcmp("cholesterol",&evalst[a2])==0)
                        strcpy(op1,cholesterol);
                else if (strcmp("sgot",&evalst[a2])==0) strcpy(op1,sgot);
                else if (strcmp("albumin",&evalst[a2])==0) strcpy(op1,albumin);
                else if (strcmp("protime",&evalst[a2])==0) strcpy(op1,protime);
                else if (strcmp("alkphos",&evalst[a2])==0) strcpy(op1,alkphos);
                else if (strcmp("calcium",&evalst[a2])==0) strcpy(op1,calcium);
                else if (strcmp("tfour",&evalst[a2])==0) strcpy(op1,tfour);
                else if (strcmp("bilirubin",&evalst[a2])==0) 
                        strcpy(op1,bilirubin);
                else if (strcmp("wbc",&evalst[a2])==0) strcpy(op1,wbc);
                else if (strcmp("hemoglobin",&evalst[a2])==0) 
                        strcpy(op1,hemoglobin);
                else if (strcmp("sedrate",&evalst[a2])==0) strcpy(op1,sedrate);
                }
        else atof(op1,&evalst[a2]);

        if (isalpha(evalst[a4])) {
                if (strcmp("bun",&evalst[a2])==0) strcpy(op2,bun);
                else if (strcmp("creat",&evalst[a2])==0) strcpy(op2,creat);
                else if (strcmp("sodium",&evalst[a2])==0) strcpy(op2,creat);
                else if (strcmp("potassium",&evalst[a2])==0)
                        strcpy(op2,potassium);
                else if (strcmp("cholesterol",&evalst[a2])==0)
                        strcpy(op2,cholesterol);
                else if (strcmp("sgot",&evalst[a2])==0) strcpy(op2,sgot);
                else if (strcmp("albumin",&evalst[a2])==0) strcpy(op2,albumin);
                else if (strcmp("protime",&evalst[a2])==0) strcpy(op2,protime);
                else if (strcmp("alkphos",&evalst[a2])==0) strcpy(op2,alkphos);
                else if (strcmp("calcium",&evalst[a2])==0) strcpy(op2,calcium);
                else if (strcmp("tfour",&evalst[a2])==0) strcpy(op2,tfour);
                else if (strcmp("bilirubin",&evalst[a2])==0) 
                        strcpy(op2,bilirubin);
                else if (strcmp("wbc",&evalst[a2])==0) strcpy(op2,wbc);
                else if (strcmp("hemoglobin",&evalst[a2])==0) 
                        strcpy(op2,hemoglobin);
                else if (strcmp("sedrate",&evalst[a2])==0) strcpy(op2,sedrate);
                }
        else atof(op2,&evalst[a4]);
*/
	op1 = atof(&(evalst[a2]));
	op2 = atof(&evalst[a4]);
	op=evalst[a3];


#ifdef MACRO_DEBUGGING
	if (macro_debug & 2) printf("\nop1 = %e, op2 = %e, op = %c",op1,op2,op);
#endif
	switch (op) {
		case '+': pbfnum(op1 + op2); break;
		case '-': pbfnum(op1 - op2); break;
		case '*': pbfnum(op1 * op2); break;
		case '/': pbfnum(op1 / op2); break;
		case '=': pbnum((op1 == op2) ? 1 : 0); break;
		case '>': pbnum((op1 > op2) ? 1 : 0); break;
		case '<': pbnum((op1 < op2) ? 1 : 0); break;
		case '!': if (evalst[a3 + 1] == '=')
			  pbnum((op1 != op2) ? 1 : 0); break;
		default: break;
		}
}
//@-node:dofloat
//@+node:dosub
#endif

#ifndef PRODUCTION_VERSION
void dosub(int *argstk, int i, int j)
{
        unsigned int ap,fc,k,nc;
        unsigned int sl;

		if (macro_debug & 2) printf("\ndosub entered");
        if (j-i < 3) return;
        if (j-i<4) nc=MAXTOK;
        else   {
                k=argstk[i+4];
                nc=atoi(&evalst[k]);    /* number of characters */
                }
        k=argstk[i+3];          /* position of the new string   */
        ap=argstk[i+2];                 /* start of original string     */

        fc=ap + atoi(&evalst[k]) -1;

        sl=(int)strlen(&evalst[fc]);         /* the string length    */
        if (fc >= ap && fc < ap + strlen(&evalst[ap])) {
                k=fc -1 +((nc > sl)  ?  sl  : nc) ;
                while (k >= fc) putbak(evalst[k--]);
                }
}
//@-node:dosub
//@+node:roff
#endif

void roff(char c) {
	if (roff_index >= (M_MAX-2))
		fprintf(stderr,"bad");
	assert(roff_index <  M_MAX -2);
	assert(macro_temp[M_MAX-1]==(char)255);
	macro_temp[++roff_index]=c;
}
//@nonl
//@-node:roff
//@-others
//@-node:@file C:/html/bid20/bridge2.cpp
//@-leo
