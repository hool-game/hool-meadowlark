#pragma once

#include <napi.h>

class Hool : public Napi::ObjectWrap<Hool>
{
public:
    Hool(const Napi::CallbackInfo&);
    Napi::Value Play(const Napi::CallbackInfo&);
    ~Hool();

    static Napi::Function GetClass(Napi::Env);

private:
    int who;
    int where;
    int what;
    int lo;
    int high;
};
