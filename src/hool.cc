#include "hool.h"
#include "hello.h"
using namespace Napi;

Hool::Hool(const Napi::CallbackInfo& info) : ObjectWrap(info) {
    Napi::Env env = info.Env();

    if (info.Length() < 5) {
        Napi::TypeError::New(env, "Wrong number of arguments")
          .ThrowAsJavaScriptException();
        return;
    }
    for(int i=0;i<5;i++){

    if (!info[i].IsNumber()) {
        Napi::TypeError::New(env, "Please input arguments of proper type")
          .ThrowAsJavaScriptException();
        return;
    }
    }
    this->who = info[0].As<Napi::Number>().Int64Value();;
    this->what =  info[1].As<Napi::Number>().Int64Value();;    
    this->where =  info[2].As<Napi::Number>().Int64Value();;
    this->lo =  info[3].As<Napi::Number>().Int64Value();;
    this->high =  info[4].As<Napi::Number>().Int64Value();;



}
Hool::~Hool() {

}
Napi::Value Hool::Play(const Napi::CallbackInfo& info) {
    Napi::Env env = info.Env();

    std::string toBePlayed= generateCommandString(this->who,this->what,this->where,this->lo,this->high);
    return Napi::String::New(env, toBePlayed);
}

Napi::Function Hool::GetClass(Napi::Env env) {
    return DefineClass(env, "Hool", {
        Hool::InstanceMethod("play", &Hool::Play),
    });
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    Napi::String name = Napi::String::New(env, "Hool");
    exports.Set(name, Hool::GetClass(env));
    return exports;
}
 
NODE_API_MODULE(addon, Init)
