$ verbose = false
? verbose
    > "verbose is true"
    }
? arguments.length() != 2
    > "test requires the name of an rbn file and the number of the hand to use"
    * exit(1)
    }
? verbose
    > "argument count is OK at 2"
    }

$ file_name = arguments.get(0)
? file_name.getType() != "string"
    > "parameter 1, file_name must be a string"
    * exit(1)
    }
? !file_name.isMatch("^[A-Za-z_][A-Z_a-z0-9]*\.rbn$")
    > "the first argument must be a file name, type rbn"
    * exit(1)
    }
? verbose
    > "file name is correct"
    }
$ count = number(arguments.get(1))
? count.getType() != getType(1)
    > "the second argument must be a number, the hand to use"
    * exit(1)
    }
$ command = "pushd c:\html" + crlf
& command = command + "python c:\html\dobid.py "
& command = command + "-n 1 -s " +  count + " -h "
& command = command + "-f " + "c:\html\" + file_name + " "
& command = command + "-b102,102  > temp.txt" + crlf
! & command = command + "-b102,102  | fgrep HOOL | sed s/HOOL// > temp.txt" + crlf
& command = command + "popd" + crlf
> command
* writeFile("test.cmd",command,"ascii")
* exec("test")
& command = "copy c:\html\temp.txt hand.txt"  + crlf
> command
* writeFile("test.cmd",command,"ascii")
* exec("test")
> "Done."
* exit(0)


