#pragma once

#include <string>

const std::string generateHelloString(const std::string & personName);
const std::string generateCommandString(unsigned int who, unsigned int what, unsigned int where, unsigned int low, unsigned int high);