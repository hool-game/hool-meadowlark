/* Versions
210209 No changes from windows version
*/
#include "meadowlark.h"
#undef GLOBALS
#include "../include/GLOBALS.H"

#if defined(LINUX) || defined(linux)
int errorcheck(int *);
#endif

#ifndef DIVORCE
#if !defined(NO_ASSERTS)
#include "assert.h"
#endif
void sort(int *,int,int,int);
void output(char* fmt, ...);

void printout(int deck[52],int first) {
	int abbrev[4];
	int cp,dp,hp,x,i;

	abbrev[0]='J';	abbrev[1]='Q';	abbrev[2]='K';	abbrev[3]='A';
	sort(deck,first,13,DOWN);
	cp=dp=hp=FALSE;

	fprintf(stderr,"\n\nS: ");
	for (x=first; x <=first + 12; x++) {
		if (hp==FALSE && deck[x] <39) { hp=TRUE; fprintf(stderr,"\nH: "); }
		if (dp==FALSE && deck[x] <26) { dp=TRUE; fprintf(stderr,"\nD: "); }
		if (cp==FALSE && deck[x] <13) { cp=TRUE; fprintf(stderr,"\nC: "); }
		i=deck[x] - (13 * (deck[x]	/13));
		if (i>8) fprintf(stderr,"%c ",abbrev[i-9]);
		else fprintf(stderr,"%u ",i+2);
		if ( x== first + 12) {
			if (hp == FALSE) fprintf(stderr,"\nH: ");
			if (dp == FALSE) fprintf(stderr,"\nD: ");
			if (cp == FALSE) fprintf(stderr,"\nC: ");
			}
		}
}

void sort_the_hands(int deck[52]) {
    sort(deck,0,13,UP);
    sort(deck,13,13,UP);
    sort(deck,26,13,UP);
    sort(deck,39,13,UP);
    }

int get_ms(int deck[52], char *szFormat) {
   int i,j,len = 0,flags[3];
   char cards[13][2];


       for (j=0; j<13; j++) {
           switch (j) {
                   case 0: cards[j][0]='2'; break;
                   case 1: cards[j][0]='3'; break;
                   case 2: cards[j][0]='4'; break;
                   case 3: cards[j][0]='5'; break;
                   case 4: cards[j][0]='6'; break;
                   case 5: cards[j][0]='7'; break;
                   case 6: cards[j][0]='8'; break;
                   case 7: cards[j][0]='9'; break;
                   case 8: cards[j][0]='T'; break;
                   case 9: cards[j][0]='J'; break;
                   case 10:cards[j][0]='Q'; break;
                   case 11:cards[j][0]='K'; break;
                   case 12:cards[j][0]='A'; break;
                   }
               cards[j][1] = '\0';
               }

       for (i=0; i< 4; i++) {
           for (j=0; j<3; j++) flags[j]=OFF;
           for (j=12; j>=0; j--) {
               if (flags[0] == OFF && (deck[i*13+j] /13) < 3) {
                   strcpy(&szFormat[len],"."); len++; flags[0]=ON;
                   }
               if (flags[1] == OFF && (deck[i*13+j] /13) < 2) {
                   strcpy(&szFormat[len],"."); len++; flags[1]=ON;
                   }
               if (flags[2] == OFF && (deck[i*13+j] /13) < 1) {
                   strcpy(&szFormat[len],"."); len++; flags[2]=ON;
                   }
               strcpy(&szFormat[len],cards[deck[i*13+j]%13]);
                       len++;
               }
           if (flags[1] == OFF) {
               strcpy(&szFormat[len],".");
               len++;
                       }
           if (flags[2] == OFF) {
               strcpy(&szFormat[len],".");
               len++;
               }
                   if (i<3) { strcpy(&szFormat[len],"\n"); len++; }
           }
       return 1;
}




int get_matt_string2(int deck[52], char *szFormat) {
    sort_the_hands(deck);
    get_ms(deck,szFormat);
    return 1;
    }



// #pragma message("before CLEAN_UP")
#undef CLEAN_UP
#define USE_IO3
#ifndef CLEAN_UP
// #pragma message("CLEAN_UP is not defined")
// #include "stdafx.h"
#include "../include/Pch.h" // note that this includes proto.h and proto2.h
#if defined(LINUX)
/* because not precompiling */
#endif

#include "../include/GLOBALS.H"
#ifdef WIN32
#include <stdio.h>
// #include "..\include\io.h"
#endif
#include "../include/myiff.h"
#include "../include/tegl_iff.h"
#include "ctype.h"
#include "../include/bergen.h"
#include "../include/bridge.h"

#define IOS
// #pragma message("IOS defined here")

#ifdef CXLOGGING
#define LOGIMPORT __declspec(dllimport)



LOGIMPORT int __stdcall LogDebug(const char *);
LOGIMPORT int __stdcall LogInfo(const char *);
LOGIMPORT int __stdcall LogWarning(const char *);
LOGIMPORT int __stdcall LogError(const char *);
LOGIMPORT int __stdcall StartLoggingStderr(unsigned long,const char *);
LOGIMPORT int __stdcall StartLoggingStdout(unsigned long,const char *);
LOGIMPORT int __stdcall StartLogging(const char *, unsigned long, unsigned long, unsigned long, const char *);
LOGIMPORT int __stdcall StopLogging();
#endif



void cx_test(const char *message);

void cx_test(const char *message) {
#ifdef CXLOGGING
	LogError(message);
#endif
}



// #define sprintf sprintf_s
// #define strcat strcat_s
// #define strtok strtok_s
// #define strcpy strcpy_s

#ifdef __DLL__
#define MBCALL
// #define MB_BAD
#endif

#ifndef GEN32
#ifdef MB_BAD
void c_do_execute(char far *);
#endif
#endif
void hand_print(int deck[52],int k,int w);
void hand_set(int deck[52]);
void print_bids(char *str, int dealer);
void bids_out(void);
void hand_out(void);					
void hand_copy2(int, int);
void hand_copy(int, int);
int read_hand(char *, int [13]);
int read_card(char *);
int convert_matt_to_deck(char *, int hand, int deck[52]);
void sort_the_hands(int [52]);
int get_matt_string2(int [52], char *);
int get_matt_string(char *, char *);

#ifndef IOS
HINSTANCE hinstH=NULL;
void (FAR *lpfnCDoEx)(LPSTR);
#endif

#define HEIGHT 16
#define WIDTH 61
#define SINGLE_HAND_HEIGHT 5
#define SINGLE_HAND_WIDTH 30

char hands_buffer[HEIGHT][WIDTH];
char hand_buffer[SINGLE_HAND_HEIGHT][SINGLE_HAND_WIDTH];
int right_margin_1, right_margin_2;
int offset_1, offset_2;
char page[HEIGHT][181];
char bids[40][40];
int margin, width;
#ifdef STAND_ALONE
int log_flag=0;
#else
int log_flag=1;
#endif

void pairprint(int deck[52],int num, int dealer, int responder)
{
	int abbrev[4];
	char hand[4][WIDTH];
		int cp,dp,hp,x,i,j,k,l,left=0,first,pos[2];

		abbrev[0]='J';	abbrev[1]='Q';	abbrev[2]='K';	abbrev[3]='A';

	for (i=0;i<4;i++)
		for (j=0;j<WIDTH;j++) hand[i][j]=' ';

	output((char *)"\nDealer	   %3u			   Responder	%3u",num,num);
	output((char *)"\n----------------			   ----------------");

	if (dealer ==0 || responder ==0) {
		output((char *)"\nBad dealer or responder index (0)");
		return;
		}
	pos[0]=dealer-1; pos[1]=responder-1;
	for (k=0; k<2; k++) {
		first=pos[k]*13;
		sort(deck,first,13,DOWN);

			cp=dp=hp=FALSE;

		if (k==0) {i=0; left=0;}
		else if (k==1) {i=0; left=30;}
		j=left;

			hand[i][j++]='S'; hand[i][j++]=':'; hand[i][j++]=' ';
			for (x=first; x <=first + 12; x++) {
				  if (hp==FALSE && deck[x] <39) { 
						hp=TRUE; i++; j=left;
						hand[i][j++]='H';
				hand[i][j++]=':';
				hand[i][j++]=' ';
						  }
				  if (dp==FALSE && deck[x] <26) { 
							dp=TRUE; i++; j=left;
							hand[i][j++]='D';
				hand[i][j++]=':';
				hand[i][j++]=' ';
							}
				  if (cp==FALSE && deck[x] <13) { 
							cp=TRUE; i++; j=left;
							hand[i][j++]='C';
				hand[i][j++]=':';
				hand[i][j++]=' ';
							}
				  l=deck[x] - (13 * (deck[x]  /13));
				  if (l>8)	{
				hand[i][j++]= (char) abbrev[l-9];
				hand[i][j++]=' ';
				}
				  else if (l==8) {
				hand[i][j++]='1';
				hand[i][j++]='0';
				hand[i][j++]=' ';
				}
		  else {
				hand[i][j++]=(char) 50+l;
				hand[i][j++]=' ';
				}
		  if (x==first + 12 && dp==FALSE) {
							i++; j=left;
				hand[i][j++]='D';
				hand[i][j++]=':';
				hand[i][j++]=' ';
				}
				  if (x==first + 12 && cp==FALSE) {
							i++; j=left;
				hand[i][j++]='C';
				hand[i][j++]=':';
				hand[i][j++]=' ';
							}
				  }
		}

	{ char temp[4][255];

	for (i=0;i<4;i++) {
		for (j=0;j<WIDTH;j++) temp[i][j]=hand[i][j];
		temp[i][WIDTH]='\0';
		}
	output((char *)"\n");
	output(temp[0]);
	output((char *)"\n");
	output(temp[1]);
	output((char *)"\n");
	output(temp[2]);
	output((char *)"\n");
	output(temp[3]);
	output((char *)"\n");	 }
}
// Printing out hands...  1    10	 20    30	 40    50  |	|	  | 	
// |	 |	   |  __________________________________________________ |		 
// S:				  | |		H: A K Q J 10 9 8 7 6 5 4 3 2	 | |		
// D:				  | |		C:				   | |							
// | |S:			  S: A K Q J 10 9 8 7 6| |H:			  H:	  5 4 3 2| 
// |D: A K Q J 10 9 8 7 6 5 4 3 2D: 		 | |C:				C:			| 
// |						 | |	   S:				  | |		
// H:				  | |		D:				   | |		 C: A K Q J 10 9 8 
// 7 6 5 4 3 2	  | 
// |__________________________________________________| 		
// A				  B  VARIABLES NEEDED: A: The distance from the left 
// margin to indent for hands B: The total width for the display.  EXAMPLE: 
// Here A=15, B=51 ... allowing 9 card hands to be displayed in the   East 
// hand. Other cards are placed down one line if needed. A= 15, B=	 60 would 
// allow all hands to be displayed without dropping down a line.   This 
// function returns an array filled to the given specifications,   however it 
// is always of size x=60, y=14.  Pica = 10 CPI, Elite = 12, Condensed Pica = 
// 17, Condensed Elite = 20  Given condensed elite with margins of 5 left and 
// right; while between   the hands is 4 leaves 162 spaces for 3 hands 
// across... ie B=54. In that	situation if A=12, then East hands will fold 
// after 10. If A=15 in that same	situation, then West will fold after 10 
// cards.

void hand_print_setup(int m, int w)
{
	if (m < 12 || w < 40) {
		margin=15; width=60;
		}
	else { margin=m; width=w; }

	right_margin_1 = margin * 2;
	right_margin_2 = width;
	offset_1 = 30 - right_margin_1;
	offset_2 = 30 - right_margin_2;
}

void handprint(int deck[52])
{
	hand_set(deck);
	hand_out();
}


void hand_set(int deck[52])
{
	int i,j;

	for (i=0; i<HEIGHT; i++)
		for (j=0; j<WIDTH; j++) hands_buffer[i][j]=' ';


	hand_print(deck,NORTH,30);
/*	  hand_copy(margin,1); */
	hand_copy(1,1);
	hand_print(deck,WEST,margin*2);
	hand_copy(1,6);
	hand_print(deck,SOUTH,30);
/*	hand_copy(margin,11); */
	hand_copy(1,11);
	hand_print(deck,EAST,width-margin*2);
	hand_copy(margin*2-5,6);
	hands_buffer[0][1]='\0';
	hands_buffer[5][1]='\0';
	hands_buffer[10][1]='\0';
	hands_buffer[15][1]='\0';
}

// #pragma message("hand and bids")
void hand_and_bids(int deck[52], char *sequence, int dealer)
{
	hand_print_setup(15,60);
	hand_set(deck);
	hand_out();
	print_bids(sequence,dealer);
	bids_out();
}


#ifdef DYNAMIC_CALL_EXE
int FAR _export load_mb(void) {
	MessageBox(GetFocus(),"loading mb from gen.dll","err?",MB_TASKMODAL);
	if (hinstH==NULL) {
	   hinstH=LoadLibrary("MB.DLL");
	   if ((UINT)hinstH > 32) {
		(FARPROC) lpfnCDoEx = GetProcAddress(hinstH,"_c_do_execute");
		if (lpfnCDoEx == NULL) {
			MessageBox(GetFocus(),"lpfnCDoEx function not found","GEN.DLL",MB_ICONHAND);
			FreeLibrary(hinstH);
			}
		}
	   else MessageBox(GetFocus(),"LoadLibrary failed for mb.dll","GEN.DLL",MB_ICONHAND);
	   }
	return 1;
	}

void unload_mb(void) {
  if (hinstH) FreeLibrary(hinstH);
  }
#endif


void bids_only(char *sequence, int dealer, int number)
{
	int i,j;
	char temp[20];

	for (i=0; i<40; i++)
		for (j=0; j<40; j++)
			bids[i][j]=' ';
	sprintf(temp,"Hand %u\n",number);
	output(temp);

	print_bids(sequence,dealer);
	bids_out();
}

void print_pair(char * indeck, int left, int right, int hand) {
	int deck[52],i;

	for (i=0; i< 52; i++)
		deck[i] = ((int) indeck[i]) - 32;
	pairprint(deck,left,right,hand);
	}

void print_hand_out(char * indeck, int pos) {
	int deck[52],i,j,flag;

	for (i=0; i< 52; i++)
		deck[i] = ((int) indeck[i]) - 32;
	hand_print(deck,pos,30);

	for (i=0; i<SINGLE_HAND_HEIGHT; i++) {
		flag = ON;
		for (j= SINGLE_HAND_WIDTH - 3; j >= 0 && flag == ON; j--)
			if (hand_buffer[i][j] != ' ') {
				hand_buffer[i][j+1]='\n';
				hand_buffer[i][j+2]='\0';
				flag= OFF;
				}
		}
	output((char *)hand_buffer[0]);
	output((char *)hand_buffer[1]);
	output((char *)hand_buffer[2]);
	output((char *)hand_buffer[3]);
	}

#define FIRST_GET_PRINT_ALL
static char result[255];
char *get_print_all(char *);
char *get_print_all(char * indeck) {
#ifdef FIRST_GET_PRINT_ALL
	int deck[52];
	int i,hoi,j;
	char temp[HEIGHT][200];

	for (i=0; i< 52; i++)
		deck[i] = ((int) indeck[i]) - 32;

	if (width < 30) hand_print_setup(15,60);
	hand_set(deck);


	for (i=0;i<HEIGHT;i++) {
		for (j=0;j<width;j++) temp[i][j]=hands_buffer[i][j];
		temp[i][width]='\0';
		}
	result[0]='\0';
	for (hoi=0; hoi<HEIGHT; hoi++) {
		strcat(result,"\n");
		strcat(result,temp[hoi]);
		}
	strcat(result,"\n");
	return result;
#else
	char far *temp;

	result[0]='\0';
	temp = get_print_hand(indeck,0);
	strcpy(result,temp);
	temp = get_print_hand(indeck,1);
	strcat(result,temp);
	temp = get_print_hand(indeck,2);
	strcat(result,temp);
	temp = get_print_hand(indeck,3);
	strcat(result,temp);
	return result;
#endif

}



char *get_print_hand(char *,int);
char *get_print_hand(char *indeck, int pos) {
	int deck[52],i,j,flag;

	for (i=0; i< 52; i++)
		deck[i] = ((int) indeck[i]) - 32;
	hand_print(deck,pos,30);

	for (i=0; i<SINGLE_HAND_HEIGHT; i++) {
		flag = ON;
		for (j= SINGLE_HAND_WIDTH - 3; j >= 0 && flag == ON; j--)
			if (hand_buffer[i][j] != ' ') {
				hand_buffer[i][j+1]='\n';
				hand_buffer[i][j+2]='\0';
				flag= OFF;
				}
		}

	strcpy(result,(char *)hand_buffer[0]);
	strcat(result,(char *)hand_buffer[1]);
	strcat(result,(char *)hand_buffer[2]);
	strcat(result,(char *)hand_buffer[3]);
	return (char *) result;
	}


void set_log(int value) {
	log_flag = value;
	}

void hand_print_n(int deck[52], int, int, int);

void output_ending(char *indeck, int num, int hand) {
	int deck[52],i,j;

	for (i=0; i< 52; i++)
		deck[i] = ((int) indeck[i]) - 32;
	hand_print_setup(15,60);

	for (i=0; i<HEIGHT; i++)
		for (j=0; j<WIDTH; j++) hands_buffer[i][j]=' ';

	if (hand == 0) hand = 15;  /* do all hands */
	if (hand & 1) {
		hand_print_n(deck,NORTH,num,30);
		hand_copy(margin,1);
		}
	if (hand & 2) {
		hand_print_n(deck,WEST,num,margin*2);
		hand_copy(1,6);
		}
	if (hand & 4) {
		hand_print_n(deck,SOUTH,num,30);
		hand_copy(margin,11);
		}
	if (hand & 8) {
		hand_print_n(deck,EAST,num,width-margin*2);
		hand_copy(margin*2,6);
		}
	hand_out();
}

void all_out(char *indeck, char *sequence, int dealer)
{
	int deck[52],i;

	for (i=0; i< 52; i++)
		deck[i] = ((int) indeck[i]) - 32;
	hand_and_bids(deck,sequence,dealer);
}

void hand_copy(int x, int y)
{
	int i,j;

	for (i=0; i<SINGLE_HAND_HEIGHT; i++)
		for (j=0; j<SINGLE_HAND_WIDTH; j++)
			hands_buffer[i+y][x+j] = hand_buffer[i][j];

}

void pageprint(int deck[52], int pos)
{
	int k, l, m, n, xbase, xpos;
	char temp[200];

	hand_print_setup(12,44);

	xpos = pos % 3;
	xbase = xpos*(4+width);

	if (xpos == 0)
		for (n=0; n<HEIGHT; n++)
			for (m=0;m<180; m++) page[n][m]=' ';

	hand_print(deck,NORTH,30);
	hand_copy2(margin+xbase,1);
	hand_print(deck,WEST,margin*2);
	hand_copy2(1+xbase,6);
	hand_print(deck,SOUTH,30);
	hand_copy2(margin+xbase,11);
	hand_print(deck,EAST,width-margin*2);
	hand_copy2(margin*2+xbase,6);

	if (xpos == 2)
		for (k=0;k<HEIGHT;k++) {
			output((char *)"\n");
			for (l=0;l<150;l++)
				temp[l]=page[k][l];
			temp[150]='\0';
			output(temp);
			}
}


void hand_copy2(int x, int y)
{
	int i,j;

	for (i=0; i<SINGLE_HAND_HEIGHT; i++)
		for (j=0; j<SINGLE_HAND_WIDTH; j++) {
			page[i+y][j+x] = hand_buffer[i][j];
			}			
}

int hoi;

void hand_out(void)
{
	int i,j;
	char temp[HEIGHT][200];

	for (i=0;i<HEIGHT;i++) {
		for (j=0;j<width;j++) temp[i][j]=hands_buffer[i][j];
		temp[i][width]='\0';
		}
	for (hoi=0; hoi<HEIGHT; hoi++) {
		output((char *)"\n");
		output(temp[hoi]);
		}
	output((char *)"\n");
}


void bids_out(void)
{
	int i;

	for (i=0; bids[i][0] != '\0'; i++)
		output((char *)bids[i]);
}

void hand_print(int deck[52], int k, int w) {
	hand_print_n(deck,k,13,w);
	}

void hand_print_n(int deck[52],int k, int num, int w)
{
	int abbrev[4];
	int cp,dp,hp,x,i,j,l,first;

	abbrev[0]='J';	abbrev[1]='Q';	abbrev[2]='K';	abbrev[3]='A';

	for (i=0;i<SINGLE_HAND_HEIGHT;i++)
		for (j=0;j<SINGLE_HAND_WIDTH;j++) { hand_buffer[i][j]=' '; }

	first=k*13;
	sort(deck,first,num,DOWN);
	i=0;
	cp=dp=hp=FALSE;
			j=0;
			hand_buffer[i][j++]='S'; hand_buffer[i][j++]=':';
			hand_buffer[i][j++]=' ';
			for (x=first; x <=first + (num-1); x++) {
				  if (hp==FALSE && deck[x] <39) { 
					  hp=TRUE; if (k != 3) { hand_buffer[i][j]='\0';} i++; j=0;
						hand_buffer[i][j++]='H';
				hand_buffer[i][j++]=':';
				hand_buffer[i][j++]=' ';
						  }
				  if (dp==FALSE && deck[x] <26) { 
					  dp=TRUE; if (k!=3) {hand_buffer[i][j]='\0';} i++; j=0;
							hand_buffer[i][j++]='D';
				hand_buffer[i][j++]=':';
				hand_buffer[i][j++]=' ';
							}
				  if (cp==FALSE && deck[x] <13) { 
					  cp=TRUE; if (k!=3) {hand_buffer[i][j]='\0';} i++; j=0;
							hand_buffer[i][j++]='C';
				hand_buffer[i][j++]=':';
				hand_buffer[i][j++]=' ';
							}
				  l=deck[x] - (13 * (deck[x]  /13));
				  if (l>8)	{
				hand_buffer[i][j++]=(char) abbrev[l-9];
				hand_buffer[i][j++]=' ';
				}
				  else if (l==8) {
				hand_buffer[i][j++]='1';
				hand_buffer[i][j++]='0';
				hand_buffer[i][j++]=' ';
				}
		  else {
				if (j<w) {
					hand_buffer[i][j++]=(char) 50+l;
					hand_buffer[i][j++]=' ';
					}
				else {
					hand_buffer[i+1][-30+w+j]=(char) 50+l; j++;
					hand_buffer[i+1][-30+w+j]=' '; j++;
					}
				}
		  if (x==first + (num-1) && hp==FALSE) {
							i++; j=0;
				hand_buffer[i][j++]='H';
				hand_buffer[i][j++]=':';
				hand_buffer[i][j++]=' ';
				}

		  if (x==first + (num-1) && dp==FALSE) {
							i++; j=0;
				hand_buffer[i][j++]='D';
				hand_buffer[i][j++]=':';
				hand_buffer[i][j++]=' ';
				}
		  if (x==first + (num-1) && cp==FALSE) {
							i++; j=0;
				hand_buffer[i][j++]='C';
				hand_buffer[i][j++]=':';
				hand_buffer[i][j++]=' ';
							}
				  }
			if (k!=3) {hand_buffer[i][j]='\0';}
}



int read_card(char *p)
{	char c;
	int rank, suit;

#ifndef NO_ASSERTS
    assert(p!=NULL);
#endif
    c = *p;
	if (c == '\0') return ERR;
	switch (toupper(c)) {
		case '2': case '3': case '4': case '5': case '6':
		case '7': case '8': case '9': rank = c - '2'; break;
		case 'T': rank = 8;  break;
		case 'J': rank = 9;  break;
		case 'Q': rank = 10; break;
		case 'K': rank = 11; break;
		case 'A': rank = 12; break;
		default: return ERR;
		}
	c = *++p;
	switch (toupper(c)) {
		case 'C': suit = 0; break;
		case 'D': suit = 1; break;
		case 'H': suit = 2; break;
		case 'S': suit = 3; break;
		default: return ERR;
		}
	return(rank + suit * 13);
}
// important to realize that p has already had strtok called hence the further 
// calls to strtok with a null pointer

int read_hand(char *p, int hand[13]) {
	int i,j;

#ifndef NO_ASSERTS
    assert(p != NULL);
#endif
    
	for (i=0; i< 13; i++) {
		if (p) {
			if ((j=read_card(p)) == ERR)
				return ERR;
			else hand[i] = j;
			}
		else {
			gen_err((char *)"p is null in read hand");
			return 0;
			}
		p=strtok(NULL," ,");
		}
#ifndef NO_ASSERTS
	assert(i==13);
#endif
	return(i==13 ? OK : ERR);
}

int read_hand2(char *p, char *result, char *d1, char *d2,char *d3, char *d4) {
	int i,j,k,hand[52],h[52];
	char *temp;
	temp=p;

#ifndef NO_ASSERTS
    assert(temp != NULL);
#endif
    for (i=0; i< 13; i++) {
		if (temp == NULL) return ERR;
		else if ((j=read_card(temp)) == ERR) return ERR;
		else { hand[i]=j; }
		temp += 2;
		while (*temp == ' ' || *temp == ',') temp++;
		}

	for (j=0;j<52;j++) h[j]=1;
	for (j=0;j<13;j++) h[hand[j]] = 0;
	k=13;
	for (j=0;j<52;j++) if (h[j]) hand[k++]=j;
	gen32_shuffle(hand,13,39);


	for (j=0; j<13; j++) {	   /* exchange south and north hands */
		k=hand[26+j];
		hand[26+j]=hand[j];
		hand[j]=k;
		}

	sort(hand,0,13,UP);
	sort(hand,13,13,UP);
	sort(hand,26,13,UP);
	sort(hand,39,13,UP);
#ifndef USE_BERGEN
	get_description(hand,0,d1);
	get_description(hand,1,d2);
	get_description(hand,2,d3);
	get_description(hand,3,d4);
#endif

#if defined(NEED_ERRORCHEC)
	if (errorcheck(hand) == ERR) {
			gen_err((char *)"errorcheck problem in gethand");
			return(ERR);
			}
#endif

	for (j=0; j< 52; j++) result[j] = hand[j] + 32;

#ifndef NO_ASSERTS
    assert(i==13);
#endif
    return(i==13 ? OK : ERR);
}
// #pragma argsused

int read_title(char *p) { return 1;}
// #pragma argsused

int read_comment(char *p) {return 1;}
// #pragma argsused

int write_hand(int nDescriptor) {return 1;}
// extern iffheader(int); extern iffout(int,struct info *); extern 
// out_long(int,unsigned long); extern iffappend(int,struct info *); extern 
// iffread(int, int, struct info *);

#define FSEEK_OK 0
int write_matt(char *);
int write_file(char *);
int write_pbn(char *,char *);
int read_file(char *,char *);
int read_pbn(char *,char *);

#ifdef __DLL__
int gen_proc(int command, char *file) {
  int result=0;

  switch(command) {
	 case 1: result = write_matt(file); break;	 /* iff -> gib */
	 case 2: result = write_file(file); break;	 /* iff -> txt */
	 case 3: result = write_pbn(file,(char *)"out.pbn"); break; /* iff -> pbn */
	 case 4: result = read_file(file,(char *)"out.hnd"); break; /* txt -> iff */
	 case 5: result = read_pbn(file,(char *)"out.hnd"); break; /* pbn -> iff */
	 }
  return result;
}
// #define EXE_CALLBACK

#ifdef EXE_CALLBACK
typedef void FAR (CALLBACK * EXECB)(LPSTR);
EXECB call_exe=NULL;
void FAR _export set_gen(EXECB exe_call) {
  call_exe = exe_call;
  }
#endif


#endif

int write_file(char *name) {
#ifndef IOS

	int nInDescriptor, nOutDescriptor;
	static OFSTRUCT WF_Out_Of,WF_In_Of;
	struct info state;
	int i,j;
	size_t len;
	int first_seek_done=0;
	static char szFormat[255];
	char cards[52][4];

	int hand=1;
	if ((nOutDescriptor = MyOpenFile("tempout2",MY_CREATE | MY_RDWR)) == -1) {
		gen_err("can't open output file");
		return 0;
		}
	if ((nInDescriptor = MyOpenFile(name,MY_RDONLY)) == -1) {
		gen_err("can't open input file");
		return 0;
		}

		for (i=0; i< 4; i++) {
			for (j=0; j<13; j++) {
				switch (j) {
					case 0: cards[i*13+j][0]='2'; break;
					case 1: cards[i*13+j][0]='3'; break;
					case 2: cards[i*13+j][0]='4'; break;
					case 3: cards[i*13+j][0]='5'; break;
					case 4: cards[i*13+j][0]='6'; break;
					case 5: cards[i*13+j][0]='7'; break;
					case 6: cards[i*13+j][0]='8'; break;
					case 7: cards[i*13+j][0]='9'; break;
					case 8: cards[i*13+j][0]='T'; break;
					case 9: cards[i*13+j][0]='J'; break;
					case 10:cards[i*13+j][0]='Q'; break;
					case 11:cards[i*13+j][0]='K'; break;
					case 12:cards[i*13+j][0]='A'; break;
					}
				switch(i) {
					case 0: cards[i*13+j][1]='C'; break;
					case 1: cards[i*13+j][1]='D'; break;
					case 2: cards[i*13+j][1]='H'; break;
					case 3: cards[i*13+j][1]='S'; break;
					}
				cards[i*13+j][2]=' ';
				cards[i*13+j][3]='\0';
				}
			}







	while (1) {
		if (!first_seek_done) {
			MySeek(nInDescriptor,0L,SEEK_SET);
			first_seek_done = 1;
			if (iffread(nInDescriptor,hand++,&state) != OK) break;
			}
		else if (iffreadnext(nInDescriptor,&state) != OK) break;

		sprintf(szFormat,sizeof(szFormat),"1 %s\r\n2 %u",state.title,state.vulnerability);
		len = strlen(szFormat);

		for (i=0; i< 4; i++) {
			sprintf(&szFormat[len],sizeof(szFormat)-len,"\r\n%u ",i+3);
			len = (int)strlen(szFormat);
			for (j=0; j<13; j++) {
				strcpy(&szFormat[len],cards[state.deck[i*13+j]]);
				len += 3;
				}
			}
		sprintf(&szFormat[len],sizeof(szFormat)-len,"\r\n9 %u\r\n10 %s\r\n7\r\n",state.dealer,state.pref_leads);
		len = strlen(szFormat);
		MyWrite(nOutDescriptor,(char *)szFormat,(int)len);
	}
	MyClose(nOutDescriptor);
	MyClose(nInDescriptor);
#endif
	return 1;
}

void test_cx(void) {
	// printf("Gen32 -  just a print statement\n");

	// StartLogging("gen32",10,2,1000 * 1000,"%t");
#ifdef CXLOGGING
	LogError("Gen32 logging is really working");
#endif
}

void print_matt_hand(int num, char *name) {
#ifndef IOS
	int nInDescriptor, nOutDescriptor;
	static OFSTRUCT WF_Out_Of,WF_In_Of;
	struct info state;
	size_t len;
	int first_seek_done=0,count=0,done=0;

	static char szFormat[255];
	char file_name[12];

	int hand=1;
	sprintf(file_name,12,"matt%03u.out",num);
	if ((nOutDescriptor = MyOpenFile(file_name,MY_CREATE | MY_RDWR)) == -1) {
		gen_err("can't open output file");
		return;
		}
	if ((nInDescriptor = MyOpenFile(name,MY_RDONLY)) == -1) {
		gen_err("can't open input file");
		return;
		}

	len = 0;
	while (!done) {
		if (!first_seek_done) {
			MySeek(nInDescriptor,0L,SEEK_SET);
			first_seek_done = 1;
			if (iffread(nInDescriptor,hand++,&state) != OK) break;
			}
		else if (iffreadnext(nInDescriptor,&state) != OK) break;

		if (++count == num) {
				done = 1;
	sort(state.deck,0,13,UP);
	sort(state.deck,13,13,UP);
	sort(state.deck,26,13,UP);
	sort(state.deck,39,13,UP);
		get_ms(state.deck,szFormat);
			len = (int) strlen(szFormat);
		strcpy(&szFormat[len],"\n");  len++;

		switch (state.dealer) {
			case 0 : strcpy(&szFormat[len],"n "); break;
			case 1 : strcpy(&szFormat[len],"e "); break;
			case 2 : strcpy(&szFormat[len],"s "); break;
			case 3 : strcpy(&szFormat[len],"w "); break;
			}
			len += 2;

		switch (state.vulnerability) {												 
			case 0 : strcpy(&szFormat[len],"- "); break;
			case 1 : strcpy(&szFormat[len],"n "); break;
			case 2 : strcpy(&szFormat[len],"e "); break;
			case 3 : strcpy(&szFormat[len],"b "); break;
			case 4 : strcpy(&szFormat[len],"b "); break;
			}
		len++;
		strcpy(&szFormat[len],"\n");  len++;
		len = strlen(szFormat);
		MyWrite(nOutDescriptor,(LPSTR)szFormat,(unsigned int)len);
		}
		}
	MyClose(nOutDescriptor);
	MyClose(nInDescriptor);
	return;
#endif
}


int write_matt(char *name) {
#ifndef IOS
	int nInDescriptor, nOutDescriptor;
	static OFSTRUCT WF_Out_Of,WF_In_Of;
	struct info state;
	size_t len;
	int first_seek_done=0;
	static char szFormat[255];

	int hand=1;
	if ((nOutDescriptor = MyOpenFile("matt.out",MY_CREATE | MY_RDWR)) == -1) {
		gen_err("can't open output file");
		return 0;
		}
	if ((nInDescriptor = MyOpenFile(name,MY_RDONLY)) == -1) {
		gen_err("can't open input file");
		return 0;
		}

	len = 0;
	while (1) {
		if (!first_seek_done) {
			MySeek(nInDescriptor,0L,SEEK_SET);
			first_seek_done = 1;
			if (iffread(nInDescriptor,hand++,&state) != OK) break;
			}
		else if (iffreadnext(nInDescriptor,&state) != OK) break;

		get_matt_string2(state.deck,szFormat);
		len = (int) strlen(szFormat);
		strcpy(&szFormat[len],"\n");  len++;

		switch (state.dealer) {
			case 0 : strcpy(&szFormat[len],"n "); break;
			case 1 : strcpy(&szFormat[len],"e "); break;
			case 2 : strcpy(&szFormat[len],"s "); break;
			case 3 : strcpy(&szFormat[len],"w "); break;
			}
		len += 2;

		switch (state.vulnerability) {												 
			case 0 : strcpy(&szFormat[len],"- "); break;
			case 1 : strcpy(&szFormat[len],"n "); break;
			case 2 : strcpy(&szFormat[len],"e "); break;
			case 3 : strcpy(&szFormat[len],"b "); break;
			case 4 : strcpy(&szFormat[len],"b "); break;
			}
		len++;
		strcpy(&szFormat[len],"\n");  len++;
		len = strlen(szFormat);
		MyWrite(nOutDescriptor,(LPSTR)szFormat,(unsigned int)len);
	}
	MyClose(nOutDescriptor);
	MyClose(nInDescriptor);
#endif
	return 1;
}

char out[52];
char *convert_matt_to_binary(char *);
int convert_matt_string(char *,int,char *);

char *convert_matt_to_binary(char *instr) {
	int i,j,val,suit,count;
	char *p, *o;
	// char suits[5] = "CDHS";
#ifndef NO_ASSERTS
    assert(strlen(instr) == 67);
#endif
    o = out;
	for (j=0; j<4; j++) {
	  p = &instr[j * 17];
	  suit = 3;
	  count = 0;

	  for (i=0; i<16; i++) {
		val = 32;
		switch (toupper(*p++)) {
			case 'A': val=12; break;
			case 'K': val=11; break;
			case 'Q': val=10; break;
			case 'J': val=9; break;
			case 'T': val=8; break;
			case '9': val=7; break;
			case '8': val=6; break;
			case '7': val=5; break;
			case '6': val=4; break;
			case '5': val=3; break;
			case '4': val=2; break;
			case '3': val=1; break;
			case '2': val=0; break;
			case '.': suit--; break;
			default: break;
			}
		if (val != 32) {
			*o++ = 32 + suit*13 + val;
			++count;
			}
		}
	}
	return out;
}

int convert_matt_string(char *instr, int hand, char *outstr) {
	int i,suit=3,count=0;
	char *p,*o;
	char suits[5] = "CDHS";

#ifndef NO_ASSERTS
    assert(hand >= 0 && hand < 4);
#endif
    /*	assert(strlen(instr) == 67);	*/
	p = &instr[hand * 17];
	o = outstr;

	for (i=0; i<16; i++)
		switch (toupper(*p)) {
			case 'A':
			case 'K':
			case 'Q':
			case 'J':
			case 'T':
			case '9':
			case '8':
			case '7':
			case '6':
			case '5':
			case '4':
			case '3':
			case '2': *o++ = toupper(*p++); *o++ = suits[suit];
						if (++count<13) *o++ = ' ';
						*o = '\0'; break;
			case '.': suit--; p++; break;
			default : return -1;
			}
	if (count == 13) return 13;
	else return -1;
}


int convert_matt_to_deck(char *instr, int hand, int deck[52]) {
	int i,suit=3,count=0,rank;
	char *p;

#ifndef NO_ASSERTS
    assert(hand >= 0 && hand < 4);
#endif
    /*	assert(strlen(instr) == 67);	*/
	p = &instr[hand * 17];

	for (i=0; i<16; i++) {
		switch (toupper(*p)) {
			case 'A': rank = 12; break;
			case 'K': rank = 11; break;
			case 'Q': rank = 10; break;
			case 'J': rank = 9; break;
			case 'T': rank = 8; break;
			case '9': rank = 7; break;
			case '8': rank = 6; break;
			case '7': rank = 5; break;
			case '6': rank = 4; break;
			case '5': rank = 3; break;
			case '4': rank = 2; break;
			case '3': rank = 1; break;
			case '2': rank = 0; break;
			case '.': suit--; rank = 13; break;
			default : return -1;
			}
		++p;
		if (rank != 13) deck[hand*13 + count++] = suit * 13 + rank;
		}
	if (count == 13) return 13;
	else return -1;
}



int get_matt_string(char *indeck, char *szFormat) {
	int i,deck[52];

	for (i=0; i< 52; i++)
		deck[i] = (int) ((int)indeck[i] - 32);
	get_matt_string2(deck,szFormat);
	return 1;
	}

int write_pbn(char *name, char *outname) {
#ifndef IOS
	int nInDescriptor, nOutDescriptor;
	static OFSTRUCT WF_Out_Of,WF_In_Of;
	struct info state;
	int i,j;
	size_t len;
	int first_seek_done=0;
	static char szFormat[255];
	char OutFile[255];
	char cards[13][2],flags[3];

	int hand=1;
	if (strlen(outname) == 0) strcpy(OutFile,"out.pbn");
	else strcpy(OutFile,outname);
	if ((nOutDescriptor = MyOpenFile(OutFile,MY_CREATE | MY_RDWR)) == -1) {
		gen_err("can't open output file");
		return 0;
		}
	if ((nInDescriptor = MyOpenFile(name,MY_RDONLY)) == -1) {
		gen_err("can't open input file");
		return 0;
		}


		for (j=0; j<13; j++) {
			switch (j) {
					case 0: cards[j][0]='2'; break;
					case 1: cards[j][0]='3'; break;
					case 2: cards[j][0]='4'; break;
					case 3: cards[j][0]='5'; break;
					case 4: cards[j][0]='6'; break;
					case 5: cards[j][0]='7'; break;
					case 6: cards[j][0]='8'; break;
					case 7: cards[j][0]='9'; break;
					case 8: cards[j][0]='T'; break;
					case 9: cards[j][0]='J'; break;
					case 10:cards[j][0]='Q'; break;
					case 11:cards[j][0]='K'; break;
					case 12:cards[j][0]='A'; break;
					}
				cards[j][1] = '\0';
				}

	while (1) {
		if (!first_seek_done) {
			MySeek(nInDescriptor,0L,SEEK_SET);
			first_seek_done = 1;
			if (iffread(nInDescriptor,1,&state) != OK) break;
			}
		else if (iffreadnext(nInDescriptor,&state) != OK) break;

		len = 0;
		if (hand != 1) sprintf(szFormat,254,"\n[Board \"%u\"]\n",hand++);
		else sprintf(szFormat,254,"[Board \"%u\"]\r\n",hand++);
//		strcpy(&szFormat[0],"[Board \"%u\"]\r\n",hand++);
		len = (int) strlen(szFormat);

		switch (state.dealer) {
			case 0 : strcpy(&szFormat[len],"[Dealer \"N\"]\r\n"); break;
			case 1 : strcpy(&szFormat[len],"[Dealer \"E\"]\r\n"); break;
			case 2 : strcpy(&szFormat[len],"[Dealer \"S\"]\r\n"); break;
			case 3 : strcpy(&szFormat[len],"[Dealer \"W\"]\r\n"); break;
			}
		len += 13;

		switch (state.vulnerability) {
			case 0 : strcpy(&szFormat[len],"[Vulnerable \"None\"]\r\n"); len += 2; break;
			case 1 : strcpy(&szFormat[len],"[Vulnerable \"NS\"]\r\n"); break;
			case 2 : strcpy(&szFormat[len],"[Vulnerable \"EW\"]\r\n"); break;
			case 3 : strcpy(&szFormat[len],"[Vulnerable \"Both\"]\r\n"); len += 2; break;
			case 4 : strcpy(&szFormat[len],"[Vulnerable \"Both\"]\r\n"); len += 2; break;
			}
		len += 18;

		strcpy(&szFormat[len],"[Deal \"N:");
		len += 9;

		sort_the_hands(state.deck);
		for (i=0; i< 4; i++) {
			for (j=0; j<3; j++) flags[j]=OFF;
			for (j=12; j>=0; j--) {
				if (flags[0] == OFF && (state.deck[i*13+j] /13) < 3) {
					strcpy(&szFormat[len],"."); len++; flags[0]=ON;
					}
				if (flags[1] == OFF && (state.deck[i*13+j] /13) < 2) {
					strcpy(&szFormat[len],"."); len++; flags[1]=ON;
					}
				if (flags[2] == OFF && (state.deck[i*13+j] /13) < 1) {
					strcpy(&szFormat[len],"."); len++; flags[2]=ON;
					}
				strcpy(&szFormat[len],cards[state.deck[i*13+j]%13]);
				len++;
				}
			if (flags[1] == OFF) {
				strcpy(&szFormat[len],".");
				len++;
				}
			if (flags[2] == OFF) {
				strcpy(&szFormat[len],".");
				len++;
				}
			if (i<3) { strcpy(&szFormat[len]," "); len++; }
			}
		strcpy(&szFormat[len],"\"]\r\n\r\n");  len += 3;
		len = strlen(szFormat);
		MyWrite(nOutDescriptor,(LPSTR)szFormat,(int)len);
	}
	MyClose(nOutDescriptor);
	MyClose(nInDescriptor);
#endif
	return 1;
}


char pbn[200];
char *get_pbn(int deck[52]) {
	char cards[13][2],flags[3];
    int len=0;
		
    for (int j=0; j<13; j++) {
			switch (j) {
					case 0: cards[j][0]='2'; break;
					case 1: cards[j][0]='3'; break;
					case 2: cards[j][0]='4'; break;
					case 3: cards[j][0]='5'; break;
					case 4: cards[j][0]='6'; break;
					case 5: cards[j][0]='7'; break;
					case 6: cards[j][0]='8'; break;
					case 7: cards[j][0]='9'; break;
					case 8: cards[j][0]='T'; break;
					case 9: cards[j][0]='J'; break;
					case 10:cards[j][0]='Q'; break;
					case 11:cards[j][0]='K'; break;
					case 12:cards[j][0]='A'; break;
					}
				cards[j][1] = '\0';
				}
		
    sort_the_hands(deck);
		for (int i=0; i< 4; i++) {
			for (int j=0; j<3; j++) flags[j]=OFF;
			for (int j=12; j>=0; j--) {
				if (flags[0] == OFF && (deck[i*13+j] /13) < 3) {
					strcpy(&pbn[len],"."); len++; flags[0]=ON;
					}
				if (flags[1] == OFF && (deck[i*13+j] /13) < 2) {
					strcpy(&pbn[len],"."); len++; flags[1]=ON;
					}
				if (flags[2] == OFF && (deck[i*13+j] /13) < 1) {
					strcpy(&pbn[len],"."); len++; flags[2]=ON;
					}
				strcpy(&pbn[len],cards[deck[i*13+j]%13]);
				len++;
				}
			if (flags[1] == OFF) {
				strcpy(&pbn[len],".");
				len++;
				}
			if (flags[2] == OFF) {
				strcpy(&pbn[len],".");
				len++;
				}
			if (i<3) { strcpy(&pbn[len]," "); len++; }
			}
        return pbn;
}

int read_file(char *name, char *out) {
#ifndef IOS
	int nInDescriptor, nOutDescriptor;
	static OFSTRUCT WF_In_Of, WF_Out_Of; 
	char *p;
	char  buffer[256],outfile[60];
	int i, j;
	static int deck[52];
	struct info state;
	int initialized=0,val=OK;
	unsigned long len,length;

	if (strlen(out)==0) strcpy(outfile,"tempout");
	else strcpy(outfile,out);

	ZeroMemory(&state,sizeof(state));

	if ((nInDescriptor = MyOpenFile(name,MY_RDONLY)) == -1) {
		gen_err("can't open input file");
		return 0;
		}
	if ((nOutDescriptor = MyOpenFile(outfile,MY_CREATE | MY_RDWR)) == MY_IO_ERROR) {
		gen_err("can't open output file");
		return 0;
		}
	if (iffheader(nOutDescriptor) != 12) { MyClose(nOutDescriptor); return 0; }

	while (val==OK) {
		i=0;
		buffer[i]='\0';
		while (1) {
			j =  MyRead(nInDescriptor,&buffer[i],1);
			if (j == 0 && i == 0) goto done;
			if (buffer[i] == '\r') ;
			else if (buffer[i] == '\n') {
				buffer[i] = '\0';
				break;
				}
			else if (buffer[i] == '\0') break;
			i++;
			}
		p= strtok(buffer," ");
		if ((i= atoi(p)) !=0) {
			p= strtok(NULL,"\r\n ");
			switch(i) {
				case 1: p= strtok(p,"\n");
					strcpy_s(state.title,80,p);
					break;
				case 2: j = atoi(p);
					switch(j) {
						case 0: case 1: case 2: case 3: state.vulnerability = j; break;
						case 4: state.vulnerability = 3; break;
						}
					break;
				case 3: val=read_hand(p,&deck[0]); break;
				case 4: val=read_hand(p,&deck[13]); break;
				case 5: val=read_hand(p,&deck[26]); break;
				case 6: val=read_hand(p,&deck[39]); break;
				case 7:
					for (j=0; j<52; j++) state.deck[j]=deck[j];
#ifndef USE_BERGEN
					for (j=0; j<4; j++) get_description(state.deck,j,state.statistics[j]);
#endif
					if (!initialized) {
						len = iffout(nOutDescriptor,&state);
						if (len < 8) MyClose(nOutDescriptor);
						else if (MySeek(nOutDescriptor,4L,SEEK_SET) != 4) MyClose(nOutDescriptor);
						else if (out_long(nOutDescriptor,len) < 0) MyClose(nOutDescriptor);
						else initialized=1;
						if (initialized == 0) return 0;
						}
					else if ((length=iffappend(nOutDescriptor,&state)) == ERR)
						{MyClose(nOutDescriptor); return 0; }
					break;
				case 8: p[16]=' ';
						for (j=0; j<4; j++)
							convert_matt_to_deck(p,j,&deck[0]);
						break;
				case 9: j = atoi(p);
					switch (j) {
						case 1: case 2: case 3: case 0: state.dealer = j; break;
						}
					break;
				case 10: p= strtok(p,"\n");
						 j=(int) strlen(p);
						 j = j<40 ? j : 39;
						 strcpy_s(state.pref_leads,j,p);
						 state.pref_leads[j]='\0';
						 break;
				case 11: p= strtok(p,"\n");
						 strcpy_s(state.pref_suits,80,p);
						 break;
				case 12: state.hand_num = atoi(p); break;
				case 13: p = strtok(p,"\n"); strcpy_s(state.contract,6,p); break;
				case 14: state.declarer = atoi(p); break;
				case 15: state.result = atoi(p); break;
				case 16: state.score = atoi(p); break;
				case 17: p=strtok(p,"\n"); strcpy_s(state.auction,255,p); break;
				case 18: p=strtok(p,"\n"); strcpy_s(state.play,255,p); break;
				case 20: p=strtok(p,"\n"); strcpy_s(state.north,60,p); break;
				case 21: p=strtok(p,"\n"); strcpy_s(state.east,60,p); break;
				case 22: p=strtok(p,"\n"); strcpy_s(state.south,60,p); break;
				case 23: p=strtok(p,"\n"); strcpy_s(state.west,60,p); break;
				case 24: p=strtok(p,"\n"); strcpy_s(state.event,255,p); break;
				case 25: p=strtok(p,"\n"); strcpy_s(state.date,11,p); break;
				case 26: p=strtok(p,"\n"); strcpy_s(state.site,255,p); break;
				case 27: p=strtok(p,"\n"); strcpy_s(state.home_team,60,p); break;
				case 28: p=strtok(p,"\n"); strcpy_s(state.visit_team,60,p); break;
				case 29: p=strtok(p,"\n"); strcpy_s(state.room,60,p); break;
				case 30: p=strtok(p,"\n"); strcpy_s(state.stage,24,p); break;
				case 31: state.round = atoi(p); break;
				default: break;
				}
			}
		}
done:
	assert(val==OK);
	MySeek(nOutDescriptor,4L,SEEK_SET);
	out_long(nOutDescriptor,length);
	MyClose(nOutDescriptor);
#endif
	return 1;
}


#ifdef READ_PBN_IN_IO3
int read_pbn(char *name, char *out) {
#ifndef IOS

	int nInDescriptor, nOutDescriptor;
	static OFSTRUCT WF_In_Of, WF_Out_Of; 
	char *p,c;
	static char  buffer[256];
	char outfile[255];
	int i, j;
	static int deck[52];
	struct info state;
	int initialized=0,val=OK;
	unsigned long len,length;

	if (strlen(out)==0) strcpy(outfile,"tempout");
	else strcpy(outfile,out);


	if ((nInDescriptor = MyOpenFile(name,MY_RDONLY)) == -1) {
		gen_err("can't open input file");
		return 0;
		}
	if ((nOutDescriptor = MyOpenFile(outfile,MY_CREATE)) == -1) {
		gen_err("can't open output file");
		return 0;
		}
	else if (iffheader(nOutDescriptor) != 12) { MyClose(nOutDescriptor); return 0; }
	while (val==OK) {
		i=0;
		buffer[i]='\0';
		while (1) {
			j =  MyRead(nInDescriptor,&buffer[i],1);
			if (j == 0 && i == 0) goto done;
			if (buffer[i] == '\r') ;
			else if (buffer[i] == '\n') {
				buffer[i] = '\0';
				break;
				}
			else if (buffer[i] == '\0') break;
			i++;
			}
		if ((c=toupper(buffer[1])) == 'D' && toupper(buffer[6])=='R') /* dealer */
			switch (toupper(buffer[9])) {
				case 'N' : state.dealer = 0; break;
				case 'E' : state.dealer = 1; break;
				case 'S' : state.dealer = 2; break;
				case 'W' : state.dealer = 3; break;
				}
		else if (c=='D' && toupper(buffer[4])=='L') /* deal */
			;
		else if (c=='B' && toupper(buffer[5])=='D') /* board */
			;
		else if (c=='V') /* vulnerable */
			{
			if ((c=toupper(buffer[13])) == 'E') state.vulnerability = 2;
			else if (c=='B') state.vulnerability = 3;
			else if (toupper(buffer[14])=='S') state.vulnerability = 1;
			else state.vulnerability = 0;
			}
			switch(i) {
				case 1: p= strtok(p,"\n"); strcpy_s(state.title,80,p); break;
				case 2: j = atoi(p);
					switch(j) {
						case 0: case 1: case 2: case 3: state.vulnerability = j; break;
						case 4: state.vulnerability = 3; break;
						}
					break;
				case 3: val=read_hand(p,&deck[0]); break;
				case 4: val=read_hand(p,&deck[13]); break;
				case 5: val=read_hand(p,&deck[26]); break;
				case 6: val=read_hand(p,&deck[39]); break;
				case 7:
					for (j=0; j<52; j++) state.deck[j]=deck[j];
#ifndef USE_BERGEN
					for (j=0; j<4; j++) get_description(state.deck,j,state.statistics[j]);
#endif
					if (!initialized) {
						len = iffout(nOutDescriptor,&state);
						if (len < 8) MyClose(nOutDescriptor);
						else if (MySeek(nOutDescriptor,4L,SEEK_SET) != 4) MyClose(nOutDescriptor);
						else if (out_long(nOutDescriptor,len) < 0) MyClose(nOutDescriptor);
						else initialized=1;
						if (initialized == 0) return 0;
						}
					else {
						if ((length=iffappend(nOutDescriptor,&state)) == ERR) {MyClose(nOutDescriptor); return 0; }
					} break;
				case 8: break;
				case 9: j = atoi(p);
					switch (j) {
						case 1: case 2: case 3: case 0: state.dealer = j; break;
						}
					break;
				case 10: p= strtok(p,"\n");
						 j=(int)strlen(p);
						 j = j<40 ? j : 39;
						 strcpy_s(state.pref_leads,j,p);
						 state.pref_leads[j]='\0';
						 break;
				case 11: p= strtok(p,"\n");
						 strcpy_s(state.pref_suits,80,p);
						 break;
				default: break;
				}
		}
done:
	assert(val==OK);
	MySeek(nOutDescriptor,4L,SEEK_SET);
	out_long(nOutDescriptor,length);
	MyClose(nOutDescriptor);
#endif
	return 1;
}

#endif


int gethand(int deck[52],int start)
{
#ifndef IOS
	int bytes_read,i,j,k,temp[52];
	static char mybuf[54];

	if (start==0) {
		if ((bytes_read = MyRead(infp,mybuf,53)) == 0)
			return(EOF);
		}
	for (i=0; i<52; i++) temp[i]=0;
	for (i=0; i < 13; i++) {
		j=mybuf[start+i] - 32;
		if (j<0 || j>51) {
			gen_err("bad value in gethand");
			return(ERR);
			}
		if (temp[j] == 1) {
			gen_err("duplicate card in gethand");
			return(ERR);
			}
		else temp[j]=1;
		}
	k=0;							/* index to number added to deck */
	for (j=0; j<52; j++) {			/* find the cards read in		*/
		if (temp[j]==0) ;
		else	{
		   for (i=start + k ; i<52; i++) { /* find in deck	*/
			  if (deck[i] == j) {	  /* switch */
				deck[i]= deck[start+k];
				deck[start+k]=j;
				k++;
				break;
				}
			  }
			}
		 }
	if (errorcheck(deck) == ERR) {
		gen_err("errorcheck problem in gethand");
		return(ERR);
		}
	if (k != 13) {
		gen_err("k != 13 in gethand");
		return(ERR);
		}
	return(13+start);
#else
    return(0);
#endif
}


void print_bids(char *str, int dealer)
{
	char *s;
	int i,j,x,y;

	s=str; x=0; y=1;

	for (i=0; i<4; i++)
		switch((dealer + i) % 4) {
			case 0: bids[0][i*5+2]='N'; break;
			case 1: bids[0][i*5+2]='E'; break;
			case 2: bids[0][i*5+2]='S'; break;
			case 3: bids[0][i*5+2]='W'; break;
			}
	bids[0][19]='\n'; bids[0][20]='\0';

	j=0;
	while (*s) {
		j=0;
		while (*s != ':' && *s != '\0') {
			bids[y][x*5 + 1 + j++] = *s;
			++s;
			}
		if (++x > 3) {
			bids[y][x*5+1+j++]='\n';
			bids[y][x*5+1+j++]='\0';
			j=0; x=0; y++;
			}
		if (*s == ':') ++s;
		else if (*s == '\0' && x < 4) {
			bids[y][x*5+1+j++] = '\n';
			bids[y][x*5+1+j]='\0';
			}
		}
	if (x!=0) ++y;
	bids[y][0]='\n'; bids[y][1]='\0';
	bids[++y][0]='\0';
}



char szFormat[255];
#ifdef DYNAMIC_CALL_EXE
extern void (FAR *lpfnCDoEx)(LPSTR);
#endif

void output(char *fmt,...)
{
//	size_t len;
//	char szBuf[80];
	va_list argptr;

	va_start(argptr,fmt);
	if (outfile) {
#ifndef USE_IO3
#pragma message("Using MyWrite")
		vsprintf(szFormat,fmt,argptr);
		len = strlen(szFormat);
		MyWrite(outfp,(char *) szFormat,(unsigned int)len);
#endif
	}
#ifndef GEN32
	else if (log_flag) {
		char temp[255];
//		lstrcpy(szFormat,"LOG2 ");
		szFormat[0] = 'L'; szFormat[1] = 'O';
		szFormat[2] = 'G'; szFormat[3] = '2';
		szFormat[4] = ' '; szFormat[5] = '\0';
		vsprintf(temp,fmt,argptr);
//#if defined(LINUX)
		strcat(szFormat, temp);
//#else
//		lstrcat(szFormat,temp);
//#endif

#ifdef MBCALL
#ifndef MB_BAD
/* This was the source of a bug in version 1.2 wherein if the
hand librarian was started before Meadowlark Bridge, the
c_do_execute function, was loading the mb.dll before the
mb.exe could do so, and caused an error. Now the function
c_do_execute is called by dynamically loading mb.dll and calling the
function. The function is only called by the print hand routine
and so is not used by the hand librarian at all, just a call from
the print routine to io3.c for printing out the hand and then
this is sent via c_do_execute to the mb.exe, all rather complicated
but here because these are dll files and dde was not implemented for
interprocess communication, rather a direct SendMessage to the main
mb.exe window from the mb.dll. The window was sent to the dll from
mb.exe when it started up, thus the problem of starting the hand
librarian with automatic loading of mb.dll before the mb.exe was
started whew!!! This is all fixed in version 1.31 date 8.16.97 RAL */

	   hinstH=LoadLibrary("MB.DLL");
	   if ((UINT)hinstH > 32) {
		(FARPROC) lpfnCDoEx = GetProcAddress(hinstH,"_c_do_execute");
		if (lpfnCDoEx == NULL) {
			MessageBox(GetFocus(),"lpfnCDoEx function not found","GEN.DLL",MB_ICONHAND);
			FreeLibrary(hinstH);
			}
				else (*lpfnCDoEx)(szFormat);
		}
	   else MessageBox(GetFocus(),"LoadLibrary failed for mb.dll","GEN.DLL",MB_ICONHAND);


//#ifdef DYNAMIC_CALL_EXE
//		{
//		if (lpfnCDoEx) (*lpfnCDoEx)(szFormat);
//		}
//#else
//		{
//		if (external_print_function)
//		  (*external_print_function)(szFormat);
//					}
//#endif
#else
		c_do_execute(szFormat);   //   Simple call was here for Version 1.2
#endif
#endif
		}
#endif
	else {
		vsprintf(szFormat,fmt,argptr);
		// len = strlen(szFormat);
        printf("%s", szFormat);
		}
	va_end(argptr);
}
#endif
#endif
