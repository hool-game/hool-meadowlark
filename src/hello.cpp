#include "meadowlark.h"
#include "hello.h"
#include <iostream>
#include <fstream>
#include <string>
// #include <windows.h>
#include <stdlib.h>
char *hool_test(int,int,int,int,int);
char *constraint_command(int,int,int,int,int);
extern "C" int gen32_do_log(int,int,const char *,...);
int set_offsets(int);
void restricted_shuffle(void);
void deck_copy_all(void);
void deck_copy_some(int);
void deck_copy_to(unsigned int[52]);
void set_active(int);
char *get_dealt_hands(int,int);
#if !defined(linux)
#include "c:\tools\dds_2.9.0\include\dll.h"
#include "c:\tools\dds_2.9.0\examples\hands.h"
#else
#include "dll.h"
#include "hands.h"
#endif
#define UNSIGNED unsigned int
// Each played makes two shows; this is for input to the library about the other players shows.
#define CHECK1_HCPS 64
#define CHECK1_LEN 65
#define CHECK1_DIST 66
#define CHECK2_HCPS 67
#define CHECK2_LEN 68
#define CHECK2_DIST 69
#define GET_SHOW_1 70
#define GET_SHOW_2 71
#define GET_BID1 72
#define CHECK_BID 75
#define RESET 99
#define CHECK_PLAY 76
#define CHECK_CARD 73
#define GET_PLAY 74
#define GET_VERSION 90
#define CHECK_CONTRACT 86
#define CHECK_CLAIM 101
#define CHECK_SYSTEM 102
#define CLUBS 0
#define DIAMONDS 1
#define HEARTS 2
#define SPADES 3
#define DISTRIBUTION 4
#define HCPS 5
#define NORTH 0
#define EAST 1
#define SOUTH 2
#define WEST 3
#define ID_GET_PLAYING_HAND 92
#define ID_PLAYED 37
#define ID_VOID 24
#define ID_RESET 99
#define ID_GET_HAND 97
#define ID_PRINT_HANDS 94
#define ID_DIFF 7
#define ID_LEN 1
#define ID_HCPS 0
#define ID_DIST 2
#define ID_AND 3
#define LOOP_COUNT 10


using namespace std;
unsigned int who_am_i=0;
unsigned int deck[52];
unsigned int leader[13];
namespace globals {
    unsigned int state=0;
    unsigned int seat=0, contract=0, declarer=0, doubled=0;
    unsigned int verbose=255;
    unsigned int trump;
    unsigned int systems[2] = {0,0};
}
namespace plays {
    unsigned int round, seat;
    unsigned int who_played[13][4];
    unsigned int card_played[13][4];
    unsigned int leader[13];
    }

namespace constraints {
    unsigned int count = 0; // global index into constraints
    bool showed_void[4][4]; // who,suit
    }

namespace recorder {
    unsigned int fence[100];
    unsigned int num_of_cards[4] = {0,0,0,0};
    }

string suits[4]= {"C","D","H","S"};
unsigned int showed[4][2][5];

void deck_copy_to_deck(void) {
    deck_copy_to(deck);
    }
int do_make(int,int,int,int,int);
int do_set(int,int,int);
int score(int contract, int tricks, int vulnerability, int doubled) {
    int level, set, denom, val;
    level = contract / 10;
    if (tricks < level + 6) set = 1; else set = 0;
    if (contract % 5 == 0 || contract % 5 == 1) denom = 0 ;
    else if (contract % 5 == 2|| contract % 5 == 3) denom = 1;
    else denom = 2;
    if (set == 1) val = do_set(level + 6 - tricks, vulnerability, doubled);
    else val = do_make(denom, tricks-6, doubled, level, vulnerability);
    return val;
    }

int do_set(int diff, int vulnerability, int doubled) {
    int val;
    int base[3] = {50,100,200};
    int additional[6] = {50,200,400,100,300,600};
    int bonus[3] = {0,100,200};

    val = base[doubled] * (vulnerability+1);
    if (diff > 1) val += (diff - 1) * additional[doubled + vulnerability * 3];
    if (vulnerability == 0 && diff > 3) val += bonus[doubled]*(diff-3);
    return -val;
    }

int do_make(int denom, int tricks, int doubled, int level, int vulnerability) {
    int val, below, num_of_overtricks, trick_val, non_overtricks;
    int base[3] = {20,30,40};
    int additional[3] = {20,30,30};
    int game[2] = {300,500};
    int slams[4] = {50,750,1000,1500};
    int overtricks[4] = { 100,200,200,400};

    // below the line
    val = base[denom];
    if (tricks > 1) {
        non_overtricks = tricks;
        if (non_overtricks > level) non_overtricks = level;
        val += additional[denom] * (non_overtricks-1);
        }

    if (doubled > 0) val *= doubled * 2;
    below = val;

    // above the line
    if (tricks > level) {
        num_of_overtricks = tricks - level;
        if (doubled > 0) trick_val = overtricks[vulnerability + (doubled-1)*2];
        else trick_val = additional[denom];
        val += trick_val * num_of_overtricks;
        }

    if (below >= 100) val += game[vulnerability]; // game bonus
    else val += 50;                                  // part score bonus

    if (level > 5) val += slams[(level-6) * 2 + vulnerability]; // slam bonus
    if (doubled > 0) val += 50 * doubled; // insult
    return val;
    }



int int1(char *in) { return *in - '0'; }
int int2(char *in) { return (*in - '0') * 10 + *(in+1)-'0'; }

void longest_fit(char *d1, char *d2, int given, int *suit, int *longer, int *total_cards) {
    // find longest fit; but if given is a suit (0-3) rather than 4, return values for that suit
     int len1, len2;
     *total_cards = 0;
    for (int _suit =0; _suit < 4; _suit++) { int _total_cards;
        if (given != 4 && _suit != given) continue;
        len1 = int1(&d1[15 + _suit]);
        len2 = int1(&d2[15 + _suit]);
        _total_cards = len1 + len2;
        if (_total_cards > *total_cards) {
            if (len1 >= len2) *longer=0;
            else *longer = 1;
            *total_cards = _total_cards;
            *suit = _suit;
            }
        }
    }

int calculate_number_of_tricks(int points) {
    if (points > 36) return 13;
    else if (points > 32) return 12;
    else if (points > 28) return 11;
    else if (points > 25) return 10;
    else if (points > 23) return 9;
    else if (points > 21) return 8;
    else if (points > 19) return 7;
    else if (points > 17) return 6;
    else if (points > 15) return 5;
    else if (points > 12) return 4;
    else if (points > 10) return 3;
    else if (points > 8) return 2;
    else if (points > 3) return 1;
    else return 0;
    }

#define DUMMY 1 
#define DECLARER 0

int figure_points(char *d0, char *d1, int suit, int longer) {
    char *declarer, *dummy;
    int declarer_length, dummy_length, declarer_points_with_support, declarer_points, dummy_points=0,total_points;
    int adjust1, adjust2;

    int length_position_start = 15; // position in description
    if (longer==0) { declarer = d0; dummy = d1; }
    else { declarer = d1; dummy = d0; }
    declarer_length = int1(&declarer[length_position_start+suit]);
    dummy_length =  int1(&dummy[length_position_start+suit]);

    // if equal length make a correction here that the hand with ruffable shortness becomes dummy
    if (declarer_length == dummy_length) {
        int short_suit_length=10, short_suited_hand=2, dummy_suit_length,declarer_suit_length;
        for (int _i=0; _i < 4; _i++) {
            if (_i==suit) continue; // looking for shortness in a side suit
            dummy_suit_length = int1(&d1[length_position_start+suit]);
            declarer_suit_length = int1(&d0[length_position_start+suit]);
            if (dummy_suit_length < short_suit_length) {
                short_suit_length = dummy_suit_length;
                short_suited_hand = DUMMY;
                }
            if (declarer_suit_length < short_suit_length) {
                short_suit_length = declarer_suit_length;
                short_suited_hand = DECLARER;
                }
            }
        if (short_suited_hand == 2) ; // invalid hand means it was not set above so there is no short-suited-hand
        else if (short_suit_length > 2) ;
        else if (short_suited_hand == DUMMY && dummy==d1) ;
        else if (short_suited_hand == DECLARER && dummy == d0) ;
        else if (dummy==d1) {
            declarer = d1; dummy=d0;
            declarer_length = int1(&d1[length_position_start+suit]);
            dummy_length = int1(&d0[length_position_start+suit]);
            }
        else {
            declarer = d0; dummy=d1;
            declarer_length = int1(&d0[length_position_start+suit]);
            dummy_length = int1(&d1[length_position_start+suit]);
            }
        }

    declarer_points_with_support = int2(&declarer[41]);

    if (dummy_length < 2) {
        declarer_points = int2(&declarer[39]);
        adjust1 = declarer_points_with_support - declarer_points;
        }
    else if (dummy_length == 2) {
        if (declarer_length < 6) {
            declarer_points = int2(&declarer[39]);
            adjust1 = declarer_points_with_support - declarer_points;
            }
        else if (declarer_length == 6) { declarer_points = declarer_points_with_support - 2;  adjust1 = 2; }
        else { declarer_points = declarer_points_with_support; adjust1 = 0; }
        }
    else { declarer_points = declarer_points_with_support; adjust1 = 0; }

    switch (suit) {
        case 0: dummy_points = int2(&dummy[43]); break;
        case 1: dummy_points = int2(&dummy[46]); break;
        case 2: dummy_points = int2(&dummy[49]); break;
        case 3: dummy_points = int2(&dummy[52]); break;
        }

    adjust2 = adjust1; // I put this here just to get rid of an unused adjust1 warning 220516
    adjust2 = 0;
// 220513 Ensure that bad fits loose proper number of points @hool +points rip:todo
    if (declarer_length + dummy_length < 8) {adjust2 = 2; dummy_points -= adjust2; }
    else if (declarer_length + dummy_length > 9) { adjust2 = -2; dummy_points -= adjust2; }
    if (declarer_length + dummy_length < 7) dummy_points = 0; // added 220513
    total_points = declarer_points + dummy_points;
    if (total_points > 40) total_points = 40;

// 220513 Ensure that quick losers do not bomb slam @hool +losers rip:todo
    { 
        if (total_points > 36) {}
        else if (total_points > 32) {}
        }

    return total_points;
    }



int get_length(unsigned int who, unsigned int suit) {
    int length=0;
	for (unsigned int i=who*13; i<who*13+13; i++) {
	 	if (deck[i]==53) continue;
        if (deck[i]/13 != suit) continue;
        length += 1;
	   	}
    return length;
    }

void set_longest(unsigned int who, unsigned int *suit, unsigned int *length) {
    unsigned int _length;
    unsigned int _longest = 0;
    for (unsigned int i=0; i<4; i++) {
        _length = get_length(who,i);
        if (_length > _longest) {
            _longest = _length;
            *length = _longest;
            *suit = i;
            }
        }
    }

void set_shortest(unsigned int who, unsigned int *suit, unsigned int *length) {
    unsigned int _length;
    unsigned int _shortest = 13;
    for (unsigned int i=0; i<4; i++) {
        _length = get_length(who,i);
        if (_length < _shortest) {
            _shortest = _length;
            *length = _shortest;
            *suit = i;
            }
        }
    }

void set_three_suited(unsigned int who, unsigned int *three_suited, unsigned int *short_suit) {
    unsigned int _length;
    unsigned int _three_suited_count = 0;
    unsigned int _shortest = 13;
    for (unsigned int i=0; i<4; i++) {
        _length = get_length(who,i);
        if (_length > 3) _three_suited_count++;
        if (_length < _shortest) {
            _shortest = _length;
            *short_suit = i;
            }
        }
    if (_three_suited_count == 3) *three_suited = 1;
    else *three_suited = 0;
    }

const string get_distribution_string(int who) {
		int counts[4] = {0,0,0,0};
		string dist(""); 
		int i,j,temp;
        for (int suit=0; suit<4; suit++) { counts[suit] = get_length(who,suit); }
		for (i=0; i<3; i++) {				  // sort
			for (j = i + 1; j < 4; j++) {
				if (counts[i] > counts[j]) {
					temp = counts[j];
					counts[j] = counts[i];
					counts[i] = temp;
					}
				}
			}
		for (i=3; i>=0; i--) {				  // create string (commas required as count can be >9)
			dist += to_string(counts[i]);
			if (i!=0) dist += ",";
			}
		return dist;
	}	

int get_hcps(unsigned int who) {
	unsigned int i, hcps = 0, sub;
	int val;
	for (i = who * 13; i < who * 13 + 13; i++) {
		if (deck[i] == 53) continue;
		sub = (deck[i] / 13) * 13;
		val = deck[i] - sub - 8;
		if (val > 0) hcps += (unsigned) val;
		}
	return hcps;
    }

const string get_hcps_string(unsigned int who) { return(to_string(get_hcps(who))); }
char *get_dealt_hands(int hand_num, int exclude_played);
char *gen32_get_description2(int,int);

const string get_bid(UNSIGNED who, UNSIGNED current_bid, UNSIGNED doubled) {
    // int fits[4] = {0,0,0,0};
    int check_suit,check_longer,check_length;
    int loop_values[5][LOOP_COUNT];
    unsigned int generate_parameter=0;
    char my_d0[110],my_d1[110];

    // int verbose = 31;

    int offsets[4] = { 14,13,11,7 };
    set_offsets(offsets[who]);                        // set other three players constraints for hand generation


    cout << "get_bid entered for " << who << ";";
    if (current_bid && doubled) cout << " my bid after being doubled of " << current_bid << endl;
    else if (current_bid) cout << " against opponents bid of " << current_bid << endl;
    else cout << endl;
    switch (who) {
        case 0: generate_parameter = 1; break;
        case 1: generate_parameter = 2; break;
        case 2: generate_parameter = 4; break;
        case 3: generate_parameter = 8; break;
        }
    hool_test(0,98,generate_parameter,0,LOOP_COUNT);   // generate the hands, excluding who.
    set_active(0);
    cout << hool_test(0, ID_PRINT_HANDS,1,0,LOOP_COUNT) << endl; // forced printing by setting where to 1
    cout << "So, at this point restricted shuffling is proven to work" << endl;
    cout << "calling hool_test to print constraints" << endl;
    for (int _loop=0; _loop < LOOP_COUNT; _loop++) {
        char *des;
        des = get_dealt_hands(_loop,0); // transfer hand to gen32_hand; required by gen32_get_description2
        cout << "dealt hands:" << des << endl;
        strcpy(my_d0,gen32_get_description2(who,0));
        strcpy(my_d1,gen32_get_description2((who+2)%4,0));
        cout << "who :" << who << " " << my_d0 << endl;
        cout << "pard:" << (who+2)%4 << " " << my_d1 << endl;
        // for (int _index=0; _index < 4; _index++) fits[_index]=0;

        for (int _suit=0; _suit < 4; _suit++) {
            longest_fit(my_d0,my_d1,_suit,&check_suit,&check_longer,&check_length);  // note third parameter is specific, just get its value, not the best
            // fits[_suit] = check_length;
            int calculated_points = figure_points(my_d0, my_d1, _suit, check_longer);
            int calculated_tricks = calculate_number_of_tricks(calculated_points);
            loop_values[_suit][_loop] =  calculated_tricks;
            }

        {   // for nt: get hcps from descriptions and calculate number of tricks won from that and store
            int total_hcps = (my_d0[1]-'0') * 10 + my_d0[2] - '0' + (my_d1[1]-'0') * 10 + my_d1[2]-'0';
            loop_values[4][_loop] = calculate_number_of_tricks(total_hcps);
            }


        }


// now we can finally figure a bid
        signed int total_score[7][5];
        unsigned int contract;
        int _vulnerability=0, _doubled=0, penalty_double = 0;
        int best_total_score = -10000, best_level = 0, best_denom=0, best_bid=0;
        signed int opponents_score=0,my_score=0;
        unsigned int bid_state = 0;
        for (int _level = 0; _level < 7; _level++) 
            for (int _denom = 0; _denom < 5; _denom++) {
                total_score[_level][_denom] = 0;
                contract = 10 * (_level + 1) + _denom;
                if (current_bid == contract && !doubled && bid_state < 2) { 
                    unsigned int opponents_level=current_bid / 10-1, opponents_denom=current_bid % 10;
                    contract = current_bid;
                    for (bid_state = 0; bid_state < 2; bid_state++) {
                        for (unsigned int _loop_index = 0; _loop_index < LOOP_COUNT; ++_loop_index) { // total up all hands
                            if (bid_state == 0) {
                                best_bid = 0; // pass
                                total_score[opponents_level][opponents_denom] -= 
                                    score(contract, loop_values[_denom][_loop_index], _vulnerability, 0); // undoubled
                                }
                            else {
                                opponents_score -= 
                                    score(contract, loop_values[_denom][_loop_index], _vulnerability, 1); // doubled
                                }
                            }
                        if (bid_state == 1 && opponents_score > total_score[opponents_level][opponents_denom]) {
                            total_score[opponents_level][opponents_denom] = opponents_score;
                            best_bid = 1;
                            }
                        if (bid_state == 1) best_total_score =  total_score[opponents_level][opponents_denom];
                        }
                    }
                else if (current_bid == contract && doubled && bid_state < 2) { 
                    unsigned int my_level=current_bid / 10-1, my_denom=current_bid % 10;
                    contract = current_bid;
                    for (bid_state = 0; bid_state < 2; bid_state++) {
                        for (unsigned int _loop_index = 0; _loop_index < LOOP_COUNT; ++_loop_index) { // total up all hands
                            if (bid_state == 0) {
                                best_bid = 0; // pass
                                total_score[my_level][my_denom] += 
                                    score(contract, loop_values[_denom][_loop_index], _vulnerability, 0); // undoubled
                                }
                            else {
                                my_score += 
                                    score(contract, loop_values[_denom][_loop_index], _vulnerability, 2); // redoubled
                                }
                            }
                        if (bid_state == 1 && my_score > total_score[my_level][my_denom]) {
                            total_score[my_level][my_denom] = my_score;
                            best_bid = 2;    // redoubled
                            }
                        if (bid_state == 1) best_total_score =  total_score[my_level][my_denom];
                        }
                    }
                if (contract <= current_bid) continue;
                if (doubled) continue;
                if (contract == 44) { total_score[_level][_denom] = -10000; continue; } // never bid 4N as it would be interpreted as blackwood
                for (int _loop_index = 0; _loop_index < LOOP_COUNT; ++_loop_index) {
                    int hand_score;
                    int tricks_taken = loop_values[_denom][_loop_index];
                    if (_level + 7 - tricks_taken > 2 && _doubled == 0) penalty_double = 1;
                    hand_score = score(contract, loop_values[_denom][_loop_index], _vulnerability, _doubled + penalty_double);
                    total_score[_level][_denom] += hand_score;
                    penalty_double = 0;
                    }
                cout << "Total score for level:" << (_level+1) << ":denom:" << _denom << " is:" << total_score[_level][_denom] << endl;
                if (total_score[_level][_denom] > best_total_score) {
                    best_total_score = total_score[_level][_denom];
                    best_level = _level+1;
                    best_denom = _denom;
                    best_bid = best_level * 10 + best_denom;
                    }
                }


        string result = "";
        if (best_bid < 0 || best_bid > 74 || best_bid % 10 > 4) {
            result = "ERR. Cannot get first bid (" + to_string(best_bid) + ")";
            return result;
            }
        else {
            char denoms[5] = {'C','D','H','S','N'};
            char levels[8] = {'0','1','2','3','4','5','6','7'};
            if (best_bid == 0) result.push_back('P');
            else if (best_bid == 1) result.push_back('P');
else {
                result.push_back(levels[best_level]);
                result.push_back(denoms[best_denom]);
                }
            return result;
            }
    }

const string check_show1(UNSIGNED who, UNSIGNED what, UNSIGNED where, UNSIGNED low, UNSIGNED high) {
    string showed_info = "";
    char *result = hool_test(0,88,0,0,0);
    int found = sscanf(result,"%u",&constraints::count);
    if (found != 1) return "ERR: cannot get constraint count in check_show1";
    if (what > 5) return "ERR: bad what in check_show1:" + to_string(what);
    gen32_do_log(1,40,(char *)"check_show1 entered; constraint_count=%u",constraints::count);
    showed[who][0][0] = who; showed[who][0][1]=what; showed[who][0][2]=where; showed[who][0][3]=low; showed[who][0][4]=high;
    if (what < DISTRIBUTION) {
        showed_info = "length";
        hool_test(who,ID_LEN,where,low,high);
        if (globals::systems[who%2]>0) { // additional info allowed, i.e., robots partnered
            hool_test(who,ID_HCPS,0,13,17); 
            hool_test(who,ID_AND,0,constraints::count,constraints::count + 1);
            constraints::count += 3;
            }
        else {
            hool_test(who,ID_AND,0,constraints::count,constraints::count);
            constraints::count += 2;
            }
        }
    else if (what == DISTRIBUTION) {
        showed_info = "pattern";
        hool_test(who,ID_DIST,0,where,where);
        hool_test(who,ID_DIST,1,low,low);
        hool_test(who,ID_DIST,2,high,high); // additional info #2
        if (globals::systems[who%2] > 0) {  // hidden information allowed, i.e., robots partnered
            hool_test(who,ID_HCPS,0,0,12); // additional information #1
            hool_test(who,ID_AND,0,constraints::count,constraints::count + 3); 
            constraints::count += 5;
            }
        else {
            hool_test(who,ID_AND,0,constraints::count,constraints::count + 2); 
            constraints::count += 4;
            }
        }
    else {
        showed_info = "hcps";
        hool_test(who,ID_HCPS,0,where,where);
        hool_test(who,ID_DIFF,constraints::count,0,0);
        constraints::count += 2;
        }  // no additional information
    return "OK:showed1:who:" + to_string(who) + ":" + showed_info;
    }

const string check_show2(UNSIGNED who, UNSIGNED what, UNSIGNED where, UNSIGNED low, UNSIGNED high) {
        string showed_info = "";
        showed[who][1][0] = who; showed[who][1][1]=what; showed[who][1][2]=where; showed[who][1][3]=low; showed[who][1][4]=high;
    if (what < DISTRIBUTION) { hool_test(who,ID_LEN,where,low,high);
        showed_info = "length";
        hool_test(who,ID_AND,0,constraints::count,constraints::count); constraints::count += 2; }
    else if (what == DISTRIBUTION) {
        showed_info = "pattern";
        hool_test(who,ID_DIST,0,where,where); hool_test(who,ID_DIST,1,low,low); hool_test(who,ID_DIST,2,high,high); // additional info #2
        hool_test(who,ID_AND,0,constraints::count,constraints::count+2); 
        constraints::count += 4;
        }
    else {
        showed_info = "hcps";
        hool_test(who,ID_HCPS,0,where,where);
        hool_test(who,ID_DIFF,constraints::count,0,0);
        constraints::count += 2;
        }  // no additional information
    return "OK:showed2:who:" + to_string(who) + ":" + showed_info;
    }

const string get_show1(unsigned int who) {
    gen32_do_log(1,20,(char *)"get_show1 entered");
    unsigned int hcps = get_hcps(who);
    unsigned int length, longest;
    set_longest(who, &longest, &length); // longest suit and its length
    if (hcps < 13)  {
        string dist = get_distribution_string(who);
        showed[who][0][1]=DISTRIBUTION;
        if (globals::systems[who%2] > 0)  return "Pattern:" + dist + ":also shows 0-12 hcps";
        else return "Pattern:" + dist;
        }
    else if (hcps < 19) {
        showed[who][0][1] = ID_DIST;
        if (globals::systems[who%2] > 0) 
            return "Length:" + to_string(length) + suits[longest] + ":also shows 13-18 hcps"; 
        else return "Length:" + to_string(length) + suits[longest]; }
    else {
        showed[who][0][1] = HCPS;
        string _hcps = get_hcps_string(who);
        return "HCPs:" + _hcps; }
    }

const string get_show2(unsigned int who) {
    unsigned int hcps = get_hcps(who);
    unsigned int longest_length, longest, shortest_length, shortest, three_suited, short_suit;
    set_longest(who, &longest, &longest_length); // longest suit and its length
    set_shortest(who, &shortest, &shortest_length); // shortest suit and it length
    set_three_suited(who,&three_suited,&short_suit); 
    if (showed[who][0][1] == DISTRIBUTION) {  // check first show
        if (hcps < 6) { 
            showed[who][1][1] = ID_DIST;
            if (globals::systems[who%2]>0)
                return "Length:" + to_string(shortest_length) + suits[shortest] + ":shortest:also shows 0-5 hcps"; 
            else return "Length:" + to_string(shortest_length) + suits[shortest];
            }
        else {
            showed[who][1][1] = ID_DIST;
            if (globals::systems[who%2]>0)
                return "Length:" + to_string(longest_length) + suits[longest] + ":longest:also shows 6-12 hcps";
            else return "Length:" + to_string(longest_length) + suits[longest];
            }
        }
    else if (showed[who][0][1] < DISTRIBUTION) { // first show was length (longest suit)
        if (longest_length < 5) {
            if (globals::systems[who%2]>0)
                return "Length:" + to_string(shortest_length) + suits[shortest] + ":also shows longest suit < 5";
            else return "Length:" + to_string(shortest_length) + suits[shortest];
            }
        else {
            string dist=get_distribution_string(who);
            return "Pattern:" +  dist;
            }
        }
    else {  // first show was HCPS
        if (three_suited==1) {
            return "Length:" + to_string(shortest_length) + suits[short_suit] + ":shows three suited this is short suit"; 
            } 
        else if (longest_length < 5) {
            string dist = get_distribution_string(who);
            return "Pattern:" + dist;
            }
        else {
            return "Length:" + to_string(longest_length) + suits[longest]; }
        }
    }

const string check_claim(unsigned int who, unsigned int tricks) {
    return "Reject:" + to_string(tricks);
    // return "Accept:" + to_string(tricks);
    }

const string check_system(unsigned int who, unsigned int system) {
    int dir = who % 2;
    globals::systems[dir] = system;
    string which_dir;
    if (dir==0) which_dir = "NS"; else which_dir = "EW";
    return "OK:system:" + which_dir + ":" + to_string(system);
    }

const string check_contract(int _contract, int _declarer, int _doubled) {
	int level = _contract / 10, denom = _contract % 10;
	if (_declarer < 0 || _declarer > 3) return "ERR:bad declarer";
	if (_doubled <0 || _doubled > 2) return "ERR:bad doubled";
	if (_contract == 0) { globals::contract = 0;  return "OK:passed out"; }
	if (level < 0 || level > 7) return "ERR:bad contract level";
	if (denom <0 || denom > 4) return "ERR:bad contract denom";
	leader[0] = (_declarer + 1) % 4; 
    plays::leader[0] = leader[0];
    globals::seat=0; globals::trump = _contract % 5;
	globals::contract = _contract; globals::declarer = _declarer; globals::doubled = _doubled;   // set globals
	return "OK:contract=" + std::to_string(globals::contract) + ",declarer=" + to_string(globals::declarer) + ",doubled=" + to_string(globals::doubled);
	}

const string check_bid(unsigned int bid) {
	if (bid==0) return "OK:0:pass received";
    else if (bid==1) return "OK:1:double received";
    else if (bid==2) return "OK:2:redouble received";
	int level = bid / 10, denom = bid % 10;
	if (level >=1 && level <=7 && denom <5) return "OK:" + to_string(bid) + ":bid received";
	return "ERR:bad bid in check_bid:" + to_string(bid);
	}


char card_str[3];
char *show_card_as_string(int card) {
    card_str[2]='\0';
    switch (card / 13) {
        case 0: card_str[0] = 'C'; break;
        case 1: card_str[0] = 'D'; break;
        case 2: card_str[0] = 'H'; break;
        case 3: card_str[0] = 'S'; break;
        default:  break;
        }
    switch (card % 13) {
        case 0: card_str[1] = '2'; break;
        case 1: card_str[1] = '3'; break;
        case 2: card_str[1] = '4'; break;
        case 3: card_str[1] = '5'; break;
        case 4: card_str[1] = '6'; break;
        case 5: card_str[1] = '7'; break;
        case 6: card_str[1] = '8'; break;
        case 7: card_str[1] = '9'; break;
        case 8: card_str[1] = 'T'; break;
        case 9: card_str[1] = 'J'; break;
        case 10:card_str[1] = 'Q'; break;
        case 11:card_str[1] = 'K'; break;
        case 12:card_str[1] = 'A'; break;
        default:  break;
        }
    return card_str;
}


const string check_play(unsigned int who, unsigned int card, unsigned int round, unsigned int seat) {
    /* does three things:
        1) stores into the arrays card_played and who_played: the play this round and seat
        2) updates the constraints that are used for dealing
            a) on the first time a player shows out of cards add the ID_VOID constraint
            b) add ID_PLAYED for the card that has been played in the constraints.
        3) updates/increments the globals for round and seat
    */
    if (round != plays::round) return "ERR:wrong round:" + to_string(round);
    if (seat != plays::seat) return "ERR:wrong seat:" + to_string(seat);
    plays::who_played[round][seat] = who; plays::card_played[round][seat]=card;
    if (seat==0) plays::leader[round] = who;
    unsigned int suit_led = plays::card_played[round][0] % 13;
    // unsigned int start = constraints::count;
    if (seat > 90 && card % 13 != suit_led && !constraints::showed_void[who][suit_led]) {    // this player is void
        hool_test(who,ID_VOID,suit_led,0,0);     // additional constraint for resticting dealing
        constraints::count++;
        }
    hool_test(who,ID_PLAYED,card,0,0);
    constraints::count++;
    string card_string = show_card_as_string(card);
    string played_message = "OK:round:" + to_string(plays::round) + ":seat:" + to_string(plays::seat);
    played_message += ":card:" + card_string;
    played_message += ":by " + to_string(who);
    plays::seat += 1;
    if (plays::seat > 3) { plays::round += 1; plays::seat = 0; }
    return played_message;
    }

unsigned int get_a_play(unsigned int trump, unsigned int leader, unsigned int, unsigned int, unsigned int);
const string get_play(unsigned int who, unsigned int round, unsigned int seat) {
    unsigned int card;
    string result;
    if (who > 3) return "ERR:bad who:" + to_string(who);
    if (recorder::num_of_cards[who] != 13) {
        for (int i=0;i<4;i++) fprintf(stderr,"%d %d; ",recorder::num_of_cards[i],i);
        return "ERR: I do not have 13 cards for " + to_string(who) + " rather " + to_string(recorder::num_of_cards[who]);
        }
    if (round != plays::round) return "ERR:wrong round:" + to_string(round);
    if (seat != plays::seat) return "ERR:wrong seat:" + to_string(seat);
    // strange but a reset for seat 0 is this as the 51(ace of spades) will be translated to 0s as a suit.
    // 220527 made 1st parameter the global trump
    // 220527 made 2nd parameter correct by setting the leader of a round given seat is 0.3
    if (seat==0) {
        plays::leader[round] = who;
        printf("leader round %d (0 based) is set to %d - info gained from hool server\n",round,who);
        }
    if (1) {
        card = get_a_play(globals::trump,plays::leader[round],plays::card_played[round][0],
            plays::card_played[round][1],plays::card_played[round][2]);
        if (card == 52) return "ERR: get_a_play returns 52";
        }
    result = check_play(who,card,round,seat);
    if (result[0]=='E') return result;
    string scard = show_card_as_string(card);
    return "OK:" + to_string(card) + ":" + scard;
    }

const string check_card(unsigned int who, unsigned int card) {
    /* 1) verifies this card is in who's hand and 2) stores the number of cards given to who.
	      command 9 sends card to dealer code; command 73 sends to here in the hool code.
    */

    bool found_card = false;
    int bottom = who*13, top = who*13 + 13;
    if (card > 51) return "ERR:bad card";
	if (who > 3) return "ERR:bad who";
    if (recorder::num_of_cards[who] > 13) return "ERR: too many cards given to " + to_string(who);
    for (int i=bottom; i<top; i++)
        if (deck[i]==card) { found_card = true; break; }
        else printf("%d-",deck[i]);
    if (!found_card) return "ERR:check_card:card " + to_string(card) + " not with " + to_string(who);
    recorder::num_of_cards[who]++; 
return "OK:who:" + to_string(who) + ":card:" + to_string(card);
}

void gen32_set_evaluation_system(int);
const string do_reset(unsigned int who) {
	if (who > 3) return "ERR:bad who on reset";
    gen32_set_evaluation_system(1); // BERGEN counting of points
	constraints::count = 0;
    for (int i=0; i<52; i++) {
        deck[i]=53;
        }
	for (int i=0; i<4; i++) {
        recorder::num_of_cards[i]=0;
        for (int j=0; j<2; j++) { showed[i][j][3]=0; showed[i][j][4]=40; }
        for (int j=0; j<4; j++) { constraints::showed_void[i][j]=false; }
        }
    for (int i=0; i<13; i++)
        for (int j=0; j<4; j++) {
            plays::who_played[i][j]=0;
            plays::card_played[i][j]=0;
            }
    plays::round=0; plays::seat=0;
	globals::state = 0;
	globals::seat = 0; who_am_i = who;
	return "OK:reset:who_am_i=" + to_string(who_am_i);
}

bool check_played(unsigned int card) {
	for (unsigned int i=0; i<52; i++)
    if (plays::card_played[i/4][i%4] == card) return true;
	return false;
	}                         

	
unsigned int get_winner(unsigned card1, unsigned card2, unsigned _trump) {
	unsigned int suit1 = card1 / 13, suit2 = card2 / 13;
	if (suit1 != suit2) {                                                  
		if (suit1 == _trump) return card1;
		else if (suit2 == _trump) return card2;
		else return card1;
		}
	else if (card1 > card2) return card1;
	else return card2;
	}


bool initial_reset = false;

void show_num_of_cards(void) {
    for (int i=0; i<4; i++) printf("%d %d; ",i,recorder::num_of_cards[i]);
    }
const string generateCommandString(unsigned int who,unsigned int what,unsigned int where,unsigned int low, unsigned int high) {
	string result;
	switch (what) {
		case GET_SHOW_1: deck_copy_to_deck(); show_num_of_cards(); return get_show1(who); 
		case GET_SHOW_2: show_num_of_cards(); return get_show2(who);
		case GET_BID1: show_num_of_cards(); return get_bid(who,where,low);
		case CHECK_BID: show_num_of_cards(); return check_bid(where); 
		case RESET: if (!initial_reset) {
            hool_test(0,99,0,0,0);
            initial_reset=false;
            }
            return do_reset(who);
		case CHECK_CARD: deck_copy_to_deck(); show_num_of_cards(); return check_card(who,where);
		case CHECK_CONTRACT: return check_contract(where,low,high); 
        case CHECK_CLAIM: return check_claim(who,where);
        case CHECK_SYSTEM: return check_system(who,where);

		case GET_VERSION: return "version:0.3:220425";
        case CHECK1_HCPS: show_num_of_cards(); return check_show1(who,5,where,0,0);
        case CHECK2_HCPS: return check_show2(who,5,where,0,0);
        case CHECK1_LEN: show_num_of_cards(); return check_show1(who,where,where,low,high);
        case CHECK2_LEN: return check_show2(who,where,where,low,high);
        case CHECK1_DIST: show_num_of_cards(); return check_show1(who,4,where,low,high);
        case CHECK2_DIST: return check_show2(who,4,where,low,high);
        case CHECK_PLAY: return check_play(who, where, low, high); // who, card, round, seat
case GET_PLAY: return get_play(who, low, high); // who, round, seat                  
	}
	return hool_test(who, what, where, low, high);

    // "ERR:command not recognized" + to_string(what);
}


unsigned int get_a_play(unsigned int trump, unsigned int leader, unsigned int play1, unsigned int play2, unsigned int play3);
void readData(std::istream& in, const char *prompt)
{            
   // Do the necessary work to read the data.
   // vector <string> line;
   
   string line;

    char *tok, *result;
    int who, what, where, low, high;
    unsigned int values[5], index;
    if (prompt[0] != '\0') cout << ">";
    while (std::getline(in, line)) {
        tok = strtok((char *) line.c_str(),",");
        if (tok==NULL) break;
        index = 0;
        values[index] = atoi(tok);
        while ( (tok = strtok(NULL,",")) != NULL) {
            values[++index] = atoi(tok);
            if (index ==4) break;
            }
        if (index == 4) {
            who = values[0]; what = values[1]; where=values[2]; low=values[3]; high = values[4];
            if (what != 70 && what != 71 && what != 72 && 
                what < 64 && what > 69) { result = hool_test(who, what, where, low, high);  cout << result << endl; }
            else { 
                deck_copy_to(deck);
                printf("deck copied\n");
                cout << generateCommandString(who, what, where, low, high) << endl;
                }
            }
        else { cout << "Invalid input:<" << line << ">" << "=> not processed" << endl; }
        if (prompt[0] != '\0') cout << ">";
        }
}                     

#define PBN_COUNT 10
unsigned int get_suit(unsigned int);
unsigned int get_rank(unsigned int);
char pbns[PBN_COUNT][80]; // store of the hands that have been generated.
char best_card[3];
bool initial_set_resources=false;
unsigned int get_a_play(unsigned int trump_for_play, unsigned int leader, unsigned int play1, unsigned int play2, unsigned int play3)
/*  returns a card (or impossible 52 on error detected)

    Given trump, the leader (not current player), and the plays already made to this trick
        determine the best play.
    This is accomplished by:
        1) from the bidding and play so far a set of hands can been generated, the number
                of hands is set by PBN_COUNT
        2) 13 cards are known to opening leader, but 26 for all other plays.
        3) send the hands to the dds function SolveAllBoards, returns the number of tricks
                won by each card on each hand 
        4) find the card with the highest number of tricks won and return it 
*/
{
    boardsPBN bo;
    solvedBoards solved;
    int res;
    char line[80];
    unsigned int card_to_play;

  bo.noOfBoards = PBN_COUNT;
  unsigned int current_player = (leader + plays::seat) % 4;
  
  if (recorder::num_of_cards[current_player] != 13) {
    fprintf(stderr,"error from get_a_play; does not know all 13 cards for %d\n",current_player);
    return 52; // error, must know the cards
    }

  // dealing_command indicates which hand of known cards are blocked from the dealing function.
  unsigned int dealing_command=0;  // do not include hand in dealing because it is known 1=N,2=E,4=S,8=W
  unsigned int dummy = (plays::leader[0]+1) % 4;  // dummy always known after opening lead
  unsigned int hidden_player; // hidden player is current player except declarer is for dummy
  if (current_player != dummy) hidden_player = current_player;
  else hidden_player = (dummy + 2) % 4; // declarer is the known hand opposite dummy
  switch (hidden_player) {
      case 0: dealing_command = 1; break;
      case 1: dealing_command = 2; break;
      case 2: dealing_command = 4; break;
      case 3: dealing_command = 8; break;
    }
  if (plays::round > 0 || plays::seat > 0) { // add dummy to dealing_command except for opening leader
      switch (dummy) {
        case 0: dealing_command |= 1; break;
        case 1: dealing_command |= 2; break;
        case 2: dealing_command |= 4; break;
        case 3: dealing_command |= 8; break;
        }
      }
  // deal out the hands for other players and also mark played cards by current player
  // this should be marking the played cards for all 4 hands.
  // the dealing_command is bit_wise for the hands that should be excluded from dealing.
  hool_test(0,98,dealing_command,0,PBN_COUNT); // need to deal out the hands with played cards set now
  cout << "back from dealing" << endl;
    for (int handno = 0; handno < PBN_COUNT; handno++)
        {
    bo.deals[handno].trump = trump_for_play;
    bo.deals[handno].first = leader;
    bo.deals[handno].currentTrickSuit[0] = get_suit(play1);
    bo.deals[handno].currentTrickSuit[1] = get_suit(play2);
    bo.deals[handno].currentTrickSuit[2] = get_suit(play3);
    bo.deals[handno].currentTrickRank[0] = get_rank(play1);
    bo.deals[handno].currentTrickRank[1] = get_rank(play2);
    bo.deals[handno].currentTrickRank[2] = get_rank(play3);
    for (int seat=plays::seat; seat < 3; seat ++) { // zero out unplayed cards
        bo.deals[handno].currentTrickSuit[seat] = 0;
        bo.deals[handno].currentTrickRank[seat] = 0;
        }
    strcpy(&pbns[handno][0],hool_test(0, ID_GET_PLAYING_HAND, handno, 0, 0));
    strcpy(bo.deals[handno].remainCards, pbns[handno]);
    if (handno < 10) cout << handno << ":remaining cards:" << bo.deals[handno].remainCards << endl;

    bo.target [handno] = -1;
    bo.solutions[handno] = 3;
    bo.mode [handno] = 0;
  }

    if (!initial_set_resources) {
        SetResources(0,4); // automatic memory allocation, 4 cores for threading
        initial_set_resources = true;
        }
  res = SolveAllBoards(&bo, &solved);
  
  if (res != RETURN_NO_FAULT)
  {
    ErrorMessage(res, line);
    if (strcmp(line,"Zero cards")==0) {
        fprintf(stderr,"DDS received no cards\n");
        return 52;
        }
    else {
        fprintf(stderr,"DDS error: %s\n", line);
        cout << "constraints" << endl << hool_test(0,89,0,0,0) << endl; // print out constraints
        return 52;
        }
  }
    futureTricks *fut;
    extern unsigned char dcardSuit[5],dcardRank[16];
    int best_result=0;
    int results[4][15]; //  not 13 because DDS is 2 to 14 for rank
    for (int i=0; i< 4; i++) for (int j=0; j<15; j++) results[i][j]= -1;
    // accumulate the number of tricks won by each card in each hand
    for (int handno=0; handno<PBN_COUNT; handno++) {
        fut = &solved.solvedBoard[handno];
        for (int i = 0; i < fut->cards; i++) {
            char res[15] = "";
            equals_to_string(fut->equals[i], res);
            // special exception for singletons
            if (fut->score[i] == -2) fut->score[i]=1; // because it was coming back -2 for lone card
            // special exception to get back to an available card; else initialized to -1 to prevent use
            if (results[fut->suit[i]][fut->rank[i]] == -1)  results[fut->suit[i]][fut->rank[i]]=0;
            results[fut->suit[i]][fut->rank[i]] += fut->score[i];
            }
        }
    // find the best_result from the accumulated number of tricks for each card
	card_to_play = 52; // impossible value to initialize
	for (int i=0; i< 4; i++)
        for (int j=14; j>=2; j--) {
        // for (int j=2; j<15; j++) {
            if (results[i][j] >= best_result) {
                best_card[0]=dcardSuit[i];
                best_card[1]=dcardRank[j];
                card_to_play = (3-i)*13 + j-2;
                best_result = results[i][j];
                }
            }
    printf("the best play is %s (tested %d hands)\n",best_card,PBN_COUNT);
    best_card[2]='\0';
    return card_to_play;
    }

int main(int argc, char** argv)
{

   gen32_set_evaluation_system(1); // use BERGEN for evaluation of hands
   if ( argc > 1 )
   {
      // The input file has been passed in the command line.
      // Read the data from it.
      std::ifstream ifile(argv[1]);
      if ( ifile )
      {
         readData(ifile,"");
      }
      else
      {
         // Deal with error condition
      }
   }
   else
   {
      // No input file has been passed in the command line.
      // Read the data from stdin (std::cin).
      readData(std::cin,">");
   }

   // Do the needful to process the data.
}
unsigned int get_suit(unsigned int play) { return 3 - (play / 13); }
unsigned int get_rank(unsigned int play) { return play % 13 + 2; }
unsigned char dcardRank[16] =
{ 
  'x', 'x', '2', '3', '4', '5', '6', '7',
  '8', '9', 'T', 'J', 'Q', 'K', 'A', '-'
};

unsigned char dcardSuit[5] = { 'S', 'H', 'D', 'C', 'N' };
unsigned short int dbitMapRank[16] =
{
  0x0000, 0x0000, 0x0001, 0x0002, 0x0004, 0x0008, 0x0010, 0x0020,
  0x0040, 0x0080, 0x0100, 0x0200, 0x0400, 0x0800, 0x1000, 0x2000
};
unsigned char dcardHand[4] = { 'N', 'E', 'S', 'W' };

void equals_to_string(int equals, char * res)
{
  int p = 0;
  int m = equals >> 2;
  for (int i = 15; i >= 2; i--)
  {
    if (m & static_cast<int>(dbitMapRank[i]))
      res[p++] = static_cast<char>(dcardRank[i]);
  }
  res[p] = 0;
}


