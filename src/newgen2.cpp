/* Versions
210209 No changes from windows version
*/
#include "meadowlark.h"
#define GLOBALS
#include "../include/GLOBALS.H"

#ifndef DIVORCE

#ifdef WIN32
int write_matt(char *);
int write_file(char *);
int write_pbn(char *,char *);
int read_file(char *,char *);
int read_pbn(char *,char *);
int gen32_get_hcps(int *,int,int);

#endif


#if !defined(LINUX)
#include "../include/Pch.h"
#else
#include <stdarg.h>
void output(char* fmt, ...);
#endif
#include <stdarg.h>
#include "string.h"
#include "time.h"


#include "ctype.h"
//#include "dos.h"
//#include "io.h"
#ifdef GEN32
// #include <io.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif
#ifdef GEN32
#define _fstrcat strcat
#define _fstricmp _stricmp
#endif
#define lseek _lseek

// #define GIB

/* #define EXCHANGING */
#include "../include/bergen.h"
#include "../include/br.h"

// struct nlist *buildtree(int distributions[DISTMAX][4],int distmax,int leftovers[4],int care);
int gen32_get_quicks(int deckp[52], int distp[4], int i, int num);
// int gen32_bergen(int [52],int [4],int,int,int);
int gen32_bergen(int [52],int,int,int,int);


void test_hcp(void);
#ifdef EXCHANGING
extern unsigned int evaluate_hcp(int i);
#endif
void my_newgen_error(char *str);
void gen_init2(void);
void gen_init1(void);
extern int ready(void);
#ifdef EXCHANGING
extern void exchanging(int [4], int);
#endif

#ifdef TEST_IO
extern void test_io(void);
#endif

// OFSTRUCT In_Of,Out_Of;

#define HIGH 10000
int gen32_deck[52];
// int gen32_dist[4];
int meter_parts;
int meter_count;
int max_store;
char tempc[53];
char gen_initialized = 0;
int copy_only=OFF;
// static char debug=0;
int rotate_dealer = OFF;
int rotate_vul = OFF;
#ifdef DEXTRO_ROTATE
int rotate_dextro = OFF;
#endif
int offset;
struct nlist *start=0;
#ifdef HUGE
struct hands huge *h_start;
struct hands huge *h_last;
struct hands huge *h_current;
struct hands huge *p_low;
struct hands huge *p_high;
struct hands huge h_use;
#else
struct hands *h_start;
struct hands *h_last;
struct hands *h_current;
struct hands *p_low;
struct hands *p_high;
struct hands h_use;
#endif

#ifndef __DLL__
void process(void);
#if !defined(linux)
#ifndef WIN32
#pragma argsused
#endif
#endif

#ifdef FALL_BACK
#include <time.h>
#include <sys/time.h>
#include <stdint.h>

uint64_t get_gtod_clock_time ()
{
    struct timeval tv;

    if (gettimeofday (&tv, NULL) == 0)
        return (uint64_t) (tv.tv_sec * 1000000 + tv.tv_usec);
    else
        return 0;
}
#endif


char *constraint_command(int,int,int,int,int);
int deck_verify(int);
int set_offsets(int);
void clear_constraints(int);
void do_all_unity_tests(void);
#if !defined(LINUX)  && defined(NEEDING_MAIN)
main(int argc, char **argv)
{
	LARGE_INTEGER tcounter;
	LONGLONG freq;    
	LONGLONG prev_tick_value, tick_value;
	LONGLONG usecs;
	char *result;

	do_all_unity_tests();

if (QueryPerformanceFrequency (&tcounter) != 0)
    freq = tcounter.QuadPart;

// The timer frequency is fixed on the system boot so you need to get it only once.

// query the current ticks value with QueryPerformanceCounter:



	fprintf(stderr,"main. initializing\n");
	initialize();
	deck_verify(0);
	set_offsets(15);
	clear_constraints(0);



	// fprintf(stderr,"unity testing is turned off\n"); 
if (QueryPerformanceCounter (&tcounter) != 0)
    prev_tick_value = tcounter.QuadPart;
#ifdef VERBOSE
	fprintf(stderr,constraint_command(0,ID_RESET,0,0,0)); fprintf(stderr,"\n");
	fprintf(stderr,constraint_command(0,0,0,10,12)); fprintf(stderr," 10-12 HCPs\n"); // 10-12 high card points
	fprintf(stderr,constraint_command(0,7,0,0,0)); fprintf(stderr,"\n"); // control
	fprintf(stderr,constraint_command(0,2,3,2,2)); fprintf(stderr,"short suit is 2\n"); // shortest suit is 2
	fprintf(stderr,constraint_command(0,7,2,0,0)); fprintf(stderr,"\n"); // control
	fprintf(stderr,constraint_command(0,1,3,3,3)); fprintf(stderr," 3 spades\n"); // spade length is 3
	fprintf(stderr,constraint_command(0,7,4,0,0)); fprintf(stderr,"\n"); // control
	fprintf(stderr,constraint_command(0,2,0,4,4)); fprintf(stderr,"long suit is 4\n"); // shortest suit is 2
	fprintf(stderr,constraint_command(0,7,6,0,0)); fprintf(stderr,"\n"); // control
	fprintf(stderr,"now calling deal\n");
#else
	constraint_command(0,ID_RESET,0,0,0); // fprintf(stderr,"\n");
	constraint_command(0,0,0,10,12); // fprintf(stderr," 10-12 HCPs\n"); // 10-12 high card points
	constraint_command(0,7,0,0,0); // fprintf(stderr,"\n"); // control
	constraint_command(0,2,3,2,2); // fprintf(stderr,"short suit is 2\n"); // shortest suit is 2
	constraint_command(0,7,2,0,0); // fprintf(stderr,"\n"); // control
	constraint_command(0,1,3,3,3); // fprintf(stderr," 3 spades\n"); // spade length is 3
	constraint_command(0,7,4,0,0); // fprintf(stderr,"\n"); // control
	constraint_command(0,2,0,4,4); // fprintf(stderr,"long suit is 4\n"); // shortest suit is 2
	constraint_command(0,7,6,0,0); // fprintf(stderr,"\n"); // control
#endif
	result = constraint_command(0,ID_DEAL,0,0,10); // deal out 10 hands
if (QueryPerformanceCounter (&tcounter) != 0)
    tick_value = tcounter.QuadPart;
	usecs = (tick_value - prev_tick_value) / (freq / 1000000);

	fprintf(stderr,"usecs:%d\n",usecs);
	fprintf(stderr,"result:%s\n",result);
	fprintf(stderr,"now calling ID_GET_HAND\n");
	fprintf(stderr,constraint_command(0,ID_GET_HAND,0,0,0)); fprintf(stderr,"\n"); // get the first dealt hand
	fprintf(stderr,"now calling gen32_get_description2\n");
	fprintf(stderr,gen32_get_description2(0,0)); fprintf(stderr,"\n");
	fprintf(stderr,"done\n");
	return 1;


}
#endif

#if !defined(linux)
#ifndef GEN32
void set_percent_loaded(int);
#pragma argsused
void set_percent_loaded(int i) {}
#endif	  /* of STAND_ALONE */
#endif
#endif



#ifndef HOFFER
#ifdef __DLL__
// #pragma argsused
int FAR PASCAL LibMain(HANDLE hInstance, WORD wDataSeg,
	WORD wHeapSize, LPSTR lpszCmdLine)
{
#ifndef GEN32
	if (wHeapSize > 0) UnlockData(0);
#endif

	return 1;
}
// #pragma argsused

int FAR PASCAL WEP( int nParam)
{
return 1;
}
#endif	  /* of DLL defined */
#endif


#ifndef CLEAN_UP
void process2(char *str) {
	output(str);
}
#endif

#undef GIB
// #define GIB_DBG
#ifdef GIB
FPSTRPR external_print_function;
void FAR PASCAL _export gib_process(FPSTRPR pp,LPSTR str) {
#ifdef GIB_DBG
	MessageBox(GetFocus(),str,"GIB_PROCESS",MB_TASKMODAL);
#else
	FPSTRCB lp = NULL;
		external_print_function = pp;
	process(lp,str);
#endif
	}
#endif

#ifdef GIB
void FAR _export gib_process2(FPSTRPR pp,LPSTR str) {
#ifdef GIB_DBG
	MessageBox(GetFocus(),str,"_gib_process2",MB_TASKMODAL);
#else
	FPSTRCB lp = NULL;
		external_print_function = pp;
	process(lp,str);
#endif
	}
#endif


char generator_hand[100];
int get_matt_string2(int deck[52], char *);

#ifndef CLEAN_UP
char *generator(void) {
//	  int i;
	mymax = 1;


//	srand(time(NULL));


	if (do_gen() == 0) generator_hand[0] = '\0';
	else get_matt_string2(gen32_deck,generator_hand);
	return generator_hand;
	}
#endif

void validate_top(int value) {
	if (!use_flag && value == 0) {	 // free up a dynamically allocated hand
		if (h_last->prev == NULL) free((void *) h_last); // only one node
		else {
			h_last = h_last->prev;
			h_current = h_last;
			free((void *)h_last->next);
			}
		}
	else if (use_flag && value == 1) {
		if (h_current->next != NULL) h_current = h_current->next;
		}
	}



char *my_get_first(void) {
	int i;

	for (i=0; i<52; i++)
		tempc[i]=h_start->deck[i];

	return(tempc);
}

void initialize(void) {
	if (!gen_initialized) {
		gen_init1();
		gen_init2();
		gen_initialized = TRUE;
		}
	}


void gen_init2(void) {
	divisions = 1;
	rotate_dealer = OFF;
#ifdef DEXTRO_ROTATE
	rotate_dextro = OFF;
#endif
	rotate_vul = OFF;
	combined=rheader=unique=intake=hcponly=range=OFF;
	deck_shuffle_flag=OFF;
#ifdef DEXTRO_ROTATE
	match_rotation = OFF;
#endif
	distonly=shape=care=dheader=gen_header=twohandin=OFF;
	toprinter=bytesonly=print_range=OFF;
	crlf=dataout=view=pairpr=graphic=OFF;
	fillpage=both=OFF;
	copy_only=OFF;

	iff_out_flag = OFF;
	specify_flag = OFF;
	print_flag=ON;
	// normal=ON;
	prompt_flag=OFF;
	fillpos=0;
#ifndef CLEAN_UP
	if (h_start != NULL) hand_free();
	if (start != NULL) treefree(start);
#endif
	start=NULL;

	zap_flag = OFF;
	total_hands=0;
	meter_parts = 0;

	readflag=prompt_flag=ON;
	bugsflag=biddingflag=outfile=OFF;

	wait_time=0;
	handcount=0;
	high=37; low=0;
	starter=0;
	mymax=3;
	max_store = mymax;
	hands_read=0;
	innum=0;
	prhands=1;
	dextro = 0;
	shuffling=1;
	distmax=0;
	paircount=0;
#ifndef HOFFER
#ifndef GEN32
	randomize();
#else
	srand((unsigned) time(NULL));
#endif
#else
srand((unsigned) time(NULL));
#endif
	offset = 0;
}
#define OLD_SHUFFLING

void gen_init1(void) {
	int i;
#ifndef CLEAN_UP
	setperms(); 			/* sets permutation table up	*/
#endif
#if !defined(GEN32) && !defined(OLD_SHUFFLING)
	setcbrk(1);
#endif

	for ( i=0; i < 52; i++) gen32_deck[i]=i;
	for ( i=0; i < 4; i++) gen32_dist[i]=0;
#ifndef CLEAN_UP
	for (i=0; i<=52; i++)
		factorials[i]=factorial(i);
#endif
}

extern void shuffle(int *deck, int first,int number);
#if !defined(OLD_SHUFFLING)
void hcp_range_shuffle(int *ar, int first, int number)
{

    float percent, run_percent;
    int a,b,c,d,i,rank,j;


    percent = (float) rand() / (float) RAND_MAX;
    run_percent = 0.0;
    
    for (i=low; i<=high; i++) {
        run_percent += points[i].percent;
        if (percent < run_percent) break;
        }

    /* a point count has now been selected; represented by i    */

    percent = (float) rand() / (float) RAND_MAX;
    run_percent = 0.0;

    for (a=0; a<=honor_count[3]; a++)
     for (b=0; b<=honor_count[2]; b++)
      for (c=0; c<=honor_count[1]; c++)
       for (d=0; d<=honor_count[0]; d++) {
        if (t[a][b][c][d].HCPs == i) {
            t[a][b][c][d].percent =
                ((float) t[a][b][c][d].count / (float) points[i].count);
            run_percent += t[a][b][c][d].percent;
            if (percent < run_percent) {
                do_select(a,b,c,d,honors,honor_count);
                rearrange(ar,first,a,b,c,d,honors,honor_count);
                honor_selected_count = a+b+c+d;
                goto get;
                }
            }
        }
        
    /* select the spot cards. Number = 13 - numbers of honors selected */
    
get:    spot_count=0;
    for (i=0; i<36; i++) spots[i]=0;

    for (i=first; i<first + number; i++) {
        rank = ar[i] % 13;
        if (rank < 9) spots[spot_count++]=ar[i];
        }


    get_selections(spots,13 - honor_selected_count,spot_count);

    for (i=0; i<13-honor_selected_count; i++) {
        for (j=0; j<52; j++)
            if (ar[j] == spots[spot_count-i-1]) break;
        exchange(ar,first+honor_selected_count+i,j);
        }
    
    /* now shuffle the cards leftover */
    
    gen32_shuffle((int *)ar,first+13,52-first-13);
}

void rearrange(int ar[],int first,
        int a,int b,int c,int d,int honors[4][4],int count[4])
{
    rearrange2(ar,first,a,honors[3],count[3]-1);
    rearrange2(ar,first+a,b,honors[2],count[2]-1);
    rearrange2(ar,first+a+b,c,honors[1],count[1]-1);
    rearrange2(ar,first+a+b+c,d,honors[0],count[0]-1);
}

void rearrange2(int ar[52],int first,int num,int honors[4], int count)
{
    int i;

    while (num--) {
        for (i=first; i<52; i++) {
            if (ar[i] == honors[count]) break;
            }
        count--;
        exchange(ar,first++,i);
        }
}
    
void do_select(int a,int b,int c,int d,int honors[4][4],int count[4])
{
    get_selections(honors[3],a,count[3]);
    get_selections(honors[2],b,count[2]);
    get_selections(honors[1],c,count[1]);
    get_selections(honors[0],d,count[0]);
}
#endif

/* exchange
    exchange elements of an int array
*/

void exchange(int *a,int to,int from)
{
    int temp;
/*    if (to < 0 || to >51 || from <0 || from > 51)  {
        printf("\nexchange error");
        exit(1);
        }                    */
    
    temp = a[to];
    a[to]=a[from];
    a[from]=temp;
}

#if !defined(OLD_SHUFFLING)
/* get_selections
    from an array select num elements randomly...done recursively
    the selected elements are moved to the top of the array.
*/

void get_selections(int *a,int num,int tot)
{

    if (!num  || !tot) return;
    exchange(a,tot-1,rand() % tot);
    get_selections(a,num-1,tot-1);
}

int rset(int ar[], int first, int number)
{


/* Data structures:
    honors[]        - the 16 honor cards;
    spots[]         - the 36 spots;
    points[]        - a count and relative % for each HCP total;
    t[]             - a table of the HCPs, total combinations possible
              and relative % within a point count.
              t[aces][kings][queens][jacks]...

              i.e. t[1][1][1][1].HCPs will be 10.
                   t[1][1][1][1].count will be 4*4*4*4 given
                a full deck to select from;
                   t[1][1][1][1].percent will be 4*4*4*4 /
                the total count of ways to produce 10 HCPs
    honor_count[]   - the  number of availble honors for each ace, king ...
    combos[][]      - the number of combinations available for honors...
              for example combos[2][4] is 6, the total number
              of combinations of 2 things from a set of 4.
*/
    int i, j, count, total_points;
    int a,b,c,d, rank;
    float combo_total;

/*    float percent, run_percent;      */

    for (i=0; i<4; i++)
        for (j=0; j<4; j++) honors[i][j]=0;
    for (i=0; i<36; i++) spots[i]=0;
    for (i=0; i<4; i++) honor_count[i]=0;
    for (i=0; i<40; i++) {
        points[i].count = 0.0;
        points[i].percent = 0.0;
        }

    count =0; total_points = 0; spot_count=0;

    for (i=first; i<first + number; i++) {
        rank = ar[i] % 13;
        if (rank > 8) {
            count++;
            honors[rank-9][honor_count[rank-9]++] = ar[i];
            total_points += rank-8;
            }
        else spots[spot_count++]=ar[i];
        }

    if (total_points < low) {
/*        printf(" Total pts < low pts");      */
        return(ERROR);
        }

    for (a=0; a<=honor_count[3]; a++)
     for (b=0; b<=honor_count[2]; b++)
      for (c=0; c<=honor_count[1]; c++)
       for (d=0; d<=honor_count[0]; d++) {
        t[a][b][c][d].HCPs = a*4 + b*3 + c*2 +d; /* HCPs */
        if (t[a][b][c][d].HCPs > high || t[a][b][c][d].HCPs < low ||
            a+b+c+d > 13)
            t[a][b][c][d].count = 0.0;
        else t[a][b][c][d].count = combos[a][honor_count[3]] *
                    combos[b][honor_count[2]] *
                    combos[c][honor_count[1]] *
                    combos[d][honor_count[0]];
/* PN1 */       if (t[a][b][c][d].count != 0.0 && 13-a-b-c-d) {
            ftemp = (float)combination(spot_count,13-a-b-c-d);
            t[a][b][c][d].count *= ftemp;
            }
        points[t[a][b][c][d].HCPs].count += t[a][b][c][d].count;
        }

/*      Given the total number of combinations for each point count
    now figure the relative percentage of each point count.
    For example: there are more ways to get a 13 point honor
    combination than 10 point combinations, however, the number
    of the spot card combinations makes a total so that 10 point
    hands are the most common and 10 point hands would always be
    more likely than 13 point hands even if the range is limited
    to 10-13 point hands. These relative percentages are calculated
    in the following section.
*/

    combo_total = 0.0;
    for (i=low; i<= high; i++)
        combo_total += points[i].count;

    if (combo_total < 1) return(ERROR);

    for (i=low; i<= high; i++)
        points[i].percent = points[i].count / combo_total;

    return(OK);
}

/* PN1
    At this point the number of honor cards has been selected.
    There are a certain number of combinations for these honor
    cards and there are a certain number of combinations for the
    spot card positions that remain.

    For example: if 10 honor cards are selected from a full deck
    then there are 3 spot cards to be selected from the 36 spot
    cards available. If all 13 cards are honors then there are no
    spot cards to be selected.

    The number of ways to get the spots and honors is the combination
    of honors times the combination of spot cards.
*/





double combination(int x, int y)
{
	return(permutation(x,y)/factorials[y]);
}

double permutation(int x, int y)
{
	if (y==1) return((double) x);
	else return(((double) x) * permutation(x-1,y-1));
}

double factorial(int x)
{
	if (x<2) return(1.0);
	else if (factorials[x-1] > 0.0) return((float) x * factorials[x-1]);
	else return((float) x * factorial(x-1));
}
#endif

void do_dextro(void) {
	int temp[52];
	int i,start;

	start = (4 - dextro) * 13;
	for (i=0; i<52; i++)
		temp[i] = gen32_deck[(start + i) % 52];
	for (i=0; i< 52; i++)
		gen32_deck[i]=temp[i];
}

#ifndef CLEAN_UP
#ifdef DLL
#define DO_GEN_FUNC int do_gen(FPSTRCB lpfnSPL)
#else
#define DO_GEN_FUNC int do_gen(void) 
#endif
DO_GEN_FUNC {
	int count,i,j,k;
    int total_points;

	if (use_flag) {    // get the hand from already generated hands.
		for (i=0; i<52; i++) h_use.deck[i]=h_current->deck[i];
		starter = twohandin ? 26 : 13;
		for (i=0; i<52; i++) gen32_deck[i]=h_use.deck[i];
		build_tree_flag = ON;
		if (autoinc_flag) {
			myuse++;
			build_tree_flag = ON;
			}
		}
	else if (intake == ON && readflag == ON)  {
			starter=0; handcount=0; readflag=OFF;
			build_tree_flag=ON;
			if ((starter=gethand(gen32_deck,starter)) == EOF)
				return 0;
			hands_read++;
#ifndef EXCHANGING
			reset_hcps();
#endif
			if (starter != 13) {
#ifdef WINDOWS
			MessageBox(GetFocus(),"Improper readin...not 13","DO_GEN",MB_TASKMODAL);
#else
				fprintf(stderr,"\nImproper readin...not 13");
				exit(1);
#endif
				}
			if (copy_only)	{
					combined = OFF; build_tree_flag = OFF;
					shape = OFF; range=OFF;
					if ((starter=gethand(gen32_deck,starter)) != 26) goto errm;
					if ((starter=gethand(gen32_deck,starter)) != 39) goto errm;
					if ((starter=gethand(gen32_deck,starter)) != 52) goto errm;
					goto okm;
errm:
#ifdef WINDOWS
			MessageBox(GetFocus(),"Not enough in gethand for twohandin","DO_GEN",MB_TASKMODAL);
#else
					fprintf(stderr,"\ncant read 2nd hand");
#endif
okm:;
					}

			else if (twohandin==ON) {
				if ((starter=gethand(gen32_deck,starter)) != 26)
#ifdef WINDOWS
			MessageBox(GetFocus(),"Not enough in gethand for twohandin","DO_GEN",MB_TASKMODAL);
#else
					fprintf(stderr,"\ncant read 2nd hand");
#endif
				}
			}  /* of the if statement */

			/* build_tree_flag is set on in four situations:
				1) range changed with eval_command r.
				2) shape changed with eval_command s.
				3) use_flag and autoinc_flag is on.
				4) intake is on

			   In those situations it is necessary to rebuild
			   the trees which hold the information for generating
			   the shape or high card points. Once generated they
			   do not have to change without a change in the state
			   of the hand being used or of the range or shape whichever
			   is the case.
			*/

	if (combined == ON) {
		total_points = gen32_get_hcps(gen32_deck,0,13);
		low = c_low - total_points;
		high = c_high - total_points;
		if (low < 0) low = 0;
		if (high < 0) high = 0;
		}

#ifdef EXCHANGING
	restrict_range(low,high);  /* in new section */
	if (starter != 0)
		restrict_available(deck,0,starter);
#endif


	if (build_tree_flag == ON) {
		if (shape == ON) {
			for (i=0; i<4;i++) leftovers[i]=13;
			for (i=0; i < starter; i++)
				--leftovers[gen32_deck[i] / 13];
			if (start != NULL) treefree(start);
			start=buildtree(distmem,distmax,leftovers,care);
			}
		if (range==ON || combined == ON)
			if (rset(gen32_deck,starter,52-starter)== ERROR) {
#ifdef WINDOWS
			MessageBox(GetFocus(),"RSET error...No combinations available for that","DO_GEN",MB_TASKMODAL);
#else
				my_newgen_error("No combinations available for that");
				exit(0);
#endif
				}
		build_tree_flag=OFF;
		}


		/* This next section is a loop. The one troublesome situation
		   is that of a constraint on the range and a constraint on the
		   distribution. In that case the distributions are generated
		   and then the hand is checked to see if it has the required
		   number of high cards. If not then it loops and regenerates
		   until one can be found. Obviously then there could be times
		   when it is impossible to generate a hand of x points with
		   a particular distribution. It is a vexing problem to try
		   and figure out that beforehand (if it is possible it is a
		   tough problem!). Therefore to give some latitude I have let
		   it loop up to 10000 times before deciding it is so unlikely
		   that it will quit. If required this loop variable could be
		   a value that is settable, but that appears so unnecessary that
		   I will wait until I am asked for it before implementing such
		   a process

			*/

		/* By the way, if either the distribution or the high card point
			range is given as an isolated constraint the generation of
			the hand is automatically done and testing can be done to
			avoid trying impossible constraints.

		*/
		count =0;

	do {
	  if (shape==ON) {					  /* distributional dealing */
		int result;
		result = getdist(start,care,distpick);
		if (result == 0) {
			gen_err("no distribution back from getdist");
			}
		else {
#ifdef EXCHANGING
			if (range == ON || combined == ON) {
				int count=0;
				restrict_dist(distpick);
				do {
					if ((picked_hcp = get_hcp()) == -1) {
						gen_err("Bad picked hcp from get_hcp()");
						}
					else {
						shuffling = h_and_d_shuffle(deck,starter,13,picked_hcp,distpick);
						count++;
						}
					} while (shuffling == ERR && count < 1000);
				}
			else  {
#endif
				shuffling=distdeal(gen32_deck,starter,distpick);
#ifdef EXCHANGING
				}
#endif
			if (shuffling== ERR) {
				gen_err("no proper distribution back from distdeal");
				}
			}
		}
		else if (range == ON  || combined == ON)	  /* range only */
#ifdef EXCHANGING
			exchanging(deck,starter);
#else
			hcp_range_shuffle(gen32_deck,starter,52-starter);
#endif
		else if (copy_only == ON) ;
		else gen32_shuffle(gen32_deck,starter,52-starter);

		total_points=gen32_get_hcps(gen32_deck,starter,13);
		if (specify_flag == ON && total_points >= low && total_points <= high) {
				j=0;
			for (i=starter; i<starter+13;i++)  {
				if (gen32_deck[i] / 13 == (specify_suit - 1) &&
						(k=gen32_deck[i] - (specify_suit-1)*13-8) > 0)
							j += k;
					}
				if (j<specify_low || j>specify_high) total_points = low - 1;
				}
		count++;
	} while ((total_points < low || total_points > high) && count<10000);

	if (count>=10000) {
#ifndef debugging
			gen32_shuffle(gen32_deck,starter,52-starter);
#else
#ifdef WINDOWS
		MessageBox(GetFocus(),"Insufficient points after 10000 tries","DO_GEN",MB_TASKMODAL);
#endif
		if (h_start != NULL) hand_free();
		if (start != NULL) treefree(start);
		start = NULL;
		return ERROR;
#endif
		}

	if (use_flag) ;
	else h_current = (struct hands *) malloc(sizeof(struct hands));
	if (h_current == NULL) {
#ifdef WINDOWS
			MessageBox(GetFocus(),"Insufficient memory","DO_GEN",MB_TASKMODAL);
#else
		fprintf(stderr,"\nNo more memory");
#endif
		exit(1);
		}
	else if (copy_only) ;
	else { long index;
		total_hands++;
		if (meter_parts>0) {
				index = ((long)++meter_count * 300)/ (long)meter_parts;
				if (index<=100) index = index/6;
				else if (index<=200) index = 16 + (index-100)/3;
				else if (index <= 300) index = 50 + (index-200)/2;

#ifndef GEN32
#ifdef DLL
				if (lpfnSPL != NULL) {
					if (index<0) lpfnSPL(0);
					else if (index>100) lpfnSPL(100);
					else lpfnSPL(index);
										}
#else
				if (index<0) set_percent_loaded(0);
				else if (index>100) set_percent_loaded(100);
				else set_percent_loaded(index);
#endif
#endif
				}
		}


	if (!use_flag) {
		if (h_start == NULL) {
			h_start = h_current;
			h_start->next=NULL;
			h_start->prev=NULL;
			}
		if (h_last == NULL) {
			h_last= h_start;
			}
		else {
			h_last->next=h_current;
			h_last->next->prev = h_last;
			h_last=h_current;
			}
		h_current->next=NULL;
		}


#ifdef DEXTRO_ROTATE
	if (rotate_dextro) dextro = rand() % 4;
#endif
	if (dextro > 0) do_dextro();
	for (i=0; i<52; i++)  h_current->deck[i]=gen32_deck[i];
	++handcount;

	return OK;

}			 /* of do_gen  */
#endif

extern "C" void gen32_distro(int[52], int, int, int[4]);

#ifndef CLEAN_UP
int do_print(void) {
#ifndef HOFFER
#ifdef HUGE
	struct hands huge *pr;
	struct hands huge *pr_runner;
#else
	struct hands *pr;
	struct hands *pr_runner;
#endif
	struct info itemp;
	int i;
// #ifdef __DLL__
//	int out_count=0;
// #endif
	int *deck_shuffle_stack;
	HGLOBAL hGM;
	void far *lpGM;

  if (deck_shuffle_flag) {
	deck_shuffle_count = 0;
	for (pr = p_low; pr != p_high->next && pr !=  NULL; pr = pr->next) 
		deck_shuffle_count++;
	hGM = GlobalAlloc(GMEM_MOVEABLE,deck_shuffle_count * sizeof(int));
	lpGM = GlobalLock(hGM);
	deck_shuffle_stack = (int *) lpGM;
	for (i=0;i<deck_shuffle_count; i++) 
		deck_shuffle_stack[i]=i;
	gen32_shuffle(deck_shuffle_stack,0,deck_shuffle_count);
	deck_shuffle_index=0;
	}

	if (iff_out_flag) {
		iff_out(p_low,outfp,p_high->next);
		return OK;
		}

	if (biddingflag != ON && bytesonly != ON) {
		output("\n");
		output((char *)title);
		if (title[0] != '\0') output("\n");
		}


	assert(p_low !=NULL);
	assert(p_high != NULL);
 for (pr = p_low; pr != p_high->next && pr !=  NULL; pr = pr->next) {

if (deck_shuffle_flag) {
	pr_runner = p_low;
	i = deck_shuffle_stack[deck_shuffle_index++];
	while (i--) pr_runner = pr_runner->next;
	}
else pr_runner = pr;


	for (i=0;i<52; i++) deck[i]= pr_runner->deck[i];

		if (distonly==ON) {
			gen32_distro(deck,starter,13,dist);
			for (i=0; i<4; i++)
				output("%u",gen32_dist[i]);
			output("\n");
			}
		else if (hcponly==ON) output("%u\n",total_points);
		else if (bytesonly==ON) {
			for (i=0; i<52; i++) output("%c",deck[i] + 32);
			output("\n");
			}
		else if (fillpage == ON) { pageprint(deck,fillpos); fillpos = ++fillpos % 15; }
		else if (both == ON) hand_and_bids(deck,"1N:P:3N",2);
		else {
			if (dheader == ON) output("\n\nDealer		");
			else if (rheader==ON) output("\n\nResponder    ");
			else if (gen_header==ON || view == ON) output("\n\nHand 		");
			if (dheader==ON || rheader==ON || gen_header == ON || view == ON) {
				   output("%3u ",paircount++ +1+offset);
				   output("\n----------------");
				   }


			/* Print out section for the cards. Handprint does all
			   4 hands in north, east, south, west positions.
			   Pairprint does 2 hands side by side, usually headed
			   by the dheader and rheader output. Printout prints one
			   hand only on the left margin
																	*/

			if (biddingflag == ON) ;
			else if (view==ON) {
				hand_print_setup(15,60);
				handprint(deck);
				}
			else if (pairpr)
				pairprint(deck,paircount++ + 1 + offset,1,3);
			else if (prhands==1 && intake==ON)
				printout(deck,13);
			else for (i=0; i<prhands; i++)
				printout(deck,i * 13);

			if (biddingflag == ON) {
				for (i=0; i<4; i++) {
#ifndef USE_BERGEN
					get_description(deck,i,itemp.statistics[i]);
#endif
					output("\nHAND%4u%s",D_LEN,itemp.statistics[i]);
					}


				  } 	/* bidding flag section 	 */
		}		/* normal mode as an else if */
// #ifdef __DLL__
//	if (++out_count >= max_store * divisions) break;	
// #endif
	}			/* print loop	*/

	if (deck_shuffle_flag) {
		GlobalUnlock(hGM);
		GlobalFree(hGM);
	}
// #ifdef __DLL__
//	return out_count;
// #else
#endif // of HOFFER
	return 1;
// #endif
} /* of do_print */
#endif

int get_stopper(int deck[52], int dist[4], int i, int num) {
	int max, j,k;	
		max=0;
		for (j=i*13; j < i*13+13; j++)
			if ((k=(deck[j] % 13)) > 8 && (deck[j]/13) == num) {
				max += (k-8);
				}
		if (max==3 && dist[num]==2) return 0; /* This exception found 200724! */
		max += dist[num];
		if (max > 7) return 2;
		else if (max > 4) return 1;
		else return 0;
}

int gen32_get_quicks(int deck[52], int dist[4], int i, int num) {
	int suit,quicks=0,honors[4],j,k, ans = 0;

	for (suit=0; suit<4; suit++) honors[suit]=0;
	for (j=i*13; j<i*13+num; j++)
		if ((k=(deck[j] % 13)) > 9) honors[deck[j] / 13] += (1 <<(k-10));
	for (suit = 0; suit<4; suit++) {
		switch (dist[suit]) {
			case 1: quicks = honors[suit] > 3 ? 2 : 0; break;  // stiff ace
			default: switch (honors[suit]) {
				case 0: quicks=0; break;
				case 1: quicks=0; break;
				case 2: quicks=1; break;
				case 3: quicks=2; break;
				case 4: quicks=2; break;
				case 5: quicks=3; break;
				case 6: quicks=4; break;
				case 7: quicks=4; break;
				}
			}
		ans += quicks;
		}
	return ans;
	}

int get_support_points(int deck[52], int distp[4], int i, int suit) {
#ifdef USE_BERGEN
// #pragma message("Make sure this problem has been fixed!")
	int value = gen32_bergen(deck,i*13,13,SUPPORT_OF,suit);
    // int value = gen32_bergen(deck,distp,13,SUPPORT_OF,suit);
#else
	int value = hoffer(deck,i*13,13,SUPPORT_OF,suit);
#endif
	return value;
}


int get_supported_points(int deck[52], int distp[4], int i, int suit) {
#ifdef USE_BERGEN
// #pragma message("Make sure this problem has been fixed!")
	 int value = gen32_bergen(deck,i*13,13,SUPPORTED,suit);
   //  int value = gen32_bergen(deck,distp,13,SUPPORTED,suit);
#else
	int value = hoffer(deckp,i*13,13,SUPPORTED,suit);
#endif
	return value;
}


// I know I could have made the above call this, but it is 20 years after above was written 180224
int gen32_get_suit_quicks(int deck[52], int dist[4], int i, int suit) {
	int quicks=0,honors[4],j,k;

	for (j=0; j<4; j++) honors[j]=0;
	for (j=i*13; j<i*13+13; j++)
		if ((k=(deck[j] % 13)) > 9) honors[deck[j] / 13] += (1 <<(k-10));
	switch (honors[suit]) {
		case 0: quicks=0; break;
		case 1: quicks=0; break;
		case 2: quicks=1; break;
		case 3: quicks=2; break;
		case 4: quicks=2; break;
		case 5: quicks=3; break;
		case 6: quicks=4; break;
		case 7: quicks=4; break;
		}
	return quicks;
	}

// fixed 200411 - consider all aces and king of agree suit, but agreed suit could be 4 (not a suit), so only aces are considered
int gen32_get_controls(int deck[52], int agreed_suit, int i,int num) {
	int suit,controls=0,honors[4],j,k, ans = 0;

	for (suit=0; suit<4; suit++) honors[suit]=0;
	for (j=i*13; j<i*13+num; j++)
		if ((k=(deck[j] % 13)) > 10) honors[deck[j] / 13] += (1 <<(k-11));  // only aces and kings are considered
	for (suit = 0; suit<4; suit++) {
		switch (honors[suit]) {
				case 0: controls = 0; break;
				case 1: if (suit == agreed_suit) { controls = 1; }
                        break;
				case 2: controls = 1; break;
				case 3: if (suit == agreed_suit) { controls = 2; }
					    else { controls = 1; }
                        break;
				}
		ans += controls;
		}
	return ans;
	}

#ifndef FIX_LOSERS

int get_losers(int deck[52], int dist[4], int i, int num) {
	int losers[4],suit,j,k, ans = 0;

	for (suit=0; suit<4; suit++) {
		losers[suit]=0;
		for (j=i*13; j<i*13+num; j++)
			if ((k=(deck[j] % 13)) > 9 && (deck[j]/13) == suit) {
				losers[suit] += (1 << (k-10));
				}
		switch (dist[suit]) {
			case 0: losers[suit]=0; break;
			case 1: losers[suit] = losers[suit] > 3 ? 0 : 1; break;  // stiff ace
			case 2: switch (losers[suit]) {
				case 0: losers[suit]=2; break;
				case 1: losers[suit]=2; break;
				case 2: losers[suit]=1; break;
				case 3: losers[suit]=1; break;
				case 4: losers[suit]=1; break;
				case 5: losers[suit]=1; break;
				case 6: losers[suit]=0; break;
				case 7: losers[suit]=0; break;
				}
			default: switch (losers[suit]) {
				case 0: losers[suit]=3; break;
				case 1: losers[suit]=2; break;
				case 2: losers[suit]=2; break;
				case 3: losers[suit]=1; break;
				case 4: losers[suit]=2; break;
				case 5: losers[suit]=1; break;
				case 6: losers[suit]=1; break;
				case 7: losers[suit]=0; break;
				}
			}
		ans += losers[suit];
		}
	return ans;
	}
#endif

int gen32_get_intermediates(int deck[52], int dist[4], int i, int suit) { // find number of 9,10,J,Q in a suit (intermediates)
    int j,k,ans=0;
	for (j=i*13; j < i*13+13; j++)
	   if ((k=(deck[j] % 13)) > 6 && k<11 && (deck[j]/13) == suit) ans++;
    return ans;
    }
    
int get_minor_honors(int deck[52], int dist[4], int i, int suit) { // find number of j,q,k (minor honors)
    int j,k,ans=0;
	for (j=i*13; j < i*13+13; j++)
	   if ((k=(deck[j] % 13)) > 8 && k<12 && (deck[j]/13) == suit) ans++;
    return ans;
    }

int gen32_get_suit_strength(int deck[52], int dist[4], int i, int num) {
	int j, k, count[3],jack_flag, ten_flag, max, top_three, top_five, level;	

		if (dist[num] < 4) return 0;
		else {
			for (j=0; j<3; j++) count[j]=0;
			jack_flag=OFF; ten_flag=OFF; max=0;
			for (j=i*13; j < i*13+13; j++)
				if ((k=(deck[j] % 13)) > 6 && (deck[j]/13) == num)
					switch (k) {
						case 8: ten_flag = ON; ++count[0]; break;
						case 9: jack_flag = ON; ++count[0]; break;
						case 10: case 11: case 12:
							++count[0]; ++count[1]; break;
						}

			top_three = count[1];
			if (ten_flag == ON && count[0] > 1) max += 1;
			if (jack_flag == ON && count[1] > 0) max += 1;
			top_five = max + count[1];

			level = -1;
			for (j=7; j>=0 && level == -1; j--)
				switch (j) {
					case 7: if (top_three == 3 && dist[num] > 8) level = 7;
							else if (top_three == 3 && top_five > 3 && dist[num] > 7) level = 7;
							else if (top_five == 5 && dist[num] > 5) level = 7;
							break;
					case 6: if (top_three == 3 && dist[num] > 6) level = 6;
							else if (top_five == 4 && dist[num] > 6) level = 6;
							break;
					case 5: if (top_three >1 && dist[num] > 7) level = 5;
							else if (top_five == 3 && dist[num] > 7) level = 5;
							else if (dist[num]==7 && top_five > 3) level = 5;
							break;
					case 4: if (dist[num] > 8) level = 4;
							else if (dist[num] > 7 && top_three > 1) level = 4;
							else if (dist[num] > 6 && top_three == 3) level = 4;
							else if (dist[num] > 6 && top_five > 3) level = 4;
							break;
					case 3: if (dist[num] > 7) level = 3;
							else if (dist[num] == 7 && top_five > 1) level = 3;
							else if (dist[num] == 6 && top_five > 3) level = 3;
							else if (dist[num] == 5 && top_three ==3) level = 3;
							break;
					case 2: if (dist[num] > 6) level = 2;
							else if (dist[num] == 6 && top_five > 1) level = 2;
							else if (dist[num] == 5 && top_three > 1) level = 2;
							else if (dist[num]==5 && top_five > 3) level = 2;
							break;
					case 1: if (dist[num] > 5) level = 1;
							else if (dist[num] == 5 && top_three > 0) level = 1;
							else if (dist[num] == 5 && top_five > 1) level = 1;
							else if (dist[num] == 4 && top_three > 1) level = 1;
							else if (dist[num] == 4 && top_three > 0 && top_five > 2) level = 1;
							break;
					case 0: level = 0; break;
					}

			}
		return level;
}


#ifndef USE_BERGEN

#ifdef __DLL1__
#define GET_DESCRIPTION_FUNC void FAR _export get_description(int far deck[52],int i,char far *temp)
#else
#define GET_DESCRIPTION_FUNC void get_description(int far deck[52],int i,char far *temp)
#endif
GET_DESCRIPTION_FUNC {
	char st_temp[D_LEN];
	int j,k;
	int quicks[4];
//	  char jack_flag, ten_flag, top_five, top_three, count[2];
//	  char level;

	total_points = gen32_get_hcps(deck,i*13,13);
	sprintf(st_temp,"P%02u:D",total_points);
#ifndef GEN32
	_fstrcpy(temp,st_temp);
#else
	strcpy(temp,st_temp);
#endif
	gen32_distro(deck,i*13,13,dist);
	num=0;
	for (j=0; j<4; j++) gen32_temp_dist[j]=0;
	while (num <4) {
		max = -1;
		for (j=3; j>=0; j--)
			if (gen32_temp_dist[j] == 0 && gen32_dist[j] > max) {
				max = gen32_dist[j];
				k = j;
				}
		gen32_temp_dist[k]=1;
		max = max < 10 ? max : 9;
		sprintf(st_temp,"%u%c",max, suits[k]);
		_fstrcat(temp,st_temp);
		num++;
		}
	sprintf(st_temp,":C");
		_fstrcat(temp,st_temp);
	for (j=0; j<4; j++) {
		sprintf(st_temp,"%u",gen32_dist[j] < 10 ? gen32_dist[j] : 9);
		_fstrcat(temp,st_temp);
	}
	sprintf(st_temp, ":L%02u",gen32_getLTC(deck,i*13,13));
		_fstrcat(temp,st_temp);
	sprintf(st_temp,":H");
		_fstrcat(temp,st_temp);
	num=0;
	while (num <4) {
		max=0;
		for (j=i*13; j < i*13+13; j++)
			if ((k=(deck[j] % 13)) > 8 && (deck[j]/13) == num) {
				max += (1 << (k-9));
				}
		sprintf(st_temp,"%02u%c",max, suits[num]);
		_fstrcat(temp,st_temp);
		num++;
		}
#ifdef USE_BERGEN
	sprintf(st_temp,":R%02u",gen32_bergen(deck,i*13,13,1,0));
#else
	sprintf(st_temp,":R%02u",gen32_roth(deck,i*13,13,1,0));
#endif
		_fstrcat(temp,st_temp);
	num=0;
	while (num < 4) {
		sprintf(st_temp, "%02u%c",gen32_roth(deck,i*13,13,2,num),suits[num]);
		_fstrcat(temp,st_temp);
		num++;
		}
	_fstrcat(temp,":Q");
	max=0;
	for (num=0; num<4; num++) {
		quicks[num]=0;
		for (j=i*13; j<i*13+13; j++)
			if ((k=(deck[j] % 13)) > 10 && (deck[j]/13) == num) {
				quicks[num] += k-10;
				max += k-10;
				}
		}

	sprintf(st_temp,"%02u",max);
		_fstrcat(temp,st_temp);
	num=0;

	for (num=0; num<4; num++) {
		sprintf(st_temp,"%02u%c",quicks[num],suits[num]);
		_fstrcat(temp,st_temp);
		}

/* added to show stoppers just before the release of 1.3 
	8/5/97 note: 1.3 was not released until 11/97 */

	_fstrcat(temp,":S");

	for (num=0; num<4; num++) {
		sprintf(st_temp,"%u%c",get_stopper(deck,dist,i,num),suits[num]);
		_fstrcat(temp,st_temp);
		}
/* added to show suit strength 3/14/98 */
	_fstrcat(temp,":G");
	for (num=0; num<4; num++) {
		sprintf(st_temp,"%u%c",gen32_get_suit_strength(deck,dist,i,num),suits[num]);
		}
}
#endif

// #pragma argsused
void my_newgen_error(char *str) {
}

#ifndef CLEAN_UP
#ifndef HOFFER
#ifdef __DLL__
#define EVAL_FUNC void FAR eval_command(LPSTR s)
#else
#define EVAL_FUNC void eval_command(LPSTR s)
#endif
#else
#define EVAL_FUNC void eval_command(char *s)
#endif
EVAL_FUNC {
#ifndef HOFFER
	int count,i,j,temp_count,cbRead;
	char c;
#ifdef HUGE
	struct hands huge *hptr;
#else
	struct hands *hptr;
#endif
	char szSrc[] = {"tempout"};
	char *szDst = NULL;
	char com_line[255];
	char outfilename[255];
	char infilename[255];

	while (*s != '\0') {
		count = 1;
		switch(toupper(*s)) {
		   case '?': explain(); generate_flag=OFF; print_flag= OFF; break;

		   case 'N': specify_flag = ON;
					 s++;
					 specify_suit = *s - '0';
					 s++;
					 if (specify_suit < 1 || specify_suit>4) specify_flag=OFF;
					 else count = get_range(&specify_low,&specify_high,0,10,0,10,s);
					 if (count == 0) specify_flag = OFF;
					 break;
		   case 'A': prhands=0;
					 prhands= *(s+1) - '0';
					 if (prhands < 1 || prhands > 4 ) prhands=0;
					 else count = 2;																	 
					 break;
		   case 'D': dextro=0;					 /* dextro, ie rotate right */
					 dextro= *(s+1) - '0';
#ifdef DEXTRO_ROTATE
					 if (dextro == 5) rotate_dextro = ON;
					 else rotate_dextro = OFF;
#endif
					 if (dextro < 1 || dextro > 4 ) dextro=0;
					 count = 2;
					 break;

#ifdef DEXTRO_ROTATE
		   case ';': match_rotation = ON;
					 break;

#endif
		   case 'C':
					 count = get_range(&c_low,&c_high,0,HIGH,0,HIGH,++s);
					 combined = count > 0 ? ON : OFF;									 
					 if (combined) { build_tree_flag = ON; range = OFF; }
					 break; 																		

		   case 'J': if (both==ON) both=OFF; else both=ON; break;
		   case 'F': ff_flag = ff_flag ? OFF : ON; break;

		   case 'P': print_range=ON;
					 count = get_range(&print_low, &print_high,0,HIGH,0,HIGH,s+1);
					 if (count == 0) {
					   print_range=OFF;
					   set_print_flags(*(s+1));
					   count=2;
					   break;
					   }
					 hptr=h_start; i=0;
					 while (hptr) {
						i++;
						if (i==print_low) { p_low=hptr; break; }
						hptr=hptr->next;
						}
					 while(hptr) {
						if (i==print_high) { p_high=hptr; break; }
						hptr=hptr->next;
						i++;
						}
					 if (p_low == NULL) p_low = h_start;
					 if (p_high == NULL) p_high = hptr;
					 break;

		   case 'R': range =ON;
					 count = get_range(&low,&high,0,37,0,37,s+1);
					 if (count == 0) {
						range = OFF;
						build_tree_flag=OFF;
						combined = OFF;
						}
					 else build_tree_flag=ON;
					 break; 																		

		   case 'H': count += get_string(title,s+1); break;

		   case '%': bugsflag=ON; break;													

		   case 'U': pairpr=1; view=OFF; bytesonly=OFF; hcponly=OFF;
					 dheader = gen_header = rheader = OFF;
					 distonly=OFF;
					 if (s[count+1] == '2') { count++; pairpr=2; }
					 else if (s[count+1] == '3') { count++; pairpr=3; }
					 break;

		   case '&': dataout=ON; break;

		   case '!': while ((c =  toupper(s[count])) != ' ' && c != '\0') {
					   switch(c) {																	
						   case 'G': generate_flag=OFF; break;
						   case 'P': print_flag=OFF; break; 										
						   default: break;															
						   }																		
					   ++count; 																	
					   }																			
					  break;

		   case '#':  if (*(s+1) == '#') {
						count += get_name(infilename,s+2);
						count += get_name(outfilename,s+count+2);
						 read_file((char *)infilename,(char *)outfilename);
						}
					 else {
						count += get_name(filein,s+1);
					 read_file((char *)filein,(char *)"");
						}
					 break;

		   case '|': copy_only = ON; print_flag = OFF; break;

		   case '*': count += get_name(filein,s+1);
					 write_file(filein); break;

		   case '~': count += get_name(filein,s+1);
					 write_matt(filein);
					 break;

		   case '=': count += get_name(filein,s+1);
					szDst = filein;
#ifndef GEN32
					{
					HFILE hfSrcFile, hfDstFile;
										OFSTRUCT ofStrSrc, ofStrDest;

					hfSrcFile = LZOpenFile(szSrc, &ofStrSrc, OF_READ);
					hfDstFile = LZOpenFile(szDst, &ofStrDest, OF_CREATE);
					LZCopy(hfSrcFile, hfDstFile);
					LZClose(hfSrcFile);
					LZClose(hfDstFile);
										}
#else
					CopyFile((LPCTSTR)szSrc,(LPCTSTR)szDst,0);
#endif
					break;

		   case '.': count += get_name(filein,s+1);
#ifndef GEN32
if ((comfp=OpenFile(filein,(LPOFSTRUCT) &In_Of,OF_READ)) == -1) {}
#else
if ((comfp=MyOpenFile(filein,MY_RDONLY)) == MY_IO_ERROR) {}
#endif
	 else {
		i=0;
		do {
			cbRead = _lread(comfp, &com_line[i], 255-i);
			if (cbRead != 0) {
			   do {
				  for (j=i; j<255 && j-i<cbRead; j++)
					if (com_line[j]== 13) break;
				  com_line[j] = '\0';
				  eval_command((char *)&com_line[i]);
				  i = j+2;
				  } while (i < cbRead);
				for (j=i; j<255; j++) com_line[i++]=com_line[j];
				}
			} while (cbRead != 0);
		_lclose(comfp);
		}
				break;

		   case '>': count += get_name(filein,s+1);
					 strcpy(fileout,"out.pbn");
					 write_pbn(filein,fileout);
					 break;

		   case '<': count += get_name(filein,s+1);
					 strcpy(fileout,"out.txt");
					 read_pbn(filein,fileout);
					 break;

#ifdef BINARY_FILE
		   case ':': count += get_name(filein,s+1);
					 write_binary_file(filein);
					 break;

#endif
		   case 'I': count += get_name(filein,s+1);
					  if (infp) { _lclose(infp); intake = OFF; infp = -1;}
					 if (count>1) {
						   intake=ON; innum=1; readflag = ON;
#ifndef GEN32
						   if ((infp=OpenFile(filein,(LPOFSTRUCT) &In_Of,OF_READ)) == -1)
#else
						   if ((infp=MyOpenFile(filein,MY_RDONLY)) == MY_IO_ERROR)
#endif
                                                                                        {
#ifdef WINDOWS
	MessageBox(GetFocus(),"Cant open ",filein,MB_TASKMODAL);
#else									
							   fprintf(stderr,"\nCan't open %s",filein);
#endif								
							   intake=OFF;															
							   }																	
						   }																		
					 break;

		   case '@': gen_init2();
					 if (outfp) { _lclose(outfp); outfile=OFF; outfp=0;}
					 if (infp) { _lclose(infp); intake = OFF; infp = 0;}
					 break; 						/* reinitialize everything */
		   case 'Q': if (outfile) {
						_lclose(outfp);
						outfile = OFF;
						}
					  break;
		   case 'K': divisions=1;					
					 divisions= *(s+1) - '0';
					 if (divisions < 1 || divisions > 4 ) divisions=1;
					  break;


		   case 'E': count += get_name(fileout,s+1);
					 if (outfp) { _lclose(outfp); outfile=OFF; outfp= -1;}
					 if (count >1) {
						outfile=ON;
						toprinter=OFF;
#ifndef GEN32
						if ((outfp=OpenFile(fileout,(LPOFSTRUCT)&Out_Of,OF_READWRITE)) == HFILE_ERROR)
#else
						if ((outfp=MyOpenFile(fileout,MY_RDWR)) == HFILE_ERROR)
#endif
                                                                                  {      
#ifdef WINDOWS
	MessageBox(GetFocus(),"Cant open file",fileout,MB_TASKMODAL);
#else
						  fprintf(stderr,"Can't open output file %s",fileout);
#endif
						  outfile=OFF;
						  }
						}

#ifndef NEWIO
					 if (lseek (outfp,0L, SEEK_END) == -1) {
#else
					 if (MySeek(outfp,0L,SEEK_END) == -1) {
#endif


#ifdef WINDOWS
	MessageBox(GetFocus(),"Cant seek for append file",fileout,MB_TASKMODAL);
#else
						  fprintf(stderr,"Can't seek for append file %s",fileout);
#endif
						  outfile=OFF;
						  }

					 break;

		   case 'O': count += get_name(fileout,s+1);
					 if (outfp) { _lclose(outfp); outfile=OFF; outfp= -1;}
					 if (count >1) {
						outfile=ON;
						toprinter=OFF;
#ifndef GEN32
						if ((outfp=OpenFile(fileout,(LPOFSTRUCT)&Out_Of,OF_CREATE | OF_READWRITE)) == -1)
#else
						if ((outfp=MyOpenFile(fileout,MY_CREATE | MY_RDWR)) == MY_IO_ERROR)
#endif
                                                                                                {
#ifdef WINDOWS
	MessageBox(GetFocus(),"Cant open file",fileout,MB_TASKMODAL);
#else
						  fprintf(stderr,"Can't open output file %s",fileout);
#endif
						  outfile=OFF;
						  }
						}

					 break;

		   case 'V': count += set_validate_flag(s+1); break;

		   case 'X': temp_count = get_setting(&myuse,0,1000,s+1);
					 use_flag = temp_count > 0 ? ON : OFF;
					 if (temp_count == 0 && toupper(*(s+1)) == 'A') {
						autoinc_flag=ON;
						count =2;
						}
					 if (use_flag == OFF) { starter = 0; break; }
					 else h_current = h_start; // point to first hand
					 build_tree_flag=ON;
					 count += temp_count;
					 hptr=h_start; i=1;
					 while (i < myuse && hptr != NULL) {
						hptr = hptr->next;
						i++;
						}
					 if (hptr) for (i=0; i<52; i++)
									h_use.deck[i]=hptr->deck[i];

					 break;


		   case '^':
#ifdef DEXTRO_ROTATE
					if (match_rotation == ON) dealer=0;
					 else {
#endif
						dealer = *(s+1) - '0';
						if (dealer == 5) rotate_dealer = ON;
						else rotate_dealer = OFF;
#ifdef DEXTRO_ROTATE
						}
#endif
					if (dealer < 0 || dealer > 3) dealer = 0;
					 vully = *(s+2) - '0';
					 if (vully == 5) rotate_vul = ON;
					 else rotate_vul = OFF;
					 if (vully < 0 || vully > 3) vully = 0;
					 count = 3;
					 break;

		   case 'S': count += get_shape(s+1); break;

		   case 'M': count += get_setting(&mymax,0,HIGH,s+1);
					 if (mymax > 1) {
						meter_parts = mymax * divisions * 3;
						meter_count = 0;
						max_store = mymax;
						}
					 if (mymax == 0) 
						mymax = max_store;

					  break;
		   case 'L': count += get_setting(&offset,0,HIGH,s+1);
					  break;

		   case '$': deck_shuffle_flag = ON; break;
		   case 'G': graphic=ON; break; 															

		   case 'B': biddingflag=ON; view=ON; break;

		   case 'T': twohandin=ON; starter=26; readflag=ON; 											
					 break; 																		

		   case 'W':

					 count += get_setting(&wait_time,0,37,s+1);
#ifdef EXCHANGING
					 evaluate_hcp(wait_time);
					 wait_time = 0;
#endif
					 break;

#ifndef PRODUCTION_VERSION
		   case 'Y': do_debug(*(s+1)); break;
#endif
		   case 'Z': if (h_start != NULL) hand_free();
					if (start != NULL) treefree(start);
					 start = NULL;
					 print_range=OFF;
					 use_flag=OFF;
					 total_hands=0;
					 break;
		   default:  break;
		}
	count = count <= 0 ? 1 : count;
	s += count;
	}
#endif // of HOFFER
}
#endif

#ifndef CLEAN_UP
void explain(void) {
		output("\nC 	- combined point count (given for third hand);");
		output("\nR 	- set range of high card points; ex. r16-18");
		output("\nPx-y	- print hands held in the buffer ex. p1-3");
		output("\nPN	- print out the hand normally.");
		output("\nPL	- print out the distribution only");
		output("\nPH	- print out the high card points only");
		output("\nPB	- print out the bytes only as a 52 byte array");
		output("\nPI	- print out an iff file of the hands");
		output("\nPD	- print out with dealer header");
		output("\nPE	- print out with responder header");
		output("\nPA	- print out with numbered header");
		output("\nPP	- send output to printer");
		output("\nP$	- toggle the command line prompt");
		output("\nPC	- toggle carriage return after line feed");
		output("\nPV	- view mode");
		output("\nPF	- fill page");
		output("\n# 	- read_file (txt->iff default=tempout)");
		output("\n* 	- write_file (iff->txt default=tempout2)");
		output("\n& 	- dataout variable turned on");
		output("\n: 	- write_binary_file");
		output("\n> 	- write_pbn (iff->pbn default=out.pbn)");
		output("\n< 	- read_pbn (pbn->txt default=out.txt)");
		output("\n~ 	- write_matt(iff->matt default=matt.out)");
		getchar();

		output("\nF 	- send form feed after printing a section");
		output("\nH 	- add a quote delimited string as title");
		output("\nU 	- pair print mode");
		output("\nI 	- input a hand from a file ifile");
		output("\nO 	- output the hand to a file ofile");
		output("\nL 	- offset to be added to hand number");
		output("\nS 	- shape, give the distribution s4333 or s4333,4432");
		output("\nSU	- shape, unique for distribution su3433");
		output("\nA 	- print out a particular hand a1");
		output("\nM 	- use this as the maximum number of hands");
		output("\nG 	- graphic output");
		output("\nS     - specify HCPs for a particular suit-e.g.,N15-5 sets 5 HCPS in clubs");
		output("\nB 	- output to be used as input for bidding system");
		output("\nQ 	- quit (close) the open output file");
		output("\nT 	- twohands to be readin, used with i option");
		output("\nX 	- use a buffer hand, rather than reading in a file");
		output("\nXA	- toggle autoincrement when using buffers");
		output("\n!pg	- temporarily turn off generate or printing sections");
		output("\nW 	- wait a specified number of seconds");
		output("\nZ 	- zap the hands currently stored");
		output("\n@ 	- reinitialize every flag and global");
	}
#endif

int get_range(int *l, int *h, int dl, int dh, int ml, int mh, char *s) {
	int i;

	*l=dl; *h=dh;
	i=0;

	if (dl > dh) return ERROR;
	if (ml > mh) return ERROR;

	if (!isdigit(s[i])) return 0;
	else *l=0;

	while (s[i] != '-' && isdigit(s[i]) && *l <38)
		*l = (10 * *l) + s[i++] - '0';

	if (s[i]=='-') {
				*h=0; ++i;
				while ( isdigit(s[i]) && *h < 38)
					*h=(10 * *h) + s[i++] - '0';
				}
	else *h=*l;    /* maybe this should default to dh?...
					  this seems more appropriate. */


	if (*l < ml) *l = ml;
	if (*h > mh) *h = mh;
	if (*l > *h) *l = dl;
	if (*h < *l) *h = dh;

	return i;
}

int set_print_flags(char c) {
	switch (toupper(c)) {
	  case 'P': iff_out_flag=OFF;
		if (toprinter==ON) toprinter=OFF;
		else toprinter=ON;
		break;
	  case 'F': iff_out_flag=OFF;
		if (fillpage==ON) fillpage=OFF;
		else { fillpage=ON; fillpos=1;}
		break;
	  case 'C': iff_out_flag=OFF;
		if (crlf) crlf = OFF;
		else crlf=ON;
		break;
	  case 'N': iff_out_flag=OFF;
		// normal=ON;
        distonly=OFF; hcponly=OFF; bytesonly=OFF;
		view=OFF; pairpr = 0;
		break;
	  case 'S': iff_out_flag=OFF;
		distonly=ON; hcponly=OFF; bytesonly=OFF;
		break;
	  case 'H': iff_out_flag=OFF;
		hcponly=ON; distonly=OFF; bytesonly=OFF;
		break;
	  case 'B': iff_out_flag=OFF;
		bytesonly=ON; distonly=OFF; hcponly=OFF;
		break;
	  case 'D': iff_out_flag=OFF;
		dheader=ON; rheader=OFF; gen_header = OFF;
		distonly=OFF; bytesonly=OFF; hcponly=OFF; view=OFF;
		break;
	  case 'A': iff_out_flag=OFF;
		dheader=OFF; rheader=OFF; gen_header = ON; pairpr = 0;
		distonly=OFF; bytesonly=OFF; hcponly=OFF; view=OFF;
		break;
	  case 'I': iff_out_flag=ON;
				distonly=OFF; hcponly=OFF; bytesonly=OFF;
				break;
	  case 'R': iff_out_flag=OFF;
		rheader=ON; dheader=OFF; gen_header=OFF;
		distonly=OFF; bytesonly=OFF; hcponly=OFF; view=OFF;
		break;
	  case 'V': view=ON;  iff_out_flag=OFF;
			distonly=OFF; hcponly=OFF; bytesonly=OFF;
		break;
	  case '$':
		if (prompt_flag) prompt_flag=OFF;
		else prompt_flag = ON; 
		break;
	  default: break;
	  }
	 return 1;
}

int get_string(char *out, char	*s) {
	int count,index;

	count=0; index=0;
	*out = '\0';
	while(*s != '"' && *s != '\0') { s++; count++; }
	if (*s != '"') return 0;
	s++; count++;
	while ((out[index] = *s++) != '"' && count < 80)
			if (out[index++]=='\0') break;
	if (count == 80) return 0;
	count++;
	out[index] = '\0';
	return count+index;
}

int get_name(char *out, char *s) {
	int count;
	char *temp;

	temp=s;
	count=0;
	while (*s != '\0' && *s != ' ' && *s != '\t' && *s != 10 && *s != 13)
			out[count++] = *s++;
	out[count]='\0';
	s=temp;
	return count;
}

#ifndef CLEAN_UP
int set_validate_flag(char *s) {
#ifndef HOFFER
static OFSTRUCT Of;
static char szFormat[60];
int count, valid;
int i;
char c;
#ifdef HUGE
struct hands huge *hptr;
#else
struct hands *hptr;
#endif

	count=1;

	switch (toupper(*s)) {
	  case 'R': 	   /* set random number seed */
		i=0;
		while (isdigit(s[count]))
			i=(i * 10) + s[count++] - '0';
		srand(i);
		for (i=0;i<52;i++) deck[i]=i;
		position=0;
		break;
	  case 'F': 	   /* set validation file */
		count += get_name(vfilein,s+1);
		break;
	  case 'C': 	   /* set validation comment */
		count += get_string(vcomment,s+1);
		break;
	  case 'W': 		/* write to validation file */
#ifndef GEN32
		  if ((infp= OpenFile(vfilein,(LPOFSTRUCT) &Of,OF_WRITE)) == -1) break;
#else
		  if ((infp= MyOpenFile(vfilein,MY_WRONLY)) == MY_IO_ERROR) break;
#endif
		  hptr = h_start;
		while (hptr) {
			for (i=0; i<52; i++) {
				sprintf(szFormat,"%c",hptr->deck[i]);
				_lwrite(infp,szFormat,1);
				}
			hptr= hptr->next;
			}
		_lclose(infp);
		break;
	  case 'V': 		/* do validation; exit on err with comment */
#ifndef GEN32
		  if ((infp=OpenFile(vfilein,(LPOFSTRUCT) &Of,OF_READWRITE)) == HFILE_ERROR) break;
#else
		  if ((infp=MyOpenFile(vfilein,MY_RDWR)) == MY_IO_ERROR) break;
#endif
		  valid = ON;
		hptr = h_start;
		while (hptr != NULL) {
			for (i=0; i<52; i++) {
				_lread(infp,szFormat,1);
				c = szFormat[0];
				if (c != hptr->deck[i]) { valid=OFF; goto myclose; }
				}
			hptr=hptr->next;
			}
		myclose: _lclose(infp);
		if (valid == OFF) {
#ifdef WINDOWS
	MessageBox(GetFocus(),"Validation error","SET_VALID_FLAG",MB_TASKMODAL);
#else
			fprintf(stderr,"\nValidation error\n");
			fprintf(stderr,vcomment);
			fprintf(stderr,"\n\n");
#endif
			exit(1);
			}
	  default:
		break;
	  }
	return count;
#else
    return 0;
#endif
}
#endif

int get_shape(char *s) {
  int count,tot;
  int i,k;

  count=0;
  care=OFF;
  if (toupper(s[count])=='U') {care=ON; count++; }
  else if (!isdigit(s[count])) {shape=OFF; return 0;}

	i=0;  shape=ON; distmax=0;
	build_tree_flag=ON;
	do {
		while (isdigit(s[count]))
			distmem[distmax][i++]=s[count++] - '0';
		if (i != 4) { shape=OFF; break; }
		tot=0;
		for (k=0; k<4; k++) tot = tot + distmem[distmax][k];
		if (tot != 13) { shape=OFF; break; }
		i=0; distmax++;
		} while (s[count++] == ',' && shape == ON && distmax < DISTMAX-1);
  if (count > 0) count--;
  return count;
}

int get_setting( int *out,int min ,int max ,char *in) {
int count;

*out =0; count=0;
while (isdigit(in[count]))
	*out = (*out * 10) + in[count++] - '0';
*out = *out < min ? min : *out;
*out = *out > max ? max : *out;
return count;
}

void hand_free(void) {
#ifdef HUGE
	struct hands huge *temp1;
	struct hands huge *temp2;
#else
	struct hands *temp1;
	struct hands *temp2;
#endif
	temp1 = h_start;
	while (temp1) {
		temp2 = temp1->next;
		free((void *)temp1);
		temp1 = temp2;
		}
	h_start = h_current = h_last = NULL;
}

long get_print_count(void) {
#ifdef HUGE
	struct hands huge *temp_ptr;
#else
	struct hands *temp_ptr;
#endif
	long count=0;

	temp_ptr=h_current;
	while (temp_ptr != NULL) {
		temp_ptr = temp_ptr->prev;
		count++;
		}
	return count;
}


void set_print_range(long index) {
#ifdef HUGE
	struct hands huge *temp_ptr;
#else
	struct hands *temp_ptr;
#endif

	temp_ptr=h_current;
// #ifdef __DLL__
//	if (twohandin==ON) index *= (hands_read/2);
//	else if (intake==ON) index *= hands_read;
// #endif
	while (index !=0 && temp_ptr != NULL) {
		p_low = temp_ptr;
		temp_ptr = temp_ptr->prev;
		index--;
		}
	p_high=h_current;
}

#ifndef CLEAN_UP
#ifndef PRODUCTION_VERSION
void do_debug(char c) {

	switch (toupper(c)) {
		case 'D': debug_dist(start); break;
		default:  break;
		}
}
#endif
#endif


#ifdef __DLL__
// #pragma argsused
#endif
extern void gen_err(char *err_msg) {
#ifndef __DLL__
#ifdef _Windows
	MessageBox(GetFocus(),err_msg,"Generate Error",MB_ICONSTOP);
#else
	fprintf(stderr,"%s\n",err_msg);
#endif
#endif
}

#ifndef CLEAN_UP
#ifndef HOFFER
#ifdef GEN32
int process(int) 
#else
#ifdef __DLL__
void PASCAL _export process(FPSTRCB lpfnSPL, char *str)
#else
void FAR process(void)
#endif
#endif
#else
extern int process(char *str)
#endif

{
#ifdef STAND_ALONE
	int done = NO;
	int process_debug = FALSE;
	char my_inline[255];
#else
	char *my_inline;
#endif
	long num_generated, total_generated;
#ifndef WIN32
    fprintf(stderr,"command is %s\n",str);
#endif

#ifndef STAND_ALONE
	if (!gen_initialized) {
#ifdef TEST_IO
		test_io();
#endif
        fprintf(stderr,"calling initialize\n");
		initialize();
		gen_initialized = TRUE;
		}
	my_inline = NULL; // (char *) str;
#endif

	/* This is really a state machine. The state is set with global flags.
	   All flags remain unchanged except for print and generate which are
	   represented by print_flag and generate_flag respectively. These will
	   be reset upon each iteration of the main loop.

	   The loop accomplishes the following:
		1. Reset the flags...generate and print.
		2. Prompt for input
		3. Get a line of input commands.
		4. Evaluate the input line and set states (global variables as per
			the commands on the input line.
		5. Generate the hands and put them into a linked list until the
			number of generated hands exceeds mymax.
		6. Print out the hands that have thus been generated.

	*/

	h_start=NULL; h_last=NULL;
#ifndef EXCHANGING
	if (ready() == ERR) {
		printf("ready failed");
		exit(0);
		}
#endif

#ifdef STAND_ALONE
	while (done==NO) {
#endif
		generate_flag = ON; handcount=0; paircount=0;

#ifdef STAND_ALONE
		if (prompt_flag) printf("\n$>");
		if (gets(my_inline)==NULL) { done=YES; generate_flag=OFF;print_flag=OFF; }
#endif
#ifdef HOFFER
		if (strcmp(my_inline,"exit")==0) {
			if (h_start != NULL) hand_free();
			if (start != NULL) treefree(start);
			start = NULL;
			if (outfp) fclose((FILE *)(long)outfp);
			if (infp) fclose((FILE *)(long)infp);
			return 0;
			}
#endif
#ifdef STAND_ALONE
		if (_fstricmp(my_inline,"@")==0)
#else
		if (1)
#endif
                                {
			if (outfile)
				MyClose(outfp);
			outfp= -1;
			outfile = OFF;
			if (intake)
				MyClose(infp);
			intake=OFF;
			infp= -1;
            fprintf(stderr,"calling n_fw\n");
			gen_init2();
#ifdef STAND_ALONE
			}
		else {
#endif
			specify_flag = OFF;
			eval_command(my_inline);
			total_generated = 0;
			if (generate_flag) {   // sometimes generation of hands does not occur.
				while (1) {
					if (handcount >= mymax) {	   // go until handcount reaches mymax
						if (intake==ON) readflag=ON; // read another line
						else break; 				 // done
						}
					if ((num_generated = (long) do_gen()) == 0) break;
					else total_generated += num_generated;
					}
				}
			if (print_flag) {
				if (!print_range)
					set_print_range(total_generated);
				do_print();
				if (ff_flag) output("\f");
				}
			if (h_start != NULL) hand_free();
/*			if (start != NULL) treefree(start); start = NULL;   */
			}

#ifdef STAND_ALONE
		}
#else
    return 1;
#endif
}					/* of process routine */
#endif
#endif
