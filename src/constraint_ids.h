//
//  constraint_ids.h
//  gen
//
//  Created by Rodney Ludwig on 1/24/20.
//  Copyright © 2020 Rodney Ludwig. All rights reserved.
//

#ifndef constraint_ids_h
#define constraint_ids_h


#define ID_HCP 0
#define ID_DIST 1
#define ID_LEN 2
#define ID_AND 3
#define ID_OR 4
#define ID_SUM 5
#define ID_SUB 6
#define ID_DIFF 7
#define ID_VALUE 8
#define ID_HAS 9
#define ID_NOT 10 // used in test_stoppers
#define ID_XOR 11
#define ID_QUALITY 12
#define ID_STOPPER 13
#define ID_ROTH 14
#define ID_IF 15
#define ID_COMPARE 16
#define ID_LESS 17
#define ID_MORE 18
#define ID_LESS_OR_EQ 19
#define ID_MORE_OR_EQ 20
#define ID_EQUAL 21
#define ID_HASNOT 22 // used in id_stoppers also
#define ID_ACES 23
#define ID_ORANDS 54
#define ID_PLACING_CARDS 58
#define ID_OVERRIDE 59
#define ID_NOP 60 // must be the highest ID
#define ID_NOP_CONSTRAINTS 51
#define ID_NOP_DIR 52
#define ID_LOSERS 33
#define ID_SUIT_QUICKS 31
#define ID_QUICKS 32
#define ID_INOUT 34
#define ID_FORCE 41
#define ID_SIMULATE 42
#define ID_CONTROLS 49
#define ID_BERGENS 48
#define ID_OUT 50
#define ID_ODD 35
#define ID_EVEN 36
#define ID_PLAYED 37
#define ID_VOID 24
#define ID_INTERMEDIATES 44
#define ID_OD_OVER 45
#define ID_OD_UNDER 46
#define ID_REMOVE 47
#define ID_SUPPORT 25
#define ID_SUPPORTED 26
#define ID_SINGLETON 27
#define ID_KNOW 28
#define ID_RESET 99
#define ID_DEAL 98
#define ID_GET_HAND 97
#define ID_UNITY_TESTS 96
#define ID_SHUFFLE 95
#define ID_PRINT_HANDS 94
#define ID_ROD2 93
#define ID_GET_PLAYING_HAND 92
#define ID_PRINT_PLAYING_HANDS 91
#define ID_VERSION 90
#define ID_PRINT_CONSTRAINTS 89
#define ID_GET_COUNT 88
#define ID_GET_LEAD 78
#define ID_SET_CONTRACT 81
#define ID_SET_CSW 85
#define ID_DEBUGGING 84

#define ID_SET_EXCHANGES 83
#define ID_SET_HOOL_EXCHANGES 82
#define ID_PASS_OR_DOUBLE 86
#define ID_SET_RAVEN_EXCHANGES 80
#define ID_SET_NORMAL_EXCHANGES 79
#define ID_COVERAGE 77 // FIXED this was set to 78 which is ID_GET_LEAD
#define ID_TEST_SHOWS 64
#define ID_HOFFER_EVALUATE 501
#define ID_HOFFER_BID 502
#define ID_HOFFER_CONSTRAINTS 503

// TRIGGERS
#define ID_GENERAL 1500
#define ID_LEADS 1499
#define ID_BID_ALL 1498
#define ID_BID_AMZI 1497
#define ID_INITIALIZE 1496
#define ID_MACROS 1495

#endif /* constraint_ids_h */
