//
//  prefix.pch
//  bidlibc
//
//  Created by Rodney Ludwig on 9/13/20.
//  Copyright © 2020 Rodney Ludwig. All rights reserved.
//

#ifndef prefix_h
#define prefix_h
#undef VOLUBLE
#undef OK_FOR_SCOTT
#ifdef OK_FOR_SCOTT
#define NO_CSW
#endif

#if defined(ANDROID) || defined(__ANDROID__) || defined(linux)
#define NO_CSW
#endif

// Statements like:
// #pragma message(Reminder "Fix this problem!")
// Which will cause messages like:
// C:\Source\Project\main.cpp(47): Reminder: Fix this problem!
// to show up during compiles. Note that you can NOT use the
// words "error" or "warning" in your reminders, since it will
// make the IDE think it should abort execution. You can double
// click on these messages and jump to the line in question.

#define Stringize( L )     #L
#define MakeString( M, L ) M(L)
#define $Line MakeString( Stringize, __LINE__ )
#define Reminder __FILE__ "(" $Line ") : Reminder: "


#if !defined(linux)
#define IOS
#define MYLEAD
#endif

#if !defined(IOS) && !defined(linux)   // eliminate amzi for testing 210403
#pragma message("IOS and linux are not defined")
#define AMZI
#define AMZI_THERE
#define AMZI_PLAY
#endif

#define ELIMINATE_ALL_AMZI_FOR_IOS

#if defined(ELIMINATE_ALL_AMZI_FOR_IOS)
#undef AMZI
#undef AMZI_THERE
#undef AMZI_PLAY
#undef AMZI_BID
#undef AMZI_GET_A_LEAD
#else
#define AMZI
#define AMZI_THERE
#define AMZI_PLAY
#define AMZI_BID
#define AMZI_GET_A_LEAD
#endif

/* this section allows turning AMZI back on oneby-one */
#undef TURN_BACK_ON_BY_SECTIONS

#if defined(TURN_BACK_ON_BY_SECTIONS)
#define AMZI_GET_A_LEAD
#undef AMZI_BID
#if defined(AMZI_GET_A_LEAD)
#if !defined(IOS)
#define AMZI_THERE
#endif
#endif
#endif


#if !defined(linux)
#define NO_ASSERTS
#endif


#ifndef NO_ASSERTS
#include "assert.h"
#endif


#if defined(OK_FOR_SCOTT) && !defined(NO_CSW)
#pragma message(ERROR "should exclude CSW on production")
#endif
#if defined(OK_FOR_SCOTT) && (!defined(AMZI_THERE) || !defined(AMZI_PLAY))
#pragma message(ERROR "should have amzi on")
#endif

// #pragma message("pre-compilation in progress")
#if !defined(ANDROID) && !defined(__ANDROID__) && !defined(linux)
#if defined(AMZI)
#if defined(VOLUBLE)
#pragma message("amzi is defined")
#endif
#endif
#include "hoffer.h"
#else
#define OSX
#define HOFFER
#endif

#define EXTERN_HOFFER_C extern


#ifdef NO_CSW
#undef CSW
#if defined(VOLUBLE)
#pragma message("CSW is not being used")
#endif
#define CSW_EXEC(x)  ((void)0)
#define TRIGGER(x,y) ((void)0)
#define ONE_SHOT(x,y) ((void)0)
#define SINGLETON(x,y) ((void)0)
#define CHECK_STATE(x,y) ((void)0)
#define CPP_DO_STATE(x) ((void)0)
#define DO_STATE(x) ((void)0)
#undef USE_STATE

#else
#define CSW
#if defined(VOLUBLE)
#pragma message("CSW is being used and its macros are set in pre-compiled header")
#endif
#define CSW_EXEC(x)  (csw_ison(__FILE__,__LINE__) ? (x) : (0))
#define TRIGGER(x,y) (trigger(x) ? (y) : (0))
#define ONE_SHOT(x,y) (trigger(x) ? (y) : (0))
#define SINGLETON(x,y) (trigger(x) ? (y) : (0))
#define CHECK_STATE(x,y) (check_state(x) ? (y) : (0))
#define CPP_DO_STATE(x) cpp_do_state(x)
#define DO_STATE(x) do_state(x)
#define USE_STATE

void do_state(unsigned int);
unsigned int get_state(void);
unsigned int check_state(unsigned int);


#ifdef __cplusplus
extern "C" {
#endif

int csw_ison(const char *, int);
int trigger(int);
void getargs(int, char **);
int check_one_shots(int,int);
int getargs2(const char *);
int set_trigger(const char *);
int set_one_shot(const char *);
int set_singleton(const char *);
void clear_csw(void);
void pop_csw(void);
void clear_triggers(void);
void push_triggers(void);
void pop_triggers(void);
void print_triggers(void);
    int check_trigger(int);
    void cpp_do_state(unsigned int);
    unsigned int cpp_get_state(void);
    unsigned int cpp_check_state(unsigned int);

#ifdef __cplusplus
}
#endif

#define FIND_RELEASE_CRASH

#if defined(OK_FOR_SCOTT) && !defined(NO_ASSERTS)
#pragma message(ERROR "should turn off asserts")
#endif
#if defined(OK_FOR_SCOTT) && !defined(NO_CSW)
#pragma message(ERROR "should turn off csw")
#endif


#endif

#if defined(VOLUBLE)
#ifndef NO_CSW
#pragma message("NO_CSW is not defined")
#else
#pragma message("NO_CSW is defined")
#endif
#endif

#if defined(AMZI) && defined(VOLUBLE)
#pragma message("amzi is defined in prefix.pch")
#endif

#ifdef __cplusplus
extern int debugging_flag, debugging_level;
#endif
#define PREFIX_IS_DEFINED
#endif /* prefix_h */

#if defined(AMZI) || defined(AMZI_GET_A_LEAD)
#ifdef IOS
char *call_amzi(const char *);
char *str_call_amzi(const char *command, int position);
int assert_amzi(const char *term);
int int_call_amzi(const char *, int);
char *int_str_call_amzi(const char *command,int i_index, int s_index);
#else
EXTERN_HOFFER_C char *call_amzi(const char *);
EXTERN_HOFFER_C char *str_call_amzi(const char *command, int position);
EXTERN_HOFFER_C int assert_amzi(const char *term);
// EXTERN_HOFFER_C int int_call_amzi(const char *command, int index);
EXTERN_HOFFER_C char *int_str_call_amzi(const char *command,int i_index, int s_index);

#endif
#endif
