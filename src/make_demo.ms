> arguments
? arguments.length() < 1
    > "need the name of a file to convert that consists of lines of 5 integers which are hool library commands"
    V
    }
$ in_file_name = arguments.get(0)
$ lines = readFile(in_file_name,"ascii")

$ commands = split(trimmed(replaced(lines, crlf, lf)), lf)
$ demo_command = ""
$ count = 0
@ command : commands
    ? command.get(0) == ";"
        v
        }
    $ command_start = get(split(command," "),0) 
    > "command is:" + command + "<"
    ? count == 0
        & demo_command = demo_command + 'const Turn = require("../lib/binding.js");' + crlf
        & demo_command = demo_command + "let "
        }
    ? command_start.length() < command.length()
        $ comment = command.subset(command_start.length())
        & demo_command = demo_command + "/" + "/ " + comment + crlf
        }
    & demo_command = demo_command + "turn = new Turn(" + command_start + ");" + crlf
    > demo_command
    & demo_command = demo_command + "console.log(turn.play())" + crlf + crlf
    & count = count + 1
    }

$ out_file_name = "../meadowlark/test.js"
* writeFile(out_file_name,demo_command,"ascii")


> "done"



