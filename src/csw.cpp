// #include "y:/Desktop/bidding/bidding/bidding/bidlibc_new/gen/include/ring.h"
#include "meadowlark.h"
#undef GLOBALS
#include "../include/GLOBALS.H"


#ifndef NO_CSW
#if !defined(linux)
#pragma message("turning on CSW")
#endif
#define EOS '\0'

#ifndef CSWITCH_H
#define CSWITCH_H

#define MAX_STR 128
#define DEFAULT_MAX 1000000
#define FNAME_SIZE 13
#define MAX_CSW_AREAS 500

struct {
    char file[FNAME_SIZE];
    int min;
    int max;
    } csw_area[MAX_CSW_AREAS];

int n_csw_areas=0;

struct {
    int min;
    int max;
    int one_shot_flag;
    int singleton_flag;
    long count;
    } trigger_area[MAX_CSW_AREAS];
int n_trigger_areas=0;
int trigger_stack[MAX_CSW_AREAS];
int trigger_top= 0;
#endif

void message(const char *str) {
    // BidMessage(str);
    }
    
void print_triggers(void) {
    int min,max,flag1,flag2;
    long count;
	int i;
    for (i=0; i<n_trigger_areas; i++) {
        min=trigger_area[i].min;    
        max=trigger_area[i].max;
        count = trigger_area[i].count;
        flag1 = trigger_area[i].one_shot_flag;
        flag2 = trigger_area[i].singleton_flag;
        fprintf(stderr,"%u min:%u max:%u count:%lu one_shot:%u singleton:%u\n",
                   i,min,max,count,flag1,flag2);
        }
    }        

int set_trigger(const char *s) {
    int max=0,min;
//	printf("set_trigger %s",s);
    if (n_trigger_areas >= MAX_CSW_AREAS) {
        fprintf(stderr,"trigger overflow\n");
        return 0;
        }
    min = *s - '0';
    while (*++s >= '0' && *s <= '9')
        min=(10 * min) + *s - '0';
    if (*s == ',') {
       while (*++s >= '0' && *s <= '9')
           max=(10 * max) + *s - '0';
       }
    else max = min;
    trigger_area[n_trigger_areas].min = min;
    trigger_area[n_trigger_areas].max = max;
    trigger_area[n_trigger_areas].count = DEFAULT_MAX;
    trigger_area[n_trigger_areas].one_shot_flag = 0;
    trigger_area[n_trigger_areas].singleton_flag = 0;
    n_trigger_areas++;
    return 1;
}

int set_one_shot(const char *s) {
    int max=0,min=0;
    long count=0;

    if (n_trigger_areas >= MAX_CSW_AREAS) {
        fprintf(stderr,"trigger overflow\n");
        return 0;
        }
    min = *s - '0';
    while (*++s >= '0' && *s <= '9')
        min=(10 * min) + *s - '0';
    if (*s == ',') {
       while (*++s >= '0' && *s <= '9')
           max=(10 * max) + *s - '0';
       }
    else max = min;
    if (*s == ',') {
       while (*++s >= '0' && *s <= '9')
           count=(10 * count) + *s - '0';
       }
    else count=1;
    trigger_area[n_trigger_areas].min = min;
    trigger_area[n_trigger_areas].max = max;
    trigger_area[n_trigger_areas].count = count;
    trigger_area[n_trigger_areas].one_shot_flag = 1;
    trigger_area[n_trigger_areas].singleton_flag = 0;
    n_trigger_areas++;
    return 1;
}

int set_singleton(const char *s) {
    set_one_shot(s);
    trigger_area[n_trigger_areas-1].singleton_flag = 1;
    return 1;    
    }

int getargs2(const char *cmd_line) {
    char c;
	const char *s;
    int ik;
    int max, min;
    // extern int debug;
#ifndef NO_LOGGING
    do_log(1,20,cmd_line);
#endif
	TRIGGER(1,fprintf(stderr,"the command line is <%s>\n",cmd_line));
    if (n_csw_areas >= MAX_CSW_AREAS) {
        fprintf(stderr,"csw overflow\n");
        return 0;
        }

    s=cmd_line;
    while (*s != '\0')
        switch(toupper(*s)) {
        case 'S':
                                     /* turn on area debugging */
            do {
               ik=0;
               while ( isspace(*++s)) ;
               while ((c= *s) != EOS && c != ',' && c != ';') {
                ++s;
                csw_area[n_csw_areas].file[ik++] = toupper(c);
                }
               csw_area[n_csw_areas].file[ik]='\0';
               min = 0;
               if (*s == ',')
                while (*++s >= '0' && *s <= '9')
                    min=(10 * min) + *s - '0';
               max=0;
               if (*s == ',') {
                while (*++s >= '0' && *s <= '9')
                    max=(10 * max) + *s - '0';
                }
               else max = 20000;
               csw_area[n_csw_areas].min = min;
               csw_area[n_csw_areas].max = max;
              // BidMessage("\nFile: %s",csw_area[n_csw_areas].file);
              // BidMessage("\nAreas = %d, min=%d, max=%d",n_csw_areas+1,
            //  csw_area[n_csw_areas].min,
            //  csw_area[n_csw_areas].max);
               ++n_csw_areas;

            } while (*s == ';');
            break;
        case '-': ++s; break;
        default:
            fprintf(stderr,"\n CSW: illegal option %c\n",*s);
            exit(1);
        }
    return 1;
}

void clear_triggers(void) { n_trigger_areas = 0; }
void push_triggers(void) { trigger_stack[trigger_top] = n_trigger_areas; trigger_top++; }
void pop_triggers(void) { if (trigger_top) n_trigger_areas = trigger_stack[--trigger_top]; }

void clear_csw(void) { n_csw_areas = 0; }
void pop_csw(void) { if (n_csw_areas) n_csw_areas--; }

void getargs(int argc,char **argv)
{
    char *s;
    int count;
    char **v;

    count = argc;
    v = argv;

        n_csw_areas = 0;

   while (--count > 0)
    if  ((*++v)[0] == '-') {
    s=v[0] + 1;
    getargs2(s);
    }
}
// This is a routine from the artice A Generic Command-Line Switch in the C Users Journal Page 95, October 1991.  Given the source 
// code and the CSW_EXEC macro, this will turn on sections for debugging by the use of a command line switch. The switch format is 
// -sxxx.y,a,b where xxx.y is a filename of the source code section to be debugged and a to b is the range of line numbers.  
// Therefore, in order to debug a section... 1) put the CSW_EXEC macro in a section of code. 2) add the cswitch.h header to the 
// code. 3) use the command line -sbid.c to see all macros function in the source code file bid.c; or -sbid.c,6 to see just line 
// 6; and -sbid.c,6,33 to turn on debugging for lines 6 to 33.  The format for the macro is:  CSW_EXEC(x) where x is usually a 
// printf statement, but can be any executable; such as:  CSW_EXEC(printf("\nVariable j is %u",j));

int csw_ison(const char *file, int line)
{
    char name[FNAME_SIZE + 1];
    char c;
	const char *p;
    int i;

    if (n_csw_areas == 0) return(FALSE);
    if (file == NULL) return(TRUE);
    p = strrchr(file,'\\');
    if (p != NULL) ++p;
    else p=file;

    i=0;
   while ((c= *p) != EOS) {
    ++p;
       if (c=='/') { i=0; continue; } // needed for linux, ios, osx
    name[i++] = toupper(c);
    }
    name[i]=EOS;

    i=0;
    while (i<n_csw_areas) {
        if (strcmp(csw_area[i].file,name) ==0) {
            if (line >= csw_area[i].min && line <= csw_area[i].max)
                return(TRUE);
            }
        i++;
        }

    return(FALSE);
}

int trigger(int index) {
    int i=0,min,max,flag;
    long count;
    if (n_trigger_areas == 0) return(FALSE);
    while (i<n_trigger_areas) {
        min = trigger_area[i].min;
        max = trigger_area[i].max;
        count = trigger_area[i].count;
        flag = trigger_area[i].singleton_flag;
        if (index >= min && index <= max) {
            if ((index == 102 || index == 103) && min != max)
                fprintf(stderr,"trigger: %u index:%u min:%u max:%u count:%lu flag:%u\n",i,index,min,max,count,flag);
            if (trigger_area[i].count > 0)
                { trigger_area[i].count--; return(TRUE); }
            else if (trigger_area[i].count == 0 && flag == 1) {
                trigger_area[i].count--;
                fprintf(stderr,"SINGLETON %u fired more than once\n",index);
                }
            return FALSE;
            }
        i++;
        }
    return FALSE;
    }

int check_one_shots(int first, int last) {
    int index,ans=1,i;
    for (i=first; i<= last; i++) {
      index = 0;
      while (index<n_trigger_areas) {
            if (i >= trigger_area[index].min &&
                 i <= trigger_area[index].max && trigger_area[index].one_shot_flag == 1)
                if (trigger_area[index].count != 0) {
                    fprintf(stderr,"ONESHOT: did not fire %u, count is %ld\n",i,trigger_area[index].count);
                    ans = 0;
                    }
            index++;
            }               
        }
    return ans;
}

#endif
