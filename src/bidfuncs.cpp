#include "meadowlark.h"
#undef GLOBALS
#include "../include/GLOBALS.H"

#ifndef DIVORCE

// Shuffle  Uses a pseudo-random number generator routine. The random number 
// is an integer and the modulo operator is used to determine the next element 
// to be selected. A random number generator which produces numbers from 0 to 
// less than 1 can be used by using the multiplication operator.

int gen32_get_hcps(int *deck, int first,int number)
{
        int i,j,points;
        i=j=points=0;

        for (j=first; j< first + number; j++) {
                i = deck[j] % 13;
                if (i > 8) points=points +i -8;
                }
        return(points);
}

// Shuffle  Uses a pseudo-random number generator routine. The random number 
// is an integer and the modulo operator is used to determine the next element 
// to be selected. A random number generator which produces numbers from 0 to 
// less than 1 can be used by using the multiplication operator.


/* Versions
210209 No changes to this file when windows version brought over
*/
void gen32_shuffle(int deck[52], int first,int number)
{
        int temp,pick;
		char temp_msg[80];
		sprintf(temp_msg,"shuffle entered");
#ifndef NO_LOGGING
		gen32_do_log(1,10,temp_msg);
#endif

        if (number < 2) return;
        while (number != 1) {
                pick=first + ( rand() % number );     
                if (pick != first) {
                        temp=deck[first];
            deck[first]=deck[pick];
                        deck[pick]=temp;
                        }
                --number; ++first;
                }
}


extern "C" void gen32_distro(int deck[52], int b, int c, int d[4])
{
        int i;
        for (i=0; i<4; i++) d[i]=0;

        for ( i=b; i< b+c; i++)
                d[(deck[i]/13)]++;
}


void sort_by_length(int *d)
{
        int i,j,temp;
        for (i=3; i>0; i--)
           for (j=i-1; j>=0; j--)
               if (d[j]<d[i]) {
                   temp = d[i];
                   d[i]=d[j];
                   d[j]=temp;
                   }
}

int gen32_getLTC(int deck[52],int  first, int number)
{
    int i,suit;
    int num[4],losers[4];
    int tops[4],t;
    int total_losers=0;

    for (i=0;i<4;i++) { tops[i]=0; num[i]=0; losers[i]=0; }

    for (i=first; i< first + number; i++) {
        suit=deck[i] / 13;
        t=(deck[i] % 13);
        if (t>9) switch (t) {
            case 10: tops[suit] += 1; break;
            case 11: tops[suit] += 2; break;
            case 12: tops[suit] += 4; break;
            }
        if (++num[suit] < 4)
            ++losers[suit];
        }

    for (i=0; i<4; i++) {
        switch(losers[i]) {
            case 1: if (tops[i]>3) losers[i] = 0;
                    break;
            case 2: if (tops[i]>5) { losers[i] = 0; }
                    else if (tops[i]>1) { losers[i]=1; }
                    break;
            case 3: if (tops[i]==7) { losers[i] =0; }
                    else if (tops[i]==1 || tops[i]==2 || tops[i]==4) { losers[i] = 2; }
                    else if (tops[i] == 0) { ; }
                    else { losers[i] = 1; }
                    break;
            }
        }

    for (i=0; i<4; i++)
        total_losers += losers[i];

    return(total_losers);
}

int getAces(int *deck, int first,int number)
{
        int i=0,j=0,num_of_aces=0;
 
        for (j=first; j< first + number; j++) {
                i = deck[j] % 13;
                if (i == 12) num_of_aces += 1;
                }
        return(num_of_aces);
}


int gen32_getQuicks(int deck[52], int first, int number, int quicks[4])
{
        int i,j,denom, suit,total,temp[4][3];

        for (i=0; i<4; i++)
            for (j=0; j<3; j++)
                temp[i][j]=0;
        total = 0;
        for (j=first; j< first + number; j++) {
                denom = deck[j] % 13;
                suit = deck[j] / 13;
                switch (denom) {
                    case 12: temp[suit][2]= 1; break;
                    case 11: temp[suit][1]= 1; break;
                    case 10: temp[suit][0]= 1; break;
                    default: break;
                    }
                }
        for (i=0; i<4; i++) {
            if (temp[i][2] == 1 && temp[i][1] == 1) quicks[i] = 4;
            else if (temp[i][2] == 1 && temp[i][0] == 1) quicks[i] = 3;
            else if (temp[i][2] == 1) quicks[i] = 2;
            else if (temp[i][1] == 1 && temp[i][0] == 1) quicks[i] = 2;
            else if (temp[i][1] == 1) quicks[i] = 1;
            else quicks[i] = 0;
            total += quicks[i];
            }


        return(total);
}
//#include "StdAfx.h"
#include "../include/Pch.h"
#include "../include/bergen.h"

#define STOP 0
#define ERR (-1)

char *addr;
int temp[52];
int makearray(int array[4]);
int matchdist(int a[4],int b[4]);




#ifndef USE_BERGEN
void rubber_shuffle(char str[53],int seed,int rubber, int hand, char *d1, char *d2,
    char *d3, char *d4)
{
    long s;
    int deck[52],i;

    for (i=0;i<52;i++)
        deck[i]=i;

    s = (seed * 500) + (20 * rubber) + hand;
    srand((unsigned int)s);

    gen32_shuffle(deck,0,52);
    sort(deck,0,13,UP);
    sort(deck,13,13,UP);
    sort(deck,26,13,UP);
    sort(deck,39,13,UP);
    get_description(deck,0,d1);
    get_description(deck,1,d2);
    get_description(deck,2,d3);
    get_description(deck,3,d4);

    for (i=0; i<52; i++)
            str[i] = deck[i]+ 32;

}
#endif


// int ckdist(int mode, char far *str) {     int 
// temp1[4],temp2[4],i,j,k,l,ans;     addr=str;     if (mode==ONE) {   while 
// ((ans=makearray(temp1)) != ERR && ans != STOP)    if 
// (matchdist(dist,temp1)==1) return(1);             }     else if 
// (mode==PERMUTE) {   while ((ans=makearray(temp1)) != ERR && ans != 
// STOP)          for (i=0; i<4; i++)             for (j=0; j<4; 
// j++)              if (j==i) ;              else for (k=0; k<4; 
// k++)                 if (k==i || k== j) ;     else for (l=0; l<4; 
// l++)                   if (l==k || l==i || l==j) ;                   else 
// {                     temp2[0]=temp1[i];                     
// temp2[1]=temp1[j];                     
// temp2[2]=temp1[k];                     temp2[3]=temp1[l];      if 
// (matchdist(dist,temp2)==1)                         
// return(1);                     }         }     else return(ERR);     if 
// (ans==STOP) return(0);     else return(ERR); }

int makearray(int array[4])
{
        int i;

        if (addr[0]==',') addr += 1;    /* advance to next set */
        else if (addr[0]== '\0') return(STOP);

        for (i=0; i<4; i++) 
                if (addr[i] < '0' || addr[i] > '9') return(ERR);
                else array[i]=addr[i] - '0';

        addr = &addr[4];

        return(1);
}

int matchdist(int a[4],int b[4])
{
        int i;

        for (i=0; i<4; i++)
        if (a[i] != b[i]) return(0);
        return(1);
}

#define OPENING_BID 1
#define SUPPORT_OF 2

/* opp_bid is 0 neither bid; 1 = RHO bid; 2 = LHO bid; 3 = both bid for each suit */

#ifndef USE_BERGEN
int get_roth(char *input, int start) {
    int i,deck[52];

    for (i=start; i<start+13; i++) deck[i] = input[i]-32;
    return gen32_roth(deck,start,13,0,0);
}


int gen32_roth(int deck[52],int start,int number,int state, int suit)
{
    int i, j, count, dist[4], hcp[4], aces[4], ace_flag;
    int doubleton_flag, kings[4];
    int honors[4];
    ace_flag = count = 0;



    gen32_distro(deck,start,number,dist);

    for (i=0; i<4; i++) { hcp[i]=0; aces[i]=0; kings[i]=0; honors[i]=0;}

    /* get high card points counted up and put in hcp array  */
    /* also get the number of aces and their suits  */

    for (i=start; i<start + number; i++) {
        if ((j=deck[i] % 13) > 8) {
            hcp[deck[i] / 13] += j-8;
            honors[deck[i]/13] += (1 << (j-9));
            }
        if (j == 12) { ace_flag += 1; aces[deck[i]/13]=1;}
        if (j == 11) { kings[deck[i]/13]=1; }
        }

    /* add distributional points: 1,2,3 for shortness;
       1,2 for length of 6,7; but minor suits require
        1 of top 3 honors Note: this was changed 6/13/94 from
        2 of top 3 honors to 1 */

    /* the rules are:
        add only 1 point for doubletons in support
        add 1 point for voids or singletons and 4 card support
        add 2 points for voids or singletons and 5+ support
        get rid of shortness points with no fit and keep them
             the same with 3 card support.
    */

    doubleton_flag=0;
    for (i=0; i<4; i++) {
        if (dist[i] < 3) {
            if (state == SUPPORT_OF) switch (dist[suit]) {
                case 0:
                case 1:
                case 2: break;
                default:
                case 5: if (dist[i] <2) count += 1;
                case 4: if (dist[i] <2) count += 1;
                        if (dist[i] == 2 && doubleton_flag == 0) {
                            count += 1;
                            doubleton_flag = 1;
                            }
                case 3: count += (3-dist[i]); break;
                }
            else count += (3-dist[i]);
            }

        if (dist[i] == 6 && i > 1) count += 1;  /* major */
        else if (dist[i] >= 7 && i > 1) count += 2; /* major */
        else if (dist[i] == 6 && i < 2 && hcp[i]>1) count += 1; /* minor with honor */
        else if (dist[i] >= 7 && i < 2 && hcp[i]>1) count += 2; /* minor with honor */
        }

    /* add up high card points  */

    for (i=0; i<4; i++)
        count += hcp[i];


// #define IN_OUT_VALUATION

    /* remove high card points based on opponents bidding; our suit and shortness */

#ifdef IN_OUT_VALUATION
    for (i=0; i<4; i++)
        if (opp_bid[i]) switch(honors[i]) {
            case 0: break;
            case 1: case 2: count--; break;
            case 3: if (dist[i] == 2) count -= 2; else count--; break;
            case 4: case 5: case 6: if (opp_bid[i] & 1) count++; else count--; break;
            case 7: case 8: break;
            case 9: if (opp_bid[i] & 2) count--; break;
            case 10: case 11: if (opp_bid[i] & 1) count++; else count--; break;
            case 12: break;
            case 13: if (opp_bid[i] & 1) count++; else count--; break;
            default: break;
            }
        else if (state == SUPPORT_OF && i == suit) switch(honors[i]) {
            case 0: break;
            case 1: case 2: case 3: count++; break;
            case 4: break;
            case 5: count++; break;
            case 6: case 7: case 8: break;
            case 9: count++; break;
            default:  break;
            }
        else switch(honors[i]) {
            case 0: break;
            case 1: case 2: if (dist[i] == 1) count--; break;
            case 3: if (dist[i] == 2) count--; break;
            case 4: if (dist[i] == 1) count--; break;
            case 5: case 6: if (dist[i] == 2) count--; break;
            case 7: case 8: break;
            case 9: if (dist[i] == 2) count--; break;
            default: break;
            }
#else
    /* subtract 1 point for unguarded honors */
    /* 1/16/96 - do not subtract unguarded honor in given suit */

    for (i=0; i<4; i++) {
        if (state == SUPPORT_OF && i == suit) /* not for support */ ;
        else {
          if (dist[i]==1 && hcp[i] >0 && hcp[i]<4) count -= 1;
          if (dist[i]==2 && hcp[i] >0
             && hcp[i] != 3 && hcp[i]<6 && aces[i]==0) count -= 1;
          if (dist[i]==2 && hcp[i] == 3 && kings[i]==0) count -= 1;
          if (dist[i]==3 && hcp[i] == 1 ) count -= 1;
          }
        }
#endif

    /* for opening bids correct for all or none aces */
    /* changed 1/19/96 so that count is only reduced for hands that
        have more than 10 count points. This was done since the
        OPENING_BID count is used for purposes of original strength
        not just support, and hands such as those with 5 or 6 points
        would always end up with less whenever an ace did not make
        up a part of the hand. Hands over ten points should have an
        ace and thus deserve to be lowered in count */

    if (ace_flag == 4 && state == OPENING_BID) count += 1;
    if (count > 10 && ace_flag == 0 && state == OPENING_BID) count -= 1;

    if (count <0) count = 0;
    return(count);

}
#endif
#endif
