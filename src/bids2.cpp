#if defined(ANDROID)
#define SIGNED_CHAR_GRAB_INDEX
#endif

#undef ALOGGING
#if defined(IOS)
#undef ALOGGING
#endif

#if defined(ALOGGING)
#define ANDROID_LOGGING_FOR_TRICKY
#ifdef ANDROID_LOGGING_FOR_TRICKY
#include <android/log.h>
#define  LOG_TAG    "testjni"
#define  ALOG(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#endif
#endif

#ifdef WIN_ONLY
#include "windows.h"
#endif
#include "hoffer.h"
#ifdef NEW_LEAK_DEBUGGING
// #include "dbg.h"
#include <crtdbg.h>
#endif

#include "header.h"

#ifdef WIN_ONLY
#define snprintf _snprintf
#endif

extern int fval[100];

#ifdef NO_MACROS_VERSION

void push_forth(int);
void pop_forth(int);
#endif

#include "bid20.h"
#include "non_exportables.h"

#if defined(linux)
#define NO_CSW
#include "cswitch.h"
#define EXTERN_HOFFER_C
#include "assert.h"
#endif

extern int ns_system, ew_system;
#if !defined(EXCLUDE_BIDINFO)
BINFO bid_info[BID_INFO_MAX][4], temp_info; // BID_INFO_MAX (probably 25) rounds of bidding, by player direction
#endif

void CmdProcess(const char *);

#define PARSE_OK 0
#define MAX_SEQ_LEN 500

#undef VERSION2
#ifdef VERSION2
#define NUM_FEATURES 20
typedef struct struct_bidding_info {
  int lower[4][NUM_FEATURES], upper[4][NUM_FEATURES];
} bidding_info;

extern bidding_info b_info;
#endif
extern MatchTree *sys;
extern TBidState *BiddingSystem;
extern bool special_id;

#ifndef MULTISYSTEMS
#define NS_CONVENTION(p) p->convention.ns
#define EW_CONVENTION(p) p->convention.ew
#else
#define NS_CONVENTION(p) sys->vSystems[ns_system].lines[p->line_number]
#define EW_CONVENTION(p) sys->vSystems[ew_system].lines[p->line_number]
#endif

int fence0=1;
int fence1=0;
int table=0;
int fence2=0;
int fence3=1;
EXTERN_HOFFER_C void set_table(int t) {
    assert(t >=0 && t < NUM_OF_TABLES);
    table = t;
}

#define EMPTY -1
#define CHARCAST char *
#undef MACRO
#ifdef MACRO
extern Macro m;
#endif
#ifndef CHAR_ONLY
void reset_gi(void);
#endif
// void reset_closures(RXNODE *);
int getseat(char *);

#define SET_HCP
#ifdef SET_HCP
#define LOW 0
#define HIGH 1
void set_hcp(int,int,int,int);
int set_of_hcps[4][2];
int set_of_bidders[4][4];
// void set_bidder(int i,int j) {}

void set_hcp(int i, int j, int k, int l) {
    if (j==4) {
        for (i=0; i<4; i++) {
            set_of_hcps[i][LOW] = 0;
            set_of_hcps[i][HIGH] = 37;
            }
        }
    else {
        set_of_hcps[i][LOW]=k;
        set_of_hcps[i][HIGH]=l;
        }
    }
#endif


char grab_info[27];
char forth_info[9];

#include "bids2.h"

#ifdef VERSION2
extern signed char blower[20], bupper[20];
#endif


// #define PRIORITIZE
#define NEW_TEMP
#define CONSTRAINT_IN
#define NEW_GRABS
int max_depth=0;
#define CONVENTIONS
BIDNODE  *tree;
char bid_out[10];
/* these next eight variables were taken out of bid_search
as local variables and globalized to save stack space */
char mbuf[80];
char mbuf2[80];
BIDNODE *bid2_ptemp;
BIDNODE *btemp2;
const char *bid2_ans;
char  *spot;
int k,c,skip_macro;
int bid_debug=0;
char des[D_LEN];
int print_stack_top;
extern int rx_stack_index;
int clue_flag;
struct pstack clue_temp[C_MAX];
struct pstack pstack_temp[C_MAX];
int pstack_temp_top;
#ifdef VERSION2
BIDNODE *first_default;
#endif

extern VSA vsa[VSA_MAX];

char hi_low[8];
VSA temp_vsa;


#ifdef PRIORITIZE
typedef struct leaves {
    BIDNODE *pos;
    int depth;
    } LEAF;
LEAF leaf[100];
int vsa_index;
void prioritize(void);
#endif





#ifndef BID_STRUCT
static void  bid_search(char  *, BIDNODE  *, unsigned char *,int) ;
#else
static void  bid_search(int depth);
#endif


/* in version2 the space was eliminated after START */
#ifdef VERSION2
#define STARTING_POSITION 5
#else
#define STARTING_POSITION 6
#endif


#if !defined(EXCLUDE_BIDINFO)
void reset_all_bid_info(void) {
    for (int round=0; round< BID_INFO_MAX; round++) 
        for (int seat=0; seat<4; seat++) reset_bid_info(&bid_info[round][seat]);
    }
        
void reset_bid_info(BINFO *b) {
	for (int i=0; i<4; i++) b->bid[i]=0;
	b->forcing_pass = 48;
	b->game_force = 48; // '0'
	b->high = 40;
	b->low = 0;
	b->level = 0;
	b->one_round_force=0;
	b->vul=0;
}

void copy_bid_info(BINFO *b, BINFO *from) {
	for (int i=0; i<4; i++) b->bid[i]=from->bid[i];
	b->forcing_pass =from->forcing_pass;
	b->game_force=from->game_force;
	b->high=from->high;
	b->low=from->low;
	b->level=from->level;
	b->one_round_force=from->one_round_force;
	b->vul=from->vul;
    }
    
int get_colon_count(const char *s) {
    int i=0,count=0;
    while (s[i++] != '\0')
        if (s[i]==':') count++;
    return count;
}
int get_bid_count(char *s);
int get_bid_count(char *s) {
	bool in_bid=false,correction=false;
	int bid_count=0,colon_count=0,i=0;
	while (s[i] != '\0') {
		if (s[i]==':') {
			switch (s[i-1]) {
				case 'C': case 'X': case 'P': case 'R':
				case 'D':
				case 'H':
				case 'S':
				case 'N': bid_count=1; break;
				default: break;
				}
			colon_count=1;
			break;
			}
		i++;
	}
    if (colon_count && bid_count==0) correction=true;
	if (colon_count) {
		while (s[i] != '\0') {
			if (s[i]==':') in_bid=false;
			else if (!in_bid) { in_bid=true; bid_count++;}
			i++;
			}
		}
	else bid_count=1;
	if (correction) bid_count++;
	return bid_count;
}
     
int get_current_round_and_seat(int depth, int *round, int *seat) { // round and seat are zero based
	*round = vsa[depth].current_round;
	*seat = (vsa[depth].current_bidder - vsa[0].current_bidder + 4) % 4;
	return *round * 4 + *seat;

/*    int leader; // total_bids_A
	int total_bids_B,bids_remaining,current_bidder;
    leader = vsa[0].current_bidder;
    // total_bids_A = get_colon_count(vsa[0].str) + 1;
	total_bids_B = get_bid_count(vsa[0].str);
    bids_remaining = get_bid_count(vsa[depth].str);
    current_bidder = (leader + total_bids_B - bids_remaining) % 4;
	if (current_bidder != vsa[depth].current_bidder) {
		get_bid_count(vsa[0].str);
		get_bid_count(vsa[depth].str);
		assert (current_bidder == vsa[depth].current_bidder);
	}
    *round = (total_bids_B-bids_remaining) / 4;
    *seat = (total_bids_B-bids_remaining) % 4;
    return *round * 4 + *seat; // leaves the ability to double check status on return
*/
    }
#endif    
        
    
EXTERN_HOFFER_C void clear_clues(void) {
#ifdef CSW
    TRIGGER(602,do_log(1,10,"clear_clues\n"));
#endif
        for (int ii=0; ii < C_MAX - 1; ii++) {
            clue_stack[ii].node = NULL;
            clue_temp[ii].node = NULL;
            }
    }

int forth_bids[100];
int forth_bid_count=0;

EXTERN_HOFFER_C int split_sequence(const char *in_seq) {
/* added 210117 split a bidding sequence into an array of integers representing the bids to use in forth evaluations
*/
	int level=0,denom,bid,state=0,count=0;
	char ch;
	forth_bid_count=0;
	char *cptr = (char *) in_seq;
		// some sequences have prepended "START " or "DEFAULT " skip that to space character; 9 used as it is longer that START and DEFAULT string length
	while (*cptr != ' ' && *cptr != '\0' && count < 9) {count++; cptr++;}
	if (*cptr != ' ') cptr = (char *) in_seq; // no space found go back to original start; it is all part of the sequence
	else ++cptr; // space found; start at next character 
	while ((ch = *cptr++) != '\0') {
		if (state == 99) goto done;
		if (ch == ':') continue;
		if (state == 2) state = 0;
		if (state == 0) switch(ch) {
			case 'P': forth_bids[forth_bid_count++] = 0; state = 2; break;
			case 'X': forth_bids[forth_bid_count++] = 1; state = 2; break;
			case 'R': forth_bids[forth_bid_count++] = 2; state = 2; break;
			case '1': case '2': case '3': case '4': case '5': case '6': case '7': level = (ch - '0') * 10; state = 1; break;
			default: state = 99; break;
			}
		else if (state == 1) switch(ch) {
			case 'C': denom = 0; bid = level + denom; forth_bids[forth_bid_count++]=bid; state = 2; break;
			case 'D': denom = 1; bid = level + denom; forth_bids[forth_bid_count++]=bid; state = 2; break;
			case 'H': denom = 2; bid = level + denom; forth_bids[forth_bid_count++]=bid; state = 2; break;
			case 'S': denom = 3; bid = level + denom; forth_bids[forth_bid_count++]=bid; state = 2; break;
			case 'N': denom = 4; bid = level + denom; forth_bids[forth_bid_count++]=bid; state = 2; break;
			default: state = 99; break;
			}
		}
done:
	if (state != 2) { forth_bid_count = 0; return 99; } // must find at least one bid and finish on a bid
	return forth_bid_count;
}

#undef SC1
// SC1 was used to limit the considered bids to only that of a double of 2C for debugging purposes 210520
char *do_constraints2(const char *in_seq, const char *description,
     BIDNODE  *position, BIDNODE  *clue, unsigned int *p_return)
{
#ifdef _LEAK
    _CrtMemState s1, s2, s3;
    _CrtMemCheckpoint(&s1);
#endif
    // set_trigger("1609,1602,1200");
#if defined(CSW)
    int debug_level = 30;
    TRIGGER(1200,do_log(1,debug_level,"bidding:do_constraints2:entered:%s %s",in_seq,description));
#endif
    
    char seq[MAX_SEQ_LEN];
    char temp[MAX_SEQ_LEN], temp2[MAX_SEQ_LEN];
    
    char level;
    char dbl;
    char bid[4];
    char *tempbid;
    unsigned char priority, temp_priority;
    int suit;
    enum levels { one=1, two, three, four, five, six, seven };
    enum suits { clubs=1, diamonds, hearts, spades, notrump };
    char suit_char[6]="CDHSN";
    char level_char[8]="1234567";
    char suffix[3];
        int SNT_flag=0;
    int double_flag=0;
//	MSG msg;
//    var_struct v;
#ifdef GSIZION
    int dealer,seat,vul,side;
#endif

    int colon_flag;
    int i,len, in_prepend=0;
    strcpy(seq,in_seq);
    temp[MAX_SEQ_LEN-2]='@';
    temp2[MAX_SEQ_LEN-2]='@';
    /* flag that tells whether or not a colon
                                       is present at the end of a bidding
                                       sequence.						   */

#ifdef GSIZION
    seat = getseat((char *)&seq[STARTING_POSITION]);
    vul = 1;
    switch (description[57]) {
        case '0': vul = 0; break;
        case '2': vul = 0; break;
        }
    dealer = BiddingSystem->GetDealer();
    side = (seat + dealer - 1) % 2;
    sprintf(temp,"resetconvention G1 %u",side);
    CmdProcess(temp);
    sprintf(temp,"resetconvention G2 %u",side);
    CmdProcess(temp);
    sprintf(temp,"resetconvention G3 %u",side);
    CmdProcess(temp);
    if (vul == 1) {
        sprintf(temp,"setconvention G3 %u",side);
        CmdProcess(temp);
        }
    else if (seat == 1 || seat == 2) {
        sprintf(temp,"setconvention G1 %u",side);
        CmdProcess(temp);
        } 
    else {
        sprintf(temp,"setconvention G2 %u",side);
        CmdProcess(temp);
        } 
#endif

#ifdef CSW
    // set_trigger("1200");
    TRIGGER(1200,do_log(1,20,"doconstraints2 seq:%s des:%s",seq,description));
#endif

    tree = position;
#ifdef CSW
    ONE_SHOT(206,do_log(1,10,"climbing tree"));
#endif
    assert(tree != NULL);
    while (tree->parent)
         tree = tree->parent;

    // Here we finally prepend from the "1H:P:2H:2S:P@123456789012345678@PRESS_01:P"
    //                                   ssssssssssss pppppppppppppppppppppppppppSS
    // where s is intial sequence, p is the string sent to the bidding system and SS is appended to both
    // Now the legal generated bids will be appended to the temp2 string
    // Without any of these schenanigans it just copies seq into temp2
    TRIGGER(1200,do_log(1,30,"doconstraints2:before prepend"));
    { int prepend_count=0,seq_count=0;
    i=0;len=(int)strlen(seq);
        in_prepend = 0;
    while (i < len && seq[i] != '\0') {
        if (in_prepend==1 && seq[i] == ':') in_prepend = 2; 
        if (in_prepend) temp2[prepend_count++] = seq[i];
        if (in_prepend==2) seq[seq_count++]=seq[i]; 
        if (seq[i] == '@') { in_prepend = 1; prepend_count=0; seq_count=i; }
        if (!in_prepend) temp2[prepend_count++] = seq[i];
        i++;
        }
    if (!in_prepend) seq_count = i;
    temp2[prepend_count] = '\0'; // using above example this is now 123456789012345678@PRESS_01:P
    // the original sequence needs to be shortened, but it is a constant.
    
    seq[seq_count]='\0'; // using above example this is now 1H:P:2H:2S:P:P
    if (in_prepend) { TRIGGER(1602,do_log(1,30,"prepend: %s new_seq:%s",temp2,seq)); }
    }
    
    bid[0]='\0';
    priority = 0;

    if (strcmp(description,"EVALUATE")==0 || strcmp(description,"RECURSE1") == 0
            || strcmp(description,"RECURSE2")==0) {
        struct pstack clue_temp2[C_MAX];
#ifdef CSW
        ONE_SHOT(200,do_log(1,10,"do_constraints 2 evaluation section entered\n"));
        ONE_SHOT(600,do_log(1,10,"do_constraints 2 evaluation section entered\n"));
#if defined(ANDROID)
        TRIGGER(1602,do_log(1,39,"do_constraints2 evaluation section entered:%s",seq));
#endif
#endif
        i = BiddingSystem->GetDealer() << 8;
        strcpy(des,description);
#ifndef BID_STRUCT
        bid_search(seq,position,&priority,i);
#else
        for (int ii=0; ii<VSA_MAX; ii++) {
            vsa[ii].temp[MAX_SEQ_LEN-2]='@';
        }

		// initialize unless doing bergen bidding evaluation
		if (position == sys->GetRoot()) {

        vsa[0].str = (char  *)seq;
        vsa[0].pos = position;
        vsa[0].current_bidder = BiddingSystem->GetDealer();
		vsa[0].current_round = 0;
        vsa[0].priority = &priority;
#ifdef VERSION2
        vsa[0].default_flag = 0;
#endif
#ifdef CSW
        TRIGGER(600,do_log(1,10,"clue stack stored before bid_search"));
#endif
        for (int ii=0; ii < C_MAX - 1; ii++) {
            clue_temp2[ii].node = clue_stack[ii].node;
            clue_temp2[ii].match_length = clue_stack[ii].match_length;
            clue_stack[ii].node = NULL;
            clue_temp[ii].node = NULL;
            }
		}
#ifdef CSW
        TRIGGER(201,do_log(1,10,"Evaluation calling bid_search"));

#endif
        // split sequence is a manner for looking at previous bids by using 39O in forth; this sets up the bids
        // so forth can look backwards at bids

		split_sequence(seq);
#if defined(CSW)
        TRIGGER(1200,do_log(1,20,"bidding:calling bid_search(0) 1"));
#endif
        bid_search(0);
#endif

#ifdef CSW
        ONE_SHOT(202,do_log(1,10,"Evaluation completed"));
#if defined(CSW)
        TRIGGER(1200,do_log(1,debug_level,"evaluation completed. Printing print stack"));
#endif
        // fprintf(stderr,"PST7:%d ",print_stack_top);
        for (int _i=0; _i<=print_stack_top; _i++) {
            if (vsa[_i].str == NULL) break;

            TRIGGER(1200,do_log(1,debug_level,"VSA: %s",vsa[_i].str)); // was TRIGGER(602,
            TRIGGER(1200,do_log(1,debug_level,"VSA: ID:%u",vsa[_i].pos->id));
        }
#endif
        // the clues need to be reinstated after an evaluation for further bidding
#ifdef CSW
        TRIGGER(601,do_log(1,10,"clue stack reinstated after bid_search"));
#if defined(ANDROID)
        // set_trigger("1200");
        TRIGGER(1200,do_log(1,debug_level,"clue stack reinstating2"));
#endif
#endif

        for (int ii=0; ii < C_MAX - 1; ii++) {
            clue_stack[ii].node = clue_temp2[ii].node;
            clue_stack[ii].match_length = clue_temp2[ii].match_length;
            }

#if defined(CSW)
        TRIGGER(1200,do_log(1,debug_level,"do_constraints2 returning null string"));
#endif
    return (char *) "";
                }


    /* determine the starting point for generating bids; if there has
       been no bid, then start at One Club; if there have been bids then
       start just beyond the last bid, taking into account that doubles
       and redoubles may have been made. getprior finds the previous
       bid and the double status of a sequence. denom is the character
       value of clubs through notrump. level is 1 to 7 for the possible
       levels of bidding. */

#if defined(ANDROID)
    TRIGGER(1200,do_log(1,10,"at starting position"));
#endif
    if (seq[STARTING_POSITION]) {  						/* previous bids have been made  */
        suit = 0; level = 0; dbl = ' ';
        getprior(&suit,&level,&dbl,&seq[STARTING_POSITION],0); // evaluate seq to generate all legal bids
        if (suit == 0 || level == 0) {  // all bids were passes
            double_flag = 1;   // allow 1st bid to be a P
            suit = clubs-1; level = one;
            }
        else switch (dbl) {
            case 'R': double_flag = 1; break;
            case 'X': double_flag = 3; break;
            case ' ': double_flag = 2; break;
            case 'N': double_flag = 1; break;
#ifndef PRODUCTION_VERSION
            default: my_error("Unavailable double option"); 
#endif
            }

    //if (temp2[6]=='P' && temp2[8]=='P') printf("1 level,suit:%d %d\n",level,suit);
                if (suit == 5 && level == 7) SNT_flag = 1;
        else {
            if (++suit > notrump) { suit=clubs; level++; }
    //if (temp2[6]=='P' && temp2[8]=='P') printf("1 level,suit:%d %d\n",level,suit);
                        }
        colon_flag = 1;
        strcat(temp2,":");
        }
    else { suit=clubs; colon_flag = 0; level=1; dbl=' ';
    // if (temp2[6]=='P' && temp2[8]=='P') printf("2 level,suit:%d %d\n",level,suit);
            double_flag=1;}

        pstack_temp_top = -1;

#ifdef PRIORITIZE
    strcpy(temp,temp2);
    strcat(temp,"ZZ");
    vsa[0].str = (char  *) temp;
    vsa[0].pos = tree;
    vsa[0].current_bidder = BiddingSystem->GetDealer();
    vsa[0].priority = &priority;
#ifdef VERSION2
    vsa[0].default_flag = 0;
#endif
    vsa_index = 0;
#if defined(ALOGGING)
    ALOG("calling bid_search 2");
#endif
    bid_search(0);
    prioritize();
#if defined(ALOGGING)
    ALOG("back from bid_search");
#endif
#endif

#ifdef CSW
    // set_trigger("1605");
    TRIGGER(1605,do_log(1,30,"top of loops:DES:%s",description));
#endif
    // if (temp2[6]=='P' && temp2[8]=='P') printf("2 level,suit:%d %d\n",level,suit);
    while (level <= seven) {
        while (suit <= notrump) {
            switch (double_flag) {
                case 3: suffix[0]='R'; suffix[1]='\0'; double_flag = 1; break;
                case 2: suffix[0]='X'; suffix[1]='\0'; double_flag = 1; break;
                case 1: suffix[0]='P'; suffix[1]='\0'; double_flag = 0; break;
                case 0: if (SNT_flag) { goto SNT_skip; }
                    else {
                        suffix[0]=level_char[level-1];
                        suffix[1]=suit_char[suit-1];
                        suffix[2]='\0'; suit++;
                        }
                                        break;
#ifndef PRODUCTION_VERSION
                default: my_error("Double_flag error");
#endif
                }

#if defined(SC1)
            { static bool only_double = false;
            
                if (double_flag==1 && level == 2 && suit == 0) only_double = true; // only double 2C bids
                if (only_double && double_flag == 0) {
                    if (suit > notrump) {suit = clubs; level++;}
                    else { suit++; }
                    }
            if (suffix[0] != 'X') continue; // only do double bids for finding stack overrun error 210520
            TRIGGER(1605,do_log(1,30,"letting double through only"));
                set_trigger("1200");
        }
#endif
            strcpy(temp,temp2);
            strcat(temp,suffix);
            // if (temp[6]=='P' && temp[8]=='P') printf("%s\n",temp);
#ifdef CSW
            TRIGGER(702,printf("%s\n",temp));
            TRIGGER(1612,do_log(1,30,"constraints2:temp:<%s>",temp));
#endif

#ifndef CHAR_ONLY
            reset_gi();
#endif
#if defined(ANDROID)
            TRIGGER(1200,do_log(1,30,"bidding:setting grab_index to -1"));
#endif
            grab_index = -1; temp_priority = priority;
            clue_flag = 0;

#ifdef DISPATCHING
            while (PeekMessage(&msg,NULL,0,0,PM_NOREMOVE)) {
                GetMessage(&msg,NULL,0,0);
                TranslateMessage(&msg);
                DispatchMessage(&msg);
        }
#endif

        // It became necessary to update the bidder through the
        // bidding system, therefore I added it in a high bit
        // position to the index (the last parameter)

            i = BiddingSystem->GetDealer() << 8;
            strcpy(des,description);

#ifndef BID_STRUCT
            bid_search(temp,tree, &priority, i);
#else
            vsa[0].str = (char  *) temp;
            vsa[0].pos = tree;
            // if prepending we are not evaluating from the start
        if (in_prepend) vsa[0].current_bidder = (BiddingSystem->GetBidder() + BiddingSystem->GetDealer())%4;
        else	vsa[0].current_bidder = BiddingSystem->GetDealer();
            assert(vsa[0].current_bidder >= 0 && vsa[0].current_bidder < 4);
			vsa[0].current_round=0;
            vsa[0].priority = &priority;
#ifdef VERSION2
            vsa[0].default_flag = 0;
#endif
#ifndef PRIORITIZE
			// split sequence is a manner for looking at previous bids by using 39O in forth; this sets up the bids
            // so forth can look backwards at bids
#if defined(CSW)
            TRIGGER(1200,do_log(1,10,"bidding:calling split_sequence"));
#endif
			split_sequence(temp);
#if defined(CSW)
            TRIGGER(1200,do_log(1,10,"bidding:calling bid_search 3:%s",temp));
#endif
            bid_search(0);
            TRIGGER(1200,do_log(1,30,"bidding:back from bid_search 3"));
#endif
#endif

#ifdef PRIORITIZE
            { int i,j,depth;
              for (i=0; i<vsa_index; i++) {    /* set up clue_stack */
                depth = leaf[i].depth;
                clue_stack[depth].node = leaf[i].pos;
                clue_stack[depth].match_length= -1; /* an invalid length */
                for (j=depth; depth > 0; depth--) {
                    clue_stack[j-1].node = clue_stack[j].node->parent;
                    clue_stack[j-1].match_length = -1;
                    }
                vsa[0].pos = clue_stack[0].node;
#if defined(ALOGGING)
                  ALOG("calling bid_search 4");
#endif
                bid_search(0);
#if defined(ALOGGING)
                  ALOG("back from bid_search");
#endif
                }
            }
#endif
        //	TRIGGER(702,printf(" back "));
            *p_return = priority;
            for (i=0; i <= pstack_temp_top; i++) {
                print_stack[i].node = pstack_temp[i].node;
                increment_line_count(pstack_temp[i].node->id);
                print_stack[i].match_length = pstack_temp[i].match_length;
                print_stack[i].bidder = pstack_temp[i].bidder;
                }
                        print_stack_top = pstack_temp_top;
                        // fprintf(stderr,"PST1:%d ",print_stack_top);
                        print_stack[i].node = NULL;
#ifdef PRIORITIZE
            for (i=0; i < C_MAX - 1; i++) clue_stack[i].node = NULL;
#endif
            if (clue_flag) {
                for (i = 0; i <= print_stack_top && i < C_MAX - 1; i++) {
                    clue_stack[i].node=clue_temp[i].node;
                    clue_stack[i].match_length=clue_temp[i].match_length;
                    }
                clue_stack[i].node = NULL;
                }

            if (temp_priority < priority) {
                strcpy(bid,suffix);
                TRIGGER(1200,do_log(1,30,"Bid: %s Priority:%u\n",bid,priority));
                    TRIGGER(1605,do_log(1,30,"Bid: %s Priority:%u\n",bid,priority));
                }
            if (priority == 9) {
                TRIGGER(1605,do_log(1,30,"priority_skip"));

                goto priority_skip;
                }
            }
        if (suit > notrump) {suit = clubs; level++;}
        }
priority_skip:
SNT_skip:
#ifndef NEW_BID_STATE
    colon_flag = 0;       /* just return the bid */
#endif
    if (bid[0]=='\0') {
        if (colon_flag) strcpy(bid_out,":");
        else bid_out[0]='\0';
        strcat(bid_out,"P");
        }
    else {
#ifndef NEW_BID_STATE
        tempbid = bid;
        while (*tempbid == ':') tempbid++;
        if (tempbid[2] == ':') tempbid[2]='\0';
        if (tempbid[1] == ':') tempbid[1]='\0';
        strcpy(bid_out,tempbid);
#else
        strcpy(bid_out,bid);
#endif
        }
#ifdef CSW
    CSW_EXEC(BidMessage("10 Returning:%s\n",bid_out));
#endif

#if _LEAK
    _CrtMemCheckpoint(&s2);
    
#ifdef CSW
        TRIGGER(620,do_log(1,20,"Ending heap state"));
#endif
    if (_CrtMemDifference(&s3, &s1, &s2)) {
#ifdef CSW
        TRIGGER(620,do_log(1,20,"_CrtMemDifferences run...leaks printed"));
#endif
        _CrtMemDumpStatistics(&s3);
        }
    else {
#ifdef CSW
        TRIGGER(600,do_log(1,20,"no leak found"));
#endif
    }
#endif

    TRIGGER(1200,do_log(1,30,"do_constraints2 returns:%s",bid_out));

    return((char *)bid_out);
}



#define REDO_BID_SEARCH
static void  bid_search(int depth)
{
        char flag;
    int i;     // FIX TRICKY 210322
char temp_bidder,temp_round;
#if !defined(SIGNED_CHAR_GRAB_INDEX)
char temp_g_index;
#else
signed char temp_g_index;  // all instances of setting with grab_index were checked in this file 210520
#endif
//    if (depth == 0) my_depth=0;
//    else my_depth += 1;
//    if (my_depth > 500)
//        return;
    if (depth <= 1) {
        TRIGGER(1200,do_log(1,30,"bid_search entered at depth:%d\n",depth));
        }
    
#ifdef REDO_BID_SEARCH
    redo_bid_search:
#endif
    if (depth <= 1) {
        TRIGGER(1200,do_log(1,30,"bid_search at redo_bid_search (top) depth:%d",depth));
        }


            /* this next line takes into account the possibility of a @ command that
               has no regular expression to match. Such a circumstance may occur
               when checking the constraints to send off the matching to sections.
               An example would be to check the Roth points to see if they are
               greater than 12. Then go to the one level bidding patterns. That way
               the point count requirement would not have to be part of each one
               level opening bid, rather that would already have been taken care
               of.

               " @ONE_LEVEL"	!!	%1%	%P~12>%
               "ONE_LEVEL"		!!  %1% %%
               " 1{[HS]"			!!  %3% %:D[5-9][#a]%

               Here the first line checks for high card point requirements without
               checking for any specific bid. If it meets these requirements then
               the bid will be found in ONE_LEVEL and the third line is an
               example of the standard match.                     */
    
    // if (depth==0 && vsa[0].str[0]=='L' && vsa[0].str[1] == 'E' && print_stack_top != -1) {
       // if (vsa[0].pos->sibling == NULL) return; // do not look further this is leads and done now
        //}
    // else
    if (depth==0) print_stack_top = -1;

    #ifdef CSW
    // CSW_EXEC(do_log(1,20,"bid_search:%u",depth));
    if (depth < 4) CSW_EXEC(do_log(1,20,"BS1a table:%u depth:%04u vsa[depth].str:%s %s con:%s",table,depth,vsa[depth].str,vsa[depth].pos->exp.c_str(),vsa[depth].pos->constraint.c_str()));
    ONE_SHOT(1200,do_log(1,10,"BS1b depth:%d %s %s %s id:%d\n",depth,vsa[depth].str,vsa[depth].pos->exp.c_str(),vsa[depth].pos->constraint.c_str(),vsa[depth].pos->id));
    #endif
    // ALOG("bidding:depth is :%d",depth);
    // ALOG("bidding:BS1 %s %s %s\n",vsa[depth].str,vsa[depth].pos->exp.c_str(),vsa[depth].pos->constraint.c_str());
    // ALOG("bidding: setting clue_skip");
    vsa[depth].clue_skip=0;
        if (depth > max_depth) max_depth = depth;
        temp_g_index = grab_index;  // store for sibling processing below

        if (vsa[depth].pos->sibling)         {
            // ALOG("doing stuff because there is a sibling");
                for (int i=0; i<=grab_index; i++)
                    vsa[depth].grab[i] = grab[i];
        #ifdef NO_MACROS_VERSION
                push_forth(vsa[depth].current_bidder % 2);
        #endif
        #ifdef HIGH_LOW
                for (int i=0; i<4; i++) {
                    vsa[depth].high_low[i][0] = high_low[i][0];
                    vsa[depth].high_low[i][1] = high_low[i][1];
                    }
        #endif
                }
    // ALOG("vsa[depth] where depth is %d done for grabs and high_low",depth);
        temp_bidder = vsa[depth].current_bidder;
    	temp_round = vsa[depth].current_round;
    // ALOG("after setting temp_bidder and temp_round");
#ifdef SINGLE_TABLE
            if (table != 0)
    				assert(table==0);  for testing when only innate single table used
#endif
                        // ALOG("bidding:at switch");
			switch (vsa[depth].current_bidder % 2) {
                case 0: if (check_convention(table,0,vsa[depth].pos->line_number) == 0) {
                    goto skip; }
                    break;
                case 1: if (check_convention(table,1,vsa[depth].pos->line_number) == 0){
                    goto skip;}
                    break;
                }
    //this is old code from before tables were used for conventions
    //     else {
    //         switch (vsa[depth].current_bidder % 2) {
    //             case 0: if ((NS_CONVENTION(vsa[depth].pos) & 1) == 0)
    //                     goto skip;
    //                     break;
    //             case 1: if ((EW_CONVENTION(vsa[depth].pos) & 1) == 0)
    //                     goto skip;
    //                     break;
    //             default: break;
    //             }
    //         }
               // skip current line because it is turned off by conventions
    #ifndef CHAR_ONLY
        reset_gi();
    #endif
        rx_stack_index = EMPTY;

    // find the node among siblings (from known previous bidding)
    // do not process further siblings by flagging here and skipping at bottom
    // ALOG("bidding:before clue_stack");
        if (clue_stack[depth].node != NULL) {
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"clue stack is not null...starting override evaluation"));
            TRIGGER(1200,do_log(1,10,"clue stack is not null...starting override evaluation"));
#if defined(ALOGGING)
            ALOG("clue stack is not null...starting override evaluation");
#endif
#endif
                switch (strlen((char *)vsa[depth].str)) {
                    case 1: if (vsa[depth].str[0] == ':') goto skip_override;
                            else break;
                    case 2: if (vsa[depth].str[1] == ':') goto skip_override;
                            else break;
                    case 3: if (vsa[depth].str[2] == ':') goto skip_override;
                            else if (vsa[depth].str[0] != ':') goto skip_override;
                            else break;
                    case 4: if (vsa[depth].str[3] == ':') goto skip_override;
                            else if (vsa[depth].str[3] == 'P' || vsa[depth].str[3]=='X' || vsa[depth].str[3]=='R') {
                                if (vsa[depth].str[0] !=':' || vsa[depth].str[1] != 'P' || vsa[depth].str[2] != ':')
                                    goto skip_override;
                                }
                            else if (vsa[depth].str[0] != 'P' || vsa[depth].str[1] != ':')
                                goto skip_override;
                            break;
                    case 5: if (vsa[depth].str[4] == ':') goto skip_override;
                            else if (vsa[depth].str[0] != ':' || vsa[depth].str[1] != 'P' || vsa[depth].str[2] != ':')
                                goto skip_override;
                            break;
                    default: goto skip_override;
                    }
                if (vsa[depth].pos->parent == clue_stack[depth].node->parent) {
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"clue stack is not null...could not override"));
    #endif
                    goto override;
                    }
    skip_override:
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"clue stack is not null...overriding to correct sibling"));
    #endif
            if (vsa[depth].pos == NULL || vsa[depth].pos == clue_stack[depth].node) {
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"clue stack is not null...they are the same %u (or NULL)",vsa[depth].pos->id));
    #endif
            }
            {
            vsa[depth].clue_skip = ON;
            bid2_ptemp=vsa[depth].pos;
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"clue stack is not null...initialized properly for %u",vsa[depth].pos->id));
    #endif
            }

            try {
            while (vsa[depth].pos != NULL && vsa[depth].pos != clue_stack[depth].node) {
                if (vsa[depth].pos->sibling != NULL) {
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"clue override from id %u to id %u depth %u",vsa[depth].pos->id,vsa[depth].pos->sibling->id,depth));
    #endif
                assert(vsa[depth].pos->parent == vsa[depth].pos->sibling->parent); // double checking not running past somehow;
                }
            else {
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"clue override sibling is null"));
    #endif
                }
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"assert completed"));
    #endif
                 vsa[depth].pos = vsa[depth].pos->sibling;
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"set completed"));
    #endif
            if (vsa[depth].pos == clue_stack[depth].node) {
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"they match"));
    #endif
            }
            
                 
                 }
            }
            catch(...) {
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"clue override from error; id %u to id %u",vsa[depth].pos->id,vsa[depth].pos->sibling->id));
    #endif
            throw std::runtime_error("clue override from error");
            
            }
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"clue override completed"));
#if defined(ALOGGING)
            ALOG("clue override completed");
#endif
    #endif
            if (vsa[depth].pos == NULL || vsa[depth].pos != clue_stack[depth].node) {
    #ifdef CSW
            // TRIGGER(601,do_log(1,10,"undoing because vsa[depth].pos is NULL or nodes do not match"));
            // printf("undoing ");
#if defined(ALOGGING)
                ALOG("undoing");
#endif
    #endif
                
                vsa[depth].pos = bid2_ptemp;
                vsa[depth].clue_skip = OFF;
                }
            else {
    #ifdef CSW
            TRIGGER(601,do_log(1,10,"OK, depth is %u id is %u",depth,vsa[depth].pos->id));
    #endif
                }
            }
    override:
#if !defined(EXCLUDE_BIDINFO)
        { int round, seat, bidder,vul; // result
            // ALOG("bidding:doing bid_info");
        get_current_round_and_seat(depth,&round,&seat);
		bidder = vsa[depth].current_bidder;
		bid_info[round][seat].bidder = bidder;
#ifdef FLUMMOXED
        if (round == 0) reset_bid_info(&bid_info[round][seat]);
		else { // carry forward last round to this but the info is by [round][seat] so correct for that
			for (int _seat=0; _seat<4; _seat++) 
				if (bid_info[round-1][_seat].bidder == bidder) {
					copy_bid_info(&bid_info[round][seat],&bid_info[round-1][_seat]);
					copy_bid_info(&bid_info[round][(seat+1)%4],&bid_info[round-1][(_seat+1)%4]);
					copy_bid_info(&bid_info[round][(seat+2)%4],&bid_info[round-1][(_seat+2)%4]);
					copy_bid_info(&bid_info[round][(seat+3)%4],&bid_info[round-1][(_seat+3)%4]);
					break;
					}
			}
#endif
		bid_info[round][seat].vul=48;
		vul = get_vul();  // 48 = '0'
		if (vul == 3) bid_info[round][seat].vul=49;
		else if (vul == 1 && bidder % 2 == 1) bid_info[round][seat].vul=49;
		else if (vul == 2 && bidder % 2 == 0) bid_info[round][seat].vul=49;
        }
#endif
                                      // find the node among siblings (from known previous bidding)
                                      // find among siblings, different method
    #ifdef CSW
    //	CSW_EXEC(do_log(1,20,"table:%u",table));
    if (depth < 5) {
        TRIGGER(1200,do_log(1,20,"table:%u VSA:%u %u %s %s\n",table,depth,vsa[depth].pos->id,vsa[depth].str,vsa[depth].pos->constraint.c_str())); }
#endif
   // set_trigger("1604");

    TRIGGER(1604,do_log(1,30,"bidding:calling far_match_node_depth with:depth:%d str[0]:<%c> <%s>",depth,vsa[depth].str[0], vsa[depth].str));
    TRIGGER(1604,do_log(1,30,"bidding:calling far_match_node_depth:exp<%s>",vsa[depth].pos->exp.c_str()));
    // ALOG("bidding:fixed signed vs unsigned char for vsa[depth].len");
    // vsa[depth].len = -1 ;

/*
this code was used for debugging when the stack overflow problem was being debugged
 for temp_g_index and grab_index being unsigned under ANDROID but needing to have a -1 value on reset
    { int len;
        if (vsa[depth].pos->id == 5953) {
            set_trigger("1613");
            TRIGGER(1613,do_log(1,30,"at 5953 and grab_index is:%d",grab_index));
            }
        if (vsa[depth].pos->id == 5990) {
            set_trigger("1613");
            TRIGGER(1613,do_log(1,30,"at 5990 and grab_index is:%d",grab_index));
            }
        if (vsa[depth].pos->id != 25669 && vsa[depth].pos->id != 7307) {
            len = far_match_node_depth(depth,(CHARCAST)vsa[depth].str);
            TRIGGER(1604,do_log(1,30,"len for far_match_node_depth is %d\n",len));
            }
        else {
            do_log(1,30,"skipping 25669 or 7307");
            }
    }
 */

    if ( vsa[depth].pos->id != 25669 && vsa[depth].pos->id != 7307 && (vsa[depth].len = far_match_node_depth(depth,(CHARCAST)vsa[depth].str)) >= 0) {
            TRIGGER(1604,do_log(1,30,".len is:%d\n",vsa[depth].len));
            TRIGGER(1604,do_log(1,30,"bidding:matched:str:%s",vsa[depth].str));
            TRIGGER(1604,do_log(1,30,"bidding:matched:exp:%s",vsa[depth].pos->exp.c_str()));
            
            /* matched but not a full match of the bidding sequence input and the exp
             which is a regular expression to be matched to, then the front of the
             sequence is chopped off, the pattern is chopped off and recursively called
             */
#ifdef INNER_DEBUGGING
                // even if processing is via clues the input
                // pattern must be parsed for grabs
		//if (bid20_debugging == 3 && vsa[depth].str[4] == '2') {
		//	fprintf(stderr,"debugging:%s\n",vsa[depth].str);
		//}
		if (bid20_debugging == 3 && vsa[depth].str[23] == '3' && vsa[depth].str[24]=='N') {
			fprintf(stderr,"found1:%s\n",vsa[depth].str);
			bid20_debugging = 7;
		}
		if (bid20_debugging == 7) {
			fprintf(stderr,"found2:%s\n",vsa[depth].str);
			bid20_debugging = 8;
		}
		if (bid20_debugging == 8 && strcmp("_WEAK_TWO:2N:P:3N",vsa[depth].str)==0) {
			fprintf(stderr,"found3:%s %u\n",vsa[depth].str,vsa[depth].pos->id);
			bid20_debugging = 9;
		}
		if (bid20_debugging == 9 && strcmp(":2N:P:3N",vsa[depth].str)==0) {
			fprintf(stderr,"found4:%s %u\n",vsa[depth].str,vsa[depth].pos->id);
		}
		if (bid20_debugging > 0 && strcmp("NT_BID:3N",vsa[depth].str)==0) {
			fprintf(stderr,"found5:%s %u\n",vsa[depth].str,vsa[depth].pos->id);
		}
		if (bid20_debugging > 0 && strcmp("NT_BID3N",vsa[depth].str)==0) {
			fprintf(stderr,"found6:%s %u\n",vsa[depth].str,vsa[depth].pos->id);
		}
		if (bid20_debugging > 0 && strcmp("NT_BID:P:3N",vsa[depth].str)==0) {
			fprintf(stderr,"found7:%s %u\n",vsa[depth].str,vsa[depth].pos->id);
		}
		if (bid20_debugging > 0 && strcmp("NT_BIDP:3N",vsa[depth].str)==0) {
			fprintf(stderr,"found8:%s %u\n",vsa[depth].str,vsa[depth].pos->id);
		}
		if (bid20_debugging > 0 && strcmp("NT_BID3N:P",vsa[depth].str)==0) {
			fprintf(stderr,"found8:%s %u debugging halted\n",vsa[depth].str,vsa[depth].pos->id);
			bid20_debugging = 0;
		}
		//if (bid20_debugging && vsa[depth].pos->id == 5733) {
		//	fprintf(stderr,"found 9(5733):%s\n",vsa[depth].str);
		//	bid20_debugging = 11;
		//}
		if (bid20_debugging && vsa[depth].pos->id == 2365) {
			fprintf(stderr,"found 9(2365):%s\n",vsa[depth].str);
		}
		if (bid20_debugging && vsa[depth].pos->id == 20724) {
			fprintf(stderr,"found 9(20724):%s\n",vsa[depth].str);
		}
		if (bid20_debugging && vsa[depth].pos->id == 17314) {
			fprintf(stderr,"found 9(17314):%s debugging halted\n",vsa[depth].str);
			bid20_debugging = 0;
		}
		if (bid20_debugging && vsa[depth].pos->id == 5731) {
			fprintf(stderr,"found 9(5731 2N):%s\n",vsa[depth].str);
		}
		if (bid20_debugging && vsa[depth].pos->id == 5732) {
			fprintf(stderr,"found 9(5732 P after 2N):%s\n",vsa[depth].str);
		}
		if (bid20_debugging && vsa[depth].pos->id >= 28228 && vsa[depth].pos->id <= 28230) {
			fprintf(stderr,"found 9(28228):%s %u\n",vsa[depth].str,vsa[depth].pos->id);
			bid20_debugging = 0;
		}
		if (bid20_debugging && strcmp(":P:3N",vsa[depth].str)==0) {
			fprintf(stderr,"found9:%s %u\n",vsa[depth].str,vsa[depth].pos->id);
		}
		if (bid20_debugging && strcmp("_CHECKP:P",vsa[depth].str)==0) {
			fprintf(stderr,"found9:%s %u\n",vsa[depth].str,vsa[depth].pos->id);
		}
		if (bid20_debugging && strcmp(":3N",vsa[depth].str)==0) {
			fprintf(stderr,"found9:%s %u\n",vsa[depth].str,vsa[depth].pos->id);
		}
		if (bid20_debugging && strcmp("P:3N",vsa[depth].str)==0) {
			fprintf(stderr,"found9:%s %u\n",vsa[depth].str,vsa[depth].pos->id);
		}
#endif
                #ifdef CSW
                        TRIGGER(600,do_log(1,20,"far_match_node of len:%u for depth:%u",vsa[depth].len,depth));
                        if (depth == 0) {
                            if (grab_index >= 0) {
                    TRIGGER(205,do_log(1,10,"4 G: a:%c b:%c c:%c d:%c %u\n",grab[0],grab[1],grab[2],grab[3],grab_index));
                            }
                    TRIGGER(205,do_log(1,10,"BS4 M: %s %s %s\n",vsa[depth].str,vsa[depth].pos->exp.c_str(),vsa[depth].pos->constraint.c_str()));
                        }
                #endif

                for (int i=0; i<vsa[depth].len; i++) {
                    if (vsa[depth].str[i]==':') {
                        assert(vsa[depth].current_bidder >=0 && vsa[depth].current_bidder < 4);			
                        vsa[depth].current_bidder = (vsa[depth].current_bidder + 1) % 4;
        				if (vsa[depth].current_bidder == vsa[0].current_bidder) 
        					vsa[depth].current_round++;
                    }
                    }

                fval[69] = vsa[depth].current_bidder;   // puts current bidder into this forth variable

                fval[80]= depth;

                if (strchr((CHARCAST) vsa[depth].pos->exp.c_str(),'@') != NULL) flag = 1;
                else flag = 0;
                if (vsa[depth].len == (char) strlen((char *)vsa[depth].str)) flag |= 2;
#ifndef HOFFER
                if ((flag & 1 && des[0] != 'P') && vsa[depth].pos->generate_command.c_str() == "") flag |= 4;
#else
                if ((flag & 1 && des[0] != 'P') && (strcmp(vsa[depth].pos->generate_command.c_str(),"")==0)) flag |= 4;
#endif
                if (flag == 2 && vsa[depth].pos->priority == 0 && vsa[depth].pos->comment == 0) flag |= 4;


        if (flag & 2 && !(flag & 4)) {                  // matched full length but not if @ sign with no generate command present
            #ifdef CSW
                        ONE_SHOT(206,do_log(1,10,"BS: full length reached"));
                        CSW_EXEC(do_log(1,20,"matched full length %s %s %u",vsa[depth].str,vsa[depth].pos->exp.c_str(),vsa[depth].pos->id));
                        TRIGGER(1200,do_log(1,20,"matched full length %s %s %u",vsa[depth].str,vsa[depth].pos->exp.c_str(),vsa[depth].pos->id));
            #endif

                 
            /* we have matched the full length of sequence input */
            // << log2 >>
            if (des[0] == 'R') 
            //  << recurse >>
                         // the pos->comment != NULL was added 2003-2-8
                         // this keeps searching for a commented node not just the first matched node
            {
            #ifdef CSW
                            ONE_SHOT(206,do_log(1,10,"BS:Recursing"));
            #endif

                if (vsa[depth].pos->priority > 0) { int i=0;
                                
                                // FIX TRICKY 210322
                                for (i=0; i<26 && i<=grab_index; i++) grab_info[i] =  grab[i].c;
                                grab_info[i]='\0';
                            assert(vsa[depth].current_bidder >=0 && vsa[depth].current_bidder < 4);	
                                do_range(vsa[depth].pos,vsa[depth].current_bidder);
                                for (i=0; i<8; i++) forth_info[i]=32+ fval[70+i];
                                forth_info[8]='\0';
            //					if (strcmp(des,"RECURSE1")==0) 
                                if (des[7] == '1') 
                                    set_id(vsa[depth].pos->id,vsa[depth].pos->priority+10,(char *)grab_info,(char *)forth_info);
            //					else if (strcmp(des,"RECURSE2")==0)
                                else if (des[7]=='2')
                                    set_id(vsa[depth].pos->id,vsa[depth].pos->priority,(char *)grab_info,(char *)forth_info);
                                }
                            }
            else if (des[0] == 'E')
                         {
                            if (1) {
#if !defined(EXCLUDE_BIDINFO)
            					int round, seat;
#endif
#ifdef CSW
                                ONE_SHOT(205,do_log(1,10,"BS: EVALUATE non-recursively\n"));
                                TRIGGER(1200,do_log(0,10,"E %04u %s %s %s",depth,vsa[depth].str,vsa[depth].pos->exp.c_str(),vsa[depth].pos->constraint.c_str()));
#endif

                                *vsa[depth].priority = vsa[depth].pos->priority;

                                grab_index = vsa[depth].pos->num_of_grabs;  // found to be missing with KDIFF3 191011
                                // grab_index = temp_g_index;
                                for (int i2=0; i2<GRAB_MAX; i2++)  result[i2]=grab[i2];
                    //			vsa[depth].current_bidder = temp_bidder;
                                print_stack[depth].node = vsa[depth].pos;
                                print_stack[depth].match_length=vsa[depth].len;
                            assert(vsa[depth].current_bidder >=0 && vsa[depth].current_bidder < 4);	
                                print_stack[depth].bidder=vsa[depth].current_bidder;
                                print_stack_top = depth;
                                // fprintf(stderr,"PST2:%d ",print_stack_top);
                                // for (int i3=0; i3<4; i3++)
                                    // temp_info.bid[i3] = bid_info.bid[i3];
#if !defined(EXCLUDE_BIDINFO)
            					get_current_round_and_seat(depth,&round,&seat);
                                copy_bid_info(&temp_info,&bid_info[round][seat]);
#endif
                                // do_constraint_macros >>
                                // added this next section 11/21/01
                                // This handles constraint macros such as MACROFCM which sets up the
                                // FOUR vs FIVE card majors definition.
                                // Note that this can be done by just having different recognizers in the
                                // bidding system with different comments, rather than the same comment
                                // with a macro in it.
                                // This next search for @ is not relevant to the NO_MACROS_VERSION

                                #ifndef NO_MACROS_VERSION                
                                                    if (vsa[depth].pos->constraint[0] == '@') {
                                #ifdef CSW
                                                         ONE_SHOT(206,do_log(1,10,"BS: doing macro processing"));
                                #endif
                                                        domacro((char *)vsa[depth].pos->constraint,vsa[depth].current_bidder%2);
                                                        }
                                #endif
                                // do_constraint_macros >>
                                TRIGGER(206,do_log(1,10,"BS: Calling do_range"));
                                // ALOG("calling do_range");
                                do_range(vsa[depth].pos,vsa[depth].current_bidder);
                                // ALOG("back from do_range");
                                }
#if defined(ANDROID)
                             TRIGGER(1200,do_log(1,10,"returning from evaluate in bid_search"));
#endif
                return;
                            }
            // evaluate_non-recursively >>
            else if (vsa[depth].pos->priority > *vsa[depth].priority) 
            // constraint_match >>
            {
                                 
                TRIGGER(1200,do_log(1,20,"priority is higher...in constraint_match table:%u VSA:%u %u %s %s\n",table,depth,vsa[depth].pos->id,vsa[depth].str,vsa[depth].pos->constraint.c_str()));

#ifdef CSW
            //	CSW_EXEC(do_log(1,20,"table:%u",table));
            	if (depth > 15) CSW_EXEC(do_log(1,20,"in constraint_match table:%u VSA:%u %u %s %s\n",table,depth,vsa[depth].pos->id,vsa[depth].str,vsa[depth].pos->constraint.c_str())); 
            #endif
#ifndef HOFFER
                            if (vsa[depth].pos->constraint.c_str() != "") {
#else
                            if (strcmp(vsa[depth].pos->constraint.c_str(), "") != 0) {
#endif
                            
            #ifdef CSW
                                    CSW_EXEC(do_log(1,20,"to rx_match_depth table: %u depth:%u id:%5u %s\n",table,depth,vsa[depth].pos->id,des));
            #endif
                                // good place to stop
                                // ALOG("rx_match_depth to description:%s",des);
                                
                                if (!rx_match_depth(depth,des)) ; // ALOG("rx_match_depth fails");
                                // rx_match_depth >>
                                else                        {
#if defined(CSW)
                                    TRIGGER(1200,do_log(1,30,"rx_match_depth matched priority:%u->%u depth:%u %u %s %s\n%s\n",vsa[depth].pos->priority,*vsa[depth].priority,depth,vsa[depth].pos->id,vsa[depth].str,vsa[depth].pos->constraint.c_str(),des));
TRIGGER(1605,do_log(1,30,"rx_match_depth matched priority:%u->%u depth:%u %u %s %s\n%s\n",vsa[depth].pos->priority,*vsa[depth].priority,depth,vsa[depth].pos->id,vsa[depth].str,vsa[depth].pos->constraint.c_str(),des));
#endif
                                                        fval[79]=1;
                                                        *vsa[depth].priority = vsa[depth].pos->priority;
                                                        // << trigger priority >>
                                                        #ifdef CSW
                                                                                TRIGGER(1200,do_log(1,300,"priority set to %u",*vsa[depth].priority));
                                                        #endif
                                                        // << trigger priority >>
                                                        // << copy_to_print_stack >>
                                                                                print_stack[depth].node=vsa[depth].pos;
                                                                                print_stack[depth].match_length=vsa[depth].len;
                                                                                print_stack[depth].bidder=vsa[depth].current_bidder;
#if !defined(EXCLUDE_BIDINFO)
                                                                                { int round, seat;
                                                                                get_current_round_and_seat(depth,&round,&seat);
                                                                                copy_bid_info(&temp_info,&bid_info[round][seat]);
                                                                                //for (int i=0; i<4; i++)
                                                                                    //temp_info.bid[i] = bid_info.bid[i];
                                                                                    
                                                                                }
#endif
                                                                                pstack_temp_top = depth;
                                                                                for (int i2=0; i2 <= depth; i2++)  {
                                                                                    pstack_temp[i2].node = print_stack[i2].node;
                                                                                    pstack_temp[i2].match_length=print_stack[i2].match_length;
                                                                                    pstack_temp[i2].bidder=print_stack[i2].bidder;
                                                                                    }
                                                        // copy_to_print_stack >>
                                                        // log3 >>
                                                        #ifdef CSW
                                                                                if (grab_index >= 0) {
                                                                                    TRIGGER(206,do_log(1,10,"BS8 G: a:%c b:%c c:%c d:%c %u\n",grab[0],grab[1],grab[2],grab[3],grab_index));
                                                                                }
                                                                                TRIGGER(206,do_log(1,10,"BS8 RX  :%5u %s\n",vsa[depth].pos->id,des));
                                                        #endif
                                                        // Once we have matched a pattern it is then checked to see if a trail
                                                        // of clues should be left behind to reduce processing. This is indicated
                                                        // in the tree by having the exp contain '~' or '`'. The former means
                                                        // set clue_flag and no further processing at this level, i.e., among
                                                        // this patterns siblings. The latter sets the clue_flag on but the
                                                        // remembered nodes are set only down to this patterns parent. Thus siblings
                                                        // of this pattern can still be searched, but prevous patterns are set
                                                        // in stone; these are processed by the if statement in this function
                                                        // that checks to see if the clue is set for the current level, if it is
                                                        // then the matching node is used and none others have to be searched;
                                                        // The node itself has to be run through the pattern matching to get all
                                                        // the grabs set up correctly, but processing time is saved because no
                                                        // other nodes have to be searched.

                                                                                if (strchr((CHARCAST) vsa[depth].pos->exp.c_str(),'~')!= NULL) {
                                                                                    clue_flag=1;
                                                        #ifdef CSW
                                                            TRIGGER(602,do_log(1,10,"clue_flag set to 1 for id:%u %s",vsa[depth].pos->id,vsa[depth].pos->exp.c_str()));
                                                        #endif
                                                                                    }
                                                                                else if (strchr((CHARCAST) vsa[depth].pos->exp.c_str(),'`') != NULL) {
                                                                                    clue_flag = 2;
                                                        #ifdef CSW
                                                            // TRIGGER(602,do_log(1,10,"clue_flag set to 2 for id:%u %s",vsa[depth].pos->id,vsa[depth].pos->exp.c_str()));
                                                        #endif
                                                                                    }

                                                                                /***********   STORE THE CLUES  *************/
                                                                                // clue_flag = 0;
                                                                                
                                                                                if (clue_flag) {
                                                                                      int i3;

                                                                                    for (i3=0; i3<C_MAX; i3++) {
                                                                                        if (i3 == depth && clue_flag == 2) break;
                                                                                        if (i3 > depth) break;
                                                                                        k=0;
                                                                                        if (print_stack[i3].node != NULL)
                                                                                            k = (int)print_stack[i3].node->skip;
                                                                                        assert(k>=0 && "bad node skip value");
                                                                                        btemp2 = print_stack[i3].node;
                                                                                        while (k--) {
                                                                                            if (btemp2->sibling == NULL) {
                                                                                                throw std::logic_error("should not encounter null sibling in setting clues");
                                                        #ifdef CSW
                                                            TRIGGER(602,do_log(1,40,"NULL sibling problem for id:%u %s",vsa[depth].pos->id,vsa[depth].pos->exp.c_str()));
                                                        #endif
                                                                                                break;
                                                                                                }
                                                                                            btemp2 = btemp2->sibling;
                                                                                            // if (btemp2->line_number == 551) printf("here is is\n");
                                                                                            }
                                                        #ifdef CSW
                                                            TRIGGER(601,do_log(1,10,"clue_temp[%u]=%u(id)",i3,btemp2->id));
                                                        #endif
                                                                                        clue_temp[i3].node=btemp2;
                                                                                        clue_temp[i3].match_length = print_stack[i3].match_length;
                                                                                        }
                                                                                    clue_temp[i3].node = NULL;
                                                                                    clue_flag = 1;
                                                                                    }
                                                        // << clues >>
                                                            // maybe FIX TRICKY 210322
                                                        for (int k=0; k<GRAB_MAX; k++)  result[k]=grab[k];
                                    // ALOG("setting print_stack_top to depth:%d",depth);
                                                        print_stack_top = depth;
                                                        // fprintf(stderr,"PST5:%d ",print_stack_top);
                                                        grab_index = vsa[depth].pos->num_of_grabs;  // found to be missing with KDIFF3 191011
                                                        // grab_index = temp_g_index;
                                                        assert(temp_bidder >= 0 && temp_bidder < 4);					
                                                        vsa[depth].current_bidder = temp_bidder;
                                						vsa[depth].current_round = temp_round;
                                                        }

                                // << rx_match_depth >>
                                if (optimize & 1) 
                                // << delete_rx_node >>
                                {
                                #ifdef CSW
                                                         ONE_SHOT(206,do_log(1,10,"BS: delete rxnode"));
                                #endif
                                                        delete_rxnode(vsa[depth].pos->con_pat);
                                                        vsa[depth].pos->con_pat = NULL;
                                                        }

                                //  << delete_rx_node >>
                                }
            } // constraint is not null

            // << constraint_match >>
            }
        else if (vsa[depth].pos->skip) 
        // << do_skip_overs >>
                 //  Skipping is identified in the file to avoid unnecessary processing.
                 // it is done in a section where the siblings are ordered by priority; 
                 // once a sibling if found and has been marked for skipping it jumps to the last sibling to continue
                 // the last sibling is a collection area and must be sending off to the same section for all of the siblings 
                 //   " 2H;2" !TWO_OVER_ONE! %5% !! %:D[6-9]H%
                 //   " 2D;1" !TWO_OVER_ONE! %4% !! %:D[45]D
                 //   " {2{[DH]@TWO_OVER_ONE! %0% !! %.%
                 // the numbers after the semi-colon show how far down to skip

                 // << skipping_documentation >>
                 //  Non-equivalent rules in a section point down to a sibling that has
                 //  children available for further processing. No children are available
                 //  for further processing for any siblings in the skipped section.

                 /* This section processes the current line. Then goes down the
                  siblings and takes the skipped to siblings children as the next
                  section to process.   */
                 // << skipping_documentation >>
        {
#if defined(ALOGGING)
            ALOG("bidding:skipping");
#endif
                    i = (int)vsa[depth].pos->skip;
                    do_range(vsa[depth].pos,vsa[depth].current_bidder);
                    while (i--) {
                        vsa[depth].pos = vsa[depth].pos->sibling;
                        }

                                                                                    // SKIP_OVER processes the last sibling, thus the big change.
                    print_stack[depth].node = vsa[depth].pos;
                    print_stack[depth].match_length = vsa[depth].len;
                    print_stack[depth].bidder = vsa[depth].current_bidder;
                    vsa[depth+1].str = vsa[depth].str + vsa[depth].len;
                    vsa[depth+1].pos = vsa[depth].pos->child;
                    vsa[depth+1].priority = vsa[depth].priority;
                    vsa[depth+1].current_bidder = vsa[depth].current_bidder;
        			vsa[depth+1].current_round = vsa[depth].current_round;
                    bid_search(depth+1);
        }


        //  << do_skip_overs >>
        else if (vsa[depth].pos->pattern->type == RXNT_STR) 
        //  << process_string >>
        { bool constraint_matched = true;
            // ALOG("bidding:pattern type of string encounterd");
        #ifdef CSW
                    CSW_EXEC(do_log(1,20,"table:%u OS: pattern type of string encountered",table));
        #endif
                        { int str_len,pat_len;
                    str_len = (int)strlen((char *)&vsa[depth].str[(unsigned int)vsa[depth].len]);
                    pat_len = (int)strlen((char *)vsa[depth].pos->pattern->str); str_len += (pat_len + 1);
                    if (str_len < pat_len)
                        printf("foundit0");
        #ifndef NEW_TEMP
                    vsa[depth].temp = new char[i];
        #endif
        #ifdef NOSUB
                    strncpy(vsa[depth].temp,(char *) vsa[depth].pos->pattern->str,i);
        #else
                    { int ii,jj;
                     ii=0; jj=0;
                     vsa[depth].temp[MAX_SEQ_LEN-2]='@';
                    do {
                        if (vsa[depth].pos->pattern->str[jj] == '#') {
                            vsa[depth].temp[ii] = vsa[depth].grab[vsa[depth].pos->pattern->str[jj+1]-'a'].c;
                            jj++;
                            }
                        else vsa[depth].temp[ii] = vsa[depth].pos->pattern->str[jj];
                        jj++; ii++;
                        } while (vsa[depth].temp[ii-1] && ii <= str_len);
                    assert(vsa[depth].temp[MAX_SEQ_LEN-2] == '@');
                    }
        #endif
        #ifdef CSW
                    CSW_EXEC(do_log(1,20,"table:%u OS: pattern type of string encountered 2",table));
        #endif
                    if (des[0] == 'E' || des[0] == 'R') ;
                    else if ((strcmp(vsa[depth].temp,"CONV6_CHECK") == 0 ||
						strcmp(vsa[depth].temp,"CONV6_CONTROLS") == 0) && des[0]=='P') {   // description is EVALUATE or RECURSE when just checking sequences.
                             int check_flag=false;
                             
                             GRAB my_grab_temp[GRAB_MAX];
                             int my_print_stack_top;
                             int my_grab_index;
#ifdef CSW
					if (depth>100)
						TRIGGER(1602,do_log(1,20,"CONV6_CHECK encountered, depth:%u for id:%u",depth,vsa[depth].pos->id));
#endif
                    
                            rx_stack_index = EMPTY;
                                for ( k=0; k<GRAB_MAX; k++)  my_grab_temp[k]=grab[k];
                                my_print_stack_top = print_stack_top;
                                my_grab_index = grab_index;
                    
                        {
        #ifdef CSW
                    CSW_EXEC(do_log(1,20,"table:%u OS: pattern type of string encountered 3",table));
        #endif
                        
                         int val; //, current_bidder ,result;
                        char templine[40];
                        // current_bidder=f_eval((const char *)"69H");
                        // TRIGGER(602,do_log(1,10,"current bidder is %u",current_bidder));
                        for (int iii=0; iii<8; iii++) {
                            val = 70 + iii;
                            sprintf(templine,"%uH",val);
                            // result = f_eval(templine);
                            // TRIGGER(602,do_log(1,10,"value for %u is %u",val,result));
                            }
                        }
        #ifdef CSW
                    CSW_EXEC(do_log(1,20,"table:%u before colon_count",table));
        #endif
                        { int colon_count=0, bids=0,in_bid=0; //,original_bidder , current_bidder, last_bidder;
                            // first use original sequence to find the bidders
        				for (int iiii=0; vsa[0].str[iiii] != '\0'; iiii++)  
                                if (vsa[0].str[iiii]==':') colon_count++;
                            // original_bidder = vsa[0].current_bidder;
                            // last_bidder=(original_bidder+colon_count) % 4;
                            // current_bidder = vsa[depth].current_bidder;
         
                           // bids will be the number of bids left and 2 requires processing
        					 for (int iiii=0; vsa[depth].str[iiii] != '\0'; iiii++) {  
        						 if (vsa[depth].str[iiii]==':') { colon_count++; in_bid=false; }
        						 else if (!in_bid) { in_bid=true; bids++; }
        						}
#define NEW_VERSION_2019
#ifdef NEW_VERSION_2019
							// was colon_count == 2
							// if ( bids == 2 && (current_bidder + 2) % 4 == last_bidder) {
							 if (bids > 2) {
								 check_flag = false;
 /* for debugging during the time that the RAVEN was not having clues cleared
								 if (vsa[depth].str[9] == 'X' && vsa[depth].str[7]=='H' &&
									 vsa[depth].str[6]=='5')
									 fprintf(stderr,"letting %s through, bids:%u\n",vsa[depth].str,bids);
*/
								}
							else if ( bids == 2)
									check_flag=true;  // needs to have 2 bids to continue on
							else check_flag=false;

#else                            
                            // if (colon_count==0 && current_bidder==last_bidder) check_flag=true;
                            //else if (colon_count == 2 && (current_bidder + 2) % 4 == last_bidder) check_flag=true;
                            if (bids == 2)
        						check_flag = true;
                            else {
        						check_flag=false;
								constraint_matched = false;
								}
#endif

                            
                            
                        }
                        
/* 190904 - the logic here was corrected. Either the check_flag being false or if
that is not the case then if the rx_match_depth is false will cause constraint_matched
to be false and the match fails. However, recursion continues. If the constraint_matched
is true, then this section is passed through also */
                        
                        
                        if (check_flag) {
                            if (!rx_match_depth(depth,des)) {
                                constraint_matched = false;
                            }
                    else {
        #ifdef CSW
                    TRIGGER(602,do_log(1,10,"check:  constraint_matched continues as true"));
                        TRIGGER(602,do_log(1,10,"%u %s",depth,des));
                        TRIGGER(602,do_log(1,10,"%s",vsa[depth].str));
        #endif
                    }}
                    if (depth > 15) CSW_EXEC(do_log(1,20,"table:%u after rx_match_depth",table));
                        if (optimize & 1) 
                        // << delete_rx_node >>
                        {
                        #ifdef CSW
                                                 ONE_SHOT(206,do_log(1,10,"BS: delete rxnode"));
                        #endif
                                                delete_rxnode(vsa[depth].pos->con_pat);
                                                vsa[depth].pos->con_pat = NULL;
                                                }

                        //  << delete_rx_node >>
                                for ( k=0; k<GRAB_MAX; k++)  grab[k] = my_grab_temp[k];
                                print_stack_top = my_print_stack_top;
                                // fprintf(stderr,"PST3:%d ",print_stack_top);
                                grab_index = my_grab_index;
                        
                        
                        
                        }
/* 180201
somehow the next if was changed to an else if so the tilde removal was not occurring.
It may not be as simple as replacing it becuase there is something going on with CONV6_CHECK
*/                        
                        
                if (vsa[depth].temp[pat_len-1] == '~') {
                        vsa[depth].temp[pat_len-1]='\0';
                        --str_len;
                        }
                if (constraint_matched) {
                    {
#if !defined(linux)
                        int rr1,rr2,rr3;
                    rr1 = (int)strlen(vsa[depth].temp);
                    rr2 = (int)strlen(&vsa[depth].str[vsa[depth].len]);
                    rr3 = str_len-pat_len;
#else
                        int rr1,rr3;
                    rr1 = (int)strlen(vsa[depth].temp);
                    rr3 = str_len-pat_len;
#endif
                    if (str_len<pat_len)
                        printf("found it1");
                    if (rr1 + rr3 > MAX_SEQ_LEN)
                        printf("bad copy");
                    else strncat(vsa[depth].temp,(char *)&vsa[depth].str[(unsigned int) vsa[depth].len],str_len - pat_len);
                    if (vsa[depth].temp[MAX_SEQ_LEN-2] != '@')
                        printf("found it");
                    assert(vsa[depth].temp[MAX_SEQ_LEN-2]=='@');
                    }
                        }
                    // fprintf(stderr,"PST4x:%d ",print_stack_top);
                    if (print_stack_top<VSA_MAX) {
                        print_stack[depth].node=vsa[depth].pos;
                        print_stack[depth].match_length=vsa[depth].len;
                        print_stack[depth].bidder=vsa[depth].current_bidder;
                        }
                    else {
                        my_error("bad print_stack_top 2");
                        // fprintf(stderr,"\nbad pst %d\n",print_stack_top);
                    }
                    
                    // << forced_documentation >>

                                            /* In the forced situation, here is what should happen.
                                            First, a forced situation is one wherein a sequence can be reached
                                            from many different bidding sequences; typically Blackwood would be
                                            such a situation. In that case you want to used the priority from the
                                            node just before you go off to Blackwood. If you do not then any sequence
                                            that is similar can go off to Blackwood as responder and not have met
                                            the constraints set up by the Blackwood originator. That is the reason
                                            that this section exists.

                                                Here I first find out when a situation is FORCED, when I do I
                                            check for a constraint match, and if it is the original time through this
                                            section the des is stored as fdes, the forcing description. It then
                                            accompanies the forcing priority, and the two parameters to bid_search
                                            make sure that bids are not traversed that do not meet original constraints.

                                                If this doesn't make sense yet think about the following:
                                            1) bids and constraints are often set up in the following manner:
                                                "3N"	!!	%6% %:L~6<%
                                                "3N"	!!	%5% %:D......[23]%
                                                " :P:4N" !!	%5% %P~20>%
                                            2) In the above situation both of the constraints can lead to the 4N
                                                subsequent bids; i.e., either way that we get here it doesn't matter
                                                how, just continue to bid.
                                            3) consider this situation however:
                                                "{{{{{{4N<#j=4><#k=7><#l=#d>@SETACES" !! %7% %:D[^:]*[4-9]#d&&:L~2<%
                                                "{{{{{{4N<#j=4><#k=6><#l=N>@SETACES"  !! %6% %:L~2<%
                                            4) in the #3 situation there is nothing in the priorities to keep subsequent
                                               bids from entering Blackwood, yet not qualifying with the constraints.
                                               The other problem is that the first such sequence to pass through will
                                               be the entitled sequence. That is because since the priorities do not change
                                               the first bid through gets that priority and will be higher than any others
                                               that come afterwards.
                                            5) the solution is that there must be a window that only allows bids with a
                                               match to the constraint through. This is indicated to the system by having
                                               a ~ in the @ pattern, for example, @SETACES~. In that situation a bid cannot
                                               pass through as it normally would without a further check on its validity.
                                            6) If this doesn't make sense look and the code and come back in one week. Think
                                               about how subsequent bids do not have to match constraints as they pass through
                                               which could be a problem in many situations, Blackwood being the best example.

                                               */
                    // << forced_documentation >>
#ifdef CSW
                    CSW_EXEC(do_log(1,20,"table:%u id:%u BS-OS: calling do_range",table,vsa[depth].pos->id));
#endif
                    if (vsa[depth].current_bidder < 0 || vsa[depth].current_bidder > 3)
                        do_log(1,40,"bad current bidder");
                    do_range(vsa[depth].pos,vsa[depth].current_bidder);            
                    vsa[depth+1].str = vsa[depth].temp;
                    vsa[depth+1].pos = tree;
                    vsa[depth+1].priority = vsa[depth].priority;
                    vsa[depth+1].current_bidder = vsa[depth].current_bidder;
        			vsa[depth+1].current_round = vsa[depth].current_round;
        #ifdef CSW
                    CSW_EXEC(do_log(1,20,"BS-OS: table:%u calling bid_search recursively",table));
#if defined(ANDROID)
                    TRIGGER(1200,do_log(1,30,"BS-OS: table:%u calling bid_search recursively",table));
#endif
                            
        #endif
                    bid_search(depth+1);
        #ifndef NEW_TEMP
                    delete(vsa[depth].temp);
        #endif
        //			delete(nv);
                }
            }

        // << process_string >>
        else if (vsa[depth].pos->child) 
        // << bid_search_child >>
                {
                    // ALOG("bidding:checking child");
                    print_stack[depth].node = vsa[depth].pos;
                    print_stack[depth].match_length = vsa[depth].len;
                    print_stack[depth].bidder = vsa[depth].current_bidder;
        //			i = (vsa[depth].current_bidder << 8) + depth + 1;
                    do_range(vsa[depth].pos,vsa[depth].current_bidder);

        //            var_struct *nv2 = new var_struct;
        //			nv2->str = vsa[depth].str + vsa[depth].len;
        //			nv2->pos = vsa[depth].pos->child;
        //			nv2->priority = vsa[depth].priority;
        //			nv2->p_index = i;
                    vsa[depth+1].str = vsa[depth].str + vsa[depth].len;
                    vsa[depth+1].pos = vsa[depth].pos->child;
                    vsa[depth+1].priority = vsa[depth].priority;
                    if (vsa[depth].current_bidder < 0 || vsa[depth].current_bidder > 3) {
                        do_log(1,40,"bad current bidder");	
                    }
                    vsa[depth+1].current_bidder = vsa[depth].current_bidder;
        			vsa[depth+1].current_round = vsa[depth].current_round;
        #ifdef CSW
                    ONE_SHOT(206,BidMessage("BS-OS: bid_search for a child, line %u",__LINE__));
        #endif
                    bid_search(depth+1);
        //            delete nv2;
        //			bid_search(vsa[depth].str+vsa[depth].len,vsa[depth].pos->child,vsa[depth].priority,i);
                    }

        // << bid_search_child >>
        }
    if (des[0] =='E' && print_stack_top != -1) {
                // ALOG("bidding:stop further searches");
                return;      // stop further searches first one is enough; non-recursive
            }
    grab_index = temp_g_index;
    vsa[depth].current_bidder = temp_bidder;
	vsa[depth].current_round = temp_round;

skip:
             // bidding:at skip");
    if (depth <= 1) {
                TRIGGER(1200,do_log(1,30,"at skip\n"));
                if (vsa[depth].pos->sibling==NULL) { TRIGGER(1200,do_log(1,30,"at skip; no sibling\n")); }
            }
    if (vsa[depth].pos->sibling  && !vsa[depth].clue_skip) 
    // << bid_search_sibling >>
         {
    //		i = depth + (vsa[depth].current_bidder << 8);
    //		var_struct *nv3 = new var_struct;
    //		nv3->str = vsa[depth].str; nv3->pos = vsa[depth].pos->sibling;
    //		nv3->priority = vsa[depth].priority; nv3->p_index = i;

    /* vsa[depth].grab holds a temporary set of grabs, since in many
    situations the grabs below GRAB_MAX are manipulated in recursive
    sections and need to be updated. */
    // i<grab_index changed to i <= grab_index 5/27/01 because grab_index off is at -1
            for (i=0; i<= grab_index; i++)
                grab[i] = vsa[depth].grab[i];
    #ifdef CSW
            ONE_SHOT(206,do_log(1,10,"pop_forth line: %u",__LINE__));
    #endif
            pop_forth(vsa[depth].current_bidder % 2);

    #ifdef HIGH_LOW
            for (i=0; i<4; i++) {
                high_low[i][0] = vsa[depth].high_low[i][0];
                high_low[i][1] = vsa[depth].high_low[i][1];
                }
    #endif
            vsa[depth].pos = vsa[depth].pos->sibling;
    #ifdef CSW
            ONE_SHOT(206,do_log(1,10,"BS-OS: bid_search for a sibling, line %u",__LINE__));
    #endif
#ifdef REDO_BID_SEARCH
             grab_index = temp_g_index;
             vsa[depth].current_bidder = temp_bidder;
             vsa[depth].current_round = temp_round;
             if (depth <= 1) {
                 TRIGGER(1200,do_log(1,30,"goto redo_bid_search\n"));
                }
             goto redo_bid_search;
#else
            TRIGGER(1200,do_log(1,30,"bidding:calling bid_search for depth:%d",depth));
            bid_search(depth);
#endif
            }

    // << bid_search_sibling >>
     // same level, search next sibling
    grab_index = temp_g_index;
    vsa[depth].current_bidder = temp_bidder;
	vsa[depth].current_round = temp_round;
    TRIGGER(1200,do_log(1,30,"bidding:done with bid_search at depth:%d",depth));
}


//@+node:ral.20180109191625.12: ** int mymatch
int mymatch(const char *pattern, const char *str) {
    int ans;
    reset_lex(pattern);
    printf("\n-> yyparse");
    ans = yyparse();
    printf("\n<- yyparse");
    count_grabs(rx_root,0);
    ans = rx_match(rx_root,str);
    return ans;
}
// this assumes a properly formatted sequence without a terminating colon

//@+node:ral.20180109191625.13: ** int getseat
int getseat(char *seq) {
    int position=0,seat=1;

    while (seq[position] && position < MAX_SEQ_LEN) {
        if (seq[position++] == ':') seat++;
        }
    return seat;
    }


//@+node:ral.20180109191625.14: ** void getprior
void getprior(int *d, char *l, char *dbl, const char *seq, int index)
{
    int len, bid;
    char temp[255];
    enum suits { clubs=1, diamonds, hearts, spades, notrump };

    strcpy(temp,seq);
    len = (int) strlen(seq);
    if (len <= 1) return;

    bid = seq[len-1];
    if (bid == 'P') {                /* no info from a pass */
        temp[len-2]='\0';
        getprior(d, l, dbl, temp, index+1);
        }
    else if (bid == 'R') {           /* redouble cancels any further doubles */
        temp[len-2]='\0';
        if (*dbl != ' ') BidMessage("Redouble not possible here");
        *dbl = 'R';
        getprior(d, l,dbl, temp, index + 1);
        }
    else if (bid == 'X') {          /* redouble only after opp double */
        temp[len-2]='\0';
        if (index != 1 && *dbl != 'R') *dbl='X';      /* mark double except in redouble sequence */
        else if (index == 1  && *dbl != 'R') *dbl='N';   /* don't double partner */
			getprior(d, l, dbl, temp, index +1);
        }
    else {
        *l=seq[len-2]-'0';
        switch (seq[len-1]) {
            case 'C': *d=clubs; break;
            case 'D': *d=diamonds; break;
            case 'H': *d=hearts; break;
            case 'S': *d=spades; break;
            case 'N': *d=notrump; break;
            default: break;
            }
        if (index == 1 && *dbl == ' ') *dbl='N';     /* don't double partner */
        if (*l<1 || *l>7 || *d<clubs || *d>notrump) { *l=1; *d=clubs;}
        }
}


#define LEN_FOR_RANGE 60
char f1[LEN_FOR_RANGE],f2[LEN_FOR_RANGE];
char fmsg[LEN_FOR_RANGE];
extern void python_log(char *);
//@+node:ral.20180109191625.15: ** void do_range
void do_range(BIDNODE *pos, int current_bidder) {
char *rp;
int f1_flag, f2_flag,i,j;
int f1_val=0, f2_val=0;
f1_flag = f2_flag = 0;

if (current_bidder % 2 == 1 && !pos->range2.empty())   // EW bidder with macro different from NS
    rp = (char *) pos->range2.c_str();
else if (!pos->range1.empty()) rp = (char *) pos->range1.c_str();  // NS bidder with macro
else rp = (char *) pos->range.c_str(); // either bidder without any macros

    // if (current_bidder == 2) ALOG("LUDDY <%s>",rp);

if (rp == NULL) return;
if (rp[0] == '\0') {
#ifdef CSW
    ONE_SHOT(230,do_log(1,50,"NO STR line: %u",__LINE__));
#endif
    return;
    }
#ifdef CSW
ONE_SHOT(230,do_log(1,10,"do_range:<%s>",rp));
#endif
// for now forth can be detected by #F which it should be
// or if 69 is present (the forth variable for current bidder)
// then it is assumed to be a forth command.

i=0;j=0;
if (rp[0] == '#' && rp[1] == 'F') { f1_flag = 1; i=2; j=0; }
if (rp[0] == '6' && rp[1] == '9') { f1_flag = 1; i=0; j=0; }
if (!f1_flag) { i=0; sscanf(rp,"%u",&f1_val); }
f1[0]='\0'; f2[0]='\0';
while (f1_flag && j< LEN_FOR_RANGE-1) {
   if (rp[i] == ',') f1_flag = 0;
   else if (rp[i] == '\0') f1_flag = 0;
   else if (rp[i] == '\n') f1_flag = 0;
   else if (rp[i] == ')') f1_flag = 0;
   else { f1[j]=rp[i]; j++; i++; }
   if (!f1_flag) f1[j] = '\0';
   }
while (rp[i] != ',' && rp[i] != '\0') i++;
i++; j=0;
if (rp[i] == '#' && rp[i+1]=='F') { f2_flag = 1; i += 2; }
if (rp[i] == '6' && rp[i+1]=='9') { f2_flag = 1; }
if (!f2_flag) sscanf(&rp[i],"%u",&f2_val);  
while (f2_flag && j<LEN_FOR_RANGE-1) {
   if (rp[i] == ',') f2_flag = 0;
   else if (rp[i] == '\0') f2_flag = 0;
   else if (rp[i] == '\n') f2_flag = 0;
   else if (rp[i] == ')') f2_flag = 0;
   else { f2[j]=rp[i]; j++; i++; }
   if (!f2_flag) f2[j] = '\0';
   }

if (f1[0] != '\0') f1_val = f_eval(f1);   
if (f2[0] != '\0') f2_val = f_eval(f2);
if (f1_val > f2_val) {
	// a P that is known to produce this if points > 24 for BOTTOM and this is not a problem there
    if (pos->id != 18546) snprintf(fmsg,LEN_FOR_RANGE,"DR: %u %s reversed: %d %d\n",pos->id,rp,f1_val,f2_val);
#ifdef CSW
    ONE_SHOT(220,do_log(1,10,fmsg));
#endif
    fprintf(stderr,"%s", fmsg);
    }

// very important; this stores the TOP and BOTTOM values of this bid
if (f2_val < 0 || f2_val > 40 || f1_val < 0 || f1_val > 40) {
    snprintf(fmsg,LEN_FOR_RANGE,"badf1_val or f2_val");
}
if (f2_val < 0) f2_val = 0;
if (f2_val >37) f2_val = 37;
if (f1_val < 0) f1_val = 0;
if (f1_val >37) f1_val = 37;


snprintf(fmsg,LEN_FOR_RANGE,"%u %u G %u %u G",f2_val,70+current_bidder*2,f1_val,71+current_bidder*2);
#ifdef CSW
ONE_SHOT(220,do_log(1,10,fmsg));
CSW_EXEC(do_log(1,10,fmsg));
#endif

f_eval(fmsg);

//  CSW_EXEC(BidMessage("Do range returning"));
return;  

/*
#ifdef NO_MACROS_VERSION
#ifndef HOFFER
  if (current_bidder%2 == 1 && pos->range2.c_str() != "") rp = (char *) pos->range2.c_str();
#else
  if (current_bidder%2 == 1)
     rp = (char *) pos->range2.c_str();
#endif
  else if (!pos->range1.empty()) rp = (char *) pos->range1.c_str();
  else rp = (char *) pos->range.c_str();
#endif
*/
//    int i, flag;


/*	for (i=0, flag = 1; pos->range[i] != '\0' && flag == 1; i++)
    if (isdigit(pos->range[i])) ;
    else if (pos->range[i] == ',') ;
    else flag = 0; 

if (flag == 0) return;  */

/*
#ifndef NO_MACROS_VERSION
#ifndef PRODUCTION_VERSION
    sprintf(mbuf,"%s",rp);
#ifdef individual_dictionaries
    domacro((char *)mbuf,current_bidder+3);
#else
    domacro((char *)mbuf,current_bidder%2);
#endif
    sprintf(mbuf,"@dr(%s)",macro_temp);
    sprintf(mbuf2,"@dr2(%s)",macro_temp);
#else
    sprintf(mbuf,"@dr(%s)",rp);
    sprintf(mbuf2,"@dr2(%s)",rp);
#endif
#else // no macros version
    sprintf(mbuf,"@dr(%s)",rp);
    sprintf(mbuf2,"@dr2(%s)",rp);
#endif // no macros version

// For now (2003-3-16) I will implement the ranges with macros, but this
// will have to go also to reduce processing
// At least I dont have to do the macro processing above
// since that was done at the loading of the mb.sys file

//        python_log(mbuf);
#ifdef CSW
    ONE_SHOT(220,do_log(1,10,"range: calling domacro"));
#endif
#ifdef individual_dictionaries
//        python_log(mbuf2);
    domacro((char *)mbuf,current_bidder+3);
    domacro((char *)mbuf2,((current_bidder+2)%4)+3);
#else
    domacro((char *)mbuf,current_bidder%2);
#endif
*/
}

#ifdef PRIORITIZE
//@+node:ral.20180109191625.16: ** void prioritize
void prioritize(void) {
  int i,j,k,l,index;
  LEAF temp;

  index = 0;     /* input position increases as high priorities are found */
  for (i=9; i>0; i--)     /* cycle through the priorities */
    for (k=index; k<vsa_index ; k++)
            if (leaf[k].pos->priority == i) {   /* found correct priority */
                temp.pos = leaf[k].pos;       /* shift it to current index */
                temp.depth = leaf[k].depth;
                for (l=k; l>index; l--) {
                    leaf[l].pos = leaf[l-1].pos;
                    leaf[l].depth = leaf[l-1].depth;
                    }
                leaf[index].pos = temp.pos;
                leaf[index].depth = temp.depth;
                index++;
                }
  if (leaf[0].pos == NULL || leaf[0].pos->priority == 0)
    vsa_index = 0;
  else for (j = vsa_index -1 ; j>=0; j--)
    if (leaf[j].pos == NULL || leaf[j].pos->priority == 0) {
        leaf[j].pos = NULL; leaf[j].depth = 0;
        vsa_index = j;
        }
}
#endif
/* ExtractBidInfo(void)
    In "EVALUATE" mode, not bid mode.
    Look for the longest match, first in the DEFAULT section,
        then in the normal START section. Note that the match is
        done with the full string and then progressively shortened
        until a match can be made.
    Once a match has been made then extract_process is called to
        match the sequence correctly from the start.
*/

#define TEMPSIZE MAX_SEQ_LEN
void extract_process(int,int);
unsigned char ranged[4][2];
char sbuffer[TEMPSIZE+1],seq1[TEMPSIZE+1], seq[TEMPSIZE+1];
// added to swig      2006-3-13
//@+node:ral.20180109191625.17: ** void ExtractBidInfo
void ExtractBidInfo(void) {
    int i,dealer,state;
    unsigned int p;
    char des[9]="EVALUATE";
    char temp[TEMPSIZE];

#ifdef CSW
    TRIGGER(205,do_log(1,10,"ExtractBidInfo\n"));
#endif
    dealer = BiddingSystem->GetDealer();
#ifdef MYPLAY
    set_bidder(0,4); /* this resets the bid suits in prolog section */
#endif
    for (i=0; i<4; i++) { ranged[i][0]=0; ranged[i][1]=37; }
    set_hcp(0,4,0,0); // reset hcp ranges

    strcpy(temp,BiddingSystem->GetSequence());
#ifdef EXPLAIN
    TheExplainDialog->SetSequence((char *)temp);
#endif

  while (temp[0] != '\0') {

    for (state=1; state<3; state++) {
#if !defined(EXCLUDE_BIDINFO)
		int round, seat,depth=0;
#endif    
        // for (i=0; i< 4; i++) bid_info.bid[i]=0;
        // for (i=0; i< 4; i++) temp_info.bid[i]=0;
#if !defined(EXCLUDE_BIDINFO)
		reset_bid_info(&temp_info);
        get_current_round_and_seat(depth,&round,&seat);
        reset_bid_info(&bid_info[round][seat]);
#endif
        print_stack_top = -1; p=0;
        for (i=0; i<C_MAX; i++) clue_stack[i].node=NULL;
        grab_index = -1;
        bidder = dealer;  // initialize global dealer for do_constraints call
        if (state==1) strcpy(seq1,"DEFAULT ");
        else strcpy(seq1,"START ");
        strcat(seq1,temp);
        p=0;
        do_constraints2(seq1,des,sys->root,sys->clue,&p);
        extract_process(i,dealer);
        }
    i = (int) strlen(temp);
    while (--i >= 0) {
        if (temp[i] == ':') { temp[i]='\0'; break; }
        else temp[i]='\0';
        }
    }
}

//@+node:ral.20180109191625.10: ** void push_forth
void push_forth(int side) {
    for (int i=0; i<8; i++) {
        hi_low[i] = fval[70+i];
        }
    }


//@+node:ral.20180109191625.11: ** void pop_forth
void pop_forth(int side) {
    for (int i=0; i<8; i++) {
        fval[70+i] = hi_low[i];
        }
    }



//@+node:ral.20180109191625.18: ** void extract_process
void extract_process(int section, int dealer) {
    int i,j,len,temp; // ,flag=0;
    char *spot;
//    char mtemp[9];

    if (print_stack_top < 0) return;
#ifdef CSW
    ONE_SHOT(205,do_log(1,10,"extract process entered line %u",__LINE__));
#endif

#ifndef NO_MACROS_VERSION
    // fprintf(stderr,"PST9:%d",print_stack_top);
    j = print_stack[print_stack_top].bidder;
    for (i=0; i<=print_stack_top; i++)
        do_range(print_stack[i].node,print_stack[i].bidder);
    sprintf(mtemp,"MYBOTTOM");
    domacro((char *)mtemp,j+3);
    if (macro_temp[0] != 'M') { low = atoi(macro_temp); flag = 1;}
    else low = ranged[j][0];
    sprintf(mtemp,"MYTOP");
    domacro((char *)mtemp,j+3);
    if (macro_temp[0] != 'M') { high = atoi(macro_temp); flag = 1;}
    else high = ranged[j][1];
    if (flag) {
        flag = 0;
        if (low > ranged[j][0]) { flag = 1; ranged[j][0] = low; }
        if (high < ranged[j][1]) { flag = 1; ranged[j][1] = high; }
        if (flag)
            set_hcp(j,0,low,high);
        }
#endif

/* Now do the pattern matching
 3/22/97 extract_process taken out of extractbidinfo
 Before this the sequence was evaluated for a longest match working
    backwards from the end of the sequence.
 Now the sequence is run in its entirety in order to process any small
    sections of bidding that may not have made it into the total sequence.
 For example, 1S:P:2C:2H:2S:P:P:P, will match to 1S:P:2C:P:2S:P:P:P, but
     if shortened to 1S:P:2C:2H, will match the 2H call correctly.  */

    if (section == 1) strcpy(seq,"DEFAULT ");
    else strcpy(seq,"START ");
    strcat(seq,BiddingSystem->GetSequence());
    len = 0; temp = 0; bidder = dealer;
    while (temp <= print_stack_top) {
        // fprintf(stderr,"PST11:%d",print_stack_top);
#if !defined(EXCLUDE_BIDINFO)
		int round,seat,depth=0;
#endif
		//for (i=0;i<4;i++) bid_info.bid[i]=0;
#if !defined(EXCLUDE_BIDINFO)
		get_current_round_and_seat(depth,&round,&seat);
		reset_bid_info(&bid_info[round][seat]);
#endif
		assert(print_stack[temp].node != NULL);
        assert(print_stack[temp].node->pattern != NULL);
        far_match_node(print_stack[temp].node->pattern,(char *)(seq+len));
        i = print_stack[temp].match_length;

        for (j=0; j<i; j++)
            if ((*(char *)(seq + len + j)) == ':')
                bidder = (bidder + 1) % 4;

#ifdef RESET_BIDINFO_OLD
		for (j=0; j< 4; j++)
            if (bid_info.bid[j]) {
                bid_info.bid[j]=0;
#ifdef MYPLAY
                set_bidder(bidder,j);
#endif
                }
#else
#if !defined(EXCLUDE_BIDINFO)
		{ int round, seat;
		get_current_round_and_seat(depth,&round,&seat);
		reset_bid_info(&bid_info[round][seat]);
		}
#endif
#endif

        len += i;
        spot = strchr((char *) print_stack[temp].node->exp.c_str(),'@');
        if (spot != NULL) {
            spot += 1;
#if !defined(linux)
            strncpy(sbuffer,(char *) spot,TEMPSIZE-1);
            sbuffer[TEMPSIZE-1]='\0';
            strncat(sbuffer,(char *)(seq + len),TEMPSIZE - strlen(sbuffer) - 1);
            strncpy(seq,sbuffer,TEMPSIZE-1);
#else
            strncpy(sbuffer,(char *) spot,TEMPSIZE+1);
            sbuffer[TEMPSIZE]='\0';
            strncat(sbuffer,(char *)(seq + len),TEMPSIZE - strlen(sbuffer)+1);
            strncpy(seq,sbuffer,TEMPSIZE+1);
#endif
            len =0;
            }
        temp++;
        }
}

#if defined(MYLEAD)

extern VSA vsa[VSA_MAX];
// void bid_search(int depth);
int set_hand(const char *,int);


char out_play[3];
char final_for_play[250];
EXTERN_HOFFER_C char *mylead(const char *sequence, const char *hand, unsigned int bid_suits, unsigned int opp_bid_suits) {
	// given hand send each card to get the best lead (highest priority)
	int suit = 3;
    unsigned int temp_id=0;
	unsigned char temp_priority=0,priority=0,count, index;
    char forth_command[40];
    char *terminal_line;
    char *comment=NULL;
	char ch, suits[4]={'C','D','H','S'},play[3] = {' ',' '};
	char *contract;
    char default_play[3] = {' ',' '};
    bool bad_sequence = false;
    bool card_is_ok = false;

    contract = get_contract();
    // ALOG("mylead entered with %s %s; contract is %s\n",sequence,hand,contract);
    if (contract != NULL && strlen(contract) > 1) ch = contract[1];
    else ch = 'N';
	char command[11] = {'L','E','A','D',' ','S',' ' ,' ',' ',' '};
    command[5] = ch;
	char *description=NULL;
    // ALOG("the input is %s<-",command);
    
    // here the forth variables are set to 0 or 1 depending on whether the suit was bid for the side.
    // then the grabs #c to #j (2-10) are set to space (unbid) or the suit if bid in the bidding rules
    // 10 grabs are immediately set up, and then the values are set.
    // 211023 changed from setting as a space to P as that can be checked.
    
    for (unsigned int i=0; i<4; i++) {
        index = 30+i;
        if (bid_suits & 1 << i) { sprintf(forth_command,"%d %d G", suits[i], index); } // forth variables 30 (clubs) to 33 (spades)
        else { sprintf(forth_command,"80 %d G", index); }
        f_eval(forth_command);
        index = 34+i;
        if (opp_bid_suits & 1 << i) { sprintf(forth_command,"%d %d G", suits[i],index); } // forth variables 34 (clubs) to 37 (spades)
        else { sprintf(forth_command,"80 %d G", index); }
        f_eval(forth_command);
        }
        

	set_hand(hand,0);
    // printf("the hand is %s\n",hand);

	description = get_description2(0,0);  // gets the description from the deck; 2nd parameter is vul; first is dir

	print_stack_top = -1;
    for (int i=0; i<C_MAX; i++) clue_stack[i].node=NULL;
    grab_index = -1;
    bidder = 0;  // initialize global dealer for do_constraints call
	count = '0';
    
    
    try { my_evaluate_sequence(sequence); }
    catch( const std::invalid_argument& e ) {
        // do stuff with exception...
        bad_sequence = true;
        }
	temp_priority = 0;
    out_play[0]=' '; out_play[1]=' ';
	while ((ch = *hand)) {
        card_is_ok = false;
		if (ch == '.') {suit--; hand++; count='0'; continue; }
		command[7]= toupper(*hand);
        if (suit < 0 || suit > 3) { hand++; continue; } // bad suit
		hand++; command[8]=suits[suit];
		count += 1;
		command[9]=count;
		play[0]=command[7]; play[1]= command[8];
        
        switch (play[0]) {
            case 'A': case 'K': case 'Q': case 'J': case 'T': card_is_ok = true; break;
            case '2': case '3': case '4': case '5': case '6': case '7': case '8':
            case '9': card_is_ok = true; break;
        }
        if (!card_is_ok) { hand++; continue; }
        
        default_play[0]=play[0]; default_play[1]=play[1]; default_play[2]='\0';
        
        

        if (bad_sequence) {  // use the first card and do no processing, send back exception message
            out_play[0]=default_play[0]; out_play[1]=default_play[1];
            break;
            }
        // ALOG("LUDDY the search string is <%s>\n",command);

        vsa[0].str = (char  *)command;
        vsa[0].pos = sys->root;
        vsa[0].current_bidder = 0;
		vsa[0].current_round = 0;
        priority = 0;
        vsa[0].priority = &priority;
#ifdef VERSION2
        // vsa[0].default_flag = 0;
#endif
        strcpy(des,description);

		bid_search(0); // send each card to the rules section; match and get the priority
                
		if (priority == 9) {
            temp_priority = 9;
            comment = get_best_comment(bidder);
            temp_id = print_stack[print_stack_top].node->id;
            terminal_line = get_line_info(1);
            out_play[0]=play[0]; out_play[1]=play[1];
            // printf("the comment is %s for priority of %d for lead:%s id:%d\n",comment,priority,play,temp_id);
            break; }
		else if (priority > temp_priority) {
            comment = get_best_comment(bidder);
            temp_id = print_stack[print_stack_top].node->id;
			temp_priority = priority;
			out_play[0]=play[0]; out_play[1]=play[1];
            // printf("the comment is %s for priority of %d for lead:%s id:%d\n",comment,priority,play,temp_id);
			}
        else {
            // my_evaluate_sequence(sequence);
            // comment = get_best_comment(bidder);
            // printf("the unused comment is %s for priority of %d for lead:%s\n",comment,priority,play);
            }
        }

    // ALOG("LUDDY the final priority is %d and the final id is %d\n",temp_priority,temp_id);
    out_play[2]='\0';
    // ALOG("LUDDY the lead is %s",out_play);
    // if (comment == NULL) printf("...No comment\n");
    // else printf(" with comment of %s\n",comment);
    if (out_play[0] == ' ' || out_play[1] == ' ') {
        out_play[0] = default_play[0]; out_play[1]=default_play[1]; }
    strcpy(final_for_play,out_play);
    strcat(final_for_play,":");
    if (bad_sequence) strcpy(final_for_play,"bad sequence");
    else if (comment == NULL) {} // strcat(final_for_play,"no comment");
    else if (final_for_play[0]==' ' || final_for_play[1]==' ') strcpy(final_for_play,"bad card");
    else {
        if (strlen(comment) < 240) strcat(final_for_play,comment);
        else strcat(final_for_play,"bad comment; too long (> 240 chars)");
        }
    // ALOG("LUDDY mylead returning %s\n",final_for_play);
    return final_for_play;
}
#endif
