

#define DECLARE_DESCRIPTION
#include "bid20.h"
#include "stdio.h"
#include "string.h"
#include "math.h"
#include "defines.hpp"
// release #include "assert.h"
#if defined(AMZI)
#include "amzi.h"
#endif

#include "bergen.h"

#if defined(linux)
#define EXTERN_HOFFER_C
#define NO_CSW
#include "cswitch.h"
#include "assert.h"
#endif

#ifdef TWEAKING
#undef AMZI
#endif

#ifdef AMZI
extern int amzi_flag;
#endif

#define OFF 0
#define ON 1


extern "C" int my_total_points;
// static int dist[4];

int roth(int deck[52],int start,int number,int state, int suit);
int bergen(int deck[52],int start,int number,int state, int suit);
int klinger(int deck[52],int start, int number, int state, int suit);
int kaplan_rueben(int deck[52],int start, int number, int state, int suit);
int bbo(int deck[52],int start, int number, int state, int suit);
int hoffer_count(int deck[52],int start, int number, int state, int suit);


// extern "C" {

// void set_evaluation_system(int i);
char *get_description(int i);
// char *get_description2(int i, int v);
char *get_des(int i);
int deck[52];
//void set_deck(int,int);
int get_deck(int);
// }

extern "C" int gen32_set_deck(int,int);
extern "C" int gen32_get_deck(int);

EXTERN_HOFFER_C void set_deck(int position, int value) {
    gen32_set_deck(position,value);
}

int get_deck(int card) {
    return gen32_get_deck(card);
}

int getHCPs(int  *deck, int first,int number)
{
        int i,j,points;
        i=j=points=0;

        for (j=first; j< first + number; j++) {
                i = deck[j] % 13;
				if (i > 8) points=points +i -8;
                }
        return(points);
}

int getQuicks(int deck[52], int first, int number, int quicks[4])
{
        int i,j,denom, suit,total,temp[4][3],len[4];

		for (i=0; i<4; i++) {
			len[i]=0;
			for (j=0; j<3; j++)
				temp[i][j]=0;
			}
        total = 0;
        for (j=first; j< first + number; j++) {
				denom = deck[j] % 13;
                suit = deck[j] / 13;
				len[suit] += 1;
				switch (denom) {
					case 12: temp[suit][2]= 1; break;
					case 11: temp[suit][1]= 1; break;
					case 10: temp[suit][0]= 1; break;
					default: break;
                    }
				}
		for (i=0; i<4; i++) {
			if (temp[i][2] == 1 && temp[i][1] == 1) quicks[i] = 4; // AK
			else if (temp[i][2] == 1 && temp[i][0] == 1) quicks[i] = 3; // AQ
			else if (temp[i][2] == 1) quicks[i] = 2; // A
			else if (temp[i][1] == 1 && temp[i][0] == 1) quicks[i] = 2; // KQ
			else if (temp[i][1] == 1 && len[i]>1) quicks[i] = 1; // K but not singleton 191115
			else quicks[i] = 0;
			total += quicks[i];
            }


        return(total);
}


int getLTC(int deck[52],int  first, int number)
{
	int i,suit;
	int num[4],losers[4];
	int tops[4],t;
	int total_losers=0;

	for (i=0;i<4;i++) { tops[i]=0; num[i]=0; losers[i]=0; }

	for (i=first; i< first + number; i++) {
		suit=deck[i] / 13;
		t=(deck[i] % 13);
		if (t>9) switch (t) {
			case 10: tops[suit] += 1; break;
			case 11: tops[suit] += 2; break;
			case 12: tops[suit] += 4; break;
			}
		if (++num[suit] < 4)
			++losers[suit];
		}

	for (i=0; i<4; i++) {
		switch(losers[i]) {
			case 1: if (tops[i]>3) losers[i] = 0; break;
			case 2: if (tops[i]>5) losers[i] = 0;
					else if (tops[i]>1) losers[i]=1; break;
			case 3: if (tops[i]==7) losers[i] =0;
					else if (tops[i]==1 || tops[i]==2 ||
						tops[i]==4) losers[i] = 2;
					else if (tops[i] == 0) ;
					else losers[i] = 1;
					break;
			}
		}

	for (i=0; i<4; i++)
		total_losers += losers[i];

	return(total_losers);
}

void distro(int *deck,int b,int c,int *d)
{
        int i;
        for (i=0; i<4; i++) d[i]=0;

        for ( i=b; i< b+c; i++)
                d[(deck[i]/13)]++;
}


int num;
int temp_dist[4];
int max;
char suits[] = "CDHS";

int gen32_evaluation_system=HOFFER_COUNT;
#ifdef TWEAKING
#undef AMZI
#endif
EXTERN_HOFFER_C void set_evaluation_system(int i) {
// #define BERGEN 1
// #define ROTH 2
// #define KLINGER 3
// #define BERGEN_HCPS 4
// #define KLINGER_HCPS 5
// #define KAPLAN_RUEBEN 6
// #define HOFFER 7
    // fprintf(stderr,"evaluation system set to %u (BERGEN is 1)",i);
// #define BBO 8


    gen32_evaluation_system = i;
    }
#ifndef AMZI_THERE
#ifdef AMZI_ADDITIONAL
#if !defined(IOS)
EXTERN_HOFFER_C int assert_amzi(const char *);
EXTERN_HOFFER_C char *call_amzi(const char *);
#endif
extern "C" int retractall_amzi(void);
extern "C" int get_dealer();
#endif
#endif
extern "C" int retract_points();
extern "C" int gen32_get_suit_strength(int deck[52], int dist[4], int i, int num);

#ifdef OLD_DESCRIPTION2

#pragma message("OLD_DESCRIPTION2 remains defined in describe.cpp")

EXTERN_HOFFER_C char *get_description2(int i, int v) {
	char st_temp[D_LEN];
#ifdef AMZI
	char amzi_temp[255];
#endif
	int j,k=0;
	int quicks[4];
    // char jack_flag, ten_flag, top_five, top_three, count[2];
	// char level;
	int temp,bidder;
	hoffer_setup();

#ifdef AMZI
	// this is dependent on north gettting called first for get_descriptions
	if (i==0 && amzi_flag) retractall_amzi();
	if (i==0 && amzi_flag) { int result;
		result = retract_points();
		if (result == 0) fprintf(stderr,"could not retract points");
		}
#endif

	if (gen32_evaluation_system == BERGEN)
	// Bergen can vary hcps by a total of 3, 2 for acetens vs quacks, and one for dubious honors
	   my_total_points = bergen(deck,i*13,13,BERGEN_HCPS,0);
    else if (gen32_evaluation_system ==ROTH)
   	    my_total_points = getHCPs(deck,i*13,13);
    else if (gen32_evaluation_system == KLINGER)
        my_total_points = klinger(deck,i*13,13,KLINGER_HCPS,0);
	else if (gen32_evaluation_system == KAPLAN_RUEBEN)
		my_total_points = kaplan_rueben(deck,i*13,13,KLINGER_HCPS,0);
    else if (gen32_evaluation_system == HOFFER_COUNT)
        my_total_points = getHCPs(deck,i*13,13);
    else if (gen32_evaluation_system == BBO)
		my_total_points = bbo(deck,i*13,13,BBO,0);
    
	bidder = i; // for amzi; it is called at the start so use i to get to next bidder
	sprintf(st_temp,"P%02u:D",my_total_points);
#ifdef AMZI
	if (amzi_flag) {
		sprintf(amzi_temp,"temp_hcps(%u,%u)",bidder,my_total_points);
		assert_amzi(amzi_temp);
		}
#endif
	strcpy(description,st_temp);
	distro(deck,i*13,13,dist);
  	num=0;
  	for (j=0; j<4; j++) temp_dist[j]=0;
  	while (num <4) {
  		max = -1;
  		for (j=3; j>=0; j--)
  			if (temp_dist[j] == 0 && dist[j] > max) {
  				max = dist[j];
  				k = j;
  				}
		temp_dist[k]=1;
        max = max < 10 ? max : 9;
  		sprintf(st_temp,"%u%c",max, suits[k]);
		strcat(description,st_temp);
#ifdef AMZI
		if (amzi_flag) {
			sprintf(amzi_temp,"temp_len(%u,%u,%u)",bidder,k,max);
			assert_amzi(amzi_temp);
		}
#endif
		num++;
  		}
  	sprintf(st_temp,":C");
  		strcat(description,st_temp);
	for (j=0; j<4; j++) {
    	sprintf(st_temp,"%u",dist[j] < 10 ? dist[j] : 9);
		strcat(description,st_temp);
	}
	temp = getLTC(deck,i*13,13);
  	sprintf(st_temp, ":L%02u",temp);
		strcat(description,st_temp);
#ifdef AMZI
		if (amzi_flag) {
			sprintf(amzi_temp,"temp_losers(%u,%u)",bidder,temp);
			assert_amzi(amzi_temp);
		}
#endif  	
		sprintf(st_temp,":H");
		strcat(description,st_temp);
  	num=0;
  	while (num <4) {
  		max=0;
  		for (j=i*13; j < i*13+13; j++)
  			if ((k=(deck[j] % 13)) > 8 && (deck[j]/13) == num) {
  				max += (1 << (k-9));
  				}
  		sprintf(st_temp,"%02u%c",max, suits[num]);
		strcat(description,st_temp);
#ifdef AMZI
		if (amzi_flag) {
			sprintf(amzi_temp,"honors(%u,suit_%c,%u)",bidder,suits[num],max);
			assert_amzi(amzi_temp);
		}
#endif
		num++;
  		}
    if (gen32_evaluation_system == BERGEN) {
		temp = bergen(deck,i*13,13,STARTING,0); 
	sprintf(st_temp,":R%02u",temp);
	strcat(description,st_temp);
#ifdef AMZI
	if (amzi_flag) {
		sprintf(amzi_temp,"temp_starting(%u,%u)",bidder,temp);
		assert_amzi(amzi_temp);
		}
#endif
	temp = bergen(deck,i*13,13,SUPPORTED,0);
	sprintf(st_temp,"%02u",temp);  /* supported */
#ifdef AMZI
	if (amzi_flag) {
		sprintf(amzi_temp,"temp_supported(%u,%u)",bidder,temp);
		assert_amzi(amzi_temp);
		}
#endif
	strcat(description,st_temp);
  	num=0;
  	while (num < 4) {
		temp = bergen(deck,i*13,13,SUPPORT_OF,num);
		sprintf(st_temp, "%02u%c",temp,suits[num]);
		strcat(description,st_temp);
#ifdef AMZI
		if (amzi_flag) {
		sprintf(amzi_temp,"temp_support(%u,suit_%c,%u)",bidder,suits[num],temp);
		assert_amzi(amzi_temp);
		}
#endif
		num++;
		}
    }
                                /* roth allowed from 180413 on */
    else if (gen32_evaluation_system == ROTH) {
	temp = roth(deck,i*13,13,1,0);
	sprintf(st_temp,":R%02u",temp);
	strcat(description,st_temp);
#ifdef AMZI
	if (amzi_flag) {
		sprintf(amzi_temp,"temp_starting(%u,%u)",bidder,temp);
		assert_amzi(amzi_temp);
	}
#endif
	temp = roth(deck,i*13,13,SUPPORTED,0);
    sprintf(st_temp,"%02u",temp);
    strcat(description,st_temp);
#ifdef AMZI
	if (amzi_flag) {
		sprintf(amzi_temp,"temp_supported(%u,%u)",bidder,temp);
		assert_amzi(amzi_temp);
		}
#endif
	num=0;
  	while (num < 4) {
		temp = roth(deck,i*13,13,2,num);
		sprintf(st_temp, "%02u%c",temp,suits[num]);
		strcat(description,st_temp);
#ifdef AMZI
		if (amzi_flag) {
			sprintf(amzi_temp,"temp_support(%u,suit_%c,%u)",bidder,suits[num],temp);
			assert_amzi(amzi_temp);
		}
#endif
		num++;
		}
    }
    else if (gen32_evaluation_system == KLINGER) {
	temp = klinger(deck,i*13,13,STARTING,0);
	sprintf(st_temp,":R%02u",temp);
	strcat(description,st_temp);
#ifdef AMZI
	if (amzi_flag) {
		sprintf(amzi_temp,"temp_starting(%u,%u)",bidder,temp);
		assert_amzi(amzi_temp);
		}
#endif
	temp = klinger(deck,i*13,13,SUPPORTED,0);
	sprintf(st_temp,"%02u",temp);  /* supported */
	strcat(description,st_temp);
#ifdef AMZI
	if (amzi_flag) {
		sprintf(amzi_temp,"temp_supported(%u,%u)",bidder,temp);
		assert_amzi(amzi_temp);
		}
#endif
	num=0;
  	while (num < 4) {
		temp = klinger(deck,i*13,13,SUPPORT_OF,num);
		sprintf(st_temp, "%02u%c",temp,suits[num]);
		strcat(description,st_temp);
#ifdef AMZI
		if (amzi_flag) {
			sprintf(amzi_temp,"temp_support(%u,suit_%c,%u)",bidder,suits[num],temp);
			assert_amzi(amzi_temp);
		}
#endif
		num++;
		}
    }

    else if (gen32_evaluation_system == KAPLAN_RUEBEN) {
		temp = kaplan_rueben(deck,i*13,13,STARTING,0);
	sprintf(st_temp,":R%02u",temp);
	strcat(description,st_temp);
#ifdef AMZI
	if (amzi_flag) {
		sprintf(amzi_temp,"temp_starting(%u,%u)",bidder,temp);
		assert_amzi(amzi_temp);
	}
#endif
	temp = kaplan_rueben(deck,i*13,13,SUPPORTED,0);
	sprintf(st_temp,"%02u",temp);  /* supported */
	strcat(description,st_temp);
#ifdef AMZI
	if (amzi_flag) {
		sprintf(amzi_temp,"temp_supported(%u,%u)",bidder,temp);
		assert_amzi(amzi_temp);
		}
#endif
	num=0;
  	while (num < 4) {
		temp = kaplan_rueben(deck,i*13,13,SUPPORT_OF,num);
		sprintf(st_temp, "%02u%c",temp,suits[num]);
		strcat(description,st_temp);
#ifdef AMZI
		if (amzi_flag) {
		sprintf(amzi_temp,"temp_support(%u,suit_%c,%u)",bidder,suits[num],temp);
		assert_amzi(amzi_temp);
		}
#endif
		num++;
		}
    }
    else if (gen32_evaluation_system == HOFFER_COUNT) {
		temp = hoffer_count(deck,i*13,13,STARTING,0);
		sprintf(st_temp,":R%02u",temp);
        strcat(description,st_temp);
#ifdef AMZI
	if (amzi_flag) {
		sprintf(amzi_temp,"temp_starting(%u,%u)",bidder,temp);
		assert_amzi(amzi_temp);
	}
#endif
		temp = hoffer_count(deck,i*13,13,SUPPORTED,0); 
        sprintf(st_temp,"%02u",temp);  /* supported */
        strcat(description,st_temp);
#ifdef AMZI
	if (amzi_flag) {
		sprintf(amzi_temp,"temp_supported(%u,%u)",bidder,temp);
		assert_amzi(amzi_temp);
		}
#endif
        num=0;
        while (num < 4) {
			temp = hoffer_count(deck,i*13,13,SUPPORT_OF,num);
            sprintf(st_temp, "%02u%c",temp,suits[num]);
            strcat(description,st_temp);
            num++;
        }
    }

	// added 171015
	sprintf(st_temp, ":V%01u",v);
	strcat(description,st_temp);
#ifdef AMZI
	if (amzi_flag) {
		sprintf(amzi_temp,"vul(%u,%u)",bidder,v);
		assert_amzi(amzi_temp);
		}
#endif

	strcat(description,":Q");
#define NEW_QSECTION
#ifdef NEW_QSECTION
	max = getQuicks(deck, i*13, 13, quicks); // 160510 corrected, was too low for KQ (1 vs 2) 
#else
	max=0;
	for (num=0; num<4; num++) {
		quicks[num]=0;
		for (j=i*13; j<i*13+13; j++)
			if ((k=(deck[j] % 13)) > 10 && (deck[j]/13) == num) {
				quicks[num] += k-10;
				max += k-10;
				}
		}
#endif
	sprintf(st_temp,"%02u",max);
		strcat(description,st_temp);
#ifdef AMZI
		if (amzi_flag) {
		sprintf(amzi_temp,"temp_quicks(%u,%u)",bidder,max);
		assert_amzi(amzi_temp);
		}
#endif
		num=0;

	for (num=0; num<4; num++) {
       	sprintf(st_temp,"%02u%c",quicks[num],suits[num]);
		strcat(description,st_temp);
#ifdef AMZI
		if (amzi_flag) {
		sprintf(amzi_temp,"temp_quicks(%u,suit_%c,%u)",bidder,suits[num],quicks[num]);
		assert_amzi(amzi_temp);
		}
#endif
	}

/* added to show stoppers just before the release of 1.3 
	8/5/97 note: 1.3 was not released until 11/97 */

	strcat(description,":S");
	for (num=0; num<4; num++) {
  		max=0;
  		for (j=i*13; j < i*13+13; j++)
  			if ((k=(deck[j] % 13)) > 8 && (deck[j]/13) == num) {
  				max += (k-8);
  				}
		max += dist[num];
		if (max > 7) { strcat(description,"2");
#ifdef AMZI
		if (amzi_flag) {
		sprintf(amzi_temp,"temp_stopper(%u,suit_%c,2)",bidder,suits[num]); }
#endif
		}
		else if (max > 4) { strcat(description,"1");
#ifdef AMZI
		if (amzi_flag) {
		sprintf(amzi_temp,"temp_stopper(%u,suit_%c,1)",bidder,suits[num]); }
#endif
		}
		else { strcat(description,"0");
#ifdef AMZI
		if (amzi_flag) {
		sprintf(amzi_temp,"temp_stopper(%u,suit_%c,0)",bidder,suits[num]); }
#endif
		}
		sprintf(st_temp,"%c",suits[num]);
        strcat(description,st_temp);
#ifdef AMZI
		assert_amzi(amzi_temp);
#endif
	}
/* added to show suit strength 3/14/98 */
	strcat(description,":G");

#define NEW_STRENGTH
#ifdef NEW_STRENGTH
    for (num=0;num<4;num++) {
        sprintf(st_temp,"%u",gen32_get_suit_strength(deck,dist,i,num));
        strcat(description,st_temp);
        sprintf(st_temp,"%c",suits[num]);
        strcat(description,st_temp);
        }
#else
	for (num=0; num<4; num++) {
		if (dist[num] < 4) {
			sprintf(st_temp,"0%c",suits[num]);
			strcat(description,st_temp);
#ifdef AMZI
		if (amzi_flag) {
			sprintf(amzi_temp,"temp_strength(%u,suit_%c,0)",bidder,suits[num]);
			assert_amzi(amzi_temp);
		}
#endif
		}
		else {
			count[1]=0; count[0]=0;
			jack_flag=OFF; ten_flag=OFF; max=0;
			for (j=i*13; j < i*13+13; j++)
				if ((k=(deck[j] % 13)) > 6 && (deck[j]/13) == num)
					switch (k) {
						case 8: ten_flag = ON; break;
						case 9: jack_flag = ON; ++count[0]; break;
						case 10: case 11: case 12:
							++count[0]; ++count[1]; break;
						}

            top_three = count[1];
			if (ten_flag == ON && count[0] > 1) max += 1;
			if (jack_flag == ON && count[1] > 0) max += 1;
			top_five = max + count[1];

            level = -1;
			for (j=7; j>=0 && level == -1; j--)
				switch (j) {
					case 7: if (top_three == 3 && dist[num] > 8) level = 7;
							else if (top_three == 3 && top_five > 3 && dist[num] > 7) level = 7;
							else if (top_five == 5 && dist[num] > 6) level = 7;
							break;
					case 6: if (top_three == 3 && dist[num] > 6) level = 6;
							else if (top_five == 4 && dist[num] > 6) level = 6;
							break;
					case 5: if (top_three >1 && dist[num] > 7) level = 5;
							else if (top_five == 3 && dist[num] > 7) level = 5;
							else if (dist[num]==7 && top_five > 3) level = 5;
							break;
					case 4: if (dist[num] > 8) level = 4;
                    		else if (dist[num] > 7 && top_three > 0) level = 4;
							else if (dist[num] > 6 && top_three == 2) level = 4;
							else if (dist[num] > 6 && top_five > 2) level = 4;
							break;
					case 3: if (dist[num] > 7) level = 3;
							else if (dist[num] == 7 && top_five > 1) level = 3;
							else if (dist[num] == 6 && top_five > 3) level = 3;
							else if (dist[num] == 5 && top_three ==3) level = 3;
							break;
					case 2: if (dist[num] > 6) level = 2;
							else if (dist[num] == 6 && top_five > 1) level = 2;
							else if (dist[num] == 5 && top_three > 1) level = 2;
							else if (dist[num]==5 && top_five > 3) level = 2;
							break;
					case 1: if (dist[num] > 5) level = 1;
							else if (dist[num] == 5 && top_three > 0) level = 1;
							else if (dist[num] == 5 && top_five > 1) level = 1;
							else if (dist[num] == 4 && top_three > 1) level = 1;
							else if (dist[num] == 4 && top_three > 0 && top_five > 2) level = 1;
							break;
                    case 0: level = 0; break;
                    }

			sprintf(st_temp,"%u%c",level,suits[num]);
			strcat(description,st_temp);

            
#ifdef AMZI
			if (amzi_flag) {
			sprintf(amzi_temp,"temp_strength(%u,suit%c,%u)",bidder,suits[num],level);
			assert_amzi(amzi_temp);
			}
#endif
		}
        
    }
#endif // new strength

#define EXTRA_M
#ifdef EXTRA_M
                        { int n,m,temp_middle[4];  // added to this description 200129
                        // fprintf(stderr,"len:%u",strlen(description_buffer));
                        strcat(description,":M");
                        for (n=0;n<4;n++) temp_middle[n]=0;
                        for (n=i*13;n<i*13+13;n++) {
                            m = deck[n] % 13;
                            if (m < 11 && m > 6) temp_middle[deck[n]/13]++;
                            }
                        for (n=0;n<4;n++) {
                            sprintf(st_temp,"%u%c",temp_middle[n],suits[n]);
                            strcat(description,st_temp);
                            }
                            
                        }
#endif


return description;
}
#endif

#ifdef WHAT_THE_HELL

char *get_description(int i) {
	char st_temp[D_LEN];
	int j,k=0;
	int quicks[4];
    char jack_flag, ten_flag, top_five, top_three, count[2];
	char level;
	// printf("\nevaluation system: %u should be 1 bergen",gen32_evaluation_system);

//#define BERGEN 1
//#define ROTH 2
//#define KLINGER 3
//#define BERGEN_HCPS 4
//#define KLINGER_HCPS 5
//#define KAPLAN_RUEBEN 6
//#define HOFFER_COUNT 7

    if (gen32_evaluation_system == BERGEN)
	//  Bergen can vary hcps by a total of 3, 2 for acetens vs quacks, and one for dubious honors
	   my_total_points = bergen(deck,i*13,13,BERGEN_HCPS,0);
    else if (gen32_evaluation_system ==ROTH)
   	    my_total_points = getHCPs(deck,i*13,13);
    else if (gen32_evaluation_system == KLINGER)
        my_total_points = klinger(deck,i*13,13,KLINGER_HCPS,0);
    else if (gen32_evaluation_system == HOFFER_COUNT)
        my_total_points = getHCPs(deck,i*13,13);
	else assert(1==0);
        
	sprintf(st_temp,"P%02u:D",my_total_points);
	strcpy(description,st_temp);
	distro(deck,i*13,13,dist);
  	num=0;
  	for (j=0; j<4; j++) temp_dist[j]=0;
  	while (num <4) {
  		max = -1;
  		for (j=3; j>=0; j--)
  			if (temp_dist[j] == 0 && dist[j] > max) {
  				max = dist[j];
  				k = j;
  				}
		temp_dist[k]=1;
        max = max < 10 ? max : 9;
  		sprintf(st_temp,"%u%c",max, suits[k]);
		strcat(description,st_temp);
		num++;
  		}
  	sprintf(st_temp,":C");
  		strcat(description,st_temp);
	for (j=0; j<4; j++) {
    	sprintf(st_temp,"%u",dist[j] < 10 ? dist[j] : 9);
		strcat(description,st_temp);
	}
  	sprintf(st_temp, ":L%02u",getLTC(deck,i*13,13));
		strcat(description,st_temp);
  	sprintf(st_temp,":H");
		strcat(description,st_temp);
  	num=0;
  	while (num <4) {
  		max=0;
  		for (j=i*13; j < i*13+13; j++)
  			if ((k=(deck[j] % 13)) > 8 && (deck[j]/13) == num) {
  				max += (1 << (k-9));
  				}
  		sprintf(st_temp,"%02u%c",max, suits[num]);
		strcat(description,st_temp);
  		num++;
  		}
    if (gen32_evaluation_system == BERGEN) {
	sprintf(st_temp,":R%02u",bergen(deck,i*13,13,STARTING,0));
	strcat(description,st_temp);
	sprintf(st_temp,"%02u",bergen(deck,i*13,13,SUPPORTED,0));
	strcat(description,st_temp);
  	num=0;
  	while (num < 4) {
		sprintf(st_temp, "%02u%c",bergen(deck,i*13,13,SUPPORT_OF,num),suits[num]);
		strcat(description,st_temp);
  		num++;
		}
	// printf("\nBERGEN:%s",description);
    }

    else if (gen32_evaluation_system == ROTH) {
	sprintf(st_temp,":R%02u",roth(deck,i*13,13,STARTING,0));
	strcat(description,st_temp);
    sprintf(st_temp,"%02u",roth(deck,i*13,13,SUPPORTED,0));
  	num=0;
  	while (num < 4) {
		sprintf(st_temp, "%02u%c",roth(deck,i*13,13,2,num),suits[num]);
		strcat(description,st_temp);
  		num++;
		}
    }
    else if (gen32_evaluation_system == KLINGER) {
	sprintf(st_temp,":R%02u",klinger(deck,i*13,13,STARTING,0));
	strcat(description,st_temp);
	sprintf(st_temp,"%02u",klinger(deck,i*13,13,SUPPORTED,0));  /* supported */
	strcat(description,st_temp);
  	num=0;
  	while (num < 4) {
		sprintf(st_temp, "%02u%c",klinger(deck,i*13,13,SUPPORT_OF,num),suits[num]);
		strcat(description,st_temp);
  		num++;
		}
    }
    else if (gen32_evaluation_system == HOFFER_COUNT) {
        sprintf(st_temp,":R%02u",hoffer_count(deck,i*13,13,STARTING,0));
        strcat(description,st_temp);
        sprintf(st_temp,"%02u",hoffer_count(deck,i*13,13,SUPPORTED,0));  /* supported */
        strcat(description,st_temp);
        num=0;
        while (num < 4) {
            sprintf(st_temp, "%02u%c",hoffer_count(deck,i*13,13,SUPPORT_OF,num),suits[num]);
            strcat(description,st_temp);
            num++;
        }
    }

	strcat(description,":Q");
#define NEW_QSECTION
#ifdef NEW_QSECTION
	max = getQuicks(deck, i*13, 13, quicks); // 160510 corrected, was too low for KQ (1 vs 2) 
#else
	max=0;
	for (num=0; num<4; num++) {
		quicks[num]=0;
		for (j=i*13; j<i*13+13; j++)
			if ((k=(deck[j] % 13)) > 10 && (deck[j]/13) == num) {
				quicks[num] += k-10;
				max += k-10;
				}
		}
#endif
	sprintf(st_temp,"%02u",max);
		strcat(description,st_temp);
	num=0;

	for (num=0; num<4; num++) {
       	sprintf(st_temp,"%02u%c",quicks[num],suits[num]);
		strcat(description,st_temp);
        }

/* added to show stoppers just before the release of 1.3 
	8/5/97 note: 1.3 was not released until 11/97 */

	strcat(description,":S");
	for (num=0; num<4; num++) {
  		max=0;
  		for (j=i*13; j < i*13+13; j++)
  			if ((k=(deck[j] % 13)) > 8 && (deck[j]/13) == num) {
  				max += (k-8);
  				}
		max += dist[num];
		if (max > 7) strcat(description,"2");
		else if (max > 4) strcat(description,"1");
		else strcat(description,"0");
		sprintf(st_temp,"%c",suits[num]);
        strcat(description,st_temp);
		}
/* added to show suit strength 3/14/98 */
	strcat(description,":G");
	for (num=0; num<4; num++) {
		if (dist[num] < 4) {
			sprintf(st_temp,"0%c",suits[num]);
			strcat(description,st_temp);
			}
		else {
			count[1]=0; count[0]=0;
			jack_flag=OFF; ten_flag=OFF; max=0;
			for (j=i*13; j < i*13+13; j++)
				if ((k=(deck[j] % 13)) > 6 && (deck[j]/13) == num)
					switch (k) {
						case 8: ten_flag = ON; break;
						case 9: jack_flag = ON; ++count[0]; break;
						case 10: case 11: case 12:
							++count[0]; ++count[1]; break;
						}

            top_three = count[1];
			if (ten_flag == ON && count[0] > 1) max += 1;
			if (jack_flag == ON && count[1] > 0) max += 1;
			top_five = max + count[1];

            level = -1;
			for (j=7; j>=0 && level == -1; j--)
				switch (j) {
					case 7: if (top_three == 3 && dist[num] > 8) level = 7;
							else if (top_three == 3 && top_five > 3 && dist[num] > 7) level = 7;
							else if (top_five == 5 && dist[num] > 6) level = 7;
							break;
					case 6: if (top_three == 3 && dist[num] > 6) level = 6;
							else if (top_five == 4 && dist[num] > 6) level = 6;
							break;
					case 5: if (top_three >1 && dist[num] > 7) level = 5;
							else if (top_five == 3 && dist[num] > 7) level = 5;
							else if (dist[num]==7 && top_five > 3) level = 5;
							break;
                    case 4: if (dist[num] > 8) level = 4;
                    		else if (dist[num] > 7 && top_three > 1) level = 4;
							else if (dist[num] > 6 && top_three == 3) level = 4;
							else if (dist[num] > 6 && top_five > 3) level = 4;
							break;
					case 3: if (dist[num] > 7) level = 3;
							else if (dist[num] == 7 && top_five > 1) level = 3;
							else if (dist[num] == 6 && top_five > 3) level = 3;
							else if (dist[num] == 5 && top_three ==3) level = 3;
							break;
					case 2: if (dist[num] > 6) level = 2;
							else if (dist[num] == 6 && top_five > 1) level = 2;
							else if (dist[num] == 5 && top_three > 1) level = 2;
							else if (dist[num]==5 && top_five > 3) level = 2;
							break;
					case 1: if (dist[num] > 5) level = 1;
							else if (dist[num] == 5 && top_three > 0) level = 1;
							else if (dist[num] == 5 && top_five > 1) level = 1;
							else if (dist[num] == 4 && top_three > 1) level = 1;
							else if (dist[num] == 4 && top_three > 0 && top_five > 2) level = 1;
							break;
                    case 0: level = 0; break;
                    }

			sprintf(st_temp,"%u%c",level,suits[num]);
			strcat(description,st_temp);
#ifdef AMZI
			if (amzi_flag) {
			sprintf(amzi_temp,"temp_strength(%u,suit%c,%u)",bidder,suits[num],level);
			assert_amzi(amzi_temp);
			}
#endif
		}
		}
	return description;
}
#endif

char *make_description(const char *in) {
	int suit=3,rank,position=0;
	char c;
	while ((c = *in++)) {
		if (c=='.') suit--;
		else {
			if (c=='A' || c=='a') rank = 12;
			else if (c=='K' || c=='k') rank = 11;
			else if (c=='Q' || c=='q') rank = 10;
			else if (c=='J' || c=='j') rank = 9;
			else if (c=='T' || c=='t') rank = 8;
			else rank = c-'2';
			assert(rank >= 0 && rank <= 12);
			assert(suit >= 0 && suit <= 3);
			set_deck(position++,suit*13+rank);
			}
		}
	return get_description2(0,0);
}



/* opp_bid is 0 neither bid; 1 = RHO bid; 2 = LHO bid; 3 = both bid for each suit */
int roth(int deck[52],int start,int number,int state, int suit)
{
	int i, j, count, dist[4], hcp[4], aces[4], ace_flag;
	int doubleton_flag, kings[4];
    int honors[4];
	ace_flag = count = 0;


	distro(deck,start,number,dist);

	for (i=0; i<4; i++) { hcp[i]=0; aces[i]=0; kings[i]=0; honors[i]=0;}

	/* get high card points counted up and put in hcp array  */
	/* also get the number of aces and their suits  */

	for (i=start; i<start + number; i++) {
		if ((j=deck[i] % 13) > 8) {
			hcp[deck[i] / 13] += j-8;
            honors[deck[i]/13] += (1 << (j-9));
            }
		if (j == 12) { ace_flag += 1; aces[deck[i]/13]=1;}
		if (j == 11) { kings[deck[i]/13]=1; }
		}

	/* add distributional points: 1,2,3 for shortness;
	   1,2 for length of 6,7; but minor suits require
		1 of top 3 honors Note: this was changed 6/13/94 from
		2 of top 3 honors to 1 */

	/* the rules are:
		add only 1 point for doubletons in support
		add 1 point for voids or singletons and 4 card support
		add 2 points for voids or singletons and 5+ support
		get rid of shortness points with no fit and keep them
			 the same with 3 card support.
	*/

	doubleton_flag=0;
	for (i=0; i<4; i++) {
		if (dist[i] < 3) {
			if (state == SUPPORT_OF) switch (dist[suit]) {
				case 0:
				case 1:
				case 2: break;
				default:
				case 5: if (dist[i] <2) count += 1;
				case 4: if (dist[i] <2) count += 1;
						if (dist[i] == 2 && doubleton_flag == 0) {
							count += 1;
							doubleton_flag = 1;
							}
				case 3: count += (3-dist[i]); break;
				}
			else count += (3-dist[i]);
			}

		if (dist[i] == 6 && i > 1) count += 1;	/* major */
		else if (dist[i] >= 7 && i > 1) count += 2;	/* major */
		else if (dist[i] == 6 && i < 2 && hcp[i]>1) count += 1; /* minor with honor */
		else if (dist[i] >= 7 && i < 2 && hcp[i]>1) count += 2;	/* minor with honor */
		}

	/* add up high card points  */

	for (i=0; i<4; i++)
		count += hcp[i];


// #define IN_OUT_VALUATION

	/* remove high card points based on opponents bidding; our suit and shortness */

#ifdef IN_OUT_VALUATION
	for (i=0; i<4; i++)
		if (opp_bid[i]) switch(honors[i]) {
			case 0: break;
			case 1: case 2: count--; break;
			case 3: if (dist[i] == 2) count -= 2; else count--; break;
 			case 4: case 5: case 6: if (opp_bid[i] & 1) count++; else count--; break;
			case 7: case 8: break;
			case 9: if (opp_bid[i] & 2) count--; break;
			case 10: case 11: if (opp_bid[i] & 1) count++; else count--; break;
			case 12: break;
			case 13: if (opp_bid[i] & 1) count++; else count--; break;
			default: break;
			}
		else if (state == SUPPORT_OF && i == suit) switch(honors[i]) {
			case 0: break;
			case 1: case 2: case 3: count++; break;
			case 4: break;
			case 5: count++; break;
			case 6: case 7:	case 8: break;
			case 9: count++; break;
			default:  break;
			}
		else switch(honors[i]) {
			case 0: break;
			case 1: case 2: if (dist[i] == 1) count--; break;
			case 3: if (dist[i] == 2) count--; break;
			case 4: if (dist[i] == 1) count--; break;
			case 5: case 6: if (dist[i] == 2) count--; break;
			case 7: case 8: break;
			case 9: if (dist[i] == 2) count--; break;
			default: break;
			}
#else
	/* subtract 1 point for unguarded honors */
    /* 1/16/96 - do not subtract unguarded honor in given suit */

	for (i=0; i<4; i++) {
        if (state == SUPPORT_OF && i == suit) /* not for support */ ;
		else {
		  if (dist[i]==1 && hcp[i] >0 && hcp[i]<4) count -= 1;
		  if (dist[i]==2 && hcp[i] >0
			 && hcp[i] != 3 && hcp[i]<6 && aces[i]==0) count -= 1;
          if (dist[i]==2 && hcp[i] == 3 && kings[i]==0) count -= 1;
		  if (dist[i]==3 && hcp[i] == 1 ) count -= 1;
          }
		}
#endif

    /* SUPPORTED points implemented 180413 by just adding more points for shortness.
           adding points for voids and singletons to make is 3,5 from 1,2. */
           
     for (i=0; i<4; i++) {
        if (state == SUPPORTED) {
            if (dist[i] == 0) count += 3;
            else if (dist[i] == 1) count += 2;
            }
        }
 
	/* for opening bids correct for all or none aces */
	/* changed 1/19/96 so that count is only reduced for hands that
		have more than 10 count points. This was done since the
		OPENING_BID count is used for purposes of original strength
		not just support, and hands such as those with 5 or 6 points
		would always end up with less whenever an ace did not make
		up a part of the hand. Hands over ten points should have an
        ace and thus deserve to be lowered in count */

	if (ace_flag == 4 && state == OPENING_BID) count += 1;
	if (count > 10 && ace_flag == 0 && state == OPENING_BID) count -= 1;

    if (count <0) count = 0;
	return(count);

}




#undef DEBUG_BERGEN
int bergen(int deck[52],int start,int number,int state, int suit)
{
	int i, j, dist[4], hcp[4], aces[4];
	int doubleton_flag = 0, kings[4];
    int honors[4], tens[4], topfive[4];
	int ace_flag, count, quacks, acetens;
	int quality, slength, dubious, hcps, adjust, tlength, xlength, shortness;
	quality = slength = dubious = hcps = adjust = tlength = xlength = shortness = 0;
	ace_flag = count = quacks = acetens = 0;

#ifdef DEBUG_BERGEN
	printf("\nstate: %d",state);
#endif

	distro(deck,start,number,dist);

	for (i=0; i<4; i++) { hcp[i]=0; aces[i]=0; kings[i]=0; honors[i]=0; tens[i]=0; topfive[i]=0; }

	/* get high card points totaled by suit and put in hcp array  */
	/* also get the number of honors including tens in their suits  */

	for (i=start; i<start + number; i++) {
		if ((j=deck[i] % 13) > 8) {
			hcp[deck[i] / 13] += (j-8);
            honors[deck[i]/13] += (1 << (j-9));
            }
		if (j>7) {
			topfive[deck[i]/13] += 1;
			if (j == 12) { ace_flag += 1; aces[deck[i]/13]=1; acetens += 1;}
			else if (j == 11) { kings[deck[i]/13]=1; }
			else if (j == 10 || j == 9) { quacks += 1; }
			else if (j == 8) { acetens += 1; }
			}
		}

	/* adjust-3 count aces + tens vs quacks 
	    for 0-2 difference then no change
		for 3-5 adjust by one point
		for 6+ (rare) adjust by two points */



	for (i=0; i<4; i++) {
		if (topfive[i] >= 3 && dist[i]>3) { count += 1; quality += 1; } /* quality suits of 4+ length */
		if (dist[i] < 3) {  /* SHORTNESS */  /* only after support has been established */
			if (state == SUPPORT_OF) switch (dist[suit]) {
				case 0:
				case 1:
				case 2: break;
				case 3: if (dist[i]== 0) { count += 3; shortness += 3;}  /* void */ /* three trump support */
						else if (dist[1] == 1) { count += 2; shortness += 2; }/* singleton */
						else { count += 1; shortness += 1;} break; /* doubleton */
				case 4: if (dist[i] == 0) { count += 4; shortness += 4; }/* four trump support */
						else if (dist[i] == 1) { count += 3; shortness += 3; }
						else { count += 1; shortness += 1; } break;
				default: if (dist[i] == 0) { count += 5; shortness += 5; } /* five+ trump support */
						 else if (dist[i] == 1) { count += 3; shortness += 3;}
						 else { count += 1; shortness += 1; } break;
				}
			else if (state == SUPPORTED) switch (dist[i]) {
				case 0: { count += 4; shortness += 4; } break;
				case 1: { count += 2; shortness += 2; } break;
				case 2: if (doubleton_flag > 0) { count += 1; shortness += 1; }  
					doubleton_flag += 1; break;
				default: break;
				}
			} /* SHORTNESS */
		if (dist[i]>4) { count += (dist[i]-4); slength += (dist[i]-4); }/* length of suit 5+ */
		if (dist[i]>4 && state == SUPPORTED) { /* correct when reconsidered 200316 */
			int ii;
			if (dist[i] > 5) {
				count += (dist[i] - 5); /* 5+ trump length additional after supported */
				tlength = dist[i] - 5;
				}
			for (ii=0; ii<4; ii++) /* look for side 4 card suit or longer */
				if (ii!=i && dist[ii] > 3 && xlength == 0) {
					count += 1;
					xlength += 1;
					}
			}
	}

#ifdef DEBUG_BERGEN
	printf("\nquality:%u",quality);
	printf("\nslength:%u",slength);
	if (tlength > 0) printf("\ntlength:%u",tlength);
	if (xlength > 0) printf("\nxlength:%u",xlength);
	if (shortness > 0) printf("\nshortness:%u",shortness);
	// printf("\ncount after length is %u (2)",count);
#endif
	/* add up high card points and subtract  DUBIOUS */

	for (i=0; i<4; i++) { /* i do not know how to do 1/2 point for singleton ace */
		count += hcp[i]; /* first add up hcps, then remove dubious honors */
		hcps += hcp[i];
		// printf("\ncount %u, hcp %u i %u",count,hcp[i],i);
		if (dist[i] == 1 && hcp[i]>0 && hcp[i] < 4) { count -= 1; dubious += 1; }  /* singleton k,q,j */
		else if (dist[i] == 2 && hcp[i]>0 && hcp[i] < 4 && kings[i]==0) { count -= 1; dubious += 1; } /* jx,qx,qj */
		else if (dist[i] == 2 && hcp[i] == 4 && aces[i] == 0) { count -= 1; dubious += 1; } /* kj */
		else if (dist[i] == 2 && hcp[i] == 5) { count -= 1; dubious += 1; } /* aj,kq */
		}
#ifdef DEBUG_BERGEN
	printf("\nhcps:%u",hcps);
	printf("\ndubious:%u",dubious);
#endif
	/* adjust-3 ace tens vs quacks */
	// printf("\ncount before adjust:%u",count);
	if (acetens - quacks >= 6) { count += 2; adjust += 2; }
	else if (acetens - quacks > 2) { count += 1; adjust += 1; }
	else if (quacks - acetens >= 6) { count -= 2; adjust -= 2; }
	else if (quacks - acetens > 2) { count -= 1; adjust -= 1; }
#ifdef DEBUG_BERGEN
	printf("\nadjust:%u",adjust);
	printf("\ncount:%u\n***********\n",count);
	if (state==BERGEN_HCPS) printf("\nhcps+adjust-dubious = %d",hcps+adjust+slength-dubious+quality);
#endif
	// 2019-8-26 can I really have been so wrong as to add quality to BERGEN_HCPS? removed
	if (state == BERGEN_HCPS) return hcps+adjust-dubious;
	else return(count);

}

int bbo(int deck[52],int start, int number, int state, int suit) { return 0; }
int klinger(int deck[52],int start,int number,int state, int suit)
{
	int i, j, dist[4], hcp[4], aces[4];
	int kings[4];
	
	int honors[4], tens[4], topfive[4];
	int ace_flag, count,ace_king_total=0;
    int has_4333_flag=0,no_aces_flag=0;
	int quality, slength, dubious, hcps, adjust, tlength, xlength, shortness;
	quality = slength = dubious = hcps = adjust = tlength = xlength = shortness = 0;
	ace_flag = count = 0;

	distro(deck,start,number,dist);

	for (i=0; i<4; i++) { hcp[i]=0; aces[i]=0; kings[i]=0; honors[i]=0; tens[i]=0; topfive[i]=0; }

	/* get high card points totaled by suit and put in hcp array  */
	/* also get the number of honors including tens in their suits  */

	for (i=start; i<start + number; i++) {
		if ((j=deck[i] % 13) > 8) {
			hcp[deck[i] / 13] += (j-8);
			honors[deck[i]/13] += (1 << (j-9));
			}
		if (j>10) { ace_king_total += (j-8); }
		if (j>7) {
			topfive[deck[i]/13] += 1;
			if (j == 12) { ace_flag += 1; aces[deck[i]/13]=1;}
			else if (j == 11) { kings[deck[i]/13]=1; }
			}
		}

	for (i=0; i<4; i++) {
		if (topfive[i] >= 3 && dist[i]>3) { count += 1; quality += 1; } /* quality suits of 4+ length */
		if (dist[i] < 3) {  /* SHORTNESS */  /* only after support has been established */
			if (state == SUPPORT_OF) switch (dist[suit]) {
				case 0:
				case 1:
				case 2: break;
				case 3: if (dist[i]== 0) { count += 3; shortness += 3;}  /* void */ /* three trump support */
						else if (dist[1] == 1) { count += 2; shortness += 2; }/* singleton */
						else { count += 1; shortness += 1;} break; /* doubleton */
				case 4: if (dist[i] == 0) { count += 4; shortness += 4; }/* four trump support */
						else if (dist[i] == 1) { count += 3; shortness += 3; }
						else { count += 1; shortness += 1; } break;
				default: if (dist[i] == 0) { count += 5; shortness += 5; } /* five+ trump support */
						 else if (dist[i] == 1) { count += 3; shortness += 3;}
						 else { count += 1; shortness += 1; } break;
				}
			else if (state == SUPPORTED) switch (dist[i]) {
				case 0: { count += 4; shortness += 3; } break;
				case 1: { count += 2; shortness += 2; } break;
				case 2: { count += 1; shortness += 1; } break;
				default: break;
				}
			}    /* SHORTNESS */

		if (dist[i]>4) { slength += (dist[i]-4); }/* length of suit over 4 */
		if (dist[i]>4 && state == SUPPORTED) {
			int ii;
			if (dist[i] > 5) {
				count += (dist[i] - 5); /* 5+ trump length additional after supported */
				tlength = dist[i] - 5;
				}
			for (ii=0; ii<4; ii++) /* look for side 4 card suit or longer */
				if (ii!=i && dist[ii] > 3 && xlength == 0) {
					count += 1;
					xlength += 1;
					}
			}
	}

#ifdef DEBUG_KLINGER
	printf("\nquality:%u",quality);
	printf("\nlength:%u",slength);
	if (tlength > 0) printf("\ntlength:%u",tlength);
	if (xlength > 0) printf("\nxlength:%u",xlength);
	if (shortness > 0) printf("\nshortness:%u",shortness);
	// printf("\ncount after length is %u (2)",count);
#endif
	
    /* add up high card points and subtract  DUBIOUS */
	// DUBIOUS for klinger is K,Q,J; Jx, Qx, QJ; 4333 distribution; no aces in 12 HCPs or higher hand

	for (i=0; i<4; i++) {
		hcps += hcp[i];
		if (aces[i]) no_aces_flag = 0;
		if (dist[i] < 3) has_4333_flag = 0; 
		if (dist[i] == 1 && hcp[i]>0 && hcp[i] < 4) { dubious += 1; }  /* singleton k,q,j */
		else if (dist[i] == 2 && hcp[i]>0 && hcp[i] < 4 && kings[i]==0) { dubious += 1; } /* jx,qx,qj */
		}
	if (hcps < 12) no_aces_flag = 0; // only 12 HCPs or more without an expected ace 

#ifdef DEBUG_KLINGER
	printf("\nhcps:%u",hcps);
	printf("\ndubious:%u",dubious);
#endif

	{
		int total;
	if (ace_king_total > 12) hcps += 1;
	total = hcps - dubious - no_aces_flag - has_4333_flag + shortness;
	if (state != KLINGER_HCPS)
    	 total = hcps - dubious - no_aces_flag - has_4333_flag + slength + shortness;
    else total = hcps - dubious - no_aces_flag - has_4333_flag;
//fixed 171016 because 4333 subtracts 1 point so 0 hand goes negative
	if (total < 0) total = 0;
	return total;
	}

}

int kaplan_rueben(int deck[52],int start,int number,int state, int suit)
{
	int i, j, dist[4], hcp[4], aces[4];
	int kings[4], queens[4], jacks[4], nines[4];
	
	int honors[4], topfive[4];
	float tens[4], accompanied[4],temp[4],subtotal[4],guards[4], four333=0.0,total;
	int ace_flag, count,ace_king_total=0;
	int quality, slength, dubious, hcps, adjust, tlength, xlength, shortness;
	quality = slength = dubious = hcps = adjust = tlength = xlength = shortness = 0;
	ace_flag = count = 0;

	distro(deck,start,number,dist);

	for (i=0; i<4; i++) { hcp[i]=0; aces[i]=0; kings[i]=0; honors[i]=0; tens[i]=0.0; topfive[i]=0;
		queens[i]=0; jacks[i]=0; temp[i]=0.0; subtotal[i]=0.0; total=0.0; guards[i]=0.0; four333=0.0;
		accompanied[i]=0.0;
		}

	/* get high card points totaled by suit and put in hcp array  */
	/* also get the number of honors including tens in their suits  */
	/* note that in KR a ten will 0.5 HCP, so get tens */

	for (i=start; i<start + number; i++) {
		if ((j=deck[i] % 13) > 8) {
			hcp[deck[i] / 13] += (j-8);
			honors[deck[i]/13] += (1 << (j-9));
			}
		if (j==12) { guards[deck[i]/13] += 3.0; }
		if (j==11) {
			if (dist[deck[i]/13]==1) guards[deck[i]/13] += 0.5;
			else guards[deck[i]/13] += 2.0;
			}
		if (j==10) {
			if (dist[deck[i]/13]>2) { // queen 3+
				if (aces[deck[i]/13] ==1 || kings[deck[i]/13] == 1)
					guards[deck[i]/13] += 1.0;
				else guards[deck[i]/13] += 0.75;
				}
			else if (dist[deck[i]/13]==2) {
				if (aces[deck[i]/13] ==1 || kings[deck[i]/13] == 1)
					guards[deck[i]/13] += 0.5;
				else guards[deck[i]/13] += 0.25;
				}
			}
		if (j==9) { // jack
			if (topfive[deck[i]/13] == 2) guards[deck[i]/13] += 0.5;
			else if (topfive[deck[i]/13] ==1) guards[deck[i]/13] += 0.25;
			}
		if (j==8) { // ten
			if (topfive[deck[i]/13] == 2) guards[deck[i]/13] += 0.5;
			}
		if (j==7) { // nine and, next,  ten
			if (tens[deck[i]/13] > 0.4 && topfive[deck[i]/13] == 2) guards[deck[i]/13] += 0.25;
			}
		if (j>10) { ace_king_total += (j-8); }
		if (j>7) {
			topfive[deck[i]/13] += 1;
			if (j == 12) { ace_flag += 1; aces[deck[i]/13]=1;}
			else if (j == 11) { kings[deck[i]/13]=1; }
			else if (j==8) {
				tens[deck[i]/13]=0.5;
				if (jacks[deck[i]/13]==1) accompanied[deck[i]/13]= 0.5;
				else if (topfive[deck[i]/13] > 2) accompanied[deck[i]/13]=0.5;
				}
			}
		if (j==7) {  // nine with ten or two higher
			nines[deck[i]/13] = 1;
			if (topfive[deck[i]/13] > 1) accompanied[deck[i]/13] += 0.5;
			else if (tens[deck[i]/13] > 0.4) accompanied[deck[i]/13] += 0.5;
			if (topfive[deck[i]/13] == 3 && tens[deck[i]/13] < 0.4 &&
					dist[i]>3 && dist[i]<7) temp[deck[i]/13] += 0.5;
			}
		if (j==6) { // eight
			if (nines[deck[i]/13] && topfive[deck[i]/13] < 2 && tens[deck[i]/13] < 0.4)
				accompanied[deck[i]/13] += 0.5; // eight accompanies nine
			if (temp[deck[i]/13]>0.4) temp[deck[i]/13] = 0.0; // no eight allowed
			}
		}
	for (i=0; i<4; i++) {
		if (temp[i] > 0.4) accompanied[i] += 0.5;
		if (dist[i] > 6 && (queens[i]==0 || jacks[i]==0)) accompanied[i] += 1.0;
		if (dist[i] > 7 && queens[i]==0) accompanied[i] += 1.0;
		if (dist[i] > 8 && queens[i]==0 && jacks[i]==0) accompanied[i] += 1.0;
		}
	for (i=0; i<4; i++) {
		float flen = (float) dist[i]; 
		subtotal[i] = (float) hcp[i]; // a-j 4321
		subtotal[i] += tens[i]; // add 0.5 if
		subtotal[i] += accompanied[i]; // add the accompanied totals
		subtotal[i] *= flen;
		subtotal[i] /= 10.0;
		}
	for (i=0; i<4; i++) {
		subtotal[i] += guards[i];
		}
	for (i=0; i<4; i++) {
		if (dist[i]<3 || dist[i]>4) four333=0.5;
		if (dist[i]==0) subtotal[i] += 3.0;
		else if (dist[i]==1) subtotal[i] += 2.0;
		else if (dist[i]==2) subtotal[i] += 1.0;
		}
	total = four333;
	for (i=0; i<4; i++) {
		total += subtotal[i];
		}
	return (int) floor(total+0.5);
}


int hoffer_count(int deck[52],int start,int number,int state, int suit)
{
    int i, j, dist[4], hcp_total=0;
    int count=0, shortness=0, slength=0;
    distro(deck,start,number,dist);
    
    /* get high card points  */
    
    for (i=start; i<start + number; i++) {
        if ((j=deck[i] % 13) > 8) {
            hcp_total += (j-8);
        }
    }

    for (i=0; i< 4; i++) {
      if (dist[i] < 3) {  /* SHORTNESS */  /* only after support has been established */
        if (state == SUPPORT_OF) switch (dist[suit]) {
            case 0:
            case 1:
            case 2: break;
            case 3: if (dist[i]== 0) { count += 3; shortness += 3;}  /* void */ /* three trump support */
            else if (dist[1] == 1) { count += 2; shortness += 2; }/* singleton */
            else { count += 1; shortness += 1;} break; /* doubleton */
            case 4: if (dist[i] == 0) { count += 4; shortness += 4; }/* four trump support */
            else if (dist[i] == 1) { count += 3; shortness += 3; }
            else { count += 1; shortness += 1; } break;
            default: if (dist[i] == 0) { count += 5; shortness += 5; } /* five+ trump support */
            else if (dist[i] == 1) { count += 3; shortness += 3;}
            else { count += 1; shortness += 1; } break;
        }
        else if (state == SUPPORTED) switch (dist[i]) {
            case 0: { count += 4; shortness += 3; } break;
            case 1: { count += 2; shortness += 2; } break;
            case 2: { count += 1; shortness += 1; } break;
            default: break;
        }
      }    /* SHORTNESS */
    
      if (dist[i]>4) { slength += (dist[i]-4); }/* length of suit over 4 */
    }
    return hcp_total + slength + shortness;
}

