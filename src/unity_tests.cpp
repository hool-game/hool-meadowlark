#if !defined(linux)
/* Versions
210209
*/
#include "meadowlark.h"
#undef GLOBALS
#include "../include/GLOBALS.H"

#if !defined(ANDROID) && !defined(__ANDROID__)
#ifdef USE_CSWITCH
#pragma message("cswitch is on at start of unity_tests")
#endif

#ifdef UNITY_TESTING
#include "unity.h"
#include "unity_internals.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#ifdef IOS_ONLY
#include <time.h>
#endif

#undef USE_CSWITCH
#ifdef USE_CSWITCH
#pragma message("cswitch is on at start of unity_tests")
#else
// #pragma message("cswitch is off later of unity_tests")
#endif


#include "constraint_ids.h"

void initialize(void);
void gen_init2(void);
int getargs2(const char *cmd_line);
void print_triggers(void);
int trigger(int);
int set_trigger(const char *);
int deck_verify(int);
int constraint_deal(int first, int num);
int constraint_deal_n(int first, int num, int loops);
void setup_logging(int, char *);
#ifdef BEST_HOFFER
int check_constraints(int [4]);
#else
int check_constraints(int);
#endif
int add_constraint(int, int, int, int, int);
extern int decks[DECKNUM][52];
extern int gen32_deck[52];
char *get_binary(int);
char *get_description(int i);
int set_offsets(int);
void printout(int deckp[52],int first);
char *gen32_get_description2(int i, int v);
void clear_constraints(int start);








void setUp(void) {
	static int initialized=0;
	int num_of_hands;
#ifdef USE_CSWITCH
	ONE_SHOT(1,fprintf(stderr,"setUp entered\n"));
#endif
	if (!initialized) {
#ifdef USE_CSWITCH
		TRIGGER(1,fprintf(stderr,"initializing generate\n"));
#endif
		initialize();
		deck_verify(0);
		num_of_hands = set_offsets(15);
		TEST_ASSERT_EQUAL_INT(4,num_of_hands);
		initialized=1;
		}
	gen_init2();
    // set stuff up here
}

void tearDown(void) {
    // clean stuff up here
	clear_constraints(0);
}

void test_process(void) {
#ifdef USE_CSWITCH
	TRIGGER(1,fprintf(stderr,"test_process entered\n"));
#endif
	}

void test_add_constraint(void) {
	int result,loops_per,num=50;
	result = add_constraint(0,ID_HCP,0,5,5);
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,7,0,0,0);
	TEST_ASSERT_EQUAL_INT(1,result);
	loops_per = constraint_deal(0,num);
	TEST_ASSERT_GREATER_THAN(1,loops_per);
	}

#undef TEST_VERSION_2
void test_constraint_deal(void) { // tests DIST for length of a given suit and LEN relative lengths
	char *hand;
	unsigned int len;
	int hcount;
	char hcp_count[3];
	char *adescription;
	int num=50,i,j;
#ifdef TEST_VERSION_2
	int result,loops_per;
	test_add_constraint();
	result = add_constraint(0,ID_DIST,0,7,9); // clubs is 7-9
	result = add_constraint(0,7,2,0,0);
	result = add_constraint(0,ID_DIST,3,2,2); // spades is 2
	result = add_constraint(0,7,4,0,0);
	result = add_constraint(0,ID_LEN,3,0,1); // len of shortest suit is 0 or 1
	result = add_constraint(0,7,6,0,0);
	loops_per = constraint_deal(0,num);
#else
	int num_of_hands,result,loops_per;
	num_of_hands = set_offsets(15);
	result = add_constraint(0,ID_HCP,0,5,5);
	result = add_constraint(0,7,0,0,0);
	loops_per = constraint_deal(0,num);
#endif
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"%s\n",adescription);
		hcp_count[0]=adescription[1];
		hcp_count[1]=adescription[2];
		hcp_count[2]='\0';
		sscanf(hcp_count,"%u",&hcount); 
		TEST_ASSERT_EQUAL_INT(5,hcount);
		}
	// fprintf(stderr,"loops_per:%u\n",loops_per);

	hand = get_binary(0);
	TEST_ASSERT_NOT_NULL(hand);
	adescription = gen32_get_description2(0,0);
	TEST_ASSERT_NOT_NULL(adescription);
	// fprintf(stderr,"\n%s\n",adescription);
	len = (int) strlen(adescription);
	TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
	}

void test_has(void) {
	int result,num=25,i,j,hcount;
	char hcp_count[3];
	unsigned int loops_per;
	char *adescription;
	result = add_constraint(0,ID_HAS,51,0,0); // has AS
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,7,0,0,0);
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(0,ID_HAS,50,0,0); // has KS
	TEST_ASSERT_EQUAL_INT(2,result);
	result = add_constraint(0,7,2,0,0);
	TEST_ASSERT_EQUAL_INT(3,result);
	result = add_constraint(0,ID_HASNOT,49,0,0); // hasnot QS
	TEST_ASSERT_EQUAL_INT(4,result);
	result = add_constraint(0,7,4,0,0);
	TEST_ASSERT_EQUAL_INT(5,result);
	result = add_constraint(0,ID_HASNOT,48,0,0); // hasnot JS
	TEST_ASSERT_EQUAL_INT(6,result);
	result = add_constraint(0,7,6,0,0);
	TEST_ASSERT_EQUAL_INT(7,result);
	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		// printout(deck,0);
		// fprintf(stderr,"\n");
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"%s\n",adescription);
		hcp_count[0]=adescription[34];
		hcp_count[1]=adescription[35];
		hcp_count[2]='\0';
		sscanf(hcp_count,"%u",&hcount); 
		TEST_ASSERT_TRUE(hcount==12);
		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);
	}

void test_quality(void) {
	int result,num=100,i,j;
	unsigned int qcount;
	char quality_count[2];
	unsigned int loops_per;
	char *adescription;
	result = add_constraint(0,ID_QUALITY,3,4,4); // spades good enough for 4 level
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,7,0,0,0);
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(1,ID_QUALITY,2,4,4); // hearts good enough for 4 level
	TEST_ASSERT_EQUAL_INT(2,result);
	result = add_constraint(1,7,2,0,0);
	TEST_ASSERT_EQUAL_INT(3,result);

	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"\n%s",adescription);
		quality_count[0]=adescription[92];
		quality_count[1]='\0';
		sscanf(quality_count,"%u",&qcount); 
		// fprintf(stderr,"\nqcount:%u",qcount);
		TEST_ASSERT_TRUE(qcount==4);
        // TEST_ASSERT_TRUE(qcount<5);
		// printout(deck,13);
		// fprintf(stderr,"\n");

		adescription = gen32_get_description2(1,0);
		// fprintf(stderr,"\n%s",adescription);
		quality_count[0]=adescription[90];
		quality_count[1]='\0';
		sscanf(quality_count,"%u",&qcount);
		// fprintf(stderr,"\nqcount%u",qcount);
		TEST_ASSERT_TRUE(qcount==4);

		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);
	}

void test_intermediates(void) {
	int result,num=25,i,j;
	// ,hcount;
	// char hcp_count[3];
	unsigned int loops_per,intermediates;
	size_t len;
	char *adescription;
	char intermediate_count[2];

	result = add_constraint(0,ID_INTERMEDIATES,3,4,4); // spades good enough for 4 level
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,7,0,0,0);
	TEST_ASSERT_EQUAL_INT(1,result);
	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		// printout(deck,0);
		// fprintf(stderr,"\n");
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"%s\n",adescription);
		len = strlen(adescription);
		TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
		intermediate_count[0]=adescription[D_LEN-3];
		intermediate_count[1]='\0';
		sscanf(intermediate_count,"%u",&intermediates); 
		TEST_ASSERT_TRUE(intermediates==4);
		}
	// fprintf(stderr,"intermediates:loops_per:%d\n",loops_per);
	}

void test_shortness(void) {
	int result,num=25,i,j;
	// ,hcount;
	// char hcp_count[3];
	unsigned int loops_per,voids,singletons;
	size_t len;
	char *adescription;
	char void_count[2],singleton_count[2];


	result = add_constraint(0,ID_VOID,3,0,0); // north has a spade void
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,7,0,0,0);
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(1,ID_SINGLETON,2,0,0); // east has a heart singleton
	TEST_ASSERT_EQUAL_INT(2,result);
	result = add_constraint(0,7,2,0,0);
	TEST_ASSERT_EQUAL_INT(3,result);
	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		// fprintf(stderr,"North\n");
		// printout(deck,0);
		// fprintf(stderr,"\n");
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"%s\n",adescription);
		void_count[0]=adescription[18];
		void_count[1]='\0';
		sscanf(void_count,"%u",&voids); 

		adescription = gen32_get_description2(1,0);
		// fprintf(stderr,"East\n");
		// printout(deck,13);
		// fprintf(stderr,"\n");
		// fprintf(stderr,"%s\n",adescription);
		singleton_count[0]=adescription[17];
		singleton_count[1]='\0';
		sscanf(singleton_count,"%u",&singletons); 
		TEST_ASSERT_EQUAL_INT(0,voids);
		TEST_ASSERT_EQUAL_INT(1,singletons);
		len = strlen(adescription);
		TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);
	}

void test_losers(void) {
	int result,num=25,i,j;
	// ,hcount;
	// char hcp_count[3];
	unsigned int loops_per,losers1,losers2;
	size_t len;
	char *adescription;
	char losers1_count[3],losers2_count[3];


	result = add_constraint(0,ID_LOSERS,0,7,8); // north has 7,8 losers
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,7,0,0,0);
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(1,ID_LOSERS,0,4,5); // east has 4,5 losers
	TEST_ASSERT_EQUAL_INT(2,result);
	result = add_constraint(0,7,2,0,0);
	TEST_ASSERT_EQUAL_INT(3,result);
	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		adescription = gen32_get_description2(0,0);
        if (/* DISABLES CODE */ (0)) {
			fprintf(stderr,"North:%d\n",j);
			printout(gen32_deck,0);
			fprintf(stderr,"\n");
			fprintf(stderr,"%s\n",adescription);
			}
		losers1_count[0]=adescription[21];
		losers1_count[1]=adescription[22];
		losers1_count[2]='\0';
		sscanf(losers1_count,"%u",&losers1); 
		TEST_ASSERT(losers1>6);
		TEST_ASSERT(losers1<9);
		adescription = gen32_get_description2(1,0);
        if (/* DISABLES CODE */ (0)) {
			fprintf(stderr,"East\n");
			printout(gen32_deck,13);
			fprintf(stderr,"\n");
			fprintf(stderr,"%s\n",adescription);
			}
		losers2_count[0]=adescription[21];
		losers2_count[1]=adescription[22];
		losers2_count[2]='\0';
		sscanf(losers2_count,"%u",&losers2); 
		TEST_ASSERT(losers2>3);
		TEST_ASSERT(losers2<6);
		len = strlen(adescription);
		TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);
	}

void test_aces(void) {
	int result,num=25,i,j;
	// ,hcount;
	// char hcp_count[3];
	unsigned int loops_per,len,aces,total_aces=0;
	char *adescription;
	char aces_count[3];


	result = add_constraint(0,ID_ACES,0,0,0); // north has 0 aces
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,7,0,0,0);
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(1,ID_ACES,0,2,3); // east has 2 or 3 aces
	TEST_ASSERT_EQUAL_INT(2,result);
	result = add_constraint(0,7,2,0,0);
	TEST_ASSERT_EQUAL_INT(3,result);
	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		// printout(deck,0);
		// fprintf(stderr,"\n");
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"ad:%s\n",adescription);
		total_aces = 0;
		for (i=0; i<4; i++) {
			aces_count[0] = adescription[25+i*3];
			aces_count[1] = adescription[26+i*3];
			aces_count[2] = '\0';
			// fprintf(stderr,"aces_count:%s\n",aces_count);
			sscanf(aces_count,"%2u",&aces);
			if (aces & 8) total_aces += 1;
			} 
		TEST_ASSERT(total_aces==0);
		total_aces=0;
		adescription = gen32_get_description2(1,0);
		// fprintf(stderr,"%s\n",adescription);
		for (i=0; i<4; i++) {
			aces_count[0] = adescription[25+i*3];
			aces_count[1] = adescription[26+i*3];
			aces_count[2] = '\0';
			// fprintf(stderr,"aces_count:%s\n",aces_count);
			sscanf(aces_count,"%2u",&aces);
			if (aces & 8) total_aces += 1;
			} 
		TEST_ASSERT(total_aces > 0 && total_aces < 4);
		len = (unsigned int) strlen(adescription);
		TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);
	}

void test_more_or_less(void) {
	int result,num=25,i,j;
	char count[3];
	unsigned int loops_per,spades=0,hearts=0,clubs=0;
	size_t len;
	char *adescription;

	result = add_constraint(0,ID_COMPARE,ID_MORE,3,2); // more spades than hearts
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,7,0,0,0);
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(0,ID_COMPARE,ID_LESS,0,2); // less clubs than hearts
	TEST_ASSERT_EQUAL_INT(2,result);
	result = add_constraint(0,7,2,0,0);
	TEST_ASSERT_EQUAL_INT(3,result);
	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		// printout(deck,0);
		// fprintf(stderr,"\n");
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"%s\n",adescription);
		count[1] = '\0';
		count[0] = adescription[18];
			sscanf(count,"%2u",&spades);
		count[0] = adescription[17];
			sscanf(count,"%2u",&hearts);
		count[0] = adescription[15];
			sscanf(count,"%2u",&clubs);
			} 
		TEST_ASSERT(clubs < hearts);
		TEST_ASSERT(hearts < spades);
		len = strlen(adescription);
		TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);

void test_stoppers(void) {
	int result,num=25,i,j;
	char count[3];
	unsigned int loops_per,clubs,spades;
	size_t len;
	char *adescription;

	result = add_constraint(0,ID_DIST,3,3,3); // three spades
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,7,0,0,0);
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(0,ID_HAS,50,0,0); // I have the king of spades
	TEST_ASSERT_EQUAL_INT(2,result);
	result = add_constraint(0,7,2,0,0);
	TEST_ASSERT_EQUAL_INT(3,result);
	result = add_constraint(0,ID_DIST,0,0,0); // no clubs = no stoppers
	TEST_ASSERT_EQUAL_INT(4,result);
	result = add_constraint(0,7,4,0,0);
	TEST_ASSERT_EQUAL_INT(5,result);
	result = add_constraint(0,ID_HASNOT,51,0,0); // have as
	TEST_ASSERT_EQUAL_INT(6,result);
	result = add_constraint(0,7,6,0,0);
	TEST_ASSERT_EQUAL_INT(7,result);
	result = add_constraint(0,ID_HAS,49,0,0); // have qs
	TEST_ASSERT_EQUAL_INT(8,result);
	result = add_constraint(0,ID_NOT,8,0,0);
	TEST_ASSERT_EQUAL_INT(9,result);

	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		// printout(deck,0);
		// fprintf(stderr,"\n");
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"%s\n",adescription);
		count[2] = '\0';
		count[0] = adescription[82];
		count[1] = adescription[83];
			sscanf(count,"%2u",&spades);
			// fprintf(stderr,"ss:%s\n",count);
		count[0] = adescription[76];
		count[1] = adescription[77];
			sscanf(count,"%2u",&clubs);
			// fprintf(stderr,"sc:%s\n",count);
		TEST_ASSERT_EQUAL_INT(1,spades);
		TEST_ASSERT_EQUAL_INT(0,clubs);
		len = strlen(adescription);
		TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);
}



void test_equal(void) {
	int result,num=25,i,j;
	char count[3];
	unsigned int loops_per,spades=0,hearts=0,clubs=0;
	size_t len;
	char *adescription;

	result = add_constraint(0,ID_COMPARE,ID_EQUAL,0,2); // clubs and hearts are equal
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,7,0,0,0);
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(0,ID_COMPARE,ID_MORE_OR_EQ,3,2); // more spades or equal spades to hearts
	TEST_ASSERT_EQUAL_INT(2,result);
	result = add_constraint(0,7,2,0,0);
	TEST_ASSERT_EQUAL_INT(3,result);
	result = add_constraint(0,ID_COMPARE,ID_LESS_OR_EQ,0,3); // less clubs or equal than spades
	TEST_ASSERT_EQUAL_INT(4,result);
	result = add_constraint(0,7,4,0,0);
	TEST_ASSERT_EQUAL_INT(5,result);
	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		// printout(deck,0);
		// fprintf(stderr,"\n");
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"%s\n",adescription);
		count[1] = '\0';
		count[0] = adescription[18];
			sscanf(count,"%2u",&spades);
		count[0] = adescription[17];
			sscanf(count,"%2u",&hearts);
		count[0] = adescription[15];
			sscanf(count,"%2u",&clubs);
			} 
		TEST_ASSERT(clubs == hearts);
		TEST_ASSERT(spades >= hearts);
		TEST_ASSERT(clubs <= spades);
		len = strlen(adescription);
		TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);

void test_roth(void) {
	int result,num=25,i,j;
	char count[3];
	unsigned int loops_per,roth,supported,support_of;
	size_t len;
	char *adescription;

	result = add_constraint(0,ID_ROTH,0,10,12); // roth points
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,7,0,0,0);
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(0,ID_SUPPORTED,0,13,14); // I have been supported points 13,14 gains a bit of points for supported in contract of clubs
	TEST_ASSERT_EQUAL_INT(2,result);
	result = add_constraint(0,7,2,0,0);
	TEST_ASSERT_EQUAL_INT(3,result);
	result = add_constraint(0,ID_SUPPORT,3,10,15); // points when I support spades is 10,15
	TEST_ASSERT_EQUAL_INT(4,result);
	result = add_constraint(0,7,4,0,0);
	TEST_ASSERT_EQUAL_INT(5,result);

	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		// printout(gen32_deck,0);
		// fprintf(stderr,"\n");
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"%s\n",adescription);
		count[2] = '\0';
		count[0] = adescription[39];
		count[1] = adescription[40];
		sscanf(count,"%2u",&roth);
		// fprintf(stderr,"r:%s\n",count);
		count[0] = adescription[41];
		count[1] = adescription[42];
		sscanf(count,"%2u",&supported);
		// fprintf(stderr,"s:%s\n",count);
		count[0] = adescription[52];
		count[1] = adescription[53];
		sscanf(count,"%2u",&support_of);
		// fprintf(stderr,"sos:%s\n",count);
		TEST_ASSERT(roth > 9);
		TEST_ASSERT(roth < 13);
		TEST_ASSERT(supported > 12);
		TEST_ASSERT(supported < 15);
		TEST_ASSERT(support_of > 9);
		TEST_ASSERT(support_of < 16);

		len = strlen(adescription);
		TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);
}

void test_xor(void) {
	int result,num=25,i,j;
	char count[3];
	unsigned int loops_per,spades,hearts;
	size_t len;
	char *adescription;

	result = add_constraint(0,ID_DIST,3,5,5); // I have 5 spades
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,ID_DIST,2,5,5); // I have 5 hearts
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(0,ID_XOR,0,0,1); // xor the spades and hearts
	TEST_ASSERT_EQUAL_INT(2,result);


	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		// fprintf(stderr,"hand:%u\n",j);
		// printout(deck,0);
		// fprintf(stderr,"\n");
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"%s\n",adescription);
		count[1] = '\0';
		count[0] = adescription[18];
			sscanf(count,"%2u",&spades);
		count[0] = adescription[17];
			sscanf(count,"%2u",&hearts);
		TEST_ASSERT(hearts != spades);
		TEST_ASSERT(spades == 5 || hearts == 5);
		len = strlen(adescription);
		TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);
}

void test_or(void) { // note that ID_OR can use a sequence of constraints, not just two as in ID_XOR
	int result,num=25,i,j;
	char count[3];
	unsigned int loops_per,spades,hearts,diamonds,clubs;
	size_t len;
	char *adescription;

	result = add_constraint(0,ID_DIST,3,5,5); // I have 5 spades
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,ID_DIST,2,5,5); // I have 5 hearts
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(0,ID_DIST,1,5,5); // I have 5 diamonds
	TEST_ASSERT_EQUAL_INT(2,result);
	result = add_constraint(0,ID_DIST,0,5,5); // I have 5 clubs
	TEST_ASSERT_EQUAL_INT(3,result);
	result = add_constraint(0,ID_OR,0,0,3); // or the 4 suits to find a 5 card suit
	TEST_ASSERT_EQUAL_INT(4,result);


	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		// fprintf(stderr,"hand:%u\n",j);
		// printout(deck,0);
		// fprintf(stderr,"\n");
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"%s\n",adescription);
		count[1] = '\0';
		count[0] = adescription[18];
			sscanf(count,"%2u",&spades);
		count[0] = adescription[17];
			sscanf(count,"%2u",&hearts);
		count[0] = adescription[16];
			sscanf(count,"%2u",&diamonds);
		count[0] = adescription[15];
			sscanf(count,"%2u",&clubs);
		TEST_ASSERT(spades == 5 || hearts == 5 || diamonds == 5 || clubs == 5);
		len = strlen(adescription);
		TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);
}

void test_and(void) { // note that ID_OR can use a sequence of constraints, not just two as in ID_XOR
	int result,num=25,i,j;
	char count[3];
	unsigned int loops_per,spades,hearts,diamonds,clubs;
	size_t len;
	char *adescription;

	result = add_constraint(0,ID_DIST,3,3,3); // I have 3 spades
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,ID_DIST,2,3,3); // I have 3 hearts
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(0,ID_DIST,1,3,3); // I have 3 diamonds
	TEST_ASSERT_EQUAL_INT(2,result);
	result = add_constraint(0,ID_AND,0,0,2); // and the 3 suits  and it will be 3334
	TEST_ASSERT_EQUAL_INT(3,result);


	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		// fprintf(stderr,"hand:%u\n",j);
		// printout(deck,0);
		// fprintf(stderr,"\n");
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"%s\n",adescription);
		count[1] = '\0';
		count[0] = adescription[18];
			sscanf(count,"%2u",&spades);
		count[0] = adescription[17];
			sscanf(count,"%2u",&hearts);
		count[0] = adescription[16];
			sscanf(count,"%2u",&diamonds);
		count[0] = adescription[15];
			sscanf(count,"%2u",&clubs);
		TEST_ASSERT(spades == 3 && hearts == 3 && diamonds == 3);
		len = strlen(adescription);
		TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);
}

void test_quicks(void) {
	int result,num=25,i,j;
	char count[3];
	unsigned int loops_per,quicks,clubs,diamonds,hearts,spades;
	size_t len;
	char *adescription;

	result = add_constraint(0,ID_HCP,0,14,15);
	TEST_ASSERT_EQUAL_INT(0,result);
	result = add_constraint(0,7,0,0,0);
	TEST_ASSERT_EQUAL_INT(1,result);
	result = add_constraint(0,ID_HAS,50,0,0); // I have the king of spades
	TEST_ASSERT_EQUAL_INT(2,result);
	result = add_constraint(0,7,2,0,0);
	TEST_ASSERT_EQUAL_INT(3,result);
	result = add_constraint(0,ID_HAS,51,0,0); // have as
	TEST_ASSERT_EQUAL_INT(4,result);
	result = add_constraint(0,7,4,0,0);
	TEST_ASSERT_EQUAL_INT(5,result);
	result = add_constraint(0,ID_SUIT_QUICKS,0,4,4); // clubs has AK
	TEST_ASSERT_EQUAL_INT(6,result);
	result = add_constraint(0,7,6,0,0);
	TEST_ASSERT_EQUAL_INT(7,result);



	loops_per = constraint_deal(0,num);
	for (j=0;j<num;j++) {
		for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
		// printout(deck,0);
		// fprintf(stderr,"\n");
		adescription = gen32_get_description2(0,0);
		// fprintf(stderr,"%s\n",adescription);
		count[2] = '\0';
		count[0] = adescription[60];
		count[1] = adescription[61];
			sscanf(count,"%2u",&quicks);
			// fprintf(stderr,"ss:%s\n",count);
		count[0] = adescription[62];
		count[1] = adescription[63];
			sscanf(count,"%2u",&clubs);
			// fprintf(stderr,"sc:%s\n",count
		count[0] = adescription[65];
		count[1] = adescription[66];
			sscanf(count,"%2u",&diamonds);
		count[0] = adescription[68];
		count[1] = adescription[69];
			sscanf(count,"%2u",&hearts);
		count[0] = adescription[71];
		count[1] = adescription[72];
			sscanf(count,"%2u",&spades);

		TEST_ASSERT_EQUAL_INT(8,quicks); // it is twice expected so as to not use fractions
		TEST_ASSERT_EQUAL_INT(4,spades); // ""
		TEST_ASSERT_EQUAL_INT(0,hearts);
		TEST_ASSERT_EQUAL_INT(0,diamonds);
		TEST_ASSERT_EQUAL_INT(4,clubs);
		len = strlen(adescription);
		TEST_ASSERT_EQUAL_INT(D_LEN-1,len);
		}
	// fprintf(stderr,"loops_per:%d\n",loops_per);
}

extern char *constraint_command(int who, int what, int where, int low, int hi);
void test_get_hand(void) {
    char *hand;
    char *answer;
    int result,num=25,j;
    // unsigned int loops_per;

    result = add_constraint(0,ID_HCP,0,14,15);
    TEST_ASSERT_EQUAL_INT(0,result);
    result = add_constraint(0,7,0,0,0);
    TEST_ASSERT_EQUAL_INT(1,result);

    result = add_constraint(0,ID_KNOW,51,0,0);
    TEST_ASSERT_EQUAL_INT(2,result);
    result = add_constraint(0,7,2,0,0);
    TEST_ASSERT_EQUAL_INT(3,result);
    
    result = add_constraint(1,ID_KNOW,50,0,0);
    TEST_ASSERT_EQUAL_INT(4,result);
    result = add_constraint(0,7,4,0,0);
    TEST_ASSERT_EQUAL_INT(5,result);
    
    result = add_constraint(2,ID_KNOW,49,0,0);
    TEST_ASSERT_EQUAL_INT(6,result);
    result = add_constraint(0,7,6,0,0);
    TEST_ASSERT_EQUAL_INT(7,result);

    result = add_constraint(3,ID_KNOW,48,0,0);
    TEST_ASSERT_EQUAL_INT(8,result);
    result = add_constraint(0,7,8,0,0);
    TEST_ASSERT_EQUAL_INT(9,result);


    // loops_per = constraint_deal(0,num);
    answer = constraint_command(0,ID_DEAL,0,0,num);
    
    for (j=0;j<num;j++) {
        // for (i=0;i<52;i++) deck[i] = decks[j][i];
        hand = constraint_command(0,ID_GET_HAND,j,0,0);
        // fprintf(stderr,"%s\n",hand);
        }
    // fprintf(stderr,"loops_per:%d\n",loops_per);
}

void test_get_playing_hand(void) {
    char *hand;
    int result,num=25,j;
    // unsigned int loops_per;
    char *answer;

    result = add_constraint(0,ID_HCP,0,14,15);
    TEST_ASSERT_EQUAL_INT(0,result);
    result = add_constraint(0,7,0,0,0);
    TEST_ASSERT_EQUAL_INT(1,result);

    result = add_constraint(0,ID_PLAYED,51,0,0);
    TEST_ASSERT_EQUAL_INT(2,result);
    result = add_constraint(0,7,2,0,0);
    TEST_ASSERT_EQUAL_INT(3,result);
    
    result = add_constraint(1,ID_PLAYED,50,0,0);
    TEST_ASSERT_EQUAL_INT(4,result);
    result = add_constraint(0,7,4,0,0);
    TEST_ASSERT_EQUAL_INT(5,result);
    
    result = add_constraint(2,ID_PLAYED,49,0,0);
    TEST_ASSERT_EQUAL_INT(6,result);
    result = add_constraint(0,7,6,0,0);
    TEST_ASSERT_EQUAL_INT(7,result);

    result = add_constraint(3,ID_PLAYED,48,0,0);
    TEST_ASSERT_EQUAL_INT(8,result);
    result = add_constraint(0,7,8,0,0);
    TEST_ASSERT_EQUAL_INT(9,result);

    

    // loops_per = constraint_deal(0,num);
    answer = constraint_command(0,ID_DEAL,0,0,num);
    for (j=0;j<num;j++) {
        // for (i=0;i<52;i++) deck[i] = decks[j][i];
        hand = constraint_command(0,ID_GET_PLAYING_HAND,j,0,0);
        // fprintf(stderr,"%s\n",hand);
        }
    // fprintf(stderr,"loops_per:%d\n",loops_per);
}

void test_speed(void) {
    // char *hand;
    int result,num=25;
    // unsigned int loops_per;
    char *answer;

    
    result = add_constraint(2,1,3,0,3);
    result = add_constraint(2,16,17,3,1);
    result = add_constraint(2,4,0,0,1);
    result = add_constraint(2,1,2,0,3);
    result = add_constraint(2,16,17,2,1);
    result = add_constraint(2,4,0,3,4);
    result = add_constraint(2,1,1,4,9);
    result = add_constraint(2,16,19,0,1);
    result = add_constraint(2,3,0,6,7);
    
    result = add_constraint(3,41,1,0,0);
    result = add_constraint(3,1,3,5,8);
    result = add_constraint(3,12,3,1,7);
    result = add_constraint(3,3,0,9,11);
    
    // result = add_constraint(0,1,1,0,2);
    // result = add_constraint(0,1,0,4,5);
    // result = add_constraint(0,1,2,4,5);
    // result = add_constraint(0,3,0,13,15);
    
    result = add_constraint(1,1,3,3,5);
    result = add_constraint(1,26,3,0,40);
    result = add_constraint(1,3,0,13,14); // show 13-14 on theirs, was 17,18
    
    result = add_constraint(3,14,0,14,16);
    // result = add_constraint(0,14,0,7,10);
    result = add_constraint(1,14,0,7,10);
    result = add_constraint(2,14,0,13,15);
    result = add_constraint(0,3,0,16,18); // was 20,23
    
    result = add_constraint(0,28,47,0,0);
    result = add_constraint(0,7,20,0,0);
    result = add_constraint(0,28,42,0,0);
    result = add_constraint(0,7,22,0,0);
    result = add_constraint(0,28,40,0,0);
    result = add_constraint(0,7,24,0,0);
    result = add_constraint(0,28,38,0,0);
    result = add_constraint(0,7,26,0,0);
    result = add_constraint(0,28,29,0,0);
    result = add_constraint(0,7,28,0,0);
    result = add_constraint(0,28,28,0,0);
    result = add_constraint(0,7,30,0,0);
    result = add_constraint(0,28,27,0,0);
    result = add_constraint(0,7,32,0,0);
    result = add_constraint(0,28,24,0,0);
    result = add_constraint(0,7,34,0,0);
    result = add_constraint(0,28,21,0,0);
    result = add_constraint(0,7,36,0,0);
    result = add_constraint(0,28,19,0,0);
    result = add_constraint(0,7,38,0,0);
    result = add_constraint(0,28,14,0,0);
    result = add_constraint(0,7,40,0,0);
    result = add_constraint(0,28,4,0,0);
    result = add_constraint(0,7,42,0,0);
    result = add_constraint(0,28,1,0,0);
    result = add_constraint(0,7,44,0,0);

    // loops_per = constraint_deal(0,num);
#ifdef IOS_ONLY
    clock_t start = clock(), diff;
#endif
    
    
    answer = constraint_command(0,ID_DEAL,0,0,num);
#ifdef IOS_ONLY
    diff = clock() - start;

    unsigned long msec = diff * 1000 / CLOCKS_PER_SEC;
#endif
    // fprintf(stderr,"Number of hands:%d\n",num);
    // fprintf(stderr,"Time taken %lu seconds %lu milliseconds\n", msec/1000, msec%1000);

}

void test_card9(void) {
    // char *hand;
    int result,num=25;
    // unsigned int loops_per;
    char *answer;



result=add_constraint(2,1,3,0,3);
result=add_constraint(2,16,17,3,1);
result=add_constraint(2,4,0,0,1);
result=add_constraint(2,1,2,0,3);
result=add_constraint(2,16,17,2,1);
result=add_constraint(2,4,0,3,4);
result=add_constraint(2,1,1,4,9);
result=add_constraint(2,16,19,0,1);
result=add_constraint(2,3,0,6,7);
result=add_constraint(0,1,1,0,2);
result=add_constraint(0,1,0,4,5);
result=add_constraint(0,1,2,4,5);
result=add_constraint(0,3,0,9,11);
result=add_constraint(0,14,0,7,10);
result=add_constraint(2,14,0,13,15);
result=add_constraint(0,3,0,13,14);


result=add_constraint(1,28,35,0,0);
result=add_constraint(0,7,16,0,0);
result=add_constraint(1,28,32,0,0);
result=add_constraint(0,7,18,0,0);
result=add_constraint(1,28,31,0,0);
result=add_constraint(0,7,20,0,0);
result=add_constraint(1,28,11,0,0);
result=add_constraint(0,7,22,0,0);
result=add_constraint(1,28,7,0,0);
result=add_constraint(0,7,24,0,0);
result=add_constraint(1,28,5,0,0);
result=add_constraint(0,7,26,0,0);
result=add_constraint(1,28,3,0,0);
result=add_constraint(0,7,28,0,0);
result=add_constraint(1,28,2,0,0);
result=add_constraint(0,7,30,0,0);
result=add_constraint(1,28,0,0,0);
result=add_constraint(0,7,32,0,0);
result=add_constraint(1,28,48,0,0);
result=add_constraint(0,7,34,0,0);
result=add_constraint(1,28,46,0,0);
result=add_constraint(0,7,36,0,0);


result=add_constraint(3,28,51,0,0);
result=add_constraint(0,7,38,0,0);
result=add_constraint(3,28,49,0,0);
result=add_constraint(0,7,40,0,0);
result=add_constraint(3,28,45,0,0);
result=add_constraint(0,7,42,0,0);
result=add_constraint(3,28,44,0,0);
result=add_constraint(0,7,44,0,0);
result=add_constraint(3,28,39,0,0);
result=add_constraint(0,7,46,0,0);
result=add_constraint(3,28,34,0,0);
result=add_constraint(0,7,48,0,0);
result=add_constraint(3,28,33,0,0);
result=add_constraint(0,7,50,0,0);
result=add_constraint(3,28,26,0,0);
result=add_constraint(0,7,52,0,0);
result=add_constraint(3,28,12,0,0);
result=add_constraint(0,7,54,0,0);
result=add_constraint(3,28,10,0,0);
result=add_constraint(0,7,56,0,0);
result=add_constraint(3,28,6,0,0);
result=add_constraint(0,7,58,0,0);


result=add_constraint(0,37,14,0,0);
result=add_constraint(0,7,60,0,0);

result=add_constraint(1,37,16,0,0);
result=add_constraint(0,7,62,0,0);
result=add_constraint(2,37,17,0,0);
result=add_constraint(0,7,64,0,0);
result=add_constraint(3,37,13,0,0);
result=add_constraint(0,7,66,0,0);
result=add_constraint(2,37,25,0,0);
result=add_constraint(0,7,68,0,0);
result=add_constraint(3,37,15,0,0);
result=add_constraint(0,7,70,0,0);
    
result=add_constraint(0,37,19,0,0);
result=add_constraint(0,7,72,0,0);
    
result=add_constraint(1,37,43,0,0);
result=add_constraint(0,7,74,0,0);
#define QUIET9
#ifndef QUIET9
    // loops_per = constraint_deal(0,num);
    clock_t start = clock(), diff;
#endif
    
    answer = constraint_command(0,ID_DEAL,0,0,num);
#ifndef QUIET9
    diff = clock() - start;

    unsigned long msec = diff * 1000 / CLOCKS_PER_SEC;
    fprintf(stderr,"9all:Number of hands:%d\n",num);
    fprintf(stderr,"Time taken %lu seconds %lu milliseconds\n", msec/1000, msec%1000);
    fprintf(stderr,"%s\n",answer);
#endif
#ifdef IOS_ONLY
    answer = constraint_command(0,ID_VERSION,0,0,0);
#endif

#ifndef QUIET9
    fprintf(stderr,"%s\n",answer);
#endif


}


void test_card11(void) {
    // char *hand;
    int result,num=25;
    // unsigned int loops_per;
    char *answer;

    

result=add_constraint(2,1,3,0,3);
result=add_constraint(2,16,17,3,1);
result=add_constraint(2,4,0,0,1);
result=add_constraint(2,1,2,0,3);
result=add_constraint(2,16,17,2,1);
result=add_constraint(2,4,0,3,4);
result=add_constraint(2,1,1,4,9);
result=add_constraint(2,16,19,0,1);
result=add_constraint(2,3,0,6,7);
result=add_constraint(0,1,1,0,2);
result=add_constraint(0,1,0,4,5);
result=add_constraint(0,1,2,4,5);
result=add_constraint(0,3,0,9,11);
result=add_constraint(0,14,0,7,10);
result=add_constraint(2,14,0,13,15);
result=add_constraint(0,3,0,13,14);


result=add_constraint(1,28,35,0,0);
result=add_constraint(0,7,16,0,0);
result=add_constraint(1,28,32,0,0);
result=add_constraint(0,7,18,0,0);
result=add_constraint(1,28,31,0,0);
result=add_constraint(0,7,20,0,0);
result=add_constraint(1,28,11,0,0);
result=add_constraint(0,7,22,0,0);
result=add_constraint(1,28,7,0,0);
result=add_constraint(0,7,24,0,0);
result=add_constraint(1,28,5,0,0);
result=add_constraint(0,7,26,0,0);
result=add_constraint(1,28,3,0,0);
result=add_constraint(0,7,28,0,0);
result=add_constraint(1,28,2,0,0);
result=add_constraint(0,7,30,0,0);
result=add_constraint(1,28,0,0,0);
result=add_constraint(0,7,32,0,0);
result=add_constraint(1,28,48,0,0);
result=add_constraint(0,7,34,0,0);


result=add_constraint(3,28,51,0,0);
result=add_constraint(0,7,36,0,0);
result=add_constraint(3,28,49,0,0);
result=add_constraint(0,7,38,0,0);
result=add_constraint(3,28,45,0,0);
result=add_constraint(0,7,40,0,0);
result=add_constraint(3,28,44,0,0);
result=add_constraint(0,7,42,0,0);
result=add_constraint(3,28,39,0,0);
result=add_constraint(0,7,44,0,0);
result=add_constraint(3,28,34,0,0);
result=add_constraint(0,7,46,0,0);
result=add_constraint(3,28,33,0,0);
result=add_constraint(0,7,48,0,0);
result=add_constraint(3,28,26,0,0);
result=add_constraint(0,7,50,0,0);
result=add_constraint(3,28,12,0,0);
result=add_constraint(0,7,52,0,0);
result=add_constraint(3,28,10,0,0);
result=add_constraint(0,7,54,0,0);
result=add_constraint(3,28,6,0,0);
result=add_constraint(0,7,56,0,0);


result=add_constraint(0,37,14,0,0);
result=add_constraint(0,7,58,0,0);
result=add_constraint(1,37,16,0,0);
result=add_constraint(0,7,60,0,0);
result=add_constraint(2,37,17,0,0);
result=add_constraint(0,7,62,0,0);
result=add_constraint(3,37,13,0,0);
result=add_constraint(0,7,64,0,0);
result=add_constraint(2,37,25,0,0);
result=add_constraint(0,7,66,0,0);
result=add_constraint(3,37,15,0,0);
result=add_constraint(0,7,68,0,0);
result=add_constraint(0,37,19,0,0);
result=add_constraint(0,7,70,0,0);
result=add_constraint(1,37,43,0,0);
result=add_constraint(0,7,72,0,0);
result=add_constraint(1,37,46,0,0);
result=add_constraint(0,7,74,0,0);
result=add_constraint(2,37,50,0,0);
result=add_constraint(0,7,76,0,0);

#define QUIET11

    // loops_per = constraint_deal(0,num);
#ifndef QUIET11
    clock_t start = clock(), diff;
#endif
    
    answer = constraint_command(0,ID_DEAL,0,0,num);
#ifndef QUIET11
    diff = clock() - start;

    unsigned long msec = diff * 1000 / CLOCKS_PER_SEC;
    fprintf(stderr,"11:Number of hands:%d\n",num);
    fprintf(stderr,"Time taken %lu seconds %lu milliseconds\n", msec/1000, msec%1000);
#endif
}

extern int already_played[52];

void test_card12(void) {
    char *hand;
    int result,num=25;
    // unsigned int loops_per;
    char *answer;
    int i,j;

    
result=add_constraint(2,1,3,0,3);
result=add_constraint(2,16,17,3,1);
result=add_constraint(2,4,0,0,1);
result=add_constraint(2,1,2,0,3);
result=add_constraint(2,16,17,2,1);
result=add_constraint(2,4,0,3,4);
result=add_constraint(2,1,1,4,9);
result=add_constraint(2,16,19,0,1);
result=add_constraint(2,3,0,6,7);
result=add_constraint(3,41,1,0,0);
result=add_constraint(3,1,3,5,8);
result=add_constraint(3,12,3,1,7);
result=add_constraint(3,3,0,9,11);
// 0 1 1 0 2
// 0 1 0 4 5
// 0 1 2 4 5
// 0 3 0 13 15
// 1 1 3 3 5
// 1 26 3 0 40
// 1 3 0 17 18
// 0 14 0 7 10
// 1 14 0 7 10
// 2 14 0 13 15
//3 14 0 14 16
result=add_constraint(2,14,0,13,14); // changed
result=add_constraint(3,14,0,14,15); // changed
result=add_constraint(0,3,0,13,14); // changed

//    fprintf(stderr,"adding north and east hands\n");

result=add_constraint(1,28,35,0,0);
result=add_constraint(0,7,16,0,0);
result=add_constraint(1,28,32,0,0);
result=add_constraint(0,7,18,0,0);
result=add_constraint(1,28,31,0,0);
result=add_constraint(0,7,20,0,0);
result=add_constraint(1,28,11,0,0);
result=add_constraint(0,7,22,0,0);
result=add_constraint(1,28,7,0,0);
result=add_constraint(0,7,24,0,0);
result=add_constraint(1,28,5,0,0);
result=add_constraint(0,7,26,0,0);
result=add_constraint(1,28,3,0,0);
result=add_constraint(0,7,28,0,0);
result=add_constraint(1,28,2,0,0);
result=add_constraint(0,7,30,0,0);
result=add_constraint(1,28,0,0,0);
result=add_constraint(0,7,32,0,0);
result=add_constraint(1,28,48,0,0);
result=add_constraint(0,7,34,0,0);

/*    result=add_constraint(1,37,16,0,0);
    result=add_constraint(0,7,36,0,0);
    result=add_constraint(1,37,43,0,0);
    result=add_constraint(0,7,38,0,0);
    result=add_constraint(1,37,46,0,0);
    result=add_constraint(0,7,40,0,0); */



result=add_constraint(0,28,47,0,0);
result=add_constraint(0,7,36,0,0);
result=add_constraint(0,28,42,0,0);
result=add_constraint(0,7,38,0,0);
result=add_constraint(0,28,40,0,0);
result=add_constraint(0,7,40,0,0);
result=add_constraint(0,28,38,0,0);
result=add_constraint(0,7,42,0,0);
result=add_constraint(0,28,29,0,0);
result=add_constraint(0,7,44,0,0);
result=add_constraint(0,28,28,0,0);
result=add_constraint(0,7,46,0,0);
result=add_constraint(0,28,27,0,0);
result=add_constraint(0,7,48,0,0);
result=add_constraint(0,28,24,0,0);
result=add_constraint(0,7,50,0,0);
result=add_constraint(0,28,21,0,0);
result=add_constraint(0,7,52,0,0);
result=add_constraint(0,28,4,0,0);
result=add_constraint(0,7,54,0,0);
result=add_constraint(0,28,1,0,0);
result=add_constraint(0,7,56,0,0);
 

 
result=add_constraint(0,37,14,0,0);
result=add_constraint(0,7,58,0,0);
 

result=add_constraint(1,37,16,0,0);
result=add_constraint(0,7,60,0,0);
result=add_constraint(2,37,17,0,0);
result=add_constraint(0,7,62,0,0);
result=add_constraint(3,37,13,0,0);
result=add_constraint(0,7,64,0,0);
result=add_constraint(2,37,25,0,0);
result=add_constraint(0,7,66,0,0);
result=add_constraint(3,37,15,0,0);
result=add_constraint(0,7,68,0,0);
result=add_constraint(0,37,19,0,0);
result=add_constraint(0,7,70,0,0);
result=add_constraint(1,37,43,0,0);
result=add_constraint(0,7,72,0,0);
result=add_constraint(1,37,46,0,0);
result=add_constraint(0,7,74,0,0);
result=add_constraint(2,37,50,0,0);
result=add_constraint(0,7,76,0,0);
result=add_constraint(3,37,51,0,0);
result=add_constraint(0,7,78,0,0);


    // loops_per = constraint_deal(0,num);
#ifdef IOS_ONLY
clock_t start = clock(), diff;
#endif

    answer = constraint_command(0,ID_DEAL,0,0,num);
#ifdef IOS_ONLY
	diff = clock() - start;
#endif
#define QUIET12
    
#ifndef QUIET12
    fprintf(stderr,"answer: %s\n",answer);
#endif

    for (j=0;j<num;j++) {
        for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];
        hand = constraint_command(0,ID_GET_HAND,j,0,0);
#ifndef QUIET12
        fprintf(stderr,"%s\n",hand);
#endif
        
    }
    
#ifndef QUIET12
    for (j=51; j>=0; j--) {
        fprintf(stderr,"%d",already_played[j]);
        if (j%13==0) fprintf(stderr," ");
        }
    fprintf(stderr,"\n");
#endif
    for (j=0;j<num;j++) {
        for (i=0;i<52;i++) gen32_deck[i] = decks[j][i];

            hand = constraint_command(0,ID_GET_PLAYING_HAND,j,0,0);
#ifndef QUIET12
        fprintf(stderr,"%s\n",hand);
#endif
        
    }

#ifndef QUIET12
    unsigned long msec = diff * 1000 / CLOCKS_PER_SEC;
    fprintf(stderr,"12:Number of hands:%d\n",num);
    fprintf(stderr,"Time taken %lu seconds %lu milliseconds\n", msec/1000, msec%1000);
#endif

}

#ifdef BEST_HOFFER
// #pragma message("BEST_HOFFER setting up exchanges")
void gen32_set_loops(int loops);
void gen32_set_individual_exchanges(int,int,int,int);
#else
void gen32_set_exchanges(int loops, int module,int delta,int threshhold);
#endif

#ifdef USE_MAIN
// not needed when using generate_test_runner.rb
int main( int argc, char *argv[] ) {
#else
char unity_result[100];
// #pragma message("compiling do_all_unity_tests")
extern char *constraint_command(int,int,int,int,int);
extern char *do_all_unity_tests(void) {
#endif
	char msg[100];
    UNITY_BEGIN();
#ifdef USE_GETARGS2
    for (i=0; i<argc; i++)
		if (argv[i][0]=='-' && toupper(argv[i][1]) == 'S')
			getargs2((const char *) argv[i]);
#endif    
#ifndef NO_CSW
// #pragma message("use_cswitch is on")
  //  set_trigger("2,20");
  //	TRIGGER(1,fprintf(stderr,"main()\n"));
 //	TRIGGER(1,print_triggers());
#else
#pragma message("use_cswitch is off")
#endif
#ifdef BEST_HOFFER
// #pragma message("BEST_HOFFER")
	gen32_set_loops(10000);
	gen32_set_individual_exchanges(0,400,1,1);
	gen32_set_individual_exchanges(1,400,1,1);
	gen32_set_individual_exchanges(2,400,1,1);
	gen32_set_individual_exchanges(3,400,1,1);
#else
    gen32_set_exchanges(10000,4000,1,0);
#endif
	RUN_TEST(test_add_constraint);
	RUN_TEST(test_constraint_deal);
	RUN_TEST(test_has);
	RUN_TEST(test_quality);
	sprintf(msg,"output under :G may be too strong");
	TEST_MESSAGE(msg);
	RUN_TEST(test_intermediates);
	RUN_TEST(test_shortness);
	RUN_TEST(test_losers);
	RUN_TEST(test_aces);
	RUN_TEST(test_more_or_less);
	RUN_TEST(test_equal);
	RUN_TEST(test_stoppers); // uses ID_NOT 
	RUN_TEST(test_roth);
	RUN_TEST(test_xor);
	RUN_TEST(test_or); // uses more than 2 constraints
	RUN_TEST(test_and); // uses more than 2 constraints
	RUN_TEST(test_quicks);
    RUN_TEST(test_get_hand);
    RUN_TEST(test_get_playing_hand);
    RUN_TEST(test_speed);
    RUN_TEST(test_card9);
    RUN_TEST(test_card11);
    RUN_TEST(test_card12);

    UNITY_END();
    strcpy(unity_result,"Unity testing completed");
    return unity_result;
}
#endif
#endif
#endif // linux for full file
