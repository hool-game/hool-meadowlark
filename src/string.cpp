

// Fig. 11.10: String.cpp
// String class member-function and friend-function definitions.
// generated from string.m4 then string.exp yielding string.cpp Version 1.02.

#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

#include <iomanip>
using std::setw;

#include <cstring> // strcpy and strcat prototypes
using std::strcmp;
using std::strcpy;
using std::strcat;

#include <cstdlib> // exit prototype
using std::exit;
#include "hoffer.h"
#if !defined(linux)
#include "../../../bid20.h"
#else
#include "../include/bid20.h"
#endif
#if !defined(linux)
#include "../../../bidlibc_new/mystring.h"
#else
#include "../include/mystring.h" // String class definition
#endif
#include "../include/non_exportables.h"
// #include "assert.h"
#include "../include/bergen.hpp"
#include "../include/constraint_ids.h"


#if !defined(IOS)
#include "../include/prefix.pch"
#endif

extern bool explicit_flag;
#ifdef WIN_ONLY
void sort(int *,int,int,int);
#else
extern "C" void sort(int[52],int,int,int);
#endif

#undef ALOGGING
#if defined(IOS)
#undef ALOGGING
#endif

#if defined(ALOGGING)
#define ANDROID_LOGGING_FOR_TRICKY
#ifdef ANDROID_LOGGING_FOR_TRICKY
#include <android/log.h>
#define  LOG_TAG    "testjni"
#define  ALOG(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#endif
#endif


extern int gen32_evaluation_system;
void testString(void) {
    CSW_EXEC(fprintf(stderr,"this is testing csw\n"));
	String simpleText1("");
	String simpleText2("a test string");
	std::cout << simpleText1 << " " << simpleText2;
/*	simpleText1 = simpleText2;
	std::cout << "operator copy worked" << simpleText1;
	simpleText2.erase();
	simpleText2 = simpleText1;
	std::cout << "erase and copy worked" << simpleText2;
*/
}



// conversion (and default) constructor converts char * to String
String::String( const char *s ) 
   : length( ( s != 0 ) ? (int) strlen( s ) : 0 )
{
   // cout << "Conversion (and default) constructor: " << s << endl;
	// CSW_EXEC(do_log(1,20,(char *)s));
   setString( s ); // call utility function
} // end String conversion constructor

// copy constructor
String::String( const String &copy ) 
   : length( copy.length )
{
   // cout << "Copy constructor: " << copy.sPtr << endl;
   setString( copy.sPtr ); // call utility function
} // end String copy constructor

// Destructor
String::~String()
{
   // cout << "Destructor: " << sPtr << endl;
   delete [] sPtr; // release pointer-based string memory
} // end ~String destructor

// overloaded = operator; avoids self assignment
const String &String::operator=( const String &right )
{
   // cout << "operator= called" << endl;

   if ( &right != this ) // avoid self assignment
   {         
      delete [] sPtr; // prevents memory leak
      length = right.length; // new String length
      setString( right.sPtr ); // call utility function
   } // end if
   else
      cout << "Attempted assignment of a String to itself" << endl;

   return *this; // enables cascaded assignments
} // end function operator=

// concatenate right operand to this object and store in this object
const String &String::operator+=( const String &right )
{
   size_t newLength = length + right.length; // new length
   char *tempPtr = new char[ newLength + 1 ]; // create memory

   strcpy( tempPtr, sPtr ); // copy sPtr
   strcpy( tempPtr + length, right.sPtr ); // copy right.sPtr

   delete [] sPtr; // reclaim old space
   sPtr = tempPtr; // assign new array to sPtr
   length = (int) newLength; // assign new length to length
   return *this; // enables cascaded calls
} // end function operator+=

// get c_str
char *String::c_str() {
	return sPtr;
}

// is this String empty?

bool String::empty() {
	return length == 0;
	}
bool String::operator!() const
{ 
   return length == 0; 
} // end function operator! 

// Is this String equal to right String?
bool String::operator==( const String &right ) const
{ 
   return strcmp( sPtr, right.sPtr ) == 0; 
} // end function operator==

// Is this String less than right String?
bool String::operator<( const String &right ) const
{ 
   return strcmp( sPtr, right.sPtr ) < 0; 
} // end function operator<

// return reference to character in String as a modifiable lvalue
char &String::operator[]( int subscript )
{
   // test for subscript out of range
   if ( subscript < 0 || subscript >= length )
   {
      cerr << "Error: Subscript " << subscript 
         << " out of range" << endl;
      exit( 1 ); // terminate program
   } // end if

   return sPtr[ subscript ]; // non-const return; modifiable lvalue
} // end function operator[]

// return reference to character in String as rvalue
char String::operator[]( int subscript ) const
{
   // test for subscript out of range
   if ( subscript < 0 || subscript >= length )
   {
      cerr << "Error: Subscript " << subscript 
           << " out of range" << endl;
      exit( 1 ); // terminate program
   } // end if

   return sPtr[ subscript ]; // returns copy of this element
} // end function operator[]

// return a substring beginning at index and of length subLength
String String::operator()( int index, int subLength ) const
{
   // if index is out of range or substring length < 0, 
   // return an empty String object
   if ( index < 0 || index >= length || subLength < 0 )  
    return (char *) "";

   // determine length of substring
   int len;

   if ( ( subLength == 0 ) || ( index + subLength > length ) )
      len = length - index;
   else
      len = subLength;

   // allocate temporary array for substring and 
   // terminating null character
   char *tempPtr = new char[ len + 1 ];

   // copy substring into char array and terminate string
   strncpy( tempPtr, &sPtr[ index ], len );
   tempPtr[ len ] = '\0';

   // create temporary String object containing the substring
   String tempString( tempPtr );
   delete [] tempPtr; // delete temporary array
   return tempString; // return copy of the temporary String
} // end function operator()

// return string length
int String::getLength() const 
{ 
   return length; 
} // end function getLength

// utility function called by constructors and operator=
void String::setString( const char *string2 )
{
   sPtr = new char[ length + 1 ]; // allocate memory
   if (!sPtr) CSW_EXEC(do_log(1,40,"setString failed"));
   // CSW_EXEC(do_log(1,20,(char *)string2));
   // CSW_EXEC(do_log(1,20,c_str()));

   if ( string2 != 0 ) // if string2 is not null pointer, copy contents
      strcpy( sPtr, string2 ); // copy literal to object
   else // if string2 is a null pointer, make this an empty string
      sPtr[ 0 ] = '\0'; // empty string
} // end function setString 

// overloaded output operator
ostream &operator<<( ostream &output, const String &s )
{
   output << s.sPtr;
   return output; // enables cascading
} // end function operator<<

// overloaded input operator
istream &operator>>( istream &input, String &s )
{
   char temp[ 100 ]; // buffer to store input
   input >> setw( 100 ) >> temp;
   s = temp; // use String class assignment operator
   return input; // enables cascading
} // end function operator>>

/**************************************************************************
 * (C) Copyright 1992-2008 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 **************************************************************************/

Bergen global_b;

EXTERN_HOFFER_C char *c_process(char *line) {

	return global_b.c_process(line);
}

EXTERN_HOFFER_C int get_actual_line_no(void) {
	return global_b.get_line_no();
	}

void get_constraints_by_sequence(char *sequence, int vul, int dealer, int bidder) {

	// std::cout << "-> get_constraints_by_sequence" << std::endl;

	global_b.get_constraints_by_line(sequence,vul,dealer,bidder,-1);
	// std::cout << "<- get_constraints_by_sequence" << std::endl;
	}

#if defined(ANDROID)
EXTERN_HOFFER_C void bug_get_constraints_by_sequence(char *sequence, int vul, int dealer, int bidder) {

    get_constraints_by_sequence(sequence,vul,dealer,bidder);
    }
#endif

#if defined(OSX) || defined(WIN32)
EXTERN_HOFFER_C int cpp_get_constraint_count(void) {
#else
int cpp_get_constraint_count(void) {
#endif
    return global_b.constraint_parser->get_number_of_constraints();
}
    
#if defined(OSX) || defined(WIN32) || defined(ANDROID)
EXTERN_HOFFER_C int my_get_constraint_item(int i, int j) {
#else
int my_get_constraint_item(int i, int j) {
#endif
    return global_b.constraint_parser->get_constraint(i,j);
    }

    
#if defined(OSX) || defined(WIN32)
EXTERN_HOFFER_C int my_set_bid_info(void) {
#else
int my_set_bid_info(void) {
#endif
    global_b.bid_info->set_all_bid_info();
    return 1;
}
    
#if defined(OSX) || defined(WIN32)
EXTERN_HOFFER_C int my_get_bid_info(int dir, int suit) {
#else
int my_get_bid_info(int dir, int suit) {
#endif
    return global_b.bid_info->get_bid_info(dir,suit);
}
    
    // check that my_get_bid_info works for leads
    // check that overide works for constraint correction
    // check that the hoffer get_description2 works correctly
    // check that o in the constraints sets up the next constraint for correction
    // check that ID_PLACING_CARDS is used and is 58
    // check that ID_OVERRIDE is 59
    // check that f_push is OK when LEADs were put to the end and depth > print_stack_top is checked
    // check that 58 is sent just before send_hand, just to be sure
    // check that leads is at the end of sys.cpp
    // check each item in xdeal.c again
    // check that the constraint_parsers are initialized correctly
    // then a new library can be sent
    // figure out how to get fossil back up and running
    //



#if defined(OSX) || defined(WIN32)
EXTERN_HOFFER_C int my_get_constraint(int row, int column) {
#else
int my_get_constraint(int row, int column) {
#endif
    return global_b.constraint_parser->get_constraint(row,column);
}


EXTERN_HOFFER_C void set_template(int index,const char *value);
EXTERN_HOFFER_C char *get_template(int);

class hoffer_class {
public:
	int get_comments_loaded(void);
	void set_comments_loaded(void);
	int get_rules_loaded(void);
    void set_rules_not_loaded(void);
	void set_rules_loaded(void);
	int get_convents_loaded(void);
	void set_convents_loaded(void);
	int get_macros_loaded(void);
	void set_macros_loaded(void);
	int get_tables_loaded(void);
	void set_tables_loaded(void);
	hoffer_class();
private:
	int comments_loaded;
	int rules_loaded;
	int convents_loaded;
	int macros_loaded;
	int tables_loaded;
};
hoffer_class::hoffer_class(void) { comments_loaded=0; rules_loaded=0;
    macros_loaded = 0; tables_loaded = 0;
}
int hoffer_class::get_comments_loaded(void) { return comments_loaded; }
void hoffer_class::set_comments_loaded(void) { comments_loaded=1; }
int hoffer_class::get_convents_loaded(void) { return convents_loaded; }
void hoffer_class::set_convents_loaded(void) { convents_loaded=1; }
int hoffer_class::get_rules_loaded(void) { return rules_loaded;}
void hoffer_class::set_rules_loaded(void) {rules_loaded=1; }
void hoffer_class::set_rules_not_loaded(void) { rules_loaded=0; }
int hoffer_class::get_tables_loaded(void) { return tables_loaded;}
void hoffer_class::set_tables_loaded(void) {tables_loaded=1; }
int hoffer_class::get_macros_loaded(void) { return macros_loaded;}
void hoffer_class::set_macros_loaded(void) {macros_loaded=1; }
hoffer_class hoffer;
void load_comments(void);
void load_convents(void);
void load_macros(void);
void do_update_macros(void);
void set_explicit_flag(int);
#define EXCLUDED_HOFFER_MATERIAL
#ifndef EXCLUDED_HOFFER_MATERIAL
EXTERN_HOFFER_C int read_rules(void);
EXTERN_HOFFER_C void load_macros(void);
EXTERN_HOFFER_C void load_rules(void);
EXTERN_HOFFER_C void load_comments(void);
EXTERN_HOFFER_C void load_convents(void);
EXTERN_HOFFER_C void set_explicit_flag(int);
EXTERN_HOFFER_C int test_amzi(void);
EXTERN_HOFFER_C int reload_amzi(void);
EXTERN_HOFFER_C int retract_who_am_i_amzi(void);
EXTERN_HOFFER_C int retractall_amzi(void);
// EXTERN_HOFFER_C int retract_constraints_amzi(void);
EXTERN_HOFFER_C int retract_sequence_amzi(void);
EXTERN_HOFFER_C intt assert_amzi(const char *);
EXTERN_HOFFER_C void show_coverage(void);
EXTERN_HOFFER_C int bid_amzi(void);
EXTERN_HOFFER_C char *get_amzi_comment(void);
EXTERN_HOFFER_C int close_amzi(void);
EXTERN_HOFFER_C void initialize_matchtree(void);
EXTERN_HOFFER_C void setup_macros(int min,int oppmin,int tntmin,int hioppmin,int dir);
EXTERN_HOFFER_C int macro_verify(void);
EXTERN_HOFFER_C void clearbids(void);
EXTERN HOFFER_C void set_dealer(int dealer);

EXTERN_HOFFER_C int c_check(const char *name, int dir);
EXTERN_HOFFER_C void set_evaluation_system(int i);
extern int gen32_evaluation_system;
EXTERN_HOFFER_C void set_table(int t);
EXTERN_HOFFER_C void c_lock(int table, int dir);
extern void domacro(const char *cmd, int section);
EXTERN_HOFFER_C void my_evaluate_sequence(const char *seq);
EXTERN_HOFFER_C char *get_best_comment(int dir);
EXTERN_HOFFER_C void set_deck(int card, int value);
EXTERN_HOFFER_C void set_vul(int dir, int value);
EXTERN_HOFFER_C void set_description(int i,const char *desc);
EXTERN_HOFFER_C void set_seq(const char *seq);
EXTERN_HOFFER_C int mybid(int);
EXTERN_HOFFER_C char *get_seq(void);
EXTERN_HOFFER_C char *get_description2(int i, int v);
#ifdef AMZI
EXTERN_HOFFER_C int close_amzi(void);
#endif
EXTERN_HOFFER_C void clear_clues(void);
#endif
int set_hand(const char *,int);


char bidbuf[250];
char the_bid[3];
void hoffer_setup(void);
#undef TESTING
#define HOFFER_BID_ID 1605

char returnbuf[250];
char *my_return(const char *result) {
    strcpy(returnbuf,result);
    TRIGGER(HOFFER_BID_ID,do_log(1,30,"my_return:%s",result));
    return returnbuf;
    }

EXTERN_HOFFER_C int f_eval(const char *);
// char myftemp[250];
char f_command[20];

#if defined(OSX) || defined(WIN32)
// #pragma message("Compiling for OSX or windows")
EXTERN_HOFFER_C char *hoffer_set(int item, int i_value, const char *s_value) {
#else
// #pragma message("Compiling hoffer_set for IOS")
char *hoffer_set(int item, int i_value, const char *s_value) {
#endif
    int hand[52],i;
    // fprintf(stderr,"item:%d, integer value:%d, string value:%s\n",item,i_value,s_value);
    if (item == 2) return my_return("v7.20");
    if (item==1 && i_value > 0 && i_value < 8) {
        set_evaluation_system(i_value);
        switch (i_value) {
            case 1: return my_return("eval is BERGEN"); break;
            case 2: return my_return("eval is ROTH"); break;
            case 3: return my_return("eval is KLINGER"); break;
            case 4: return my_return("eval is BERGEN_HCPS"); break;
            case 5: return my_return("eval is KLINGER_HCPS"); break;
            case 6: return my_return("eval is KAPLAN_RUEBEN"); break;
            case 7: return my_return("eval is HOFFER_COUNT"); break;
            }
        }
    for (i=0; i<52;i++) hand[i]=51-i;
    sort(hand,0,52,1);
    
    
        return my_return("OK");
    }

#ifdef WIN_ONLY
char *constraint_command(int,int,int,int,int);
#else
extern "C" char *constraint_command(int,int,int,int,int);
#endif

char *my_constraint_command(int who, int what, int where, int low, int high) {
        return constraint_command(who,what,where,low,high);
}
    
    
extern "C" void clear_constraints(int);
void do_clear_constraints(int start) {
    clear_constraints(start);
}
    
    

    

#ifdef WIN_ONLY
void csw_print(const char *msg);
#else
extern "C" void csw_print(const char *msg);
#endif

void cpp_csw_print(const char *msg) { csw_print(msg); }
    
extern int print_stack_top;
// extern "C" char *hoffer_constraints(const char *sequence, int vul, int dealer, int range);
extern void test_castor(void);
int castor_bid(const char *hand, const char *sequence,int vul, int dealer, int bidder);

#ifdef IOS
#define USE_HOFFER_BID_ALL
#endif
    
#ifdef ANDROID
#define USE_HOFFER_BID_ALL
#endif

char all_sequence[1000];
char *hoffer_evaluate(const char *,int,int);

#ifdef USE_HOFFER_BID_ALL
#if defined(ANDROID)
char *hoffer_bid_all(const char *west,const char *north,const char *east,const char *south,const char *s_dealer,const char *s_vul) {
#else
#if defined(OSX) || defined(WIN32)
#pragma message("Compiling hoffer_bid_all for OSX or windows")
EXTERN_HOFFER_C char *hoffer_bid_all(const char *hand,const char *sequence,int vul, int dealer) {
#else
// #pragma message("Compiling hoffer_bid_all for IOS")
char *hoffer_bid_all(const char *west,const char *north,const char *east,const char *south,const char *s_dealer,const char *s_vul) {
#if defined(CSW)
	TRIGGER(ID_BID_ALL, do_log(1, 30, "hoffer_bid_all w:%s n:%s e:%s s:%s\n", west, north, east, south));
#endif
#endif
#endif
    int i,bidder,dir,ew_vul=0,ns_vul=0,length,hoffer_result,passes=0,dealer=0,vul=0;
    all_sequence[0]='\0';
    // test_castor();
    hoffer_setup();
#if defined(ALOGGING)
    ALOG("hoffer_setup completed at hoffer_bid_all");
#endif
    // fprintf(stderr,"hoffer_setup completed\n");
    // fprintf(stderr,"N:%s\nE:%s\nS:%s\nW:%s\n",northm,east,south,west);
    if (strcmp(s_dealer,"N")==0) { bidder=0; dealer=0; }
    else if (strcmp(s_dealer,"E")==0) { bidder=1; dealer=1; }
    else if (strcmp(s_dealer,"S")==0) { bidder=2; dealer=2; }
    else if (strcmp(s_dealer,"W")==0) { bidder=3; dealer=3; }
    else return my_return("bad dealer");
    if (strcmp(s_vul,"NONE")==0) vul=0;
    else if (strcmp(s_vul,"None")==0) vul=0;
    else if (strcmp(s_vul,"Z")==0) vul=0;
    else if (strcmp(s_vul,"NS")==0) vul=1;
    else if (strcmp(s_vul,"N")==0) vul=1;
    else if (strcmp(s_vul,"EW")==0) vul=2;
    else if (strcmp(s_vul,"E")==0) vul=2;
    else if (strcmp(s_vul,"BOTH")==0) vul=3;
    else if (strcmp(s_vul,"B")==0) vul=3;
    else if (strcmp(s_vul,"All")==0) vul=3;
    else return my_return("bad vul");
    if (set_hand(north,0)<0) return my_return("bad hand");
    if (set_hand(east,1)<0) return my_return("bad hand");
    if (set_hand(south,2)<0) return my_return("bad hand");
    if (set_hand(west,3)<0) return my_return("bad hand");
    dir = bidder % 2;

    if (vul <0 || vul > 3) return my_return("bad vul");
    if (dir <0 || dir > 3) return my_return("bad dir");
    if (vul==0) { set_vul(0,0); set_vul(1,0); }
    else if (vul==1) { set_vul(1,1); if (dir==1) {ns_vul=1;}}
    else if (vul==2) { set_vul(0,1); if (dir==2) {ew_vul=1;}}
    else if (vul==3) { set_vul(0,1); set_vul(1,1); ew_vul=1; ns_vul=1; }
    dir = bidder % 2;
    set_evaluation_system(gen32_evaluation_system);
    TRIGGER(ID_BID_ALL,do_log(1,10,"hoffer_bid_all:calling get_description2"));
    sprintf(bidbuf,"%s",get_description2(0,ns_vul));
    set_description(0,bidbuf);
    sprintf(bidbuf,"%s",get_description2(1,ew_vul));
    set_description(1,bidbuf);
    sprintf(bidbuf,"%s",get_description2(2,ns_vul));
    set_description(2,bidbuf);
    sprintf(bidbuf,"%s",get_description2(3,ew_vul));
    set_description(3,bidbuf);
    set_dealer(dealer);
    set_seq(all_sequence);
    clearbids();
    while (1) {
        if (passes == 3 && strcmp(all_sequence,"P:P:P") != 0) break;
        else if (passes==4) break;
        clear_clues();
        // fprintf(stderr,"sequence is <%s> passes are <%i>\n",get_seq(),passes);
        //fprintf(stderr,"in hoffer bid with hand:%s and sequence:%s\n",hand,sequence);
        // fprintf(stderr,"hbpst:%d\n",print_stack_top);
        bidder = get_bidder(); // determined by sequence and dealer
        dir = bidder % 2;

        try {
            TRIGGER(ID_BID_ALL,do_log(1,10,"calling mybid(0)"));
            hoffer_result=mybid(0); /* added result which is the actual line number the produced this bid */
            TRIGGER(ID_BID_ALL,do_log(1,20,"mybid(0) returns:%d",hoffer_result));
            }
        catch (...) {
            do_log(1,40,"hoffer_result failed from mybid(0)setting to 0");
            hoffer_result = 0;
            }
#ifdef FIND_RELEASE_CRASH
        TRIGGER(1200,do_log(1,10,"hoffer_result:%d",hoffer_result));
#endif
    /*    { int i,result;
            myftemp[0]='c'; myftemp[1]='\0';
            for (i=0;i<8;i++) {
                sprintf(f_command,"%u H",70+i);
                result =  f_eval(f_command);
                sprintf(myftemp+strlen(myftemp),"%u ",result);
            }
            fprintf(stderr,"%s\n",myftemp);
        } */
#ifdef CASTOR_BIDDING
        int cbid=0;
        if (hoffer_result<2 && strlen(all_sequence) > 0 && all_sequence[0] != '\0') {
            // int len = (int) strlen(all_sequence);
            char hand[16];
            hand[0]='\0';
            switch(bidder) {
                case 0: strcpy(hand,north); break;
                case 1: strcpy(hand,east); break;
                case 2: strcpy(hand,south); break;
                case 3: strcpy(hand,west); break;
                }
            cbid=castor_bid(hand,all_sequence,vul,dealer,bidder);
            }
        // fprintf(stderr,"new sequence is <%s> bid from line<%i>\n",get_seq(),hoffer_result);
        if (hoffer_result<2 && cbid != 1) {
            sprintf(bidbuf,"%s:P",get_seq()); // add that no bid was found (as yet)
            if (strlen(bidbuf)==2) sprintf(bidbuf,"P"); // no leading colon
            strcpy(all_sequence,bidbuf);
            set_seq(all_sequence);
            }
        if (hoffer_result<2 && cbid == 1) {
            fprintf(stderr,"added double after cbid == 1\n");
            sprintf(bidbuf,"%s:X",get_seq()); // add that no bid was found (as yet)
            if (strlen(bidbuf)==2) sprintf(bidbuf,"X"); // no leading colon
            strcpy(all_sequence,bidbuf);
            set_seq(all_sequence);
            }
        else {
            strcpy(all_sequence,get_seq());
            strcpy(bidbuf,get_seq());
            }
#else
        strcpy(all_sequence,get_seq());
        strcpy(bidbuf,get_seq());
#ifdef FIND_RELEASE_CRASH
        TRIGGER(1200,do_log(1,10,"bidbuf is %s",bidbuf));
#endif
#endif
#ifdef FIND_RELEASE_CRASH
    TRIGGER(1200,do_log(1,30,"RC: the new sequence came back as %s, evaluating",bidbuf));
        // try {my_evaluate_sequence(bidbuf);}
        // catch (...) { fprintf(stderr,"D: my_evaluate_sequence failed\n"); }
        // fprintf(stderr,"before:%s\n",get_seq());
        // fprintf(stderr,"D0:%s\n",hoffer_evaluate(bidbuf,0,0));
        // fprintf(stderr,"after:%s\n",get_seq());
        // fprintf(stderr,"D2:%s\n",hoffer_evaluate(bidbuf,0,2));
      // sprintf(bidbuf,"comment: %s\n",get_best_comment(0));
        // fprintf(stderr,"%s\n",bidbuf);
#endif
    //    fprintf(stderr,"testing complete; OK\n");


    //    fprintf(stderr,"%s\n",get_best_comment(0));
        the_bid[0]='\0';
        length = (int) strlen(bidbuf);
        // fprintf(stderr,"%s %u",bidbuf,length);
        passes = 0;
        for (i=0; i<BUF_MAX && bidbuf[0] != '\0' && i<length; i++) {
            if (bidbuf[i]=='X' || bidbuf[i] == 'x') {the_bid[0]='X'; the_bid[1]='\0'; passes =0; }
            else if (bidbuf[i]=='P' || bidbuf[i]=='p') {the_bid[0]='P'; the_bid[1]='\0'; passes++; }
            else if (bidbuf[i]=='R' || bidbuf[i]=='r') {the_bid[0]='R'; the_bid[1]='\0'; passes=0; }
            else if (bidbuf[i]>='1' && bidbuf[i]<='7') {the_bid[0]=bidbuf[i]; the_bid[1]='\0'; passes = 0; }
            else switch(bidbuf[i]) {
                case 'C': case 'c': case 'D': case 'd': case 'H': case 'h':
                case 'S': case 's': case 'N': case 'n':
                    the_bid[1]=bidbuf[i]; the_bid[2]='\0'; passes=0; break;
                default: the_bid[0]='\0';
                }
            // fprintf(stderr,"the_bid:<%s> passes:<%i>\n",the_bid,passes);
            }
        length = (int) strlen(the_bid);
        if (length==0) return my_return("no bid found");
        }
    // fprintf(stderr,"returning <%s> with <%i> passes\n",sequence,passes);
    // sprintf(sequence,"%s\n%s\n%s\n%s\n%s",north,east,south,west,get_seq());
    sprintf(all_sequence,"%s",get_seq());
    DO_STATE(50);
    TRIGGER(ID_BID_ALL,do_log(1,10,"hoffer_bid_all:%s",all_sequence));
    return all_sequence;
    }

#endif



#if defined(MYLEAD)
EXTERN_HOFFER_C char *mylead(const char *,const char *, unsigned int, unsigned int);
void do_bid_info(void);
int send_constraints(void);
#if defined(ANDROID)
char *hoffer_lead(const char *hand,const char *sequence,int vul, int dealer) {
#pragma message("Compiling hoffer_lead for ANDROID")
#elif defined(OSX) || defined(WIN32)
EXTERN_HOFFER_C char *hoffer_lead(const char *hand,const char *sequence,int vul, int dealer) {
#pragma message("Compiling hoffer_lead for OSX or WIN32")
#else
#pragma message("Compiling hoffer_lead for IOS")
char *hoffer_lead(const char *hand,const char *sequence,int vul, int dealer) {
#endif
	unsigned int leader,dir,i_am_vul=0;
	char seq[256];
	unsigned int bid_suits[2] = {0,0};
	char *declarer;
	hoffer_setup();
	strcpy(seq,sequence);
    clearbids();
	clear_clues();
    set_dealer(dealer);
    // ALOG("LUDDY setting sequence to %s in hoffer_lead",sequence);
	set_seq(sequence);
	declarer = get_declarer();
		switch (declarer[0]) {
			case 'W': case 'w': leader = 0; break;
			case 'E': case 'e': leader = 2; break;
			case 'S': case 's': leader = 3; break;
			default: leader = 1; break;
			}
	dir = leader % 2;
	if (set_hand(hand,leader)<0) return my_return("bad hand");
	if (vul <0 || vul > 3) return my_return("bad vul");
	if (dir <0 || dir > 3) return my_return("bad dir");
	i_am_vul=0;
	if (vul==0) { set_vul(0,0); set_vul(1,0); }
	else if (vul==1) { set_vul(1,1); if (dir==1) {i_am_vul=1;}}
	else if (vul==2) { set_vul(0,1); if (dir==2) {i_am_vul=1;}}
	else if (vul==3) { set_vul(0,1); set_vul(1,1); i_am_vul=1;}
    set_evaluation_system(gen32_evaluation_system);
    // fprintf(stderr,"set evaluation system to %u\n",evaluation_system);
	sprintf(bidbuf,"%s",get_description2(leader,i_am_vul));
    // fprintf(stderr,"description for bidder is %s\n",bidbuf);
	set_description(leader,bidbuf);
	get_constraints_by_sequence(seq, vul, dealer, 99);
    // cc = send_constraints();
    // printf("the number of constraints is %d",cc);
	// my_set_bid_info();
    do_bid_info();
    // ALOG("LUDDY do_bid_info was run");
	for (unsigned int suit=0; suit<4; suit++) 
		for (unsigned int side=0; side<2; side++)
			if (my_get_bid_info(side,suit)) {
                bid_suits[side] |= 1 << suit;
                // ALOG("LUDDY adding suit %d to bid suits on side:%d\n",suit,side);
                }

    // the second parameter is a bit_wise representation of the suits bit by leaders side
    // 3rd parameter is suits bit by opponents side;
    return mylead(seq,hand,bid_suits[dir], bid_suits[1-dir]); /* added result which is the actual line number the produced this bid */
}
#endif






#ifdef SCOTTY_BUF
    char scott_buffer[2000];
#endif
    char temp_buf[1000];
    char *hoffer_bid(const char *hand,const char *sequence,int vul, int dealer);
    char *hoffer_lead(const char *hand,const char *sequence,int vul, int dealer);
    char *do_hoffer_bid(const char *hand,const char *sequence,int vul, int dealer);
// hoffer_bid is working with ANDROID as is hoffer_evaluate 210505
// hoffer_lead is not working with ANDROID but signatures are to be the same as hoffer_bif
#if defined(OSX) || defined(WIN32)

char bidtemp[100];
EXTERN_HOFFER_C char *do_hoffer_bid(const char *hand, const char *sequence, int vul, int dealer) {
    do_log(1,30,"calling hoffer_bid");
     bidtemp[99] = '@';
    strcpy(bidtemp,hoffer_bid(hand,sequence,vul,dealer));
    assert(bidtemp[99]=='@');
    return bidtemp;
    }
EXTERN_HOFFER_C char *hoffer_bid(const char *hand,const char *sequence,int vul, int dealer) {
#else
extern "C" char *cpp_hoffer_bid(const char *hand, const char *sequence, int vul, int dealer) {
    // char fence[10];
    static char *temp=hoffer_bid(hand,sequence,vul,dealer);
    // for (int i=0;i<20;i++) fence[i]=77;
    return temp;
    }
    



extern "C" int check_trigger(int);
char *hoffer_bid(const char *hand,const char *sequence,int vul, int dealer) {
#endif
	int i,bidder,dir,i_am_vul=0,length,hoffer_result;
#undef SHORT_CIRCUIT
    // allowing intervening testing of one hand that causes stack overflow

#if defined(SHORT_CIRCUIT)
    static int call_count = 0;
    if (call_count == 2) {   // was equal to 2
        set_trigger("1605");
        // set_trigger("1200");
        set_trigger("1609");
        // set_trigger("1604");
        // set_trigger("1611");
        // print_triggers();
        // TRIGGER(1605,do_log(1,30,"triggers printed"));
        // do_log(1,30,"triggers set; printed should be just before this");
        TRIGGER(1605,do_log(1,30,"entered on recurse"));
        TRIGGER(1605,do_log(1,30,"sequence:%s",sequence));
        TRIGGER(1605,do_log(1,30,"hand:%s",hand));
        }
    call_count++;
    TRIGGER(1605,do_log(1,30,"call_count incremented to %d",call_count));
    if (call_count < 1) constraint_command(0,ID_COVERAGE,ID_HOFFER_BID,0,0);
#endif

#if defined(SHORT_CIRCUIT)
    if (call_count == 2) { char *result=NULL;
        TRIGGER(1605,do_log(1,30,"recursing on 3S bid"));
        ALOG("LUDDY evaluating 1N:2C:2S bid");
        // result = hoffer_bid("AJt65.k32.Aq54.3","1N:2C",0,0);
        result = hoffer_evaluate("1N:2C:2S",0,0);
        ALOG("LUDDY result is:%s",result);
        TRIGGER(1605,do_log(1,30,"back from recursing"));
        }
#endif
    if (debugging_flag & 2) {
        // set_trigger("1609");
        TRIGGER(1609,do_log(1,30,"hoffer_bid:entered with:%s,%s,%d,%d",hand,sequence,vul,dealer));
        }
#if defined(ALOGGING)
    ALOG("LUDDY:hoffer_bid:entered with:%s,%s; hoffer_setup next",hand,sequence);
#endif
    TRIGGER(1605,do_log(1,30,"hoffer_bid:entered with:%s,%s; hoffer_setup next",hand,sequence));
    TRIGGER(HOFFER_BID_ID,do_log(1,30,"hoffer_bid:entered with:%s,%s; hoffer_setup next",hand,sequence));
    TRIGGER(ID_BID_ALL,do_log(1,10,"hoffer_bid:entered with:%s,%s (temp_fix is set)",hand,sequence));
	hoffer_setup();
    TRIGGER(HOFFER_BID_ID,do_log(1,30,"hoffer_setup finished"));
    clearbids();
	clear_clues();
    set_dealer(dealer);
    TRIGGER(ID_BID_ALL,do_log(1,10,"hoffer_bid:setting sequence to <%s>",sequence));
    TRIGGER(HOFFER_BID_ID,do_log(1,10,"hoffer_bid:setting sequence to <%s>",sequence));
	set_seq(sequence);
	bidder = get_bidder(); // determined by sequence and dealer
	dir = bidder % 2;
    if (set_hand(hand,bidder)<0) { char bad_buffer[200];
        sprintf(bad_buffer,"Err. bad hand:%s for bidder:%d",hand,bidder);
        return my_return(bad_buffer);
        }
    // if (set_hand("AKJ876.5.54.AJT4",bidder)<0) return my_return("Err. bad hand");
	if (vul <0 || vul > 3) return my_return("Err. bad vul");
	if (dir <0 || dir > 3) return my_return("Err. bad dir");
	i_am_vul=0;
	if (vul==0) { set_vul(0,0); set_vul(1,0); }
	else if (vul==1) { set_vul(1,1); if (dir==1) {i_am_vul=1;}}
	else if (vul==2) { set_vul(0,1); if (dir==2) {i_am_vul=1;}}
	else if (vul==3) { set_vul(0,1); set_vul(1,1); i_am_vul=1;}
    set_evaluation_system(gen32_evaluation_system);
    sprintf(bidbuf,"%s",get_description2(bidder,i_am_vul));
#ifdef SCOTTY_BUF
    sprintf(scott_buffer,"    ");
    if (debugging_flag & 32 && debugging_level <= 20) {
        sprintf(scott_buffer,"   e:%d <%s> %s",gen32_evaluation_system,sequence,bidbuf);
        CSW_EXEC(fprintf(stderr,"set evaluation system to %u\n",gen32_evaluation_system));
        CSW_EXEC(fprintf(stderr,"description for bidder is %s\n",bidbuf));
        CSW_EXEC(fprintf(stderr,"sequence is ->%s<-\n",sequence));
        }
#endif
	set_description(bidder,bidbuf);
    TRIGGER(HOFFER_BID_ID,do_log(1,30,"hoffer_bid:%s, calling mybid(0)",bidbuf));
    TRIGGER(ID_BID_ALL,do_log(1,20,"hoffer_bid:%s, calling mybid(0)",bidbuf));
#if defined(ALOGGING)
    ALOG("LUDDY hoffer_bid calling mybid(0)");
#endif
    hoffer_result=mybid(0); /* added result which is the actual line number the produced this bid */

#ifdef CASTOR_BIDDING
    if (hoffer_result<2)
        hoffer_result = castor_bid(hand,sequence,vul,dealer,bidder);
#endif

	sprintf(bidbuf,"%s",get_seq());

    TRIGGER(HOFFER_BID_ID,do_log(1,20,"hoffer_result:%d",hoffer_result));
    TRIGGER(HOFFER_BID_ID,do_log(1,20,"hoffer_bid:<%s>",bidbuf));

    if (strlen(bidbuf)==0) return my_return("Err. null sequence returned after a bid was made");
    
	// fprintf(stderr,"the new sequence came back as %s\n",bidbuf);
//    my_evaluate_sequence(bidbuf);
//	sprintf(bidbuf,"comment: %s\n",get_best_comment(0));
//    fprintf(stderr,"%s\n",bidbuf);
//	fprintf(stderr,"testing complete; OK\n");


//	fprintf(stderr,"%s\n",get_best_comment(0));
	the_bid[0]='\0';
	length = (int) strlen(bidbuf);
	// fprintf(stderr,"%s %u",bidbuf,length)
	for (i=0; i<BUF_MAX && bidbuf[0] != '\0' && i<length; i++) {
		//fprintf(stderr,"the_bid:%s\n",the_bid);
		if (bidbuf[i]=='X' || bidbuf[i] == 'x') {the_bid[0]='X'; the_bid[1]='\0';}
		else if (bidbuf[i]=='P' || bidbuf[i]=='p') {the_bid[0]='P'; the_bid[1]='\0';}
		else if (bidbuf[i]=='R' || bidbuf[i]=='r') {the_bid[0]='R'; the_bid[1]='\0';}
		else if (bidbuf[i]>='1' && bidbuf[i]<='7') {the_bid[0]=bidbuf[i]; the_bid[1]='\0';}
		else switch(bidbuf[i]) {
			case 'C': case 'c': case 'D': case 'd': case 'H': case 'h':
			case 'S': case 's': case 'N': case 'n':
                if (the_bid[0] < '1' || the_bid[0] > '7') return my_return("Err. no level before denom");
				the_bid[1]=bidbuf[i]; the_bid[2]='\0'; break;
            case ':': the_bid[0]='\0'; break;
            default: return my_return("Err. bad character in returned sequence");
			}
		}
	length = (int) strlen(the_bid);
	if (length==0) return my_return("Err. 0 length; no bid found");
	else if (length==1) switch(the_bid[0]) {
        case 'P': case 'p': return my_return("P"); // scott_buffer[0] = 'P'; return scott_buffer; // my_return("P");
        case 'X': case 'x': return my_return("X"); // scott_buffer[0] = 'X'; return scott_buffer; // my_return("X");
        case 'R': case 'r': return my_return("R"); //scott_buffer[0] = 'R'; return scott_buffer; //return my_return("R");
		default:
            sprintf(temp_buf,"Err. bad length 1; rule:%d the_bid:%s char:%d seq:<%s>",hoffer_result,the_bid,the_bid[0],bidbuf);
            return my_return(temp_buf);
		}
	else if (length==2) {
		switch(the_bid[0]) {
			case '1': case '2': case '3': case '4': case '5': case '6': case '7': break;
			default: return my_return("Err. bad 1st char (level)");
			}
		switch(the_bid[1]) {
			case 'C': case 'c': case 'D': case 'd': case 'H': case 'h':
			case 'S': case 's': case 'N': case 'n': 
                bidbuf[2]='\0'; return the_bid; // scott_buffer[0] = bidbuf[0]; scott_buffer[1] = bidbuf[1]; return scott_buffer; //the_bid;
			default: return my_return("Err. 2nd char (bad denom)");
			}
		}
    else return my_return("Err. bad length of bid");
	return my_return("Err.. no bid processed");
}

// set - set the constraints from an input sequence and info about the hand, dealer, bidder.
// get(1) - get the constraints by line of 5 integers -  a generator of constraints
// get(2) - get the hands - a generator of hands
char *constraint_command(int,int,int,int,int);
	
void ConstraintAccess::set_sequence(const char *in_sequence,int in_vul, int in_dealer) {
	strcpy(sequence,in_sequence);
	vul = in_vul;
	dealer = in_dealer;
	what_index = 0;
	line_index = 0;
	}
	
int ConstraintAccess::setup_sequence(void) {	
	int result;
	result = global_b.get_constraints_by_line((char *)sequence,vul,dealer,player,-1);
	set_count(result);
	transfer_constraints();
	add_constraints();
	return result;
	}

void ConstraintAccess::set_count(int count) { constraint_index=0; constraint_count=count; }
void ConstraintAccess::reset_count() { constraint_index = 0; }

char ca_buf[250];
char ret_value[150];
extern "C" char *get_constraint_message(int,int,int,int,int);

char *ConstraintAccess::get(int what) {
	// return strings of the constraints, or of generated hands like a generator, one at a time.
	if (what == 2) { int ci=what_index;
		if (ci < constraint_count) { int who, what , where, lo, hi;
			who = constraints[ci][0];
			what = constraints[ci][1];
			where = constraints[ci][2];
			lo = constraints[ci][3];
			hi = constraints[ci][4];
			sprintf(ca_buf,"%3d-%2d,%2d,%2d,%2d,%2d    %s",ci,who,what,where,lo,hi,get_constraint_message(who,what,where,lo,hi));
			what_index++;
			}
#if defined(linux)
        else ca_buf[0]='\0';
#else
        else sprintf(ca_buf,"");
#endif
        return ca_buf;
		}
	else if (what == 3) { int ci=line_index;
		if (ci < global_b.constraint_parser->get_line_count()) { 
			sprintf(ca_buf,"%s",global_b.constraint_parser->get_line(ci));
			line_index++;
			}
#if defined(linux)
		else ca_buf[0]='\0';
#else
        else sprintf(ca_buf,"");
#endif
        return ca_buf;
		}
	else if (what == 1) return deal_and_return_one_hand();

    else {
#if defined(linux)
        ret_value[0]='\0';
#else
        sprintf(ret_value,"");
#endif        
        return ret_value;
        }
	}
	
void ConstraintAccess::transfer_constraints(void) {
	for (int i=0; i< constraint_count; i++)
		for (int j=0; j<5; j++)
			constraints[i][j] = global_b.constraint_parser->get_constraint(i,j);
	}

void ConstraintAccess::add_constraints(void) {
	constraint_command(0,99,0,0,0);
	for (int i=0; i< constraint_count; i++)
		constraint_command(constraints[i][0],constraints[i][1],constraints[i][2],constraints[i][3],constraints[i][4]);
	start_of_hand = constraint_count; // the hands are added as knowns, but since they can change and the constraints do not, store the start for resetting and adding a hand
	}

	
int ConstraintAccess::get_count(void) {
	int count=0;
	char *count_str = constraint_command(0,89,0,0,0);
	if (strlen(count_str) <= 0) return 0;
	if (count_str[0] >= '0' && count_str[0] <= '9') count = count_str[0] - '0';
	if (count_str[1] >= '0' && count_str[1] <= '9') {
		count = count * 10 + count_str[1]-'0';
		if (count_str[2] >= '0' && count_str[2] <= '9') count = count * 10 + count_str[1]-'0';
		}
	return count;
	}
	
char *ConstraintAccess::deal(int number,int where) { return constraint_command(0,98,0,where,number); }
char *ConstraintAccess::deal(int number) { return constraint_command(0,98,0,0,number); }
char *ConstraintAccess::get_hand(int index) { return constraint_command(0,97,index,0,0); }
char *ConstraintAccess::deal_and_return_one_hand(void) {
	char *ans;
	ans = deal(1);
	if (ans[0] == 'E') { // error condition
		strcpy(error_message,ans);
		return error_message;
		} 
	return get_hand(0);
	} 
	
int ConstraintAccess::add_a_card(int card,int dir) {
	int what=ID_KNOW,who=dir,where=card,lo=0,hi=0,constraint_count=get_count();
	constraint_command(who,what,where,lo,hi);
	constraint_command(0,7,constraint_count,0,0);
	return get_count();
	}
	
int ConstraintAccess::set_hand(const char *in_hand, int in_player) {
	player=in_player;
	strcpy(hand,in_hand);
	what_index = 0;
	setup_sequence();
	setup_hand();
	return 1;
	}

int ConstraintAccess::setup_hand(void) {
	// string needs to be of format ak43.ak43.4.qj76 so 13 cards and 3 periods
	// store the in_hand in hand
	// add the constraints of known cards via the has constraint_command
	if (constraint_count > start_of_hand) // there is already a hand there
		constraint_count = start_of_hand; // so reset the area where the hand is placed above the other constraints
	
	if (1)	{ int suit=3,_count=0, letter;
			for (int _ch=0; _ch < 16; _ch++) {
				letter = hand[_ch];
				switch (letter) {
					case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': 
						add_a_card(suit*13 + letter-'2',player); { _count++; break; }
					case 'T': case 't': add_a_card(suit*13 + 8,player); { _count++; break; }
					case 'J': case 'j': add_a_card(suit*13 + 9,player); { _count++; break; }
					case 'Q': case 'q': add_a_card(suit*13+10,player); { _count++; break;  }
					case 'K': case 'k': add_a_card(suit*13+11,player); { _count++; break; }
					case 'A': case 'a': add_a_card(suit*13+12,player); { _count++; break; }
					case '.': suit--; break;
					}
				}
			if (suit!=0) { constraint_count = start_of_hand; return 0; }
			if (_count!=13) { constraint_count = start_of_hand; return 0; }
		}
	return 1;
	}


ConstraintAccess c_global;

#ifndef IOS
EXTERN_HOFFER_C char *hoffer_set_sequence(const char *sequence, int vul, int dealer) {
#else
char *hoffer_set_sequence(const char *sequence, int vul, int dealer) {
#endif
	hoffer_setup();
	c_global.set_sequence(sequence,vul,dealer);
    sprintf(ret_value,"OK");
    DO_STATE(8);
	return ret_value;
}

#ifndef IOS
EXTERN_HOFFER_C char *hoffer_set_hand(const char *hand, int player) {
#else
char *hoffer_set_hand(const char *hand, int player) {
#endif
	hoffer_setup();
	c_global.set_hand(hand,player);
    sprintf(ret_value,"OK");
	return ret_value;
}

#ifndef IOS
EXTERN_HOFFER_C char *hoffer_get(int what) {
#else
char *hoffer_get(int what) {
#endif
	return c_global.get(what);	
}

EXTERN_HOFFER_C int f_eval(const char *);
int r_top[4],r_bottom[4];
char release_buffer[500];
char *do_hoffer_evaluate(const char *seqeuence, int vul, int dealer);

#ifndef IOS
EXTERN_HOFFER_C char *hoffer_evaluate(const char *sequence, int vul, int dealer) {
#else
char *hoffer_evaluate(const char *sequence, int vul, int dealer) {
#endif
    char store_sequence[500];
    char *result;
    result = constraint_command(0,ID_COVERAGE,ID_HOFFER_EVALUATE,0,0);
    hoffer_setup(); // added 210506
    if (strlen(result)> 2 && result[0]=='E' && result[1] == 'R' and result[2]=='R')
        return((char *)"ERR: cannot initialize hoffer");
    strcpy(store_sequence,get_seq());
    //if (debugging_flag & 2) {
        // set_trigger("1609,1602,1200");
        // TRIGGER(1602,do_log(1,30,"trigger:hoffer_evaluate:P:1N",sequence));
        // result = do_hoffer_evaluate("P:1N", 0, 1); // debugging test
        // TRIGGER(1609,do_log(1,30,"trigger:hoffer_evaluate:%s",sequence));
      //  }

    
    result = do_hoffer_evaluate(sequence, vul, dealer); // surround so that hoffer_evaluate never changes get_seq() result
    TRIGGER(1602,do_log(1,30,"setting sequence"));
    set_seq(store_sequence);
    TRIGGER(1602,do_log(1,30,"trigger:hoffer_evaluate returns:%s",result));
    return result;
    }

char *do_hoffer_evaluate(const char *sequence, int vul, int dealer) {
	int index,j;
	char temp_sequence[255],*bc=release_buffer;
    strcpy(release_buffer,"");
    if (strlen(sequence)==0) { strcpy(release_buffer,"Err. empty sequence"); return release_buffer; }
    int status = 1;
    try {
    try {
        hoffer_setup();
        clearbids();
        clear_clues();
        set_dealer(dealer);
        }
    catch(...) {
        sprintf(release_buffer,"Err. hoffer_evaluate setup fails");
        return release_buffer;
    }
	// set_seq(sequence);
	if (vul==0) { set_vul(0,0); set_vul(1,0); }
	else if (vul==1) { set_vul(1,1);}
	else if (vul==2) { set_vul(0,1);}
	else if (vul==3) { set_vul(0,1);}
	for (index=0; index<4; index++) { r_top[index]=37; r_bottom[index]=0;}
	for (index=0; index < (int) strlen(sequence); index++) {
        status = 1;

	if (sequence[index]!='X' && sequence[index]!='P' && sequence[index] != 'R'
		&& sequence[index]!='C' && sequence[index] != 'D' && sequence[index] != 'H' && sequence[index] != 'S' && sequence[index] != 'N') continue;
		for (j=0; j<=index;j++) temp_sequence[j]=sequence[j];
		temp_sequence[index+1]='\0';

        try {
            my_evaluate_sequence(temp_sequence);
            my_evaluate_sequence(temp_sequence);
        }
        catch (...) {
            sprintf(release_buffer,"EXCEPTION: my_evaluate_sequence:%s\n",temp_sequence);
            fprintf(stderr,"%s\n",release_buffer);
            status = 0;
            }

        if (status==1) try { int i,top,bottom; // do range constraining
        for (i=0;i<4;i++) {
            sprintf(f_command,"%u H",70+2*i);
            top=f_eval(f_command);
            // fprintf(stderr,"top is %u\n",r_top[i]);
            sprintf(f_command,"%u H",70+2*i+1);
            bottom=f_eval(f_command);
            // fprintf(stderr,"bottom is %u\n",r_bottom[i]);
            if (top>37) top =37;
            if (bottom<0) bottom=0;
            if (top < bottom) { 
                if (1)
                    fprintf(stderr,"top < bottom");
                top = bottom+1;
                }
            if (top < r_top[i]) {
#ifdef TOP_BOTTOM_ERR
                    fprintf(stderr,"top < r_top[i]");
#endif
                if (top < r_bottom[i]) r_top[i]=r_bottom[i];
                else r_top[i]=top;
                }
            if (bottom > r_bottom[i]) {
#ifdef TOP_BOTTOM_ERR
                    fprintf(stderr,"bottom > r_bottom[i]");
#endif
                if (bottom > r_top[i]) r_bottom[i]=r_top[i];
                else r_bottom[i]=bottom;
                }
            if (r_top[i] < r_bottom[i]) {
                if (1)
                    fprintf(stderr,"r_top < r_bottom");
                }            if (top < bottom) top = bottom+1;

            }
    }
    catch (...) {
        sprintf(release_buffer,"EXCEPTION. top bottom processing");
        fprintf(stderr,"%s\n",release_buffer);
        status = 0;
        }
    
    // fprintf(stderr,"calling get_best_comment(2)\n");
    if (status == 1)
        try {
            bc = get_best_comment(2);
            }
        catch (...) {
            strcpy(release_buffer,"EXCEPTION: get_best_comment");
            bc = release_buffer;
            }
    }
    }
    catch(...) {
        sprintf(release_buffer,"EXCEPTION. outside try catch of hoffer_evaluate");
        fprintf(stderr,"%s\n",release_buffer);
        bc = release_buffer;
        }
    return bc;
	}
EXTERN_HOFFER_C char *get_line_info(int);
extern char *get_constraint(int,int);

extern char *get_generate(int);

EXTERN_HOFFER_C char *hoffer_verify(int section, char *in) {
	/* section == 0: send a line of constraint strings and process them into set of constraints (5 ints)
	   otherwise get the constraints for comparison to expected as a test. Stored in c.constraints array */
	static ConstraintParser c;
	if (section==0) { c.set_test(in); return (char *) "ok"; }
	else return c.get_test(section-1,in); /* 0 based array, and I wanted to use 0 to set the test */
}

char temp_constraint[100];
void get_ranges_from_forth(char *range, int *lo, int *hi) {
    int start = 0;
    if (range[0]=='#' && range[1]=='F') start = 2;
	temp_constraint[0]='\0';
	bool comma_flag= false;
	char part1[200],part2[200];
	int j=0,k=0;
	for (unsigned int i=start; i<=strlen(range) && i<199; i++) { // want to include EOS
		if (range[i]==',') { comma_flag=true; part1[j]='\0'; }
		else if (!comma_flag) part1[j++]=range[i];
		else part2[k++]=range[i];
		}
	*lo = f_eval(part1);
    if (part2[0]=='#' && part2[1]=='F') { part2[0]=' '; part2[1]=' '; } 
    *hi = f_eval(part2);
	}


char range_result[40];
// this is the old code
char *process_range(int lineno, int bidder) {
	int lo,hi;
	char *range;
	range=get_range(lineno,bidder);
	if (strcmp("RANGE NOT FOUND",range)==0)
        {range_result[0]='\0'; return range_result; }
	if (strcmp("NONE",range)==0)
        {range_result[0]='\0'; return range_result; }
	range = macro(range,bidder);
        // (range[0] == '#' || (range[0]=='6' && range[1]=='9')) {
    get_ranges_from_forth(range,&lo,&hi);
    sprintf(range_result,"%d,%d",lo,hi);
	if (range_result[0]<'0' || range_result[0]>'9') range_result[0]='\0'; // some sort of illegal input
	return range_result;
	}

EXTERN_HOFFER_C char *get_range_processed(int lineno,int bidder) {
	char *range;
	range = process_range(lineno,0);
	if (strlen(range)==0) range = process_range(lineno,1+bidder%2);  // range can be different for different directions
	return range;
	}
	
	
extern char h_constraint[250];
#undef INCLUDE_RANGE_NOTE
#if defined(OSX) || defined(WIN32)
EXTERN_HOFFER_C char *hoffer_constraints(const char *sequence, int vul, int dealer, int range_command) {
#else
char *hoffer_constraints(const char *sequence, int vul, int dealer, int range_command) {
#endif
	char constraint[250];
    char constraint_holder[250];
    char *temp, *terminal_line=NULL,*temp_constraint,*processed,*c_macro,*range;
    unsigned int i,j,k,line_num=0;
    int last_bidder;
	static Bergen b;
	char temp1[25],temp2[300];
    bool range_flag = false;
    constraint_command(0,ID_COVERAGE,ID_HOFFER_CONSTRAINTS,0,0);


#ifndef EXPLICIT
    // removed 210121
    hoffer_setup();
#endif
    
#if defined(BLOCK_CONSTRAINTS)
    return "";
#endif
    
    if (range_command & 1) // reset top and bottom values for each direction
        for (int i=0; i<4; i++) { r_top[i]=40; r_bottom[i]=0; }

    

    strcpy(h_constraint,sequence);
/*    { int i,result;
        myftemp[0]='a'; myftemp[1]='\0';
        for (i=0;i<8;i++) {
            sprintf(f_command,"%u H",70+i);
            result =  f_eval(f_command);
            sprintf(myftemp+strlen(myftemp),"%u ",result);
                    }
        fprintf(stderr,"%s\n",myftemp);
    } */

    temp = hoffer_evaluate(sequence,vul,dealer);
/*    { int i,result;
        myftemp[0]='b'; myftemp[1]='\0';
        for (i=0;i<8;i++) {
            sprintf(f_command,"%u H",70+i);
            result =  f_eval(f_command);
            sprintf(myftemp+strlen(myftemp),"%u ",result);
        }
        fprintf(stderr,"%s\n",myftemp);
    }*/
    last_bidder = (get_bidder() + 3) % 4;
    if (temp && strlen(temp) > 0) {
        // fprintf(stderr,"calling get_best_comment\n");
    
        temp = get_best_comment(last_bidder);
        // fprintf(stderr,"calling get_line_info(1)");
        terminal_line = get_line_info(1);
        strcat(h_constraint,terminal_line);
        }
/*    if (terminal_line == NULL)
		fprintf(stderr,"terminal line is null\n");
		*/
    if (terminal_line !=NULL) {
        for (i=0; i< strlen(terminal_line) && (terminal_line[i] <'0' ||  terminal_line[i] > '9'); i++) ;
        for (;i<strlen(terminal_line) && terminal_line[i] >= '0' && terminal_line[i] <= '9';i++)
            line_num = 10*line_num + terminal_line[i]-'0';
        }
    sprintf(h_constraint+strlen(h_constraint),"(%i)",line_num);

#define USE_BEST_GENERATE
#ifdef USE_BEST_GENERATE
    range = NULL;

    temp_constraint = BestGenerate(last_bidder);  // puts the range at the end but not only is that not needed; ID_ORAND must be at the end of a list of constraints
    { int state =0; int len = (int) strlen(temp_constraint)-1;

	while (len > 0 && temp_constraint[len] == ' ') len--;
	if (temp_constraint[len] >= '0' && temp_constraint[len] <= '9') { state = 1; len--; }
	if ( state == 1 && temp_constraint[len] >= '0' && temp_constraint[len] <= '9') { len--; }
	if (state == 1 && (temp_constraint[len] == ',' || temp_constraint[len] == ':')) { state = 2; len--; }
	if (state == 2 && temp_constraint[len] >= '0' && temp_constraint[len] <= '9') { state = 3; len--; }
	if (state == 3 && temp_constraint[len] >= '0' && temp_constraint[len] <= '9') { len--; }
	if (state == 3 && (temp_constraint[len] == 'R' || temp_constraint[len] == 'r')) { state = 4; len--;}

    if (state == 4) {
        temp_constraint[len]='\0';
        range = &temp_constraint[len+2];
        }
    }
	c_macro = macro(temp_constraint,last_bidder);
    
	strcpy(constraint_holder,c_macro);
    // sprintf(constraint_holder,"%s",c_macro);  dropped the %2:1| front part of %2 with formatting changes 210123
    processed = b.c_process(constraint_holder);
#else
    temp_constraint = get_generate(line_num);
#endif

	i=0;j=0;
	if (range == NULL) range = get_range_processed(line_num,last_bidder);
	while (processed[j] != '\0' && i < 250 ) {
		constraint[i++]=processed[j++];
		if (i>1 && constraint[i-2]=='B' && (constraint[i-1]=='s' || constraint[i-1]=='S')) { // looking at B[sS][CDHS]
			if(range != NULL && range[0] != '\0') {
                range_flag = true;
				for (k=0; range[k] != '\0'; k++) { 
					if (range[k]==',' || range[k] == ':') constraint[i++]=':';
					else if (range[k] >= '0' && range[k] <= '9') constraint[i++]=range[k];
					else break;
					}
				}
			else { strcpy(&constraint[i],"0:40"); i += 4;}  // there was no range found (an error) put in a default range of 0:40
			}
		}
        
    if (range_flag) range = NULL; // so that it is not processed by the next paragraph which is a range without a preceding Bs or BS
	constraint[i]='\0';

#define ITEMP_MAX 20
	if (range != NULL && range[0] != '\0') {  // fixed 200514 as it was adding a spurious R for empty string (not null)
    // fixed 200828 to put the R in front of the rest of the constraints
    // fixed i values
        char temp[ITEMP_MAX],temp2[200];
        int i=0;
		temp[i++]=' '; temp[i++]='R';
		for (int k=0; range[k] != '\0'; k++) {
                if (i >= ITEMP_MAX-1) break;
				if (range[k]==',') temp[i++]=':';
				else temp[i++]=range[k];
                }
        if (i >= ITEMP_MAX-1)
            printf("oh oh, ITEMP_MAX exceeeded\n");
        temp[i]='\0';
        strcpy(temp2,constraint);
        strcpy(constraint,temp);
        strcat(constraint," ");
        strcat(constraint,temp2);
        // sprintf(constraint,"%s %s",temp,temp2); use strcpy instead of sprintf with embedded %
		}

	
	c_macro = macro(constraint,last_bidder);
    sprintf(h_constraint,"%s",c_macro);
    if (range_command & 2) {

        for (i=0; i<4; i++) {
            // done for putting ID_ORANDS at the end always to aid processing
            sprintf(temp1,"R%u:%u:%u ",i,r_bottom[i],r_top[i]);
            sprintf(temp2,"%s",h_constraint);
            strcpy(h_constraint,temp1);
            strcat(h_constraint,temp2);
            // sprintf(h_constraint,"%s%s",temp1,temp2);
            }
        }
    // sprintf(h_constraint+strlen(h_constraint)," V1:03");

    return h_constraint;
    }

/* 'C':'DHS','D':'CHS','H':'CDS','S':'CDH',
'CD':'HS','DC':'HS','CH':'DS','HC':'DS', 'CS':'DH','SC':'DH',
'DH':'CS','HD':'CS','DS':'CH','SD':'CH','HS':'CD','SH':'CD',
'CHS':'D','CSH':'D','HSC':'D','HCS':'D','SHC':'D', 'SCH':'D',
'DHS':'C','DSH':'C','HSD':'C','HDS':'C','SHD':'C', 'SDH':'C',
'CDS':'H','CSD':'H','DSC':'H','DCS':'H','SDC':'H', 'SCD':'H',
'CHD':'S','CDH':'S','HDC':'S','HCD':'S','DHC':'S', 'DCH':'S' }*/

char *get_grab(int);

int set_hand(const char *hand, int dir) {
	 char ch;
	 int i,card=14,suit,hcps=0,cards[13],card_index=0;
	 suit = 3; 
	 if (strlen(hand) != 16) {
		fprintf(stderr,"bad length");
		return -1;
		}
	 for (i=0; i<16; i++) {
		 ch = hand[i];
		 switch (ch) {
			 case 'A':
			 case 'a': card = 12 + suit*13;  hcps += 4; cards[card_index++]=card; break;
			 case 'K':
			 case 'k': card = 11 + suit*13;  hcps += 3; cards[card_index++]=card; break;
			 case 'Q':
			 case 'q': card = 10 + suit*13;  hcps += 2; cards[card_index++]=card; break;
			 case 'J':
			 case 'j': card = 9 + suit*13;  hcps += 1; cards[card_index++]=card; break;
			 case 'T':
			 case 't': card = 8 + suit*13;  cards[card_index++]=card; break;
			 case '9': case'8': case '7': case'6': case '5': case'4': case'3': case'2':
				card = ch - '2' + suit*13; cards[card_index++] = card; break;
			 case '.': suit -= 1; break;
			 default: 
				 fprintf(stderr,"bad card %s %u\n",hand,i);
				 return -1;
			}	
		 if (i>0 && ch != '.' && card_index>1 && cards[card_index-1] >= cards[card_index-2]) {
			 fprintf(stderr,"bad order i:%u card_index:%u card:%u cards[i-1]:%u\n",i,card_index,card,cards[card_index-1]);
			 for (int ii=0; ii<card_index; ii++) {
				 fprintf(stderr,"so far, card_index:%u card:%u\n",ii,cards[ii]);
				}
			 return -1; // not in decreasing order
			}
		}		
	for (i = 12; i>=0; i--) set_deck(12-i+(dir*13),cards[i]); // must be in increasing order
	return hcps;
 }

#ifdef IOS
void UpdateSystem(void);
#else
EXTERN_HOFFER_C void UpdateSystem(void);
#endif
void load_rules(void);

    
#ifndef IOS
extern "C" int set_system(int,int);
#else
int set_system(int,int);
#endif

    void update_macros(const char *, const char *);

#if defined(linux)
    extern int hoffer_setup_flag;
#else
    int hoffer_setup_flag = 0;
#endif

    extern "C" char *do_setup_macros_early(void) {
        macro_init(0);
        setup_macros(15, 12, 20, 18, 3);
        load_macros();
        domacro("__TNT", 0);
        return macro_temp;
        }
#if defined(ANDROID)
#define USE_ALFT
#endif

#undef USE_ALFT
#ifdef USE_ALFT
#include <android/log.h>
#define  LOG_TAG    "testjni"
#define  ALOG(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#endif


#define EXHAUSTIVE
#if defined(EXHAUSTIVE)

#include <string.h>
    extern "C" char *get_template(int);
    extern "C" int macro_init_flag;
#endif
    
    extern "C" void do_hoffer_setup(void) { hoffer_setup(); }
void hoffer_setup(void) {
    
#if defined(ALOGGING)
    ALOG("LUDDY hoffer setup entered");
#endif

#ifdef USE_CSW
    #if defined(USE_ALFT)
        ALOG("hoffer:not calling csw_setup");
#endif
	// csw_setup();
#if defined(ALOGGING)
        ALOG("hoffer:back from not calling csw_setup");
#endif
#endif

#if defined(ANDROID) && !defined(NO_CSW)
    static bool csw_loaded = false;

    if (!csw_loaded) {
#if defined(USE_ALFT)
        // ALOG("hoffer:calling getargs2 -slogging.cpp;matcher.cpp;bridge2.cpp;string.cpp");
#endif
        // getargs2((const char *) "-slogging.cpp;matcher.cpp;bridge2.cpp;string.cpp");
        // set_one_shot("200,201");
        ONE_SHOT(200,do_log(1,20,"hoffer:csw initialized correctly"));
#if defined(USE_ALFT)
        // ALOG("hoffer:back from getargs2");
#endif
        csw_loaded = true;
#if defined(USE_ALFT)
             ALOG("setting 1602 trigger");
#endif
                  // set_trigger("1602");
    }
#endif


    // if (hoffer_setup_flag == 0) macro_init_flag = 0;
#if defined(ALOGGING)
    ALOG("hoffer_setup entered with hoffer_setup_flag at %d",hoffer_setup_flag);
#endif
    if (hoffer_setup_flag == 32) return;
	if (hoffer_setup_flag != 32) {


        // ALOG("This message comes from hoffer_setup at line %d. Before get_comments_loaded", __LINE__);

        if (!hoffer.get_comments_loaded()) {
			if (strcmp(get_template(101), "BIDDING") == 0) hoffer.set_comments_loaded();
			else {
				CSW_EXEC(BidMessage("loading comments..."));

					load_comments();
// #pragma message("long assert present")
#if defined(EXHAUSTIVE)
                    if (strcmp(get_template(101), "BIDDING") != 0) return;
                // assert(strcmp(get_template(101), "BIDDING") == 0);
#endif
					hoffer.set_comments_loaded();
					CSW_EXEC(BidMessage("comments loaded\n"));

			}
			hoffer_setup_flag |= 1;
		}
        
#if defined(EXHAUSTIVE)
        if (!hoffer.get_comments_loaded() || !(hoffer_setup_flag & 1)) return;
		hoffer_setup_flag = 125;
#endif


		if (!hoffer.get_macros_loaded()) {
		    macro_init(0);


#if !defined(EXHAUSTIVE)
#ifdef OLD_MACRO_SETUP
            CSW_EXEC(BidMessage("calling do_update_macros..."));
            update_macros("_NTMIN","15");
            update_macros("_TNTMIN","20");
            update_macros("_TNT_MAX","21");
            update_macros("_NT_OPP_MIN","12");
            update_macros("_NT_HIOPP_MIN","18");
#else
            setup_macros(15, 12, 20, 18, 3);

#endif
#endif

#if defined(EXHAUSTIVE)
            if (macro_init_flag) hoffer_setup_flag = 126;
            else hoffer_setup_flag = 127;

            domacro("@define(BTHUYR,10)",0);

            hoffer_setup_flag = 129;
            // domacro("BTHUYR",0);
            // ALOG("back from checking the value of BTHUYR");
            // if (strcmp(macro_temp,"10") != 0) {
                // ALOG("hoffer: value is not 10...<%s>",macro_temp);
                // return;
            //}
            // else ALOG("hoffer:value is ten it is:<%s>",macro_temp);

            hoffer_setup_flag = 130;
            setup_macros(15,12,20,18,3);
			domacro("_TNTMIN", 0);
            hoffer_setup_flag = 131;
            if (strcmp(macro_temp,"20") != 0) return;
			// assert(strcmp(macro_temp, "20") == 0);
			domacro("_NTMIN", 0);
            hoffer_setup_flag = 132;
            if (strcmp(macro_temp,"15") != 0) return;
			// assert(strcmp(macro_temp, "15") == 0);
#endif



			CSW_EXEC(BidMessage("calling load_macros..."));
			load_macros();

#if defined(EXHAUSTIVE)
            hoffer_setup_flag = 133;
			domacro("__TNT", 0);
            if (strcmp(macro_temp,"(~19>&~22<)") != 0) return;
			// assert(strcmp(macro_temp, "(~19>&~22<)") == 0);
            hoffer_setup_flag = 134;
#endif

			// fprintf(stderr,"macros verified\n");
			CSW_EXEC(BidMessage("macros updated for comments..."));
			hoffer.set_macros_loaded();

			if (debugging_flag & 1 && debugging_level >= 20)
				CSW_EXEC(fprintf(stderr, "macros loaded and verified for _NTMIN 15\n"));
			// hoffer_setup_flag |= 2;
			hoffer_setup_flag = 3;
		}

#if defined(EXHAUSTIVE)
    if (!hoffer.get_macros_loaded() || !(hoffer_setup_flag & 2)) return;
#endif


	if (!hoffer.get_rules_loaded()) {
        CSW_EXEC(BidMessage("loading rules..."));
    	load_rules();
    	hoffer.set_rules_loaded();
    	UpdateSystem();

        CSW_EXEC(BidMessage("rules loaded\n"));
        hoffer_setup_flag |= 4;
	}

#if defined(EXHAUSTIVE)
    if (!hoffer.get_rules_loaded() || !(hoffer_setup_flag & 4)) return;
#endif

	if (!hoffer.get_convents_loaded()) {
        CSW_EXEC(BidMessage("loading conventions..."));
    	load_convents();
    	hoffer.set_convents_loaded();
    	CSW_EXEC(BidMessage("conventions loaded\n"));
        hoffer_setup_flag |= 8;
	}

#if defined(EXHAUSTIVE)
    if (!hoffer.get_convents_loaded() || !(hoffer_setup_flag & 8)) return;
#endif

                
    if (!hoffer.get_tables_loaded()) {
        CSW_EXEC(BidMessage("setting evaluation system\n"));
		set_evaluation_system(gen32_evaluation_system);
        CSW_EXEC(BidMessage("setting tables..."));
		set_table(0);
		c_lock(0,0);
		c_lock(0,1);
		hoffer.set_tables_loaded();
        CSW_EXEC(BidMessage("tables loaded\n"));
        hoffer_setup_flag |= 16;
		}
        
#if defined(EXHAUSTIVE)
    if (!hoffer.get_tables_loaded() || !(hoffer_setup_flag & 16)) return;
#endif

        
    } // hoffer_setup_flag

    if (hoffer_setup_flag == 31) {
        // CSW_EXEC(BidMessage("hoffer_setup calling set_system for both dirs"));
        // fprintf(stderr,"hoffer_setup calling set_system for both dirs");
        // set_system(0,0);
        // set_system(0,1);
        // CSW_EXEC(BidMessage("hoffer_setup finished OK and set hoffer_setup_flag to 32\n"));
        hoffer_setup_flag = 32;
        }
    else if (hoffer_setup_flag < 31) {
        fprintf(stderr,"hoffer_setup failed, value is %d; should have been 31",hoffer_setup_flag);
        exit(0);
        }
}

void do_update_macros(void) {
    update_macros("MACROFCM","FIVE_CARD_MAJOR OPENER");
    update_macros("MACRO_ACES","!ROMAN KEY CARD BLACKWOOD");
    // fprintf(stderr,"do_update_macros called\n");
#ifndef HOFFER
    update_macros("_NTMIN","15");
    update_macros("_TNTMIN","20");
    update_macros("_TNT_MAX","21");
    update_macros("_NT_OPP_MIN","12");
    update_macros("_NT_HIOPP_MIN","18");
#endif
}


void update_macros(const char *macro,const char *value) {
	char buffer[240];

    sprintf(buffer,"@undefine(/%s\\)",macro);

    // fprintf(stderr,"%s\n",buffer);
    domacro(buffer,0);
    domacro(buffer,1);
    sprintf(buffer,"@define(%s,%s)",macro,value);
	// fprintf(stderr,"%s\n",buffer);
    domacro(buffer,0);
    domacro(buffer,1);
}

#ifdef EXPLICIT
void load_comments(void) {
#else
EXTERN_HOFFER_C void load_comments(void) {
#endif
#ifdef EXPLICIT
	static bool loaded=false;
	char inbuf[BUF_MAX];
	char commentbuf[BUF_MAX];
	int comment_index = 0;
	int comment_length;
	int i,j,index;
	bool index_done;
	inbuf[BUF_MAX-1]='%';
	if (loaded) return;
	int max = get_comment_size();
	assert(max > 0);
	while (comment_index < max) {
		index = 0; index_done=false;
		strcpy(inbuf,get_comment(comment_index++));
		if (inbuf[0]==0) break;
		assert(inbuf[BUF_MAX-1]=='%');
		comment_length = (int) strlen(inbuf);
		assert(comment_length>0);
		for (i=0;i<comment_length && index_done==false;i++) {
			if (inbuf[i] >= '0' && inbuf[i] <= '9') index = index*10 + inbuf[i]-'0';
			else index_done = true;
			}
		j=0; i+=3;
		while (i<comment_length && inbuf[i] != 0) {
			commentbuf[j++]=inbuf[i++];
			}
		commentbuf[j-1]='\0'; // get rid of extra quote
		assert(index > 0);
		assert(strlen(commentbuf) > 0);
		// fprintf(stderr,"comments %u %s\n",index,commentbuf);
		set_template(index,commentbuf);
	}
	if (strcmp(get_template(101),"BIDDING")==0) loaded=true;
#endif
}

#ifndef EXPLICIT
void load_convents(void) {
#else
EXTERN_HOFFER_C void load_convents(void) {
#endif
#ifdef EXPLICIT
	char inbuf[BUF_MAX];
	char conventbuf[BUF_MAX];
	int convent_index = 0;
	int convent_length;
	int i,j,index;
	inbuf[BUF_MAX-1]='%';
	int max = get_convent_size();
	while (convent_index < max) {
		strcpy(inbuf,get_convent(convent_index++));
		if (inbuf[0]==0) break;
		assert(inbuf[BUF_MAX-1]=='%');
		convent_length = (int) strlen(inbuf);
		assert(convent_length>0);
		//fprintf(stderr,"inbuf:%s\n",inbuf);
		index = inbuf[0]-'0';
		//fprintf(stderr,"index:%u",index);
		j=0; i=1;
		while (i<convent_length && inbuf[i] != 0) {
			conventbuf[j++]=inbuf[i++];
			}
		conventbuf[j]='\0'; // get rid of extra quote
		assert(strlen(conventbuf) > 0);
		c_set(conventbuf,0,index);
        c_set(conventbuf,1,index);
	}

	for(i=0;i<2;i++) {
		f_setbidder(i);  // negative double
		push_contract("4S");
		f_eval("0H50G");
        f_setbidder(i);
        push_contract("2S"); // pushes into 0 variable
        f_eval("0H49G");  // pull 0 variable into 49 variable
		f_setbidder(i);
        push_contract("4H"); //  pushes into 0 variable
        f_eval("0H48G");  // pull 0 variable into 48 variable
		}
#endif
}

EXTERN_HOFFER_C void set_explicit_flag(int flag) {
	if (flag == 0) explicit_flag = false;
	else explicit_flag = true;
}

#ifdef EXPLICIT
void load_macros(void) {
#else
EXTERN_HOFFER_C void load_macros(void) {
#endif
#ifdef EXPLICIT
	static bool loaded = false;
	char inbuf[BUF_MAX];
	char macrobuf[BUF_MAX];
	int macro_index = 0;
	int macro_length;
	int i,j;
	if (loaded) return;
	
// 	assert(1==macro_verify());
    CSW_EXEC(BidMessage("calling load_macros..."));

	inbuf[BUF_MAX-1]='%';
	int max = get_macro_size();
	while (macro_index < max) {
		strcpy(inbuf,get_macro(macro_index++));
		if (inbuf[0]==0) break;
		assert(inbuf[BUF_MAX-1]=='%');
		macro_length = (int) strlen(inbuf);
		assert(macro_length>0);
		j=0; i=0;
		while (i<macro_length && inbuf[i] != 0) {
			macrobuf[j++]=inbuf[i++];
			}
		macrobuf[j]='\0'; // get rid of extra quote
		assert(strlen(macrobuf) > 0);
		domacro(macrobuf,0);
        domacro(macrobuf,1);
	}
	domacro("__LOADED",0);
	assert(inbuf[BUF_MAX-1]=='%');
	assert(strcmp(macro_temp,"TRUE")==0); 
	loaded=true;
#endif
}

EXTERN_HOFFER_C void load_rules(void) { // read rules explicitly from code
	static bool loaded=false;
	if (!loaded) initialize_matchtree();
    if (debugging_flag & 17 && debugging_level <=30)
        CSW_EXEC(fprintf(stderr,"reading the rules\n"));
	read_rules();
	loaded = true;
}


// #endif


 #if defined(OSX) && !defined(ANDROID)
    extern "C" char *cpp_call_amzi(const char *command) {
        return (char *) "not implemented for OSX";
    }
 #endif

    
    // there are different amzi.h... this is the one from the working amziios library
#if defined(AMZI) || defined(AMZI_GET_A_LEAD)
#include "amzi.h"
    
    int reload_amzi(void);

    extern "C" char *cpp_call_amzi(const char *command) {
        return call_amzi(command);
        }

    
    extern "C" int cpp_assert_amzi(const char *command) {
        return assert_amzi(command);
        }
    
    extern "C" char *cpp_str_call_amzi(const char *command, int position) {
        return str_call_amzi(command,position);
        }
#endif
    
#if !defined(ANDROID)
int initialize_amzi_there(void); // in amziios library
extern "C" void set_pybid_loaded(void);
int amzi_initialize(void) {
    // fprintf(stderr,"deprecated for amziios library\n");
    TRIGGER(ID_INITIALIZE,do_log(1,30,"amzi_initialize entered to call initialize_amzi_there in amziios library"));
    if (initialize_amzi_there()>0) {
        set_pybid_loaded(); // added and fixed this for when AMZI is active 210408
        return 1;
        }
    else return 0;
    }

extern "C" bool cpp_initialize_amzi(void) {
    TRIGGER(ID_INITIALIZE,do_log(1,40,"cpp_initialize_amzi entered to call amzi_initialize"));
    amzi_initialize();
    return true;
    }
// #pragma message("cpp_initialize_amzi is coded")
#endif


#if defined(AMZI_GET_A_LEAD)
extern "C" char *test_get_a_lead(void);
char *cpp_test_get_a_lead(void) {
        return test_get_a_lead();
    }
#endif

    
#define AMZI_THERE_TEST
#ifdef AMZI_THERE_TEST
int amzi_initialize(void);
char *call_amzi(const char *);
int assert_amzi(const char *);
char *str_call_amzi(const char *,int);
int initialize_amzi_there(void); // in amziios library
#endif // AMZI_THERE_TEST

#if defined(AMZI_PLAY) || defined(AMZI_GET_A_LEAD)    // 0
    extern "C" char *cpp_amzi_get_a_lead(int);
    
#if defined(AMZI_GET_A_LEAD)
#pragma message("test_get_a_lead is defined")
extern "C" char *test_get_a_lead(void) {
    constraint_command(2,ID_SET_CONTRACT,43,0,0);

#ifdef WHY_IS_THIS_HERE_201128
    call_amzi("set_cards(0,53)");
       call_amzi("set_cards(0,52)");
       call_amzi("set_cards(1,52)");
       call_amzi("set_cards(2,52)");
       call_amzi("set_cards(3,52)");
#endif
    int constraints_count_temp = global_b.get_constraints_count();
           constraint_command(3,28,44,0,0);
            constraint_command(3,7,constraints_count_temp+0,0,0);
            constraints_count_temp = global_b.get_constraints_count();
           constraint_command(3,28,42,0,0);
           constraint_command(3,7,constraints_count_temp+2,0,0);
       
           constraint_command(3,28,41,0,0);
           constraint_command(3,7,constraints_count_temp+4,0,0);
           constraint_command(3,28,39,0,0);
           constraint_command(3,7,constraints_count_temp+6,0,0);
           constraint_command(3,28,38,0,0);
           constraint_command(3,7,constraints_count_temp+8,0,0);
           constraint_command(3,28,34,0,0);
           constraint_command(3,7,constraints_count_temp+10,0,0);
           constraint_command(3,28,29,0,0);
           constraint_command(3,7,constraints_count_temp+12,0,0);
           constraint_command(3,28,28,0,0);
           constraint_command(3,7,constraints_count_temp+14,0,0);
           constraint_command(3,28,24,0,0);
           constraint_command(3,7,constraints_count_temp+16,0,0);
           constraint_command(3,28,23,0,0);
           constraint_command(3,7,constraints_count_temp+18,0,0);
           constraint_command(3,28,15,0,0);
           constraint_command(3,7,constraints_count_temp+20,0,0);
           constraint_command(3,28,13,0,0);
           constraint_command(3,7,constraints_count_temp+22,0,0);
       constraint_command(3,28,6,0,0);
       constraint_command(3,7,constraints_count_temp+24,0,0);
       constraints_count_temp = global_b.get_constraints_count();
    return cpp_amzi_get_a_lead(3);
} // test_get_a_lead
#endif // AMZI_GET_A_LEAD
#endif // 0 AMZI_PLAY
    
    
#if defined(AMZI_PLAY) || defined(AMZI_GET_A_LEAD) // 0
void send_bid_info(void);
char get_a_lead_buf[100];
char *do_a_lead(int who, bool use_passive);

    
    
#undef GET_A_LEAD_VERBOSE
#if !defined(AMZI_GET_A_LEAD)
#pragma message("amzi_get_a_lead is not defined")
#else
char *amzi_get_a_lead(int who) {
    // use the AMZI prolog to get a lead
    // to test call the above function which can set a hand up for this call
    char trump;
    bool use_passive = false;

    send_bid_info(); // added 210408 to replace inline code; sends suits bid naturally to prolog

// VERIFY CARDS FOR WHO IN AMZI PROLOG
    { int total_cards = 0;
    for (int suit=0; suit<4; suit++) {
        int length=0;
        char command[40];
        sprintf(command,"count(%d,%d,X)",who,suit);
        length = int_call_amzi(command,3);
        total_cards += length;
        }

        if (total_cards != 13) {
            sprintf(get_a_lead_buf,"ERR cards do not toal 13 for %d",who);
            DO_STATE(96);
            return get_a_lead_buf;
            }
        }

// VERIFY LEADER IS THE SAME AS WHO
        { char *declarer = get_declarer(); int leader=4;
            switch (declarer[0]) {
                case 'N': case 'n': leader = 1; break;
                case 'E': case 'e': leader = 2; break;
                case 'S': case 's': leader = 3; break;
                case 'W': case 'w': leader = 0; break;
                }
            if (leader != who) {
                if (leader == 4) sprintf(get_a_lead_buf,"ERR get_a_lead:leader could not be determined from declarer");
                else sprintf(get_a_lead_buf,"ERR get a lead:for:%d but leader is:%d",who,leader);
                DO_STATE(95);
                return get_a_lead_buf;
                }
        }
    
// VERIFY trump
    {
        char command[40];
        sprintf(command,"trump(X)");
        trump = int_call_amzi(command,1);
    }
    
// DETERMINE WHICH LEAD TO CALL
    {   char *contract = get_contract();
        if (strlen(contract) == 2 && contract[0] == '6' && contract[1] == 'N')
            use_passive = true;
    }


    TRIGGER(1499,do_log(1,30,"calling: get_a_lead for:%u trump:%u\n",who,trump));
#define USE_PASSIVE
#if defined(USE_PASSIVE)
    strcpy(get_a_lead_buf,do_a_lead(who,use_passive));
#else
    { char command[40];
        sprintf(command,"get_a_lead(%d,X,Y)",who);
    do_log(1,20,command);
    temp = int_str_call_amzi(command,2,3);
    do_log(1,20,temp);
    strcpy(get_a_lead_buf,temp);
    }
#endif
    return get_a_lead_buf;
    }
#endif // 0 AMZI_PLAY IOS
    
#if defined(AMZI_GET_A_LEAD)
    char *do_a_lead(int who, bool use_passive) {
        char *temp;
        char command[40];
        if (!use_passive) sprintf(command,"get_a_lead(%d,X,Y)",who);
        else sprintf(command,"get_a_passive_lead(%d,X,Y)",who);
        do_log(1,20,"calling:%s",command);
        temp = int_str_call_amzi(command,2,3);
        do_log(1,20,"got:%s",temp);
        return temp;
        
    }
    
    extern "C" char *cpp_amzi_get_a_lead(int who) { return amzi_get_a_lead(who); }
#endif
#endif
    
    void do_bid_info(void) {
        ConstraintParser *cp=NULL;
        if (global_b.bid_info == NULL) {
            BidInfo *bi = new BidInfo();
            global_b.bid_info = bi;
            }
        if (global_b.bid_info->constraint_parser == NULL && global_b.constraint_parser == NULL) {
            cp = new ConstraintParser();
            global_b.bid_info->constraint_parser = cp;
            global_b.constraint_parser = cp;
            }
        else if (global_b.bid_info->constraint_parser != NULL) global_b.constraint_parser = global_b.bid_info->constraint_parser;
        else if (global_b.constraint_parser != NULL) global_b.bid_info->constraint_parser = global_b.constraint_parser;
        
        // do_log(1,20,"reverse_transfer");
        global_b.bid_info->constraint_parser->reverse_transfer_constraints();
        // do_log(1,20,"retracting suits_bid");
        global_b.bid_info->set_all_bid_info();
    }
    

#if defined(AMZI_GET_A_LEAD)
void send_bid_info(void) {
        char temp_command[200];
        // do_log(1,20,"reverse_transfer");
        global_b.bid_info->constraint_parser->reverse_transfer_constraints();
        // do_log(1,20,"retracting suits_bid");
        call_amzi("retractall(suits_bid(_,_))");
        global_b.bid_info->set_all_bid_info();
        // do_log(1,30,"going through the suits for the sides");
        for (int i=0; i<2; i++)
            for (int j=0; j<4; j++)
                if (global_b.bid_info->get_bid_info(i,j)) {
                    sprintf(temp_command,"suits_bid(%d,%d)",i,j);
                    do_log(1,20,"sending to prolog:%s",temp_command);
                    assert_amzi(temp_command);
                    }
        DO_STATE(7);
    }
    
    

extern "C" void cpp_send_bid_info(void) { send_bid_info(); }
#endif
    
    int send_constraints(void) {
#if defined(AMZI)  // was AMZI_BID but constraints needed for lead also without bidding for testing
        return global_b.bid_info->constraint_parser->send_id_constraints_to_amzi();
#else
        return 0;
#endif
    }
    
extern "C" void cpp_initialize_constraints() {
    BidInfo *bi = new BidInfo();
    ConstraintParser *cp = new ConstraintParser();
    global_b.bid_info = bi;
    global_b.bid_info->set_constraint_parser(cp);
    }
    
extern "C" void gen32_set_loops(int);
void cpp_set_loops(int number) {
        gen32_set_loops(number);
    }


