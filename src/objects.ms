> "testing hand"
$ north = hand("qt8.j52.jt84.t65")
$ east = hand("a.983.a75.aj8432")
$ south = hand("kj76432.k76.q2.9")
$ west = hand("95.aqt4.k963.kq7")

> "trying north first"

> north
> "calling hprint for: qt8.j52.jt84.t65"
> hprint(north)
> "finished"

/ So a hand will actually be an array of int, 52 in number, usually sorted, with shdc and north east souht west
/ or it could be a array of either 13 cards or 14 cards. Or is could be a structure with the number of cards,
/ and the array.     probably should be an index and the cards are a list  maybe with a list of played cards.
/ or a list of 13 cards that points to list of 52 cards with whether it has been played or not.
/ How about a list of 52 cards with status. So (0,1,2) wouls show no aS, has ks, played qs? that would be fast
/ and easy to process.
/ hand could also be created from a list of card integers.

/ $ test_hand = index("size",13,"count",13,"cards",list(51,43,12,10,8))
/ & test_hand = test_hand.add( 


~ hand (h) 
    $ uh = h.toUpper()
    <- get_cards(uh)
}

~ get_cards (hand)
    $ result = list()
    $ suits = split(hand,".")
    @ suit : list(0,1,2,3)
        $ values =  get_indices(suit,suits.get(suit))
        & result = result.add(values) 
    }
    <- result
}

~ get_indices (suit,suit_str)
    $ values = index("2",0,"3",1,"4",2,"5",3,"6",4,"7",5,"8",6,"9",7,"T",8,"J",9,"Q",10,"K",11,"A",12,"t",8,"j",9,"q",10,"k",11,"a",12)
    $ value = 0
    $ result = list()
    @ ch : suit_str
        & value = values.get(ch)
        & value = value + 13*suit
        * result.add(value)
        }
    <- result
    }

~ get_card(i)
    $ values = index(0,"2",1,"3",2,"4",3,"5",4,"6",5,"7",6,"8",7,"9",8,"T",9,"J",10,"Q",11,"K",12,"A")
    <- values.get(i % 13)
}

~ hprint(h)
    $ result = ""
    $ l1 = list()
    @ suit : list(0,1,2,3)
        & l1 = h.get(suit)
        @ card : l1
            & result = result.add(get_card(card))
            } 
        ? suit < 3
            & result = result.add(".")
            }
        }
    <- result
    }


