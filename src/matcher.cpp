








#if !defined(IOS)
#include "prefix.pch"
#endif
// #define EXCLUDE_BIDINFO
#if defined(OSX) || defined(ANDROID)
extern "C" void set_pybid_loaded(void);
// #pragma message("compiling for OSX")
#include "wchar.h"
#define MYBUF wchar_t
#endif
#if defined(IOS)
// #pragma message("compiling matcher.cpp for IOS")
#else
// #pragma message("not compiling matcher.cpp for IOS")
#endif

// #pragma message("matcher.cpp entered")
#if defined(AMZI)
// #pragma message("amzi is defined before bid20.h")
#endif
#include "bid20.h"
#ifdef AMZI
// #pragma message("AMZI on via bid20.h via hoffer.h")
#endif
#ifndef AMZI_THERE
// #pragma message("AMZI_THERE not defined to import amziios static library")
#endif
#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

// #include "stdafx.h"
#define MATCHER_EXPORTS
#include "stdio.h"
// release #include "assert.h"
#if !defined(linux)
#ifndef BBO_ALERT
#include "windows.h"
#endif
#else
#include <string.h>
#endif
// #define BOOST
#ifdef BOOST
#include <string>
#endif

#include "typedef.hpp"
#define RXTRNL
#include "rx.hpp"
#include "node.hpp"
#include "bidstate.hpp"
// #include "cswitch.h"
#include "bid20.h"
#include "non_exportables.h"
#ifdef AMZI
// #pragma message("including amzi.h in matcher.cpp")
#include "amzi.h"
#endif

#include "bergen.hpp"
#include "constraint_ids.h"

#if !defined(EXCLUDE_BIDINFO)
extern BINFO bid_info[BID_INFO_MAX][4]; // to be used with BestComment for game_force and forcing pass
#endif
extern void free_conventions(void);
extern void clean_up2(BIDNODE *);
extern MatchTree *sys;

// char *get_line_info(int count);
void my_set_sequence(const char *seq);
void test(void);
char *do_association(int command, int dictionary, const char *name, const char *value);



#define GCC_PY_COMMENT
#define ELIMINATE_TESTDLL
#ifdef ELIMINATE_TESTDLL
#define NO_TESTDLL
#else
#define TESTDLL
#endif
#define VERBOSE1

#ifndef HOFFER
struct HINSTANCE__ *hInst;
#endif


#undef ALOGGING
#if defined(IOS)
#undef ALOGGING
#endif

#if defined(ALOGGING)
#define ANDROID_LOGGING_FOR_TRICKY
#ifdef ANDROID_LOGGING_FOR_TRICKY
#include <android/log.h>
#define  LOG_TAG    "testjni"
#define  ALOG(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#endif
#endif


void setAssoc(const char *, const char *, int);
char *getAssoc(const char *,int);
void clearAssoc(const char *,int);
#ifdef BOOST
extern "C" {
__declspec(dllimport) char *do_association(int command, int dictionary, char *key, char *value);
__declspec(dllimport) void initialize_dictionaries(void);
}
#endif

/* extern "C" {
#ifdef BOOST
void test_association(void);
#endif
char *get_constraint(int id,int which);
char *get_range(int id, int which);
char *get_priority(int id, int which);
void clear_value_macros(void);
#ifdef TESTDLL
void showAssociations(void);
char *getAssociation(char *, int);
void setAssociation(char *, char *, int);
void clearAssociation(char *, int);
void APIENTRY dll_mark_gstack(unsigned long *);
void APIENTRY dll_release_gstack(unsigned long);
#endif
void test(void);
void test_malloc_nodestr(void);
void teststr(void);
int test_amzi(void);
int reload_amzi(void);
int retract_who_am_i_amzi(void);
// int retract_constraints_amzi(void);
// int retract_constraints_amzi(void);
int retract_sequence_amzi(void);
// int assert_amzi(char *);
void show_coverage(void);
int bid_amzi(void);
int close_amzi(void);
int read_rules(void);
int reset_read_rules(void);
void verify_rules(void);
int mybid(int);
void mybreak(int);
void my_evaluate_sequence(char *);
void my_recurse_sequence(char *);
char *get_best_generate(int,char *);
char *get_best_comment(int);
int get_best_help_context(void);
char *get_line_info(int);
int get_line_count(int);
void increment_line_count(int);
char *get_comment();
char *get_template(int);
char *get_grab(int);
void set_id(int,int,char *,char *);
void clearbids(void);
char *macro(char *, int);
void autobid(void);
void autosetup(void);
void set_seq(char *);
void my_show_associations(void);
#ifndef BOOST
char *do_association(int,int,char *,char *);
#endif
void my_nextplay(void);
int my_get_play(int);
void my_init_hand(char *);
void my_set_cards(int,int);
#ifdef SEQ
void my_set_declarer(char *);
void my_set_contract(char *);
void my_set_sequence(char *);
char *my_get_sequence(void);
#ifdef PROLOG
void set_contract(char *);
void set_declarer(char *);
void set_cards(int,int);
void nextplay(void);
int get_play(int);
#endif
void init_hand(char *);
void set_sequence(char *);
char *get_sequence(void);
#endif

void set_forced_pass(int,int);
void set_bidder(int,int);
char *get_seq(void);
char *get_declarer(void);
int get_doubled(void);
int get_vul(void);
void set_vul(int,int);
char *get_contract(void);
int get_dealer(void);
void set_dealer(int);
int call_match(char *, char *);
void set_description(int, char *);
char *get_des(int);
}
*/



typedef void (*CB)(void);
typedef void (*SB)(char *);
typedef char *(*AB)(int,int,char *, char *);
typedef char *(*IB)(int);
typedef void (*IIB)(int,int,char *,char *);

// extern "C" {
// char *matcher_version = "Meadowlark Bridge 2.0";
// char *matcher_date = "February 16, 2001";
void set_callback(CB);
void set_strcallback(SB);
void set_assoccallback(AB);
void set_commentcallback(IB);
void set_idcallback(IIB);
// } 


CB my_callback=0;
SB my_strcallback=0;
AB my_assoccallback=0;
IB my_commentcallback=0;
IIB my_idcallback=0;

void set_assoccallback(AB a) {
    my_assoccallback = a;
    }

void set_strcallback(SB s) {
	my_strcallback = s;
}

void set_callback(CB c) {
  my_callback = c;
}

void set_commentcallback(IB i) {
    my_commentcallback = i;
    }

void set_idcallback(IIB i) {
	my_idcallback = i;
}


extern "C" char *gen32_get_description2(int,int);
char *get_description2(int i, int v) {
    return gen32_get_description2(i,v);
}

EXTERN_HOFFER_C void set_template(int index,const char *value) {
    char temp[6];
    sprintf(temp,"%u",index);
    do_association(1,8,temp,value);
    }

EXTERN_HOFFER_C char *get_template(int index) {
    CSW_EXEC(BidMessage("get_template %u\n",index));
    if (index <=0) return (char *)"";
    char temp[11];
    sprintf(temp,"%u",index);
    return (char *)do_association(2,8,temp,"");    
    }



void test_association(void) {
//    std::string n("rod");
//    std::string v("ludwig");
#ifdef BOOST
	initialize_dictionaries();
#endif
	do_association(1,1,"rod","ludwig");
    assert(strcmp("ludwig",do_association(2,1,"rod",""))==0);
    }
#ifndef BOOST
extern char *new_association(int command, int dictionary, const char *name, const char *value);
char *do_association(int command, int dictionary, const char *name, const char *value) {
    // char *ans=NULL;
	return new_association(command,dictionary,name,value);
    // fprintf(stderr,"%s -> %s\n",name,value);

/*    if (my_assoccallback) {
        ans = my_assoccallback(command,dictionary,name,value);
        return ans;
        }
    else {
        fprintf(stderr,"No callback3 registered\n");
        return ans;
        }    */
    }
#endif


#ifndef HOFFER
void python_log(char *msg) { 
	if (my_strcallback) my_strcallback(msg);
	else printf("No callback2 registered\n");
#else
void python_log(char *msg) {
#endif
}

#ifndef HOFFER
void set_id(int id, int priority, char *grabs, char *forths) {
	if (my_idcallback) my_idcallback(id,priority,grabs,forths);
	else printf("No callback5 registered\n");
#else
void set_id(int id, int priority, char *grabs, char *forths) {
#endif
}

void test(void) {
  // printf("Testing the callback function\n");
  if (my_callback) (*my_callback)();
  else printf("No callback registered\n");
}

void test_malloc_nodestr(void) {
    // boost_store("hello");
    }


// extern "C" {
// void do_range(BIDNODE *, int);
// int BidMessage(char *,...);
// }



extern MatchTree *sys;
extern TBidState *BiddingSystem;

int verify_rules(void) {
    int result = sys->Verify();
    SINGLETON(5,BidMessage("System verified"));
    return result;
	}

void attach(void) {
#if !defined(linux)
    int i;
    i=6;
#endif
}

int reset_read_rules(void) {
	SINGLETON(4,BidMessage("going to Close()"));
	sys->Close();
	SINGLETON(4,BidMessage("going to reread rules"));
	sys->Read("mb.sys");
    SINGLETON(4,BidMessage("System read"));
	do_log(1,20,"calling verify rules");
	verify_rules();
	do_log(1,20,"verified rules");
	return 1;
}

#ifdef HOFFER
EXTERN_HOFFER_C int set_first_rule(const char *first_line) {
	return sys->SetFirstRule(first_line);
	}
EXTERN_HOFFER_C int set_next_rule(const char *line) {
	return sys->SetNextRule(line);
}
EXTERN_HOFFER_C int finish_rules(void) {
	return sys->FinishRules();
}
#endif

// #pragma message("before Bergen global_b")
extern Bergen global_b;
#ifdef AMZI
#pragma message("amzi is on")
static bool use_amzi = false;
#ifndef AMZI_THERE // 0
#pragma message("amzi_there is off")
 #ifndef AMZI // 1
#pragma message("amzi not on")
#if defined(IOS)
int initialize_amzi(int setup_flag) { return 0;}
#else
EXTERN_HOFFER_C int initialize_amzi(int setup_flag) { return 0;}
#endif
#pragma message("amzi on")
    
EXTERN_HOFFER_C int assert_amzi(const char *term) {return 0; }
EXTERN_HOFFER_C int retract_points(void) { return 0; }
EXTERN_HOFFER_C int retractall_amzi(void) { return 0; }
#pragma message("amzi is off")
#else // 1 AMZI is on
// #pragma message("checking one")
EXTERN_HOFFER_C char *call_amzi(const char *command);
EXTERN_HOFFER_C char *str_call_amzi(const char *command, int index);

#define AMZI_COMMENT_MAX 100
char amzi_comment[AMZI_COMMENT_MAX+1];

// extern "C" char *get_amzi_comment(void) {
EXTERN_HOFFER_C char *get_amzi_comment(void) {
	int i;
	if (amzi_comment[0] != '\0') 
		{i=3;}
	return amzi_comment;
	}

EXTERN_HOFFER_C char *get_bergen_comment(void) {
	return(global_b.get_bergen_comment());	
	}

EXTERN_HOFFER_C int test_amzi(void) {
	printf("test_amzi\n");
	return 1;
}
// #pragma message("amzi 2")
int amzi_flag=0;
int debugging=0;
ENGid CurEng;
ENGid PlayEng;

#ifdef OSX
// #pragma message("trying to have lsGetExceptMsgW working")
    // extern "C" void  LSAPI lsGetExceptMsgW(ENGid eid, wchar_t* s, int buflen) {}
// void  LSAPI lsGetExceptMsgW(ENGid eid, wchar_t* s, int buflen) {}
// extern "C" void  LSAPI lsGetExceptMsg(ENGid eid, wchar_t* s, int buflen) {}
#endif

    
  #ifdef REMOTE_DEBUGGING  // 2
RC EXPFUNC InitPreds(ENGid eid, void* p);
RC EXPFUNC DebugStart(char *szHostName, int iPortNumber);
  #endif  // 2
    
// static bool use_amzi = false;

    
  #if defined(OSX) || defined(WIN32) // 2
  #pragma message("initialize amzi for non-IOS")
EXTERN_HOFFER_C int initialize_amzi(int setup_flag)
  #else // 2
int initialize_amzi(int setup_flag)
    
#pragma message("initialize amzi for IOS")
  #endif  // 2
// #pragma message("this is back on")
{
   char buf[120];
   RC   rc;   /* LSAPI return code */
   static bool initialized=false;
   static bool debugging = false;
   static bool amzi_play = false;

   if (setup_flag & 1) use_amzi = true;
   if (setup_flag & 2) debugging = true;
   if (setup_flag & 4) amzi_play = true;
   if (debugging) do_log(1,30,"enter initialize_amzi:%u\n",setup_flag);
   if (debugging & amzi_play) do_log(1,30,"amzi_play being used\n");
   if (initialized == true) return(1); // just changing a setting

  #ifdef REMOTE_DEBUGGING  // 2
   char p[1000];
   char szHostName[34];
   int iPortNumber;
  #endif  // 2

    
/* Initialize and load the compiled Prolog program */
   if (use_amzi) {
		if ((rc = lsInit(&CurEng, (MYBUF *) "pybid")))
    {
      printf("Prolog initialization error: %d\n  %s", (int)rc, buf);
      return -1;
   }
   else if (debugging) printf("\nAmzi! pybid initialized\n");
   if (lsLoad(CurEng, (MYBUF *) "pybid")) {
		amzi_error();
		return 0;
		}
   else {
        if (debugging) printf("pybid xpl loaded\n");
		amzi_flag = 1;
       set_pybid_loaded(); // does this in xdeal.c
	   initialized=true;
		str_call_amzi("setup_logging(X)",1);
		if (debugging) printf("amzi debugging is on\n");
		if (debugging) str_call_amzi("debugger(_)",1);
  #ifdef REMOTE_DEBUGGING  // 2
		InitPreds(CurEng, (void *)&p);
		strcpy(szHostName,"localhost");
		iPortNumber=8000;
		DebugStart(szHostName,iPortNumber);
  #endif  // 2
   }
   }
   if (amzi_play) {

   
      if ((rc = lsInit(&PlayEng, (MYBUF *) "pyplay")))
   {
      printf("Prolog initialization error: %d\n  %s", (int)rc, buf);
      return -1;
   }
   else if (debugging) printf("\nAmzi! pyplay initialized\n");
   if (lsLoad(CurEng, (MYBUF *) "pyplay")) {
		amzi_error();
		return 0;
		}
   else {
        if (debugging) printf("pyplay xpl loaded\n");
	}

   
   
   }

   return 1;
}

char amzi_result[255];
// #pragma message("compiling call_amzi")
//extern "C" char *call_amzi(const char *command, int index) {
    
EXTERN_HOFFER_C char *call_amzi(const char *command) {
   TF   tf;   /* LSAPI true/false/err return codes */
   TERM t;    /* a Prolog term */
   amzi_result[0]='\0';

   tf = lsCallStr(CurEng, &t, (MYBUF *) command);

/* If the query succeeded print up the results */

   if (tf)
   {
       amzi_result[0] = 'O'; amzi_result[1]='K'; amzi_result[2]='\0';
      // lsGetArg(CurEng, t, index, cSTR, amzi_result);
   }
   else {
		printf("%s",command);
		printf(" failed\n");
   }
   return amzi_result;
}
    
EXTERN_HOFFER_C char *int_str_call_amzi(const char *command, int i_index, int s_index) {
        TF tf;
        // static CLogicServer oldServer = ls;
        // int rc;
        char stemp[250];
        int itemp=0;
    TERM again_t;
        
        tf = lsCallStr(CurEng,&again_t,(MYBUF *) command);
        if (tf) {
            lsGetArg(CurEng,again_t,s_index, cSTR, stemp);
            lsGetArg(CurEng,again_t,i_index,cINT, (int *) &itemp);
            // fprintf(stderr,"%d %s\n",itemp,stemp);
            sprintf(amzi_result,"%d %s",itemp,stemp);
            }
        else printf("%s failed\n",command);
        return amzi_result;
    }
    
    
EXTERN_HOFFER_C char *str_call_amzi(const char *command, int index) {
        TF tf;
        // static CLogicServer oldServer = ls;
        // int rc;
        amzi_result[0] = '\0';
    TERM again_t;
    
        // tf = lsCallStr(CurEng,&again_t,(char *) command);
        tf = lsCallStr(CurEng,&again_t,(MYBUF *) command);
        if (tf) {
            lsGetArg(CurEng,again_t,index, cSTR, amzi_result);
            // fprintf(stderr,"rc is %d\n",rc);
            }
        else printf("%s failed\n",command);
        return amzi_result;
    }



    


EXTERN_HOFFER_C void show_coverage(void) {
	str_call_amzi("show_coverage(X)",1);
	}

#if !defined(IOS)
EXTERN_HOFFER_C int assert_amzi(const char *term) {
#else
int assert_amzi(const char *term) {
#endif
//   		lsAssertaStr(CurEng,(char *) term);
//		return 0;
   TF   tf;   /* LSAPI true/false/err return codes */
   TERM t;    /* a Prolog term */
   int result=0;
   MYBUF buf[1024];
    
    
    
    
    
  #ifdef TWEAKING  // 2
   return 0;
  #endif  // 2

   /* Build a query term and call it */
    
  /*  #include <wchar.h>
    int swprintf(wchar_t *wcsbuffer, size_t n,
                 const wchar_t *format, argument-list); */
    
   // changed to swprintf 210209
    swprintf(buf,50,L"my_assert(%s,X)",term);
   // fprintf(stderr,"assert_amzi->%s<-\n",buf);
    tf = lsCallStr(CurEng, &t, buf);

	try {
		if (tf)	lsGetArg(CurEng, t, 2, cINT, &result);
		else printf("my_assert failed\n");
		}
	catch(...) { fprintf(stderr,"assert_amzi failed"); }
	return result; 
}

 #endif  // 1 of AMZI

// #pragma message("back on")

EXTERN_HOFFER_C int retract_points(void) {
	// at the start of the descriptions for hands that puts into temp_ values
	// such as temp_starting, temp_supported this is called to remove all temp_ values
   TF   tf;   /* LSAPI true/false/err return codes */
   TERM t;    /* a Prolog term */
   int result=0;
 #ifdef TWEAKING  // 1
   return 0;
 #endif

   /* Build a query term and call it */
    tf = lsCallStr(CurEng, &t, (MYBUF *) "abolish_points(X)");

/* If the query succeeded print up the results */

   if (tf)
   {
      lsGetArg(CurEng, t, 1, cINT, &result);
   }
   else {
		printf("retract_points failed\n");
   }
   return result; // 0 or 1 if it abolish all of len,support,supported,starting.
}


// extern "C" int retractall_amzi(void) {
EXTERN_HOFFER_C int retractall_amzi(void) {
   TF   tf;   /* LSAPI true/false/err return codes */
   TERM t;    /* a Prolog term */
   int result=0;
 #ifdef TWEAKING // 1
   return 0;
 #endif

   /* Build a query term and call it */
    tf = lsCallStr(CurEng, &t, (MYBUF *) "abolish_all(X)");

/* If the query succeeded print up the results */

   if (tf)
   {
      lsGetArg(CurEng, t, 1, cINT, &result);
   }
   else {
		printf("retractall_amzi failed\n");
   }
   return result; // 0 or 1 if it abolish all the bridge facts.
}



EXTERN_HOFFER_C int retract_description(int dir) {
   TF   tf;   /* LSAPI true/false/err return codes */
   TERM t;    /* a Prolog term */
	char buf[250];
   int result=0;

   /* Build a query term and call it */
   sprintf(buf,"retract_description(X,%d)",dir);
    tf = lsCallStr(CurEng, &t, (MYBUF *) buf);

/* If the query succeeded print up the results */

   if (tf)
   {
      lsGetArg(CurEng, t, 1, cINT, &result);
   }
   else {
		printf("retract_description failed\n");
   }
   return result; // 0 or 1 if it retracts the single description.
}

 #ifdef IOS  // 1
 #pragma message("reload amzi in IOS")
 #endif
EXTERN_HOFFER_C int reload_amzi(void)
{
   char buf[120];
   RC   rc;   /* LSAPI return code */

/* Reload the compiled Prolog program */
   printf("reloading pybid\n");

   if ((rc = lsInit(&CurEng, (MYBUF *) "pybid")))
   {
      printf("Prolog initialization error: %d\n  %s", (int)rc, buf);
      return -1;
   }

   if (lsLoad(CurEng, (MYBUF *) "pybid")) {
      amzi_error();
		return 0;
	}
   else {
      printf("pybid xpl reloaded\n\n");
	  str_call_amzi("test(X)",1);
	  return 1;
	}	
}
 

// extern "C" int close_amzi() {
EXTERN_HOFFER_C int close_amzi(void) {
   lsClose(CurEng);
   printf("Prolog successfully completed\n");
   return 0;
}
// #pragma message("and here too")
#endif // 1 AMZI_THERE
// #pragma message("and here")
#endif // 0 AMZI
// #pragma message("back on way down here")

EXTERN_HOFFER_C int read_rules(void) {
	static int rules_read=0;

	if (rules_read == 0) {

		SINGLETON(4,BidMessage("going to read the bidding system in data\\mb.sys"));

		do_log(1,20,"calling sys->Read %s","mb.sys");
		sys->Read("mb.sys");
        SINGLETON(4,BidMessage("System read"));
		do_log(1,20,"calling verify rules");
		verify_rules();
		do_log(1,20,"return, verified");
		rules_read = 1;
		}
    ONE_SHOT(7,check_one_shots(4,6));    
    return rules_read;
}

#ifdef TORONTO
extern char toronto_msg[200];
#endif
char *get_comment(void) {
#ifdef TORONTO
    return toronto_msg;
#else
    return (char *) "nothing";
#endif
}

extern int bidding_flag;
extern int print_stack_top;

EXTERN_HOFFER_C void mybreak(int id) {
#if !defined(linux)
    int i;
	i=id;
#endif
}

#ifdef SPLIT_NEXT_BID
int get_bid_val(char *);
#endif

// extern "C"
int retract_constraints_amzi();
char *constraint_command(int who, int what, int where, int low, int hi);
int add_constraint(int who, int what, int where, int low, int hi);
int set_hand(const char *,int);
char contract[3];
int doubled;
char store_sequence[250];

extern void clear_constraints(int);
int pass_or_double=0;
#define ID_CLEAR_CONSTRAINTS 99
// #define ID_GET_COUNT 89
// #define ID_PASS_OR_DOUBLE 88
    
EXTERN_HOFFER_C int mybid2(int base, char *hand) {
	// the hand is needed for bergen simulation; a simulation will send its hand as constraints and block its own bidding constraints
	if (hand != NULL)
		try { global_b.store_given_hand(hand); }
		catch (...) { fprintf(stderr,"no proper hand given to mybid2"); }
	return mybid(base);
	}

EXTERN_HOFFER_C int mybid(int base) {
	int result = -1, dealer, engine_result=0;
#if defined(AMZI_BID)
    int amzi_result=0;
    use_amzi = true;
#endif
    int declarer,bidder,contract,doubled;

#ifdef QUIT_DEBUGGING
	bool quit_debugging = false;
#endif
	char bid[3], *sequence;
	Score the_score;
	Bergen *b= &global_b;

	b->set_line_no(0);
	int vul=0;

#if defined(AMZI_BID)
	char buf[1024];
#endif


	bid[1]='\0'; bid[2]='\0';
	bidding_flag=1;
	TRIGGER(50,printf("mybid\n"));
#ifdef QUIT_DEBUGGING
	if (quit_debugging) printf("enter mybid;");
#endif
	if (1) { // assert information needed for AMZI bid
#if defined(AMZI_BID)
        if (use_amzi) {
            TRIGGER(ID_BID_AMZI,do_log(1,20,"amzi:retract_sequence_amzi"));
            retract_sequence_amzi();
            }
#endif
		sequence = BiddingSystem->GetSequence();
		strcpy(store_sequence,sequence);
#ifdef QUIT_DEBUGGING
		if (quit_debugging) fprintf(stderr,"MYBID:seq:%s\n",sequence);
		if (quit_debugging) fprintf(stderr,"MYBID:base:%u\n",base);
		// printf("mybid:%s\n",sequence);
#endif
#if defined(AMZI_BID)
		if (use_amzi) {
			sprintf(buf,"sequence('%s')",sequence);
            TRIGGER(ID_BID_AMZI,do_log(1,20,buf));
			assert_amzi(buf);
			if (strlen(sequence)==0) 
				retract_constraints_amzi();
			}
#endif
		dealer = BiddingSystem->GetDealer();
        declarer = BiddingSystem->GetDeclarer();
		bidder = BiddingSystem->GetBidder();
#if defined(AMZI_BID)
		if (use_amzi) {
		retract_who_am_i_amzi();
		sprintf(buf,"who_am_i(%u)",(dealer+bidder)%4);
        TRIGGER(ID_BID_AMZI,do_log(1,20,buf));
		assert_amzi(buf);
		}
#endif
		vul = BiddingSystem->GetVul(bidder % 2);
        contract = BiddingSystem->GetCurrentContract();
        doubled = BiddingSystem->GetDoubledStatus();
		}

#ifdef SPLIT_NEXT_BID
	try {
#ifdef QUIT_DEBUGGING
    if (quit_debugging) printf("calling NextBid1");
#endif
		result = sys->NextBid1(); // get bid from bidding engine
#ifdef QUIT_DEBUGGING
    if (quit_debugging) printf("returned from NextBid1; result is %d\n",result);
#endif
        if (result == DONE_BIDDING) {
#ifdef QUIT_DEBUGGING
            if (quit_debugging) printf("returned at DONE_BIDDING");
#endif
			return 0;
            }
		}
	catch (...) { fprintf(stderr,"NextBid1 failed"); }
#else
    result = sys->NextBid();
#endif

	// bidding_flag = 0; This might need to be uncommented for AMZI bidding
	assert(result >= 0); // returns -1 on failure

#ifdef BID20_DEBUGGING
    if (bid20_debugging) fprintf(stderr,"MYBID:result:%d\n",result):
#endif
	if (print_stack_top != -1 && result < 75) { // a valid bid (that can, however, be limited) was determined in engine
		engine_result = print_stack[print_stack_top].node->id;
#ifdef BID20_DEBUGGING
		if (bid20_debugging) fprintf(stderr,"MYBID:engine:%u\n",engine_result);
#endif
#ifdef QUIT_DEBUGGING
		if (quit_debugging) fprintf(stderr,"engine_result:%d\n",engine_result);
#endif
		increment_line_count(engine_result); // for code tracking
#ifdef QUIT_DEBUGGING
		if (quit_debugging) fprintf(stderr,"sending %d to NextBid2\n",result);
#endif
		try {sys->NextBid2(result); }// ok to process now that bid is obtained and not a pass
		catch (...) { printf("caught exception at nextbid2;"); }
#ifdef QUIT_DEBUGGING
		if (quit_debugging) fprintf(stderr,"return after NextBid2 %d",engine_result);
#endif
		global_b.set_line_no(engine_result);
		return engine_result; // the line number of the bid found
	}
	
#if !defined(AMZI_BID)
	else { sys->NextBid2(0); return 0; }
#else
	else { // now do the amzi bid as it might find a bid.
#ifdef SPLIT_NEXT_BID
		bid[0] = '\0';
		try {
		if (use_amzi) {
#ifdef QUIT_DEBUGGING
			if (quit_debugging) printf("calling bid_amzi;");
#endif
            TRIGGER(ID_BID_AMZI,do_log(1,30,"calling bid_amzi;"));
        result = bid_amzi();
#ifdef QUIT_DEBUGGING
			if (quit_debugging && (result == -1)) fprintf(stderr,"bid_amzi returned -1\n");
			if (quit_debugging) printf("back from bid_amzi;\n");
#endif
            TRIGGER(ID_BID_AMZI,do_log(1,30,"bid_amzi returns:%d",result));
		}
		else result = 0;
		}
		catch (...) { fprintf(stderr,"bid_amzi failed\n"); }
#endif
		if (result >= 0) {
			if (result == 0) bid[0]='P';
			else if (result == 1) bid[0]='X';
			else if (result == 2) bid[0]='R';
			else {
				bid[0] = result/10 + '0';
				if (result % 10 ==0) bid[1]='C';
				else if (result % 10 == 1) bid[1]='D';
				else if (result % 10 == 2) bid[1]='H';
				else if (result % 10 == 3) bid[1]='S';
				else if (result % 10 == 4) bid[1]='N';
				else {
					fprintf(stderr,"result does not translate, return 0 not -1\n");
					return 0;
					}
				}
			}
		else {
#ifdef QUIT_DEBUGGING
			if (quit_debugging) fprintf(stderr,"result is < 0 at %d; return 0",result);
#endif
			bid[0]='\0';
			}
		if (bid[0]!='\0') {  // found a bid from AMZI
#ifdef QUIT_DEBUGGING
			if (quit_debugging) printf("mybid:amzi:bid:%s\n",bid);
#endif
            TRIGGER(ID_BID_AMZI,do_log(1,20,"amzi:bid:%s",bid));
			amzi_result = get_bid_val(bid);
            TRIGGER(ID_BID_AMZI,do_log(1,20,"amzi:bid:%d sent to sys->NextBid2",amzi_result));
			sys->NextBid2(amzi_result);
            TRIGGER(ID_BID_AMZI,do_log(1,20,"returning 1 so there is a value for amzi"));
			return 1; // changed to 1 so there is a value for amzi 	
			}
		else { // go back and use the result from the engine 
#ifdef QUIT_DEBUGGING
			if (quit_debugging) printf("mybid:engine:bid:%d\n",engine_result); 
#endif
            TRIGGER(ID_BID_AMZI,do_log(1,30,"use result from the engine:%d",engine_result));
			try { sys->NextBid2(engine_result); }
			catch (...) { printf("exception caught"); }
#ifdef QUIT_DEBUGGING
			if (quit_debugging) printf("mybid:engine:bid:NextBid2 done, returning\n"); 
#endif
            TRIGGER(ID_BID_AMZI,do_log(1,30,"returning 2 from mybid"));
			return 2;
		}
	}
#endif
#ifdef QUIT_DEBUGGING
	if (quit_debugging) fprintf(stderr,"returning zero; result is %d",result);
#endif
	return 0;
}



#define COMMENT_LENGTH 240
char best_temp[COMMENT_LENGTH];

// 190928 was returning temp, a local, fixed
EXTERN_HOFFER_C char *get_best_comment(int dir) {
#ifdef OVERLY_VERBOSE
    fprintf(stderr,"Calling BestComment\n");
#endif
#ifndef REMOVE_THEM
    char temp[250];
    strcpy(temp,BestComment(dir));
    strcpy(best_temp,temp);
#else
    strcpy(best_temp,BestComment(dir));
#endif
    CSW_EXEC(do_log(1,10,"get_best_comment <%s>",best_temp));
#ifdef OVERLY_VERBOSE
    fprintf(stderr,"get_best_comment returns:<%s>\n",best_temp);
#endif

	return best_temp;
	}

// added sending sequence only for debugging purposes 190929
char best_temp_generate[COMMENT_LENGTH];
char *get_best_generate(int dir,char *seq) {
    CSW_EXEC(BidMessage("get_best_generate entered\n"));
    strcpy(best_temp_generate,BestGenerate(dir));
    CSW_EXEC(do_log(1,10,"get_best_generate <%s>",best_temp_generate));
	return best_temp_generate;
}

int BestHelp(void);
int get_best_help_context(void) {
	return BestHelp();
	}

extern int bid_evaluate();
EXTERN_HOFFER_C void my_evaluate_sequence(const char *seq) {
    int result = 0;
    ONE_SHOT(200,do_log(1,10,"my_evaluate_sequence:"));
    ONE_SHOT(200,do_log(1,10,"my_evaluate_sequence: %s",seq));
    CSW_EXEC(BidMessage("my_evaluate_sequence: %s",seq));
	my_set_sequence(seq);
#if !defined(EXCLUDE_BIDINFO)
	reset_all_bid_info();
#endif
//	assert(sys->GetRoot() != NULL);
	ONE_SHOT(200,do_log(1,10,"calling bid_evaluate"));

	result = bid_evaluate();
    // result = -2; this would create an exception...so used for testing purposes
    // if an exception occurs during either of the the do_constraints2 calls -1 or -2 is returned
        if (result != 0) fprintf(stderr,"bid_evaluate fails in my_evaluate_sequence\n");
    if (result != 0) throw std::invalid_argument( "bad sequence evaluation" );
}

extern void bid_recurse();
void my_recurse_sequence(const char *seq) {
	CSW_EXEC(BidMessage("my_recurse_sequence: %s; ",seq));
	my_set_sequence(seq);
	CSW_EXEC(BidMessage("call bid_recurse\n"));
	bid_recurse();
}

void autosetup(void) {
	sys->AutoSetup();
}

EXTERN_HOFFER_C void set_seq(const char *seq) {
	BiddingSystem->SetSequence(seq);
}

void set_forced_pass(int dir,int val) {
    BiddingSystem->SetNullBidState(dir,val);
}

void autobid(void) {
	sys->AutoBid();
}



#ifdef SEQ
#ifdef PROLOG

void my_nextplay(void) {
	nextplay();
}

int my_get_play(int hand) {
	int card;
	card = get_play(hand);
	return card;
}
void my_init_hand(char *h) {
	init_hand(h);
}
#else
void my_nextplay(void) {}
int my_get_play(int hand) {return 53;}
void my_init_hand(const char *h) {}
char *my_get_sequence(void) { return BiddingSystem->GetSequence(); }
#endif





#ifdef PROLOG
void my_set_cards (int h, int c) {
	set_cards(h,c);
}

void my_set_declarer(char *d) {
    set_declarer(d);
    }
    
void my_set_contract(char *c) {
    set_contract(c);
    }

#else
void my_set_cards (int h, int c) {}
void my_set_declarer(const char *d) {}
void my_set_contract(const char *c) {}
#endif

#endif // of SEQ

// #pragma message("compiling my_set_sequence")
void my_set_sequence(const char *seq) {
    BiddingSystem->SetSequence(seq);
    }

void domacro(const char *,int);
EXTERN_HOFFER_C char *macro(const char *cmd, int section) {
	domacro(cmd,section);
	return macro_temp;
}

EXTERN_HOFFER_C void clearbids(void) {
	BiddingSystem->SetDoneBidding(0);
	BiddingSystem->ClearAllBids();
}

EXTERN_HOFFER_C char *get_seq(void) {
	return BiddingSystem->GetSequence();
}
    
void set_declarer(int declarer) {
        BiddingSystem->SetDeclarer(declarer);
    }

char *get_declarer(void) {
	return BiddingSystem->GetDeclarerStr();
}

int get_bidder(void) {
        // GetBidder is actually the seat
        int bidder;
        bidder = (BiddingSystem->GetDealer() + BiddingSystem->GetBidder()) % 4;
        CSW_EXEC(BidMessage("get_bidder returning %u",bidder));
        return bidder;
    }



int get_vul(void) {
	return (BiddingSystem->GetVul(0) + 2 * BiddingSystem->GetVul(1));
}

EXTERN_HOFFER_C void set_vul(int dir, int value) {
	BiddingSystem->SetVul(dir,value);
}


int get_doubled(void) {
	return BiddingSystem->GetDoubledStatus();
}

char *get_contract(void) {
	return BiddingSystem->GetCurrentContractStr();
}


char grab_result[2];
char *get_grab(int i) {
	grab_result[0] = result[i].c;
	grab_result[1] = '\0';
	return (char *) grab_result;
}
    
void cpp_set_dealer(int dealer) {set_dealer(dealer); }

EXTERN_HOFFER_C void set_dealer(int dealer) {
	BiddingSystem->SetDealer(dealer);
}

EXTERN_HOFFER_C int get_dealer(void) {
    CSW_EXEC(BidMessage("get_dealer returning %u",BiddingSystem->GetDealer()));
	return BiddingSystem->GetDealer();
}

extern int mymatch(const char *, const char *);
int call_match(const char *pat, const char *str) {
	return mymatch(pat,str);
}

void showAssoc();
void my_show_associations(void) {
	showAssoc();
}

EXTERN_HOFFER_C void set_description(int i,const char *desc) {
	sys->SetDescription(i,desc);
}

char *get_des(int i) {
	char  *temp;
	temp = sys->GetDescription(i);
	return temp;
}
// #define VERBOSE1

char atemp[240];
char *getAssoc(const char *name, int dict) {
    atemp[0] = '\0';
    // CSW_EXEC(do_log(1,10,"getAssoc called with %s and %u\n",name,dict));
#ifndef TESTDLL
	strcpy(atemp,do_association(2,dict,name,""));
    return atemp;

#else
    fprintf(stderr,"NOT BOOST");
	unsigned long stack_mark;
	dll_mark_gstack(&stack_mark);
	atemp = getAssociation(name,dict);
	dll_release_gstack(stack_mark);
#ifdef VERBOSE1
	printf("%s\n",atemp);
#endif
    return atemp;
#endif
}

void setAssoc(const char *name, const char *value, int dict) {
    // static int association_setup_flag = 0;

#ifdef BOOST
	if (association_setup_flag == 0) {
        initialize_dictionaries();
        association_setup_flag = 1;
        }
#endif
#ifdef VERBOSE1
	// printf("set: %s %s %u\n",name,value,dict);
#endif
#ifdef TESTDLL
    printf("testdll");
	if (strcmp(name,value) != 0)
	  setAssociation(name,value,dict);
#else
  //  printf("do_association");
	if (strcmp(name,value) != 0) {

//     std::string n(name);
//    std::string v(value);
   // initialize_dictionaries();
    do_association(1,dict,name,value);
  //  fprintf(stderr,"%s was in %u\n",do_association(2,dict,n,std::string("")).c_str(),dict);

	 // do_association(1,dict,name,value);
     }

#endif
}

void clearAssoc(const char *name, int dict) {
    ONE_SHOT(206,do_log(1,10,"clearAssoc(OS): %s %u\n",name,dict));
#ifdef TESTDLL
#pragma error "deprecated"
	clearAssociation(name,dict);
#else
//    std::string n(name);
//    std::string v("");
    do_association(3,dict,name,"");
#endif
}

void showAssoc(void) {
#ifdef TESTDLL
	showAssociations();
#endif
}
// BestComment is a function that uses the information from the bidding system 
// and the current bid to find a good comment. The comment is the last part of 
// each line in the bidding system. That is a separate file that is read in, 
// mb.sys. To find a comment look to the current bid, if there is no comment 
// there then go to the parent of this bid and see if there is a comment 
// there. If a comment is found it is shown during the bidding of the hand. 
// The comment is a macro, and then is run through the macro processor. It can 
// use the grabs from the bidding.

#define SIMPLE_COMMENT
#define COMMENT_LENGTH 240
#define COMMENT_LIMIT (COMMENT_LENGTH - 1)
extern int print_stack_top;
void add_line_no(char * str);
int get_line_no(BIDNODE *nptr,BIDNODE *in, int *found);
struct line_info *lines;
// #define NUM_OF_LINES 15000



char *BestComment(int dir) {

    char forth_string[60];
    int i,j,k,val1,val2,com=0,index;
    int hi=40,low=0,bidder=0;
    BIDNODE *nptr=NULL;
	char ch_upper;
    char temp2[COMMENT_LENGTH],temp3[120];
	int count,suits,in_macro;

	CSW_EXEC(BidMessage("BestComment entered\n"));
#ifdef OVERLY_VERBOSE
    fprintf(stderr,"Best Comment entered\n");
#endif
    com = 0; index=print_stack_top;
	best_temp[0]='\0';

/* 2003-3-5 this was changed to the section below. This was changed
so that the calling python program can run each individual bid and
collect its information. The main reason can be seen in the following
sequence 1H:X:2H. When that sequence is evaluated it simply ignores the
double in its evaluation. So, the evaluation must proceed by sending each
individual bid and its sequence. 

Now the final result is needed and not all so the value is obtained,
the value macros are reset and the final value reapplied
*/

    clear_value_macros();
#ifdef OVERLY_VERBOSE
    fprintf(stderr,"clear_value_macros completed\n");
#endif
    { int temp_bidder,temp_depth;
	sprintf(forth_string,"69H"); // added so this is set back to original value
	temp_bidder = f_eval(forth_string);
	sprintf(forth_string,"67H");
	temp_depth = f_eval(forth_string);
	for (i=0; i<=print_stack_top; i++) {
        bidder = print_stack[i].bidder;
	   sprintf(forth_string,"%2u 67G",i);
        f_eval(forth_string); // set current depth into forth variable for introspecition of prior bids
        sprintf(forth_string,"%u 69G",bidder);
        f_eval(forth_string);
		do_range(print_stack[i].node,bidder);
        sprintf(forth_string,"%u bidder %u",i,bidder);
        ONE_SHOT(500,BidMessage("BestComment: %s",forth_string));
        }
	// check for ok 180726
	sprintf(forth_string,"%d 67G",temp_depth);
	f_eval(forth_string);
	sprintf(forth_string,"%d 69G",temp_bidder);
	f_eval(forth_string);
	}

   if (bidder >=0 && bidder < 4 && print_stack_top >= 0) {
    sprintf(forth_string,"%uH",bidder*2+70);
    CSW_EXEC(BidMessage(forth_string));
    hi = f_eval(forth_string);
    sprintf(forth_string,"%uH",bidder*2+71);
    CSW_EXEC(BidMessage(forth_string));
    low = f_eval(forth_string);        }
    else {
   sprintf(forth_string,"%d bad",bidder);
   CSW_EXEC(BidMessage(forth_string));
   }


/*
    sprintf(forth_string,"%uH",((bidder+1)%4)*2+70);
    CSW_EXEC(BidMessage(forth_string));
    hi2 = f_eval(forth_string);
    sprintf(forth_string,"%uH",((bidder+1)%4)*2+71);
    CSW_EXEC(BidMessage(forth_string));
    low2 = f_eval(forth_string);
*/
    sprintf(forth_string,"%u %u, bidder was %u",hi,low,bidder);

    CSW_EXEC(BidMessage(forth_string));
#ifdef OVERLY_VERBOSE
    fprintf(stderr,"forths evaluated; print_stack_top is %d\n",print_stack_top);
#endif
/*
    clear_value_macros();

    sprintf(forth_string,"%u %uG",hi,bidder*2+70);
    CSW_EXEC(BidMessage(forth_string));
    f_eval(forth_string);
    sprintf(forth_string,"%u %uG",low,bidder*2+71);
    CSW_EXEC(BidMessage(forth_string));
    f_eval(forth_string);

*/

      CSW_EXEC(BidMessage("Calling print_stack_top\n"));
   if (print_stack_top < 0) return (char *) "";

	if (print_stack_top >= 0)
		{
#ifdef OVERLY_VERBOSE
            fprintf(stderr,"print_stack_top is %d\n",print_stack_top);
#endif
#ifdef SIMPLE_COMMENT
		nptr = print_stack[index--].node;
		if (nptr->comment != 0) com=1;
#else
		while (com == 0 && index >= 0)
			{
			nptr = print_stack[index--].node;
#ifdef NEW_COMMENT
			if (nptr->comment != 0)
#else
			if (nptr->comment && nptr->comment[0] != '\0')
#endif
				com=1;
			while (nptr->parent != NULL && com == 0)
				{
				nptr = nptr->parent;
#ifdef NEW_COMMENT
				if (nptr->comment != 0)
#else
				if (nptr->comment && nptr->comment[0] != '\0')
#endif
					com=1;
				}
			}
#endif
		}
#ifdef OVERLY_VERBOSE
    fprintf(stderr,"com is %d",com);
    fprintf(stderr,"best_temp is <%s>\n",best_temp);
#endif
    if (com == 0) return best_temp;
	if (com != 0) {
		CSW_EXEC(BidMessage("com is not nothing\n"));
#ifdef OVERLY_VERBOSE
        fprintf(stderr,"com is not nothing\n");
#endif
        i=0; j=0; best_temp[0]='\0';
#ifdef NEW_COMMENT
		temp2[0]='\0';
		if (nptr->comment)
#ifndef GCC_PY_COMMENT        
            CSW_EXEC(BidMessage("calling loadstring\n"));
			 if (!LoadString((struct HINSTANCE__ *)hInst,nptr->comment,temp2,119)) {
                printf("Comment number %u",nptr->comment);
             	// MessageBox(GetFocus(),temp2,"LoadString failed",MB_TASKMODAL);
				temp2[0]='\0';
				}
#else
            CSW_EXEC(BidMessage("calling get_template with %u\n",nptr->comment));
#ifdef OVERLY_VERBOSE
        fprintf(stderr,"calling get_template with %u\n",nptr->comment);
#endif
        strncpy(temp2,get_template(nptr->comment),COMMENT_LIMIT);
#ifdef OVERLY_VERBOSE
        fprintf(stderr,"get_template returned <%s>\n",temp2);
#endif
        CSW_EXEC(BidMessage("returned %s",temp2));
            if (strcmp(temp2,"") == 0) {
#ifdef OVERLY_VERBOSE
                printf("Comment number %u",nptr->comment);
#endif
                // MessageBox(GetFocus(),temp2,"LoadString failed",MB_TASKMODAL);
				temp2[0]='\0';
				assert (1==2);
				}

#endif			 
#else
#ifdef OVERLY_VERBOSE
        fprintf(stderr,"copy <%s> to temp2\n",nptr->comment);
#endif
        strncpy(temp2,(char *)nptr->comment,COMMENT_LIMIT);
#endif
    CSW_EXEC(BidMessage("temp2 is %s",temp2));
#ifdef OVERLY_VERBOSE
        fprintf(stderr,"temp2 is <%s>\n",temp2);
#endif
        while  (temp2[i]) {
        	val1 = 0;
/*			if (temp2[i] == '_') { temp[j++]=' '; i++; }
			else */
			if (temp2[i]=='#') {
				val1 = temp2[i+1] - 'a';
#ifndef VERSION2
                val2 = result[val1].c;
#else
				val2 = result[val1];
#endif
                temp3[0]='\0';
/* this is such a reckless kludge, I hate to document it.
However, to see temporary grabs I have allowed the 98th forth
variable to be set in macros with the M98G forth phrase that
duplicates the top of the stack and stores it in that variable.
Now, I can print that by accessing the #z grab. Awful.
*/
				if (val1==25) { char temp4[100];
					    sprintf(forth_string,"98H");
					    hi = f_eval(forth_string);
						sprintf(temp4,"z:%d ",hi);
	sprintf(forth_string,"TOP");
	domacro((char  *)forth_string,dir+3);
	sprintf(forth_string,"T:%u ",f_eval(macro_temp));
	strcat(temp4,forth_string);
	sprintf(forth_string,"BOTTOM");
	domacro((char  *)forth_string,dir+3);
	sprintf(forth_string,"B:%u",f_eval(macro_temp));
	strcat(temp4,forth_string);
	strcpy(temp3,temp4);
				}
				else switch (val2) {
#ifdef PRODUCTION_VERSION
					case 'C': strcpy(temp3,"CLUB"); break;
					case 'D': strcpy(temp3,"DIAMOND"); break;
					case 'H': strcpy(temp3,"HEART"); break;
					case 'S': strcpy(temp3,"SPADE"); break;
					case 'N': strcpy(temp3,"NOTRUMP"); break;
					case '1': strcpy(temp3,"ONE"); break;
					case '2': strcpy(temp3,"TWO"); break;
					case '3': strcpy(temp3,"THREE"); break;
					case '4': strcpy(temp3,"FOUR"); break;
					case '5': strcpy(temp3,"FIVE"); break;
					case '6': strcpy(temp3,"SIX"); break;
					case '7': strcpy(temp3,"SEVEN"); break;
#else
					case 'C': strcpy(temp3,"C"); break;
					case 'D': strcpy(temp3,"D"); break;
					case 'H': strcpy(temp3,"H"); break;
					case 'S': strcpy(temp3,"S"); break;
					case 'N': strcpy(temp3,"N"); break;

					case '0': strcpy(temp3,"0"); break;
					case '1': strcpy(temp3,"1"); break;
					case '2': strcpy(temp3,"2"); break;
					case '3': strcpy(temp3,"3"); break;
					case '4': strcpy(temp3,"4"); break;
					case '5': strcpy(temp3,"5"); break;
					case '6': strcpy(temp3,"6"); break;
					case '7': strcpy(temp3,"7"); break;
					case '8': strcpy(temp3,"8"); break;
					case '9': strcpy(temp3,"9"); break;
					case 'P': strcpy(temp3,"P"); break;
					case 'X': strcpy(temp3,"X"); break;
					case 'R': strcpy(temp3,"R"); break;
#endif
					default : temp3[0]='#'; temp3[1]=temp2[i+1]; temp3[2]='\0'; break;
					}
                k=0;
				while (temp3[k]) best_temp[j++]=temp3[k++];
				i+=2;
				}
            else best_temp[j++]=temp2[i++];
			}
        best_temp[j]='\0';
        best_temp[COMMENT_LIMIT]='\0';
#ifdef OVERLY_VERBOSE
        fprintf(stderr,"copying best_temp:<%s>\n",best_temp);
#endif
        strncpy(temp2,best_temp,COMMENT_LIMIT+1);
        i=0;j=0;
        in_macro = 0;
        suits = 0;
        for  (i=0; temp2[i]; i++) {
                ch_upper = toupper(temp2[i]);
                if (in_macro && ch_upper == 'C') suits |= 1;
                else if (in_macro && ch_upper == 'D') suits |= 2;
                else if (in_macro && ch_upper == 'H') suits |= 4;
                else if (in_macro && ch_upper == 'S') suits |= 8;
                else if (in_macro && temp2[i] == '1') {
                        if ((suits & 1) == 0) best_temp[j++] = 'C';
                        else if ((suits & 2) == 0) best_temp[j++] = 'D';
                        else if ((suits & 4) == 0) best_temp[j++]= 'H';
                        else best_temp[j++] = 'S';
                        in_macro = 0;
                        }
                else if (in_macro && temp2[i] == '2') {
                       count = 0;
                       if ((suits & 1) == 0) count++;
                       if ((suits & 2) == 0) count++;
                       if (count == 2) { best_temp[j++] = 'D'; in_macro = 0; continue; }
                       if ((suits & 4) == 0) count ++;
                       if (count == 2) { best_temp[j++] = 'H'; in_macro=0; continue; }
                       else { best_temp[j++] = 'S'; in_macro = 0; continue; }
                       }
                else if (in_macro) { in_macro = 0; }

                else if (temp2[i] == '^')  { in_macro = 1; suits = 0; }
                else best_temp[j++] = temp2[i];
                }
        best_temp[j]='\0';

		}


#ifndef DONT_DO_MACROS_VERSION
#define SKIP_BOTTOM_TOP
#ifndef SKIP_BOTTOM_TOP
#ifndef PRODUCTION_VERSION 
	strcpy(temp2," @ifelse(MYBOTTOM,/MYBOTTOM\\,,[MYBOTTOM/,\\MYTOP])");
#ifdef OVERLY_VERBOSE
    fprintf(stderr,"calling domacro with test2:<%s>\n",temp2);
#endif
	domacro((char  *)temp2,dir+3);
	strcat(best_temp,macro_temp);

	strcpy(temp2," @ifelse(BOTTOM,/BOTTOM\\,,[BOTTOM/,\\TOP])");

	domacro((char  *)temp2,dir+3);
	strcat(best_temp,macro_temp);
	strcat(best_temp," [ MYBOTTOM MYTOP BOTTOM TOP ]");	 
#endif
#endif
    CSW_EXEC(BidMessage("BEST COMMENT <%s>\n",best_temp));
#ifdef OVERLY_VERBOSE
    if (best_temp[0] != '\0') fprintf(stderr,"domacro on:<%s>\n",best_temp);
#endif
    if (best_temp[0] != '\0') {
#ifdef OVERLY_VERBOSE
        fprintf(stderr,"calling domacro\n");
#endif
        domacro((char  *)best_temp,dir+3);
        }
    else macro_temp[0]='\0';
#ifndef PRODUCTION_VERSION
    if (macro_temp[0] != '\0') {
#ifdef OVERLY_VERBOSE
        fprintf(stderr,"calling get_line_info(1) from BestComment");
#endif
        strcat(macro_temp,get_line_info(1));
        }
#endif
#if !defined(EXCLUDE_BIDINFO)
	{ int round, seat;
	get_current_round_and_seat(print_stack_top,&round,&seat);
	if (bid_info[round][seat].game_force != 48)
		strcat(macro_temp," (GF)");
	if (bid_info[round][seat].forcing_pass != 48)
		strcat(macro_temp," (FP)");
	}
#endif
#ifdef OVERLY_VERBOSE
    fprintf(stderr,"BestComment returning:<%s>",macro_temp);
#endif
    return (char *) macro_temp;

#else   // NO_MACROS_VERSION
    ONE_SHOT(503,BidMessage("OS: end of best comment: %s",best_temp));
    return best_temp;
#endif  // DONT_DO_MACROS_VERSION
}


#ifdef AMZI
#define AMZI_BUF_MAX 200
#define INITIAL_STATE 99
int generate_amzi(int who, char *generate) {
// 0,1,12,13,14 16,17,18,19,20,23,24,25,26,27,31,32,33,44
    char outbuf[AMZI_BUF_MAX],buf[AMZI_BUF_MAX];
	char amzi_generate[AMZI_BUF_MAX];
	int i,max=(int) strlen(generate);
	if (max==0) return 0; // otherwise it will process a single space
    int in_mode = INITIAL_STATE, in_lo= true, in_hi=false, skip = false, suit=0,op=0;
	int lo = 0,	hi = 0;
	sprintf(amzi_generate,"%s",generate);
	for (i=0; i<max+1 && i<AMZI_BUF_MAX; i++) {
        if (skip) { skip = false; continue; }
		if ((in_mode == INITIAL_STATE && amzi_generate[i]=='\0')) break;
        if (op>0) switch (amzi_generate[i]) {
            case 'C': { sprintf(outbuf,"%u,16,%u,%u,0",who,op,suit); op=0; goto print; }
            case 'D': { sprintf(outbuf,"%u,16,%u,%u,1",who,op,suit); op=0; goto print; }
            case 'H': { sprintf(outbuf,"%u,16,%u,%u,2",who,op,suit); op=0; goto print; }
            case 'S': { sprintf(outbuf,"%u,16,%u,%u,3",who,op,suit); op=0; goto print; }
            }
        if (in_mode ==13) { int suit=4;
            switch(amzi_generate[i]) {
                case 'C': suit=0; break;
                case 'D': suit=1; break;
                case 'H': suit=2; break;
                case 'S': suit=3; break;
                }
			if (in_lo && amzi_generate[i]==':') { in_lo=false; in_hi=true; continue;}
			if (amzi_generate[i] >= '0' && amzi_generate[i] <= '9') {
				if (in_hi) hi=hi*10+amzi_generate[i]-'0';
				else if (in_lo) lo=lo*10+amzi_generate[i]-'0';
                continue;
				}
            if (amzi_generate[i]==' ' || amzi_generate[i]=='\0')
				{ sprintf(outbuf,"%u,13,%u,%u,%u",who,suit,lo,hi); goto print; }
            
            }
		if (in_mode ==41) { int state=6;
			// signoff, non-forcing, invitational, one-round, game, forcing pass
            switch(amzi_generate[i]) {
                case 'S': state=0; break;
                case 'N': state=1; break;
                case 'I': state=2; break;
                case 'R': state=3; break;
                case 'G': state=4; break;
                case 'P': state=5; break;
                }
            if (state<6) { sprintf(outbuf,"%u,41,%u,0,0",who,state); goto print; }
			else { sprintf(outbuf,"%u,53,0,0,0",who); goto print; } // NOP    
		}

		if (in_mode ==48) { int id=0,suit=4;
            if (amzi_generate[i]=='s') id=25;
            else if (amzi_generate[i]=='S') id=26;
            if (id>0) switch(amzi_generate[i+1]) {
                case 'C': suit=0; break;
                case 'D': suit=1; break;
                case 'H': suit=2; break;
                case 'S': suit=3; break;
                }
            if (suit<4) { sprintf(outbuf,"%u,%u,%u,0,40",who,id,suit); goto print; }
			else { sprintf(outbuf,"%u,53,%u,0,0",who,suit); goto print; } // NOP    
		}
        if (in_mode == 1) {
            if (amzi_generate[i]=='S') { sprintf(outbuf,"%u,27,%u,%u,%u",who,suit,lo,hi); goto print; }
            else if (amzi_generate[i] == 'V') { sprintf(outbuf,"%u,24,%u,%u,%u",who,suit,lo,hi); goto print; }
            else if (amzi_generate[i] == '>' && amzi_generate[i+1]=='=') { op=20; skip=true; continue;}  //20
            else if (amzi_generate[i] == '<' && amzi_generate[i+1]=='=') { op=19; skip=true; continue; }   // 19
            else if (amzi_generate[i] == '>') { op=18; continue;}    //18
            else if (amzi_generate[i] == '<') { op=17; continue; }   // 17
            
            else if (((amzi_generate[i] >='0' && amzi_generate[i] <= '9')) || amzi_generate[i] == ':' || amzi_generate[i] == ' ') {
                if (amzi_generate[i]==':') { in_lo=false; in_hi = true; continue;}
                if (amzi_generate[i]== ' ') { in_lo=true; in_mode=false; sprintf(outbuf,"%u,12,%u,%u,%u",who,suit,lo,hi); goto print; }
				if (in_hi) hi=hi*10+amzi_generate[i]-'0';
				else if (in_lo) lo=lo*10+amzi_generate[i]-'0';
                continue;
                }
            }
        else if (in_mode == 14 || in_mode == 0) {
			if (in_lo && amzi_generate[i]==':') { in_lo=false; in_hi=true; continue;}
			if (amzi_generate[i] >= '0' && amzi_generate[i] <= '9') {
				if (in_hi) hi=hi*10+amzi_generate[i]-'0';
				else if (in_lo) lo=lo*10+amzi_generate[i]-'0';
                continue;
				}
            if (amzi_generate[i] == ' ' || amzi_generate[i]=='\0') {
                if (in_mode == 16) { sprintf(outbuf,"%u,%u,0,%u,%u",who,in_mode,lo,hi); goto print; }
                else if (in_mode==14) { sprintf(outbuf,"%u,%u,0,%u,%u",who,in_mode,lo,hi); goto print; }
				else if (in_mode==0) { 
					if (lo==0 && hi==0)
						printf("all zero in hcps for ->%s<-\n",generate);
					sprintf(outbuf,"%u,%u,0,%u,%u",who,in_mode,lo,hi); goto print; }
				}
            }
		if (in_mode == INITIAL_STATE && (amzi_generate[i] == ' ' || amzi_generate[i]=='\0'))
			{ continue; }  // in_mode=false; printf("in_mode set to false\n"); in_lo = true; continue;}
        if (in_mode==INITIAL_STATE && in_hi) {
			if (amzi_generate[i] >= '0' && amzi_generate[i] <='9')
				{ hi = 10*hi + amzi_generate[i] - '0'; continue; } 
            else if (amzi_generate[i] == 'S' && amzi_generate[i+1]=='I') { sprintf(outbuf,"%u,44,3,%u,%u",who,lo,hi); skip=true; goto print;}
            else if (amzi_generate[i] == 'H' && amzi_generate[i+1]=='I') {sprintf(outbuf,"%u,44,2,%u,%u",who,lo,hi); skip=true; goto print;}
            else if (amzi_generate[i] == 'D' && amzi_generate[i+1]=='I') {sprintf(outbuf,"%u,44,1,%u,%u",who,lo,hi); skip=true; goto print;}
            else if (amzi_generate[i] == 'C' && amzi_generate[i+1]=='I') {sprintf(outbuf,"%u,44,0,%u,%u",who,lo,hi); skip=true; goto print;}
            else if (amzi_generate[i] == 'S' && amzi_generate[i+1]=='Q') {sprintf(outbuf,"%u,31,3,%u,%u",who,lo,hi); skip=true; goto print;}
            else if (amzi_generate[i] == 'H' && amzi_generate[i+1]=='Q') {sprintf(outbuf,"%u,31,2,%u,%u",who,lo,hi); skip=true; goto print;}
            else if (amzi_generate[i] == 'D' && amzi_generate[i+1]=='Q') {sprintf(outbuf,"%u,31,1,%u,%u",who,lo,hi); skip=true; goto print;}
            else if (amzi_generate[i] == 'C' && amzi_generate[i+1]=='Q') {sprintf(outbuf,"%u,31,0,%u,%u",who,lo,hi); skip=true; goto print;}
            else if (amzi_generate[i] == 'S') { sprintf(outbuf,"%u,1,3,%u,%u",who,lo,hi); goto print; }
            else if (amzi_generate[i] == 'H') { sprintf(outbuf,"%u,1,2,%u,%u",who,lo,hi); goto print; }
            else if (amzi_generate[i] == 'D') { sprintf(outbuf,"%u,1,1,%u,%u",who,lo,hi); goto print; }
            else if (amzi_generate[i] == 'C') { sprintf(outbuf,"%u,1,0,%u,%u",who,lo,hi); goto print; }
            else if (amzi_generate[i] == 'A') { sprintf(outbuf,"%u,23,0,%u,%u",who,lo,hi); goto print; }
            else if (amzi_generate[i] == 'Q') { sprintf(outbuf,"%u,32,0,%u,%u",who,lo,hi); goto print; }
            else if (amzi_generate[i] == 'L') { sprintf(outbuf,"%u,33,0,%u,%u",who,lo,hi); goto print; }
            in_hi = false; in_lo=true;
            continue;
            }
		if (in_mode==INITIAL_STATE) {
			if (in_lo && amzi_generate[i]==':') { in_lo=false; in_hi=true; continue;}
			if (amzi_generate[i] >= '0' && amzi_generate[i] <= '9') {
				if (in_hi) hi=hi*10+amzi_generate[i]-'0';
				else if (in_lo) lo=lo*10+amzi_generate[i]-'0';
                continue;
				}
			}
        if (in_mode==INITIAL_STATE) {
            switch(amzi_generate[i]) {
                case 'C': case 'c': suit = 0; in_mode = 1; break;
                case 'D': case 'd': suit = 1; in_mode = 1; break;
                case 'H': case 'h': suit = 2; in_mode = 1; break;
                case 'S': case 's': suit = 3; in_mode = 1; break;
                case 'B': in_mode = 48; break;
                case 'F': in_mode = 41; in_lo = true; break;
                case 'P': in_mode = 0; in_lo = true; break;
                case 'R': in_mode = 14; in_lo = true; break;
                case 'N': in_mode = 13; in_lo = true; break;
                }
            }
        continue;
print:  sprintf(buf,"constraint(%s)",outbuf);  // was sending as a backquoted string, wrong
#ifndef TWEAKING
		assert_amzi(buf);
#endif
		in_mode = INITIAL_STATE; in_lo=true; in_hi=false; hi=0; lo=0;
		}
	return 0;
}
#endif
    
char best_gtemp[COMMENT_LENGTH];
    EXTERN_HOFFER_C char *c_process(char *line);

char *BestGenerate(int dir) {

    char forth_string[60];
    int i,j,k,val1,val2,com,index;
    int hi=40,low=0,bidder=0;
    BIDNODE *nptr=NULL;
	// char ch_upper;
    char temp2[COMMENT_LENGTH],temp3[120],conv6_temp[120];
	// int count,suits,in_macro;

	CSW_EXEC(BidMessage("BestGenerate entered\n"));
	com = 0; index=print_stack_top;
	best_gtemp[0]='\0';

/* 2003-3-5 this was changed to the section below. This was changed
so that the calling python program can run each individual bid and
collect its information. The main reason can be seen in the following
sequence 1H:X:2H. When that sequence is evaluated it simply ignores the
double in its evaluation. So, the evaluation must proceed by sending each
individual bid and its sequence. 

Now the final result is needed and not all so the value is obtained,
the value macros are reset and the final value reapplied
*/

    clear_value_macros();
	{ int temp_bidder,temp_depth;
	sprintf(forth_string,"67H"); // added so this is set back to original value
	temp_depth = f_eval(forth_string);
	sprintf(forth_string,"69H"); // added so this is set back to original value
	temp_bidder = f_eval(forth_string);
    if (print_stack[0].node == NULL) return (char *) "";
	for (i=0; i<=print_stack_top; i++) {
        if (print_stack[i].node == NULL) break;
        bidder = print_stack[i].bidder;
        sprintf(forth_string,"%u 67G",i);
        f_eval(forth_string);
        sprintf(forth_string,"%u 69G",bidder);
        f_eval(forth_string);
		do_range(print_stack[i].node,bidder);
        sprintf(forth_string,"%u bidder %u",i,bidder);
        ONE_SHOT(500,BidMessage("BestComment: %s",forth_string));
        }
	// check for ok 180726
	sprintf(forth_string,"%d 67G",temp_depth);
	f_eval(forth_string);
	sprintf(forth_string,"%d 69G",temp_bidder);
	f_eval(forth_string);
	}


   if (bidder >=0 && bidder < 4 && print_stack_top >= 0) {
    sprintf(forth_string,"%uH",bidder*2+70);
    CSW_EXEC(BidMessage(forth_string));
    hi = f_eval(forth_string);

    sprintf(forth_string,"%uH",bidder*2+71);
    CSW_EXEC(BidMessage(forth_string));
    low = f_eval(forth_string);

   }
    else {
   sprintf(forth_string,"%d bad",bidder);
   CSW_EXEC(BidMessage(forth_string));
   }

/*
    sprintf(forth_string,"%uH",((bidder+1)%4)*2+70);
    CSW_EXEC(BidMessage(forth_string));
    hi2 = f_eval(forth_string);
    sprintf(forth_string,"%uH",((bidder+1)%4)*2+71);
    CSW_EXEC(BidMessage(forth_string));
    low2 = f_eval(forth_string);
*/
    sprintf(forth_string,"%u %u, bidder was %u",hi,low,bidder);

#if defined(CSW)
    CSW_EXEC(BidMessage(forth_string));
    TRIGGER(1200,do_log(1,20,"in BestGenerate:forth_string:%s",forth_string));
#endif
    
#ifdef AMZI
#ifndef TWEAKING
	sprintf(forth_string,"constraint(%u,14,0,%u,%u)",bidder,low,hi);
#if defined(CSW)
    TRIGGER(1200,do_log(1,20,"calling assert_amzi:%s",forth_string));
#endif
	assert_amzi(forth_string);
#endif
#endif

/*
    clear_value_macros();

    sprintf(forth_string,"%u %uG",hi,bidder*2+70);
    CSW_EXEC(BidMessage(forth_string));
    f_eval(forth_string);
    sprintf(forth_string,"%u %uG",low,bidder*2+71);
    CSW_EXEC(BidMessage(forth_string));
    f_eval(forth_string);

*/

      CSW_EXEC(BidMessage("Calling print_stack_top\n"));

	if (print_stack_top >= 0)
		{
        int conv6_check_index = -1;
        // char *conv6_generate_str=NULL;
        if (index-3>0) conv6_check_index = index-3;
        
#ifdef SIMPLE_COMMENT
		nptr = print_stack[index--].node;
		// repaird 190928 for raven constraint analysis
		// if (nptr->generate_command != NULL) com=1;
        
/* it is all very complicated but the @CONV6_CHECK is the entrance into slam bidding and ace asking and control bidding.
   the constraint is compared to that constraint and the generate command comes from that also.
   it is 3 below the current bid (usu a control bid or ace ask).
   so check to see if the @CONV6_CHECK situation obtains and, if so, get the generate command from there into best_gtemp
*/
        
        if (print_stack[conv6_check_index+2].node->id == 20724 &&
            print_stack[conv6_check_index+1].node->id == 4933)  {
            BIDNODE *temp_nptr=NULL;
            temp_nptr = print_stack[conv6_check_index].node;
            if (strcmp(temp_nptr->generate_command.c_str(),"") != 0) {
                strcpy(conv6_temp,temp_nptr->generate_command.c_str());
                strcat(conv6_temp," ");
                com = 1;
                }
            }
		if (strcmp(nptr->generate_command.c_str(),"") != 0) com |= 2;
#else
		while (com == 0 && index >= 0)
			{
			nptr = print_stack[index--].node;
#ifdef NEW_COMMENT
			if (nptr->comment != 0)
#else
			if (nptr->comment && nptr->comment[0] != '\0')
#endif
				com=1;
			while (nptr->parent != NULL && com == 0)
				{
				nptr = nptr->parent;
#ifdef NEW_COMMENT
				if (nptr->comment != 0)
#else
				if (nptr->comment && nptr->comment[0] != '\0')
#endif
					com=1;
				}
			}
#endif
		}
	if (com != 0) {
		CSW_EXEC(BidMessage("com is not nothing\n"));
		i=0; j=0; best_gtemp[0] = '\0'; // because we may have inserted something from the generate string of CONV6_CHECK
#ifdef NEW_COMMENT
		temp2[0]='\0';
        if (1) {
#ifndef GCC_PY_COMMENT        
            CSW_EXEC(BidMessage("calling loadstring\n"));
			 if (!LoadString((struct HINSTANCE__ *)hInst,nptr->comment,temp2,119)) {
                printf("Comment number %u",nptr->comment);
             	// MessageBox(GetFocus(),temp2,"LoadString failed",MB_TASKMODAL);
				temp2[0]='\0';
				}
#else
            }
        if (com == 2) strncpy(temp2,nptr->generate_command.c_str(),COMMENT_LIMIT);
            else if (com == 1) strncpy(temp2,conv6_temp,COMMENT_LIMIT);
			else if (com == 3) {
				strncpy(temp2,conv6_temp,COMMENT_LIMIT);
				strcat(temp2,nptr->generate_command.c_str());
				}
            CSW_EXEC(BidMessage("returned %s",temp2));
            if (strcmp(temp2,"") == 0) {
                printf("generate %s",nptr->generate_command.c_str());
             	// MessageBox(GetFocus(),temp2,"LoadString failed",MB_TASKMODAL);
				temp2[0]='\0';
				assert (1==2);
				}

#endif			 
#else
		strncpy(temp2,(char *)nptr->comment,COMMENT_LIMIT);
#endif
    CSW_EXEC(BidMessage("temp2 is %s",temp2));
		while  (temp2[i]) {
        	val1 = 0;
/*			if (temp2[i] == '_') { temp[j++]=' '; i++; }
			else */
			if (temp2[i]=='#') {
				val1 = temp2[i+1] - 'a';
#ifndef VERSION2
                val2 = result[val1].c;
#else
				val2 = result[val1];
#endif
                temp3[0]='\0';
/* this is such a reckless kludge, I hate to document it.
However, to see temporary grabs I have allowed the 98th forth
variable to be set in macros with the M98G forth phrase that
duplicates the top of the stack and stores it in that variable.
Now, I can print that by accessing the #z grab. Awful.
*/
				if (val1==25) { char temp4[100];
					    sprintf(forth_string,"98H");
					    hi = f_eval(forth_string);
						sprintf(temp4,"z:%d ",hi);
	sprintf(forth_string,"TOP");
	domacro((char  *)forth_string,dir+3);
	sprintf(forth_string,"T:%u ",f_eval(macro_temp));
	strcat(temp4,forth_string);
	sprintf(forth_string,"BOTTOM");
	domacro((char  *)forth_string,dir+3);
	sprintf(forth_string,"B:%u",f_eval(macro_temp));
	strcat(temp4,forth_string);
	strcpy(temp3,temp4);
				}
				else switch (val2) {
#ifdef PRODUCTION_VERSION
					case 'C': strcpy(temp3,"CLUB"); break;
					case 'D': strcpy(temp3,"DIAMOND"); break;
					case 'H': strcpy(temp3,"HEART"); break;
					case 'S': strcpy(temp3,"SPADE"); break;
					case 'N': strcpy(temp3,"NOTRUMP"); break;
					case '1': strcpy(temp3,"ONE"); break;
					case '2': strcpy(temp3,"TWO"); break;
					case '3': strcpy(temp3,"THREE"); break;
					case '4': strcpy(temp3,"FOUR"); break;
					case '5': strcpy(temp3,"FIVE"); break;
					case '6': strcpy(temp3,"SIX"); break;
					case '7': strcpy(temp3,"SEVEN"); break;
#else
					case 'C': strcpy(temp3,"C"); break;
					case 'D': strcpy(temp3,"D"); break;
					case 'H': strcpy(temp3,"H"); break;
					case 'S': strcpy(temp3,"S"); break;
					case 'N': strcpy(temp3,"N"); break;

					case '0': strcpy(temp3,"0"); break;
					case '1': strcpy(temp3,"1"); break;
					case '2': strcpy(temp3,"2"); break;
					case '3': strcpy(temp3,"3"); break;
					case '4': strcpy(temp3,"4"); break;
					case '5': strcpy(temp3,"5"); break;
					case '6': strcpy(temp3,"6"); break;
					case '7': strcpy(temp3,"7"); break;
					case '8': strcpy(temp3,"8"); break;
					case '9': strcpy(temp3,"9"); break;
#endif
					default : temp3[0]='#'; temp3[1]=temp2[i+1]; temp3[2]='\0'; break;
					}
                k=0;
				while (temp3[k]) best_gtemp[j++]=temp3[k++];
				i+=2;
				}
            else best_gtemp[j++]=temp2[i++];
			}
        best_gtemp[j]='\0';
        best_gtemp[COMMENT_LIMIT]='\0';

        strncpy(temp2,best_gtemp,COMMENT_LIMIT+1);
#undef USE_CORRECT_C_PROCESS
#ifndef USE_CORRECT_C_PROCESS
        i=0;j=0;
		char ch_upper,c_temp=0;
		int suits,in_macro;
        in_macro = 0;
        suits = 0;
        for  (i=0; temp2[i]; i++) {
                ch_upper = toupper(temp2[i]);
                if (in_macro && temp2[i] >= '1' && temp2[i] <= '3') { best_gtemp[j++] = c_temp; in_macro = 0; }
                else if (in_macro) { }

                else if (temp2[i] == '^')  {
                    in_macro = 1; suits = 0;
                    c_temp = *(c_process(&temp2[i])); }
                else best_gtemp[j++] = temp2[i];
                }
        best_gtemp[j]='\0';
#else
		strcpy(best_gtemp,c_process(temp2));
#endif

		}

#ifndef DONT_DO_MACROS_VERSION
#define SKIP_BOTTOM_TOP
#ifndef SKIP_BOTTOM_TOP
#ifndef PRODUCTION_VERSION 

	strcpy(temp2," @ifelse(MYBOTTOM,/MYBOTTOM\\,,[MYBOTTOM/,\\MYTOP])");

	domacro((char  *)temp2,dir+3);
	strcat(best_temp,macro_temp);

	strcpy(temp2," @ifelse(BOTTOM,/BOTTOM\\,,[BOTTOM/,\\TOP])");

	domacro((char  *)temp2,dir+3);
	strcat(best_temp,macro_temp);
	strcat(best_temp," [ MYBOTTOM MYTOP BOTTOM TOP ]");	 
#endif
#endif
    CSW_EXEC(BidMessage("BEST GENERATE <%s>\n",best_gtemp));
	domacro((char  *)best_gtemp,dir+3);
#ifndef PRODUCTION_VERSION
	// strcat(macro_temp,get_line_info(1));
#endif
#if !defined(EXCLUDE_BIDINFO)
	{ int round, seat;
	get_current_round_and_seat(print_stack_top,&round,&seat);
	if (bid_info[round][seat].game_force != 48)
		strcat(macro_temp," GF");
	if (bid_info[round][seat].forcing_pass != 48)
		strcat(macro_temp," FP");
	}
#endif
	// added low, hi values 190928
#if !defined(linux)
    sprintf(best_temp,"");
#else
    best_temp[0]='\0';
#endif
    if (!(low == 0 && hi == 40)) sprintf(best_temp,"  R%u:%u ",low,hi);
	strcat(macro_temp,best_temp);
	return (LPSTR) macro_temp;
#else   // NO_MACROS_VERSION
    ONE_SHOT(503,BidMessage("OS: end of best comment: %s",best_temp));
    return best_temp;
#endif  // DONT_DO_MACROS_VERSION
}



int BestHelp(void) {
    int help,index;
    BIDNODE *nptr=NULL;

	help = 0; index=print_stack_top;
	while (help == 0 && index > 0) {
			nptr = print_stack[index--].node;
			if (nptr->help_context != 0) help=nptr->help_context;
			while (nptr->parent != NULL && help == 0) {
				nptr = nptr->parent;
				if (nptr->help_context != 0) help=nptr->help_context;
				}
			}
	return help;
	}


#ifdef COUNT_LINES
void set_line_info(int num, int id) {
	int i,size;
	if (lines == NULL) {
		size = NUM_OF_LINES * sizeof(line_info);
#ifndef HOFFER
   lines= (struct line_info *)  _malloc_dbg( size, _NORMAL_BLOCK, __FILE__, __LINE__ );
#else
   lines= (struct line_info *) malloc( size);
#endif
 
		if (lines == NULL) {
			printf("\nCannot allocate lines");
            // MessageBox(NULL,"TSelectPrint","Cannot Allocate lines",MB_ICONSTOP);
			return;
			}
        else for (i=0; i< NUM_OF_LINES; i++) lines[i].count = 0;
    	}
	assert(num < NUM_OF_LINES);
	lines[num].id_num = id;
	}
#endif

void increment_line_count(int line_number) {
	for (int i=0; i<NUM_OF_LINES; i++) {
		if (lines[i].id_num == line_number) {
			++lines[i].count;
			break; }
		}
	}


int get_line_count(int line_number) {
	int i;
	for (i=0; i<NUM_OF_LINES; i++)
		if (lines[i].id_num == line_number)
			return lines[i].count;
    return 0;
	}


#define SIMPLIFY_LINES
#define LINE_INFO_LEN 40
#define RESULT_LEN 1000
char info_result[RESULT_LEN+1];

void reset_line_count() {
	for (int i=0; i<NUM_OF_LINES; i++)
		lines[i].count = 0;
}


char *get_lines() {
	char temp[20];
	int count=0;
	strcpy(info_result,"(");
	for (int i=0; i<NUM_OF_LINES; i++) {
		if (lines[i].count > 0) {
			count++;
			if (strlen(info_result) < RESULT_LEN - 10) {
				if (count>1) strcat(info_result,",");
				sprintf(temp,"%u",lines[i].id_num);
				strcat(info_result,temp);
				}
			}
		}
	strcat(info_result,")");
	return info_result;
}

EXTERN_HOFFER_C BIDNODE *get_bidnode(int *grab_count) {
    BIDNODE *nptr=NULL;
    int j=print_stack_top,grabs=0;
    
    while (j>=0) {
        nptr=print_stack[j].node;
        grabs += print_stack[j].node->num_of_grabs;
        j--;
        }
    grab_index = grabs;
    return nptr;
    }

EXTERN_HOFFER_C char *get_line_info(int count)
{
	BIDNODE *nptr;
	int i,j;
#ifndef SIMPLIFY_LINES
	char temp[LINE_INFO_LEN+1];
#else
    char temp[RESULT_LEN+1],temp2[6];
#endif

	temp[0] = '\0';
	j = print_stack_top;
	if (j<0) { info_result[0] = '\0'; return info_result; }
	strcpy(info_result," (");

	if (j<0) strcat(info_result,"NONE");
	else while (count-- > 0 && j >= 0) {
		nptr = print_stack[j].node;
		j--;
	//	assert(nptr != NULL); 180908 this was NULL during bidding of open room by human
		if (nptr == NULL) {
			strcat(temp,"NULL");
			if (count > 1 && j >= 0) strcat(temp,",");
			continue;
			}
	//	i = get_line_no(nptr,sys->root,&found);
		i = nptr->id;
	//	assert(i < NUM_OF_LINES);
	//	i = lines[i].id_num;
		sprintf(temp2,"%u",i);
#ifndef SIMPLIFY_LINES
		if (strlen(temp) + strlen(temp2) > LINE_INFO_LEN) {
			strcat(info_result,temp);
			strcat(info_result,"\n");
			strcpy(temp,temp2);
			temp2[0]='\0';
			}
		else
#endif
        strcat(temp,temp2);
		if (count > 1 && j >= 0) strcat(temp,",");
		}
	strcat(info_result,temp);
	strcat(info_result,")");
	return (char *) info_result;
}



int get_line_no(BIDNODE *nptr,BIDNODE *in, int *found) {
	int temp1=0,temp2=0;

	if (in == NULL) return 0;
	if (nptr == in) { *found = 1; return 1; }

	temp1 = 1 + get_line_no(nptr,in->child,found);
    if (*found == 1) return temp1;
	temp2 = get_line_no(nptr,in->sibling,found);
    return temp1 + temp2;
	}

BIDNODE *get_node(BIDNODE *nptr,int id) {
	BIDNODE *ans;
	if (nptr == NULL) return NULL;
	if (nptr->id == (unsigned int)id) return nptr;
	if ((ans=get_node(nptr->child,id)) != NULL) return ans;
	if ((ans=get_node(nptr->sibling,id)) != NULL) return ans;
	return NULL;
	}

char the_result[180];
char *get_constraint(int id,int which) {

	BIDNODE *ans; 
	ans = get_node(sys->GetRoot(),id);
	if (ans == NULL) {
		strcpy(the_result,"ID NOT FOUND");
		}
	else {
		if (which == 0) return (char *) ans->constraint.c_str();
		else if (which == 3) return (char *) macro(ans->constraint.c_str(),0);
		else if (which == 1) { 
			if (strcmp(ans->constraint1.c_str(),"") != 0) return (char *) ans->constraint1.c_str();
			else strcpy(the_result,"NONE");
			}
		else {
			if (strcmp(ans->constraint2.c_str(),"") != 0) return (char *) ans->constraint2.c_str();
			else strcpy(the_result,"NONE");
			}
		}
	return the_result;
	}

char *get_range(int id, int which) {
		BIDNODE *ans;
		ans = get_node(sys->GetRoot(),id);
		if (ans == NULL) {strcpy(the_result,"RANGE NOT FOUND"); }
		else if (which == 0) {
			if(!ans->range.empty()) return (char *) ans->range.c_str();
			else strcpy(the_result,"NONE");
			}
		else if (which == 1) {
				if (!ans->range1.empty()) return (char *) ans->range1.c_str();
				else strcpy(the_result,"NONE");
			}
		else {
			if (!ans->range2.empty()) return (char *) ans->range2.c_str();
			else strcpy(the_result,"NONE");
		}
	return the_result;
}

    
char *get_generate(int id) {
    BIDNODE *ans;
    ans = get_node(sys->GetRoot(),id);


    // if (ans == NULL) {sprintf(the_result,"GENERATE NOT FOUND:%u",id); }
    if (ans == NULL) {sprintf(the_result,"GENERATE NOT FOUND:id:%d",id); }

    else strcpy(the_result,ans->generate_command.c_str());
    return(the_result);
    }

char *get_priority(int id, int which) {
    BIDNODE *ans;
    ans = get_node(sys->GetRoot(),id);
    if (ans == NULL) { strcpy(the_result,"PRIORITY NOT FOUND"); return the_result; }
    the_result[0] = ans->priority + '0'; // is stored as a character
    the_result[1] = '\0';
    return the_result;
    }
    
#if defined(AMZI)
#if !defined(AMZI_THERE)
    // #pragma message("compiling int_call_amzi")

        EXTERN_HOFFER_C int int_call_amzi(const char *command, int index) {
            TF tf;
            // int rc;
            // static CLogicServer oldServer = ls;
            int return_val=0;
        TERM again_t;
            
            // tf = lsCallStr(CurEng,&again_t,(char *) command);
            tf = lsCallStr(CurEng, &again_t,(MYBUF *) command);
            if (tf) {
                lsGetArg(CurEng, again_t,index,cINT,(int *) &return_val);
                }
            else printf("%s failed\n",command);
            return return_val;
        }
#endif
#endif

#if defined(AMZI)
extern "C" int cpp_int_call_amzi(const char *command, int index) {
            return int_call_amzi(command,index);
        }
#endif

#if defined(AMZI_BID)
#if !defined(ANDROID)
ENGid CurEng;
#define MYBUF wchar_t
#define AMZI_COMMENT_MAX 100
char amzi_comment[AMZI_COMMENT_MAX+1];
#endif

void amzi_error()
    /* Report on errors and quit. */
    {
        // char buf[120];
        wchar_t buf[120];

        lsGetExceptMsg(CurEng, buf, 120);
        printf("Logic Server Error #%d %s", lsGetExceptRC(CurEng), (char *) buf);
        exit(0);
    }


// extern "C" int retract_sequence_amzi(void) {
EXTERN_HOFFER_C int retract_sequence_amzi(void) {
#if defined(OLD_RETRACT_SEQUENCE_AMZI)
   TF   tf;   /* LSAPI true/false/err return codes */
   TERM t;    /* a Prolog term */
   int result=0;
 #ifdef TWEAKING  // 1
   return 0;
 #endif

   /* Build a query term and call it */
    tf = lsCallStr(CurEng, &t, (MYBUF *) "abolish_sequence(X)");

/* If the query succeeded print up the results */

   if (tf)
   {
      lsGetArg(CurEng, t, 1, cINT, &result);
   }
   else {
        printf("retract_sequence_amzi failed\n");
   }
   return result; // 0 or 1 if it abolish all the bridge facts.
#else
    return int_call_amzi("abolish_sequence(X)",1);
#endif
}
    
    
    // extern "C" int retract_who_am_i_amzi(void) {
EXTERN_HOFFER_C int retract_who_am_i_amzi(void) {
#if defined(OLD_RETRACT_WHO_AM_I_AMZI)
    TF   tf;   /* LSAPI true/false/err return codes */
       TERM t;    /* a Prolog term */
       int result=0;

       /* Build a query term and call it */
        tf = lsCallStr(CurEng, &t, (MYBUF *) "abolish_who_am_i(X)");

    /* If the query succeeded print up the results */

       if (tf)
       {
          lsGetArg(CurEng, t, 1, cINT, &result);
       }
       else {
            printf("retract_who_am_i_amzi failed\n");
       }
       return result; // 0 or 1 if it abolish all the bridge facts.
#else
    return int_call_amzi("abolish_who_am_i(X)",1);
#endif
    }

// extern "C" int bid_amzi(void) {
#undef QUIT_DEBUGGING
EXTERN_HOFFER_C int bid_amzi(void) {
   TF   tf;   /* LSAPI true/false/err return codes */
   TERM t;    /* a Prolog term */
   char buf[1024];
#ifdef QUIT_DEBUGGING
   bool quit_debugging = false;
#endif
   // int index = 1; // the single term here but could be 2 for help(`help`,X)
   int level,denom;
#ifdef TWEAKING
   return 0;
#endif
#ifdef QUIT_DEBUGGING
   if (quit_debugging) printf("bid_amzi entered\n");
#endif
    
   amzi_comment[0]='\0';

#if defined(OLD_BID_CODE)
    
    try { tf = lsCallStr(CurEng, &t, (MYBUF *) "bid(X)"); }
   catch (...) {
#ifdef QUIT_DEBUGGING
       if (quit_debugging) fprintf(stderr,"bid(X) failed\n");
#endif
   }
#else
    { char *temp;
        temp = str_call_amzi("bid(X)",1);
        strcpy(buf,temp);
        TRIGGER(ID_BID_AMZI,do_log(1,30,temp));
    }
#endif

#ifdef QUIT_DEBUGGING
    if (quit_debugging) printf("back from bid(X)\n");
#endif
/* If the query succeeded print up the results */

#if defined(OLD_BID_CODE)
    if (tf)   {
      lsGetArg(CurEng, t, 1, cSTR, buf);
#else
       if (1) {
#endif
      if (buf[0] == 'N') return -1; // for No or NoBid
      if (buf[0] == 'P') {denom = 0; level = 0; }
      else if (buf[0]=='X') { denom = 1; level = 0; }
      else if (buf[0]=='R') { denom = 2; level = 0; }
      else if (buf[0] >= '1' && buf[0] <= '7') {
          level = buf[0]-'0';
          if (buf[1] == 'C') denom = 0;
          else if (buf[1] == 'D') denom = 1;
          else if (buf[1] == 'H') denom = 2;
          else if (buf[1] == 'S') denom = 3;
          else if (buf[1] == 'N') denom = 4;
          else return -1;
            }
      else {
#ifdef QUIT_DEBUGGING
          if (quit_debugging) fprintf(stderr,"bid(X) failed returning -1\n");
#endif
          TRIGGER(ID_BID_AMZI,do_log(1,30,"bid(X) failed so returning -1"));
          return -1;
        }
    
        strncpy(amzi_comment, &buf[2], AMZI_COMMENT_MAX+1);
        if (amzi_comment[AMZI_COMMENT_MAX] != '\0') {
            // ERROR: stringA was too long.
            amzi_comment[AMZI_COMMENT_MAX] = '\0'; // if you want to use the truncated string
            }
      return level * 10 + denom;
   }
   else {
#ifdef QUIT_DEBUGGING
       if (quit_debugging) fprintf(stderr,"calling amzi_error1");
#endif
       amzi_error();
        fprintf(stderr,"bid failed\n");
   }
#ifdef QUIT_DEBUGGING
   if (quit_debugging) fprintf(stderr,"calling amzi_error2");
#endif
   amzi_error();
   return -1;
}

#endif
    
extern "C" void cpp_set_declarer(int d) { set_declarer(d); }
    //extern "C" int retract_constraints_amzi(void) {
    // EXTERN_HOFFER_C
    
#if defined(AMZI)
    int retract_constraints_amzi(void) {
    #if defined(OLD_RETRACT_CONSTRAINTS_AMZI)
       TF   tf;   /* LSAPI true/false/err return codes */
       TERM t;    /* a Prolog term */
       int result=0;
     #ifdef TWEAKING  // 1
       return 0;
     #endif

       /* Build a query term and call it */
        tf = lsCallStr(CurEng, &t, (MYBUF *) "abolish_constraint(X)");

    /* If the query succeeded print up the results */

       if (tf)
       {
          lsGetArg(CurEng, t, 1, cINT, &result);
       }
       else {
            printf("retract_constraints_amzi failed\n");
       }
       return result; // 0 or 1 if it abolish all the bridge facts.
    #else
        return int_call_amzi("abolish_constraint(X)",1);
    #endif
    }
#endif
