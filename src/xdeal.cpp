/* Versions

210209  Coordinated with windows surface version
		Also coordinated with windows surface version of constraint_ids.h
210406  Found the passing problem for IOS
		
*/

#include "meadowlark.h"
#undef GLOBALS
#include "../include/GLOBALS.H"
int hoffer_setup_flag = 0;



#if !defined(WIN32)
#if !defined(IOS)
#if !defined(linux)
// Android studio does not have pre-compiled headers
#include "prefix.pch"
#endif
#endif
#endif

#if defined(ANDROID) || defined(__ANDROID__)
#if defined(AMZI_THERE)
#pragma message("AMZI_THERE is still defined...should not be")
#endif
#endif

#undef INFO_TO_CONTEMPLATE

#if defined(INFO_TO_CONTEMPLATE)

#if defined(IOS)
#pragma message("IOS is defined")
#endif
#if defined(AMZI)
#pragma message("AMZI is defined")
#endif
#if defined(AMZI_THERE)
#pragma message("AMZI_THERE is defined")
#endif
#if !defined(OK_FOR_SCOTT)
#pragma message("using development version from the correct prefix.pch file")
#endif
#if !defined(CSW)
#pragma message("CSW is not defined")
#else
#include "cswitch.h"
#endif

#endif  // INFO_TO_CONTEMPLATE


char *cpp_hoffer_bid(const char *hand, const char *sequence, int vul, int dealer);

#define PUT_GLOBALS_HERE

#if defined(MESSAGE_ASSERTS)
#if defined(NO_ASSERTS)
#pragma message("No asserts")
#else
#pragma message("Asserts are on")
#endif
#endif

#if !defined(linux)
#pragma message("including ring.h")
#endif
#if defined(GENERATE_ONLY)
extern "C" void gen32_distro(int [52], int, int, int [4]);
#endif
#include "ring.h"
extern int prgm[DECKNUM];


#undef CHECK_DESCRIPTIONS
extern char gen_initialized;
void gen_init2(void);
#ifndef WIN32
#include <stdbool.h>
#endif

// #include "windows.h"
// #include "..\include\pch.h"
// #include "..\include\globals.h"
// #include "time.h"
// #include "bergen.h"

#define NO_LOGGING
#undef SKIP_BAD_HANDS
#define RIGID
#undef HOFFER_DEBUGGING
#define MSG_SIZE 20000


#include "stdlib.h"
#include "stdio.h"
#include "string.h"
// release #include "assert.h"
#include "time.h"
#include "math.h"  // for floor
#if !defined(NO_ASSERTS)
#include "assert.h"
#endif
#undef GLOBALS
#include "../include/br.h"
#include "../include/GLOBALS.H"
#include "../include/proto.h"

#include "constraint_ids.h"
#if defined(MESSAGE_CSW)
#ifdef USE_CSW
#pragma message("USE_CSW is on in xdeal.c")
#else
#pragma message("USE_CSW is not on in xdeal.c")
#endif
#ifdef NO_CSW
#pragma message("NO_CSW is on in xdeal.c")
#else
#pragma message("NO_CSW is undef")
#endif
#endif

#ifdef AMZI_GEN
char *call_amzi(const char *);
// int assert_amzi(const char *);
int reload_amzi(void);
int retract_who_am_i_amzi(void);
int retractall_amzi(void);
// int retract_constraints_amzi(void);
// int retract_sequence_amzi(void);
// int assert_amzi(const char *);
void show_coverage(void);
void show_listing(void);
int bid_amzi(void);
char *get_amzi_comment(void);
char *get_bergen_comment(void);
int close_amzi(void);
int test_amzi(void);
int retract_points(void);
void set_pybid_loaded(void);
#endif

char *get_constraint_message(int who,int what,int where,int low,int hi);
char constraint_message[MSG_SIZE];
int gen32_roth(int [52],int,int,int,int);
int gen32_getQuicks(int d[52], int first, int number, int quicks[4]);
#define TRUE 1
#define FALSE 0
#define SUPPORTED 3
#define SUPPORT_OF 2
#define ON 1
#define OFF 0


#define STARTING 1
#define BERGEN 1
#define KLINGER 3
#define OPENING_BID 1

#define SUPPORT_OF 2
#define SUPPORTED 3

#define ROTH 2

#define BERGEN_HCPS 4
#define KLINGER_HCPS 5
#define KAPLAN_RUEBEN 6
#define HOFFER_COUNT 7
#define BBO 8


#define HOFFER_COMPLEX
#define FIX_SUIT_STRENGTH
int bergen(int deck[52],int start,int number,int state, int suit);
int hoffer_count(int deck[52],int start, int number, int state, int suit);
int klinger(int deck[52],int start,int number,int state, int suit);
int kaplan_rueben(int deck[52],int start,int number,int state, int suit);
int bbo(int deck[52],int start, int number, int state, int suit) { return 0; }
char *get_constraint_message(int who,int what,int where,int low,int hi);
#define ADD_CONSTRAINT_ERROR (-1)
#define KNOWS_CONSTRAINT_ERROR (-2)
char *do_all_unity_tests(void);
int gen32_deck_position[DECKNUM];
extern char gen_initialized;
extern void gen_init2(void);
char gen32_description[120];
int my_total_points;
int gen32_temp_dist[4];
int gen32_roth(int deckp[52],int start,int number,int state, int suit);
int gen32_bergen(int deckp[52],int start,int number,int state, int suit);
int gen32_klinger(int deckp[52],int start, int number, int state, int suit);
int gen32_kaplan_rueben(int deckp[52],int start, int number, int state, int suit);
int gen32_bbo(int deckp[52],int start, int number, int state, int suit);
int gen32_hoffer_count(int deckp[52],int start, int number, int state, int suit);

char description_buffer[D_LEN];
int excludes = 0;

#ifndef HOFFER
#pragma message("HOFFER is not defined in xdeal.c")
#pragma message("using HOFFER_COUNT")
extern int gen32_evaluation_system = HOFFER_COUNT;
#else
// #pragma message("HOFFER is defined in xdeal.c")
int gen32_evaluation_system;
// char description_buffer[D_LEN];
#endif

// extern int deck[52];

extern int gen32_deck[52];

bool pybid_loaded=false;
void set_pybid_loaded(void) { pybid_loaded = true; }

static int max;
static int num;
#ifdef BEST_HOFFER
int threshhold[] = {0,0,0,0};
int delta[] = {2,2,2,2};
int modulo[] = {99,99,99,99};
int loops = 500;
#else
int threshhold=0, delta=2, modulo=99,loops=500;  // these are global values set by the calling program for tuning of hand generation
#endif

static char suits[] = "CDHS";


#ifdef CXLOGGING
#define LOGIMPORT __declspec(dllimport)



LOGIMPORT int __stdcall LogDebug(const char *);
LOGIMPORT int __stdcall LogInfo(const char *);
LOGIMPORT int __stdcall LogWarning(const char *);
LOGIMPORT int __stdcall LogError(const char *);
LOGIMPORT int __stdcall StartLoggingStderr(unsigned long,const char *);
LOGIMPORT int __stdcall StartLoggingStdout(unsigned long,const char *);
LOGIMPORT int __stdcall StartLogging(const char *, unsigned long, unsigned long, unsigned long, const char *);
LOGIMPORT int __stdcall StopLogging();
#endif

char *get_binary(int);
void restricted_shuffle(void);
int useable(int,int);
int set_offsets(int);
void do_exchange(int,int);
void initialize_decks(void);
void set_card(int,int);
int deck_verify(int);
int get_matt_string2(int deck[52], char *szFormat);
int find_spot(int selected_position);
void constrained_shuffle(void);
char *get_dealt_hands(int,int);
char *get_dealt_hand(int);

#define CONSTRAINT_MAX 250
#define COMMAND_MAX 80
#define PLACING
int dirty_flag = OFF;



#ifdef VA_LOGGING
extern "C" int gen32_do_log(int logger, int level, const char* format, ...)
{
	char buffer[MAX_MSG+1];
	va_list argptr;
	int cnt;

	if (!initialized) setup_logging(20,"bid20");
    if (logger < 0 || logger > 2) logger = 1;
	if (level < 10 || level > 40) level = 10;


	va_start(argptr, format);
	cnt = vsnprintf(buffer, MAX_MSG, format, argptr);
	va_end(argptr);
	if (cnt == EOF) return cnt;

	if (logs[logger] < level) return 0;
	else if (level == 10)
		{ LogDebug(buffer); }
	else if (level == 20)
		{ LogInfo(buffer); }
	else if (level == 30)
		{ LogWarning(buffer);}
	else if (level == 40)
		{ LogError(buffer); printf("%s\n",buffer); }
	else { LogDebug(buffer); }

	return logger;
}
#endif
extern "C" int gen32_do_log(int i, int j, char const* str, ...) { return 0; }

#if defined(OLD_GEN32_DO_LOG)
extern "C" int gen32_do_log(int i, int j, char const* str, ...) {
#ifndef NO_LOGGING
	char msg[100];
	sprintf(msg,"gen32: %s",str);
#ifdef CXLOGGING
	if (j==50) LogError(msg);
	else if (j==40) LogError(msg);
	else if (j==30) LogWarning(msg);
	else if (j==20) LogInfo(msg);
	else if (j==10) LogDebug(msg);
#endif
#endif
}
#endif

void find_exchange(int,int);

#define INDEX_MAX 60

#ifdef BETTER_HOFFER
#define BAD_V
#else
#undef BAD_V
#endif
#define BAD_V

#undef EXTRA_DEBUGGING
#ifdef EXTRA_DEBUGGING
#define BAD_V
#define CSW
#endif

#ifdef BAD_V
int bad_v[6][CONSTRAINT_MAX];
#endif

int mul_13[5] = { 0, 13, 26, 39, 52 }; // to save time of multiplying 13

int decks[DECKNUM][52]; /* these are the hands */
bool decks_flag[DECKNUM]; /* these are flags for each hand indicating whether or not dealing was not faulty */
struct constraint {
	int who;
	int what;
	int where;
	int hi;
	int lo;
	int diff;
	int value;
	int flag;
	} constraints[CONSTRAINT_MAX];
int offsets[4][4];
int starters[4];
int num_of_hands=0;
int indicator;
int active_hand=0;
int initialized_flag = 0;
int constraint_deal_exclude(int first, int num, int excludes);
int constraint_deal(int first, int num);
int constraint_deal_n(int first, int num, int loops);
#ifdef BEST_HOFFER
void gen32_set_loops(int);
void gen32_set_individual_exchanges(int,int,int,int); 
int check_constraints(int [4]);
#else
void gen32_set_exchanges(int,int,int,int);
int check_constraints(void);
#endif
void setup_logging(int, char *);
int add_constraint(int, int, int, int, int);
int get_constraint_count(void);
int get_constraint_value(int i, int j);
void set_already_placed(int who);
int verify_constraint(int, int, int, int, int);
void clear_constraints(int);
int constraint_index = 0;
int index_stack[INDEX_MAX][6];
int index_index=0;
int gen32_get_hcps(int *,int,int);
int getAces(int *,int,int);
#ifndef FIX_LOSERS
int get_losers(int *,int *, int,int);
#else
int gen32_getLTC(int *, int, int);
#endif
int gen32_get_controls(int [52], int, int, int);
int gen32_get_quicks(int [52], int [4], int, int);
int gen32_get_suit_quicks(int [52], int [4], int, int);
int get_support_points(int [52], int [4], int, int);
int get_supported_points(int [52], int [4], int, int);
#if !defined(GENERATE_ONLY)
extern "C" void gen32_distro(int[52], int, int, int[4]);
#endif
char *gen32_get_description2(int,int);
void gen32_set_deck(int,int);
int gen32_get_deck(int);
void gen32_set_evaluation_system(int);
void sort_by_length(int *);
int already_placed[4];
int already_played[52];
int already_played_flag;
int already_placed_count;
void find_hand(int input_position,int *input_start,int exclude, int *output_hand, int *output_position,int *placed);
char temp_msg[180];

extern char *cpp_call_amzi(const char *command);


int get_constraint_count(void) {
    // CSW_EXEC(fprintf(stderr,"this is a test of csw\n"));
    return constraint_index;
}






int deck_verify(int hand) {
    if (hand < 0)
        return 0;
    if (hand >= DECKNUM)
        return 0;
    if (!initialized_flag) initialize_decks();
    return 1;
}
// Constrain hand patterns:
//	  Constraints have a matching controller constraint, usually one to one
//	   For instance: who=0, what=0, where=3, low = 5, high = 9
//		followed by  who=0, what=7, where=0, low = any, high = any
//		checks that spades for hand zero are 5 to 9 in length

int verify_constraint(int who, int what, int where, int low, int hi) {
    int i;

    for (i = 0; i < constraint_index; i++) {
        if (constraints[i].who != who) continue;
        if (constraints[i].what != what) continue;
        if (constraints[i].where != where) continue;
        if (constraints[i].lo != low) continue;
        if (constraints[i].hi != hi) continue;
        return 1;
        }
    return 0;
}

void set_already_placed(int who) {
#ifndef NO_ASSERTS
    assert(who>=0 && who < 4);
#endif
    already_placed[who]=13;
    already_placed_count+=13;
#ifndef NO_ASSERTS
    assert(already_placed_count<52);
#endif
}


char dealt_hand[100];
char *get_dealt_hands(int hand_num, int exclude_played) {
    int k;
    dealt_hand[0]='\0';
    // fprintf(stderr,"get_dealt_hand:%u\n",hand_num);
    for (k=0; k<52; k++) gen32_deck[k] = decks[hand_num][k];
    // printout(gen32_deck,0);
    // printout(gen32_deck,13);
    // printout(gen32_deck,26);
    // printout(gen32_deck,39);
    // hand_print_setup(15,60);
	// handprint(gen32_deck);


	get_dealt_hand(exclude_played);

    return dealt_hand;
}

char *get_dealt_hand(int exclude_played) {
    int i,j,len = 0,flags[3];
    static char cards[14];
    sort(gen32_deck,0,13,UP);
    sort(gen32_deck,13,13,UP);
    sort(gen32_deck,26,13,UP);
    sort(gen32_deck,39,13,UP);


        for (j=0; j<14; j++) {
            switch (j) {
                    case 0:  cards[j] ='2'; break;
                    case 1:  cards[j] ='3'; break;
                    case 2:  cards[j] ='4'; break;
                    case 3:  cards[j] ='5'; break;
                    case 4:  cards[j] ='6'; break;
                    case 5:  cards[j] ='7'; break;
                    case 6:  cards[j] ='8'; break;
                    case 7:  cards[j] ='9'; break;
                    case 8:  cards[j] ='T'; break;
                    case 9:  cards[j] ='J'; break;
                    case 10: cards[j] ='Q'; break;
                    case 11: cards[j] ='K'; break;
                    case 12: cards[j] ='A'; break;
                    case 13: cards[j]='\0'; break;
                    }
            }

        for (i=0; i< 4; i++) {
            for (j=0; j<3; j++) flags[j]=OFF;
            for (j=12; j>=0; j--) {
                if (flags[0] == OFF && (gen32_deck[i*13+j] /13) < 3) {
                    strcpy(&dealt_hand[len],"."); len++; flags[0]=ON;
                    }
                if (flags[1] == OFF && (gen32_deck[i*13+j] /13) < 2) {
                    strcpy(&dealt_hand[len],"."); len++; flags[1]=ON;
                    }
                if (flags[2] == OFF && (gen32_deck[i*13+j] /13) < 1) {
                    strcpy(&dealt_hand[len],"."); len++; flags[2]=ON;
                    }
                if (!exclude_played) dealt_hand[len++] = cards[gen32_deck[i*13+j]%13];
                else if (!already_played[gen32_deck[i*13+j]]) dealt_hand[len++] = cards[gen32_deck[i*13+j]%13];
                // else dealt_hand[len++] = 'x';
                }
            if (flags[1] == OFF) {
                strcpy(&dealt_hand[len],".");
                len++;
                        }
            if (flags[2] == OFF) {
                strcpy(&dealt_hand[len],".");
                len++;
                }
            if (i<3) { strcpy(&dealt_hand[len]," "); len++; }
            }
    dealt_hand[len]='\0';
        return dealt_hand;
 }

#ifndef DIVORCE2
#ifndef WIN_ONLY
#pragma messsage("blocking initialize_amzi")
// char *my_initialize_amzi(void);
#endif
#endif

// #pragma message("extern for cpp_amzi_get_a_lead")
extern char *cpp_amzi_get_a_lead(int leader);
#ifdef AMZI_THERE
#if defined(AMZI_THERE)
// #pragma message ("AMZI_THERE is on")
#endif
char *cpp_str_call_amzi(const char *,int);
bool cpp_initialize_amzi(void);
int cpp_assert_amzi(const char *);
#endif

#if !defined(GENERATE_ONLY)
void cpp_initialize_constraints(void) {};
extern "C" void initialize_matchtree(void) {};
#endif

#ifdef USE_CSW // macros for printing out debugging messages
int getargs2(const char *);
void clear_csw(void);
#endif

void csw_print(const char *msg) { CSW_EXEC(fprintf(stderr,"%s\n",msg)); }

unsigned int debugging_flag = 0;
/*
 1 = initialization
 2 = anything to do with constraints
 4 = anything to do with amzi prolog
 */
unsigned int debugging_level =  40;
unsigned int debugging_meta = 1;


void do_hoffer_setup(void);
char knows_constraint_err_msg[250];


char *hoffer_bid(const char *,const char *, int , int);
char *hoffer_lead(const char *,const char *, int , int);
char *hoffer_evaluate(const char *, int, int);

char *constraint_command(int who, int what, int where, int low, int hi);
// extern char *hello() { return "Hello Rod"; }
extern char *hello() {
    constraint_command(0,90,0,0,0);
    // hoffer_bid("ak654.ak54.654.4","",0,0);
    // return hoffer_evaluate("1S",0,0);
    return (char *) "";
}

int version_sent = 0;
int hoffer_status = 0;
int wrong_command = 200;


// int do_log(int,int,const char *,...);

#undef ALOGGING
#if defined(IOS)
#undef ALOGGING
#endif

#if defined(ALOGGING)
#include <android/log.h>
#define  LOG_TAG    "testjni"
#define  ALOG(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#endif


extern "C" int gen32_do_log(int, int, const char*, ...);

#undef VERSION_SENT_BLOCKS

#if defined(AMZI_THERE)
#define AMZI_THERE_TEST
#undef AMZI_THERE_TEST1
#define AMZI_THERE_TEST2
#if defined(AMZI_THERE_TEST)
bool cpp_initialize_amzi(void);
char *cpp_str_call_amzi(const char *,int);
int cpp_assert_amzi(const char *);

#endif
#endif


int cpp_int_call_amzi(const char *, int);

void cpp_set_declarer(int);

void set_logging_level(unsigned int);
unsigned int get_logging_level(void);

char *constraint_command(int who, int what, int where, int low, int hi) {
    static int max=0;
	static bool exchanges_flag = false;
#if defined(AMZI_THERE) || defined(AMZI_THERE_TEST)
#pragma message("AMZI_THERE is on")
    static bool amzi_there_initialized = false;
#endif
    static bool constraints_initialized = false;
    static bool loops_initialized = false;
#if !defined(NO_CSW)
    static bool csw_initialized = false;
#endif
    static bool debugging_initialized = false;
    static bool prolog_accepting_cards = true;
    static bool hoffer_initialized = false;
    char *hand = NULL, *msg=NULL;
    static bool coverage_initialized = false;
    // char *adescription;
    int result;

#ifdef USE_STATE
    if (what == ID_RESET) CPP_DO_STATE(99);
#endif
#if !defined(GENERATE_ONLY)
    if (what == ID_RESET) initialize_matchtree();
#endif
#if defined(AMZI_THERE) || defined(AMZI_THERE_TEST1)
#pragma message("amzi_there_test will be used in constraint_command")
    if (what == ID_RESET && !amzi_there_initialized) {
        TRIGGER(ID_INITIALIZE,gen32_do_log(1,30,"calling amzi_initialize from constraint_command"));
        cpp_initialize_amzi();
#ifdef USE_STATE
        CPP_DO_STATE(98);
#endif
        }
#endif


    if (what == ID_COVERAGE && coverage_initialized) { // handles coverage but also ensures initialization of everything
        sprintf(constraint_message,"coverage:ok:%d",where);
        return constraint_message;
        }


    else if (what == ID_COVERAGE){
        char *msg;
        coverage_initialized=true;
#ifdef USE_STATE
        CPP_DO_STATE(ID_COVERAGE);
#endif
        msg = constraint_command(who,ID_RESET,0,0,0);
        sprintf(constraint_message,"%s, coverage:%d",msg,where);
        return constraint_message;
        }

#if !defined(NO_CSW)
        if (!csw_initialized) {
             // getargs2("-sXDEAL.C;SYS.CPP;CONVENTI.CPP");
            // set_one_shot("1");
            // set_one_shot("1602");
            // CSW_EXEC(ALOG("hoffer:CSW is initialized"));
            csw_initialized = true;
            }
#endif
#if defined(ALOGGING)
    ALOG("LUDDY:constraint_command:%d %d %d %d %d",who,what,where,low,hi);
#endif
	sprintf(temp_msg, "constraint_command:%d %d %d %d %d", who, what, where, low, hi);
#if !defined(GODOT)
#ifdef CSW
    // TRIGGER(1602,gen32_do_log(1,10,temp_msg));
#endif
#else
	// gen32_do_log(1, 40, temp_msg);
	// gen32_do_log(1, 40, (char *) "\n");
#endif

    if (what == 99 && where == 1) {
        constraints_initialized = false;
        loops_initialized = false;
        hoffer_initialized = false;
        debugging_initialized = false;
        hoffer_setup_flag = 0;

        sprintf(constraint_message,"library initialization parameters set");
        return constraint_message;
    }
    if (what == ID_VERSION && wrong_command  != 200) {
        sprintf(constraint_message,"Version 7.20 (extra command %d)",wrong_command);
        wrong_command = 200;
        return constraint_message;
    }
    if ((what == ID_VERSION) && version_sent == 0) {
        // fprintf(stderr,"initializing to 0");
        // char *temp=cpp_hoffer_bid((const char *)"akq43.ak43.a76.4",(const char *)"P",0,0);

        // strcpy(constraint_message, temp); //cpp_hoffer_bid((const char *)"akq43.ak43.a76.4",(const char *)"P",0,0));
        strcpy(constraint_message,"Version 7.20 (is not initialized)");
#ifdef USE_STATE
        if (what==ID_VERSION) CPP_DO_STATE(ID_VERSION);
#endif
        version_sent = 1;
#if defined(VERSION_SENT_BLOCKS)
        return constraint_message;
#endif
    }

    if (!debugging_initialized) {
        debugging_flag = 127;
        debugging_level = 20;
        debugging_meta = 1;
        if (debugging_meta)
            CSW_EXEC(fprintf(stderr,"CONSTRAINT_COMMAND:debugging initialized\n"));
        debugging_initialized = true;
        }

#if defined(HOFFER_SETUP)    
    if (!hoffer_initialized) {
        if ((what == ID_VERSION || what == ID_RESET) && version_sent == 1) {
            strcpy(constraint_message, "Version 7.20 (2) calling do_hoffer_setup");
            version_sent = 2;
#ifdef USE_STATE
            CPP_DO_STATE(2);
#endif
#if defined(VERSION_SENT_BLOCKS)
            return constraint_message;
#endif
        }

        do_hoffer_setup();
        if (hoffer_setup_flag != 32) {
            sprintf(constraint_message,"ERR: hoffer_setup failed at:%d",hoffer_setup_flag);
            return constraint_message;
            }
        else gen32_do_log(1,40,"Version 7.20 of bidding setup = ok");

        if (version_sent == 2) {
            if (hoffer_setup_flag == 32) {
                sprintf(constraint_message, "Version 7.20 (3) hoffer setup ok");
                hoffer_initialized = true;
                version_sent = 3;
#if defined(VERSION_SENT_BLOCKS)
                return constraint_message;
#endif
                
            }
            else {
                sprintf(constraint_message,"hoffer_setup failed at:%d",hoffer_setup_flag);
                return constraint_message;
                }
            }

        }
#endif

    if ((what == ID_VERSION || what == ID_RESET) && version_sent == 3) {
        strcpy(constraint_message,"Version 7.20 (testing finished)");
        version_sent = 4;
#if defined(VERSION_SENT_BLOCKS)
        return constraint_message;
#endif
    }

	if (what == ID_VERSION) { // added 220408 for mscript2.exe debugging
		strcpy(constraint_message, "Version 7.20");
		return constraint_message;
	}


    if (what == ID_DEBUGGING) {
        debugging_flag = where;
        debugging_meta = low;
        debugging_level= hi;
        if (debugging_flag > 128)
            fprintf(stderr,"bad debugging flag:%u",debugging_flag);
        if (debugging_level != 10 && debugging_level !=20 && debugging_level != 30 && debugging_level != 40)
            fprintf(stderr,"bad debugging level:%u",debugging_level);
        sprintf(constraint_message,"debugging:sections:%u level:%u (sections 1-127; levels:10,20,30,40)\n",debugging_flag,debugging_level);
        if (debugging_meta)
            fprintf(stderr,"CONSTRAINT_COMMAND:ID_DEBUGGING: %d %d %d %d %d\n",who,what,where,low,hi);
        if (debugging_flag & 1 && debugging_level == 10)
            CSW_EXEC(fprintf(stderr,"constraint_command called: %d %d %d %d %d\n",who, what, where, low, hi));
        return constraint_message;
        }
#ifndef AMZI_PLAY
// #pragma message("AMZI_PLAY is not defined for set_contract")
    else if (what == ID_SET_CONTRACT) {
        sprintf(constraint_message,"contract set is not implemented: to %d for declarer %d",where,who);
        if (debugging_level <= 20) CSW_EXEC(fprintf(stderr,"%s\n",constraint_message));
        return constraint_message;
        }

#else
// #pragma message("AMZI_PLAY is defined so set_contract")
#ifdef OSX
    else if (what == ID_SET_CONTRACT) {
        sprintf(constraint_message,"contract set is not implemented: to %d for declarer %d",where,who);
        if (debugging_level == 10) CSW_EXEC(fprintf(stderr,"%s\n",constraint_message));
        return constraint_message;
        }
#endif
    else if (what == ID_SET_CONTRACT) {
        char command[100];
#if defined(CSW)
        sprintf(command,"set contract: declarer:%d contract:%d\n",who,where);
        TRIGGER(1499,gen32_do_log(1,30,command));
#endif
        int trump = where % 10;
        cpp_call_amzi("retractall(trump(_))");
        sprintf(command,"trump(%d)",trump);
        cpp_assert_amzi(command);
        // sprintf(command,"trump(%d)",trump);
        // cpp_assert_amzi(command);
        sprintf(command,"declarer(%d)",who);
        cpp_set_declarer(who);
        cpp_assert_amzi(command);
        // CSW_EXEC(fprintf(stderr,"declarer asserted: %s\n",command));
        
        sprintf(constraint_message,"OK contract set to %d for declarer %d trump is %d",where,who,trump);
        // CSW_EXEC(fprintf(stderr,"%s\n",constraint_message));
        ALOG("LUDDY %s",constraint_message);
#ifdef USE_STATE
        CPP_DO_STATE(5);
#endif
        return constraint_message;
    }
// #endif // OSX
#endif // AMZI_PLAY

#ifndef NO_CSW
    else if (what == ID_SET_CSW) {
        // all CSW_EXEC macros will be turned on, mostly fprintf(stderr statements
        if (where>0) {
            getargs2("-sxdeal.c");
            sprintf(constraint_message,"OK xdeal.c debugging output turned on");
			set_logging_level(where);
            }
        else {
            clear_csw();
            sprintf(constraint_message,"OK debugging output turned off (3rd constraint of ID 85 was 0)");
            }
        CSW_EXEC(fprintf(stderr,"constraint_command:ID_SET_CSW:level=%u:%s\n",get_logging_level(),constraint_message));
        return constraint_message;
        }
#endif
    else if (what == ID_RESET) {
// #pragma message("at ID_RESET")
		/* reset constraints - added the ability to reset a portion of the constraints, keeping lower 200421 */
        int i;
		for (i = 0; i < 52; i++) {
			gen32_deck_position[i] = i;
			decks[0][i] = i;
			}
        sprintf(knows_constraint_err_msg,"1-");
        initialize();
        deck_verify(0);
        set_offsets(15);      
        clear_constraints(0);
        prolog_accepting_cards = true;
        if (debugging_flag & 1 && debugging_level <=20) CSW_EXEC(fprintf(stderr,"CONSTRAINT_COMMAND:ID_RESET entered\n"));
 
#if !defined(GENERATE_ONLY)
        if (!constraints_initialized) {
            cpp_initialize_constraints();
            constraints_initialized = true;
            strcat(knows_constraint_err_msg,"2-");
            if (debugging_flag & 1) CSW_EXEC(fprintf(stderr,"constraints initialized\n"));
#ifdef USE_STATE
            CPP_DO_STATE(6);
#endif
            }
#endif
        
        if (!hoffer_initialized) strcat(knows_constraint_err_msg,"h");
        else strcat(knows_constraint_err_msg,"H");
// #pragma message("at ID_RESET still")

#if defined(AMZI) || defined(AMZI_THERE_TEST2)
// #pragma message("AMZI is on")
#if defined(AMZI_THERE) || defined(AMZI_THERE_TEST2)
// #pragma message("AMZI_THERE ok for cpp_initialize_amzi and beyond")
        if (!amzi_there_initialized) {
            TRIGGER(ID_INITIALIZE,gen32_do_log(1,30,"calling amzi_initialize from constraint_command"));
            strcat(knows_constraint_err_msg,"3-");
            char *out=NULL;
            cpp_initialize_amzi();
            gen32_do_log(1,40,"cpp_initialize_amzi finished v7.20");
            
            // cpp_str_call_amzi("initialize_logging(X)",1);
            cpp_call_amzi("retractall(console_level(_))");
            cpp_assert_amzi("console_level(40)");
            // cpp_assert_amzi("log_section(1)");
            cpp_call_amzi("retractall(log_level(_))");
            cpp_assert_amzi("log_level(40)");
            // cpp_assert_amzi("log_section(1001)");
            cpp_assert_amzi("log_section(88)");
            // cpp_str_call_amzi("log([88],20,'Testing console logging for prolog v6.9',X)",4);
            out = cpp_str_call_amzi("initialize_all(X)",1);
            // (fprintf(stderr,"initialize_all:%s\n",out));
            TRIGGER(ID_INITIALIZE,gen32_do_log(1,30,"amzi initialized; initialize_all just completed in prolog\n"));
            amzi_there_initialized=true;
            }
                
        cpp_call_amzi("retractall(hand(_,_,_))");
        cpp_call_amzi("set_cards(0,53)");
        cpp_call_amzi("set_cards(0,52)");
        cpp_call_amzi("set_cards(1,52)");
        cpp_call_amzi("set_cards(2,52)");
        cpp_call_amzi("set_cards(3,52)");
#endif
#endif



#if defined(CSW) && defined(WIN32)
		// set_trigger("302");
#endif

        if (!loops_initialized) {
#ifdef BEST_HOFFER
        // setup hand generation - so that South, the human, has less rigourous constraints.

            /* 21215  using ULTIMATE_FIX which presents returning failed hands
             so setting loops to 2000 is OK, higher might be better
             Also, changed the 50 to 500 for 2nd parameter of exchanges on 2 South
            */
		gen32_set_loops(2000); // has been at 1000, trying 2000 200902
		// dir, modulo, delta, threshhold
		gen32_set_individual_exchanges(0,400,1,1); // allowed robots some relaxed 200903
		gen32_set_individual_exchanges(1,400,1,1);
		gen32_set_individual_exchanges(2,400,1,1); // has been set at 50, 200 going to 100 now 200902
		gen32_set_individual_exchanges(3,400,1,1);
        // CSW_EXEC(fprintf(stderr,"Parameters for dealing hands set (with south as human relaxed)\n"));
#else
#ifdef BETTER_HOFFER
		gen32_set_exchanges(1000,400,1,1); // number_of_loops, modulo count, threshhold, threshhold increment
#else
		gen32_set_exchanges(10000,4000,1,1);
#endif
#endif
        loops_initialized = true;
        exchanges_flag = true;
        }

        strcpy(constraint_message,"");
        if (!gen_initialized) {
            initialize();
            strcat(knows_constraint_err_msg,"4-");
            if (debugging_flag & 1 && debugging_level <= 20) CSW_EXEC(fprintf(stderr,"hand generation initialized\n"));
            }
        else {
            gen_init2();
            if (debugging_flag & 1 && debugging_level <= 10) CSW_EXEC(fprintf(stderr,"hand generation reset\n"));
            }
        // sprintf(constraint_message,"OK ID_RESET completed");
        strcpy(constraint_message,knows_constraint_err_msg);
        if (debugging_flag & 1 && debugging_level == 10) CSW_EXEC(fprintf(stderr,"CONSTRAINT_COMMAND%s\n",constraint_message));
        return constraint_message;
        }
	else if (what == ID_GET_COUNT) {
        // the count of the global constraints
		sprintf(constraint_message,"%d",constraint_index);
		return constraint_message;
		}
#if defined(AMZI)
#ifdef AMZI_THERE
    else if (what == ID_GET_LEAD) {
        char *temp;
        temp = cpp_amzi_get_a_lead(who);
        strcpy(constraint_message,temp);
        if (debugging_level == 10) CSW_EXEC(fprintf(stderr,"%s\n",constraint_message));
        return constraint_message;
        }
#endif
#endif
#define PC_BUF_SIZE 250
	else if (what == ID_PRINT_CONSTRAINTS) {
		int p1, p2, p3, p4, p5;
		char temp_msg[PC_BUF_SIZE+PC_BUF_SIZE], temp_msg2[PC_BUF_SIZE], *ptr;
		constraint_message[0]='\0';
		for (int i=0; i<constraint_index; i++) {
            int len1=0, len2=0;
            p1=constraints[i].who;
			p2=constraints[i].what;
			p3=constraints[i].where;
			p4=constraints[i].lo;
			p5=constraints[i].hi;
            ptr = get_constraint_message(p1,p2,p3,p4,p5);
            for (int i=0; i<PC_BUF_SIZE-1 && ptr[i] != '\0'; i++) { temp_msg2[i]=ptr[i]; temp_msg2[i+1]='\0'; }
			sprintf(temp_msg,"my_constraint_command(%d, %d, %d, %d, %d) // %d (of %d)  ",p1,p2,p3,p4,p5,i,constraint_index);
            strncat(temp_msg,temp_msg2,PC_BUF_SIZE);
            strcat(temp_msg,"\n");

			len1 = (int) strlen(temp_msg);
			len2 = (int) strlen(constraint_message);
			if (len1 + len2 < MSG_SIZE - 2) strcat(constraint_message,temp_msg);
			else if (len2 < MSG_SIZE - 5) strncat(constraint_message,"...",4);
			}
        // strcpy(constraint_message,"ALOGGED constraints");
		return constraint_message;
		}

    else if (what == ID_VERSION) {
            if (what == ID_VERSION && version_sent >= 4) {
                strcpy(constraint_message, "Version 7.20 (initialized and reset)");
                version_sent = 5;
                return constraint_message;
            }
        }
    else if (what == ID_UNITY_TESTS) {
#if !defined(ANDROID) && !defined(__ANDROID__)
        return do_all_unity_tests();
#endif
    }
    else if (what == ID_SHUFFLE) {
        int start, total;
        start = low; total = hi;
        if (start < 0 || start + total >= DECKNUM || total < 1) {
            strcpy(constraint_message,"bad parameters for shuffle");
            return(constraint_message);
            }
        else {
            gen32_shuffle(gen32_deck_position,start,total);
            sprintf(constraint_message,"OK. shuffled %u hands starting at %u",total,start);
            return constraint_message;
            }
        }
    else if (what == ID_DEAL) {int result;
		if (!exchanges_flag) {
			strcpy(constraint_message,"ERR. exchanges not set yet");
			return constraint_message;
			}

        if (low < 0 || low + hi >= DECKNUM || hi < 1) {
            strcpy(constraint_message,"ERR. bad parameters for deal");
            return constraint_message;
            }
        result = constraint_deal_exclude(low,hi,where);
#ifndef SKIP_BAD_HANDS
        if (result == -1) {
            sprintf(constraint_message,"ERR");
            return constraint_message;
            }
#endif
        if (hi > max) { max = hi; }
        sprintf(constraint_message,"OK. Hands generated: %u, placed starting at %u. Max hand seen is %u; result is %d",hi,low,max,result);
        CSW_EXEC(fprintf(stderr,"%s\n",constraint_message));

        return constraint_message;
        }
    else if (what == ID_GET_PLAYING_HAND) {
        int position=0; // we use position because we may have shuffled decks (results in gen32_deck_position)
        if (where < 0 || where >= max) {
            sprintf(constraint_message,"ERR. p3 of get hand command is %d",where);
#if defined(ALOGGING)
            ALOG("LUDDY:%s",constraint_message);
#endif
            return constraint_message;
            }
        else {
			if (decks_flag[where]==false) {
				sprintf(constraint_message,"ERR. could not fulfill constraints for hand %d",where);
				return constraint_message;
				}
            position = gen32_deck_position[where];
            hand =  get_dealt_hands(position,1);
            if (hand != NULL) {
                sprintf(constraint_message,"N:%s",hand);
#if defined(ALOGGING)
                ALOG("LUDDY:%d:%s",where,constraint_message);
#endif

                return constraint_message;
                }
            else {
                strcpy(constraint_message,"ERR. parameter 3 of get hand returns NULL");
#if defined(ALOGGING)
                ALOG("LUDDY:%s",constraint_message);
#endif
                return constraint_message;
                }
            }
        }
    else if (what == ID_GET_HAND) {
        int position=0; // we use position because we may have shuffled decks (results in gen32_deck_position)
        if (where < 0 || where >= max) {
            sprintf(constraint_message,"ERR. p3 of get hand command is %d",where);
            return constraint_message;
            }
        else {
			if (decks_flag[where]==false) {
				sprintf(constraint_message,"ERR. could not fulfill constraints for hand %d",where);
				return constraint_message;
				}
            position = gen32_deck_position[where];
            hand =  get_dealt_hands(position,0);
            if (hand != NULL) {
                sprintf(constraint_message,"N:%s",hand);

                return constraint_message;
                }
            else {
                strcpy(constraint_message,"ERR. parameter 3 of get hand returns NULL");
                return constraint_message;
                }
            }
        }
    else if (what == ID_PRINT_HANDS) {
        int position=0,i; // we use position because we may have shuffled decks (results in gen32_deck_position)
		constraint_message[0]='\0';
		char temp_msg[250];
        if (low < 0 || low + hi >= DECKNUM || hi < 1) {
            sprintf(constraint_message,"ERR. bad input to print hands start:%u total:%u",low,hi);
            return constraint_message;
            }
        else {
            for (i=0; i< hi; i++) {
				if (decks_flag[i]==false && where != 1) { // should be able force print hands, set where to 1
					sprintf(temp_msg,"ERR. could not fulfill constraints for hand %d\n",i);
					strcat(constraint_message,temp_msg);
					}

				else {
					position = gen32_deck_position[low+i];
					hand =  get_dealt_hands(position,0);
					sprintf(temp_msg,"%s\n",hand);
					strcat(constraint_message,temp_msg);
					}
                }
            sprintf(temp_msg,"OK. %u hands printed, starting at %u\n",hi-low,low);
			strcat(constraint_message,temp_msg);
            return constraint_message;
            }
        }
    else if (what == ID_PRINT_PLAYING_HANDS) {
        int position=0,i; // we use position because we may have shuffled decks (results in gen32_deck_position)
        if (low < 0 || low + hi >= DECKNUM || hi < 1) {
            sprintf(constraint_message,"ERR. bad input to print hands start:%u total:%u",low,hi);
            return constraint_message;
            }
        else {
            for (i=0; i< hi; i++) {
                position = gen32_deck_position[low+i];
                hand =  get_dealt_hands(position,1);
                fprintf(stderr,"%s\n",hand);
                }
            sprintf(constraint_message,"OK. %u hands printed, starting at %u",hi,low);
            return constraint_message;
            }
        }
    else {
#if !defined(linux)
#if !defined(GODOT)
	char log_msg[100];
#endif
#endif
        if (what == ID_PLAYED) prolog_accepting_cards = false;
#if !defined(GODOT)
#if defined(CSW)
            // sprintf(log_msg,"adding constraint %d %d %d %d %d\n",who,what,where,low,hi);
            // TRIGGER(1500,gen32_do_log(1,20,log_msg));
#endif
#endif
			result = add_constraint(who,what,where,low,hi);
#if !defined(GODOT)
#if defined(CSW)
            // sprintf(log_msg,"done adding constraint, number of constraints is %d\n",result);
            // TRIGGER(1500,gen32_do_log(1,10,log_msg));
#endif
#endif
			if (what == ID_KNOW && prolog_accepting_cards == true) {
            char temp[100]; int suit = where / 13;
            sprintf(temp,"set_cards(%d,%d)",who,where);
#if !defined(GODOT)
            // TRIGGER(1499,gen32_do_log(1,30,temp));
#endif
#ifdef AMZI
#if !defined(ANDROID) && !defined(__ANDROID__)
            cpp_call_amzi(temp);
#endif
#endif
            sprintf(temp,"count(%d,%d,X)",who,suit);
#if defined(AMZI)
            length = cpp_int_call_amzi((const char *)temp,3);
            sprintf(temp,"length is %u",length);
            TRIGGER(1499,gen32_do_log(1,20,temp));
#endif
            }
        if (result != ADD_CONSTRAINT_ERROR && result != KNOWS_CONSTRAINT_ERROR) {
            sprintf(constraint_message,"OK. %u ",result);
            msg = get_constraint_message(who,what,where,low,hi);
            strcat(constraint_message,msg);
            if (debugging_level == 10) { CSW_EXEC(fprintf(stderr,"%s\n",constraint_message)); }
            return constraint_message;
            }
        else {
            if (result == KNOWS_CONSTRAINT_ERROR) sprintf(constraint_message,"ERR. KC. %s",knows_constraint_err_msg);
            else sprintf(constraint_message,"ERR. bad codes (%d) %u %u %u %u %u",result,who,what,where,low,hi);
            if (what == 7 && where >= result) {
                fprintf(stderr,"ERR. Not enough constraints for 7 control\n");
#ifdef USE_STATE
                CPP_DO_STATE(13);
#endif
                gen32_do_log(1,40,(const char *)"ERR. Not enough constraints for 7 control");
                }
            return constraint_message;
            }
        }
    sprintf(constraint_message,"ERR:constraint_command. unknown");
#ifdef USE_STATE
    CPP_DO_STATE(13);
#endif
    return constraint_message;
    }

char get_message[100];

void show_low_hi(int low, int hi) {
    char temp[100];
    sprintf(temp,"lo:%d hi:%d",low,hi);
    strcat(get_message,temp);
    }

void show_suit(int suit) {
    switch(suit) {
        case 0: strcat(get_message,"Clubs "); break;
        case 1: strcat(get_message,"Diamonds "); break;
        case 2: strcat(get_message,"Hearts "); break;
        case 3: strcat(get_message,"Spades "); break;
    }
}

void show_where(int where) {
    char temp[100];
    sprintf(temp,"%d ",where);
    strcat(get_message,temp);
}

void show_card(int card) {
    switch (card / 13) {
        case 0: strcat(get_message,"C"); break;
        case 1: strcat(get_message,"D"); break;
        case 2: strcat(get_message,"H"); break;
        case 3: strcat(get_message,"S"); break;
        default:  break;
        }
    switch (card % 13) {
        case 0: strcat(get_message,"2 "); break;
        case 1: strcat(get_message,"3 "); break;
        case 2: strcat(get_message,"4 "); break;
        case 3: strcat(get_message,"5 "); break;
        case 4: strcat(get_message,"6 "); break;
        case 5: strcat(get_message,"7 "); break;
        case 6: strcat(get_message,"8 "); break;
        case 7: strcat(get_message,"9 "); break;
        case 8: strcat(get_message,"T "); break;
        case 9: strcat(get_message,"J "); break;
        case 10: strcat(get_message,"Q "); break;
        case 11: strcat(get_message,"K "); break;
        case 12: strcat(get_message,"A "); break;
        default:  break;
        }
}

char *get_constraint_message(int who, int what, int where, int low, int hi) {
	get_message[0]='\0';
	switch (who) {
		case 0: strcat(get_message,"N "); break;
		case 1: strcat(get_message,"E "); break;
		case 2: strcat(get_message,"S "); break;
		case 3: strcat(get_message,"W "); break;
		default: break;
		}
	switch (what) {
		case ID_PLACING_CARDS: strcat(get_message, "allows ID_HAS to be functional");  break;
        case ID_HCP: strcat(get_message,"HCPs "); show_low_hi(low,hi); break;
        case ID_DIST: strcat(get_message,"length of suit "); show_suit(where); show_low_hi(low,hi);  break;
        case ID_LEN: strcat(get_message,"dist by position "); show_where(where); show_low_hi(low,hi);  break;
		case ID_AND: strcat(get_message,"and control "); break;
		case ID_OR: strcat(get_message,"or control "); break;
		case ID_SUM: break;
		case ID_SUB: break;
		case ID_DIFF: strcat(get_message,"diff control "); break;
		case ID_VALUE: break;
		case ID_HAS: strcat(get_message,"has ");  show_card(where); break;
		case ID_NOT: strcat(get_message,"not control "); break;
		case ID_XOR: strcat(get_message,"xor control "); break;
		case ID_QUALITY: strcat(get_message,"quality "); break;
        case ID_STOPPER: strcat(get_message,"stopper "); show_where(where); show_low_hi(low,hi);  break;
		case ID_ROTH: strcat(get_message,"roth pts"); show_low_hi(low,hi);  break;
		case ID_COMPARE: strcat(get_message,"compare"); 
			switch (where) {
                case ID_LESS: strcat(get_message," less"); break;
                case ID_MORE: strcat(get_message," more"); break;
                case ID_MORE_OR_EQ: strcat(get_message," more or equal"); break;
                case ID_LESS_OR_EQ: strcat(get_message," less or equal"); break;
                case ID_EQUAL: strcat(get_message," equal"); break;
                }
			break;
        case ID_LESS: strcat(get_message,"less comparison "); show_suit(low); strcat(get_message,"less than "); show_suit(hi); break;
        case ID_MORE: strcat(get_message,"more comparison "); show_suit(low); strcat(get_message,"more than "); show_suit(hi); break;
        case ID_LESS_OR_EQ: strcat(get_message,"<= comparison "); show_suit(low); strcat(get_message,"<= "); show_suit(hi); break;
        case ID_MORE_OR_EQ: strcat(get_message,">= comparison "); show_suit(low); strcat(get_message,">= "); show_suit(hi); break;
		case ID_EQUAL: break;
        case ID_HASNOT: strcat(get_message,"hasnot "); show_card(where); break;
		case ID_ACES: strcat(get_message,"aces "); show_low_hi(low,hi);  break;
		case ID_NOP: strcat(get_message,"nop "); break;
		case ID_NOP_CONSTRAINTS: break;
		case ID_NOP_DIR: break;
		case ID_LOSERS: strcat(get_message,"losers "); show_low_hi(low,hi);  break;
		case ID_SUIT_QUICKS: strcat(get_message,"suit quicks ");  show_suit(where); show_low_hi(low,hi);  break;
		case ID_QUICKS: strcat(get_message,"quick tricks "); show_low_hi(low,hi);  break;
		case ID_INOUT: break;
		case ID_ORANDS: strcat(get_message,"or of two groups"); show_low_hi(low,hi); break;
		case ID_FORCE: strcat(get_message,"forcing "); break;
		case ID_SIMULATE: break;
		case ID_CONTROLS: strcat(get_message,"controls "); show_low_hi(low,hi);  break;
        case ID_ODD: strcat(get_message,"odd "); break;
        case ID_EVEN: strcat(get_message,"even "); break;
        case ID_PLAYED: strcat(get_message,"played "); show_card(where); break;
        case ID_KNOW: strcat(get_message,"know "); show_card(where); break;
		case ID_VOID: strcat(get_message,"void ");  show_suit(where); break;
		case ID_OUT: strcat(get_message,"shows out "); show_suit(where); break;
		case ID_INTERMEDIATES: strcat(get_message,"intermediates "); show_low_hi(low,hi);  break;
		case ID_OD_OVER: break;
		case ID_OD_UNDER: break;
		case ID_REMOVE: strcat(get_message,"remove "); break;
		case ID_SUPPORT: strcat(get_message,"support pts ");  show_suit(where); show_low_hi(low,hi);  break;
		case ID_SUPPORTED: strcat(get_message,"supported pts "); show_low_hi(low,hi);  break;
		case ID_SINGLETON: strcat(get_message,"singleton "); show_suit(where);  break;
		case ID_RESET: break;
		case ID_DEAL: break;
		case ID_GET_HAND: break;
		default: strcat(get_message,"invalid id "); break;
		}
	return get_message;
	}

bool confirm_lead_info1(int who) {
    if (already_placed[who] != 13) return 1;
    else return 0;
    }

int get_constraint_value(int item, int position) {
    switch (position) {
        case 0: return constraints[item].who; break;
        case 1: return constraints[item].what; break;
        case 2: return constraints[item].where; break;
        case 3: return constraints[item].lo; break;
        case 4: return constraints[item].hi; break;
    }
    return 0;
}

int who_has[52];  // keep track of ID_KNOW ID_PLAYED

#define EARLY_PLACING
bool allow_placing=false;

char place_card_buf[250];
char *place_card(int who, int what, int where, int low, int hi) {
    // 201024 changed from int to char * return value to get better error messages
    int next_placed,j,v;
#if !defined(linux)
    int temp_who,temp_what;
    temp_who=who;
    temp_what=what;
#endif
#ifdef EARLY_PLACING
//    Placing means to force this card from some other hand into the placed cards for this hand
//    This can be done when the constraint is added or during the evaluation phase
    // notice that allow_placing was added 211215
    if (what == ID_HAS && allow_placing) {
        v=1;
        if (who > 3 || who < 0) { // bad who
            sprintf(place_card_buf,"bad who in place_card:%d",who);
            return place_card_buf;
        }
        else if (already_placed[who] > 13) ; // too many cards
        else for (j=13-already_placed[who]; j<13 && v==1; j++)    // placed cards at top
                if (decks[active_hand][who*13+j] == where) v=0;  // in this hand
        
        if (already_placed[who] > 12) ;  // filled already no reason to verify or waste time
        else if (v == 0) ; // already placed (so possibly an error) into this hand
        else {
            for (j=0; j<52; j++)
                if (decks[active_hand][j] == where) {
                    // fprintf(stderr,"breaking on:%d",j);
                    break;
                    }
                // find the card, see if an active hand, if so move to top of this hand
            next_placed = who*13 + 12 - already_placed[who];
            do_exchange(j,next_placed);
            already_placed[who] += 1;
            already_placed_count += 1;
            }
        }
    
    // added 200930 while TRICKY_BRIDGE was crashing on adding played cards
    switch(what) {
    
case ID_PLAYED:   // 170330, new, places a card, was ID_HAS; this fall-through is not an error.
    already_played_flag = 1;
    // fall through
case ID_KNOW:
    v=1; // look for a specific card
    if (who <0 || who > 3) {
        // CSW_EXEC(fprintf(stderr,"improper who in ID_PLAYED or ID_KNOW\n"));
        // fprintf(stderr,"DB. improper who in ID_PLAYED or ID_KNOW\n");
        sprintf(place_card_buf,"improper who:%d",who);
#ifndef NO_ASSERTS
        if (!debugging_flag) assert(who >= 0 && who <=3);
#endif
        return place_card_buf;
        }
    if (already_placed[who] > 13) {
        // CSW_EXEC(fprintf(stderr,"improper who in ID_PLAYED or ID_KNOW\n"));
        // fprintf(stderr,"DB. improper who in ID_PLAYED or ID_KNOW\n");
        sprintf(place_card_buf,"already placed for:%d is greater than 13 at:%d",who,already_placed[who]);
#ifndef NO_ASSERTS
        if (!debugging_flag) assert(already_placed[who]<14);
#endif
        return place_card_buf;
        }
    if (already_played_flag) {
        if (!already_played[where]) already_played[where] = 1;
        else {
            // CSW_EXEC(fprintf(stderr,"place_card:card %d has already been played (early) \n",where));
            // fprintf(stderr,"DB. place_card:card %d has already been played (early) \n",where);
            sprintf(place_card_buf,"card:%d has already been played (early)",where);
#ifndef NO_ASSERTS
            if (!debugging_flag) assert(already_played[where]==0);
#endif
            
            return place_card_buf;
            }
        }
    for (j=13-already_placed[who]; j<13 && v==1; j++)  // placed cards at top
         if (decks[active_hand][who*13+j] == where) v=0;  // the card is already placed in this hand
    if (v == 0) {  // fprintf(stderr,"already in hand\n");
        already_played_flag = 0;
        who_has[where]=who; // keep track of played cards while processing the constraints for use with ID_OUT
        break; }
    if (already_placed[who]>12) { already_played_flag = 0;
        if (debugging_flag & 2)
            // CSW_EXEC(fprintf(stderr,"trying to place too many cards for %d\n",who));
            // fprintf(stderr,"DB. trying to place too many cards for %d\n",who);
            sprintf(place_card_buf,"trying to place too many cards for %d",who);
        return place_card_buf; } // cannot handle card out of already placed, i.e., into another hand
    for (j=0; j<52; j++)
        if (decks[active_hand][j] == where) break;    // find card and break when found
 //   assert ((1 << (j/13)) & indicator); // the sought for card is in an active hand; remove later
    // find the card, see if an active hand, if so move to top of this hand
    if ((1<< (j/13)) & indicator) {
        next_placed = who*13 + 12 - already_placed[who];
        do_exchange(j,next_placed);
        already_placed[who] += 1;
        already_placed_count += 1;
        who_has[where]=who; // keep track of played cards while processing the constraints for use with ID_OUT
//            fprintf(stderr,"Already for %u is %u; all placed is %u\n",who,already_placed[who],already_placed_count);
        if (debugging_flag & 2 && debugging_level == 10)
            CSW_EXEC(fprintf(stderr,"PLACING:placed %d in hand %d\n",where,who));
        }
    already_played_flag = 0; // huge fix from 200811 ID_KNOWS after ID_PLAYED went to played
    break;

    }

#endif
    sprintf(place_card_buf,"OK");
    return place_card_buf; // legal to place this card
}

int add_constraint(int who, int what, int where, int low, int hi) {
    
    if (what == ID_PLACING_CARDS) allow_placing = true;
    if (who > 3 || who < 0) return ADD_CONSTRAINT_ERROR;
    if (what > ID_NOP || what < 0) return ADD_CONSTRAINT_ERROR;
	if (what == ID_ORANDS) { if (low < 1 || hi < 1) what = ID_NOP;} // illegal number of constraints 
    else if (low < 0 || (low > hi && what != ID_COMPARE))
        what = ID_NOP; // illegal parameter
    if (where < 0) return ADD_CONSTRAINT_ERROR;
    if (what == ID_DIFF && where >= constraint_index) return ADD_CONSTRAINT_ERROR; // wrong number of constraints
    if (what == ID_HAS && (where < 0 || where > 51)) return ADD_CONSTRAINT_ERROR; // illegal card
    if (what == ID_PLAYED && (where < 0 || where > 51)) return ADD_CONSTRAINT_ERROR; // illegal card
    if (what == ID_KNOW && (where < 0 || where > 51)) return ADD_CONSTRAINT_ERROR;  // illegal card
    if (what == ID_HASNOT && (where < 0 || where > 51)) return ADD_CONSTRAINT_ERROR; // illegal card
    if (what == ID_VOID && (where < 0 || where > 3)) return ADD_CONSTRAINT_ERROR; // illegal suit for void
	if (what == ID_OUT && (where < 0 || where > 3)) return ADD_CONSTRAINT_ERROR; // illegal suit showing out
    if (what == ID_SINGLETON && (where < 0 || where > 3)) return ADD_CONSTRAINT_ERROR; // illegal suit singleton
    if (what == ID_STOPPER && (where < 0 || where > 3))  return ADD_CONSTRAINT_ERROR;  // illegal suit stopper
    if (what == ID_STOPPER && (low < 0 || hi > 2))  return ADD_CONSTRAINT_ERROR; // illegal value for stopper
    if (what == ID_QUALITY && (where < 0 || where > 3))  return ADD_CONSTRAINT_ERROR; // illegal suit
    if (what == ID_QUALITY && (low < 0 || hi > 7))  return ADD_CONSTRAINT_ERROR; // illegal value for quality
    if (what == ID_LEN && (low < 0 || hi > 9))  return ADD_CONSTRAINT_ERROR; // illegal values for length
    if (what == ID_LEN && (where < 0 || where > 3))  return ADD_CONSTRAINT_ERROR;  // illegal suit for len 
    if (what == ID_DIST && (low < 0 || hi > 9))  return ADD_CONSTRAINT_ERROR;  // illegal value for suit length
    if (what == ID_DIST && (where < 0 || where > 3))  return ADD_CONSTRAINT_ERROR; // illegal suit
    if (what == ID_ODD && (where < 0 || where > 3))  return ADD_CONSTRAINT_ERROR; // illegal suit
    if (what == ID_EVEN && (where < 0 || where > 3))  return ADD_CONSTRAINT_ERROR; // illegal suit
    if (what == ID_COMPARE && (where < ID_LESS || hi > ID_EQUAL))  return ADD_CONSTRAINT_ERROR; // illegal op 
    if (what == ID_COMPARE && (low < 0 || low > 3))  return ADD_CONSTRAINT_ERROR; // illegal suit for compare
    if (what == ID_COMPARE && (hi < 0 || hi > 3))  return ADD_CONSTRAINT_ERROR; // illegal suit for compare
    if (what == ID_CONTROLS && (low < 0 || low > 5))  return ADD_CONSTRAINT_ERROR; 
    if (what == ID_ACES && (low < 0 || low > 4))  return ADD_CONSTRAINT_ERROR; 
    if (what == ID_ACES && (hi < 0 || hi > 4))  return ADD_CONSTRAINT_ERROR; 
    if (what == ID_ACES && low > hi)  return ADD_CONSTRAINT_ERROR; 
    if (what == ID_CONTROLS && (hi < 0 || hi > 5)) return ADD_CONSTRAINT_ERROR; // note that 4 can be considered, i.e, there is no king
    if (what == ID_VALUE && (where < 0 || where >= constraint_index)) return ADD_CONSTRAINT_ERROR; // illegal reference to another constraint
    if ((what == ID_SUM || what == ID_SUB) && (hi < 0 || hi >= constraint_index)) return ADD_CONSTRAINT_ERROR;  // illegal reference
    if ((what == ID_SUM || what == ID_SUB) && (low < 0 || low >= constraint_index)) return ADD_CONSTRAINT_ERROR;  // illegal reference 
	if (what == ID_NOP) return ADD_CONSTRAINT_ERROR;

#ifndef NO_LOGGING
    if (what == ID_NOP) {
        sprintf(temp_msg,"ADD CONSTRAINT ERR:%u %u %u low:%u hi:%u\n",who,what,where,low,hi);
        gen32_do_log(1,30,temp_msg);
        }
    else {
        sprintf(temp_msg,"ADD CONSTRAINT:%u %u %u low:%u hi:%u\n i:%u",who,what,where,low,hi,constraint_index);
        gen32_do_log(1,10,temp_msg);
        }
#endif

    if (what == ID_HAS || what == ID_PLAYED || what == ID_KNOW) { // check to see if card placement is a legal placement
        strcpy(knows_constraint_err_msg,place_card(who,what,where,low,hi));
        if (knows_constraint_err_msg[0]=='O' && knows_constraint_err_msg[1] == 'K') ;
        else return KNOWS_CONSTRAINT_ERROR;
        }
    constraints[constraint_index].who = who;
    constraints[constraint_index].what = what;
    constraints[constraint_index].where = where;
    constraints[constraint_index].hi = hi;
    constraints[constraint_index].lo = low;
    return constraint_index++;
    }


// allow_placing is about actually moving cards from one hand to another
// because of the ID_HAS constraint. It could be needed when generating
// hands and cards are explicitly placed, but that is not needed for Tricky Bridge.
// It should only occur when the cards are actually known such as when dummy is displayed.
// There is ID_PLAYED but that really means the card has been played.
// The problem arises in that this should not occur during the bidding, such as
// when a control has been bid and there is an ID_OR for having shortness or the control.
// You cannot place at that time. So you must use a constraint command to allow placing.
// That constraint is 58 ID_PLACING_CARDS and it will set allow_placing.
// This code change was added 211215. Note that the add_constraint is needed or
// ID_HAS will never be functional.

void clear_constraints(int start) {
    if (start==0) allow_placing=false;
// now constraints can be cleared other than from 0
    int i;
    if (debugging_flag & 2 && debugging_level <=30)
        CSW_EXEC(fprintf(stderr,"Constraints cleared from %d\n",start));
#ifndef NO_LOGGING
    sprintf(temp_msg,"clear_constraints");
    gen32_do_log(1,10,temp_msg);
#endif
#ifndef NO_ASSERTS
	assert(start < CONSTRAINT_MAX-1);
#endif
    constraint_index = start;
	constraints[constraint_index].what = -1;
    for (i=0; i<4; i++) already_placed[i]=0; // specific cards in constraints
    already_placed_count = 0;
	already_played_flag = 0;
	// fixed 100819 it was already placed instead
	for (i=0; i<52; i++) already_played[i]=0;
    if (debugging_flag & 2 && debugging_level == 10)
        CSW_EXEC(fprintf(stderr,"all playing information for constraints is cleared in clear_constraints(%d)",start));
}


int push_index(void) {
    int i;
    if (index_index < INDEX_MAX-1) {
        index_stack[index_index][0] = constraint_index;
        index_stack[index_index][1] = already_placed_count;
        for (i=2; i<6; i++)
            index_stack[index_index][i] = already_placed[i-2];
        return ++index_index; // number on stack (for verification)
        }
    return -1;	 // error condition
}

void set_card(int card, int position) {
    int i,temp;
    for (i=0; i<52; i++)
        if (decks[active_hand][i] == card) break;
    temp = decks[active_hand][position];
    decks[active_hand][position] = card;
    decks[active_hand][i] = temp;
    }



int pop_index(void) {
    int i;
    if (index_index > 0) {
        index_index--;
        constraint_index = index_stack[index_index][0];
        already_placed_count = index_stack[index_index][1];
        for (i=2; i<6; i++)
            already_placed[i-2] = index_stack[index_index][i];
        return index_index;  // number on stack (for verification)
        }
    return -1;	// error condition
}

/*
get_best_constraints either processes the constraints or returns a value for the constraint at constraint_index.

This is used to pass information from the constraints to the prolog process of the constraints. The complicated processing occurs when there are or conditions.

An example. The takeout double. This uses an ID_ORAND for processing...
here is the rule for a takeout double of 1C: #26851 "  <#b==C>X;14" !.! %2% !FR 3:5H 3:5S 0:3C 0:5L L05:9 %3:2|! %:C..[3-9][3-9]&&:R~17>% [10,25] *2525*

The ID_ORAND processes the first three constraints by anding them; it processes the next two constraints by anding them; it does an of those two ands.
This is necessary because a takeout double shows distributional information but if very strong it does not, and that is not known until later bidding. 
It can be determined that if 4+ spades or 4+ hearts or 2- clubs are present, then the takeout is based on strength not distribution.
In the case where the first test fails, the second is turned on, and the constraints for the first are turned off and the constraints for the latter are turned on.


*/
void set_active(int);
int get_best_constraints(int process_command, int constraint_index, int active_hand) {
	int val[4];
	if (process_command) { 
		set_active(active_hand);
		check_constraints(&val[0]);
		return 0;
		}
	else return constraints[constraint_index].flag;
	}

// This calculates the difference the current hand has from the goal 
// constraints.
// It is always a unsigned value. The higher the value the worse the match.
// It checks high card points, suit distribution, and length distribution.
// What is the difference from suit distribution and length. The suit is the
// actual suit such as 4 hearts or 4 spades. The length answers questions like
// the longest suit is between 7 and 9 cards in length.
// 
// It would be easy to do an alternation by having a constraint that is or and
// looks at two other nodes, picking the better of the two. The same can be
// done with the and which would total the two.
// 
// Actually most any arithmatic condition can be satisfied by calculating the
// values and storing with the constraint and then running various meta
// constraints such as (and, or, not, exclusive or).
// 
// Thoughts, the easiest way to implement this is to have a linear list of
// constraints. These are then processed. Then a FORTH system is used to 
// process
// the logic. The FORTH commands would be generated from the python portion of
// the code. This will look at the results portion of the processed 
// constraints.
// 
// For example, let us say we have four constraints.
// 1: val = 0, say hearts in range 5:9, actual = 6
// 2: val = 1, say hcps in range 10:15, actual = 9
// 3: val = 2, say spades in range 2:3, actual = 5
// 4: val = 0, say heart ace is preset, actual = 1 (boolean, is present)
// 
// It is easy to calculate the above values, both the val (calculated 
// deviation from
// desired) and the actual result.
// 
// Now those values can be used in a Forth processing system for basically 
// complete
// logical and arithmetic processing. The forth system is available in bid20 
// and could
// be pulled out as a separate dll system, or just call the bid20.dll

int gen32_get_suit_strength(int [52], int [4],int,int);
int get_stopper(int [52], int [4], int, int);
int gen32_get_intermediates(int [52], int [4], int, int);
int gen32_get_suit_quicks(int [52], int [4], int, int);
int get_support_points(int [52], int [4], int, int);
int get_supported_points(int [52], int [4], int, int);


char htemp[200];
char htemp2[200];
int tdeck[52];
int aDist[4][4];
/* UPVALUE is used to enhance the effectiveness of a constraint.
For example, ID_HAS is used to enforce having a card. This is more important
that the strength of a suit, or where 1 or 2 stoppers are present, or the hcps are off by 1 point.
So a constraint is evaluated as to its match of the current hand. The variance from what is desired
is usually something like the difference from the high or low value. For example, If the suit length
of clubs (given by constraint 2:5C to constrain from 2-5 clubs) is in the range 2-5 then the result is 0.
If not it is the difference, so a void in clubs is a variance of 2, a singleton is 1. We are trying to
minimize the total variance of all constraints so this makes since to move toward the hand that best
matches the full set of constraints. However, there may be times when this is impossible, so the match
cannot be too rigid and the variance can be increased from 0 if necessary to get a match as the number
of shuffles increase and no hand comes in with 0 variance. Also, some things I am going to set as more 
important, and certainly ID_HAS and ID_VOID  and ID_ACES would be the top three constraints to be
considered for special treatment. UPVALUE is the means to do this.
*/

#define UPVALUE 5
int used_constraint[100];

#ifdef BEST_HOFFER
int check_constraints(int val[4]) {
#else
int check_constraints(void) {
#pragma message("check constraints without val[4]")
#endif
    static int fully_initialized=0;
    int i,j,k,v=0,card,suit,who,diff=0,where,hi,lo,next_placed, what;
#ifndef BEST_HOFFER
	int val=0;
#endif
    int us,them,total;
    int losers[4],quicks[4];
    int suit_quicks[4][4];
    int intermediates[4][4];
    int controls[5];
    int bool_index;
    // char msg[80];
    int hcps[4], len[4][4], suit_quality[4][4], stoppers[4][4], aces[4];
#ifdef USE_HOFFER_EVALUATION
    int roths[4];
#else
    int bergens[4];
#endif
    int support_points[4][4], supported_points[4][4];
#ifdef BEST_HOFFER
	for (int dir=0; dir < 4; dir++) val[dir]=0;
#endif
    for (i=0; i<4; i++) {	// get the values (although in the future just get changes
#ifdef USE_HOFFER_EVALUATION
		roths[i] = gen32_hoffer_count(decks[active_hand],i*13,13,STARTING,0);
		// roths[i] = gen32_roth(decks[active_hand],i*13,13,0,0);
#else
		bergens[i] = gen32_bergen(decks[active_hand],i*13,13,0,0);
#endif
		hcps[i] = gen32_get_hcps(decks[active_hand],i*13,13);
        aces[i] = getAces(decks[active_hand],i*13,13);
    // losers[i]=0;
    // quicks[i]=0;
        gen32_distro(decks[active_hand],i*13,13,aDist[i]);
        for (j=0;j<4;j++) len[i][j]=aDist[i][j];
        { int ii; for (ii=0; ii<52; ii++) tdeck[ii] = decks[active_hand][ii]; }
        get_matt_string2(tdeck,htemp);
#ifndef FIX_LOSERS
        losers[i] = get_losers(decks[active_hand],aDist[i],i,13);
#else
		losers[i] = gen32_getLTC(decks[active_hand],i*13,13);
#endif
		quicks[i] = gen32_get_quicks(decks[active_hand],aDist[i],i,13);
		controls[i] = gen32_get_controls(decks[active_hand],constraints[i].where,i,13);
        sort_by_length(len[i]);  // use aDist copy then set longest suit first	
        for (j=0;j<4;j++)
			suit_quality[i][j]=gen32_get_suit_strength(decks[active_hand],aDist[i],i,j);
        for (j=0;j<4;j++)
            stoppers[i][j]= get_stopper(decks[active_hand],aDist[i],i,j);
        for (j=0;j<4;j++)
			intermediates[i][j]= gen32_get_intermediates(decks[active_hand],aDist[i],i,j);
        for (j=0;j<4;j++)
			suit_quicks[i][j]= gen32_get_suit_quicks(decks[active_hand],aDist[i],i,j);
#ifndef USE_HOFFER_EVALUATION
        for (j=0;j<4;j++)
            support_points[i][j] = get_support_points(decks[active_hand],aDist[i],i,j);
        for (j=0;j<4;j++)
            supported_points[i][j] = get_supported_points(decks[active_hand],aDist[i],i,j);
#else
        for (j=0;j<4;j++) {
			support_points[i][j] = gen32_hoffer_count(decks[active_hand],i*13,13,SUPPORT_OF,j); // support of a suit
		    supported_points[i][j] = gen32_hoffer_count(decks[active_hand],i*13,13,SUPPORTED,j); // I have been supported
            }
#endif
        
    }
	
    for (i=0; i<52; i++) who_has[i]=99; // invalid until a card is placed
    for (i=0; i<52; i++) already_played[i] = 0;
	
	for (i=0; i<constraint_index && i < CONSTRAINT_MAX; i++) {
		constraints[i].diff=0;
		constraints[i].value=0;
		constraints[i].flag=0; // used to determine if bidding can use the constraint (so after controls have been processed it can be determined)
		}
    // printf("processing %d constraints\n",constraint_index);
    for (i=0; i<constraint_index && i < CONSTRAINT_MAX; i++) {
        who = constraints[i].who;
		/* constraint processing can exclude a player, mainly because that hand is fully given
		however, that is not true for played cards, they must still be processed.
		So, do not continue even if this is an excluded player, if it is ID_PLAYED,
		also consider that that may be true for ID_HAS.
		220501

		*/

        if (!(indicator & 1 << who) && constraints[i].what != ID_PLAYED) {
                // continue if not in the active hands, 
                // except a played card needed for each hand generating purposes 220501 220527
			continue;
			}
        where = constraints[i].where;
        hi = constraints[i].hi; lo = constraints[i].lo;
        what = constraints[i].what;
        if (!fully_initialized) {
            switch (what) {
                case ID_NOP_DIR: {
                    int cnum;
                    for (cnum=0; cnum < i; cnum++)
                        if (constraints[cnum].who == where) 
                            constraints[cnum].what = ID_NOP;
                    } break;
                case ID_NOP_CONSTRAINTS: 
                    if (where > 0 && where < i) { constraints[where].what = ID_NOP; }
                    break;
                default: break;
                }
            }
		/* this section added 200810...it handles the exclusion of certain hands bidding constraints */
		switch (what) {
			case ID_NOP: case ID_OUT: case ID_KNOW: case ID_PLAYED: // play of the hand constraints are not affected
			case ID_DIFF: case ID_AND: case ID_OR:
				break;
			default: 
				switch(who) {
					case 0: if (1 & excludes) what = ID_NOP; break;
					case 1: if (2 & excludes) what = ID_NOP; break;
					case 2: if (4 & excludes) what = ID_NOP; break;
					case 3: if (8 & excludes) what = ID_NOP; break;
					default: break;
					}
			}
        switch (what) {
            case ID_NOP: v = 0; break;
            case ID_HCP:	v = hcps[who];	break;
            case ID_VOID: v = aDist[who][where] == 0 ? 0 : 1; break;
                /* ID_OUT fixed 200822
                 the suit for this hand has shown out. So, see how many cards in that suit the hand has played
                 then see if the count for the test hand has that count. return difference
                */
            case ID_OUT: { int _count = 0;  // set v based on the fact that no more cards should come to this hand
                for (int _card=0; _card < 52; _card++) {
                    if (who_has[_card] == who && _card / 13 == where) _count++;
                    }
                if (aDist[who][where] == _count)
                    v=0;
                else if (aDist[who][where] > _count) // he has shown out so cannot have cards in that suit later on
                    v = aDist[who][where] - _count;
                else v = _count - aDist[who][where];
                break;
                }
            case ID_SINGLETON: v = aDist[who][where] ==  1 ? 0 : 1; break;
            case ID_ACES: v = aces[who]; break;
            case ID_ROTH:
#ifdef USE_HOFFER_EVALUATION
                v=roths[who];
#else
                v = bergens[who];
#endif
                break;
            case ID_SUPPORT: v = support_points[who][where]; break;
            case ID_SUPPORTED: v = supported_points[who][where]; break;
            case ID_STOPPER: v = stoppers[who][where]; break;
            case ID_QUALITY: v = suit_quality[who][where]; break;
            case ID_LEN:	v = len[who][where];	break;
            case ID_DIST:	v = aDist[who][where];	break;
            case ID_LOSERS: v = losers[who]; break;
            case ID_FORCE: v = 0; break;
            case ID_SIMULATE: v = 0; break;
            case ID_QUICKS: v = quicks[who]; break;
            case ID_CONTROLS: v = controls[who]; break;
            case ID_INTERMEDIATES: v = intermediates[who][where]; break; // QJT9 in a suit
            case ID_SUIT_QUICKS: v = suit_quicks[who][where]; break;
            case ID_INOUT:
                us = 0; them=0;
                for (j=who*13; j<who*13+13; j++) {
                    card = decks[active_hand][j];
                    if ((k=card % 13) > 8 && k < 12) {
                        suit = 1 << (card / 13);
                        if (suit & where) us++;
                        else if (suit & (where >> 4)) us++;
                        else if (suit & (where >> 8)) them++;
                        else if (suit & (where >> 12)) them++;
                        }
                    }
                total = us + them;
                if (total == 0) v = 1;
                else {
                    v = (us * 100) / total;
                    if (v < 20) v=0;
                    else if (v < 40) v=1;
                    else if (v < 60) v=2;
                    else if (v < 80) v=3;
                    else v=4;
                }
                break;
            case ID_COMPARE:
                if (aDist[who][lo] > aDist[who][hi]) diff = aDist[who][lo] - aDist[who][hi];
                else if (aDist[who][hi] > aDist[who][lo]) diff = aDist[who][hi] - aDist[who][lo];
                else diff = 0;
                switch (where) {
                case ID_LESS: v = aDist[who][lo] < aDist[who][hi] ? 0 : 1+diff; 
                    // fprintf(stderr,"less who:%u lodist:%u hidist:%u result:%u\n",who,aDist[who][lo],aDist[who][hi],v); 
                    break;
                case ID_MORE: v = aDist[who][lo] > aDist[who][hi] ? 0 : 1+diff; 
                    // fprintf(stderr,"more who:%u lodist:%u hidist:%u result:%u\n",who,aDist[who][lo],aDist[who][hi],v); 
                    break;
                case ID_MORE_OR_EQ: v = aDist[who][lo] >= aDist[who][hi] ? 0 : diff; break;
                case ID_LESS_OR_EQ: v = aDist[who][lo] <= aDist[who][hi] ? 0 : diff; break;
                case ID_EQUAL:v = aDist[who][lo] == aDist[who][hi] ? 0 : diff; break;
                } break;
            case ID_ODD:
                v=1;
                if (aDist[who][where] % 2 ==1) v=0;
                break;
            case ID_EVEN:
                v=1;
                if (aDist[who][where] % 2 == 0) v=0;
                break;
            case ID_HASNOT:
                v=0; // look for a specific card you do not want to have
                if (who > 3 || who < 0) break; // bad who
                for (j=13*who; j<13*(who+1); j++)
                    if (decks[active_hand][j] == where) { v=2; break; }
                break;
            case ID_HAS:   // 170330 was placing the card, now properly checking for it
                v=UPVALUE; // look for a specific card you want to have
                if (who > 3 || who < 0) break; // bad who
                for (j=13*who; j<13*(who+1); j++)
                    if (decks[active_hand][j] == where) { v=0; break; }
                break;
            case ID_PLAYED:   // 170330, new, places a card, was ID_HAS; this fall-though is not an error
                already_played_flag = 1;
                // fall through
            case ID_KNOW:
                v=1; // look for a specific card
                if (who <0 || who > 3) {
                    CSW_EXEC(fprintf(stderr,"improper who in ID_PLAYED or ID_KNOW\n"));
#ifndef NO_ASSERTS
                    if (!debugging_flag) assert(who >= 0 && who <=3);
#endif
                    
                }
                if (already_placed[who] > 13) {
                    CSW_EXEC(fprintf(stderr,"improper who in ID_PLAYED or ID_KNOW\n"));
#ifndef NO_ASSERTS
                    if (!debugging_flag) assert(already_placed[who]<14);
#endif
                    
                }
                if (who_has[where] != 99) {
                    printf("%d has already been placed with %d\n",where,who_has[where]);
                    }

                if (already_played_flag) {
                    if (already_played[where] != 0) { // 220528
                        fprintf(stderr,"%d has already been played\n",where);
                        for (int j=0; j< constraint_index; j++) {
                            if (constraints[j].what == ID_PLAYED) printf("index:%d card:%d\n",j,where);
                            }

                        }
                    if (!already_played[where]) already_played[where] = 1;
                    else {
                        CSW_EXEC(fprintf(stderr,"card %d has already been played (do_constraints)\n",where));
#ifndef NO_ASSERTS
                        if (!debugging_flag) assert(already_played[where]==0);
#endif
                        
                    }
                    }
                for (j=13-already_placed[who]; j<13 && v==1; j++)  // placed cards at top
                     if (decks[active_hand][who*13+j] == where) v=0;  // the card is already placed in this hand
#ifdef PLACING
                if (v == 0) {  // fprintf(stderr,"already in hand\n");
                    already_played_flag = 0;
                    assert(who_has[where]==99);
                    who_has[where]=who; // keep track of played cards while processing the constraints for use with ID_OUT
                    break; }
                if (already_placed[who]>12) { v=10; already_played_flag = 0;
                    if (debugging_flag & 2)
                        CSW_EXEC(fprintf(stderr,"trying to place too many cards for %d\n",who));
                    break; } // cannot handle card out of already placed, i.e., into another hand
                for (j=0; j<52; j++) 
                    if (decks[active_hand][j] == where) break;	// find card and break when found
             //   assert ((1 << (j/13)) & indicator); // the sought for card is in an active hand; remove later
                // find the card, see if an active hand, if so move to top of this hand
                if (((1<< (j/13)) & indicator) || already_played_flag) {     // hool temporary fix 220527
                    next_placed = who*13 + 12 - already_placed[who];
                    assert(who_has[where]==99);
                    if (used_constraint[i] != 99)  {
                        fprintf(stderr,"used_constraint %d already for card:%d\n",i,used_constraint[i]);
                        for (int z=0; z<52; z++) printf("%d-%d;",z,decks[active_hand][z]);
                        }
                    used_constraint[i]=where;
                    fprintf(stderr,"%d placing who:%d card:%d from:%d to:%d",i,who,where,j,next_placed);
                    if (next_placed < 0 || next_placed > 51) {
                        fprintf(stderr,"bad next_placed:%d\n",next_placed);
                        exit(0);
                    }
                    do_exchange(j,next_placed); // can't exchange last
                    fprintf(stderr,"-done\n");
                    already_placed[who] += 1;
                    already_placed_count += 1;
                    who_has[where]=who; // keep track of played cards while processing the constraints for use with ID_OUT
        //			fprintf(stderr,"Already for %u is %u; all placed is %u\n",who,already_placed[who],already_placed_count);
                    if (debugging_flag & 2 && debugging_level == 10)
                        CSW_EXEC(fprintf(stderr,"PLACING:placed %d in hand %d",where,who));
                    v=0;
                    }
#ifndef NO_ASSERTS
                if (v!=0) {
                    printf("card is where:%d j is %d\n",where,j);
                    printf("indicator is %d\n",indicator);
                    printf("shift is %d\n",1 << (j/13));
                    printf("the shift should be anded with the indicator to place a card");
                    constraint_command(0,89,0,0,0);
                    }
                assert(v==0);
#endif
#endif
				already_played_flag = 0; // huge fix from 200811 ID_KNOWS after ID_PLAYED went to played
                break;
            case ID_SUM: v = constraints[hi].value + constraints[lo].value; break;
            case ID_SUB: v = constraints[hi].value - constraints[lo].value; break;
            case ID_VALUE: v = constraints[where].value; break;
            default: v=0;
            }
        constraints[i].value = v;
        if (what == ID_DIFF) {
            constraints[i].diff = constraints[where].diff;
            constraints[i].value = constraints[where].value;
			if (constraints[where].what != 41)
				constraints[where].flag = constraints[where].diff == 0 ? 1 : 0; // diff of 0 is a match so flag to use this constraint is turned on
#ifdef BAD_V
        if (diff != 0)
            { bad_v[0][i]++; bad_v[1][i]++; bad_v[who+2][i]++; }
#endif
            }
        else if (what == ID_NOP) {
            // 200405 A nop was failing for the next section because it was still checking for a diff from v
            // so C2:9 went to a NOP and the v < 2 so it had a diff of 2 and always failed
            // fprintf(stderr,"!working a NOP for position %d\n",i);
            constraints[i].diff = 0;
            }
        else if (what != ID_COMPARE) {
            if (v > hi) diff = v-hi;
            else if (v < lo) diff = lo-v;
            else diff = 0;
			constraints[i].flag = 0;
            if (what == ID_ACES) diff *= 4;  // make it more important
            else if (what == ID_VOID) diff *= 4; // also important
			else if (what == ID_OUT) diff *= 4; // also this
            else if (what == ID_SINGLETON) diff *= 4;
            constraints[i].diff = diff;
            }
        else constraints[i].diff = v;

        int diff_array[2]; // just used in ID_ORANDS as this has two sets of constraints to first AND, then OR the result
        switch (constraints[i].what) {
			case ID_ORANDS:
				diff_array[0] = 0; diff_array[1] = 0;                          // initialization for safety.
                for (bool_index = i-lo-hi; bool_index < i-hi; bool_index++) {  // check first set of constraints by AND
                    int d=0;
                    // id_type = constraints[bool_index].what; 
                    d = constraints[bool_index].diff;
                    // if (id_type == ID_ACES || id_type== ID_VOID || id_type == ID_OUT || id_type == ID_HAS || id_type == ID_SINGLETON) d *= UPVALUE;
                    diff_array[0] += d;                                        // any non-zero d is a failure of the AND
                    }
                for (bool_index = i-lo-hi; bool_index < i-hi; bool_index++) 
					constraints[bool_index].flag = diff_array[0] == 0 ? 1 : 0;    // the first set constraints each are on or off pending the AND of the set
				if (diff_array[0] != 0) {                                         // if the first sets does not fail this turns off the second set.
	                for (bool_index = i-hi; bool_index < i; bool_index++)
						constraints[bool_index].flag = 0;
					
					for (bool_index = i-hi; bool_index < i; bool_index++) {           // check second set of constraints by AND
						int d=0;
						// id_type = constraints[bool_index].what; 
						d = constraints[bool_index].diff;
						// if (id_type == ID_ACES || id_type== ID_VOID || id_type == ID_OUT || id_type == ID_HAS || id_type == ID_SINGLETON) d *= UPVALUE;
						diff_array[1] += d;                                           // any non-zero d is a failure of the AND
						}
					if (diff_array[0] != 0 && diff_array[1] != 0) {                   // both sets failed. So none can be flagged on
						for (bool_index = i-hi; bool_index < i; bool_index++) 
							constraints[bool_index].flag = 0;                         // turn off all of second set
						}
					// trying to figure a good way to march towards a match
					if (diff_array[0] == 0) diff = 0;
					else if (diff_array[1]==0) diff=0;
					else diff = diff_array[0] < diff_array[1] ? diff_array[0] : diff_array[1];
					}
				else diff=0;
				val[who] += diff; // will be zero if all are OK; was val += diff, changed 170124; fixed in 170403
                constraints[i].diff = diff;
                constraints[i].value = diff;
#ifdef BAD_V
                if (diff != 0)
					{ bad_v[0][i]++; bad_v[1][i]++; bad_v[who+2][i]++; }
#endif
				break;


            case ID_AND:
                diff =0; /* need all working so diff can be the total number */
                for (bool_index = lo; bool_index <= hi; bool_index++) {
                    int id_type,d;
                    id_type = constraints[bool_index].what; 
                    d = constraints[bool_index].diff;
#ifdef BAD_V
                    if (d != 0)
						{ bad_v[0][bool_index]++; bad_v[1][bool_index]++; bad_v[who+2][bool_index]++; }
#endif
                    if (id_type == ID_ACES || id_type== ID_VOID || id_type == ID_OUT || id_type == ID_HAS || id_type == ID_SINGLETON)
                         d *= UPVALUE;
                    diff += d;
                    }
                for (bool_index = lo; bool_index <= hi; bool_index++) 
					constraints[bool_index].flag = diff == 0 ? 1 : 0;
                val[who] += diff; // will be zero if all are OK; was val += diff, changed 170124; fixed in 170403
                constraints[i].diff = diff;
                constraints[i].value = diff;
#ifdef BAD_V
                if (diff != 0)
					{ bad_v[0][i]++; bad_v[1][i]++; bad_v[who+2][i]++; }
#endif


                break;
            case ID_REMOVE:  // remove value of a prior constraint (like an OR that will be used later)
                val[who] -= constraints[where].diff;  // get it removed so it looks like it was zero
                constraints[i].diff = constraints[where].diff; // switch from 0 180429 because of relative positioning of constraints
                constraints[i].value = constraints[where].value;
                break;

            case ID_OR:   // from lo to hi see if one of those constraints is acceptable
                diff = 1000;
                for (bool_index = lo; bool_index <= hi; bool_index++) {
                    int new_diff= constraints[bool_index].diff;
					constraints[bool_index].flag = new_diff == 0 ? 1 : 0;
#ifdef BAD_V
                    if (new_diff != 0)
						{ bad_v[0][bool_index]++; bad_v[1][bool_index]++; bad_v[who+2][bool_index]++; }
#endif
                    int id_type =  constraints[bool_index].what;
                    if (id_type == ID_ACES || id_type== ID_VOID || id_type == ID_OUT || id_type == ID_HAS || id_type == ID_SINGLETON)
                        new_diff *= UPVALUE;
                    
                    if (new_diff < diff) {
                        diff = new_diff;
                        if (diff == 0) break; // added 170124 as result cannot change now
                        }
                    }
                val[who] += diff; // will be zero if any constraint is zero;
                constraints[i].diff = diff;
                constraints[i].value = diff;
#ifdef BAD_V
                if (diff != 0)
					{ bad_v[0][i]++; bad_v[1][i]++; bad_v[who+2][i]++;}
#endif
                break;
            case ID_XOR:
					if (constraints[hi].diff == 0 && constraints[lo].diff == 0) { // both matched, a special situation will arise for diff processing
						constraints[i].value = 0; 
						constraints[hi].diff = 1; constraints[lo].diff = 1; // cannot stay at 0 or will look like it is actually a match
						constraints[hi].flag = 1; constraints[lo].flag = 1; // just getting values for bidding not concerned with generating hands so both can be used
						}
					else if (constraints[hi].diff == 0 && constraints[lo].diff != 0) { constraints[i].value=1; constraints[lo].flag = 1; }// only one matched completely
					else if (constraints[hi].diff != 0 && constraints[lo].diff == 0) { constraints[i].value=1; constraints[hi].flag = 1; }
                    else constraints[i].value = 0;
                    if (constraints[i].value == 1) constraints[i].diff = 0;
                    else { int l,h = constraints[hi].diff;
                        int id_type = constraints[hi].what;  // see it referred diff is important enough to UPVALUE
                        if (id_type == ID_ACES || id_type== ID_VOID || id_type == ID_OUT || id_type == ID_HAS || id_type == ID_SINGLETON)
                            h *= UPVALUE;
                        id_type = constraints[lo].what;  // see it referred diff is important enough to UPVALUE
                        l = constraints[lo].diff;
                        if (id_type == ID_ACES || id_type == ID_VOID || id_type == ID_OUT || id_type == ID_HAS || id_type == ID_SINGLETON)
                            l *= UPVALUE;
                     constraints[i].diff = h + l;
                     }
                    
                    val[who] += constraints[i].diff;
                      // changed to += 180325, surely it is additive
#ifdef BAD_V
                    if (constraints[i].diff != 0)
						{ bad_v[0][i]++; bad_v[1][i]++;bad_v[who+2][i]++; }
#endif
                    break;

            case ID_DIFF:   // 7 evaluate the constraint it points to    (use where)
                {	int d=constraints[where].diff;
                    int id_type = constraints[where].what;  // see it referred diff is important enough to UPVALUE
					if (id_type == ID_FORCE) ;
					else constraints[where].flag = constraints[where].diff == 0 ? 1 : 0;
                    if (id_type == ID_ACES || id_type== ID_VOID || id_type == ID_OUT || id_type == ID_HAS || id_type == ID_SINGLETON)
                        d *= UPVALUE;
                val[who] += d;
                }
#ifdef BAD_V
                if (constraints[where].diff != 0)
					{ bad_v[0][i]++; bad_v[1][i]++; bad_v[who+2][i]++;} 

        // if (constraints[where].what == 25 && bad_v[i] % 500 == 1) printf("\n25 val: %d diff:%d bad:%d",val,constraints[where].diff,bad_v[i]);
#ifdef BAD_V_TEST
        if ((i==(constraint_index-1)) &&  val == 0 && constraints[i-1].lo != 13) { int c_who, c_where;
            c_who = constraints[i-1].who;
            c_where = constraints[i-1].where;
            printf("\n***val is %d; hi:%d low:%d,support:%d",val,constraints[i-1].hi,constraints[i-1].lo,support_points[c_who][c_where]);
            support_points[c_who][c_where] = get_support_points(decks[active_hand],aDist[c_who],c_who,c_where);
            printf("\nwho:%u where:%u support:%u",c_who,c_where,support_points[c_who][c_where]);
            printf("\nvalue: %d who:%d where:%d",constraints[i-1].value,c_who,c_where);
            printf("\nadist %u %u %u %u\n",aDist[c_who][0],aDist[c_who][1],aDist[c_who][2],aDist[c_who][3]);	
        }
#endif
#endif
                break;
            case ID_NOT:
                val[who] += !constraints[where].diff; 
#ifdef BAD_V
                if ((!constraints[where].diff) != 0)
					{ bad_v[0][i]++; bad_v[1][i]++; bad_v[who+2][i]++;}
#endif
                break;
            case ID_VALUE: val[who] += constraints[i].diff; 
#ifdef BAD_V
                if (constraints[i].diff !=0)
					{ bad_v[0][i]++; bad_v[1][i]++;bad_v[who+2][i]++; }
#endif
                break;
            default: break;
            }
        }
    // sprintf(msg,"_gen32, returning %u after evaluating constraints",val);
    // LogError(msg);
    fully_initialized = 1; // for ID_NOP_CONSTRAINTS and ID_NOP_DIR to be run just once
#ifdef BEST_HOFFER
#if !defined(linux)
    { int temp_val=0,temp_bool = 0;
        temp_val = val[0] + val[1] + val[3];
        if (temp_val < 1 && val[2] < 3 && constraints[41].diff > 0)
            temp_bool = 1;
    }
#endif
    // printf("returning %d %d %d %d\n",val[0],val[1],val[2],val[3]);
    return val[0]+val[1]+val[2]+val[3];
#else
	return val;
#endif
}

#define SET_EXCHANGES
#ifdef IOS_ONLY
#define LOOP_MAX 1000
#endif
#ifdef BEST_HOFFER
void gen32_set_loops(int l) {
	if (l > 0) loops = l;
	}

int original[4][3] = {0,0,0,0,0,0,0,0,0,0,0,0 };
void gen32_reset_individual_exchanges(void) {
    for (int _dir=0; _dir < 4; _dir++) {
        modulo[_dir] = original[_dir][0];
        delta[_dir] = original[_dir][1];
        threshhold[_dir] = original[_dir][2];
    }
}
void gen32_set_individual_exchanges(int dir,int m,int d,int t) {
    original[dir][0] = m;
    original[dir][1] = d;
    original[dir][2] = t;
    modulo[dir] = m;
    delta[dir] = d;
    threshhold[dir] = t;
    }
#else
void gen32_set_exchanges(int l,int m, int d, int t) {
    if (l > 0) loops = l;
    if (m > 0) modulo = m;
    if (d >= 0) delta = d; // delta is added to threshhold each time modulo is reached, but if 0 threshhold never increases
    if (t >=0) threshhold = t; // if 0, then a perfect match is needed, usu this increases by delta on each modulo that is reached.
}
#endif

#ifdef WIN_ONLY
int bergen(int deck[52],int start,int number,int state, int suit)
{
    int i, j, dist[4], hcp[4], aces[4];
    int doubleton_flag = 0, kings[4];
    int honors[4], tens[4], topfive[4];
    int ace_flag, count, quacks, acetens;
    int quality, slength, dubious, hcps, adjust, tlength, xlength, shortness;
    quality = slength = dubious = hcps = adjust = tlength = xlength = shortness = 0;
    ace_flag = count = quacks = acetens = 0;

#ifdef DEBUG_BERGEN
    printf("\nstate: %d",state);
#endif

    gen32_distro(deck,start,number,dist);

    for (i=0; i<4; i++) { hcp[i]=0; aces[i]=0; kings[i]=0; honors[i]=0; tens[i]=0; topfive[i]=0; }

    /* get high card points totaled by suit and put in hcp array  */
    /* also get the number of honors including tens in their suits  */

    for (i=start; i<start + number; i++) {
        if ((j=deck[i] % 13) > 8) {
            hcp[deck[i] / 13] += (j-8);
            honors[deck[i]/13] += (1 << (j-9));
            }
        if (j>7) {
            topfive[deck[i]/13] += 1;
            if (j == 12) { ace_flag += 1; aces[deck[i]/13]=1; acetens += 1;}
            else if (j == 11) { kings[deck[i]/13]=1; }
            else if (j == 10 || j == 9) { quacks += 1; }
            else if (j == 8) { acetens += 1; }
            }
        }

    /* adjust-3 count aces + tens vs quacks 
        for 0-2 difference then no change
        for 3-5 adjust by one point
        for 6+ (rare) adjust by two points */



    for (i=0; i<4; i++) {
        if (topfive[i] >= 3 && dist[i]>3) { count += 1; quality += 1; } /* quality suits of 4+ length */
        if (dist[i] < 3) {  /* SHORTNESS */  /* only after support has been established */
            if (state == SUPPORT_OF) switch (dist[suit]) {
                case 0:
                case 1:
                case 2: break;
                case 3: if (dist[i]== 0) { count += 3; shortness += 3;}  /* void */ /* three trump support */
                        else if (dist[1] == 1) { count += 2; shortness += 2; }/* singleton */
                        else { count += 1; shortness += 1;} break; /* doubleton */
                case 4: if (dist[i] == 0) { count += 4; shortness += 4; }/* four trump support */
                        else if (dist[i] == 1) { count += 3; shortness += 3; }
                        else { count += 1; shortness += 1; } break;
                default: if (dist[i] == 0) { count += 5; shortness += 5; } /* five+ trump support */
                         else if (dist[i] == 1) { count += 3; shortness += 3;}
                         else { count += 1; shortness += 1; } break;
                }
            else if (state == SUPPORTED) switch (dist[i]) {
                case 0: { count += 4; shortness += 4; } break;
                case 1: { count += 2; shortness += 2; } break;
                case 2: if (doubleton_flag > 0) { count += 1; shortness += 1; }  
                    doubleton_flag += 1; break;
                default: break;
                }
            } /* SHORTNESS */
        if (dist[i]>4) { count += (dist[i]-4); slength += (dist[i]-4); }/* length of suit 5+ */
        if (dist[i]>4 && state == SUPPORTED) {
            int ii;
            if (dist[i] > 5) {
                count += (dist[i] - 5); /* 5+ trump length additional after supported */
                tlength = dist[i] - 5;
                }
            for (ii=0; ii<4; ii++) /* look for side 4 card suit or longer */
                if (ii!=i && dist[ii] > 3 && xlength == 0) {
                    count += 1;
                    xlength += 1;
                    }
            }
    }

#ifdef DEBUG_BERGEN
    printf("\nquality:%u",quality);
    printf("\nslength:%u",slength);
    if (tlength > 0) printf("\ntlength:%u",tlength);
    if (xlength > 0) printf("\nxlength:%u",xlength);
    if (shortness > 0) printf("\nshortness:%u",shortness);
    // printf("\ncount after length is %u (2)",count);
#endif
    /* add up high card points and subtract  DUBIOUS */

    for (i=0; i<4; i++) { /* i do not know how to do 1/2 point for singleton ace */
        count += hcp[i]; /* first add up hcps, then remove dubious honors */
        hcps += hcp[i];
        // printf("\ncount %u, hcp %u i %u",count,hcp[i],i);
        if (dist[i] == 1 && hcp[i]>0 && hcp[i] < 4) { count -= 1; dubious += 1; }  /* singleton k,q,j */
        else if (dist[i] == 2 && hcp[i]>0 && hcp[i] < 4 && kings[i]==0) { count -= 1; dubious += 1; } /* jx,qx,qj */
        else if (dist[i] == 2 && hcp[i] == 4 && aces[i] == 0) { count -= 1; dubious += 1; } /* kj */
        else if (dist[i] == 2 && hcp[i] == 5) { count -= 1; dubious += 1; } /* aj,kq */
        }
#ifdef DEBUG_BERGEN
    printf("\nhcps:%u",hcps);
    printf("\ndubious:%u",dubious);
#endif
    /* adjust-3 ace tens vs quacks */
    // printf("\ncount before adjust:%u",count);
    if (acetens - quacks >= 6) { count += 2; adjust += 2; }
    else if (acetens - quacks > 2) { count += 1; adjust += 1; }
    else if (quacks - acetens >= 6) { count -= 2; adjust -= 2; }
    else if (quacks - acetens > 2) { count -= 1; adjust -= 1; }
#ifdef DEBUG_BERGEN
    printf("\nadjust:%u",adjust);
    printf("\ncount:%u\n***********\n",count);
    if (state==BERGEN_HCPS) printf("\nhcps+adjust-dubious+slength+quality = %d",hcps+adjust+slength-dubious+quality);
#endif
    if (state == BERGEN_HCPS) return hcps+adjust-dubious+slength+quality;
    else return(count);

}
#endif

void restricted_shuffle(void) {   // shuffling of one hand; restricted as to directions; set_offsets must be called first
    int top_pos,x_pos, top, selection;
    deck_verify(active_hand);
    top = mul_13[num_of_hands];
    while (top > 1) {
        selection = rand() % top--;
        top_pos = starters[top/13] + top % 13;
        x_pos = starters[selection/13] + selection % 13;
        do_exchange(top_pos,x_pos);
        }
}


void gen32_set_deck(int position, int card) {
    gen32_deck[position] = card;
}

int gen32_get_deck(int position) {
    return gen32_deck[position];
    }
    
void gen32_set_evaluation_system(int system) {
    gen32_evaluation_system = system;
    }

void initialize_decks(void) {
    int i,j;
    for (i=0;i<DECKNUM;i++)
        for (j=0; j<52; j++) 
            decks[i][j]=j;
    initialized_flag = 1;
    }

    // tidyup is spacing change only
void set_active(int a) { active_hand = a; }

int useable(int hand,int hand_indicator) {
    if ((1 << hand) & hand_indicator) return TRUE;
    return FALSE;
    }

int get_offset(int hand) {
    int offset = 0,i;
    for (i=0; i<4; i++) 
        if (useable(i,indicator)) {
            if (i == hand) return offset;
            offset++;
            }
    return -1;
    }

int set_offsets(int hand_indicator) {
    int i,j, offset = 0,count;

    indicator = hand_indicator;
    for (i=0; i<4; i++)  // go through each hand
        if (useable(i,hand_indicator)) {  // a useable hand
            count = 0;
            for (j=0; j<4; j++)  // go through the other hands
                if (j != i && useable(j,hand_indicator))  // also useable
                    offsets[offset][count++] = j* 13;  // set offets to start of other hand
            starters[offset++] = i*13; // set start for each useable hand
            }
    num_of_hands = offset;	// remember total number of useable hands
    return offset;
    }

void do_exchange(int pos1, int pos2) {
    int temp;
  //  if (!(1<<(pos1/13) & indicator))
  //	  fprintf(stderr,"oh shit2");
  //  if (!(1<<(pos2/13) & indicator))
  //	  fprintf(stderr,"oh shit3");
  //  if (pos2 < 0 || pos2 > 51)
  //      err = 1;
#if defined(DEBUGGING_HOOL)
#ifndef NO_ASSERTS
    assert(pos1 >= 0 && pos1 < 52);
    assert(pos2 >= 0 && pos2 < 52);
#endif
    assert(used_constraint[pos1]=99);
    assert(used_constraint[pos2]=99);
    if (errorcheck(decks[active_hand]) != 1) printf("bad errorcheck in do_exchange");
    if (used_constraint[pos1] !=99) printf("bad exchange........\n");
    if (used_constraint[pos2] !=99) printf("bad exchange........\n");
#endif
    temp = decks[active_hand][pos1];
    decks[active_hand][pos1] = decks[active_hand][pos2];
    decks[active_hand][pos2] = temp;
    }

// This is a dealing routine that uses a very simple algorithm. It randomly
// selects a card each from two available hands. It exchanges them. If the
// two hands are closer to the goal of the given constraints the exchange is
// kept, otherwise the exchange is reversed. LOOP_MAX is currently set up
// as the maximum number of exchanges to attempt. The nice thing about this
// routine is that there is no failing case to the matches. It will get as 
// close
// as it can. What situations does this help with? If a person has 
// misrepresented
// his hand (and Kenny does do that, David does not -> so some AI can be done 
// here)
// then this will still come up with a somewhat ressonable representation.
// 
// The hands to deal are in the decks, starting with first and doing num deals
// returning the number of hands dealt.

#define EXCHANGE 0
#define REVERSE 1
int errorcheck(int *);
void setup_errorcheck2(int [52],int);
int error_check2(int [52],int);
int errchk[100][52];
void setup_errorcheck2(int in[52],int num) {
    int i;
    for (i=0; i<52; i++)
        errchk[num][i] = in[i];
}

int error_check2(int in[52],int num) {
    int i;
    for (i=0; i<52; i++)
        if (!(indicator & 1<<(i/13)))  // check inactive hands for alterations
            if (errchk[num][i] != in[i])
                return 1;
    return 0;
}

#define ERRORCHECK3
#undef HOOL_VERBOSE
#if defined(ERRORCHECK3)
int cards_chk[52];
int placed_chk[4];
void setup_errorcheck3(int hand_index) {
    // store the placed cards for testing during dealing hands to find a processing error
    // the stored cards have been moved to the top of the respective hand in the 52 card array
    int i, dir, top, count;
    int tops[4]={12,25,38,51};
#if defined(HOOL_VERBOSE)
    fprintf(stderr,"already_placed_count is %d\n",already_placed_count);
#endif
    for (i=0; i<52; i++) cards_chk[i]=99;
#if defined(HOOL_VERBOSE)
    fprintf(stderr,"setup_errorcheck3:\n");
#endif
    for (dir=0; dir<4; dir++) {
        top = tops[dir];
        count = already_placed[dir];
        placed_chk[dir]=count;
        while (count--) {
            cards_chk[top] = decks[hand_index][top];
#if defined(HOOL_VERBOSE)
            fprintf(stderr,"%d-%d;",top,cards_chk[top]);
#endif
            top--;
            }
        }
#if defined(HOOL_VERBOSE)
    fprintf(stderr,"\n");
#endif
    }

int errorcheck3(int hand_index) {
    // verify the stored placed cards against this hand in decks array
    int card,result=1;
    for (int i=0; i<52; i++) {
        card = cards_chk[i];
        if (card != 99 && card != decks[hand_index][i]) {
#if defined(HOOL_VERBOSE)
            fprintf(stderr,"errorcheck3 failed on position:%d; card should be:%d\n",i,card);
            fprintf(stderr,"the card that is there now is:%d\n",decks[hand_index][i]);
#endif
            result=0;
            }
        }
#if defined(HOOL_VERBOSE)
    if (result==0) {
        for (int i=0; i<52; i++)
            fprintf(stderr,"%d-%d;",i,decks[hand_index][i]);
        fprintf(stderr,"\n");
        }
#endif
    for (int i=0;i<4;i++)  if (already_placed[i] != placed_chk[i]) {
            result = 0;
            fprintf(stderr,"already_placed fails to match placed_chk\n");
            }
    return result;
    }
#endif

// #define SPEED_UP_HAS
#ifdef SPEED_UP_HAS
#define HAS_MAX 13
int has_count;
struct has_constraint {
    int hand;
    int card;
    } has_table[HAS_MAX];
#endif


#define SPEEDY
void deck_copy_number(int first,int number); // copy hands (used when cards have been placed
int constraint_deal_n(int first, int num, int loops) {
	// try to deal a set of hands given the constraints but the number of tries is limited to loops through  
    int i,j, val=999,num_of_exchanges,retries;
#ifdef BEST_HOFFER
	int v[4];
#else
	int v,hand_threshhold;
#endif
    long loop_counter=0;
    first = first > 0 ? first : 0;
    first = first < DECKNUM ? first : 0;
    num = num > 0 ? num : 1;
    num = first + num <= DECKNUM ? num : DECKNUM - first;
    loops = loops > 0 ? loops : LOOP_MAX;
    loops = loops <= LOOP_MAX ? loops : LOOP_MAX;
#ifdef SPEED_UP_HAS
    // set up an array that disallows removing the card of ID_HAS from a hand during shuffling
    has_count = 0;
    // resets the modulo, threshhold, and delta values for each dir on each hand at the start of generation
    for (i=0; i<constraints_index && i < CONSTRAINTS_MAX && has_count<HAS_MAX; i++) {
        if (constraints[i].what != ID_HAS) continue;
        has_table[has_count].hand= constraints[i].who;
        has_table[has_count++].card=constraints[i].what;
        }
#endif
#ifdef BAD_V
    for (i=0; i<CONSTRAINT_MAX; i++) bad_v[0][i]=0; // bad_v[0] are accumulated diffs
#endif
#ifndef NO_LOGGING
    sprintf(temp_msg,"constraint_deal_n entered first:%u num:%u loops:%u",first,num,loops);
    gen32_do_log(1,20,temp_msg);
#endif
    // LogError(temp_msg);
#define NUMBER_OF_RETRIES 0
    for (active_hand=first; active_hand < first+num; active_hand++) {
        for (int i=0; i<100 && i<CONSTRAINT_MAX; i++) used_constraint[i]=99;


#ifdef BEST_HOFFER
        val=999; retries = NUMBER_OF_RETRIES;
        gen32_reset_individual_exchanges();
#else
        val=999; hand_threshhold = threshhold; retries = NUMBER_OF_RETRIES;
#endif
        setup_errorcheck2(&decks[active_hand][0],active_hand);
        // fprintf(stderr,"constraint_deal_n:active_hand:%d\n",active_hand);
#ifndef NO_LOGGING
        sprintf(temp_msg,"constraint deal, active hand %u started",active_hand);
        gen32_do_log(1,10,temp_msg);
#endif
#ifndef NO_ASSERTS
		assert(active_hand < DECKNUM);
#endif
		decks_flag[active_hand] = true; // turn to false if all attempts to deal a hand fail
			// ID_PLACED_CARDS(58) sets this up early and does not need to be done; given cards
		if (active_hand == first) {
            for (i = 0; i < 4; i++) already_placed[i] = 0; // specific cards count
			already_placed_count = 0;
            }
        already_played_flag = 0;
#ifdef BEST_HOFFER
        if (active_hand == first && !allow_placing) {
            check_constraints(v);  // use this to recalculate already_placed_count. Note: there are more efficient ways to do this
//            constrained_shuffle(); // shuffle up the available cards; moved above the first check_constraints call 200822
            }
#else
        if (active_hand == first) {
            check_constraints();  // use this to recalculate already_placed_count. Note: there are more efficient ways to do this
//            constrained_shuffle(); // shuffle up the available cards; moved above the first check_constraints call 200822
            }
#endif

        if (active_hand == first) {
            if (already_placed_count == 0) check_constraints(v);
            // errorcheck3 will test if the placed cards stay placed
            if (already_placed_count > 50) {
                fprintf(stderr,"already_placed_count is %d; breaking\n",already_placed_count);
                break;
                }
#if defined(ERRORCHECK3)
            setup_errorcheck3(active_hand);
#endif
            }
        // copy the hand (will be shuffled) that has the placed cards to all other hands
        if (active_hand == first) deck_copy_number(first,num);
#ifdef HOFFER_DEBUGGING
        if (already_placed_count > 51)
            fprintf(stderr,"already placed is 52-1\n");
            // return 0;  cannot deal as all cards have been placed
#endif
        // now shuffle the cards that have not been placed
        constrained_shuffle();
#if defined(ERRORCHECK3)
        if (!errorcheck3(active_hand)) 
            fprintf(stderr,"bad errorcheck3\n");
#if defined(HOOL_VERBOSE)
        else fprintf(stderr,"errorcheck3 is ok\n");
#endif        
#endif
        if (mul_13[num_of_hands] <= already_placed_count) {
#ifndef NO_LOGGING
            if (mul_13[num_of_hands] == already_placed_count)
                gen32_do_log(1,20,"FIND_EXCHANGE: already_placed_count is full\n");
            else
                gen32_do_log(1,40,"FIND_EXCHANGE ERR1: already_placed_count is too high\n");
#endif
            // continue; THIS HAD TO BE REMOVED 200902 to allow placing the last card
#ifdef HOFFER_DEBUGGING
            fprintf(stderr,"already placed is 52-2\n");
#endif
            }
		j=0;
        while (1) {
#ifdef HOFFER_DEBUGGING
            if  (j== (loops-1))
                printf("\nactive_hand:%d j:%d",active_hand,j);
#endif
			if (j >=loops) { // could not find a proper hand
				if (retries > 0) { j=0; retries--; val = 999; } // try again, usu 2 or 3 times, may be a waste of time.
				else {
#ifndef NO_ASSERTS
					assert(active_hand < DECKNUM);
#endif
                    decks_flag[active_hand] = false;
                    break; } // gave it all we got and failed each time on retries
				}
#ifdef RIGID
#ifdef BEST_HOFFER
			for (int _dir =0; _dir<4; _dir++) {
                if (j % modulo[_dir] == modulo[_dir]-1) {
                    if (delta[_dir] > 0) {
                        threshhold[_dir] += delta[_dir];
#ifdef HOFFER_DEBUGGING
                        fprintf(stderr,"\nthreshhold for %d increased to %d; j is %d",_dir,threshhold[_dir],j);
#endif
                        }
                    }
				}
#else
            if (j % modulo == modulo-1)
                hand_threshhold += delta;
#endif
#else
            if (j>20 && j % 10 == 1) hand_threshhold += 3;
#endif
            // if nearing the end only do 1 exchange at a time so as to keep the
            // mismatch as low as possible on return
            // otherwise up to 6 cards could be changed on the last pass
            if (loops - j < 50) num_of_exchanges = 1;
#ifndef BEST_HOFFER
            else if (hand_threshhold > 0) num_of_exchanges = rand() % 4 + 1;
#endif
            else num_of_exchanges = rand() % 6 + 1;
#ifndef SPEEDY
            { int ii; for (ii=0; ii<52; ii++) tdeck[ii] = decks[active_hand][ii]; }
            get_matt_string2(tdeck,htemp);
#endif
            if (already_placed_count > 51) {
#ifdef HOFFER_DEBUGGING
                fprintf(stderr,"breaking before find_exchange\n");
#endif
                val = 0;
                break; // all cards have already been placed
                }
             
#if defined(ERRORCHECK3)
            if (errorcheck3(active_hand) != 1) {
                fprintf(stderr,"bad errorcheck3 before exchanges\n");
                exit(0);
                }
#endif
            find_exchange(EXCHANGE,num_of_exchanges);
#if defined(ERRORCHECK3)
            if (errorcheck3(active_hand) != 1) {
                fprintf(stderr,"bad errorcheck3 after first exchange\n");
                exit(0);
                }
#endif

#ifndef SPEEDY
            { int ii; for (ii=0; ii<52; ii++) tdeck[ii] = decks[active_hand][ii]; }
            get_matt_string2(tdeck,htemp2);
#endif
#ifdef BAD_V
			for (int _i=0; _i < CONSTRAINT_MAX && _i < constraint_index; _i++) { 
				bad_v[1][_i]=0; // bad_v[1] are non-accumulated diffs
				for (int _j=2; _j < 6; _j++) bad_v[_j][_i]=0; 
				constraints[_i].diff = 0;
				constraints[_i].value = 0;
				}
#endif
#ifdef BEST_HOFFER
/* in best hoffer each direction has a threshhold, delta, and modulo.
   This was done so that human constraints can be handled differently than robot constraints.
   The robots will bid (in Tricky Bridge) exactly to the required 0 threshhold
   but, humans can do all sorts of incorrect bidding.
   So, for humans the threshhold will gradually increase as the loops increase to get to a point
   that the hand can be matched for the given constraints.
   In this way of constraining, if there is a failure, it is because the constraints for the robots directions
   has an innate incompatibility that must be fixed in the rules database.
   */
			{ int total; bool all_constraints_ok = true;
            if (already_placed_count > 51)
                fprintf(stderr,"already placed > 51 before calling check_constraints"); // all cards have already been placed
			total = check_constraints(v);
			if (total > val) {
                find_exchange(REVERSE,num_of_exchanges);	 // reverse the exchange
#if defined(ERRORCHECK3)
                if (errorcheck3(active_hand) != 1) {
                    fprintf(stderr,"bad errorcheck3 after reversing exchanges\n");
                    exit(0);
                    }
#endif
                }
#if defined(ERRORCHECK3)
            else if (errorcheck3(active_hand) != 1) {
                    fprintf(stderr,"bad errorcheck3 without reversing exchanges\n");
                    exit(0);
                    }
#endif
			else val = total;
			for (int dir=0; dir<4; dir++) 
				if (v[dir] >= threshhold[dir]) all_constraints_ok = false;
#define ULTIMATE_FIX
#if defined(ULTIMATE_FIX)
			if (all_constraints_ok || j == (loops - 1))
#else
            if (all_constraints_ok)
#endif
				break;
			}
#else
			v = check_constraints();		// see if this improves the situation
            if (v==0) {
            //	fprintf(stderr,"%u!",j);
            //	for (j=0;j<52;j++) deck[j]=decks[active_hand][j];
            //	fprintf(stderr,"\n%s",gen32_get_description2(0,0));
                }
            // else fprintf(stderr,"-");
            // LogError("back from check constraints");
            if (v > val)
                find_exchange(REVERSE,num_of_exchanges);	 // reverse the exchange
            else val = v;					// moving val downward is improving
			if (val < threshhold) break; // reached goal; usu threshhold would be 1 and val == 0 to be done; decks_flag remains true
#endif
			j++;
		}
#ifndef BEST_HOFFER
		fprintf(stderr,"val is %d for threshhold %d\n",val,threshhold);
		hand_and_bids(decks[active_hand], "", 0);
#endif

#ifndef SPEEDY
        if (errorcheck(decks[active_hand]) != 1) {
            for (i=0; i <52; i++) printf("%u %u ",i,decks[active_hand][i]);
            break;
            }
#endif
        if (j >= loops || val != 0) {
            int total_val=0,total_threshhold=0;
#define HOFFER_DEBUGGING
#ifdef HOFFER_DEBUGGING
#ifdef BEST_HOFFER
            for (int _dir=0; _dir<4; _dir++) {
                total_val += v[_dir];
                total_threshhold += threshhold[_dir];
                }
            if (total_val >= total_threshhold) {
#if !defined(ULTIMATE_FIX)
                fprintf(stderr,"\nfailed h:%u %u loops of %u with val of %u",active_hand,j,loops,val);
                decks_flag[active_hand] = false;
#endif
                }
            else if (total_val > 0 && total_val < total_threshhold) {
#if !defined(ULTIMATE_FIX)
                fprintf(stderr,"\ncomplied h:%u %u loops of %u with val of %u",active_hand,j,loops,val);
#endif
                
            }
#endif
#ifdef BEST_HOFFER
#if !defined(ULTIMATE_FIX)
            for (int _i=0; _i < 4; _i++)
                fprintf(stderr,"\nv[%d] %d for threshhold %d",_i,v[_i],threshhold[_i]);
#endif
#endif
#endif
#if defined(ULTIMATE_FIX)
#undef BAD_V
#endif
#ifdef BAD_V
    for (int i=0; i< CONSTRAINT_MAX && i < constraint_index; i++) {
        if (constraints[i].what == 28) break; // given card, ok to quit looking
		// if (val == 0) break;
		if (constraints[i].diff > 0) printf("\ni:%i who:%u what:%u value:%d-diff:%d",i,constraints[i].who, constraints[i].what, constraints[i].value,constraints[i].diff);
		if (bad_v[0][i] >= loop_counter) {
#if defined(CSW) && defined(WIN32)
            TRIGGER(302,printf("\nbad_v %d (%d,%d)",i,bad_v[0][i],bad_v[1][i]));
            TRIGGER(302,printf(" %d",constraints[i].who));
            TRIGGER(302,printf(" %d",constraints[i].what));
            TRIGGER(302,printf(" %d",constraints[i].where));
            TRIGGER(302,printf(" %d",constraints[i].lo));
            TRIGGER(302,printf(" %d",constraints[i].hi));
			if (bad_v[0][i] >= loop_counter) TRIGGER(302,printf(" <--"));
			switch (constraints[i].what) {
				case 3: TRIGGER(302,printf(" AND ")); break;
				case 7: TRIGGER(302,printf(" DIFF ")); break;
				case 4: TRIGGER(302,printf(" OR ")); break;
				case 10: TRIGGER(302,printf(" NOT ")); break;
				case 11: TRIGGER(302,printf(" XOR ")); break;
				case 54: TRIGGER(302,printf(" ORANDS ")); break;
				default: break;
				}
			if (constraints[i].diff > 0) TRIGGER(302,printf(" value:%d-diff:%d",constraints[i].value,constraints[i].diff));
#endif
            }
        }
#endif
#if defined(CSW) && defined(WIN32)
        TRIGGER(302,printf("\n"));
#endif
        }
        loop_counter += (j + loops * (NUMBER_OF_RETRIES-retries));  // takes into account that retries resets j to 0
        // if (val == 0 && active_hand < 10) printf("\nactive hand: %d",active_hand);
        // LogError("lc++");
#ifndef SPEEDY
        if (error_check2(&decks[active_hand][0],active_hand) == 1)
            fprintf(stderr,"oh shit");
#endif
        }
    active_hand = first;		 // return active hand to a reasonable state 
#ifndef NO_LOGGING
    sprintf(temp_msg,"constraint deal finished");
    gen32_do_log(1,20,temp_msg);
#endif
#ifdef BAD_V
    for (int i=0; i< CONSTRAINT_MAX; i++) {
        if (constraints[i].what == 28) break; //given card, ok to quit looking
        if (bad_v[0][i] >= loop_counter) {
#if defined(CSW) && defined(WIN32)
            TRIGGER(302,printf("bad_v %d",i));
            TRIGGER(302,printf(" %d",constraints[i].who));
            TRIGGER(302,printf(" %d",constraints[i].what));
            TRIGGER(302,printf(" %d",constraints[i].where));
            TRIGGER(302,printf(" %d",constraints[i].lo));
            TRIGGER(302,printf(" %d",constraints[i].hi));
            TRIGGER(302,printf("\n"));
#endif
            }
        }
#endif

	// I can set the return value to -1 for a val that does not come back to 0, but for bergen bidding this is best
	return (int) (loop_counter / (long)num); // gives an estimate of how hard it was to generate the set of hands.
}

void deck_copy_some(int);
int constraint_deal_exclude(int first, int number, int exclude_bitwise) {
	// exclude them
	fprintf(stderr,"exclude_bitwise in constraint_deal_exclude is:%d\n",exclude_bitwise);
    excludes = exclude_bitwise;
	set_offsets(15 & ~excludes ); // this new command of 220305 stops the hand from being shuffled
	deck_copy_some(number); // this new command of 220305 copys the first hand to others that will
	// be dealt rather than 10001 hands
	return constraint_deal(first, number);
	}

int constraint_deal(int first, int num) {
    // int i,j, v, val=999,loops=0,num_of_exchanges;
    // int loops=0; dropped for the global defined before set_exchanges
    first = first > 0 ? first : 0;
    first = first < DECKNUM ? first : 0;
    num = num > 0 ? num : 1;
    num = first + num <= DECKNUM ? num : DECKNUM - first;
    loops = loops > 0 ? loops : LOOP_MAX;
    loops = loops <= LOOP_MAX ? loops : LOOP_MAX;
    return constraint_deal_n(first,num,loops);
}

int restricted_constraint_deal_n(int first, int num, int loops) {
    int j, val=999,num_of_exchanges;
#ifdef BEST_HOFFER
	int v[4];
#else
	int v;
#endif
    long loop_counter=0;
    first = first > 0 ? first : 0;
    first = first < DECKNUM ? first : 0;
    num = num > 0 ? num : 1;
    num = first + num <= DECKNUM ? num : DECKNUM - first;
    loops = loops > 0 ? loops : LOOP_MAX;
    loops = loops <= LOOP_MAX ? loops : LOOP_MAX;
#ifndef NO_LOGGING
    sprintf(temp_msg,"restricted_constraint_deal entered first:%u num:%u",first,num);
    gen32_do_log(1,20,temp_msg);
#endif
    // LogError(temp_msg);
    for (active_hand=first; active_hand < first+num; active_hand++) {
        val=999;
        setup_errorcheck2(&decks[active_hand][0],active_hand);
#ifndef NO_LOGGING
        sprintf(temp_msg,"restricted_constraint deal, active hand %u started",active_hand);
        gen32_do_log(1,10,temp_msg);
#endif
        // for (i=0; i<4; i++) already_placed[i] = 0; // specific cards count
        // already_placed_count = 0;
        if (active_hand == first) {
            // gen32_shuffle(0,0,52); changed 210724
            gen32_shuffle(&decks[active_hand][0],0,52);
            check_constraints(v);  // use this to recalculate already_placed_count. Note: there are more efficient ways to do this
            }
		if (mul_13[num_of_hands] <= already_placed_count) {
#ifndef NO_LOGGING
            if (mul_13[num_of_hands] == already_placed_count)
                gen32_do_log(1,20,"FIND_EXCHANGE: already_placed_count is full\n");
            else
                gen32_do_log(1,40,"FIND_EXCHANGE ERR1: already_placed_count is too high\n");
#endif
            continue;
            }
        constrained_shuffle(); // shuffle up the available cards but after check_constraints has set up those restrictions
        for (j=0; j < loops && val > 0; j++) {
            // if nearing the end only do 1 exchange at a time so as to keep the
            // mismatch as low as possible on return
            // otherwise up to 6 cards could be changed on the last pass
            if (loops -j < 50) num_of_exchanges = rand() % 6 + 1;
            else num_of_exchanges = 1;
#ifndef SPEEDY
            { int ii; for (ii=0; ii<52; ii++) tdeck[ii] = decks[active_hand][ii]; }
            get_matt_string2(tdeck,htemp);
#endif
            find_exchange(EXCHANGE,num_of_exchanges);
#ifndef SPEEDY
            { int ii; for (ii=0; ii<52; ii++) tdeck[ii] = decks[active_hand][ii]; }
            get_matt_string2(tdeck,htemp2);
#endif
#ifdef BEST_HOFFER
			{ int total;
			total = check_constraints(v);
			if (total > val)
                find_exchange(REVERSE,num_of_exchanges);	 // reverse the exchange
            else val = total; // moving val downward is improving
			}
#else
            v = check_constraints();		// see if this improves the situation
            // LogError("back from check constraints");
            if (v > val)
                find_exchange(REVERSE,num_of_exchanges);	 // reverse the exchange
            else val = v; // moving val downward is improving
#endif
            }
#ifndef SPEEDY
        if (errorcheck(decks[active_hand]) != 1) {
            for (i=0; i <52; i++) printf("%u %u ",i,decks[active_hand][i]);
            break;
            }
#endif
        loop_counter += j;
        // LogError("lc++");
#ifndef SPEEDY
        if (error_check2(&decks[active_hand][0],active_hand) == 1)
            fprintf(stderr,"oh shit");
#endif
        }
    active_hand = first;		 // return active hand to a reasonable state 
#ifndef NO_LOGGING
    sprintf(temp_msg,"constraint deal finished");
    gen32_do_log(1,20,temp_msg);
#endif
    return (int) (loop_counter / (long)num);
}

int restricted_constraint_deal(int first, int num) {
    // int j, v, val=999,loops=0,num_of_exchanges;
    int loops=0;
    first = first > 0 ? first : 0;
    first = first < DECKNUM ? first : 0;
    num = num > 0 ? num : 1;
    num = first + num <= DECKNUM ? num : DECKNUM - first;
    loops = loops > 0 ? loops : LOOP_MAX;
    loops = loops <= LOOP_MAX ? loops : LOOP_MAX;
    return restricted_constraint_deal_n(first,num,loops);
}

char xdeal_out[53];
char *get_binary(int i) {
    char *o=xdeal_out;
    int j;
#ifndef NO_ASSERTS
    assert(i>=0);
    assert(i<DECKNUM);
#endif
    deck_verify(i);
    for (j=0; j<52; j++)
        *o++ = decks[i][j]+32;
    *o = '\0';
    return xdeal_out;
    }
    
// tidyup
    void deck_copy_gen32(int from) {
        int i;
        for (i=0; i<52; i++)
            gen32_deck[i] = decks[from][i];
    }
    
void deck_copy(int from, int to) {
    int i;
#ifndef NO_ASSERTS
    assert(from >= 0);
    assert(from < DECKNUM);
    assert(to < DECKNUM);
    assert(to >= 0);
    assert(from != to);
#endif
    deck_verify(from); deck_verify(to);
    for (i = 0; i<52; i++)
        decks[to][i] = decks[from][i];
    }

void deck_copy_to(unsigned int to[52]) {
	deck_verify(0);
	for (int j = 0; j < 52; j++)
			to[j] = decks[0][j];
}

void deck_copy_some(int n) {
	int i, j;
	deck_verify(0);
	for (i = 1; i < DECKNUM && i < n; i++)
		for (j = 0; j < 52; j++)
			decks[i][j] = decks[0][j];
}

void deck_copy_number(int first, int number) {
    int i, j;
    deck_verify(first);
    for (i=first+1; i < DECKNUM && i <= first + number; i++)
        for (j=0; j<52; j++)
            decks[i][j] = decks[first][j];
    }


void deck_copy_all(void) {
    int i,j;
    deck_verify(0);
    for (i=1; i < DECKNUM; i++)
        for (j=0; j<52; j++)
            decks[i][j] = decks[0][j];
    }
    
#define NO_POS 4
#undef PREVENT_BAD_EXCHANGE
#define HOOL_PLACEMENT
void find_exchange(int action, int num) {  // built for speed and in place exchanging of selected cards between hands
    
    // 220529 Unfortunately, already_placed_count is for all four hands, because of plays made

    if (already_placed_count > 51) {
        fprintf(stderr,"find_exchange failed with already_placed_count > 51");
        return;
        }
    else if (num > 3) {
        num=4; // added 220529 for hool debugging on linux
        // fprintf(stderr,"find exchange num was forced to 4\n");
        }
    int i,c1,c2,c1_max,c2_max,exclude,hand1,hand2,placed;
    static int temp[13][2];
    static int pos1=0, pos2=0;
#if defined(HOOL_PLACEMENT)
    int placed_count=0;
    if (indicator & 1) placed_count += already_placed[0];
    if (indicator & 2) placed_count += already_placed[1];
    if (indicator & 4) placed_count += already_placed[2];
    if (indicator & 8) placed_count += already_placed[3];
    // if (placed_count > 0) fprintf(stderr,"placed_count is %d\n",placed_count);
#endif

    int loops_done=0; 
    if (action == 0) {
        while (num > 0) {
            loops_done++;
#if defined(ERRORCHECK3)
            if (errorcheck3(active_hand) != 1) {
                fprintf(stderr,"errorcheck3...error is here:%d\n",loops_done);
                exit(0);
                }
#endif
#if defined(HOOL_PLACEMENT)
            c1_max = mul_13[num_of_hands]-placed_count;  // number of cards available
#else
            c1_max = mul_13[num_of_hands]-already_placed_count;  // number of cards available
#endif
            c1 = rand() % c1_max;	// call random to get both first hand and first card
            find_hand(c1,starters,NO_POS,&hand1,&pos1,&placed); // placed is the number of already placed cards in the picked hand
#if defined(HOOL_PLACEMENT)
            if ((mul_13[num_of_hands-1] - placed_count + placed) <= 0) {  // number available in these other hands
#else
            if ((mul_13[num_of_hands-1] - already_placed_count + placed) <= 0) {  // number available in these other hands
#endif
                //	fprintf(stderr,"LOW2 ");  // I dont have any more spots to place a card
                return;
                }
#if defined(HOOL_PLACEMENT)
            c2_max = mul_13[num_of_hands-1] - placed_count + placed; // number of cards available
#else
            c2_max = mul_13[num_of_hands-1] - already_placed_count + placed; // number of cards available
#endif
            c2 = rand() % c2_max; // call random again for second hand and second card
            exclude = starters[hand1] / 13; 
            find_hand(c2,offsets[hand1],exclude,&hand2,&pos2,&placed); // dont need hand2 or placed
#if SPEED_UP_HAS
            for (int i=0; i<has_count; i++) {
                if (has_table[i].hand == pos1/13 && has_table[i].card == decks[active_hand][pos1]) {
                    temp[num-1][0]=pos1; temp[num-1][1]=pos1; // no exchange, easier to insert this coding paradigm
                    goto skip_exchange;
                    }
                if (has_table[i].hand == pos2/13 && has_table[i].card == decks[active_hand][pos2]) {
                    temp[num-1][0]=pos1; temp[num-1][1]=pos1; // no exchange, easier to insert this coding paradigm
                    goto skip_exchange;
                    }
                }
#endif
#if defined(PREVENT_BAD_EXCHANGE)
               int hand1 = pos1 / 13;
                int hand2 = pos2 / 13;
                int placed1 = already_placed[hand1];
                int placed2 = already_placed[hand2];
                int tops[4] = {12,25,38,51};
                int top1 = tops[hand1];
                int bottom1 = top1 - placed1;
                if (pos1 > bottom1 && pos1 <= top1) printf("bad exchange1\n");
                int top2 = tops[hand2];
                int bottom2 = top2 - placed2;
                if (pos2 > bottom2 && pos2 <= top2) printf("bad exchange2\n");
            
#endif
                // fprintf(stderr,"exchanging pos1:%d pos2:%d\n",pos1,pos2);
            do_exchange(pos1,pos2);  // exchange the two selected cards from different hands
#if defined(ERRORCHECK3)
            if (errorcheck3(active_hand) != 1) {
                fprintf(stderr,"errorcheck3...error is here2:%d\n",loops_done);
#if defined(PREVENT_BAD_EXCHANGE)
                fprintf(stderr,"t1:%d t2%d b1:%d b2:%d\n",top1,top2,bottom1,bottom2);
#endif
                for (int k=0; k<4; k++) fprintf(stderr,"%d-%d; ",k,already_placed[k]);
                fprintf(stderr,"\n");
                exit(0);
                }
#endif
            temp[num-1][0]=pos1; temp[num-1][1]=pos2; // save that information for possible reversal later
#if SPEED_UP_HAS
skip_exchange:
#endif
            num--;	// the number of exchanges to do
            }
        }
    else {
        for (i=0; i<num; i++)
            do_exchange(temp[i][1],temp[i][0]); // replace as this was a bad selection;
        }
}



// the hand previously was determined just by modulo 13 of the cards in that number of hands
// but now that the already placed cards are implemented (by moving to top of the hand array and keeping count)
// the find_hand must take that into account
void find_hand(int position,int *initial,int exclude,int *hand,int *which,int *placed) {
    int i,positions=0,current=0,previous_positions=0;

    for (i=0;i<4;i++) {
        if (i==exclude) continue; // cannot look into the active hand that is giving up a card, go past
        if (1 << i & indicator) {	// see if active hand
            positions += (13 - already_placed[i]);	// accumulate number of positions
            if (position < positions) break;  // yup, in this hand
            else {
                current++;	// try the next hand up
                previous_positions = positions;  // needed to step back once found
                }
            }
        }
    *hand = current;  // this is relative to the active hands ordered
    *which = position - previous_positions + initial[current]; 
    *placed = already_placed[i];
}

void rid(int hand, int position) {
    int i, hand1, hand2, pos1, pos2;
    i = rand() % mul_13[num_of_hands-1];
    hand1 = get_offset(hand);
    hand2 = i/13;
    pos1 = starters[hand1] + position;
    pos2 = offsets[hand1][hand2] + i % 13;
    do_exchange(pos1,pos2);
}

void gen32_set_seed(int n) { // note that srand should not be called multiple times
    static int initialized = 0;
    if (initialized) {
        fprintf(stderr,"initialized via set_seed multiple times");
        exit(0);
        }
    if (n==0) srand((int)time(0));
    else srand(n);
    initialized=1;
    }

int find_spot(int selected_position) {
    int i,positions=0,previous_positions=0;
    for (i=0;i<4;i++) {
        if (1 << i & indicator) {	// see if active hand
            positions += (13 - already_placed[i]);	// accumulate number of already_placed positions
            if (selected_position < positions) break;  // yup, in this hand
            else previous_positions = positions;  // needed to step back once found
            }
        }
    //	  1:start of hand		3:index to which one
    //			|					   |										 |			
    return (i * 13) + (selected_position - previous_positions);  
    }



void constrained_shuffle(void) {
    int count,selected_position,top_position;
    deck_verify(active_hand);
    for (count = mul_13[num_of_hands] - already_placed_count; count > 1; count--) {
        selected_position = find_spot(rand() % count);
        top_position = find_spot(count-1);
        do_exchange(selected_position,top_position); // could be same position but faster to exchange than check that out
        }
}

int longest_suit(int hand) {
    int i,l=0;
    for (i=1;i<4;i++)
        if (aDist[hand][i] > aDist[hand][l]) l=i;
    return l;
}



void x_set_deck(int card, int value) {
	gen32_deck[card] = value;
}

// tidyup - this was in a comment before 211212
#if defined(MAKE_DESCRIPTION)
char *make_description(char *in) {
	int suit=3,rank,position=0;
	char c;
    if (in[0]=='E') printf("make_description error\n");
	while ((c = *in++)) {
		if (c=='.') suit--;
        else if (c==' ' || c==':') break;  // tidyup
		else {
			if (c=='A' || c=='a') rank = 12;
			else if (c=='K' || c=='k') rank = 11;
			else if (c=='Q' || c=='q') rank = 10;
			else if (c=='J' || c=='j') rank = 9;
			else if (c=='T' || c=='t') rank = 8;
			else rank = c-'2';
            if (rank < 0 || rank > 12 || suit < 0 || suit > 4) printf("make_description error\n");
			// assert(rank >= 0 && rank <= 12); replaced 211212 to print not crash
			// assert(suit >= 0 && suit <= 3);
			else x_set_deck(position++,suit*13+rank);
			}
		}
	return gen32_get_description2(0,0);
}
#endif
    
#define NEW_HOFFER_COUNT
#if defined(NEW_HOFFER_COUNT)
    // fix for the length of supported 211123 corrected
    // fix for the reduction in count of minor short honors excepted for the k 211123
    
    int gen32_hoffer_count(int deckp[52],int start,int number,int state, int suit)
    {
        int i, j, disth[4], hcp_total=0,hcps[4],kings[4];
        int count=0, shortness=0, slength=0, max_shortness=0;
        gen32_distro(deckp,start,number,disth);
        for (i=0; i<4; i++) { hcps[i]=0; kings[i]=0; }
        for (i=start; i<start + number; i++) {
            if ((j=deckp[i] % 13) > 8) {
                hcps[deckp[i]/13] += (j-8);
                if (j==11) kings[deckp[i]/13]=1; //  need to separate kings from QJ
                hcp_total += (j-8);
            }
        }
        
        for (i=0; i< 4; i++) {
          if (disth[i] < 3) {  /* SHORTNESS */  /* only after support has been established */
            if (state == SUPPORT_OF) switch (disth[suit]) {
                case 0:
                case 1:
                case 2: break;
                case 3: if (disth[i]== 0) { count += 3; shortness += 3; if (max_shortness < 3) max_shortness =3; }  /* void */ /* three trump support */
                else if (disth[1] == 1) { count += 2; shortness += 2; if (max_shortness < 2) max_shortness=2; }/* singleton */
                else { count += 1; shortness += 1; if (max_shortness < 1) max_shortness=1;} break; /* doubleton */
                case 4: if (disth[i] == 0) { count += 4; shortness += 4; if (max_shortness < 4) max_shortness = 4;}/* four trump support */
                else if (disth[i] == 1) { count += 3; shortness += 3; if (max_shortness < 3) max_shortness=3; }
                else { count += 1; shortness += 1; if (max_shortness < 1) max_shortness=1; } break;
                default: if (disth[i] == 0) { count += 5; shortness += 5; if (max_shortness<5) max_shortness=5; } /* five+ trump support */
                else if (disth[i] == 1) { count += 3; shortness += 3; if(max_shortness<3) max_shortness=3;}
                else { count += 1; shortness += 1; if (max_shortness < 1) max_shortness = 1;} break;
            }
            if (shortness > max_shortness) shortness = max_shortness; // do not count two separate short suits; use the best only
            else if (state == SUPPORTED) switch (disth[i]) {
                case 0: { count += 4; shortness += 3; } break;
                case 1: { count += 2; shortness += 2; } break;
                case 2: { count += 1; shortness += 1; } break;
                default: break;
            }
          }    /* SHORTNESS */
          if (disth[i]>4) { slength += (disth[i]-4); }/* length of suit over 4 */
            // fix 211123
            if (disth[i] > 4 && state==SUPPORTED) { slength += (disth[i]-4); }/* length of suit over 4 additional after being supported */
        }
        // fix 211123 checking for kings vs QJ that was not being done before
        for (i=0; i<4; i++) { /* correct for j, q, jx, qx, qj; and jxx but 211128 not kx */
            if (disth[i]==2 && kings[i]==1) continue; // not the kx 211128
            if ((disth[i]<3 && hcps[i] > 0 && hcps[i] < 4) || (disth[i]==3 && hcps[i]==1)) {
                if (state != SUPPORT_OF && kings[i]==0) hcp_total -= 1;
                else if (state == SUPPORT_OF && suit != i && kings[i]==0) hcp_total -= 1;// count if in trump suit
                if (state != SUPPORT_OF && hcps[i]==3 && kings[i]==0) hcp_total -= 1; //211112 change QJ from 2 to 1
                else if (state == SUPPORT_OF && suit != i && hcps[i]==3 && kings[i]==0) hcp_total -= 1; // count if in trump suit
                }
            }
        return hcp_total + slength + shortness;
    }

    
    
    
    
#else

int gen32_hoffer_count(int deckp[52],int start,int number,int state, int suit)
{
    int i, j, disth[4], hcp_total=0,hcps[4];
    int count=0, shortness=0, slength=0;
    gen32_distro(deckp,start,number,disth);
	for (i=0; i<4; i++) hcps[i]=0;
    for (i=start; i<start + number; i++) {
        if ((j=deckp[i] % 13) > 8) {
			hcps[deckp[i]/13] += (j-8);
            hcp_total += (j-8);
        }
    }
	
    for (i=0; i< 4; i++) {
      if (disth[i] < 3) {  /* SHORTNESS */  /* only after support has been established */
        if (state == SUPPORT_OF) switch (disth[suit]) {
            case 0:
            case 1:
            case 2: break;
            case 3: if (disth[i]== 0) { count += 3; shortness += 3;}  /* void */ /* three trump support */
            else if (disth[1] == 1) { count += 2; shortness += 2; }/* singleton */
            else { count += 1; shortness += 1;} break; /* doubleton */
            case 4: if (disth[i] == 0) { count += 4; shortness += 4; }/* four trump support */
            else if (disth[i] == 1) { count += 3; shortness += 3; }
            else { count += 1; shortness += 1; } break;
            default: if (disth[i] == 0) { count += 5; shortness += 5; } /* five+ trump support */
            else if (disth[i] == 1) { count += 3; shortness += 3;}
            else { count += 1; shortness += 1; } break;
        }
        else if (state == SUPPORTED) switch (disth[i]) {
            case 0: { count += 4; shortness += 3; } break;
            case 1: { count += 2; shortness += 2; } break;
            case 2: { count += 1; shortness += 1; } break;
            default: break;
        }
      }    /* SHORTNESS */
      if (disth[i]>4) { slength += (disth[i]-4); }/* length of suit over 4 */
    }
	for (i=0; i<4; i++) /* correct for j, q, jx, qx, qj; and jxx */
		if ((disth[i]<3 && hcps[i] > 0 && hcps[i] < 4) || (disth[i]==3 && hcps[i]==1)) {
			if (state != SUPPORT_OF) hcp_total -= 1;
			else if (state == SUPPORT_OF && suit != i) hcp_total -= 1;
			}
    return hcp_total + slength + shortness;
}
#endif
    
int gen32_roth(int deckp[52],int start,int number,int state, int suit)
{
	int i, j, count, dist[4], hcp[4], aces[4], ace_flag;
	int doubleton_flag, kings[4];
    int honors[4];
	ace_flag = count = 0;
	gen32_distro(deckp,start,number,dist);
	for (i=0; i<4; i++) { hcp[i]=0; aces[i]=0; kings[i]=0; honors[i]=0;}
	for (i=start; i<start + number; i++) {
		if ((j=deckp[i] % 13) > 8) {
			hcp[deckp[i] / 13] += j-8;
            honors[deckp[i]/13] += (1 << (j-9));
            }
		if (j == 12) { ace_flag += 1; aces[deckp[i]/13]=1;}
		if (j == 11) { kings[deckp[i]/13]=1; }
		}
	doubleton_flag=0;
	for (i=0; i<4; i++) {
		if (dist[i] < 3) {
			if (state == SUPPORT_OF) switch (dist[suit]) {
				case 0:
				case 1:
				case 2: break;
				default:
                 // fall through       
				case 5: if (dist[i] <2) count += 1;
                    // fall through
				case 4: if (dist[i] <2) count += 1;
						if (dist[i] == 2 && doubleton_flag == 0) {
							count += 1;
							doubleton_flag = 1;
							}
                    // fall through
				case 3: count += (3-dist[i]);
                        break;
				}
			else count += (3-dist[i]);
			}
		if (dist[i] == 6 && i > 1) count += 1;	/* major */
		else if (dist[i] >= 7 && i > 1) count += 2;	/* major */
		else if (dist[i] == 6 && i < 2 && hcp[i]>1) count += 1; /* minor with honor */
		else if (dist[i] >= 7 && i < 2 && hcp[i]>1) count += 2;	/* minor with honor */
		}
	for (i=0; i<4; i++)
		count += hcp[i];
#ifdef IN_OUT_VALUATION
	for (i=0; i<4; i++)
		if (opp_bid[i]) switch(honors[i]) {
			case 0: break;
			case 1: case 2: count--; break;
			case 3: if (dist[i] == 2) count -= 2; else count--; break;
 			case 4: case 5: case 6: if (opp_bid[i] & 1) count++; else count--; break;
			case 7: case 8: break;
			case 9: if (opp_bid[i] & 2) count--; break;
			case 10: case 11: if (opp_bid[i] & 1) count++; else count--; break;
			case 12: break;
			case 13: if (opp_bid[i] & 1) count++; else count--; break;
			default: break;
			}
		else if (state == SUPPORT_OF && i == suit) switch(honors[i]) {
			case 0: break;
			case 1: case 2: case 3: count++; break;
			case 4: break;
			case 5: count++; break;
			case 6: case 7:	case 8: break;
			case 9: count++; break;
			default:  break;
			}
		else switch(honors[i]) {
			case 0: break;
			case 1: case 2: if (dist[i] == 1) count--; break;
			case 3: if (dist[i] == 2) count--; break;
			case 4: if (dist[i] == 1) count--; break;
			case 5: case 6: if (dist[i] == 2) count--; break;
			case 7: case 8: break;
			case 9: if (dist[i] == 2) count--; break;
			default: break;
			}
#else
	for (i=0; i<4; i++) {
        if (state == SUPPORT_OF && i == suit) /* not for support */ ;
		else {
		  if (dist[i]==1 && hcp[i] >0 && hcp[i]<4) count -= 1;
		  if (dist[i]==2 && hcp[i] >0
			 && hcp[i] != 3 && hcp[i]<6 && aces[i]==0) count -= 1;
          if (dist[i]==2 && hcp[i] == 3 && kings[i]==0) count -= 1;
		  if (dist[i]==3 && hcp[i] == 1 ) count -= 1;
          }
		}
#endif
     for (i=0; i<4; i++) {
        if (state == SUPPORTED) {
            if (dist[i] == 0) count += 3;
            else if (dist[i] == 1) count += 2;
            }
        }
	if (ace_flag == 4 && state == OPENING_BID) count += 1;
	if (count > 10 && ace_flag == 0 && state == OPENING_BID) count -= 1;
    if (count <0) count = 0;
	return(count);
}

int gen32_kaplan_rueben(int deckp[52],int start,int number,int state, int suit)
{
	int i, j, dist[4], hcp[4], aces[4];
	int kings[4], queens[4], jacks[4], nines[4];
	int honors[4], topfive[4];
	float tens[4], accompanied[4],temp[4],subtotal[4],guards[4], four333=0.0,total;
	int ace_flag, count,ace_king_total=0;
	// int  adjust, tlength, xlength, shortness;
	// adjust = tlength = xlength = shortness = 0;
	ace_flag = count = 0;
	gen32_distro(deckp,start,number,dist);
	for (i=0; i<4; i++) { hcp[i]=0; aces[i]=0; kings[i]=0; honors[i]=0; tens[i]=0.0; topfive[i]=0;
		queens[i]=0; jacks[i]=0; temp[i]=0.0; subtotal[i]=0.0; total=0.0; guards[i]=0.0; four333=0.0;
		accompanied[i]=0.0;
		}
	for (i=start; i<start + number; i++) {
		if ((j=deckp[i] % 13) > 8) {
			hcp[deckp[i] / 13] += (j-8);
			honors[deckp[i]/13] += (1 << (j-9));
			}
		if (j==12) { guards[deckp[i]/13] += 3.0; }
		if (j==11) {
			if (dist[deckp[i]/13]==1) guards[deckp[i]/13] += 0.5;
			else guards[deckp[i]/13] += 2.0;
			}
		if (j==10) {
			if (dist[deckp[i]/13]>2) { // queen 3+
				if (aces[deckp[i]/13] ==1 || kings[deckp[i]/13] == 1)
					guards[deckp[i]/13] += 1.0;
				else guards[deckp[i]/13] += 0.75;
				}
			else if (dist[deckp[i]/13]==2) {
				if (aces[deckp[i]/13] ==1 || kings[deckp[i]/13] == 1)
					guards[deckp[i]/13] += 0.5;
				else guards[deckp[i]/13] += 0.25;
				}
			}
		if (j==9) { // jack
			if (topfive[deckp[i]/13] == 2) guards[deckp[i]/13] += 0.5;
			else if (topfive[deckp[i]/13] ==1) guards[deckp[i]/13] += 0.25;
			}
		if (j==8) { // ten
			if (topfive[deckp[i]/13] == 2) guards[deckp[i]/13] += 0.5;
			}
		if (j==7) { // nine and, next,  ten
			if (tens[deckp[i]/13] > 0.4 && topfive[deckp[i]/13] == 2) guards[deckp[i]/13] += 0.25;
			}
		if (j>10) { ace_king_total += (j-8); }
		if (j>7) {
			topfive[deckp[i]/13] += 1;
			if (j == 12) { ace_flag += 1; aces[deckp[i]/13]=1;}
			else if (j == 11) { kings[deckp[i]/13]=1; }
			else if (j==8) {
				tens[deckp[i]/13]=0.5;
				if (jacks[deckp[i]/13]==1) accompanied[deckp[i]/13]= 0.5;
				else if (topfive[deckp[i]/13] > 2) accompanied[deckp[i]/13]=0.5;
				}
			}
		if (j==7) {  // nine with ten or two higher
			nines[deckp[i]/13] = 1;
			if (topfive[deckp[i]/13] > 1) accompanied[deckp[i]/13] += 0.5;
			else if (tens[deckp[i]/13] > 0.4) accompanied[deckp[i]/13] += 0.5;
			if (topfive[deckp[i]/13] == 3 && tens[deckp[i]/13] < 0.4 &&
					dist[i]>3 && dist[i]<7) temp[deckp[i]/13] += 0.5;
			}
		if (j==6) { // eight
			if (nines[deckp[i]/13] && topfive[deckp[i]/13] < 2 && tens[deckp[i]/13] < 0.4)
				accompanied[deckp[i]/13] += 0.5; // eight accompanies nine
			if (temp[deckp[i]/13]>0.4) temp[deckp[i]/13] = 0.0; // no eight allowed
			}
		}
	for (i=0; i<4; i++) {
		if (temp[i] > 0.4) accompanied[i] += 0.5;
		if (dist[i] > 6 && (queens[i]==0 || jacks[i]==0)) accompanied[i] += 1.0;
		if (dist[i] > 7 && queens[i]==0) accompanied[i] += 1.0;
		if (dist[i] > 8 && queens[i]==0 && jacks[i]==0) accompanied[i] += 1.0;
		}
	for (i=0; i<4; i++) {
		float flen = (float) dist[i]; 
		subtotal[i] = (float) hcp[i]; // a-j 4321
		subtotal[i] += tens[i]; // add 0.5 if
		subtotal[i] += accompanied[i]; // add the accompanied totals
		subtotal[i] *= flen;
		subtotal[i] /= 10.0;
		}
	for (i=0; i<4; i++) {
		subtotal[i] += guards[i];
		}
	for (i=0; i<4; i++) {
		if (dist[i]<3 || dist[i]>4) four333=0.5;
		if (dist[i]==0) subtotal[i] += 3.0;
		else if (dist[i]==1) subtotal[i] += 2.0;
		else if (dist[i]==2) subtotal[i] += 1.0;
		}
	total = four333;
	for (i=0; i<4; i++) {
		total += subtotal[i];
		}
	return (int) floor(total+0.5);
}
int gen32_klinger(int deckp[52],int start,int number,int state, int suit)
{
	int i, j, dist[4], hcp[4], aces[4];
	int kings[4];
	int honors[4], topfive[4];
	int ace_flag, count,ace_king_total=0;
    int has_4333_flag=0,no_aces_flag=0;
	int quality, slength, dubious, hcps, adjust, tlength, xlength, shortness;
	quality = slength = dubious = hcps = adjust = tlength = xlength = shortness = 0;
	ace_flag = count = 0;
	gen32_distro(deckp,start,number,dist);
	for (i=0; i<4; i++) { hcp[i]=0; aces[i]=0; kings[i]=0; honors[i]=0; topfive[i]=0; }
	for (i=start; i<start + number; i++) {
		if ((j=deckp[i] % 13) > 8) {
			hcp[deckp[i] / 13] += (j-8);
			honors[deckp[i]/13] += (1 << (j-9));
			}
		if (j>10) { ace_king_total += (j-8); }
		if (j>7) {
			topfive[deckp[i]/13] += 1;
			if (j == 12) { ace_flag += 1; aces[deckp[i]/13]=1;}
			else if (j == 11) { kings[deckp[i]/13]=1; }
			}
		}
	for (i=0; i<4; i++) {
		if (topfive[i] >= 3 && dist[i]>3) { count += 1; quality += 1; } /* quality suits of 4+ length */
		if (dist[i] < 3) {  /* SHORTNESS */  /* only after support has been established */
			if (state == SUPPORT_OF) switch (dist[suit]) {
				case 0:
				case 1:
				case 2: break;
				case 3: if (dist[i]== 0) { count += 3; shortness += 3;}  /* void */ /* three trump support */
						else if (dist[1] == 1) { count += 2; shortness += 2; }/* singleton */
						else { count += 1; shortness += 1;} break; /* doubleton */
				case 4: if (dist[i] == 0) { count += 4; shortness += 4; }/* four trump support */
						else if (dist[i] == 1) { count += 3; shortness += 3; }
						else { count += 1; shortness += 1; } break;
				default: if (dist[i] == 0) { count += 5; shortness += 5; } /* five+ trump support */
						 else if (dist[i] == 1) { count += 3; shortness += 3;}
						 else { count += 1; shortness += 1; } break;
				}
			else if (state == SUPPORTED) switch (dist[i]) {
				case 0: { count += 4; shortness += 3; } break;
				case 1: { count += 2; shortness += 2; } break;
				case 2: { count += 1; shortness += 1; } break;
				default: break;
				}
			}    /* SHORTNESS */
		if (dist[i]>4) { slength += (dist[i]-4); }/* length of suit over 4 */
		if (dist[i]>4 && state == SUPPORTED) {
			int ii;
			if (dist[i] > 5) {
				count += (dist[i] - 5); /* 5+ trump length additional after supported */
				tlength = dist[i] - 5;
				}
			for (ii=0; ii<4; ii++) /* look for side 4 card suit or longer */
				if (ii!=i && dist[ii] > 3 && xlength == 0) {
					count += 1;
					xlength += 1;
					}
			}
	}
#ifdef DEBUG_KLINGER
	printf("\nquality:%u",quality);
	printf("\nlength:%u",slength);
	if (tlength > 0) printf("\ntlength:%u",tlength);
	if (xlength > 0) printf("\nxlength:%u",xlength);
	if (shortness > 0) printf("\nshortness:%u",shortness);
#endif
	for (i=0; i<4; i++) {
		hcps += hcp[i];
		if (aces[i]) no_aces_flag = 0;
		if (dist[i] < 3) has_4333_flag = 0; 
		if (dist[i] == 1 && hcp[i]>0 && hcp[i] < 4) { dubious += 1; }  /* singleton k,q,j */
		else if (dist[i] == 2 && hcp[i]>0 && hcp[i] < 4 && kings[i]==0) { dubious += 1; } /* jx,qx,qj */
		}
	if (hcps < 12) no_aces_flag = 0; // only 12 HCPs or more without an expected ace 
#ifdef DEBUG_KLINGER
	printf("\nhcps:%u",hcps);
	printf("\ndubious:%u",dubious);
#endif
	{
		int total;
	if (ace_king_total > 12) hcps += 1;
	total = hcps - dubious - no_aces_flag - has_4333_flag + shortness;
	if (state != KLINGER_HCPS)
    	 total = hcps - dubious - no_aces_flag - has_4333_flag + slength + shortness;
    else total = hcps - dubious - no_aces_flag - has_4333_flag;
	if (total < 0) total = 0;
	return total;
	}
}
int gen32_bbo(int deckp[52],int start, int number, int state, int suit) { return 0; }

int gen32_bergen(int deckp[52],int start,int number,int state, int suit)
{
	int i, j, dist[4], hcp[4], aces[4];
	int doubleton_flag = 0, kings[4];
    int honors[4], topfive[4];
	int ace_flag, count, quacks, acetens;
	int quality, slength, dubious, hcps, adjust, tlength, xlength, shortness;
	quality = slength = dubious = hcps = adjust = tlength = xlength = shortness = 0;
	ace_flag = count = quacks = acetens = 0;
#ifdef DEBUG_BERGEN
	printf("\nstate: %d",state);                                                         
#endif
	gen32_distro(deckp,start,number,dist);
	for (i=0; i<4; i++) { hcp[i]=0; aces[i]=0; kings[i]=0; honors[i]=0; topfive[i]=0; }
	for (i=start; i<start + number; i++) {
		if ((j=deckp[i] % 13) > 8) {
			hcp[deckp[i] / 13] += (j-8);
            honors[deckp[i]/13] += (1 << (j-9));
            }
		if (j>7) {
			topfive[deckp[i]/13] += 1;
			if (j == 12) { ace_flag += 1; aces[deckp[i]/13]=1; acetens += 1;}
			else if (j == 11) { kings[deckp[i]/13]=1; }
			else if (j == 10 || j == 9) { quacks += 1; }
			else if (j == 8) { acetens += 1; }
			}
		}
	for (i=0; i<4; i++) {
		if (topfive[i] >= 3 && dist[i]>3) { count += 1; quality += 1; } /* quality suits of 4+ length */
		if (dist[i] < 3) {  /* SHORTNESS */  /* only after support has been established */
			if (state == SUPPORT_OF) switch (dist[suit]) {
				case 0:
				case 1:
				case 2: break;
				case 3: if (dist[i]== 0) { count += 3; shortness += 3;}  /* void */ /* three trump support */
						else if (dist[1] == 1) { count += 2; shortness += 2; }/* singleton */
						else { count += 1; shortness += 1;} break; /* doubleton */
				case 4: if (dist[i] == 0) { count += 4; shortness += 4; }/* four trump support */
						else if (dist[i] == 1) { count += 3; shortness += 3; }
						else { count += 1; shortness += 1; } break;
				default: if (dist[i] == 0) { count += 5; shortness += 5; } /* five+ trump support */
						 else if (dist[i] == 1) { count += 3; shortness += 3;}
						 else { count += 1; shortness += 1; } break;
				}
			else if (state == SUPPORTED) switch (dist[i]) {
				case 0: { count += 4; shortness += 4; } break;
				case 1: { count += 2; shortness += 2; } break;
				case 2: if (doubleton_flag > 0) { count += 1; shortness += 1; }  
					doubleton_flag += 1; break;
				default: break;
				}
			} /* SHORTNESS */
		if (dist[i]>4) { count += (dist[i]-4); slength += (dist[i]-4); }/* length of suit 5+ */
		if (dist[i]>4 && state == SUPPORTED) {
			int ii;
			if (dist[i] > 5) {
				count += (dist[i] - 5); /* 5+ trump length additional after supported */
				tlength = dist[i] - 5;
				}
			for (ii=0; ii<4; ii++) /* look for side 4 card suit or longer */
				if (ii!=i && dist[ii] > 3 && xlength == 0) {
					count += 1;
					xlength += 1;
					}
			}
	}
#ifdef DEBUG_BERGEN
	printf("\nquality:%u",quality);
	printf("\nslength:%u",slength);
	if (tlength > 0) printf("\ntlength:%u",tlength);
	if (xlength > 0) printf("\nxlength:%u",xlength);
	if (shortness > 0) printf("\nshortness:%u",shortness);
#endif
	for (i=0; i<4; i++) { /* i do not know how to do 1/2 point for singleton ace */
		count += hcp[i]; /* first add up hcps, then remove dubious honors */
		hcps += hcp[i];
		if (dist[i] == 1 && hcp[i]>0 && hcp[i] < 4) { count -= 1; dubious += 1; }  /* singleton k,q,j */
		else if (dist[i] == 2 && hcp[i]>0 && hcp[i] < 4 && kings[i]==0) { count -= 1; dubious += 1; } /* jx,qx,qj */
		else if (dist[i] == 2 && hcp[i] == 4 && aces[i] == 0) { count -= 1; dubious += 1; } /* kj */
		else if (dist[i] == 2 && hcp[i] == 5) { count -= 1; dubious += 1; } /* aj,kq */
		}
#ifdef DEBUG_BERGEN
	printf("\nhcps:%u",hcps);
	printf("\ndubious:%u",dubious);
#endif
	if (acetens - quacks >= 6) { count += 2; adjust += 2; }
	else if (acetens - quacks > 2) { count += 1; adjust += 1; }
	else if (quacks - acetens >= 6) { count -= 2; adjust -= 2; }
	else if (quacks - acetens > 2) { count -= 1; adjust -= 1; }
#ifdef DEBUG_BERGEN
	printf("\nadjust:%u",adjust);
	printf("\ncount:%u\n***********\n",count);
	if (state==BERGEN_HCPS) printf("\nhcps+adjust-dubious+slength+quality = %d",hcps+adjust+slength-dubious+quality);
#endif
	if (state == BERGEN_HCPS) return hcps+adjust-dubious+slength+quality;
	else return(count);
}


#ifndef WIN_ONLY
int kaplan_rueben(int deck[52],int start,int number,int state, int suit)
{
    int i, j, dist[4], hcp[4], aces[4];
    int kings[4], queens[4], jacks[4], nines[4];
    
    int honors[4], topfive[4];
    float tens[4], accompanied[4],temp[4],subtotal[4],guards[4], four333=0.0,total;
    int ace_flag, count,ace_king_total=0;
    // int tlength, xlength, shortness;
    // tlength = xlength = shortness = 0;
    ace_flag = count = 0;

    gen32_distro(gen32_deck,start,number,dist);

    for (i=0; i<4; i++) { hcp[i]=0; aces[i]=0; kings[i]=0; honors[i]=0; tens[i]=0.0; topfive[i]=0;
        queens[i]=0; jacks[i]=0; temp[i]=0.0; subtotal[i]=0.0; total=0.0; guards[i]=0.0; four333=0.0;
        accompanied[i]=0.0;
        }

    /* get high card points totaled by suit and put in hcp array  */
    /* also get the number of honors including tens in their suits  */
    /* note that in KR a ten will 0.5 HCP, so get tens */

    for (i=start; i<start + number; i++) {
        if ((j=deck[i] % 13) > 8) {
            hcp[deck[i] / 13] += (j-8);
            honors[deck[i]/13] += (1 << (j-9));
            }
        if (j==12) { guards[deck[i]/13] += 3.0; }
        if (j==11) {
            if (dist[deck[i]/13]==1) guards[deck[i]/13] += 0.5;
            else guards[deck[i]/13] += 2.0;
            }
        if (j==10) {
            if (dist[deck[i]/13]>2) { // queen 3+
                if (aces[deck[i]/13] ==1 || kings[deck[i]/13] == 1)
                    guards[deck[i]/13] += 1.0;
                else guards[deck[i]/13] += 0.75;
                }
            else if (dist[deck[i]/13]==2) {
                if (aces[deck[i]/13] ==1 || kings[deck[i]/13] == 1)
                    guards[deck[i]/13] += 0.5;
                else guards[deck[i]/13] += 0.25;
                }
            }
        if (j==9) { // jack
            if (topfive[deck[i]/13] == 2) guards[deck[i]/13] += 0.5;
            else if (topfive[deck[i]/13] ==1) guards[deck[i]/13] += 0.25;
            }
        if (j==8) { // ten
            if (topfive[deck[i]/13] == 2) guards[deck[i]/13] += 0.5;
            }
        if (j==7) { // nine and, next,  ten
            if (tens[deck[i]/13] > 0.4 && topfive[deck[i]/13] == 2) guards[deck[i]/13] += 0.25;
            }
        if (j>10) { ace_king_total += (j-8); }
        if (j>7) {
            topfive[deck[i]/13] += 1;
            if (j == 12) { ace_flag += 1; aces[deck[i]/13]=1;}
            else if (j == 11) { kings[deck[i]/13]=1; }
            else if (j==8) {
                tens[deck[i]/13]=0.5;
                if (jacks[deck[i]/13]==1) accompanied[deck[i]/13]= 0.5;
                else if (topfive[deck[i]/13] > 2) accompanied[deck[i]/13]=0.5;
                }
            }
        if (j==7) {  // nine with ten or two higher
            nines[deck[i]/13] = 1;
            if (topfive[deck[i]/13] > 1) accompanied[deck[i]/13] += 0.5;
            else if (tens[deck[i]/13] > 0.4) accompanied[deck[i]/13] += 0.5;
            if (topfive[deck[i]/13] == 3 && tens[deck[i]/13] < 0.4 &&
                    dist[i]>3 && dist[i]<7) temp[deck[i]/13] += 0.5;
            }
        if (j==6) { // eight
            if (nines[deck[i]/13] && topfive[deck[i]/13] < 2 && tens[deck[i]/13] < 0.4)
                accompanied[deck[i]/13] += 0.5; // eight accompanies nine
            if (temp[deck[i]/13]>0.4) temp[deck[i]/13] = 0.0; // no eight allowed
            }
        }
    for (i=0; i<4; i++) {
        if (temp[i] > 0.4) accompanied[i] += 0.5;
        if (dist[i] > 6 && (queens[i]==0 || jacks[i]==0)) accompanied[i] += 1.0;
        if (dist[i] > 7 && queens[i]==0) accompanied[i] += 1.0;
        if (dist[i] > 8 && queens[i]==0 && jacks[i]==0) accompanied[i] += 1.0;
        }
    for (i=0; i<4; i++) {
        float flen = (float) dist[i]; 
        subtotal[i] = (float) hcp[i]; // a-j 4321
        subtotal[i] += tens[i]; // add 0.5 if
        subtotal[i] += accompanied[i]; // add the accompanied totals
        subtotal[i] *= flen;
        subtotal[i] /= 10.0;
        }
    for (i=0; i<4; i++) {
        subtotal[i] += guards[i];
        }
    for (i=0; i<4; i++) {
        if (dist[i]<3 || dist[i]>4) four333=0.5;
        if (dist[i]==0) subtotal[i] += 3.0;
        else if (dist[i]==1) subtotal[i] += 2.0;
        else if (dist[i]==2) subtotal[i] += 1.0;
        }
    total = four333;
    for (i=0; i<4; i++) {
        total += subtotal[i];
        }
    return (int) floor(total+0.5);
}
#endif

char *gen32_get_description2(int i, int v) {
    char st_temp[D_LEN];
#ifdef AMZI_GEN
    char amzi_temp[255];
#endif
    int j,k=0,n,m,count;
    int quicks[4];
#ifndef FIX_SUIT_STRENGTH
    char jack_flag, ten_flag, top_five, top_three, count[2];
#endif
    char level;
    int temp; // ,bidder;
	int gen32_temp_dist[4];
    int temp_middle[4];
    int gen32_total_points;
    int gen32_dist[4];

#ifdef HOFFER_COMPLEX
    if (gen32_evaluation_system == BERGEN)
    // Bergen can vary hcps by a total of 3, 2 for acetens vs quacks, and one for dubious honors
	   gen32_total_points = gen32_bergen(gen32_deck,i*13,13,BERGEN_HCPS,0);
    else if (gen32_evaluation_system ==ROTH)
   	    gen32_total_points = gen32_get_hcps(gen32_deck,i*13,13);
    else if (gen32_evaluation_system == KLINGER)
        gen32_total_points = gen32_klinger(gen32_deck,i*13,13,KLINGER_HCPS,0);
    else if (gen32_evaluation_system == KAPLAN_RUEBEN)
		gen32_total_points = gen32_kaplan_rueben(gen32_deck,i*13,13,KLINGER_HCPS,0);
    else if (gen32_evaluation_system == HOFFER_COUNT)
        gen32_total_points = gen32_get_hcps(gen32_deck,i*13,13);
    else if (gen32_evaluation_system == BBO)
		gen32_total_points = gen32_bbo(gen32_deck,i*13,13,BBO,0);
    else gen32_total_points = 0;
#else
	gen32_total_points = gen32_get_hcps(gen32_deck,i*13,13);
#endif

    // bidder = i; // for amzi; it is called at the start so use i to get to next bidder
	sprintf(st_temp,"P%02u:D",gen32_total_points);
#ifdef AMZI_GEN
    if (pybid_loaded) {
		sprintf(amzi_temp,"temp_hcps(%u,%u)",bidder,gen32_total_points);
        cpp_assert_amzi(amzi_temp);
        }
#endif
	strcpy(description_buffer,st_temp);
	gen32_distro(gen32_deck,i*13,13,gen32_dist);
    num=0;
  	for (j=0; j<4; j++) gen32_temp_dist[j]=0;
    while (num <4) {
        max = -1;
        for (j=3; j>=0; j--)
  			if (gen32_temp_dist[j] == 0 && gen32_dist[j] > max) {
  				max = gen32_dist[j];
                k = j;
                }
		gen32_temp_dist[k]=1;
        max = max < 10 ? max : 9;
        sprintf(st_temp,"%u%c",max, suits[k]);
		strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
        if (pybid_loaded) {
            sprintf(amzi_temp,"temp_len(%u,%u,%u)",bidder,k,max);
            cpp_assert_amzi(amzi_temp);
        }
#endif
        num++;
        }
    sprintf(st_temp,":C");
  		strcat(description_buffer,st_temp);
    for (j=0; j<4; j++) {
    	sprintf(st_temp,"%u",gen32_dist[j] < 10 ? gen32_dist[j] : 9);
		strcat(description_buffer,st_temp);
    }
	temp = gen32_getLTC(gen32_deck,i*13,13);
    sprintf(st_temp, ":L%02u",temp);
		strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
        if (pybid_loaded) {
            sprintf(amzi_temp,"temp_losers(%u,%u)",bidder,temp);
            cpp_assert_amzi(amzi_temp);
        }
#endif  	
        sprintf(st_temp,":H");
		strcat(description_buffer,st_temp);
    num=0;
    while (num <4) {
        max=0;
        for (j=i*13; j < i*13+13; j++)
            if ((k=(gen32_deck[j] % 13)) > 8 && (gen32_deck[j]/13) == num) {
                max += (1 << (k-9));
                }
        sprintf(st_temp,"%02u%c",max, suits[num]);
		strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
        if (pybid_loaded) {
            sprintf(amzi_temp,"honors(%u,suit_%c,%u)",bidder,suits[num],max);
            cpp_assert_amzi(amzi_temp);
        }
#endif
        num++;
        }
#ifdef HOFFER_COMPLEX
    if (gen32_evaluation_system == BERGEN) {
		temp = gen32_bergen(gen32_deck,i*13,13,STARTING,0); 
    sprintf(st_temp,":R%02u",temp);
	strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
    if (pybid_loaded) {
        sprintf(amzi_temp,"temp_starting(%u,%u)",bidder,temp);
        cpp_assert_amzi(amzi_temp);
        }
#endif
	temp = gen32_bergen(gen32_deck,i*13,13,SUPPORTED,0);
    sprintf(st_temp,"%02u",temp);  /* supported */
#ifdef AMZI_GEN
    if (pybid_loaded) {
        sprintf(amzi_temp,"temp_supported(%u,%u)",bidder,temp);
        cpp_assert_amzi(amzi_temp);
        }
#endif
	strcat(description_buffer,st_temp);
    num=0;
    while (num < 4) {
		temp = gen32_bergen(gen32_deck,i*13,13,SUPPORT_OF,num);
        sprintf(st_temp, "%02u%c",temp,suits[num]);
		strcat(description_buffer,st_temp);
#ifdef AMZI_GEN

                                /* roth allowed from 180413 on */
        if (pybid_loaded) {
            sprintf(amzi_temp,"temp_support(%u,suit_%c,%u)",bidder,suits[num],temp);
            cpp_assert_amzi(amzi_temp);
        }
#endif
            num++;
            }
        }
#endif

#ifdef HOFFER_COMPLEX
                                /* roth allowed from 180413 on */
    else if (gen32_evaluation_system == ROTH) {
		temp = gen32_roth(gen32_deck,i*13,13,1,0);
    sprintf(st_temp,":R%02u",temp);
		strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
    if (pybid_loaded) {
        sprintf(amzi_temp,"temp_starting(%u,%u)",bidder,temp);
        cpp_assert_amzi(amzi_temp);
        }
#endif
		temp = gen32_roth(gen32_deck,i*13,13,SUPPORTED,0);
    sprintf(st_temp,"%02u",temp);  /* supported */
		strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
    if (pybid_loaded) {
        sprintf(amzi_temp,"temp_supported(%u,%u)",bidder,temp);
        cpp_assert_amzi(amzi_temp);
        }
#endif
    num=0;
    while (num < 4) {
			temp = gen32_roth(gen32_deck,i*13,13,2,num);
        sprintf(st_temp, "%02u%c",temp,suits[num]);
			strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
        if (pybid_loaded) {
            sprintf(amzi_temp,"temp_support(%u,suit_%c,%u)",bidder,suits[num],temp);
            cpp_assert_amzi(amzi_temp);
        }
#endif
        num++;
        }
    }
    else if (gen32_evaluation_system == KLINGER) {
	temp = gen32_klinger(gen32_deck,i*13,13,STARTING,0);
    sprintf(st_temp,":R%02u",temp);
	strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
    if (pybid_loaded) {
        sprintf(amzi_temp,"temp_starting(%u,%u)",bidder,temp);
        cpp_assert_amzi(amzi_temp);
    }
#endif
	temp = gen32_klinger(gen32_deck,i*13,13,SUPPORTED,0);
    sprintf(st_temp,"%02u",temp);  /* supported */
	strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
    if (pybid_loaded) {
        sprintf(amzi_temp,"temp_supported(%u,%u)",bidder,temp);
        cpp_assert_amzi(amzi_temp);
        }
#endif
    num=0;
    while (num < 4) {
		temp = gen32_klinger(gen32_deck,i*13,13,SUPPORT_OF,num);
        sprintf(st_temp, "%02u%c",temp,suits[num]);
		strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
        if (pybid_loaded) {
        sprintf(amzi_temp,"temp_support(%u,suit_%c,%u)",bidder,suits[num],temp);
        cpp_assert_amzi(amzi_temp);
        }
#endif
        num++;
        }
    }
    else if (gen32_evaluation_system == KAPLAN_RUEBEN) {
		temp = gen32_kaplan_rueben(gen32_deck,i*13,13,STARTING,0);
	sprintf(st_temp,":R%02u",temp);
	strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
	if (pybid_loaded) {
		sprintf(amzi_temp,"temp_starting(%u,%u)",bidder,temp);
		cpp_assert_amzi(amzi_temp);
	}
#endif
	temp = gen32_kaplan_rueben(gen32_deck,i*13,13,SUPPORTED,0);
	sprintf(st_temp,"%02u",temp);  /* supported */
	strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
	if (pybid_loaded) {
		sprintf(amzi_temp,"temp_supported(%u,%u)",bidder,temp);
		cpp_assert_amzi(amzi_temp);
		}
#endif
	num=0;
  	while (num < 4) {
		temp = gen32_kaplan_rueben(gen32_deck,i*13,13,SUPPORT_OF,num);
		sprintf(st_temp, "%02u%c",temp,suits[num]);
		strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
		if (pybid_loaded) {
		sprintf(amzi_temp,"temp_support(%u,suit_%c,%u)",bidder,suits[num],temp);
		cpp_assert_amzi(amzi_temp);
		}
#endif
		num++;
		}
    }
    else
        if (gen32_evaluation_system == HOFFER_COUNT) {
#endif
		temp = gen32_hoffer_count(gen32_deck,i*13,13,STARTING,0);
        sprintf(st_temp,":R%02u",temp);
        strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
    if (pybid_loaded) {
        sprintf(amzi_temp,"temp_starting(%u,%u)",bidder,temp);
        cpp_assert_amzi(amzi_temp);
    }
#endif
		temp = gen32_hoffer_count(gen32_deck,i*13,13,SUPPORTED,0); 
        sprintf(st_temp,"%02u",temp);  /* supported */
        strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
    if (pybid_loaded) {
        sprintf(amzi_temp,"temp_supported(%u,%u)",bidder,temp);
        cpp_assert_amzi(amzi_temp);
        }
#endif
        num=0;
        while (num < 4) {
			temp = gen32_hoffer_count(gen32_deck,i*13,13,SUPPORT_OF,num);
            sprintf(st_temp, "%02u%c",temp,suits[num]);
            strcat(description_buffer,st_temp);
            num++;
        }
    }

    // added 171015
    sprintf(st_temp, ":V%01u",v);
	strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
    if (pybid_loaded) {
        sprintf(amzi_temp,"vul(%u,%u)",bidder,v);
        cpp_assert_amzi(amzi_temp);
        }
#endif

	strcat(description_buffer,":Q");
#define NEW_QSECTION
#ifdef NEW_QSECTION
	max = gen32_getQuicks(gen32_deck, i*13, 13, quicks); // 160510 corrected, was too low for KQ (1 vs 2) 
#else
    max=0;
    for (num=0; num<4; num++) {
        quicks[num]=0;
        for (j=i*13; j<i*13+13; j++)
            if ((k=(gen32_deck[j] % 13)) > 10 && (gen32_deck[j]/13) == num) {
                quicks[num] += k-10;
                max += k-10;
                }
        }
#endif
    sprintf(st_temp,"%02u",max);
		strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
		if (pybid_loaded) {
		sprintf(amzi_temp,"temp_quicks(%u,%u)",bidder,max);
		cpp_assert_amzi(amzi_temp);
		}
#endif
		num=0;
	for (num=0; num<4; num++) {
       	sprintf(st_temp,"%02u%c",quicks[num],suits[num]);
		strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
		if (pybid_loaded) {
		sprintf(amzi_temp,"temp_quicks(%u,suit_%c,%u)",bidder,suits[num],quicks[num]);
		cpp_assert_amzi(amzi_temp);
		}
#endif
	}
	strcat(description_buffer,":S");
	for (num=0; num<4; num++) {
        max=0; count=0;
  		for (j=i*13; j < i*13+13; j++)
  			if ((k=(gen32_deck[j] % 13)) > 8 && (gen32_deck[j]/13) == num) {
                max += (k-8); count++;
  				}
		if (max==3 && count==2) max=2; /* exceptional situation found 200724 */
        /* but that was wrong...it would only be for QJ not Kx fixed 210409 */
		max += gen32_dist[num];
		if (max > 7) { strcat(description_buffer,"2");
#ifdef AMZI_GEN
		if (pybid_loaded) {
		sprintf(amzi_temp,"temp_stopper(%u,suit_%c,2)",bidder,suits[num]); }
#endif
		}
		else if (max > 4) { strcat(description_buffer,"1");
#ifdef AMZI_GEN
		if (pybid_loaded) {
		sprintf(amzi_temp,"temp_stopper(%u,suit_%c,1)",bidder,suits[num]); }
#endif
		}
		else { strcat(description_buffer,"0");
#ifdef AMZI_GEN
		if (pybid_loaded) {
		sprintf(amzi_temp,"temp_stopper(%u,suit_%c,0)",bidder,suits[num]); }
#endif
		}
		sprintf(st_temp,"%c",suits[num]);
        strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
        if (pybid_loaded) {
            cpp_assert_amzi(amzi_temp);
            }
        else TRIGGER(ID_BID_AMZI,gen32_do_log(1,10,"pybid_loaded is false for asserting description for amzi"));
#endif
	}
/* added to show suit strength 3/14/98 */
	strcat(description_buffer,":G");
#ifndef FIX_SUIT_STRENGTH
	for (num=0; num<4; num++) {
		if (gen32_dist[num] < 4) {
			sprintf(st_temp,"0%c",suits[num]);
			strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
		if (pybid_loaded) {
			sprintf(amzi_temp,"temp_strength(%u,suit_%c,0)",bidder,suits[num]);
			cpp_assert_amzi(amzi_temp);
		}
#endif
		}
		else {
			count[1]=0; count[0]=0;
			jack_flag=OFF; ten_flag=OFF; max=0;
			for (j=i*13; j < i*13+13; j++)
				if ((k=(gen32_deck[j] % 13)) > 6 && (gen32_deck[j]/13) == num)
					switch (k) {
						case 8: ten_flag = ON; break;
						case 9: jack_flag = ON; ++count[0]; break;
						case 10: case 11: case 12:
							++count[0]; ++count[1]; break;
						}

            top_three = count[1];
			if (ten_flag == ON && count[0] > 1) max += 1;
			if (jack_flag == ON && count[1] > 0) max += 1;
			top_five = max + count[1];
            level = -1;
			for (j=7; j>=0 && level == -1; j--)
				switch (j) {
					case 7: if (top_three == 3 && gen32_dist[num] > 8) level = 7;
							else if (top_three == 3 && top_five > 3 && gen32_dist[num] > 7) level = 7;
							else if (top_five == 5 && gen32_dist[num] > 6) level = 7;
							break;
					case 6: if (top_three == 3 && gen32_dist[num] > 6) level = 6;
							else if (top_five == 4 && gen32_dist[num] > 6) level = 6;
							break;
					case 5: if (top_three >1 && gen32_dist[num] > 7) level = 5;
							else if (top_five == 3 && gen32_dist[num] > 7) level = 5;
							else if (gen32_dist[num]==7 && top_five > 3) level = 5;
							break;
					case 4: if (gen32_dist[num] > 8) level = 4;
                    		else if (gen32_dist[num] > 7 && top_three > 1) level = 4;
							else if (gen32_dist[num] > 6 && top_three == 3) level = 4;
							else if (gen32_dist[num] > 6 && top_five > 3) level = 4;
							break;
					case 3: if (gen32_dist[num] > 7) level = 3;
							else if (gen32_dist[num] == 7 && top_five > 1) level = 3;
							else if (gen32_dist[num] == 6 && top_five > 3) level = 3;
							else if (gen32_dist[num] == 5 && top_three ==3) level = 3;
							break;
					case 2: if (gen32_dist[num] > 6) level = 2;
							else if (gen32_dist[num] == 6 && top_five > 1) level = 2;
							else if (gen32_dist[num] == 5 && top_three > 1) level = 2;
							else if (gen32_dist[num]==5 && top_five > 3) level = 2;
							break;
					case 1: if (gen32_dist[num] > 5) level = 1;
							else if (gen32_dist[num] == 5 && top_three > 0) level = 1;
							else if (gen32_dist[num] == 5 && top_five > 1) level = 1;
							else if (gen32_dist[num] == 4 && top_three > 1) level = 1;
							else if (gen32_dist[num] == 4 && top_three > 0 && top_five > 2) level = 1;
							break;
                    case 0: level = 0; break;
                    }

			sprintf(st_temp,"%u%c",level,suits[num]);
			strcat(description_buffer,st_temp);
#ifdef AMZI_GEN
			if (pybid_loaded) {
			sprintf(amzi_temp,"temp_strength(%u,suit%c,%u)",bidder,suits[num],level);
			cpp_assert_amzi(amzi_temp);
			}
#endif
		}}
#else
	for (num=0; num<4; num++) {
		gen32_distro(gen32_deck,i*13,13,aDist[i]);
		level=(char)gen32_get_suit_strength(gen32_deck,gen32_dist,i,num);
		sprintf(st_temp,"%u%c",level,suits[num]);
		strcat(description_buffer,st_temp);
		}
#endif
	strcat(description_buffer,":M");
	for (n=0;n<4;n++) temp_middle[n]=0;
	for (n=i*13;n<i*13+13;n++) {
		m = gen32_deck[n] % 13;
		if (m < 11 && m > 6) temp_middle[gen32_deck[n]/13]++; 
		}
	for (n=0;n<4;n++) {
		sprintf(st_temp,"%u%c",temp_middle[n],suits[n]);
		strcat(description_buffer,st_temp);
		}
	return description_buffer;
} 

#include <string>
#define HOOL_TEST
#if defined(HOOL_TEST)
char hool_msg[20000];

const std::string generateCommandString(unsigned int, unsigned int, unsigned int, unsigned int, unsigned int);
void deck_copy_to_deck(void);
char *hool_test(int who, int what, int where, int _low, int _high) {
	//strcpy(hool_msg,constraint_command(who, what, where, _low, _high));
	// 76 is check_play
	// 74 is get_a_play from a hand with round and seat as low high
	// 86 sets the contract
	// 65 and 66 are the checks
	if (what == 70 || what == 71 || what == 64 || what == 72 || what == 76 || what == 74
		|| what == 86 || what == 65 || what == 66 || what == 73) {
		// fprintf(stderr,"hool_test calling generateCommandString:%u",what);
		strcpy(hool_msg, generateCommandString(who, what, where, _low, _high).c_str());
		}
	else {
		strcpy(hool_msg, constraint_command(who, what, where, _low, _high));
	    if (what == 9) hool_test(who,73,where,_low,_high); // check_card in the hool app also
    }
	return hool_msg;
	}
#endif
