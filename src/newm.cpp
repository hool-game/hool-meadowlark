#if defined(ANDROID) || defined(linux)
// changed the temp_g_index and grab_index to signed char in newm.cpp and bids2.cpp
#define SIGNED_CHAR_GRAB_INDEX
#define LINUX_OR_ANDROID
#endif

// #define EXCLUDE_BIDINFO
#define BID20_DEBUGGING
#include "hoffer.h"
#ifdef BID20_DEBUGGING
int bid20_debugging = 0;
#endif
#ifdef NEW_LEAK_DEBUGGING
// #include "dbh.h" 
#include "crtdbg.h"
#endif
#include <stdio.h>
#include "stdlib.h"
#ifndef HOFFER
#include "io.h"
#endif

#include "stdarg.h"
#if !defined(linux)
#ifndef BBO_ALERT
#include "windows.h"
#endif
#endif
#include "string.h"
#include "ctype.h"
// release #include "assert.h"
#define YYBISON
#ifdef TEST
#define VERBOSE
#define RXTRNL
#define MATCH_ALONE
#endif

#define DEPTH_MATCH
#define TOTAL_LENGTH 103
#define GREEDY 0
#define NS_CONVENTION(p) p->convention.ns
#define EW_CONVENTION(p) p->convention.ew


#include "typedef.hpp"
#include "rx.hpp"
#include "header.hpp"
#include "bidstate.hpp"
#include "non_exportables.h"
// #include "cswitch.h"
#include "bid20.h"
// int  rod_lines=0;

#if defined(linux)
#define EXTERN_HOFFER_C
#define NO_CSW
#include "cswitch.h"
#include "assert.h"
#endif

#ifndef ON
#define ON 1
#define OFF 0
#endif

// #define NO_RESET_CLOSURES

#define NO_MATCH 0
#define NO_MATCH_POSITION -1
#define YES_MATCH 1
#define EOS '\0'
#define RX_STACK_DEPTH 5


#if defined(LINUX_OR_ANDROID) && defined(FIX_UNSIGNED_CHAR)
#define EMPTY 127
#else
#define EMPTY -1
#endif

#define CHARCAST const char  *
// #define wsprintf sprintf
#define _stprintf sprintf
#define SAFETY

// #define GINSBERG_V2
#define TESTMEM
#define NEW_GRAB
#define BRIDGE
// #define 

#define SINGLE
#define STATE_INFO      /* additional grabs to get the current info of bidding */
#define PATTERN_LENGTH (D_LEN - 2)

#ifdef TEST
#undef BID_STRUCT
#undef DEPTH_MATCH
#endif


#ifdef BID_STRUCT
VSA vsa[VSA_MAX];
#endif

void reset_closures(RXNODE *);
#ifndef CHAR_ONLY
void push_gi(int);
void reset_gi(void);
int pop_gi(void);
int  gi_index;
int  gi_stack[GRAB_MAX];
#endif
int  begin_flag;
#ifdef POSITIONING
int  beyond_flag;
#endif
char  gtemp[26];



int  do_match(RXNODE *np, const char  *txt);
int  rx_match2(RXNODE *np, const char  *txt, int pos);
int get_hcps(int);
int get_length_by_position(int);
int get_suit_by_position(int);


int winner_count(void);

#undef CHECK_SIGN
// for ANDROID int is unsigned unless forced with a compiler flag which is not done yet.
// changed parameter from int to signed int
void set_grab_index(signed int i) { // changed 210520
// 0 indexed so no grabs is -1; and i must therefore be less than GRAB_MAX
#if defined(CHECK_SIGN)
            /* if you just declare something as int it could be a signed int or
             an unsigned int depending on the compiler
             to test cast -1 and see if it remains negative 210520 */
    signed int test_sign = ((int) -1) < 0;
    assert (test_sign < 0);
    if (i>=GRAB_MAX) // to catch before the assertion fires
        assert(i > -2 && i < GRAB_MAX);
#endif

    assert(i > -2 && i < GRAB_MAX);  // assertion added 210520 notice the signed vs unsigned problems
    grab_index = i;
    }

extern int yydebug;
#include "non_exportables.h"

char  suit[6] = "CDHSN";
char forth_string[254];
int fstack[12];
int fval[100];
char fstr[4][40];
int fstack_index=0;
int fbidder = 0;
void fill_law_table(int, int, int, int, int, int, int);
int check_law_table(void);
void set_lex(const char *str);

// extern "C" {
// int get_vul(void);
// }

int law_table[5][4];
struct off {
    int nv;
    int nvd;
    int v;
    int vd;
    }  offs[] = {
     { 0,0,0,0},
     {-50,  -100,  -100,	 -200},
    {-100,  -300,  -200,	 -500},
    {-150,  -500,  -300,	 -800},
    {-200,  -800,  -400,	-1100},
    {-250, -1100,  -500,	-1400},
    {-300, -1400,  -600,	-1700},
    {-350, -1700,  -700,	-2000},
    {-400, -2000,  -800,	-2300},
    {-450, -2300,  -900,	-2600},
    {-500, -2600, -1000,	-2900},
    {-550, -2900, -1100,	-3200},
    {-600, -3200, -1200,	-3500},
    {-650, -3500, -1300,	-3800}
    };

struct vals {
    int contract;
    int made;
    int nv;
    int nvd;
    int v;
    int vd;
    }  values[] = {
    {10,1,70,140,70,140},
    {11,1,70,140,70,140},
    {10,2,90,240,90,340},
    {11,2,90,240,90,340},
    {10,3,110,340,110,540},
    {11,3,110,340,110,540},
    {10,4,130,440,130,740},
    {11,4,130,440,130,740},
    {10,5,150,540,150,940},
    {11,5,150,540,150,940},
    {10,6,170,640,170,1140},
    {11,6,170,640,170,1140},
    {10,7,190,740,190,1340},
    {11,7,190,740,190,1340},

    {12,1,80,160,80,160},
    {13,1,80,160,80,160},
    {12,2,110,260,110,360},
    {13,2,110,260,110,360},
    {12,3,140,360,140,560},
    {13,3,140,360,140,560},
    {12,4,170,460,170,760},
    {13,4,170,460,170,760},
    {12,5,200,560,200,960},
    {13,5,200,560,200,960},
    {12,6,230,660,230,1160},
    {13,6,230,660,230,1160},
    {12,7,260,760,260,1360},
    {13,7,260,760,260,1360},

    {14,1,90,180,90,180},
    {14,2,120,280,120,380},
    {14,3,150,380,150,580},
    {14,4,180,480,180,780},
    {14,5,210,580,210,980},
    {14,6,240,680,210,1180},
    {14,7,270,780,210,1380},

    {20,2,90,180,90,180},
    {21,2,90,180,90,180},
    {20,3,110,280,110,380},
    {21,3,110,280,110,380},
    {20,4,130,380,130,580},
    {21,4,130,380,130,580},
    {20,5,150,480,150,780},
    {21,5,150,480,150,780},
    {20,6,170,580,170,980},
    {21,6,170,580,170,980},
    {20,7,190,680,190,1180},
    {21,7,190,680,190,1180},

    {22,2,110,470,110,670},
    {23,2,110,470,110,670},
    {22,3,140,570,140,870},
    {23,3,140,570,140,870},
    {22,4,170,670,170,1070},
    {23,4,170,670,170,1070},
    {22,5,200,770,200,1270},
    {23,5,200,770,200,1270},
    {22,6,230,870,230,1470},
    {23,6,230,870,230,1470},
    {22,7,260,970,260,1670},
    {23,7,260,970,260,1670},

    {24,2,120,490,120,690},
    {24,3,150,590,150,890},
    {24,4,180,690,180,1090},
    {24,5,210,790,210,1290},
    {24,6,240,890,210,1490},
    {24,7,270,990,210,1690},

    {30,3,110,470,110,670},
    {31,3,110,470,110,670},
    {30,4,130,570,130,870},
    {31,4,130,570,130,870},
    {30,5,150,670,150,1070},
    {31,5,150,670,150,1070},
    {30,6,170,770,170,1270},
    {31,6,170,770,170,1270},
    {30,7,190,870,190,1470},
    {31,7,190,870,190,1470},

    {32,3,140,530,140,730},
    {33,3,140,530,140,730},
    {32,4,170,630,170,930},
    {33,4,170,630,170,930},
    {32,5,200,730,200,1130},
    {33,5,200,730,200,1130},
    {32,6,230,830,230,1330},
    {33,6,230,830,230,1330},
    {32,7,260,930,260,1530},
    {33,7,260,930,260,1530},

    {34,3,400,550,600,750},
    {34,4,430,650,630,950},
    {34,5,460,750,660,1150},
    {34,6,490,850,690,1350},
    {34,7,520,950,720,1550},

    {40,4,130,510,130,710},
    {41,4,130,510,130,710},
    {40,5,150,610,150,910},
    {41,5,150,610,150,910},
    {40,6,170,710,170,1110},
    {41,6,170,710,170,1110},
    {40,7,190,810,190,1310},
    {41,7,190,810,190,1310},

    {42,4,420,590,620,790},
    {43,4,420,590,620,790},
    {42,5,450,690,650,990},
    {43,5,450,690,650,990},
    {42,6,480,790,680,1190},
    {43,6,480,790,680,1190},
    {42,7,510,890,710,1390},
    {43,7,510,890,710,1390},

    {44,4,430,610,630,810},
    {44,5,460,710,660,1010},
    {44,6,490,810,690,1210},
    {44,7,520,910,720,1410},

    {50,5,400,550,600,750},
    {51,5,400,550,600,750},
    {50,6,420,650,620,950},
    {51,6,420,650,620,950},
    {50,7,440,750,640,1150},
    {51,7,440,750,640,1150},

    {52,5,450,650,650,850},
    {53,5,450,650,650,850},
    {52,6,480,750,680,1050},
    {53,6,480,750,680,1050},
    {52,7,510,850,710,1250},
    {53,7,510,850,710,1250},

    {54,5,460,670,660,870},
    {54,6,490,770,690,1070},
    {54,7,520,870,720,1270},

    {60,6,920,1090,1370,1540},
    {61,6,920,1090,1370,1540},
    {60,7,940,1190,1390,1740},
    {61,7,940,1190,1390,1740},

    {62,6,980,1210,1430,1660},
    {63,6,980,1210,1430,1660},
    {62,7,1010,1310,1460,1860},
    {63,7,1010,1310,1460,1860},

    {64,6,990,1230,1440,1680},
    {64,7,1020,1330,1470,1880},

    {70,7,1440,1630,2140,2330},
    {71,7,1440,1630,2140,2330},

    {72,7,1510,1770,2210,2470},
    {73,7,1510,1770,2210,2470},

    {74,7,1520,1790,2220,2490}
    };

#ifndef MATCH_ALONE
#if !defined(EXCLUDE_BIDINFO)
extern BINFO bid_info[BID_INFO_MAX][4];
#endif
#endif

RXNODE  *rx_stack[RX_STACK_DEPTH];
int  rx_stack_index;
unsigned int needed = 0;
unsigned int need;
extern int patlen;

#ifdef STATE_INFO
#ifndef TEST
// extern TBidState *BiddingSystem;
#endif
char get_node_c(RXNODE *);
#endif


/* Closures are the only matching elements that are at all difficult to
   implement. To use closures there must be a stack or recursion
   to do backtracking. For a discussion of this see Software Tools
   pattern matching section by Kernighan and Plauger.

    I have chosen to implement this using a stack. The stack contains
    pointers to a RXNODE that is a closure.

    When a closure is first encountered the match is made as long
    as possible and the length stored in np->c; on subsequent calls
    to the closure there are two possibilities. 1) this closure
    is on  the stack and therefore decrement the np->c and do
    another match, or 2) this closure is not on the stack and
    use the np->c as before. Once the np->c is decremented to EMPTY (-1)
    then the closure is popped off the stack.

    A match with closures cannot be determined to have failed until
    all backtracking has been completed, which is indicated by
    the rx_stack_index reverting to zero, ie. empty stack. With
    failure and an empty stack then it can truly be returned as
    a failed match.                                           */

    /*
    1 - check for match at current position; set closures
    2 - increment p only upon no further backtracking of closures; this
        is indicated by rx_stack_index dropping back to EMPTY
                                                                */

/*
    String matching algorithms:
    8/10/92:
        I changed the RX_STRING matching algorithm through two
        phases

        1) brute force algorithm -> modified Boyer-Moore
           where the string match is
           advanced one character at a time and checked against
           the input line, this was changed to finding how much
           to skip by scanning the input string pattern and
           matching the mismatched character to the pattern character.
           If the mismatched character is not in the string then
           the whole length of the string can be skipped, otherwise
           a certain portion could be skipped.

        2) modified Boyer-Moore -> Boyer-Moore.
            this requires an array to lookup the amount of
            skip rather than computing it on each miss.
            The array is allocated by the lexical analyzer
            onto the end of the string. Its size is 256
            so that any char can be checked for the amount
            to be skipped. No processing just lookup.

        see pages 286-289 Algorithms by Robert Sedgewick 2nd ed.

        note: I also tryed the Rabin-Karp algorithm but this was
        extremely slow, almost as slow as brute force. It could be
        the proper algorithm with a math co-processor!
*/

static int  match_node(RXNODE *np, const char  *txt);
void _get_parse_str(RXNODE *np, char  *txt);
static int  get_parse_str(RXNODE *np, char  *txt, int i, int depth, int tree_flag);
#ifdef NEW_ALSO
signed int  rx_match_also(RXNODE *np, char  *txt,int start);
#endif

/* far_match_node is the main matching between a pattern and the
text and is called from node2.cpp.

It just keeps calling match_node until a positive result has been
returned or the rx_stack_index turns empty. The rx_stack_index is
part of the closure processing.
*/

#if !defined(EXCLUDE_BIDINFO)
extern int get_current_round_and_seat(int depth, int *, int *seat);
#endif

int iround, seat;

#ifdef DEPTH_MATCH                                          
int bidding_depth=0;
int far_match_node_depth(int depth, const char  *txt) {
    int result;
    bidding_depth = depth;
#if !defined(EXCLUDE_BIDINFO)
    get_current_round_and_seat(depth,&iround,&seat);
#endif
    result = far_match_node(vsa[depth].pos->pattern,txt);
    TRIGGER(1603,do_log(1,30,"far_match_node_depth returns:%d\n",result));
    return result;
    }
#endif
int far_match_node(RXNODE *np, const char  *txt)
{
  int done=0,ans=0;
#ifndef NO_RESET_CLOSURES
  reset_closures(np);
#endif

#ifndef CHAR_ONLY
  reset_gi();
#endif
  rx_stack_index=EMPTY;
  rx_match_pos = rx_match_len = NO_MATCH_POSITION;
  if (np == NULL || txt == NULL) return NO_MATCH;
  begin_flag = ON;
#ifdef POSITIONING
  beyond_flag = OFF;
#endif
  while (!done) {
      TRIGGER(1603,do_log(1,30,"calling match_node for (%s)\n",txt));
    ans=match_node(np,txt);
      TRIGGER(1603,do_log(1,30,"ans for match_node is:%d\n",ans));
    if (ans>=0) goto fmn_finish;
#ifdef POSITIONING
    else if (beyond_flag == ON) done = 1;
#endif
    else if (rx_stack_index == EMPTY) done=1;
    }
  ans = -1;
fmn_finish:
    // delete_str_from_rxnode(np);
    return ans;

}

/* rx_match is not unlike far_match_node
but it walks down the pattern until it finds
a match. It is only called by RXNT_ALSO and
is called from match_node.

  rx_match_depth was used to gain access to the current
  bidder which is one of the parts of the vsa struct. The
  vsa is an array from the pattern matching in bids2.cpp
  It was used to replace the recursive calls. Given the depth
  you can find the vsa constraint pattern and the current_bidder

*/
#ifdef DEPTH_MATCH
const char *description_for_forth;
int depth_for_forth;
int  rx_match_depth(int depth, const char  *txt) {
    // BIDNODE *nptr=vsa[depth].pos;
  description_for_forth = (char *) txt;
  bidding_depth = depth;
  depth_for_forth = depth;
#ifdef CSW
  ONE_SHOT(221,BidMessage("OS: rx_match_depth encountered with %s",txt));
  CSW_EXEC(do_log(1,20,"rx_match_depth:%u %s",depth,txt));
#endif
#if !defined(EXCLUDE_BIDINFO)
  get_current_round_and_seat(depth,&iround,&seat);
#endif

  assert(vsa[depth].pos->con_pat == NULL);
    if (vsa[depth].pos->con_pat1 == NULL) {
	  printf("bad con_pat1");
    
#ifdef EXTRA_DEBUGGING_YYPARSE
    rx_root = NULL;
    if (!nptr->constraint1.empty()) {
        strcpy(macro_temp,nptr->constraint1.c_str());
    //    CSW_EXEC(do_log(1,10,"1 <%s> <%s>",nptr->constraint1.c_str(),macro_temp));
        CSW_EXEC(do_log(1,10,"1 <%s>",nptr->constraint1.c_str()));
        reset_lex(macro_temp);
        if (yyparse() == PARSE_OK) {
            reset_closures(rx_root);
            // grab_count = count_grabs(rx_root,nptr->num_of_grabs & 127);
            // assert (grab_count >= 0 && grab_count < 25);
            nptr->con_pat1 = rx_root;  rx_root = NULL;
            }
        else {
            fprintf(stderr,"Cannot parse constraint1 %s\n",nptr->constraint1.c_str());
            }
        }

#endif
    }
    
    
    
  assert(vsa[depth].pos->con_pat1 != NULL);

#ifdef LOOKING_UPWARDS
  if (depth > 2 && (vsa[depth].current_bidder == vsa[depth-3].current_bidder)) {
            if (vsa[depth-3].current_bidder % 2 == 1) {
                if (vsa[depth-3].seq == "4N")
            }
#endif
  
  if (vsa[depth].current_bidder % 2 == 1 && vsa[depth].pos->con_pat2 != NULL) {
        return (rx_match(vsa[depth].pos->con_pat2,txt));
        }
  else if (vsa[depth].pos->con_pat1 != NULL)
        return (rx_match(vsa[depth].pos->con_pat1,txt));
//  else if (vsa[depth].pos->con_pat != NULL)
//		return (rx_match(vsa[depth].pos->con_pat,txt));
  else return -1;  // can't be... should not be using con_pat now    
  }

#endif


int rx_match_id(int id, int bidder, const char  *txt, const char  *grab_str, const char  *forth_str) {
    int i,ans;
    BIDNODE *p;

    description_for_forth = txt;
    p = getNode(id);
    if (p==NULL) return -1;
    for (i=0; i<26 && grab_str[i] != '\0'; i++) grab[i].c = grab_str[i];
    for (i=0; i<8; i++) fval[i] = forth_str[i]-32;
    
    if (bidder % 2 == 1 && p->con_pat2 != NULL)
      ans = rx_match(p->con_pat2,txt);
    else if (p->con_pat1 != NULL)
      ans = rx_match(p->con_pat1,txt);
    else ans = -1;  // can't be... should not be using con_pat now    
    return ans;
}

void setup_description(const char *str) {
    yydebug = 0;
    description_for_forth = str;
    }

int  rx_match2(RXNODE *np, const char  *txt, int pos)
{
  int   j, current_pos, diff;
  char *p;
  reset_closures(np);
#ifndef CHAR_ONLY
  reset_gi();
#endif
  rx_stack_index=EMPTY;
  current_pos = PATTERN_LENGTH - (int) strlen((char  *)txt);
  if (current_pos > pos) return NO_MATCH;
  diff = pos - current_pos;
  p= (char *) (txt + diff);

  rx_match_pos = rx_match_len = NO_MATCH_POSITION;
  if (np == NULL || txt == NULL) return NO_MATCH;
  begin_flag = OFF;
  do {
        j = match_node(np,(CHARCAST)p);
        if (j >= 0)
            { rx_match_pos = diff; rx_match_len = j; return YES_MATCH; }
        } while (rx_stack_index != EMPTY);
  return NO_MATCH;
}

int  rx_match(RXNODE *np, const char  *txt)
{
  int   i, j, k;
  char *p;

#ifdef CSW
  ONE_SHOT(222,BidMessage("OS: rx_match and reset_closures encountered"));
#endif
  reset_closures(np);
#ifndef CHAR_ONLY
  reset_gi();
#endif
  rx_stack_index=EMPTY;
  if (np == NULL || txt == NULL) return NO_MATCH;
  p= (char *) txt;
  k = (int) strlen(txt);
  rx_match_pos = rx_match_len = NO_MATCH_POSITION;
  begin_flag = ON;
#ifdef POSITIONING
  beyond_flag = OFF;
#endif

  needed = 0;
  for (i=0; *p != '\0'; ) {
        if (i>0) begin_flag = OFF;
#ifdef POSITIONING
        if (beyond_flag == ON) break;
#endif
        j = match_node(np,(CHARCAST)p);
        if (j >= 0)	{
             rx_match_pos = i;
             rx_match_len = j;
#ifdef CSW
            CSW_EXEC(do_log(1,20,"match: at:%u len:%u for:%s",i,j,p));
             ONE_SHOT(223,BidMessage("OS: positive match from match_node returned to rx_match"));
#endif
              return YES_MATCH;
            }
        else if (rx_stack_index == EMPTY) {
            if (needed < (unsigned int)k)
                { 
                p -= j; i -= j; k += j;
                }
            else break;
            }
        }


  return NO_MATCH;
}

#ifdef NEW_ALSO
signed int  rx_match_also(RXNODE *np, char  *txt, int start) {
#if defined(ANDROID)
  signed int   j;
#else
  int   j;
#endif
  char *p;

  reset_closures(np);
#ifndef CHAR_ONLY
  reset_gi();
#endif
  rx_stack_index=EMPTY;
  if (np == NULL || txt == NULL) return NO_MATCH;
  p= (char *) &txt[start];
  rx_match_pos = rx_match_len = NO_MATCH_POSITION;
  begin_flag = OFF;

  j = match_node(np,(CHARCAST)p);
    TRIGGER(1600,do_log(1,30,"called match_node for rx_match_also:%s j is:%d",p,j));
  if (j >= 0) {
    rx_match_pos = start;
    rx_match_len = j;
    return YES_MATCH;
    }
  else return NO_MATCH;
  }
#endif


int  do_match(RXNODE *np, char  *txt)
{
  int   j;
  char *p;

  reset_closures(np);
#ifndef CHAR_ONLY
  reset_gi();
#endif
  rx_stack_index=EMPTY;
  p= (char *) txt;

  rx_match_pos = rx_match_len = NO_MATCH_POSITION;
  if (np == NULL || txt == NULL) return NO_MATCH;
  while (1) {
        j = match_node(np,(CHARCAST)p);
        if (j >= 0)
            { rx_match_pos = 0; rx_match_len = j; return YES_MATCH; }
        else if (rx_stack_index == EMPTY) return 0;
        }

}

#if defined(ANDROID) || defined(linux)
static signed int match_node(RXNODE *np, const char  *txt)
#else
static int match_node(RXNODE *np, const char  *txt)
#endif
{
#if defined(ANDROID) || defined(linux)
  signed int i=0, j;
#else
  char i=0, j;
#endif
#if defined(SIGNED_CHAR_GRAB_INDEX)
    signed char temp_g_index;
#else
    char temp_g_index;
#endif
//  char temp_type;
#ifndef CHAR_ONLY
  char temp_gi;
#endif
#ifndef SINGLE
    char *p1,*p2;
    long num;
#endif

  const char *p3;
  char *temp;
  struct grabs grab2[GRAB_MAX];
#if !defined(linux)
    unsigned char fence[4] = { 255,1,255,1};
#endif

#ifdef CSW
    TRIGGER(1612,do_log(1,30,"match_node:%s type:%d\n",txt,np->type));
  ONE_SHOT(224,BidMessage("OS: match_node encountered %s",txt));
#endif
  temp_g_index = grab_index;
  p3=txt;

  switch (np->type)  {

    case RXNT_RX:
#ifdef CSW
           ONE_SHOT(225,BidMessage("OS: type %u",np->type));
#endif
#ifdef CSW
                    TRIGGER(1612,do_log(1,30,"RXNT_RX\n"));
#endif

              // Must match lnode and start and/or end.

                // 7 Aug 96 The RX_END has another feature.
                //      ${num}+ is the lex expression so that no only
                //      can the $ be used as an end of string but a
                //      number of characters can be specified.
                //      This is done to avoid .*6N type of expressions
                //      where everything up to 6N is matched. That type
                //      of an expression causes lots of overhead while
                //      specifying the number of characters eliminates
                //      the extra processing completely.
                //  So, now the expression would be 6N$2 and matches
                //      6N as the last 2 characters in a matching string.
                //  The length is stored in np->c, else set to zero.

        if (begin_flag) needed = TOTAL_LENGTH;
        if (np->flag & RX_BEGIN && begin_flag != ON) return -1;

#ifdef GINSBERG_V2
        if ((np->flag & RX_END) && np->c > 0 && np->c < 90) {
            i = strlen((char  *)p3);
            j = 0;
            while (i > 0 && j < np->c) {
                if (*(p3 + --i) == ':') j++;
                }
            if (j < np->c) return -1;
            else ++i;
            }
        else i=0;
#else
        if ((np->flag & RX_END) && np->c > 0) {
            j = (int) strlen((char  *)p3);
            if (j < np->c) return -1;
            else i = j - np->c;
            }
        else i=0;
#endif


                // 7 Aug 96 note that today this was changed from
                // (p3) to (p3&i) ...we are jumping to the end of
                // the string for this match if a parameter to the
                // RX_END was given

        j = match_node (np->lnode, (CHARCAST)(p3+i));
          
          TRIGGER(1612,do_log(1,30,"RXNT_RX: i:%d h=j:%d\n",i,j));

          
        if (j<0)  { set_grab_index(temp_g_index); return -1;}
        TRIGGER(1612,do_log(1,30,"RXNT_RX fence %d %d %d %d",fence[0],fence[1],fence[2],fence[3]));

        return i+j;

    case RXNT_STR:
#ifdef CSW
          TRIGGER(1603,do_log(1,30,"RXNT_STR\n"));
           ONE_SHOT(226,BidMessage("OS: type %u",np->type));
#endif

        /* see if the reg expression portion of this node matches */
        /* this node has np->str set to the string after the @ sign
           and is usually the highest level node with the @command
           set up to restart the search at the higest level by
           prepending the command and the rest of the search string
           and then doing a search. This is actually implemented in
           the matching algorithm rather than here. */

        if (np->lnode == RXNODE_0) return(0);

        i = match_node (np->lnode, (CHARCAST)p3);
        if (i<0) {
            set_grab_index(temp_g_index);
            return i;
            }
        else return(i);

    case RXNT_SECTION:
#ifdef CSW
          TRIGGER(1603,do_log(1,30,"RXNT_SECTION\n"));
#endif
        if (*(p3) == ':' && *(p3+1) == np->c) return 2;
    //	if (np->c == *(p3+i)) return 1; // P 
    //	j=strlen((char  *)p3);
    //	else if (np->c == *(p3+i+1)) return 2;
        else switch (np->c) {
            case 'D': need = TOTAL_LENGTH-3; break;
            case 'C': need = TOTAL_LENGTH-13; break;
            case 'L': need = TOTAL_LENGTH-19; break;
            case 'H': need = TOTAL_LENGTH-23; break;
            case 'R': need = TOTAL_LENGTH-37; break;
            case 'V': need = TOTAL_LENGTH-55; break;
            case 'Q': need = TOTAL_LENGTH-58; break;
            case 'S': need = TOTAL_LENGTH-74; break; 
            case 'G': need = TOTAL_LENGTH-84; break;
            case 'M': need = TOTAL_LENGTH-94; break; 
            default: break;
            }
        if (need > needed) needed = need;
        return -1;

    //	for (i=0; i<j; i++) {
    //		if (*(p3+i) == ':' && *(p3+i+1) == np->c)
    //			return i+2;
    //	}
    //	return -1;
        

#define OLD_STRING_MATCH
    case RXNT_STRING:   /* Must match the string     */
#ifdef CSW
           ONE_SHOT(227,BidMessage("OS: type %u",np->type));
#endif
#ifdef CSW
                    TRIGGER(1603,do_log(1,30,"RXNT_STRING(OLD)\n"));
#endif

        {
#ifndef OLD_STRING_MATCH
 int m;
#else
            int inotu = np->flag; // here might be an int vs unsigned int problem
            TRIGGER(1603,do_log(1,30,"inotu (np->flag) is:%d\n",inotu));
#endif
        temp = (char *)np->str;
        i=np->flag;
        j = (int) strlen((char  *)p3);

        if (j==0) return -1;
        if (j<inotu) {
                TRIGGER(1603,do_log(1,30,"j<inotu:returning -j:%d",-j));
                return -j;
                }
#ifndef OLD_STRING_MATCH
        m=np->flag;
#endif
            while (--inotu >=0) {
                TRIGGER(1603,do_log(1,30,"inotu is %d\n",inotu));
            if (*(temp + inotu) != *(p3 + inotu)) {
                TRIGGER(1603,do_log(1,30,"nomatch(%c) (%c)\n",*(temp+inotu),*(p3+inotu)));
#ifdef OLD_STRING_MATCH
                TRIGGER(1603,do_log(1,30,"returning -1\n"));
                return -1;
            }}
        TRIGGER(1603,do_log(1,30,"after loop, return np->flag value\n"));
        return np->flag;
#else
                k=*(p3+inotu);
                j=np->flag;
#define PREPROCESS
#ifdef PREPROCESS
                j=(int) np->str[np->flag+1+ *(p3+inotu)];
#else
                for (j=np->flag; j > 0; j--)
                    if (k == *(temp+j-1)) break;
#endif
            if (j==0) {
                TRIGGER(1603,do_log(1,30,"returning -m:%d",-m));
                return -m;
            }
#ifndef PREPROCESS
                else j = m-j;
#endif
                if (m-inotu-1>j) return -m;
                else return -j;
                }
#ifdef PREPROCESS
          TRIGGER(1603,do_log(1,30,"after match returning m:%d\n",m));
        return m;
#else
          TRIGGER(1603,do_log(1,30,"after match calling lnode to <%s>\n",(p3+m));
        j = match_node(np->lnode, (CHARCAST)(p3+m));
        if (j<0)  {
            set_grab_index(temp_g_index);
            return -1;
            }
        else return m+j;
#endif
#endif
          }
    case RXNT_AND:			/* Match both nodes at this position */
#ifdef CSW
           ONE_SHOT(228,BidMessage("OS: type %u",np->type));
#endif
#ifdef CSW
              TRIGGER(1603,do_log(1,30,"RXNT_AND\n"));
#endif

        i = match_node (np->lnode, (CHARCAST)p3);
        if (i<0) {set_grab_index(temp_g_index); return i; }
        set_grab_index(temp_g_index);
        j = match_node (np->rnode, (CHARCAST)p3);
        if (j<0) {set_grab_index(temp_g_index); return -1; }
        return i;

    case RXNT_ALSO:			/* Match both nodes, second from any spot */
                    /* what if first is longer than second????? */
                  // set_trigger("1603");

#ifdef CSW
           ONE_SHOT(229,BidMessage("OS: type %u",np->type));
#endif
#ifdef CSW
              TRIGGER(1603,do_log(1,30,"RXNT_ALSO\n"));
                  TRIGGER(1612,do_log(1,30,"RXNT_ALSO\n"));
#endif

#ifdef NEW_ALSO
        { int skipped=0, len;
#endif
        i = match_node (np->lnode, (CHARCAST)p3);
              TRIGGER(1612,do_log(1,30,"called match_node(left):%s i:%d",p3,i));
        if (i<0) {set_grab_index(temp_g_index); return i; }
        
        /* This is very important. 171005 
        The set_grab_index was inappropriately put back even after a match
        So that, a new grab could not be used in a constraint using ALSO
        such as
        :D[^:]*{[3-9]H&&@arith(9,-,#a) to subtract heart length from 9
        fixed
        */
        
        //set_grab_index(temp_g_index);
#ifdef NEW_ALSO
        if (np->c == 0) 
#endif
#ifdef NEW_ALSO
            j = rx_match_also(np->rnode, (CHARCAST)p3,0);
#else
            if (np->c > 0)
                j = rx_match2(np->rnode, (CHARCAST)p3, np->c);
            else
                j = rx_match(np->rnode, (CHARCAST)p3);
#endif
#ifdef NEW_ALSO
        else {
            len = strlen((CHARCAST)p3);
            skipped = len - (PATTERN_LENGTH - np->c);
            if (skipped >= 0)
                j = rx_match_also(np->rnode, (CHARCAST) p3, skipped);
            else j = NO_MATCH;
            }
#endif

        if (j==NO_MATCH) {set_grab_index(temp_g_index); return -1; }
        else {
            TRIGGER(1612,do_log(1,30,"also returns:%d",rx_match_len));
            return rx_match_len;
            }
#ifdef NEW_ALSO
        }
#endif
    case RXNT_ALT:
      // Match  either subnode. Return the longer
#ifdef CSW
    TRIGGER(1613,do_log(1,30,"OS: type %u (RXNT_ALT)",np->type));
    // TRIGGER(1605,do_log(1,30,"OS: type %u (RXNT_ALT)",np->type));
    // TRIGGER(1605,do_log(1,30,"OS: type %u (RXNT_C)",RXNT_C));
    // TRIGGER(1605,do_log(1,30,"OS: type %u (RXNT_TERM_LIST)",RXNT_TERM_LIST));
    // TRIGGER(1605,do_log(1,30,"OS: type %u (RXNT_RX)",RXNT_RX));
#endif

        { int m,n;
#if defined(CHECK_SIGN)
        /* if you just declare something as int it could be a signed int or
         an unsigned int depending on the compiler
         to test cast -1 and see if it remains negative 210520 */
        signed char test_sign_char = ((char) -1) < 0;
        signed int test_sign_int = ((int) -1) < 0;
              
#endif
#if !defined(linux)
        RXNODE *temp;
        temp = np;
#endif
        assert(np != NULL);
//		if (grab_index > 24)
//			grab_index = 24;
        TRIGGER(1613,do_log(1,30,"grab index is bad:%d",grab_index));
#if defined(CHECK_SIGN)
        TRIGGER(1613,do_log(1,30,"grab_index as an int is signed if this is -1:%d",test_sign_int));
        TRIGGER(1613,do_log(1,30,"grab_index as a char is signed if this is -1:%d",test_sign_char));
#endif
		assert(grab_index < 25);
        i = match_node (np->lnode, (CHARCAST)p3);
//		if (grab_index > 24)
//			grab_index = 25;
		assert(grab_index < 25);
        if (np == NULL)
            return -1;
        for (m=0; m<=grab_index; m++) {
            if (np == NULL)
                return -1;
            grab2[m].c = grab[m].c;
            }
        n = grab_index;
        set_grab_index(temp_g_index);
//		if (np == NULL)
//			return -1;
        assert(np != NULL);
        j = match_node (np->rnode,(CHARCAST) p3);
      TRIGGER(1613,do_log(1,30,"RXNT_ALT i:%d j:%d",i,j));
              
        if (i < 0 && j < 0) {
            assert(temp_g_index < 26);
            set_grab_index(temp_g_index);
            return -1;
            }
        else if (i>=j) {
            assert(n<25);
            for (m=0; m<=n; m++) {
                 grab[m].c = grab2[m].c;
                 }
            set_grab_index(n);
            TRIGGER(1613,do_log(1,30,"RXNT_ALT n:%d fence %d %d %d %d",n,fence[0],fence[1],fence[2],fence[3]));
            TRIGGER(1613,do_log(1,30,"RXNT_ALT returning i:%d",i));
            return i;
            }
        else {
            TRIGGER(1613,do_log(1,30,"RXNT_ALT n:%d fence %d %d %d %d",n,fence[0],fence[1],fence[2],fence[3]));
            TRIGGER(1613,do_log(1,30,"RXNT_ALT returning j:%d",j));
            return j;
            }
        }

    case RXNT_TERM_LIST:     /* Must match both subnodes. */
#ifdef CSW
               ONE_SHOT(231,BidMessage("OS: type %u",np->type));
#endif
#ifdef CSW
              TRIGGER(1612,do_log(1,30,"RXNT_TERM_LIST\n"));
#endif

        i = match_node (np->lnode,(CHARCAST) p3);
        if (i<0) {TRIGGER(1612,do_log(1,30,"RXNT_TERM_LIST:i:%d",i));}
        if (i<0)  { set_grab_index(temp_g_index); return i; }
#if defined(ANDROID)
                  TRIGGER(1612,do_log(1,30,"RXNT_TERM_LIST calling 2nd part:type:%d",np->rnode->type));
                  TRIGGER(1612,do_log(1,30,"RXNT_TERM_LIST calling 2nd part:str:%s",(CHARCAST)(p3+i)));
                  TRIGGER(1612,do_log(1,30,"RXNT_TERM_LIST calling 2nd part: fence %d %d %d %d",fence[0],fence[1],fence[2],fence[3]));
                  j = match_node (np->rnode,(CHARCAST)(p3+i));
#else
                  j = match_node (np->rnode,(CHARCAST)((long)p3+i));
#endif
        // j = match_node (np->rnode,(CHARCAST)(p3+i));
        TRIGGER(1612,do_log(1,30,"RXNT_TERM_LIST back from 2nd part fence %d %d %d %d",fence[0],fence[1],fence[2],fence[3]));

        if (j<0) {TRIGGER(1612,do_log(1,30,"RXNT_TERM_LIST:j:%d",j));}
        if (j<0)  { set_grab_index(temp_g_index); return -1; }
        TRIGGER(1612,do_log(1,30,"RXNT_TERM_LIST:i:j:%d:%d",i,j));

        return i+j;

/* RXNT_STAR RXNT_PLUS RXNT_BIN 
  For plus, match 1 and fall into start.
  For star, match as  as you can and then backtrack. This is done
  by keeping track of the matches while proceeding down the string.
  In March of 2003 additional pattern matching was implemented.
  1) RXNT_BIN was added
  2) RXNT_STAR can now have ** or *** or **** patterns which will match
      0 or 1,2,3 patterns for each respective case
  3) the same functionality for number 2 above is done for RXNT_PLUS
*/

    case RXNT_BIN:             /* must match 0 or 1 occurances of left node */
#ifdef CSW
           ONE_SHOT(232,BidMessage("OS: type %u",np->type));
#endif

        i = match_node(np->lnode,(CHARCAST) p3);
#ifdef CSW
        CSW_EXEC(printf("\nBIN: np->c is %u",np->c));
#endif
        set_grab_index(temp_g_index);
        if (i<0) return 0;
        else return i;
        
    case RXNT_PLUS:     /* Must match 1 or more occurance(s) of left-node. */
#ifdef CSW
           ONE_SHOT(233,BidMessage("OS: type %u",np->type));
#endif

        i = match_node (np->lnode,(CHARCAST) p3);
#ifdef CSW
        CSW_EXEC(printf("\nPLUS: np->c is %u",np->c));
#endif
        if (i<0)  { set_grab_index(temp_g_index); return -1; }
        // fall through

              /* ------ Fall into RXNT_STAR from 1 match of RXNT_PLUS ----- */
    case RXNT_STAR:     /* Match 0 or more occurance(s) of left-node. */
#ifdef CSW
           ONE_SHOT(234,BidMessage("OS: type %u",np->type));
#endif
#ifdef CSW
              TRIGGER(1603,do_log(1,30,"RXNT_STAR\n"));
#endif

        { int index, n,k,m;
#ifndef CHAR_ONLY
        temp_gi = gi_index;
#endif
        n = (np->type == RXNT_PLUS ? i : 0);   /* Account for fall-in. */
#ifdef CSW
        CSW_EXEC(printf("\nSTAR: np->c is %u",np->c));
#endif
        if (np->c == EMPTY) {
            if (rx_stack_index >= RX_STACK_DEPTH) {
                my_error("rx stack depth error");
                return -1;
                }
            rx_stack[++rx_stack_index]=np;
#ifndef OLD_REVERSE
            m = (int) strlen((CHARCAST)p3);
            if (np->str) free(np->str);
            
#ifndef HOFFER
            np->str = (char *) _malloc_dbg( m/8+1, _NORMAL_BLOCK, __FILE__, __LINE__ );
#else
            np->str = (char *) malloc( m/8+1);
#endif

            if (np->str == NULL) {
                my_error("Unable to malloc in closure");
                return -1;
                }
            for (j=0; j < m/8 + 1; j++) np->str[j]=0;
#endif
#ifdef CSW
            CSW_EXEC(printf("\nEmpty stack %u entered",rx_stack_index));
#endif
// Here is the functionality for restricting the length of the match
// by using the np->flag which was set by lex to length of the number of stars-1
            index = np->flag;
            while (1)  {
                if (index > 0) i = match_node (np->lnode,(CHARCAST)(p3+n));
                if (i<0 || n+i>m || index <= 0)  { 
                    set_grab_index(temp_g_index);
#ifndef CHAR_ONLY
                    gi_index = temp_gi;
#endif
                    np->c=n;
                    // if (np->str) free(np->str);
                    return n;
                    }
                else {
#ifndef OLD_REVERSE
                    j = n/8;
                    k = n%8;
                    switch (k) {
                        case 0: np->str[j] |= 1;  break;
                        case 1:	np->str[j] |= 2;  break;
                        case 2:	np->str[j] |= 4;  break;
                        case 3:	np->str[j] |= 8;  break;
                        case 4:	np->str[j] |= 16; break;
                        case 5:	np->str[j] |= 32; break;
                        case 6:	np->str[j] |= 64; break;
                        case 7:	np->str[j] |= 128;	break;
                        }
#endif
                    temp_g_index = grab_index;
#ifndef CHAR_ONLY
                    temp_gi = gi_index;
#endif
                    }
                n += i;
                index--;  // restricts the total number of matches
                }
             }
        else if (rx_stack_index != EMPTY && np == rx_stack[rx_stack_index]) {
#ifdef CSW
            CSW_EXEC(printf("\ndecrementing in star to %u",np->c - 1));
#endif
#ifdef OLD_REVERSE
            if (--np->c >= 0) {
                // if (np->str) free(np->str);
                return(np->c);
                }
#else
            while (--np->c >= 0) {
                j = np->c / 8;
                k = np->c % 8;
                switch (k) {
                        case 0: n= 1;  break;
                        case 1:	n= 2;  break;
                        case 2:	n= 4;  break;
                        case 3:	n= 8;  break;
                        case 4:	n= 16; break;
                        case 5:	n= 32; break;
                        case 6:	n= 64; break;
                        case 7:	n= 128;	break;
                        }
                if (np->str[j] & n) {
                    // if (np->str) free(np->str);
                    return(np->c);
                    }
                }
#endif
#ifdef CSW
            CSW_EXEC(printf("\ndecrementing completed"));
#endif
            --rx_stack_index;
            // if (np->str) free(np->str);
            return -1;
            }
        else {
#ifdef CSW
            CSW_EXEC(printf("\nstar repeat return of %u",np->c));
#endif
            // if (np->str) free(np->str);
            return np->c;
            }

            }
    case RXNT_RANGE:         /* Must be in range, inclusive. */
#ifdef CSW
           ONE_SHOT(235,BidMessage("OS: type %u",np->type));
            TRIGGER(1603,do_log(1,30,"RXNT_RANGE\n"));
#endif

        return  (*p3 < np->lnode->c  ||  *p3 > np->rnode->c) ? -1 : 1;

    case RXNT_CLASS:         /* Must match left-node. */
#ifdef CSW
           ONE_SHOT(236,BidMessage("OS: type %u",np->type));
                  // set_trigger("1603");
                  TRIGGER(1603,do_log(1,30,"RXNT_CLASS\n"));
#endif

        i = match_node (np->lnode,(CHARCAST) p3);
                  TRIGGER(1603,do_log(1,30,"rxnt_class i is:%d",i));
        if (i<0) set_grab_index(temp_g_index);
        return  i;


    case RXNT_IN:  /* match, but not to string,  so returns 0 or -1 */
#ifdef CSW
           ONE_SHOT(237,BidMessage("OS: type %u",np->type));
#endif
#ifdef CSW
              TRIGGER(1603,do_log(1,30,"RXNT_IN"));
#endif

                 { char sm[2];
                 sm[0] = get_node_c(np->lnode); sm[1] = '\0';
                 i= match_node(np->rnode,(CHARCAST) sm);
                 if (i>0) return 0; else return -1;
                 }

    case RXNT_CLASS_LIST:    /* Must match either subnode. */
#ifdef CSW
           ONE_SHOT(238,BidMessage("OS: type %u",np->type));
                  TRIGGER(1603,do_log(1,30,"RXNT_CLASS_LIST\n"));
#endif

        i = match_node (np->lnode, (CHARCAST)p3);
        if (i>0)  return i;
        set_grab_index(temp_g_index);
        j = match_node (np->rnode,(CHARCAST) p3);
        if (j>0)  return j;
        set_grab_index(temp_g_index);
        return -1;

    case RXNT_NOT_CLASS:     /* Must not match left-node. */
    case RXNT_NOT:
#ifdef CSW
           ONE_SHOT(239,BidMessage("OS: type %u",np->type));
#endif

        i = match_node (np->lnode, (CHARCAST)p3);
        if (i > 0) set_grab_index(temp_g_index);
        return  i>0 ? -1 : 1;


    case RXNT_KLEENE:
        { int i,j, position, skip_amount ;

/*			case 'D': need = TOTAL_LENGTH-3; break;
            case 'C': need = TOTAL_LENGTH-17; break;
            case 'L': need = TOTAL_LENGTH-19; break;
            case 'H': need = TOTAL_LENGTH-23; break;
            case 'R': need = TOTAL_LENGTH-37; break;
            case 'V': need = TOTAL_LENGTH-53; break;
            case 'Q': need = TOTAL_LENGTH-56; break;
            case 'S': need = TOTAL_LENGTH-72; break; 
            case 'G': need = TOTAL_LENGTH-82; break;
            case 'M': need = TOTAL_LENGTH-92; break; 
H should not have two leading periods and start is 25 so add two to above which
is the start of the :H
        */

        skip_amount = 3;
        position = TOTAL_LENGTH - (int)strlen((CHARCAST)p3);
        if (position == 5) skip_amount = 2;
        else if (position == 74) skip_amount = 2;

        if (np->flag == 4) {
          for (i=0; i<3; i++) {
            if (match_node(np->lnode->lnode,(CHARCAST)(p3+i*skip_amount)) < 0) continue;
            if (match_node(np->lnode->rnode,(CHARCAST)(p3+i*skip_amount + skip_amount-1)) < 0) continue;
            else for (j=i+1; j<4; j++) {
                if (match_node(np->rnode->lnode,(CHARCAST)(p3+j*skip_amount)) < 0) continue;
                if (match_node(np->rnode->rnode,(CHARCAST)(p3+j*skip_amount +skip_amount -1)) < 0) continue;
                return (j+1)*skip_amount;
                }
            }
        return -1;
        }

        if (np->flag ==2) {
          for (i=0; i<4; i++) {
            if (match_node(np->lnode,(CHARCAST)(p3+i*skip_amount)) < 0) continue;
            if (match_node(np->rnode,(CHARCAST)(p3+i*skip_amount + skip_amount-1)) < 0) continue;
            return (i+1)*skip_amount;
            }
        return -1;
        }
        return -1;
        }

    case RXNT_C:        /* Must match the character. */
        ONE_SHOT(240,BidMessage("OS: type %u",np->type));
        TRIGGER(1612,do_log(1,30,"OS: type %u (RXNT_C):<%c>",np->type,np->c));


#ifdef POSITIONING
        if (np->leftside > 0) {
            TRIGGER(1612,do_log(1,30,"RXNT_C np->leftside > 0"));
            if (PATTERN_LENGTH - strlen((CHARCAST)p3) > np->leftside)
                beyond_flag = ON;
            }
#endif
        if (np->c == '\'') return 0;
#if defined(ANDROID)
        TRIGGER(1612,do_log(1,30,"comparing <%c> <%c>",*p3,np->c));
        if (*p3 == np->c) {TRIGGER(1612,do_log(1,30,"OK:")); TRIGGER(1612,do_log(1,30,"matched")); }
        else { TRIGGER(1612,do_log(1,30,"not matched")); }
        TRIGGER(1612,do_log(1,30,"OK:"));

        TRIGGER(1612,do_log(1,30,"RXNT_C returning; fence %d %d %d %d",fence[0],fence[1],fence[2],fence[3]));

        return  *p3 == np->c ? 1 : -1;
#else
        return  *p3 == np->c ? 1 : -1;
#endif

#ifdef POSITIONING
    case RXNT_POSITION:
#ifdef CSW
           ONE_SHOT(241,BidMessage("OS: type %u",np->type));
#endif

        return -1;
#endif

/* RXNT_COMMA change 2003-3-21 to allow matches to lengths and not
just bids. So *p <= '9' which was < '8' to match level of bids was
increased so it can match any length up to 9 */

    case RXNT_COMMA:
#ifdef CSW
           ONE_SHOT(242,BidMessage("OS: type %u",np->type));
#endif

        if (*p3 == 'P') return 1;
        else if (*p3 == 'X') return 1;
        else if (*p3 == 'R') return 1;
        else if (*p3 > '0' && *p3 <= '9') {
            if (*(p3+1) == 'C') return 2;
            else if (*(p3+1) == 'D') return 2;
            else if (*(p3+1) == 'H') return 2;
            else if (*(p3+1) == 'S') return 2;
            else if (*(p3+1) == 'N') return 2;
            else return -1;
            }
        else return -1;

    case RXNT_ANY_C:     /* Matches anything except end-of-string. */
#ifdef CSW
           ONE_SHOT(243,BidMessage("OS: type %u",np->type));
#endif

        return  *p3 ? 1 : -1;

#ifdef THE_LAW
    case RXNT_LAW:
#ifdef CSW
           ONE_SHOT(244,BidMessage("OS: type %u",np->type));
#endif

        { int x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,dir,bidder;
            x1 = grab[8].c - 'a';
            x2 = (grab[9].c - '0')*10;
            switch (grab[10].c) {
                case 'C': x3 = 0; break;
                case 'D': x3 = 1; break;
                case 'H': x3 = 2; break;
                case 'S': x3 = 3; break;
                case 'N': x3 = 4; break;
                }
            x4 = (grab[11].c - '0')*10;
            switch (grab[12].c) {
                case 'C': x5 = 0; break;
                case 'D': x5 = 1; break;
                case 'H': x5 = 2; break;
                case 'S': x5 = 3; break;
                case 'N': x5 = 4; break;
                }
    bidder= (BiddingSystem->GetDealer() + BiddingSystem->GetBidder()) % 4;
            dir = bidder % 2;
            switch (get_vul()) {  /* none=0, ns=1, ew=2, both = 3 */
                case 0 : x7 = 0; x8 = 0; break;
                case 1 : x7 = 1; x8 = 0; break;
                case 2 : x7 = 0; x8 = 1; break;
                case 3 : x7 = 1; x8 = 1; break;
                }
            x9 = grab[13].c- '0';  x10 = grab[14].c - '0';
          if (dir == 0) fill_law_table(x1,x2+x3,x4+x5,x7,x8,x9,x10);
          else fill_law_table(x1,x2+x3,x4+x5,x8,x7,x9,x10);
            x6 = check_law_table();
            if (x6 > 3) return 0;
            else return -1;
            }

    case RXNT_NOT_LAW:
#ifdef CSW
           ONE_SHOT(245,BidMessage("OS: type %u",np->type));
#endif

        { int x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,dir,bidder;
            x1 = grab[0].c - 'a';
            x2 = (grab[1].c - '0')*10;
            switch (grab[2].c) {
                case 'C': x3 = 0; break;
                case 'D': x3 = 1; break;
                case 'H': x3 = 2; break;
                case 'S': x3 = 3; break;
                case 'N': x3 = 4; break;
                }
            x4 = (grab[3].c - '0')*10;
            switch (grab[4].c) {
                case 'C': x5 = 0; break;
                case 'D': x5 = 1; break;
                case 'H': x5 = 2; break;
                case 'S': x5 = 3; break;
                case 'N': x5 = 4; break;
                }
//	bidder= (BiddingSystem->GetDealer() + BiddingSystem->GetBidder()) % 4;
            dir = bidder % 2;
//			switch (get_vul()) {  /* none=0, ns=1, ew=2, both = 3 */
//				case 0 : x7 = 0; x8 = 0; break;
//				case 1 : x7 = 1; x8 = 0; break;
//				case 2 : x7 = 0; x8 = 1; break;
//				case 3 : x7 = 1; x8 = 1; break;
//                }
            x9 = grab[5].c- '0';  x10 = grab[6].c - '0';
          if (dir == 0) fill_law_table(x1,x2+x3,x4+x5,x7,x8,x9,x10);
          else fill_law_table(x1,x2+x3,x4+x5,x8,x7,x9,x10);
            x6 = check_law_table();
            if (x6 < 2) return 0;
            else return -1;
            }

#endif

    case RXNT_EVAL:
#ifdef CSW
               ONE_SHOT(246,BidMessage("OS: type %u",np->type));
#endif

        i = match_node (np->lnode,(CHARCAST) p3);
        if (i<0)  { set_grab_index(temp_g_index); return i; }
        else return 0;


    case RXNT_NE:
#ifdef CSW
           ONE_SHOT(247,BidMessage("OS: type %u",np->type));
#endif

    { 

        if ((i = get_node_c(np->lnode)) == -1) return -1;
        if ((j = get_node_c(np->rnode)) == -1) return -1;

#if !defined(MATCH_ALONE) && !defined(EXCLUDE_BIDINFO)
 #ifndef MATCH_ALONE
        int k;
#endif
       switch (j) {
            case 'A': for (k=0; k<4; k++) if (bid_info[iround][seat].bid[k] == 2) switch(k) {
                            case 0: if (i == 'C') return 0; break;
                            case 1: if (i == 'D') return 0; break;
                            case 2: if (i == 'H') return 0; break;
                            case 3: if (i == 'S') return 0; break;
                            }
                        break;
            case 'B':  for (k=0; k<4; k++) if (bid_info[iround][seat].bid[k] == 1) switch(k) {
                            case 0: if (i == 'C') return 0; break;
                            case 1: if (i == 'D') return 0; break;
                            case 2: if (i == 'H') return 0; break;
                            case 3: if (i == 'S') return 0; break;
                            }
                        break;
            case 'F':	if (bid_info[iround][seat].forcing_pass != i) return 0;
                        else break;
            case 'G':	
                if (bid_info[iround][seat].game_force != i) return 0;
                else break;
            case 'V':   if (bid_info[iround][seat].vul != i) return 0;
                        else break;
            default: if (i != j) return 0;
            }
#else
        if (i != j) return 0;
#endif
        return -1;
            }
    case RXNT_GE:
    case RXNT_LE:
    case RXNT_GT:
	case RXNT_LT:
    case RXNT_EQ:
#ifdef CSW
           ONE_SHOT(248,BidMessage("OS: type %u",np->type));
#endif
        { 

            if ((i = get_node_c(np->lnode)) == -1) return -1;
            if ((j = get_node_c(np->rnode)) == -1) return -1;

#if !defined(MATCH_ALONE) && !defined(EXCLUDE_BIDINFO)
#ifndef MATCH_ALONE
            int k;
#endif
        switch (j) {
            case 'A': for (k=0; k<4; k++) if (bid_info[iround][seat].bid[k] == 2) switch(k) {
                            case 0: if (i == 'C') return 0; break;
                            case 1: if (i == 'D') return 0; break;
                            case 2: if (i == 'H') return 0; break;
                            case 3: if (i == 'S') return 0; break;
                            }
                        break;
            case 'B':  for (k=0; k<4; k++) if (bid_info[iround][seat].bid[k] == 1) switch(k) {
                            case 0: if (i == 'C') return 0; break;
                            case 1: if (i == 'D') return 0; break;
                            case 2: if (i == 'H') return 0; break;
                            case 3: if (i == 'S') return 0; break;
                            }
                        break;
			case 'F':{ int r;
					for (r=0; r<=iround;r++) if (bid_info[r][seat].forcing_pass == i) return 0;
					break;
					 }

				//if (bid_info[iround][seat].forcing_pass == i){
				// fprintf(stderr,"in bid_info forcing_pass matches\n");
							//return 0;
						//}
                        //else break;
			case 'G':
				// check for game force, which can occur in any round, so find out if GF exists, then check
				// stored as a char '0' is off '1' is on.
				{ int j; char f='0';
				j=1;
					for (j=0; j<=iround; j++) if (bid_info[j][seat].game_force == '1') f='1';
					
				
                if (f == i)
					return 0;
                else break;
		}
            case 'V':   if (bid_info[iround][seat].vul == i) return 0;
                        else break;
            default: if (np->type == RXNT_EQ && i==j) return 0;
                else if (np->type == RXNT_GE && i >= j) return 0;
                     else if (np->type == RXNT_LE && i <= j) return 0;
                     else if (np->type == RXNT_GT && i > j) return 0;
                     else if (np->type == RXNT_LT && i < j) return 0;
                     else break;
            }
#else
        if (i==j) return 0;
#endif
        return -1;

                }


    case RXNT_EXTRA_SET:
#ifdef CSW
           ONE_SHOT(249,BidMessage("OS: type %u",np->type));
#endif
        { char ch1,ch2;
        if (np->lnode->type != RXNT_POP) return -1;
        if (np->rnode->type == RXNT_ADD) {
            ch1 = get_node_c(np->rnode->lnode);
            if (ch1 >= '0' && ch1 <= '9') ch1 = ch1 - '0';
            else if (ch1 >= 'a' && ch1 <= 'z') ch1 = ch1 - 'a';
            else return -1;
            ch2 = get_node_c(np->rnode->rnode);
            if (ch2 >= '0' && ch2 <= '9') ch2 = ch2 - '0';
            else if (ch2 >= 'a' && ch2 <= 'z') ch2 = ch2 - 'a';
            else return -1;

            grab[(int) np->lnode->c].c = ch1 + ch2 + 'a';
            return 0;}
        else if (np->rnode->type == RXNT_SUB) {
            ch1 = get_node_c(np->rnode->lnode);
            if (ch1 >= '0' && ch1 <= '9') ch1 = ch1 - '0';
            else if (ch1 >= 'a' && ch1 <= 'z') ch1 = ch1 - 'a';
            else return -1;
            ch2 = get_node_c(np->rnode->rnode);
            if (ch2 >= '0' && ch2 <= '9') ch2 = ch2 - '0';
            else if (ch2 >= 'a' && ch2 <= 'z') ch2 = ch2 - 'a';
            else return -1;

            grab[(int) np->lnode->c].c = ch1 - ch2 + 'a';
            return 0; }
        return -1;
        }

    case RXNT_INC:
#ifdef CSW
           ONE_SHOT(250,BidMessage("OS: type %u",np->type));
#endif

        {
#ifndef MATCH_ALONE
         int k=0;
#endif
        if (np->lnode->type == RXNT_C) {
            /* B=BID A=AGREED L=LOW M=MAX */
            if (np->rnode->type == RXNT_C || np->rnode->type == RXNT_POP)
                 i = (int) get_node_c(np->rnode);
            else if (np->rnode->type == RXNT_STRING)
                i = atoi(np->rnode->str);
#if !defined(MATCH_ALONE) && !defined(EXCLUDE_BIDINFO)
            if (toupper(np->lnode->c) == 'A') k = 2;
                    else if (toupper(np->lnode->c) == 'B') k = 1;
            switch (toupper(np->lnode->c)) {
                case 'B': 
                case 'A': 
                      switch (i) {
                            case 'C': bid_info[iround][seat].bid[0]=k; break;
                            case 'D': bid_info[iround][seat].bid[1]=k; break;
                            case 'H': bid_info[iround][seat].bid[2]=k; break;
                            case 'S': bid_info[iround][seat].bid[3]=k; break;
                            }
                                break;
                case 'L': bid_info[iround][seat].low = i; break;
                case 'M': bid_info[iround][seat].high = i; break;
                case 'F': bid_info[iround][seat].forcing_pass = i; 
					bid_info[iround][(seat+2)%4].forcing_pass = i;
					break;
                case 'G': bid_info[iround][seat].game_force = i;
					bid_info[iround][(seat+2)%4].game_force=i;
					break;
                case 'V': bid_info[iround][seat].vul = i; break;
                }
#endif
            return 0;
                }
        else {
            if (np->lnode->type != RXNT_POP) return -1;
            if (np->rnode->type == RXNT_C || np->rnode->type == RXNT_POP)
                grab[(int) np->lnode->c].c = get_node_c(np->rnode);
            else return -1;
            return 0;
            }
                }

    case RXNT_SET:
#ifdef CSW
           ONE_SHOT(250,BidMessage("OS: type %u",np->type));
#endif

        {
#ifndef MATCH_ALONE
         int k=0;
#endif
        if (np->lnode->type == RXNT_C) {
            /* B=BID A=AGREED L=LOW M=MAX G=game forcing */
            if (np->rnode->type == RXNT_C || np->rnode->type == RXNT_POP)
                 i = (int) get_node_c(np->rnode);
            else if (np->rnode->type == RXNT_STRING)
                i = atoi(np->rnode->str);
#if !defined(MATCH_ALONE) && !defined(EXCLUDE_BIDINFO)
            if (toupper(np->lnode->c) == 'A') k = 2;
                    else if (toupper(np->lnode->c) == 'B') k = 1;
            switch (toupper(np->lnode->c)) {
                case 'B': 
                case 'A': 
                      switch (i) {
                            case 'C': bid_info[iround][seat].bid[0]=k; break;
                            case 'D': bid_info[iround][seat].bid[1]=k; break;
                            case 'H': bid_info[iround][seat].bid[2]=k; break;
                            case 'S': bid_info[iround][seat].bid[3]=k; break;
                            }
                                break;
                case 'L': bid_info[iround][seat].low = i; break;
                case 'M': bid_info[iround][seat].high = i; break;
                case 'F': bid_info[iround][seat].forcing_pass = i;
					bid_info[iround][(seat+2)%4].forcing_pass = i;
					break;
                case 'G': 
                    bid_info[iround][seat].game_force = i; 
					bid_info[iround][(seat+2)%4].game_force=i;
					break;
                case 'V': bid_info[iround][seat].vul = i; break;
                }
#endif
            return 0;
                }
        else {
            if (np->lnode->type != RXNT_POP) return -1;
            if (np->rnode->type == RXNT_C || np->rnode->type == RXNT_POP)
                grab[(int) np->lnode->c].c = get_node_c(np->rnode);
            else return -1;
            return 0;
            }
                }


    case RXNT_FORTH:
#ifdef CSW
               ONE_SHOT(251,BidMessage("OS: type %u",np->type));
#endif

	   sprintf(forth_string,"%2u 67G",bidding_depth);
        f_eval(forth_string); // set current depth into forth variable for introspecition of prior bids
       sprintf(forth_string,"%2u 69G",vsa[bidding_depth].current_bidder);
        f_eval(forth_string); // set current bidder into forth variable
#ifdef CSW
        CSW_EXEC(BidMessage(forth_string));
        CSW_EXEC(do_log(1,20,"rxnt_forth:%s",np->str));
#endif

        np->flag = f_eval(np->str); // evaluate forth string to calculate value
        i=99;  // it is not an error to drop through to RXNT_MATH however
                // once I added the np->str for ~#n< functionality
                // it failed. So I had to add this to allow skipping the np->str processing in RXNT_MATH
                // fall through

    case RXNT_MATH:
#define MATHPART
#ifdef MATHPART
    /* This used to be something like ~4> or ~8&, just using a 1 or 2 digit number in this lexical construct
        but I have now added the ability to use a grab, so ~#[a-z][<>|&] like ~#n<, very useful
        for something like doing arithmetic in the constraint
        For example, this sequence 3H:P:4N uses the number of losers in the hand and the
        number of trumps to calculate the slam possibilities:
            :D[^:]*{{[3-9]#b, where #b is the trump and there have been 12 grabs (#l is last)
            <#n=@arith(9,-,#m)>, where the two new grabs are used to get the new number of losers
            then later :L~#n<, so a4.kt98.akqj.ak4 has 4 losers, 9-4=5 so 0-4 losers will match, voila!
    */
#ifdef CSW
        ONE_SHOT(252,BidMessage("OS: type %u",np->type));
#endif
        // the single character has stored in np->str from the ~#n< construct it stores 'n'
        if (np->str != NULL && i != 99) {
            int my_value,j;
            my_value = np->str[0]-'a';

            if (grab_index == -1 || my_value > grab_index || my_value < 0) {
                my_error("Grab_index is incorrect in RXNT_MATH");
                return -1;
                }
                
            j=grab[my_value].c; 
            
            if ((j-'0') < 0 || (j-'0') > 9) {
                my_error("numeric value is <0 or >9 in RXNT_MATH");
                return -1;
                }
            i=(*p3-'0') * 10 + (*(p3+1)-'0');
            np->flag = j-'0';
            }
        else 
#endif			

            if (isdigit(*p3) && isdigit(*(p3+1)))
                i = (*p3-'0') * 10 + (*(p3+1)-'0');
            else { if (i==99 )i= 0; // just in case clear the 99 from RXNT_FORTH
            return -1;}

        j= -1;
        switch (np->c) {
            case '>': if (i > (int) np->flag) j=2; break;
            case '<': if (i < (int) np->flag) j=2; break;
            case '&': if ((i & (int) np->flag) == np->flag) j=2; break;
            case '|': if (i | (int) np->flag) j=2; break;
            case '!': if (!(i & (int) np->flag)) j=2; break;
            case '=': if (i == (int) np->flag) j=2; break;
            case '+': if (i >= (int) np->flag) j=2; break;
            case '-': if (i <= (int) np->flag) j=2; break;
            }
        return j;

    case RXNT_PUSH:
#ifdef CSW
           ONE_SHOT(253,BidMessage("OS: type %u",np->type));
#endif

#ifndef NEW_GRAB

#ifdef SAFETY
        if (grab_index < -1 || grab_index > GRAB_MAX) {
            my_error("Grab_index is incorrect in RXNT_PUSH");
            return 0;
            }
#endif
#endif
#ifdef NEW_GRAB
        if (np->c -1 < 0 || np->c -1 > 24)
            i = 0;
//		assert(np->c -1 >= 0 && np->c -1 < 25);
        grab[np->c-1].c = *p3;
        set_grab_index(np->c - 1);
#else
        grab[++grab_index].start = (CHARCAST) p3;
#ifndef CHAR_ONLY
        push_gi(grab_index);
#endif
        grab[grab_index].end = NULL;
        grab[grab_index+1].start=NULL;
        grab[grab_index+1].end=NULL;
#endif
        return 0;

    case RXNT_POP:
#ifdef CSW
           ONE_SHOT(254,BidMessage("OS: type %u",np->type));
#endif

            j=0;
            if (np->c > 24)
                j=0;
#ifndef NEW_GRAB
#ifdef SAFETY
            if (grab_index == -1 || np->c > grab_index || np->c < 0) {
                my_error("Grab_index is incorrect in RXNT_POP");
                return -1;
                }
#endif
#endif
// Switched to SINGLE 11/8/93 because of the problems
// with carrying information from bidding to play


#ifdef SINGLE
            if (*p3 == get_node_c(np)) return 1;
            else return -1;
#else
            p1= (char *) grab[np->c].start;
            p2= (char *) grab[np->c].end;
            if (p1==NULL) {
                if (*p3 == grab[np->c].c) return 1;
                else return -1;
                }
            else if (p2==NULL) { num =1; p2=p1; }
            else {
#if defined(ANDROID)
              num = p2 + 1;
              num = num - p1;
#else
              num = (long) p2 + 1;
              num = num - (long) p1;
#endif
                }
            if (p1 > p2) return -1;
            if (num > 0) {
                while (num--)
                    if (p3[j] != p1[j++]) return -1;
                return j;
                }
            else {
                while (p1 <= p2)
                    if (p3[j++] != *p2--) return -1;
                return j;
                }
#endif


    case RXNT_RBRACE:
#ifdef CSW
           ONE_SHOT(255,BidMessage("OS: type %u",np->type));
#endif

#ifndef CHAR_ONLY
#ifndef NEW_GRAB
#ifdef SAFETY
        if (grab_index <0 || grab_index > GRAB_MAX) {
            my_error("Grab_index is incorrect in RXNT_RBRACE");
            return 0;
            }
#endif
#endif
#ifdef NEW_GRAB
#if defined(ANDROID)
        grab[np->c -1 ].end = (CHARCAST) (p3-1);
#else
        grab[np->c -1 ].end = (CHARCAST) ((long)p3-1);
#endif
#else
#if defined(ANDROID)
        grab[pop_gi()].end=(CHARCAST)(p3-1);
#else
        grab[pop_gi()].end=(CHARCAST)((long)p3-1);
#endif
#endif
#endif
        return 0;


    case RXNT_GREATER:
#ifdef CSW
           ONE_SHOT(256,BidMessage("OS: type %u",np->type));
#endif

        switch (np->lnode->type) {
            case RXNT_C:
            case RXNT_POP:
                i= get_node_c(np->lnode); break;
            default: return -1;
            }
#ifdef BRIDGE
        if (i == 'N') return -1;
        else if (*p3 == 'N') return 1;
        else
#endif
         return *p3 > i ? 1 : -1;


    case RXNT_LESSER:
#ifdef CSW
           ONE_SHOT(257,BidMessage("OS: type %u",np->type));
#endif

        switch (np->lnode->type) {
            case RXNT_C:
            case RXNT_POP:
             i= get_node_c(np->lnode); break;
            default: return -1;
            }
#ifdef BRIDGE
        if (*p3 == 'N') return -1;
        else if (i == 'N') return 1;
        else
#endif
         return *p3 < i ? 1 : -1;


    case RXNT_INCR:
#ifdef CSW
           ONE_SHOT(258,BidMessage("OS: type %u",np->type));
#endif

        { int k;

        if (np->lnode->type == RXNT_POP) {
            if (np->flag < 0 || np->flag > 4) {
                my_error("np flag error");
                return -1;
                }
            else if (isdigit(*p3)) {
#ifndef NEW_GRAB
                if (np->lnode->c <0 || np->lnode->c > grab_index) {
                    my_error("RXNT_INCR index error");
                    return -1;
                    }
#endif
#ifndef BUG780
                { if (*p3 == get_node_c(np->lnode) + np->flag)
                     return 1;
                else return -1; }
#else
                if (*p3 == get_node_c(np->lnode) + np->flag)
                    return 1;
                else return -1;
#endif
                }
            else {
                switch (get_node_c(np->lnode)) {
                    case 'C': k =0; break;
                    case 'D': k =1; break;
                    case 'H': k =2; break;
                    case 'S': k =3; break;
                    case 'N': k =4; break;
                    default:  k = -1; break;
                    }
                if (k >= 0 && *p3 == suit[(k+np->flag)%5])
                    return 1;
//				else if (*p3 == grab[np->lnode->c].c + np->flag)
//					return 1;
                else return -1;
                }
            }
        else return -1;
                }


    case RXNT_NI:
#ifdef CSW
           ONE_SHOT(258,BidMessage("OS: type %u",np->type));
#endif

        { int k;

        if (np->lnode->type == RXNT_POP) {
            if (np->flag < 0 || np->flag > 4) {
                my_error("np flag error");
                return -1;
                }
            else if (isdigit(*p3)) {
#ifndef NEW_GRAB
                if (np->lnode->c <0 || np->lnode->c > grab_index) {
                    my_error("RXNT_INCR index error");
                    return -1;
                    }
#endif
#ifndef BUG780
                { if (*p3 == get_node_c(np->lnode) + np->flag)
                     return 1;
                else return -1; }
#else
                if (*p3 == get_node_c(np->lnode) + np->flag)
                    return 1;
                else return -1;
#endif
                }
            else {
                switch (get_node_c(np->lnode)) {
                    case 'C': k =0; break;
                    case 'D': k =1; break;
                    case 'H': k =2; break;
                    case 'S': k =3; break;
                    case 'N': k =4; break;
                    default:  k = -1; break;
                    }
                if (k >= 0 && *p3 == suit[(k+np->flag)%5])
                    return -1;
//				else if (*p3 == grab[np->lnode->c].c + np->flag)
//					return 1;
                else return 1;
                }
            }
        else return -1;
                }




    case RXNT_DECR:
#ifdef CSW
           ONE_SHOT(259,BidMessage("OS: type %u",np->type));
#endif

        { int k;

        if (np->lnode->type == RXNT_POP) {
            if (np->flag < 0 || np->flag > 4) {
                my_error("np flag error");
                return -1;
                }
            else if (isdigit(*p3)) {
#ifndef NEW_GRAB
                if (np->lnode->c <0 || np->lnode->c > grab_index) {
                    my_error("RXNT_INCR index error");
                    return -1;
                    }
#endif
                if (*p3 == get_node_c(np->lnode) - np->flag)
                    return 1;
                else return -1;
                }
            else {
                switch (grab[(int) np->lnode->c].c) {
                    case 'C': k =5; break;
                    case 'D': k =6; break;
                    case 'H': k =7; break;
                    case 'S': k =8; break;
                    case 'N': k =9; break;
                    default:  k = -1; break;
                    }
                if (k >= 0 && *p3 == suit[(k-np->flag)%5])
                    return 1;
//				else if (*p3 == grab[np->lnode->c].c - np->flag)
//					return 1;
                else return -1;
                }
            }
        else return -1;
                }


    case RXNT_ND:
#ifdef CSW
           ONE_SHOT(259,BidMessage("OS: type %u",np->type));
#endif

        { int k;

        if (np->lnode->type == RXNT_POP) {
            if (np->flag < 0 || np->flag > 4) {
                my_error("np flag error");
                return -1;
                }
            else if (isdigit(*p3)) {
#ifndef NEW_GRAB
                if (np->lnode->c <0 || np->lnode->c > grab_index) {
                    my_error("RXNT_INCR index error");
                    return -1;
                    }
#endif
                if (*p3 == get_node_c(np->lnode) - np->flag)
                    return 1;
                else return -1;
                }
            else {
                switch (grab[(int) np->lnode->c].c) {
                    case 'C': k =5; break;
                    case 'D': k =6; break;
                    case 'H': k =7; break;
                    case 'S': k =8; break;
                    case 'N': k =9; break;
                    default:  k = -1; break;
                    }
                if (k >= 0 && *p3 == suit[(k-np->flag)%5])
                    return -1;
//				else if (*p3 == grab[np->lnode->c].c - np->flag)
//					return 1;
                else return 1;
                }
            }
        else return -1;
                }





    default:    /* "I don't think we're in Kansas anymore, Toto." */
        BidMessage("Kansas? Toto. #2: %u",np->type);
     return(-1);
  }
}

char parse_str[255];
char  *get_parse_tree(const char *pattern) {
    set_lex(pattern);
    get_parse_str(rx_root,parse_str,0,0,1);
    return parse_str;
}

char  *get_parse_string(const char *pattern) {
    set_lex(pattern);
    get_parse_str(rx_root,parse_str,0,0,0);
    return parse_str;
}

static int  get_parse_str(RXNODE *np, char  *txt, int i,int depth, int tree_flag)
{
    int index,ch;
#ifndef PRODUCTION_VERSION
#ifndef SINGLE
    char *p1,*p2;
    long num;
#endif


  if (np == NULL) return i;

  if (tree_flag) {
        _stprintf(&txt[i],"\n%2u",np->type);
        i += 3;
        for (index=0; index<depth; index++)
            _stprintf(&txt[i++]," ");
        if (np->type ==RXNT_TERM_LIST) _stprintf(&txt[i++],"T");
        else if (np->type == RXNT_ALSO) _stprintf(&txt[i++],"A");
        else if (np->type == RXNT_STAR) _stprintf(&txt[i++],"S");
        else if (np->type == RXNT_CLASS_LIST) _stprintf(&txt[i++],"C");
        }
  
  index = 0;
/*
  if (np->type == RXNT_TERM_LIST && 1==2) {
        if (np->lnode->type == RXNT_TERM_LIST && np->rnode->type != RXNT_STAR) {
            if (np->lnode->lnode->type == RXNT_TERM_LIST) {
                if (np->lnode->lnode->lnode->type == RXNT_SECTION &&
                    np->lnode->lnode->rnode->type == RXNT_STAR) {
                        _stprintf(&txt[i++],":");
                        _stprintf(&txt[i++],"%c",np->lnode->lnode->lnode->c);
                        if ((ch=np->lnode->lnode->lnode->c) == 'R' || ch == 'Q') {
                            _stprintf(&txt[i],"..");
                            i+=2;
                            }
                        _stprintf(&txt[i++],"<");
                        i=get_parse_str(np->lnode->rnode,txt,i,depth,tree_flag);
                        _stprintf(&txt[i++],",");
                        i=get_parse_str(np->rnode,txt,i,depth,tree_flag);
                        _stprintf(&txt[i++],">");
                    //	return i;
                        index = 1;
                        }
                }
            if (index == 1 && np->lnode->lnode->rnode->type == RXNT_STAR) {
                }
            }
        }
*/
    
  index = 0;


    if (np->type == RXNT_TERM_LIST) {
      if (np->lnode->type == RXNT_TERM_LIST) {
        if (np->lnode->lnode->type == RXNT_TERM_LIST) {
          if (np->lnode->lnode->lnode->type == RXNT_TERM_LIST) {
          if (np->lnode->lnode->lnode->lnode->type == RXNT_TERM_LIST) {
          if (np->lnode->lnode->lnode->lnode->lnode->type == RXNT_TERM_LIST) {
            if (np->lnode->lnode->lnode->lnode->lnode->lnode->type == RXNT_SECTION &&
                    np->lnode->lnode->lnode->lnode->lnode->rnode->type == RXNT_STAR) {

                        if (np->lnode->lnode->lnode->lnode->rnode->type == RXNT_STAR ||
                            np->lnode->lnode->lnode->rnode->type == RXNT_STAR ||
                            np->lnode->rnode->type == RXNT_STAR ||
                            np->rnode->type == RXNT_STAR) ;
                        else {
                
                            _stprintf(&txt[i++],":");
                            _stprintf(&txt[i++],"%c",np->lnode->lnode->lnode->lnode->lnode->lnode->c);
                            if ((ch=np->lnode->lnode->lnode->lnode->lnode->lnode->c) == 'R' || ch == 'Q') {
                                _stprintf(&txt[i],"..");
                                i+=2;
                                }
                            _stprintf(&txt[i++],"<");
                            i=get_parse_str(np->lnode->lnode->lnode->lnode->rnode,txt,i,depth,tree_flag);
                            _stprintf(&txt[i++],",");
                            i=get_parse_str(np->lnode->lnode->lnode->rnode,txt,i,depth,tree_flag);

                            _stprintf(&txt[i++],",");
                            i=get_parse_str(np->lnode->rnode,txt,i,depth,tree_flag);
                            _stprintf(&txt[i++],",");
                            i=get_parse_str(np->rnode,txt,i,depth,tree_flag);
                        
                            _stprintf(&txt[i++],">");
                            return i;
                            }
                        }
                }
        }
      }
    }
    }
    }

    if (np->type == RXNT_TERM_LIST) {
      if (np->lnode->type == RXNT_TERM_LIST) {
      if (np->lnode->lnode->type == RXNT_TERM_LIST) {
            if (np->lnode->lnode->lnode->type == RXNT_SECTION && np->lnode->lnode->rnode->type == RXNT_STAR) {
                        if (np->lnode->rnode->type == RXNT_STAR || np->rnode->type == RXNT_STAR) ;
                        else {
                            _stprintf(&txt[i++],":");
                            _stprintf(&txt[i++],"%c",np->lnode->lnode->lnode->c);
                            if ((ch=np->lnode->lnode->lnode->c) == 'R' || ch == 'Q') {
                                _stprintf(&txt[i],"..");
                                i+=2;
                                }
                            _stprintf(&txt[i++],"<");
                            i=get_parse_str(np->lnode->rnode,txt,i,depth,tree_flag);
                            _stprintf(&txt[i++],",");
                            i=get_parse_str(np->rnode,txt,i,depth,tree_flag);
                            _stprintf(&txt[i++],">");
                            return i;
                            }
                        }
                }
        }	
    }

    if (np->type == RXNT_TERM_LIST) {
      if (np->lnode->type == RXNT_TERM_LIST) {
          if (np->lnode->lnode->type == RXNT_TERM_LIST) {
           if (np->lnode->lnode->lnode->type == RXNT_TERM_LIST) {
           if (np->lnode->lnode->lnode->lnode->type == RXNT_TERM_LIST) {
           if (np->lnode->lnode->lnode->lnode->lnode->type == RXNT_TERM_LIST) {
           if (np->lnode->lnode->lnode->lnode->lnode->lnode->type == RXNT_TERM_LIST) {
            if (np->lnode->lnode->lnode->lnode->lnode->lnode->rnode->type == RXNT_SECTION &&
                np->lnode->lnode->lnode->lnode->lnode->rnode->type == RXNT_STAR) {
                        if (np->lnode->lnode->lnode->lnode->rnode->type == RXNT_STAR ||
                            np->lnode->lnode->lnode->rnode->type == RXNT_STAR ||
                            np->lnode->rnode->type == RXNT_STAR ||
                            np->rnode->type == RXNT_STAR) ;
                        else {
                            i=get_parse_str(np->lnode->lnode->lnode->lnode->lnode->lnode->lnode,txt,i,depth,tree_flag);
                            _stprintf(&txt[i++],":");
                            _stprintf(&txt[i++],"%c",np->lnode->lnode->lnode->lnode->lnode->lnode->rnode->c);
                            if ((ch=np->lnode->lnode->lnode->lnode->lnode->lnode->rnode->c) == 'R' || ch == 'Q') {
                                _stprintf(&txt[i],"..");
                                i+=2;
                                }
                            _stprintf(&txt[i++],"<");
                            i=get_parse_str(np->lnode->lnode->lnode->lnode->rnode,txt,i,depth,tree_flag);
                            _stprintf(&txt[i++],",");
                            i=get_parse_str(np->lnode->lnode->lnode->rnode,txt,i,depth,tree_flag);
                            _stprintf(&txt[i++],",");
                            i=get_parse_str(np->lnode->rnode,txt,i,depth,tree_flag);
                            _stprintf(&txt[i++],",");
                            i=get_parse_str(np->rnode,txt,i,depth,tree_flag);
                            _stprintf(&txt[i++],">");
                            return i;
                            }
                        }
           }}}}
          }
      }	
    }
  
    
    

  
    if (np->type == RXNT_TERM_LIST) {
      if (np->lnode->type == RXNT_TERM_LIST) {
          if (np->lnode->lnode->type == RXNT_TERM_LIST) {
           if (np->lnode->lnode->lnode->type == RXNT_TERM_LIST) {
            if (np->lnode->lnode->lnode->rnode->type == RXNT_SECTION && np->lnode->lnode->rnode->type == RXNT_STAR) {
                        if (np->lnode->rnode->type == RXNT_STAR || np->rnode->type == RXNT_STAR) ;
                        else {
                            i=get_parse_str(np->lnode->lnode->lnode->lnode,txt,i,depth,tree_flag);
                            _stprintf(&txt[i++],":");
                            _stprintf(&txt[i++],"%c",np->lnode->lnode->lnode->rnode->c);
                            if ((ch=np->lnode->lnode->lnode->rnode->c) != 'D' && ch != 'C') {
                                _stprintf(&txt[i],"..");
                                i+=2;
                                }
                            _stprintf(&txt[i++],"<");
                            i=get_parse_str(np->lnode->rnode,txt,i,depth,tree_flag);
                            _stprintf(&txt[i++],",");
                            i=get_parse_str(np->rnode,txt,i,depth,tree_flag);
                            _stprintf(&txt[i++],">");
                            return i;
                            }
                        }
                }
          }
      }	
    }
  
  
  
  if (index) return i;
  
  switch (np->type)  {

    case RXNT_RX:           // Must match lnode and start and/or end.
        if (np->flag & RX_BEGIN) _stprintf(&txt[i++],"^");
        i = get_parse_str(np->lnode,txt,i,depth+1,tree_flag);
        if (np->flag & RX_END)  _stprintf(&txt[i++],"$");
        return i;

    case RXNT_STR:
        _stprintf(&txt[i++],"@");
        i = get_parse_str(np->lnode,txt, i,depth,tree_flag);
        return i;

    case RXNT_STRING:   /* Must match the string     */

        _stprintf(&txt[i],"%s",np->str);
        i += strlen(np->str);
        i = get_parse_str(np->lnode,txt,i,depth,tree_flag);
        return i;

    case RXNT_AND:			/* Match both nodes at this position */

        _stprintf(&txt[i++],"(");
        i = get_parse_str(np->lnode,txt,i,depth+1,tree_flag);
        _stprintf(&txt[i++],"&");
        i = get_parse_str(np->rnode,txt,i,depth+1,tree_flag);
        _stprintf(&txt[i++],")");
        return(i);

    case RXNT_ALSO:			/* Match both nodes, second from any spot */
                    /* what if first is longer than second????? */

    //	_stprintf(&txt[i],"((");
    //    i+=2;
        i = get_parse_str(np->lnode,txt,i,depth+1,tree_flag);
        _stprintf(&txt[i],"&&");
        i+=2;
        i = get_parse_str(np->rnode,txt,i,depth+1,tree_flag);
    //	_stprintf(&txt[i],"))");
        return i;

    case RXNT_KLEENE:
        _stprintf(&txt[i++],"<");
        i = get_parse_str(np->lnode,txt,i,depth+1,tree_flag);
        _stprintf(&txt[i++],",");
        i = get_parse_str(np->rnode,txt,i,depth+1,tree_flag);
        _stprintf(&txt[i++],">");
        return i;

    case RXNT_ALT:          // Match  either subnode. Return the longer

        _stprintf(&txt[i++],"(");
        i = get_parse_str(np->lnode,txt,i,depth+1,tree_flag);
        _stprintf(&txt[i++],"|");
        i = get_parse_str(np->rnode,txt,i,depth+1,tree_flag);
        _stprintf(&txt[i],")");
        return(i+1);

    case RXNT_TERM_LIST:     /* Must match both subnodes. */
    //	_stprintf(&txt[i],"TERM");
    //	i += 4;
        i = get_parse_str(np->lnode,txt,i,depth+1,tree_flag);
        i = get_parse_str(np->rnode,txt,i,depth+1,tree_flag);
        return i;

    case RXNT_PLUS:     /* Must match 1 or more occurance(s) of left-node. */

        _stprintf(&txt[i++],"+");
        i = get_parse_str (np->lnode,txt,i,depth+1,tree_flag);
        return i;

              /* ------ Fall into RXNT_STAR ----- */
    case RXNT_STAR:     /* Match 0 or more occurance(s) of left-node. */

        i = get_parse_str (np->lnode,txt,i,depth+1,tree_flag);
        _stprintf(&txt[i++],"*");
        return i;


    case RXNT_RANGE:
//		_stprintf(&txt[i++],"[");
        txt[i++]=np->lnode->c; txt[i++]='-'; txt[i++]=np->rnode->c;
//		_stprintf(&txt[i++],"[");
        return i;

    case RXNT_CLASS:
        _stprintf(&txt[i++],"[");
            i = get_parse_str (np->lnode,txt,i,depth,tree_flag);
        _stprintf(&txt[i++],"]");
            return  i;

    case RXNT_CLASS_LIST: 
//		_stprintf(&txt[i++],"[");
        i = get_parse_str (np->lnode,txt,i,depth,tree_flag);
        i = get_parse_str (np->rnode,txt,i,depth,tree_flag);
//		_stprintf(&txt[i++],"]");
        return i;

    case RXNT_NOT_CLASS:     /* Must not match left-node. */
    case RXNT_NOT:

        _stprintf(&txt[i],"[^");
        i+=2;
        i = get_parse_str (np->lnode,txt,i,depth,tree_flag);
        _stprintf(&txt[i++],"]");
        return  i;


    case RXNT_C:        /* Must match the character. */
        _stprintf(&txt[i++],"%c",np->c); return i;

    case RXNT_ANY_C: _stprintf(&txt[i++],"."); return i;
    case RXNT_EVAL:
        i = get_parse_str (np->lnode,txt,i,depth,tree_flag);
        return i;

    case RXNT_NE:
        _stprintf(&txt[i++],"<");
        i = get_parse_str (np->lnode,txt,i,depth,tree_flag);
//		if (np->lnode->type == RXNT_C || np->lnode->type == RXNT_POP)
//			txt[i]= get_node_c(np->lnode);
//		else return i;
        _stprintf(&txt[i],"!=");
        i += 2;
//		if (np->rnode->type == RXNT_C || np->rnode->type == RXNT_POP)
//			txt[i]= get_node_c(np->rnode);
//		else return i;
        i = get_parse_str (np->rnode,txt,i,depth,tree_flag);
        _stprintf(&txt[i++],">");
        return i;

    case RXNT_EQ:
    case RXNT_GE:
    case RXNT_LE:
        _stprintf(&txt[i++],"<");
//		if (np->lnode->type == RXNT_C || np->lnode->type == RXNT_POP)
//			txt[i]= get_node_c(np->lnode);
//		else return i;
        i = get_parse_str (np->lnode,txt,i,depth,tree_flag);

        if (np->type == RXNT_EQ) _stprintf(&txt[i],"==");
        else if (np->type == RXNT_GE) _stprintf(&txt[i],">=");
        else if (np->type == RXNT_LE) _stprintf(&txt[i],"<=");
        else _stprintf(&txt[i],"??");

        i+=2;
        i = get_parse_str (np->rnode,txt,i,depth,tree_flag);

//		if (np->rnode->type == RXNT_C || np->rnode->type == RXNT_POP)
//			txt[i]= get_node_c(np->rnode);
//		else return i;
        _stprintf(&txt[i++],">");
        return i;

    case RXNT_MATH:

        char buffer[10];
        switch (np->c) {
            case '>': _stprintf(buffer,"~%u>",np->flag); break;
            case '<': _stprintf(buffer,"~%u<",np->flag); break;
            case '&': _stprintf(buffer,"~%u&",np->flag); break;
            case '|': _stprintf(buffer,"~%u|",np->flag); break;
            case '=': _stprintf(buffer,"~%u=",np->flag); break;
            case '!': _stprintf(buffer,"~%u!",np->flag); break;
            case '+': _stprintf(buffer,"~%u+",np->flag); break;
            }
        _stprintf(&txt[i],"%s",buffer);
        i += strlen(buffer);
        return i;

    case RXNT_INC: 
        _stprintf(&txt[i++],"<");
        i = get_parse_str (np->lnode,txt,i,depth,tree_flag);
        _stprintf(&txt[i++],"=");
        i = get_parse_str (np->rnode,txt,i,depth,tree_flag);
        _stprintf(&txt[i++],">");
        return i;

    case RXNT_SET: 
        _stprintf(&txt[i++],"<");
        i = get_parse_str (np->lnode,txt,i,depth,tree_flag);
        _stprintf(&txt[i++],"=");
        i = get_parse_str (np->rnode,txt,i,depth,tree_flag);
        _stprintf(&txt[i++],">");
        return i;
    case RXNT_LAW: _stprintf(&txt[i],"<law>"); return i+5;
    case RXNT_PUSH: _stprintf(&txt[i],"{"); return i+1;
    case RXNT_POP: 
        if (np->flag > 1) {
            _stprintf(&txt[i],"#F%s",np->str);
            i += (int)(strlen(np->str)+2);
            return i;
            }
        else { _stprintf(&txt[i],"#%c",np->c+'a'); return i+2; }
    case RXNT_RBRACE: _stprintf(&txt[i],"{"); return i+1;
    case RXNT_GREATER:
        _stprintf(&txt[i],"[>");
        i+=2;
        i = get_parse_str (np->lnode,txt,i,depth,tree_flag);
        _stprintf(&txt[i],"]");
        return i+ 1;
    case RXNT_LESSER:
        _stprintf(&txt[i],"[<");
        i+=2;
        i = get_parse_str (np->lnode,txt,i,depth,tree_flag);
        _stprintf(&txt[i],"]");
        return i+ 1;
    case RXNT_INCR:
        _stprintf(&txt[i++],"[");
        for (index=0; index < np->flag; index++)
            _stprintf(&txt[i++],"+");
        i = get_parse_str (np->lnode,txt,i,depth,tree_flag);
        _stprintf(&txt[i],"]");
        return i+ 1;
    case RXNT_DECR:
        _stprintf(&txt[i++],"[");
        for (index=0; index < np->flag; index++)
            _stprintf(&txt[i++],"-");
        i = get_parse_str (np->lnode,txt,i,depth,tree_flag);
        _stprintf(&txt[i],"]");
        return i+ 1;
    case RXNT_SECTION: 
        if (np->c == 'P') { _stprintf(&txt[i],"%c",np->c); return i+1; }
        else  { _stprintf(&txt[i],":%c",np->c); return i+2; }
    default:  _stprintf(&txt[i],"ERR %2u",i); return i+6;
  }
#else
return 0;
#endif
}


/* This is very interesting...I think the only reason that
reset closures is here is that I was planning on reusing the
patterns. Right now I am not doing that, just moving forward with
the patterns. Thus for a NO_MACROS_VERSION that does not reuse
the patterns I can simply ignore this processing !!!

*/

void reset_closures(RXNODE *np)
{
    if (np == NULL) return;
    if (np->lnode) reset_closures(np->lnode);
    if (np->rnode) reset_closures(np->rnode);
    if (np->type == RXNT_STAR || np->type == RXNT_PLUS)
         np->c = -1;
}

#ifndef CHAR_ONLY
void push_gi(int i) {
#ifdef SAFETY
    if (gi_index +1 >= GRAB_MAX) {
        my_error("gi_index too high");
        return;
        }
#endif
#ifdef CSW
    CSW_EXEC(printf("\npushing %u",i));
#endif
    gi_stack[++gi_index]=i;
    }

int pop_gi(void) {
    int i;
#ifdef SAFETY
    if (gi_index < 0) {
        my_error("gi_index too low");
        return -1;
        }
#endif
    i=gi_stack[gi_index--];
#ifdef CSW
    CSW_EXEC(printf("\npopping %u",i));
#endif
    return(i);
    }

void reset_gi(void) {
    gi_index = -1;
    }
#endif


/* EFGHIJKLMNOP all pop an fval onto the stack */


void f_setbidder(int b) { assert( b < 2); fbidder = b; }
int f_pop(void) { return fstack[--fstack_index]; }
void f_push(int val) { fstack[fstack_index++] = val; }
void f_count_bits(void) { int val=0,i,a; a = f_pop();
    for (i=0;i<4;i++)
        if ((1<<i) & a) val += 1;
     f_push(val);
     }
int top(void) { return f_pop(); }
void f_rot(void) { int a, b; a=f_pop(); b=f_pop(); f_push(a); f_push(b); }
void f_invert(void) { int a; a= f_pop(); if (a) f_push(0); else f_push(1); }
void f_str(void) { f_pop(); }
void f_getval(void) { 
    int a, val; 
    a = f_pop();
	if (a<0 || a>99) 
		do_log(1,40,"bad value for a",a);
    assert( a >= 0 && a < 100);
    val = fval[a];
    if (a > 69 && a<77 && val>40) {
        do_log(1,40,"bad forth getval for TOP; BOTTOM variables");
        }
    f_push(val);
    }

extern int forth_bid_count;
extern int forth_bids[100];
void f_bid(void) {
/* added 210107!
	the sequence is split into the bids when do_constraints is entered. forth_bids and forth_bid_count are global variables holding the information gleaned.
	push the position of the desired bid (as in a stack; so if you want the last bid you would push 0, i.e., 0 based as usual in C)
	that bid as an integer will then be pushed onto the stack
*/

	int which_bid, bid;
	if (forth_bid_count < 1 || forth_bid_count > 99) { f_push(99); return; }
	which_bid = f_pop();
	if (which_bid < 0 || which_bid >= forth_bid_count) { f_push(99); return; }
	bid = forth_bids[forth_bid_count - which_bid - 1]; // 0 based remember
	f_push(bid);
}

void f_getoppval(void) { int a; a = f_pop(); assert( a >= 0 && a < 100);  f_push(fval[a]);}
void f_setval(void) { 
    int a, val;
    a=f_pop(); 
    if (a < 0 || a >= 100) 
        do_log(1,40,"bad forth setval");
    val = f_pop();
    // somewhere a bad value had been set so this is debugging
	if (a>69 && a<78 && val > 40) {
        do_log(1,40,"bad hi value for top or bottom setting");
		fprintf(stderr,"bad hi value for top or bottom setting\n");
		}
	if (a>69 && a<78 && val < 0) {
        do_log(1,40,"bad lo value for top or bottom setting");
		fprintf(stderr,"bad lo value for top or bottom setting\n");
		}
#ifdef EXTRA_CHECK
	// this is now check in do_range where both values are known at the same time
	// rather than this check of individual values against the other
	if (a>69 && a<78 && a % 2 == 0 && fval[a] < fval[a+1]) {
		fprintf(stderr,"high value is less than low value; a is %u\n",a);
		}
	if (a>69 && a<78 && a % 2 == 1 && fval[a] > fval[a-1]) {
		fprintf(stderr,"low value is greater than high value; a is %u\n",a);
		}
#endif
    assert( a >= 0 && a < 100);
    fval[a] = val;}
void f_add(void) { f_push(f_pop() + f_pop()); };
void f_drop(void) { f_pop(); }
/* check the current convention setting for this line getting it from vsa[depth].pos */
extern int is_set(char *,int);
void f_convention(void) { int s, bb,d,b,val; f_push(80); /*depth*/ f_getval(); d=top(); b=vsa[d].current_bidder;
    f_push(69); f_getval(); bb=top(); assert(bb==b);  /* current bidder obtained (and verified) */
    s = top()-1; /* the string of 4 available to search (1 based) */
    val = is_set(fstr[s],b%2);
    // fprintf(stderr,"\nchecking:%s, for dir:%d, result is:%d",fstr[s],b%2,val);
    // fprintf(stderr,"\nthe strings:%s,%s,%s,%s",fstr[0],fstr[1],fstr[2],fstr[3]);
    f_push(val);
    }
void f_dup(void) { int a = f_pop(); f_push(a); f_push(a); }
void f_sub(void) { f_push(f_pop() - f_pop()); };
void f_less_than(void) { f_push(f_pop() < f_pop()); }
void f_greater_than(void) { f_push(f_pop() > f_pop()); }
void f_equal_to(void) { f_push(f_pop() == f_pop()); }
void f_increment(void) { f_push(f_pop() + 1); }
void f_decrement(void) { f_push(f_pop() - 1); }
void f_equal_to_zero(void) { f_push(f_pop() == 0); }
void f_not(void) { 
    f_push(!f_pop()); }
void f_if(void) { int a,b,c; a = f_pop(); b = f_pop(); c=f_pop();
    if (a) f_push(b); else f_push(c); }
void f_max(void) { int a, b; a=f_pop(); b=f_pop(); f_push( a < b ? b : a); }
void f_min(void) { int a, b; a=f_pop(); b=f_pop(); f_push( a < b ? a : b); }
void f_or(void) { int a, b; a=f_pop(); b=f_pop(); f_push(a | b); }
void f_and(void) { int a,b;  a = f_pop(); /* printf("a:%u\n",a); */
b = f_pop(); /* printf("b:%u\n",b); */ f_push(a & b); }
void f_tochar(void) { f_push(f_pop() + '0'); }
void f_modulo(void) { int a,b; a = f_pop(); b=f_pop(); f_push(b % a); }
void f_tosuit(void) { int a = f_pop(); 
     switch(a) {
    case 0: f_push('C'); break;
    case 1: f_push('D'); break;
    case 2: f_push('H'); break;
    case 3: f_push('S'); break;
    case 4: f_push('N'); break;
}}
void f_print(void) {
    printf("TOP:%u\n",fstack[fstack_index-1]);
}

void f_fromchar(void) { 
    int a = f_pop();
    if (a >= '0' && a <= '9') f_push(a - '0');
    else switch(toupper(a)) {
    case 'C': f_push(0); break;
    case 'D': f_push(1); break;
    case 'H': f_push(2); break;
    case 'S': f_push(3); break;
    case 'N': f_push(4); break;
        case 'M': f_push(3); break;
        case 'm': f_push(1); break;
        default: f_push(3); break;
    }
}
int in_long1(void);
int in_long2(void);
int in_out1(void);
int in_out2(void);
int in_out3(void);
int od(void);
int shortness(void);

void f_debug(void) { bid20_debugging = f_pop(); 
	if (bid20_debugging)
		fprintf(stderr,"bid20_debugging set to %u\n",bid20_debugging);
	} // function 34
void f_mul(void) { f_push(f_pop() * f_pop()); }
void f_div(void) { f_push((f_pop() * 10) / f_pop()); }
void f_in_long1(void) { f_push(in_long1() + '0'); }
void f_in_long2(void) { f_push(in_long2() + '0'); }
void f_in_out1(void) { f_push(in_out1() + '0');}
void f_in_out2(void) { f_push(in_out2() + '0');}
void f_in_out3(void) { f_push(in_out3() + '0');}
void f_od(void) { f_push(od() + '0');}
void f_winner_count(void) { f_push(winner_count() + '0');}
void f_shortness(void) { f_push(shortness() + '0'); }
int get_total_toppers(void);
int get_total_slots(void);
void f_toppers(void) {
	int toppers=get_total_toppers();
	f_push(toppers+'0');
	}
void f_slots(void) {
	int slots=get_total_slots();
	f_push(slots+'0');
	}
void f_match_len(void) { int depth, up, len;
	f_push(67);
    f_getval();
	depth = f_pop();
	up = f_pop();
	depth -= up;
	len = vsa[depth].len;
	f_push(len);
	}
void f_match_char(void) { int depth,up, position;
	f_push(67);
	f_getval();
	depth = f_pop();
	up = f_pop();
	depth -= up;
	position = f_pop();
	f_push(vsa[depth].str[position]);
	}

#undef OLD_F_SET_GRAB
void f_set_grab1(int grab_index, int val);
// this sets the bid suits into grabs #c-#j
// the bid suits were set before the bid_search call into fval[30-7] so now transfer into the grabs
void f_set_grabs(void) {
    int i,val_index,grab_index;
    for (i=0; i<8; i++){
        val_index = 30 + i;
        grab_index = 2 + i;
        f_set_grab1(grab_index,fval[val_index]);
        }
}
                    
#if defined(OLD_F_SET_GRAB)
void f_set_grab(void) { // was function 40, now 46 210715
    int grab_index = f_pop();
    int index = grab_index + 30; // set to a fval not a f_pop()
    grab[grab_index+2].c = fval[index]; // grabs start at #c; because #a#b are the play
              /*
              if (index == 37) {
                  // printf("the fvals\n");
                  // for (int i=30; i<38;i++) printf("%c-%d;",fval[i],fval[i]);

                  printf("the grabs\n");
                  for (int i=0; i<10;i++) printf("%c-%d;",grab[i],grab[i]);
                  printf("\n");
              } */
    }
#else
void f_set_grab(void) { // was function 40, now 46 210715
    int a = f_pop(), b = f_pop();
    grab[a].c = b;
    }

void f_set_grab1(int grab_index, int val) {
    grab[grab_index].c = val;
    }
#endif
                  
void f_get_grab(void) { // function 47 210715
    int a=f_pop();
    f_push((int) grab[a].c);
    }

void f_print_nl(void) { printf("\n"); }
void f_nop(void) { f_pop(); }
void f_mod(void) { int a=f_pop(), b= f_pop(); f_push(a%b); }
int string_count=0;
void f_pr_str(void) {
    int a=f_pop();
    if (a<string_count) printf("%s",fstr[a]);
    }

extern int depth_for_forth;
int get_line_number(void) {
    int depth = depth_for_forth;
    fprintf(stderr,"depth:%d",depth);
    int line_number = vsa[depth].pos->line_number;
    int id = vsa[depth].pos->id;
    fprintf(stderr,"return id:%d line:%d\n",id,line_number);
    return id;
}

void f_line(void) { f_push(get_line_number()); }
int get_length_by_suit(int);
int get_bit_hcps(int);
                  
int get_controls(void) {
    int exclusion = fval[44], excluded_suit = fval[45], trump = fval[46];
    int total = 0,len,toppers=0,bits;
              
    for (int suit=0; suit<4; suit++) {
        if (exclusion && (suit == excluded_suit))
                      continue;
        len = get_length_by_suit(suit);
        bits = get_bit_hcps(suit);
        switch(len) {
            case 0: break;
            case 1: if (bits&8) toppers=1; break;
            default: toppers = 0;
                if (bits & 8) toppers += 1;
                if (bits & 4 && suit == trump) toppers += 1;
                else if (bits & 4 && exclusion==2) toppers += 1;
                break;
            }
        total += toppers;
        }
    return total;
    }
                  
void f_controls(void) { f_push(get_controls()); }


typedef void (*forth_function)();
/* convention = 29 (the last) */

forth_function myffs[]={f_getval,f_setval,f_add,f_mul,f_drop,f_dup,f_sub,f_less_than,f_greater_than,  //8
f_equal_to,f_increment,f_decrement,f_equal_to_zero,f_if,f_max,f_min,f_or,    // 16
f_and,f_tochar,f_fromchar,f_count_bits,f_print,f_div,f_in_out1,f_in_out2,f_in_out3,f_modulo,f_not,f_convention,  // 28
f_in_long1,f_in_long2,f_winner_count,f_shortness,f_od,f_debug,f_match_len,f_match_char,f_toppers, // 37
              f_slots, f_bid, f_print_nl, f_nop, f_mod, f_pr_str, f_line, f_controls, f_set_grab, f_get_grab, //47
              f_tosuit , f_set_grabs  };       // shortness=32, 0 based toppers=37
//f_od is 33, offensive to defensive ratio
// f_bid is the new get bid function at 39
                  // f_set_grabs transfers the bid suits after the grabs are created for bidding


/* count bits = 20*/

extern void python_log(char *);
char temp_log[90];
/*
// this uses the description to get the length representation
// it is found after the :C and each suit has 1 char 0-9
// 15 is currently the spot after the :C
int get_length_by_suit(int suit) {
    int length,pos = 14;
    const char *d = description_for_forth;
    if (d==NULL) return 0; // this can occur when evaluating a sequence but no hand or description is available
    assert (d[pos] == 'C'); // make sure description is available
    assert(suit >=0 && suit < 4); // make suit suit is correct 
    length = d[pos+suit+1] - '0';
    return length;
    }
*/

int get_bit_hcps(int suit);
int get_length_by_suit(int suit);

int get_total_toppers(void) {
	int total = 0,len,toppers=0,bits;
	for (int suit=0; suit<4; suit++) {
		len=get_length_by_suit(suit);
		bits = get_bit_hcps(suit);
		switch(len) {
			case 0:  break;
			case 1: if (bits&8) toppers=1; break;
			default:
				toppers = 0;
				if (bits & 8) toppers += 1;
				if (bits & 4) toppers += 1;
				break;
			}
		total += toppers;
		}
	return total;
	}

int get_total_slots(void) {
	int total = 0,len,toppers;
	for (int suit=0; suit<4; suit++) {
		len=get_length_by_suit(suit);
		switch(len) {
			case 0: toppers=0; break;
			case 1: toppers=1; break;
			default: toppers=2; break;
			}
		total += toppers;
		}
	return total;
	}

// this uses the description to get the length representation
// it is found after the :C and each suit has 1 char 0-9
// 15 is currently the spot after the :C
int get_length_by_suit(int suit) {
    int length,pos = 14;
    const char *d = description_for_forth;
    assert(d != NULL);
    assert (d[pos] == 'C'); // make sure description is available
    assert(suit >=0 && suit < 4); // make suit suit is correct 
    length = d[pos+suit+1] - '0';
    return length;
    }

int get_suit_by_position(int position) {
    assert(position >= 0 && position < 100); // to handle 2 variables
    f_push(position);
    f_getval(); // suit as char on forth stack
    f_fromchar(); // suit as int on forth stack
    return top();  // suit now as variable here
}

int get_length_by_position(int position) {
    int length,suit; //,pos=14;
    // const char *d = description_for_forth;
    assert(position >= 0 && position < 100); // to handle 2 variables
    suit = get_suit_by_position(position);
    length = get_length_by_suit(suit);
    return length;
    }


// this uses the description to get the hcp representation
// it is found after the :H and each suit has 3 chars 00C00D00H00S
// 25 is currently the spot after the :H
int get_variable(int position) {
    int hcp,suit,pos=24;
    const char *d = description_for_forth;
    assert(position >= 0 && position < 100); // to handle 2 variables
    f_push(position);
    f_getval(); // suit as char on forth stack
    f_fromchar(); // suit as int on forth stack
    suit = top();  // suit now as variable here
#ifdef CSW
    CSW_EXEC(do_log(1,20,"get_variable: pos:%u suit:%u",position,suit));
#endif
    assert(d != NULL);
	if (d[pos] != 'H')
		printf("bad");
	if (d[pos] != 'H')
		printf("bad H position\n");
    assert (d[pos] == 'H'); // make sure description is available
#ifdef CSW
    if (suit >3 || suit < 0) {
        CSW_EXEC(do_log(1,20,"get_variable: suit:%u",suit));
        }
#endif
    assert(suit >=0 && suit < 4); // make sure suit is correct 
    hcp = get_hcps(suit);
#ifdef CSW
    ONE_SHOT(401,BidMessage("OS: get_variable position:%u hcp:%u\n",position,hcp));
#endif
    return hcp;
    }

int get_hcps(int suit) {
    int bit_hcp,hcp=0,pos=24;
    const char *d = description_for_forth;
    assert(d != NULL);
    assert (d[pos] == 'H'); // make sure description is available
	if (suit < 0 || suit > 3)
		printf("bad suit in get_hcps\n");
    assert(suit >=0 && suit < 4); // make suit suit is correct 
    pos = pos + 1 + suit * 3; // move along string description to proper suit
    assert(isdigit(d[pos]));
    assert(isdigit(d[pos+1]));
    bit_hcp = (d[pos]-'0')*10 + d[pos+1] - '0'; // convert 2 digit representation to hcp bitwise
	if (bit_hcp & 8) hcp += 4;
	if (bit_hcp & 4) hcp += 3;
	if (bit_hcp & 2) hcp += 2;
	if (bit_hcp & 1) hcp += 1; // these convert from bits to hcps
    assert(hcp >= 0 && hcp < 16);
    return hcp;
    }

int get_bit_hcps(int suit) {
    int bit_hcp,pos=24;
    const char *d = description_for_forth;
    assert(d != NULL);
    assert (d[pos] == 'H'); // make sure description is available
	if (suit < 0 || suit > 3)
		printf("bad suit in get_hcps\n");
    assert(suit >=0 && suit < 4); // make suit suit is correct 
    pos = pos + 1 + suit * 3; // move along string description to proper suit
    assert(isdigit(d[pos]));
    assert(isdigit(d[pos+1]));
    bit_hcp = (d[pos]-'0')*10 + d[pos+1] - '0'; // convert 2 digit representation to hcp bitwise
    return bit_hcp;
    }

int get_bit_count(int hcp) {
   int count = 0;
   for (int i=0; i<4; i++)
       if (1<<i & hcp) count += 1;
 //  printf("bit:%u\n",count);
   return count;
   }
       
int get_honors(int position) {
#ifdef CSW
    CSW_EXEC(do_log(1,20,"get_honors: pos:%u",position));
#endif
    return get_bit_count(get_variable(position) & 15); }
int get_minors(int position) { int count;
#ifdef CSW
    CSW_EXEC(do_log(1,20,"get_minors: pos:%u",position));
#endif
    count = get_bit_count(get_variable(position) & 7);  // KQJ
    if (get_bit_count(get_variable(position) & 12)==2) count -= 1;  // drop K if AK present
    return count;
    }

// position is the long suit without losers.
// all aces and kings accounted for

int get_total_winners8(int position) {
    int i,hcp,length,suit,total_winners=6;
    total_winners += get_length_by_position(position); // long suit of winners
    suit = get_suit_by_position(position);
    for (i=0; i<4; i++) {
        if (i==suit) continue; // already counting this suit
        hcp = get_hcps(i);
        length = get_length_by_suit(i);
        switch (length) {
            case 0: case 1: case 2:	total_winners += length; break;
            case 3: if (hcp & 2) { total_winners += 3; } // queen
                    else { total_winners += 2; }
                    break;
            default: if (hcp & 3) total_winners += 4; // qj
                    else if (hcp & 2) total_winners += 3; // q
                    else total_winners += 2; // nada
                    break;
        }}
    return total_winners;
    }

int get_majors(int position) {
#ifdef CSW
    CSW_EXEC(do_log(1,20,"get_majors: pos:%u",position));
#endif
 return get_bit_count(get_variable(position) & 12); }     
 

int winner_count(void) {
    return get_total_winners8(90);// why does ORIGINAL_SUIT not work?
}

// we will put the variables into the following positions
// a,b = first bidder of to be agreed suit at 90
// c,d = their suit at 92
// e,f = an unbid or other suit at 94
// the forth portion of the bidding system must set up the variables

enum { 
   IN_OUT_VERY_BAD=0,
   IN_OUT_BAD,
   IN_OUT_AVG,
   IN_OUT_GOOD,
   IN_OUT_VERY_GOOD,
   FIRST_SUIT=90,
   ORIGINAL_SUIT=90,
   OPPONENT_SUIT=92,
   SECOND_SUIT=92,
   ADDITIONAL_SUIT=94
   };

int in_long1(void) {
      int i;
      int suit_honors,total_honors=0,in, ans;
#ifdef CSW
    CSW_EXEC(do_log(1,20,"in_long1:"));
#endif

      suit_honors = get_honors(FIRST_SUIT);
      for (i=0;i<3;i++) 
          total_honors += get_bit_count(get_hcps(i));
      if (suit_honors == 0) return IN_OUT_VERY_BAD;
      else if (suit_honors == total_honors) return IN_OUT_VERY_GOOD;
      else if (total_honors == 0) return IN_OUT_VERY_BAD;

      in = 100 * suit_honors;
      ans = in / total_honors;

      if (ans < 20) return IN_OUT_VERY_BAD;
      else if (ans < 40) return IN_OUT_BAD;
      else if (ans < 60) return IN_OUT_AVG;
      else if (ans < 80) return IN_OUT_GOOD;
      else return IN_OUT_VERY_GOOD;
}

int in_long2(void) {
      int i;
      int suit_honors,total_honors=0,in, ans;
#ifdef CSW
    CSW_EXEC(do_log(1,20,"in_long2"));
#endif
      suit_honors = get_honors(FIRST_SUIT);
      suit_honors += get_honors(SECOND_SUIT);
      for (i=0;i<3;i++) 
          total_honors += get_bit_count(get_hcps(i));
      if (suit_honors == 0) return IN_OUT_VERY_BAD;
      else if (suit_honors == total_honors) return IN_OUT_VERY_GOOD;
      else if (total_honors == 0) return IN_OUT_VERY_BAD;

      in = 100 * suit_honors;
      ans = in / total_honors;

      if (ans < 20) return IN_OUT_VERY_BAD;
      else if (ans < 40) return IN_OUT_BAD;
      else if (ans < 60) return IN_OUT_AVG;
      else if (ans < 80) return IN_OUT_GOOD;
      else return IN_OUT_VERY_GOOD;
}

int in_out1(void) {
      int original_suit, opponent_suit;
//	  int additional_suit;
      int total_minor_honors,in, ans;
#ifdef CSW
        CSW_EXEC(BidMessage("\nin_out1:"));
#endif
      original_suit = get_minors(ORIGINAL_SUIT);
      opponent_suit = get_minors(OPPONENT_SUIT);
      total_minor_honors = original_suit + opponent_suit;
      if (total_minor_honors == 0) return IN_OUT_AVG;
      in = 100 * original_suit;
  //    printf("in:%u\n",in);
  //    printf("tot:%u\n",total_minor_honors);
      ans = in / total_minor_honors;
  //    printf("ans is %u\n",ans); 
      if (ans < 20) return IN_OUT_VERY_BAD;
      else if (ans < 40) return IN_OUT_BAD;
      else if (ans < 60) return IN_OUT_AVG;
      else if (ans < 80) return IN_OUT_GOOD;
      // added 170725
      else if (original_suit == 1) return IN_OUT_GOOD;
      else return IN_OUT_VERY_GOOD;
      }
      
int shortness(void) {
    int bit_suit,suit,hcps;

    suit = get_suit_by_position(ORIGINAL_SUIT);
	if (suit < 0 || suit > 3)
		printf("bad suit in shortness\n");
	assert(suit >= 0 && suit <=3 );
    hcps = get_hcps(suit);  // points here are wasted
    bit_suit = get_variable(ORIGINAL_SUIT);
    if (bit_suit & 8) hcps -= 4;   // except for the ace
    // printf("shortness returns %d\n",6-hcps);
    return 6-hcps;
}

int od(void) {
      int original_suit, opponent_suit, additional_suit;
      int total_minor_honors,in, ans, opp_suit,opp_len;
      opp_suit = get_suit_by_position(OPPONENT_SUIT);
	  opp_len = get_length_by_suit(opp_suit);
	  if (opp_len > 2) return IN_OUT_VERY_BAD;
	  else if (opp_len < 1) return IN_OUT_VERY_GOOD;
      original_suit = get_minors(ORIGINAL_SUIT);
      opponent_suit = get_minors(OPPONENT_SUIT);
      additional_suit = get_minors(ADDITIONAL_SUIT);
	  if (original_suit != additional_suit) total_minor_honors = original_suit + opponent_suit + additional_suit;
      else total_minor_honors = original_suit + opponent_suit;
	  
	  if (total_minor_honors == 0) return IN_OUT_AVG;
      if (original_suit != additional_suit) in = 100 * (original_suit + additional_suit);
	  else in = 100 * original_suit;
	  ans = in / total_minor_honors;

      //if (ans < 20) return IN_OUT_VERY_BAD;
      //else 
	  if (ans < 40) return IN_OUT_BAD;
      else if (ans < 60) return IN_OUT_AVG;
      else if (ans < 80) return IN_OUT_GOOD;
      else return IN_OUT_VERY_GOOD;
}

int in_out2(void) {
#ifdef CSW
      ONE_SHOT(402,BidMessage("OS: in_out2"));
#endif
      int original_suit, opponent_suit, additional_suit;
      int total_minor_honors,in, ans;
#ifdef CSW
        CSW_EXEC(BidMessage("\nin_out2:"));
#endif
      original_suit = get_minors(ORIGINAL_SUIT);
      opponent_suit = get_minors(OPPONENT_SUIT);
      additional_suit = get_minors(ADDITIONAL_SUIT);
      total_minor_honors = original_suit + opponent_suit + additional_suit;
      if (total_minor_honors == 0) return IN_OUT_AVG;
      in = 100 * (original_suit + additional_suit);
 //     printf("in:%u\n",in);
 //     printf("tot:%u\n",total_minor_honors);
      ans = in / total_minor_honors;
 //     printf("ans is %u\n",ans); 
#ifdef CSW
      ONE_SHOT(403,BidMessage("OS: in_out2: %u",ans));
#endif
      if (ans < 20) return IN_OUT_VERY_BAD;
      else if (ans < 40) return IN_OUT_BAD;
      else if (ans < 60) return IN_OUT_AVG;
      else if (ans < 80) return IN_OUT_GOOD;
      else return IN_OUT_VERY_GOOD;
      }

int in_out3(void) {
      int original_suit, opponent_suit, additional_suit;
      int total_minor_honors,in, ans;
#ifdef CSW
        CSW_EXEC(BidMessage("\nin_out3:"));
#endif
      original_suit = get_minors(ORIGINAL_SUIT);
      opponent_suit = get_minors(OPPONENT_SUIT);
      additional_suit = get_minors(ADDITIONAL_SUIT);
      total_minor_honors = original_suit + opponent_suit + additional_suit;
      if (total_minor_honors == 0) return IN_OUT_AVG;
      in = 100 * (original_suit + additional_suit);
  //    printf("in:%u\n",in);
  //    printf("tot:%u\n",total_minor_honors);
      ans = in / total_minor_honors;
  //    printf("ans is %u\n",ans); 
      if (ans < 20) return IN_OUT_VERY_BAD;
      else if (ans < 40) return IN_OUT_BAD;
      else if (ans < 60) return IN_OUT_AVG;
      else if (ans < 80) return IN_OUT_GOOD;
      else return IN_OUT_VERY_GOOD;
      }



void push_contract(const char *contract) {
    int val;
    char command[20];

  //  ONE_SHOT(404,BidMessage("OS: push_contract"));
    val = 0;
    if (contract != NULL) {
      assert (contract[0] >= '0' && contract[0] < '8');
      val = 10 * (contract[0]-'0');
      switch (toupper(contract[1])) {
      case 'C': break;
      case 'D': val += 1; break;
      case 'H': val += 2; break;
      case 'S': val += 3; break;
      case 'N': val += 4; break;
    }}
    assert(val != 0);
    sprintf(command,"%u 0G",val);
    f_eval(command);
}

/* is this proper char for a convention? */
int is_fchar(char ch) {
    if (ch >= 'a' && ch <= 'z') return 1;
    if (ch >= 'A' && ch <= 'Z') return 1;
    if (ch >= '0' && ch <= '9') return 1;
    if (ch == '_') return 1;
    else return 0;
}
/* evaluate a string as forth commands
A-Z are forth executable words
a-z are grabs that are pushed onto the stack
[0-9]+ are literal numeric constants that are pushed onto the stack
*/

int f_stack_size(void) { return fstack_index;}
int f_stack_item(int item) {
    if (item < fstack_index) return fstack[item];
    else return -1;
    }


#ifndef IOS
EXTERN_HOFFER_C int f_eval(const char *cmds) {
#else
int f_eval(const char *cmds) {
#endif
    int i,val,j,in_num=0, in_str=0;
    int string_count=0;
    fstack_index = 0;

    if (cmds == NULL) return -1;
    for (i=0; cmds[i] != '\0'; i++) {
		if (cmds[i] == '_') {
			}
        if (in_str && is_fchar(cmds[i])) { // walk through; is_fchar checks if proper char for conventions
            fstr[string_count][in_str-1] = cmds[i];
            in_str++;
            continue;
            }
        else if (in_str) { // string has been completely copied
            fstr[string_count++][in_str]='\0'; // increment string_count and finish this string
            in_str = 0;
            continue;
            } 

        if (cmds[i] >= '0' && cmds[i] <= '9') {
            if (!in_num) { in_num = 1; val = atoi(&cmds[i]); f_push(val); continue;}
            }
        else in_num = 0;
        
        if (islower(cmds[i])) { f_push(grab[cmds[i] - 'a'].c); continue; }
        switch (cmds[i]) {
        case '$' : in_str = 1; break;
        case '!' : f_invert(); break;
        case ':' : f_rot(); break;
        case 'A' : f_less_than(); break;
        case 'B' : case 'C' : 
        case 'D' : case 'E' : case 'F' :f_push(fval[cmds[i]-'A']); break;
        case 'G' : 
            f_setval(); 
            CSW_EXEC(BidMessage(cmds)); break;
        case 'H' : f_getval(); break;
        case 'I' : f_fromchar(); break;
        case 'J' : f_getoppval(); break;
        case 'K' : f_add(); break;
        case 'L' : f_drop(); break;
        case 'M' : f_dup(); break;
        case 'N' : f_sub(); break;
        case 'O' : j=f_pop();
					myffs[j]();
					break;
        case 'P' : f_greater_than(); break;

        case 'Q' : 
				f_equal_to(); break;
        case 'R' : f_increment(); break;
        case 'S' : f_decrement(); break;
        case 'T' : f_equal_to_zero(); break;
        case 'U' : f_if(); break;
        case 'V' : f_max(); break;
        case 'W' : f_min(); break;
        case 'X' : f_or(); break;
        case 'Y' : f_and(); break;
        case 'Z' : f_tochar(); break;
        default: break;
        }
    }
	/* 180721 this was returning the bottom of the stack, which is
	OK if only one item was being left on the stack but otherwise an error */
	if (fstack_index<=0) return 999; // unlikely value
	else return fstack[fstack_index-1]; // top of the stack
}


void f_set(int pos, int val) {
    if (pos > 7 || pos < 0) return;
    fval[pos] = val;
}


extern "C" char *cpp_hoffer_bid(const char *,const char *,int,int);

extern "C" void hoffer_bid_test(void) {
    // cpp_hoffer_bid((const char *)"ak654.6543.5.ak5",(const char *)"P",0,0);
}

// extern "C" char *cpp_hoffer_bid(const char *seq,const char *hand, int vul, int dealer) {
//     return "";
// }


#ifdef STATE_INFO
char get_node_c(RXNODE *np) {
#ifndef TEST
    char ch;
#endif
    if (np->type == RXNT_C) return np->c;
    else if (np->type == RXNT_POP && np->flag == 0) {
        return grab[(int) np->c].c;
        }
    else if (np->type == RXNT_POP && np->flag > 1) {
      //  fbidder = vsa[bidding_depth].current_bidder % 2;
	   sprintf(forth_string,"%2u 67G",bidding_depth);
        f_eval(forth_string); // set current depth into forth variable for introspecition of prior bids

        sprintf(forth_string,"%2u 69G",vsa[bidding_depth].current_bidder);
        f_eval(forth_string); // set current bidder into forth variable
#ifdef CSW
        CSW_EXEC(BidMessage(np->str));
#endif
        return f_eval(np->str); // evaluate forth string
    }
#ifndef TEST
    else if (np->type == RXNT_POP && np->flag == 1) {
        switch(np->c) {
            case 0: ch = vsa[bidding_depth].current_bidder + '0'; break;
            case 1: ch = (vsa[bidding_depth].current_bidder + 1) % 4 + '0'; break;
            case 2: ch = (vsa[bidding_depth].current_bidder + 2) % 4 + '0'; break;
            case 3: ch = (vsa[bidding_depth].current_bidder + 3) % 4 + '0'; break;
//			case 4: strncpy(temp,BiddingSystem->GetCurrentContractStr(),1);
//						ch = temp[0]; break;
//			case 5: strncpy(temp,BiddingSystem->GetCurrentContractStr(),2);
//						ch = temp[1]; break;
//			case 6: ch = BiddingSystem->GetDoubledStatus() + '0'; break;


            default: return 0;
            }
        return ch;
        }
#endif
    return 0;
    }
#endif




void testTarget(void);
int  count_grabs  (RXNODE *np, int count);

struct testParameters {
    const char *regexp;
    const char *test;
    int answer;
    int position;
    int length;
    const char *comment;
    };

// #define LIMITED_TEST
#define SINGLE

struct testParameters testCase[] =
{
#ifndef LIMITED_TEST
{"[a-c]","a",1,0,1,"true case brackets"},
{"[a-c]","d",0,0,0,"failing case brackets"},
{"[a-c][A-Z]","aZ",1,0,2,"true case brackets"},
{"[a-c][A-Z]","dZ",0,0,0,"failing case brackets"},
{"[a-c][A-Z]","ad",0,0,0,"failing case brackets"},
{"[a-z]*[A-Z]*","abcdABCD",1,0,8,"true case brackets with star"},
{"^a$","a",1,0,1,"true start and end metacharacters"},
{"^a","b",0,0,0,"failing case start"},
{"^a","a",1,0,1,"true case start"},
{"a$","a",1,0,1,"true case end"},
{"a$","b",0,0,0,"failing case end"},
{"test","test",1,0,4,"true string"},
{"test","atest",1,1,4,"true string"},
{"^test","atest",0,0,0,"false string"},
{"t(a|b)t","tat",1,0,3,"true or"},
{"t(a|b)t","tct",0,0,0,"false or"},
{"t(a)+t","xtaaat",1,1,5,"true plus"},
{"t(a)+t","xtat",1,1,3,"true plus"},
{"t(a)+t","tt",0,0,0,"false plus"},
{"t.*t","taat",1,0,4,"true star"},
{"t.*t","tt",1,0,2,"true star"},
{"t.*","taa",1,0,3,"true star"},
{"t.*","tt",1,0,2,"true star"},
{"t.*","t",1,0,1,"true star"},
{"t.*x","tt",0,0,0,"false star"},
#ifndef SINGLE
{"{ab}#a","abab",1,0,4,"true grabs"},
{"{ab}{cd}#a#b","abcdabcd",1,0,8,"true multiple grabs"},
{".*{ab}#a","ddabab",1,0,6,"true grabs preceding star"},
#endif
{"{1{C:P:[+#a][>#b]","1C:P:2H",1,0,7,"manipulating grabs"},
{"{1{C<#b==C>","1C",1,0,2,"equal function tested"},
{"{1{C<#b!=D>","1C",1,0,2,"not equal function tested"},
{"{1{C<#b=D>:P:2#b","1C:P:2D",1,0,7,"Set function tested"},
{"({ab|a{b)#a","aba",1,0,3,"Disjunction OK"},
{"({ab|a{b)#a","abb",0,0,0,"Disjunction - Note: fails because 1st grab prevails"},
{"({ab#a|a{b#a)","abb",1,0,3,"Disjunction - fixed"},
{"({ab|a{ba)#a","abab",1,0,4,"Disjunction OK"},
{"({aba|a{ba)#a","abab",0,0,0,"Disjunction - Note: see above"},
{"({aba#a|a{ba#a)","abab",1,0,4,"Disjunction fixed"},
#endif
{"a&&b","aab",1,0,3,"Also"},
{"a&&b","aabb",1,0,3,"Also"},
{"a&&b","aaabb",1,0,4,"Also"},
{"end","",0,0,0,""}
};
void set_lex(const char *str) {
    rx_root = NULL; set_grab_index(-1);
    reset_lex(str);
    printf("--> yyparse");
    yyparse();
    printf("<-- yyparse");
    reset_closures(rx_root);
    count_grabs(rx_root,0);
}

/* set lex above and then call this with strings to match */

int test_rx_match(const char *pattern, const char *str) {
       // printf("%s %s\n",pattern,str);
        set_lex(pattern);
        rx_stack_index=EMPTY;
        rx_match_pos = rx_match_len = NO_MATCH_POSITION;
        begin_flag = ON;
        rx_match(rx_root,str);
     //   printf("Len:%u\n",rx_match_len);
        return rx_match_len;
        }


void testTarget(void)
{
    int i,j, problem;
    char msg[100];

#ifndef YYBISON
    setup_io();
#endif

    for (i=0; strcmp(testCase[i].regexp,"end") != 0; i++) {
        rx_root = NULL; set_grab_index(-1);
        reset_lex(testCase[i].regexp);
//		yylex();
        printf("---> yyparse");
        yyparse();
        printf("<--- yyparse");
#ifndef CHAR_ONLY
        reset_gi();
#endif
        reset_closures(rx_root);
        count_grabs(rx_root,0);
/* 		print_node(rx_root,0);       */
        rx_stack_index=EMPTY;
        rx_match_pos = rx_match_len = NO_MATCH_POSITION;
        begin_flag = ON;

#ifndef HOFFER
        _flushall();
#endif
        sprintf(msg,"%s <%s> : %s",testCase[i].regexp,testCase[i].test,testCase[i].comment);

        printf("%s\n",msg);

        problem = 0;
        if ((j=rx_match(rx_root,testCase[i].test)) != testCase[i].answer)
            problem=1;
        if (testCase[i].answer != 0 && rx_match_pos != testCase[i].position)
            problem=2;
        if (testCase[i].answer != 0 && rx_match_len != testCase[i].length)
            problem=3;

        sprintf(msg,"ans:%i tpos:%i tlen:%i mpos:%i mlen:%i",j,testCase[i].position,testCase[i].length,rx_match_pos,rx_match_len);
        printf("%s",msg);
        if (problem > 0) printf(" -- Problem %u",problem);
        printf("\n");
#ifndef HOFFER
        _flushall();
#endif
        }

}





#undef ADD_BIDMESSAGE
#ifdef ADD_BIDMESSAGE
int BidMessage(char *temp,...) {
    printf("%s",temp);
    return 1;
    }
#endif


void set_grabs(BIDNODE  *tree, int count, int index)
{
    if (tree == NULL) return;
#ifndef CHAR_ONLY
    gi_index = index;
#endif

    if ((tree->num_of_grabs & 127) == 127 && tree->pattern != NULL) {
#ifdef CSW
        CSW_EXEC(BidMessage("\nGoing to count_grabs: pattern is %s",tree->exp.c_str()));
#endif
        tree->num_of_grabs = (tree->num_of_grabs & 128) + count_grabs(tree->pattern,(count & 127));
        if ((tree->num_of_grabs & 127) < 0) {
            BidMessage("\nbad count was with exp of %s",tree->exp.c_str());
            BidMessage("\nbad count was with comment of %u",tree->comment);
#ifdef PRINT
            CSW_EXEC(print_node(tree->pattern,0));
#endif
            }
        }

#ifndef CHAR_ONLY
    if (tree->child) set_grabs(tree->child,tree->num_of_grabs,gi_index);
#else
    if (tree->child) set_grabs(tree->child,tree->num_of_grabs,index);
#endif
    if (tree->sibling) {
        if (tree->parent == NULL) {
#ifndef CHAR_ONLY
            reset_gi();
#endif
            count = tree->num_of_grabs;
            }
        set_grabs(tree->sibling,count,index);
        }
}	


/* The tree that contains the bidding system must necessarily
know how many grabs are already in use when a top section of the
tree is called. This grab information is part of the innate bidding
system and is included in its description. The top level bids all
have a count associated with them which is incorporated into the tree
that is built. For example:
    "FSF;8" shows that there are already 8 grabs in effect when this
            section is called. This is added to the pattern to keep
            track of the grabs and therefore a pattern such as
            {2{N would put 2 as grab#9 and N as grab#10

Count grabs does more than just count the grabs that are going to
be used in each section. It also puts the value for each grab
into its np->c value and thus is available for processing without
doing any calculations.
*/






int  count_grabs  (RXNODE *np, int count)
{
    char msg[40];
  int   i,j;
  if (np == (RXNODE *) 0) return count;
  if (count < 0) return count;

#ifdef CSW
  CSW_EXEC(BidMessage("\nCountGrabs type is %u",np->type));
#endif
  switch (np->type)  {
    case RXNT_RX:            /* Must match both sub-nodes. */
        i=count_grabs (np->lnode,count);
        j=count_grabs (np->rnode,i);
        return  j;

    case RXNT_EVAL:
        i=count_grabs(np->lnode,count);
        return i;

    case RXNT_NE:
        i=count_grabs(np->lnode,count);
        j=count_grabs(np->rnode,i);
        return j;

    case RXNT_GE:
    case RXNT_LE:
    case RXNT_EQ:
    case RXNT_GT:
    case RXNT_LT:
        i=count_grabs(np->lnode,count);
        j=count_grabs(np->rnode,i);
        return j;

    case RXNT_INC:
        i=count_grabs(np->lnode,count);
        j=count_grabs(np->rnode,i);
        return j;

    case RXNT_SET:
        i=count_grabs(np->lnode,count);
        j=count_grabs(np->rnode,i);
        return j;


    case RXNT_STRING:
        i=count_grabs(np->lnode, count);
        return i;

    case RXNT_MATH:
        return count;

    case RXNT_AND:
        i=count_grabs (np->lnode, count);
        j=count_grabs (np->rnode, count);
        return  i;

    case RXNT_ALSO:
        i=count_grabs (np->lnode, count);
        j=count_grabs (np->rnode, i);
        return  j;

    case RXNT_STR:
        i=count_grabs (np->lnode, count);
        return i;

    case RXNT_ALT:
        i=count_grabs (np->lnode, count);
        j=count_grabs (np->rnode, count);
        return  j;

    case RXNT_TERM_LIST:     /* Must match both subnodes. */
        i=count_grabs (np->lnode, count);
        j=count_grabs (np->rnode, i);
        return j;

    case RXNT_PLUS:     /* Must match 1 or more occurance(s) of left-node. */
        i=count_grabs (np->lnode, count);
        return i;

    case RXNT_STAR:     /* Match 0 or more occurance(s) of left-node. */
        i=count_grabs (np->lnode, count);
        return i;

    case RXNT_BIN:     /* Must match 0 or 1 occurance of left-node. */
        i=count_grabs (np->lnode, count);
        return  i;

    case RXNT_RANGE:         /* Must be in range, inclusive. */
        return count;

    case RXNT_CLASS:         /* Must match left-node. */
        i=count_grabs(np->lnode, count);
        return  i;

    case RXNT_CLASS_LIST:    /* Must match either subnode. */
        i=count_grabs (np->lnode, count);
        j=count_grabs (np->rnode, i);
        return j;

    case RXNT_NOT_CLASS:     /* Must not match left-node. */
        i=count_grabs (np->lnode, count);
        return  i;

    case RXNT_SPAN_0:
        i=count_grabs(np->lnode, count);
        j=count_grabs(np->rnode, i);
        return j;

    case RXNT_SPAN_1:
        i=count_grabs (np->lnode, count);
        j=count_grabs (np->rnode, i);
        return j;

    case RXNT_SPAN_0_TO:
        i=count_grabs(np->rnode, count);
        return i;

    case RXNT_SPAN_1_TO:
        i=count_grabs (np->rnode, count);
        return i;

    case RXNT_SPAN_0_FROM:
        i=count_grabs(np->lnode, count);
        return i;

    case RXNT_SPAN_1_FROM:
        i=count_grabs (np->lnode, count);
        return i;

    case RXNT_C:        /* Must match the character. */
        return count;

    case RXNT_ANY_C:     /* Matches anything except end-of-string. */
        return  count;

    case RXNT_COMMA:
         return count;
    
    case RXNT_FORTH:
         return count;
         
    case RXNT_PUSH:
        if (count < 0)
            return count;
        np->c = count + 1;
        if (np->c > 24)
            i=0;
#ifndef CHAR_ONLY
        push_gi(np->c);
#endif
        return np->c;

    case RXNT_POP:
    //	if (np->c - 1 > count)
    //		i=0;
    //	assert (np->c < count);
        return count;

#ifndef CHAR_ONLY
    case RXNT_RBRACE:
        np->c = pop_gi();
        assert (np->c >= 0 && np->c < 25);
        i=count_grabs (np->lnode, count);
        j=count_grabs (np->rnode, i);
        return  j;
#endif

    case RXNT_NOT_GREATER:
    case RXNT_GREATER:
        i=count_grabs(np->lnode, count);
        return i;

    case RXNT_NOT_LESSER:
    case RXNT_LESSER:
        i=count_grabs(np->lnode, count);
        return i;

    case RXNT_INCR:
        return count;
    case RXNT_NI:
        return count;

    case RXNT_SECTION:
        return count;

    case RXNT_DECR:
        return count;
    case RXNT_ND:
        return count;
    case RXNT_NOT:
        i = count_grabs(np->lnode,count);
        return i;

    case RXNT_KLEENE:
        i = count_grabs(np->lnode->lnode,count);
        i = count_grabs(np->lnode->rnode,i);
        i = count_grabs(np->rnode->lnode,i);
        i = count_grabs(np->rnode->rnode,i);
        return i;

    default:    /* "I don't think we're in Kansas anymore, Toto." */
        BidMessage("Kansas? Toto. #1");
        sprintf(msg,"Type is %u",np->type);
        BidMessage(msg);
        return -3;
  }
}



void fill_law_table(int tricks, int c1, int c2, int v1, int v2, int d1, int d2) {
int i,j,k;

for (i=0; i < 5; i++) {	               /* fill with number of tricks taken */
    law_table[i][0] = c1/10 + 4 + i;
    law_table[i][2] = tricks - law_table[i][0];
    }

switch (v1 * 2 + d1) {				   /* set up the two sets for us */
    case 0 : law_table[0][1] = offs[2].nv; break; 
    case 1 : law_table[0][1] = offs[2].nvd; break;
    case 2 : law_table[0][1] = offs[2].v; break;
    case 3 : law_table[0][1] = offs[2].vd; break;
    }
switch (v1 * 2 + d1) {				   /* set up the two sets for us */
    case 0 : law_table[1][1] = offs[1].nv; break; 
    case 1 : law_table[1][1] = offs[1].nvd; break;
    case 2 : law_table[1][1] = offs[1].v; break;
    case 3 : law_table[1][1] = offs[1].vd; break;
    }


for (j=2; j<5; j++) {				 /* set up the 3 makes for us */
for (i=0; i < 140; i++) {
  if (values[i].contract == c1 && ((c1 / 10) + (j - 2)) == values[i].made) {
    switch (v1 * 2 + d1) {				   
        case 0 : law_table[j][1] = values[i].nv;  break; 
        case 1 : law_table[j][1] = values[i].nvd; break;
        case 2 : law_table[j][1] = values[i].v;   break;
        case 3 : law_table[j][1] = values[i].vd;  break;
        }
      }
    }
}

k=0;	 /* fill up the opponents */
for (j=0; j<5; j++) {
    if (((c2/10)+6)	> law_table[j][2]) {
        switch((v2 * 2) + d2) {
          case 0: law_table[j][3] = offs[((c2/10)+6) - law_table[j][2]].nv;  break;
          case 1: law_table[j][3] = offs[((c2/10)+6) - law_table[j][2]].nvd; break;
          case 2: law_table[j][3] = offs[((c2/10)+6) - law_table[j][2]].v;   break;
          case 3: law_table[j][3] = offs[((c2/10)+6) - law_table[j][2]].vd;  break;
          }
        }
    else {
      for (i=0; i< 140; i++) {
         if (values[i].contract == c2 && (law_table[j][2]-6) == values[i].made) {
            switch ((v2 * 2) + d2) {				   
                case 0 : law_table[j][3] = values[i].nv;  break; 
                case 1 : law_table[j][3] = values[i].nvd; break;
                case 2 : law_table[j][3] = values[i].v;   break;
                case 3 : law_table[j][3] = values[i].vd;  break;
                }
            }
         }
      k++;
      }
    }
}

int check_law_table(void) {
int i,result=0;
for (i=0; i<5; i++)
    if (law_table[i][1] > -law_table[i][3]) result++;
return result;
}

