/ This takes a file as input to hool library and processes it to get the info from the hands for a bidder
/ The file is hand.txt and consists of lines of 5 ints which are commands for the hool program.
/ The file contains the hand information of all 4 of the hands.
/ The command line input is the direction of the hand that will be doing the bidding
/ This mscript file reads the information, selects the lines that are other than the current bidder and sends that info to another file
/ That file will be read in by the library and processed with output, which is then analyzed.
> "The number of arguments is:" + arguments.length()
> "The arguments are:" + arguments
? arguments.length() != 1
    > "command line argument of the direction that will be analyzing is needed (0-3)"
    * exit(1)
    }
$ dir = number(arguments.get(0))
? dir < 0 || dir > 3
    > "command line argument must be 0-3 in value"
    * exit(1)
    }
> "Arguments are correct"
/ $ command = "type temp.txt | fgrep HOOL | sed s/HOOL// > hand.in"
$ command = "type temp.txt"
> "command:" + command
$ result = exec(command)
& command = "copy c:\users\rod.LAPTOP-V8D9A4GN\source\repos\hello\Debug\hello.exe hool.exe" 
& result = exec(command)
> "hool.exe obtained"

$ hand_info = readFile("hand.in","ascii")
$ lines = split(hand_info,lf)
$ ints = list()
$ new_ints = "0,99,1,0,0" + crlf + "0,99,0,0,0" + crlf + "0,58,0,0,0" + crlf + "0,0,0,15,15" + crlf
& new_ints = new_ints + "0,7,1,0,0" + crlf
& new_ints = new_ints + "0,1,3,4,4" + crlf + "0,7,3,0,0" + crlf
$ start = 3
$ count = 0
$ second = 0
$ first = 0
> "processing lines"
@ line : lines
    > "line:" + line
    & ints = split(line,",")
    ? ints.length() < 5
        > "breaking"
        V
        }
    & first = number(ints.get(0))
    & second = number(ints.get(1))
    ? first = dir && second = 9
        & count = count + 1 
        & new_ints = new_ints + line + crlf
        }
    }
> "afterwards 1"
/ not even needed with 58 entered above & new_ints = new_ints + "0,3,0," + start + "," + (count-1)  + crlf
/ 89 is ID_PRINT_CONSTRAINTS
& new_ints = new_ints + "0,89,0,0,0" + crlf
/ 98 is ID_DEAL and 4 is for south only to be dealt
& new_ints = new_ints + "0,98,4,0,100" + crlf
/ 94 is ID_PRINT_HANDS
& new_ints = new_ints + "0,94,0,0,100" + crlf
* writeFile("hand.out",new_ints,"ascii")
& command = "hool hand.out"
> "not calling hool"
/ & result = exec(command)
> "getting shows"
$ commands = new_ints + "0,70,0,0,0" + crlf  + "1,70,0,0,0" + crlf  + "2,70,0,0,0" + crlf  + "3,70,0,0,0" + crlf 
& commands = commands + "0,71,0,0,0" + crlf  + "1,71,0,0,0" + crlf  + "2,71,0,0,0" + crlf  + "3,71,0,0,0" + crlf 
* writeFile("info.out",commands,"ascii")
& command = "hool info.out"
> "calling info:hool info.out"
& result = exec(command)
> "info result is"
> result
& count = 0
$ info_index = 0
$ suit_index = 0
> "looping output:"
> result.get("output")
@ line : split(result.get("output"),crlf)
    > line
    V
    > line
    $ info = split(line,":")
    $ info_length = info.length()
    ? info_length > 1
        $ fp = split(info.get(0),",")
        > "fp" + fp
        $ len = fp.length()
        ? len > 1
            & count = count + 1
            > count + " " + line
            }
        <>
            suit_length = fp.get(0)
            suit_value = fp.get(1)
            ? suit_value = "C"
                & suit_index = 0
                }
            ? suit_value = "D"
                & suit_index = 1
                }
            ? suit_value = "H"
                & suit_index = 2
                }
            <>
                & suit_index = 3
                }
            > (info_index % 4) + ",2," + suit_index + "," + suit_length + "," + suit_length 
            }
        }
    & info_index = info_index + 1
    }
> "done"
