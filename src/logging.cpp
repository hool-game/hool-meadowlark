/* Versions
210209 No changes from windwos version
*/
#ifndef __LOGGING
#define __LOGGING
#include "meadowlark.h"
#undef GLOBALS
#include "../include/GLOBALS.H"

#ifndef DIVORCE


#ifdef ANDROID
#define USE_ALFT
#ifdef USE_ALFT
#include <android/log.h>
#define  LOG_TAG    "testjni"
#define  ALOG(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#endif
#endif



// #define LOGGING_IMPORT __declspec(dllimport)
#define LOGGING_IMPORT extern
void enter(void);
void setup_logging(int,char *);
#ifndef NO_LOGGING
void do_log(int,int,char *);
#endif
void set_level(int);
#ifdef MY_OLD_LOGGING
LOGGING_IMPORT int createFormatter(char *);
LOGGING_IMPORT int createFileHandler(char *);
LOGGING_IMPORT int createStreamHandler(void);
LOGGING_IMPORT int setFormatter(int,int);
LOGGING_IMPORT int createLogger(char *);
LOGGING_IMPORT int addHandler(int,int);
LOGGING_IMPORT int setLevel(int,int);
LOGGING_IMPORT int callLogger(int,int,char *);
LOGGING_IMPORT int setPropagate(int,int);
#endif

#define LOGGING

#ifdef LOGGING
#ifdef MY_OLD_LOGGING
static int gen_logs[10];
#endif
#define SETUP_LOG 0
#define GEN32_LOG 1

#define ID_DEBUG 10
#define ID_INFO 20
#define ID_WARN 30
#define ID_ERROR 40
#define ID_CRITICAL 50
int logging_initialized=0;

void enter(void) {
//	int i;
//	i=1;
}

#ifndef WIN_ONLY
void set_logging_level(unsigned int);
void setup_logging(unsigned int level, char *name) {
//	char temp[255];
//	int formatter, handler1, setup_log, gen32_log;

	if (logging_initialized) {
        set_logging_level(level);
		return;
		}

#ifdef MY_OLD_LOGGING
	// formatter = createFormatter("%(name)-5s %(levelname)-5s - %(message)s");
	formatter = createFormatter("%(message)s");
    
    sprintf(temp,"%s.log",name);
    handler1 = createFileHandler(temp);
    setFormatter(handler1,formatter);
    setup_log = createLogger("GEN32_SETUP");
    gen32_log = createLogger("GEN32_TEST"),
	gen_logs[SETUP_LOG] = setup_log;
	gen_logs[GEN32_LOG] = gen32_log;
 //   addHandler(setup_log,handler1);
 //   addHandler(patty_log,handler1);
    setLevel(gen32_log,level);
    setLevel(setup_log,level);
    setPropagate(gen32_log,0);
    setPropagate(setup_log,0);
    sprintf(temp,"gen32 dll logging is at level %u",level);
	logging_initialized = 1;
	gen32_do_log(SETUP_LOG,ID_DEBUG,temp);
    gen32_do_log(GEN32_LOG,ID_DEBUG,"also setup");
#endif
}

unsigned int logging_level = 40;
unsigned int get_logging_level(void) { return logging_level; }
void set_logging_level(unsigned int level) {
	logging_level = level;
#ifdef MY_OLD_LOGGING
	setLevel(gen_logs[GEN32_LOG],level);
	setLevel(gen_logs[SETUP_LOG],level);
#endif
}


#ifdef MY_OLD_LOGGING

#pragma message("gen32_do_log being compiled")

int gen32_do_log(int logger,int value, const char *msg, ...) {
    char message[240];
	if (!logging_initialized) setup_logging(40,"ab1");
	callLogger(gen_logs[logger],value,msg);
    strcpy(message,"gen32_do_log:");
    strcat(message,msg);
    ALOG(message);
}
#endif
#else
void setup_logging(int level, char *name) {}
#endif
#endif
#endif
#endif

