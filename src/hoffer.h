#if !defined(__HOFFER)
#define __HOFFER

#ifndef BID20_DEBUGGING
extern int bid20_debugging;
#endif

#if defined(ANDROID) || defined(__ANDROID__)
#include "prefix.pch"  // since android does not allow pre-compiled headers
#else
#if !defined(GODOT)
// a mechanism to determine the operating system for IOS
// #include <TargetConditionals.h>
#endif
#if TARGET_OS_IOS
#define IOS
#define HOFFER
#define ORIGINAL_HOFFER_CONSTRAINTS
#else
#define OSX
#define HOFFER
#endif
#endif

#if defined(LINUX)
// #pragma message("LINUX is defined..TargetConditionals is not needed")
#endif

#undef TWEAKING



#endif
