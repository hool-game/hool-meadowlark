
#ifndef CSWITCH_HPP
#define CSWITCH_HPP

#ifdef NO_CSW
#undef CSW
#pragma message("CSW is not being used and macros set in pre-compiled header")
#define CSW_EXEC(x)  ((void)0)
#define TRIGGER(x,y) ((void)0)
#define ONE_SHOT(x,y) ((void)0)
#define SINGLETON(x,y) ((void)0)
#define STATE(x,y) ((void)0)
#define SET(x) ((void)0)
#define CPP_DO_STATE(x) ((void)0)
#define NORMAL_DO_STATE(x) ((void)0)
#define CORRECT_CSWITCH_H
#else
#define CSW
// #pragma message("CSW is being used and macros set in pre-compiled header")
#define CSW_EXEC(x)  (csw_ison(__FILE__,__LINE__) ? (x) : (0))
#define TRIGGER(x,y) (trigger(x) ? (y) : (0))
#define ONE_SHOT(x,y) (trigger(x) ? (y) : (0))
#define SINGLETON(x,y) (trigger(x) ? (y) : (0))
#define CHECK_STATE(x,y) (check_state(x) ? (y) : (0))
#define CPP_DO_STATE(x) cpp_do_state(x)
#define NORMAL_DO_STATE(x) do_state(x)
#define CORRECT_CSWITCH_H

#ifdef __cplusplus
extern "C" {
#endif

int csw_ison(const char *, int);
int trigger(int);
int state(int);
void getargs(int, char **);
int check_one_shots(int,int);
int getargs2(const char *);
int set_trigger(const char *);
int set_one_shot(const char *);
int set_singleton(const char *);
void clear_csw(void);
void pop_csw(void);
void clear_triggers(void);
void push_triggers(void);
void pop_triggers(void);
void print_triggers(void);
void do_state(unsigned int);
void cpp_do_state(unsigned int);
unsigned int get_state(void);
unsigned int check_state(unsigned int);

#ifdef __cplusplus
}
#endif

#endif
#endif


