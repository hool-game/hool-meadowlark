/* Versions
210209 No changes from windows
*/

#include "meadowlark.h"
#undef GLOBALS
#include "../include/GLOBALS.H"

// SORT AN ARRAY THAT REPRESENTS A DECK OF CARDS. INPUT: deck - the array of
// cards (int) to sort first - the index to the first element number - total
// number of elements in the array to sort. direction - up or down. Since the
// total range of elements is 0..51 it is quicker to do the sort by
// determining which of the 52 elements is present and then running through a
// temporary array from a specified direction. Each element found is then
// placed in the original array in sorted order. The temporary array is always
// kept at all zeroes.

void sort(int  *deck, int first, int number, int direction)
{
	int i,j,storage[DECKSIZE];

	for (i=0; i<DECKSIZE; i++) storage[i]=0;
	for (i=first; i < first + number; i++)
		storage[deck[i]]=1;
	j=first;
	if (direction==UP)	{
		for (i=0; i<DECKSIZE; i++)
			if (storage[i] != 0) deck[j++]=i;
		}
	else	{
		for (i=DECKSIZE - 1; i>=0; i--)
			if (storage[i] != 0) deck[j++]=i;
		}
}


#ifndef DIVORCE
#ifndef WIN32
// #include "hoffer.h"
#endif



#ifdef __DLL__
int errorcheck(int	*deck)
#else
int errorcheck(int	*deck)
#endif
{
		int i,temp[DECKSIZE];

		for (i=0; i<DECKSIZE; i++) temp[i]=0;
		for (i=0; i<DECKSIZE; i++) {
				if (deck[i] < 0 || deck[i] >= DECKSIZE) return(ERR);
				if (temp[deck[i]] == 1) return(ERR);
				else temp[deck[i]] =1;
				}
		for (i=0; i<DECKSIZE; i++)
				if (temp[i] != 1) return(ERR);
		return(OK);
}


#if !defined(GODOT)
#ifndef PRODUCTION_VERSION
void force_overflow(void) {
	char a[20000];

	a[0]=0;
	}
#endif
#endif
#endif
