


#include "bid20.h"
 	#include <stdio.h>
#include <stdarg.h>
	#include <stdlib.h>
	#include <malloc.h>
	#include "typedef.hpp"

#ifdef HOFFER
#pragma message("RXTRNL defined in yymain.cpp")
#define RXTRNL
#include "rx.hpp"
#else
#pragma message("RXTRNL not defined in yymain.cpp")
#endif

#include "rx.hpp"
	#include "string.h"
	#include "ctype.h"

#include "header.hpp"

#include "hoffer.h"
#include "windows.h"
#include "assert.h"
#include "bid20.h"
#include "non_exportables.h"
#include "node.hpp"
#include <iostream>
using namespace std;

#define YYBISON
#ifdef YYBISON
#define YACC
#endif

#define NO_MEM "No memory for malloc of RXNODE"
#define STR_LEN "String too long"
#define BAD_PARSE "Bad parse"
#define NODE_OVERFLOW "Node overflow"
#define BAD_TYPE "Bad type in rxnode"
#define BAD_ALLOC_RXNODE "Can't allocate for rxnode"
#define RX_DEL_ERR "Internal error in rxnode delete"
#define VOIDCAST void *

const int MAX_INBUFFER_SIZE = 255;
char myinput[MAX_INBUFFER_SIZE];
char *myinputptr;
char *myinputlim;

#ifdef __cplusplus
extern "C" {
	void prowin_msg(char *);
int yywrap(void);
	};
#endif

extern int yydebug;
void yy_init_occs();
void yy_init_lex(void);
int my_yyinput(char *, int);

/* The definition of YACC changes from Allen Hollub's occs to
the yacc/lex combination. This was done for version2 of the bidding
system because occs does not handle start states, which were added
by Ginsberg to the new pattern matching rules for version2 */


#ifndef YYBISON
void setup_io(void) {
	ii_io(_open,close,myread);
	setup_io_flag = 1;
	}

#else
extern void yyrestart(FILE *);

void set_yydebug(int);
void set_yydebug(int i) {
	yydebug = i;
}

void setup_io(void) {
	yyrestart((FILE *)NULL);
	setup_io_flag = 1;
}
int yywrap(void) { return 1; }
#endif
int io_verify(void) { return(setup_io_flag); }

char *get_myinputptr(void) {
	return myinputptr;
}

char *get_myinput(void) {
	return myinput;
}

// int yylex_destroy(void);

void reset_lex(const char *str)
{
    int n;
//	yylex_destroy();
	yy_init_lex();
#ifndef YACC
	if (strlen(str) < MAX_PATTERN_LEN) strcpy(pattern,str);
	else my_error(STR_LEN);
	read_flag=0;
//	ii_init();
//	ii_advance();
//	ii_pushback(1);
#else
    if ((n=(int)(strlen(str)+1)) < (int)MAX_INBUFFER_SIZE) ;
    else n = (int) MAX_INBUFFER_SIZE;
      
    
//    int n = __min((int)MAX_INBUFFER_SIZE,(int)(strlen(str)+1));
//	int n = __min((int)MAX_INBUFFER_SIZE,(int)(strlen(str)));
	if (n>0) {
		memcpy(myinput,str,n);
		myinputptr = myinput;
        myinputlim = myinput + n;
        }
#endif
}

#ifdef YACC
int my_yyinput(char *buf, int max_size) {
    int n;
    if (int(myinputlim - myinputptr) < max_size)
        n = int(myinputlim - myinputptr);
    else n = max_size;
 //   int n= __min(max_size,(int)(myinputlim-myinputptr));
	if (n>0) {
		memcpy(buf,myinputptr,n);
		myinputptr += n;
		}
	return n;
}
#endif

void show_lex(const char *input) {
    int i;
	reset_lex(input);
	while ((i=yylex())) printf(" %u",i);
    }


int domatch(const char *input, const char *str)
{   int i;

	reset_lex(input);

#ifdef LEX_DEBUG
	while (i=yylex()) BidMessage(" %u",i);
#else
	printf("-------> yyparse");
	if (yyparse() == -1) BidMessage(BAD_PARSE);
	printf("<------- yyparse");
#ifdef B1
#ifdef PRINT
	print_node(rx_root,0);
#endif
#endif
	i=rx_match(rx_root,str);
	if (rx_root != NULL) delete_rxnode(rx_root);
    rx_root = NULL;
	return i;
#endif
}

RXNODE *parse(const char *instr)
{
	strcpy(pattern,instr);
#ifndef YYBISON
	ii_io(_open,close,myread);
#endif
	printf("--------> yyparse");
	if (yyparse()== -1) {
		BidMessage(BAD_PARSE);
		BidMessage(instr);
		}
	printf("<-------- yyparse");
	return rx_root;
}
	

int domatch2(RXNODE *np, const char *str)
{
	return(rx_match(np,str));
}


RXNODE *make_rxnode (RX_SYM type, RXNODE *lnode, RXNODE *rnode)
{

#ifdef ORIGINAL
  if (++rxnp >= rxnp_max)  {        /* Alloc area for a new node */
	BidMessage (NODE_OVERFLOW);
	exit (4);
  }
#else
  RXNODE *rxnp;

#ifndef HOFFER
  if ((rxnp = (RXNODE *) _malloc_dbg(sizeof(RXNODE), _NORMAL_BLOCK, __FILE__, __LINE__ )) == NULL) {
#else
  rxnp = (RXNODE *) malloc(sizeof(RXNODE));
  if (rxnp == NULL) {
#endif
	my_error(BAD_ALLOC_RXNODE);
	}
#endif

#ifndef VERSION2
  if (type < RXNT_ROOT || type > RXNT_TOP)
#else
  if (type < RXNT_ROOT || type > RXNT_SUIT)
#endif
	{ int i; BidMessage(BAD_TYPE); i=RXNT_TOP; fprintf(stderr,"%u %u\n",i,type); }
  rxnp->type = type;
  rxnp->lnode = lnode;
  rxnp->rnode = rnode;
  rxnp->str = NULL;
  rxnp->c = rxnp->flag = 0;
#ifdef RX_DEBUGGING
  rxnp->test = 0;
#endif
  return (rxnp);
}

char esc_val(char c)
{
	switch (c) {
		case 'n': return('\n');
		case 'r': return('\r');
		case 't': return('\t');
		case 'b': return('\b');
		case 'f': return('\f');
		case '\'': return('\'');
		case '\\': return('\\');
		default: return(c);
		}
}


void delete_rxnode(RXNODE *node)
{
	void *tempstr; 
	if (node == NULL) return;
	if (node->lnode) delete_rxnode(node->lnode);
	if (node->rnode) delete_rxnode(node->rnode);
#ifdef RX_DEBUGGING
	if (node->test != 0)
    	BidMessage(RX_DEL_ERR);
#endif
    tempstr = (void *) node->str;
	if (tempstr != NULL)
		free(tempstr);
	free(node);
}

void delete_str_from_rxnode(RXNODE *node)
{	
	void *tempstr;

	if (node == NULL) return;
	if (node->lnode) delete_str_from_rxnode(node->lnode);
	if (node->rnode) delete_str_from_rxnode(node->rnode);
    tempstr = (void *) node->str;
	if (tempstr != NULL)
		free(tempstr);
}


int myread(int fd,char *buf,int count) {

	if (read_flag==0) {
		if (strlen(pattern) < MAX_PATTERN_LEN) strcpy(buf,pattern);
		read_flag=1;
		return((int)strlen(pattern));
		}
	else return 0;
}

#ifdef NEVER
#pragma argsused
int myopen(const char *name,int flags) {
return 0;
}
#pragma argsused
int myclose(int fd) {
return 0;
}
#define MESSAGES
#endif

void my_error(const char *str) {
#ifndef WINDOWS

	if (current != NULL) {
		BidMessage("\nThis is the offending line:");
		BidMessage("\n%s",current->constraint.c_str());
		BidMessage("\n%s",current->exp.c_str());
#ifdef PRINT
		if (which==0) print_node(current->pattern,0);
        else print_node(current->con_pat,0);
#endif
		}
#endif

// #ifdef WINDOWS
//	if (current != NULL) MessageBox(0,str,current->exp,0);
//	else MessageBox(0,str,NULL,0);
// #else
//	BidMessage("\nERR: %s",str);
// #endif
}


// extern void python_log(char *);
void BidMessage2(const char *format) {
	// char *result=NULL;
	BidMessage(format);
	// result = hoffer_evaluate("1N:P:4H",0,0);
	// BidMessage(result);
	}

int BidMessage(const char *format,...) {
	char buffer[300];
	va_list argptr;
	int cnt;


	va_start(argptr, format);
	cnt = vsnprintf(buffer, 240, format, argptr);
	va_end(argptr);
	if (cnt == EOF) return cnt;
	if (cnt >= 239) buffer[0] = '#';
#define DO_NOT_USE_LOG
    
#ifdef DO_NOT_USE_LOG
    // printf(stderr,"BidMessage entered with %s\n",format);
    fprintf(stderr,buffer);
#else
//	python_log(buffer);
#ifdef HOFFER
	printf("%s",buffer); // switched to do_log 160202
#else
	do_log(1,10,buffer);
#endif
//	MessageBox(GetFocus(),buffer,"BidMessage",MB_TASKMODAL);
#endif
	return 1;
}

// vector<BIDNODE> V;
#ifdef MULTISYSTEMS
extern MatchTree *sys;
std::vector<SYSTEMNODE>& n = sys->vSystems;
#endif

// Note the initialization of rvs, a reference to the SYSTEMNODE
// BIDNODE::BIDNODE() : rvs(n) { id=0; parent = NULL; } 
// BIDNODE::BIDNODE(char *e, char *m, char *c, char *g, char *r) :rvs(n) {
//       exp =e; macro = m; constraint = c; generate_command = g; range = r;
//       parent = NULL; } 

BIDNODE::BIDNODE() { id=0; parent = NULL; con_pat1=NULL; con_pat2=NULL;} 
BIDNODE::BIDNODE(const char *e, const char *m, const char *c, const char *g, const char *r) {
       exp =e; macro = m; constraint = c; generate_command = g; range = r;
       parent = NULL; con_pat1=NULL; con_pat2 = NULL;} 
#ifdef MULTISYSTEMS
SYSTEMNODE::SYSTEMNODE() {
    lines.reserve(10000);
    }
#endif
void BIDNODE::set_id(int i) { id = i; }
#ifdef BNV1ZZ
void BIDNODE::set_constraint(const char *str) {
    constraint = str;
    setup_rxnode(con_pat,constraint);
    }
#else
void BIDNODE::set_constraint(const char *str) {
    String temp(str);
    constraint = str;
    setup_rxnode(con_pat,temp);
    }
#endif
void BIDNODE::set_constraint1(const char *str) {
    String temp(str);
    constraint1 = str;
    setup_rxnode(con_pat1,temp);
    }
void BIDNODE::set_constraint2(const char *str) {
    String temp(str);
    constraint2 = str;
    setup_rxnode(con_pat2,temp);
    }
void BIDNODE::setup_rxnode(RXNODE *r, String& str) {
    setup_rxnode(r,(const char *)str.c_str());
    }    

int BIDNODE::UpdateRange(const char *r) {
    range = r;
	return 1;
    }
    
void BIDNODE::setup_rxnode(RXNODE *r, const char *str) {
    int grab_count;
    delete_rxnode(r);
    rx_root = NULL;
	 if (str[0] != '\0') {
    	strcpy(macro_temp,str);
		reset_lex(str);
		// printf("-----> yyparse");
		if (yyparse() == PARSE_OK) {
			// printf("<----- yyparse");
			reset_closures(rx_root);
			grab_count = count_grabs(rx_root,num_of_grabs & 127);

			
			
			assert(grab_count >=0 && grab_count < 25);
			}
		else printf("\nerr is: %s",macro_temp);
		}
    r = rx_root;
	}      
	
void BIDNODE::clean_up(void) {
	if (child)   { child->clean_up(); child = NULL; }
	if (sibling) { sibling->clean_up(); sibling = NULL; }
	if (pattern) { delete_rxnode(pattern); pattern = NULL; }
	if (con_pat) { delete_rxnode(con_pat); con_pat = NULL; }
	if (con_pat1) { delete_rxnode(con_pat1); con_pat1 = NULL; }
	if (con_pat2) { delete_rxnode(con_pat2); con_pat2 = NULL; }
}

int do_yyparse(void);
int do_yyparse(void) {
	printf("------> yyparse");
	return yyparse();
	// printf("<------ yyparse");
}
void testNewBidNode(void) {
   /* int i;
    for (i=0; i< 20000; i++) {
      BIDNODE nbn;
      nbn.set_id(i);
      V.push_back(nbn);
      }
   vector<BIDNODE>::iterator it;

   for(it=V.begin(); it != V.end(); ++it) cout << (*it).id << " "; // print member
   cout << endl;  */
   cout << "deprecated" << "\n";      
   }

#ifdef HOFFER
//int count_grabs(RX_NODE* r, int i) { return 0; }
//void reset_closures(RX_NODE *r) { }
//int rx_match(RX_NODE *r, const char *c) {return 0; }
// BIDNODE *current;
// const char *macro_temp[300];
// const char *pattern[100];
// int read_flag=0;
// RXNODE *rx_root=NULL;
// int setup_io_flag = 0;
//extern "C" {
//static void yyunput (int c, char * yy_bp );
//}
// static void yyunput (int c, char * yy_bp ) {}
//void dummy(void) { yyunput(1,(char *)""); setup_io_flag = 0;} 
#endif
    

