{
  'targets': [
    {
      'target_name': 'hool-native',
      'sources': [ 'src/hool.cc','src/hello.cpp','src/xdeal.cpp','src/csw.cpp', 'src/newgen2.cpp', 'src/bidfuncs.cpp',
        'src/logging.cpp','src/unity_tests.cpp','src/io3.cpp','src/unity.cpp','src/utility.cpp'],
      'include_dirs': ["<!@(node -p \"require('node-addon-api').include\")"],
      'dependencies': ["<!(node -p \"require('node-addon-api').gyp\")"],
      'cflags!': [ '-fno-exceptions' ],
      'cflags_cc!': [ '-fno-exceptions' ],
      'xcode_settings': {
        'GCC_ENABLE_CPP_EXCEPTIONS': 'YES',
        'CLANG_CXX_LIBRARY': 'libc++',
        'MACOSX_DEPLOYMENT_TARGET': '10.7'
      },
      'libraries': ['<(module_root_dir)/src/libdds.so'],
      'msvs_settings': {
        'VCCLCompilerTool': { 'ExceptionHandling': 1 },
      }
    }
  ]
}
