Read the other markdown file "hool doc1 api.md" it has the
changes of the library from 20220521.


## API for interfacing HOOL Bot C API with JS
### How to Install

1. `npm install`
2. `yarn demo`

Note: Read demo/demo.js for detailed JS related notes.

To implement seperately, Import "../lib/binding.js" as a Class, initialize it with parameters of Rodney's library and call the play() method of that class.

If Rodney changes any file, replace `hello.h` and `hello.c` (Names kept same as Rodney sent) in the `src` folder. 

## Rodney's Readme:
# HOOL: Library interface. 

#### 1. A library will available for the the card game Hool. 
This is based on a bridge library. It will be available for Windows, IOS, OSX (so also Linux), and android operating systems.

It is written in C++ which also has an embedded prolog language (AMZI! prolog).

Original document date: 2021-1-26.

This proposal documents the interface to the libary.

The signature for all functions is: 5 integer input parameters returning a string using a C calling convention.

char *command(int who, int what, int where, int lo, int hi); 

#### 2. The integer parameters ####

1. **WHO** This is always the direction (0,1,2,3) for North, East, South or West.
2. **WHAT** This is the commmand: for example, 70 requests the first information sharing
3. **WHERE** This is usually a subcommand: for example, 0 on 70 is the basic information sharing system.
4. **LO** a value
5. **HI** a value

#### 3. The return value is always a string. ####

1. Information sharing. After the information all text after a colon is a comment.
	
- **HCPs**: return your dir, your hcps, and HCP. For example: "0 12 HCP" indicating North showing hcps of 12 points
- **Pattern**: return your dir, your distribution is descending order and PATTERN. For example: "2 5431 PATTERN" indicating South has a longest suit of 5 cards.
- **Spade length**: return your dir, your length in spades, and SPADES.
- **Heart length**: return your dir, your length in hearts, and HEARTS.
- **Diamond length**: return your dir, your length in diamonds, and DIAMONDS.
- **Club length**: return your dir, your length in clubs, and CLUBS.

2. Bidding: return your dir,  PASS, DOUBLE, REDOUBLE, 1H, 2N etc, then BID.
3. Playing: return your dir, 2C 7S etc., then PLAY.

Comments can be appended and used for debugging. For example, if you are using a bidding system which might show extra information such as opening count, you could return messages such as:

	
- 0 1H BID : showing 5+ hearts and opening count.
- 2 3H BID : showing 7+ hearts; pre-emptive; less than 10 high card points.

or for play of the hand

- 2 AS PLAY : ace from ace-king
- 0 2S PLAY : 4th best

#### 4. Examples ####

1. result = command(0,70,0,0,0) - requests the first information sharing item. The first parameter is North, the second is the request, the third parameter is the information sharing system. Parameters 4 and 5 can be used to qualify the system, but are unused at this time.

2. result = command(2,71,2,0,0) - requests the second information sharing item. The first parameter is 2 for South, the second is the request. The third parameter is the information sharing system.

3. result = command(2,72,contract,declarer,doubled) - requests a bid.
4. result = command(2,75,bid,0,0) - notification of a bid of another player.

4. result = command(2,9,card,0,0) - sends the information of a card. You receive your 13 cards from the server. Also, after the dummy is shown, you would receive the cards for the dummy. If the declarer is South and you are West, you would receive all of your cards with result = command(3,73, card,0,0) and the cards once revealed for dummy as result = command(1,73,card,0,0) for the dummy's cards in the North hand.

5. result = command(2,74,signaling,0,0) - requests a play where parameter 3 is the signaling system being used. The 3rd parameter would be 0 = no signals. 1 = standard count and attitude signals. 2 = upside down count and attitude signals. 4 = SIT signals (switch in time).
6. result = command(2,76,signaling,0,0) - notification of a play by another player.
2. command(0,86,contract,declarer,doubled) notification of contract, where contract is a integer of the level and the denomination such as 10 is 1C, 74 is 7N. declarer is 0,1,2,3. doubled is 0,1,2 for undoubled, doubled, redoubled. 

#### 5. Addenda ####

1. command(0,99,0,0,0) does a reset.
2. Requests of information can be made to the library:

- command(0,90,0,0,0) requests the library version.
- command(0,94,0,0,0) requests a string of the hands that have been generated.
- command(0,92,0,0,0) requests a string of the hands that are being used during the play.
- command(0,89,0,0,0) requests a string of the constraints being used.

#### 6. Version, status, and update information ####

2021-01-26 Original document.

2021-01-26 Received HOOL flutter code.

- Android Studio set up on Windows and IOS. XCode set up on OSX. Flutter: "flutter doctor" without problems on all environments, and "flutter run" brings up the IOS simulator without problems. XCode does not compile directly, only through flutter run.










