// Demo of using the api 
const Turn = require("../lib/binding.js");
// The Turn API has 5 parameters to be needed
// This turn API NEEDS to be called everytime there's a new move
// The 5 parameters are:
// The WHO: Who is acting in this turn
// The WHAT: What are they doing? (Getting Some cards, setting the bid, playing a card, sharing info,or retreiving the card they should play)
// The WHERE: A Subcommand to specify the details of what
// LO: A value
// HI: A value

const  SET_CARD = 73; // Code for what for setting the cards
const  SET_CONTRACT = 86; // Code for setting the contract
const  PLAYED = 76; // Code for playing
const  GET_PLAY = 74; // Code for getting what should be played next

// Info sharing round 1
let turn = new Turn(0,70,0,0,0); // You have to create this new turn with each change in state
console.log(turn.play()) // This will return a string with the appropriate result
// Info sharing round 2
turn = new Turn(0,71,0,0,0); 
console.log(turn.play())
// Info sharing revise?
turn = new Turn(0,72,0,0,0);
console.log(turn.play()) 
// Bidding
turn = new Turn(0,75,0,0,0); 
console.log(turn.play()) 
// Reset
turn = new Turn(0,99,0,0,0); 
console.log(turn.play()) 
// Distributing Cards 1-52 // Where here is the card distributed
turn = new Turn(3,SET_CARD,51,0,0); 
console.log(turn.play()) 

turn = new Turn(0,SET_CARD,41,0,0); 
console.log(turn.play()) 

turn = new Turn(1,SET_CARD,39,0,0); 
console.log(turn.play()) 

turn = new Turn(2,SET_CARD,40,0,0); 
console.log(turn.play()) 

turn = new Turn(3,SET_CARD,49,0,0); 
console.log(turn.play()) 


turn = new Turn(0,SET_CARD,45,0,0); 
console.log(turn.play()) 

turn = new Turn(1,SET_CARD,0,0,0); 
console.log(turn.play()) 
// Setting contract
turn = new Turn(0,SET_CONTRACT,14,3,0); 
console.log(turn.play()) 
// Played being recorded
turn = new Turn(0,PLAYED,41,0,0); 
console.log(turn.play()) 

turn = new Turn(1,PLAYED,39,0,0); 
console.log(turn.play()) 

turn = new Turn(2,PLAYED,40,0,0); 
console.log(turn.play()) 

turn = new Turn(3,PLAYED,49,0,0); 
console.log(turn.play()) 

turn = new Turn(3,PLAYED,51,0,0); 
console.log(turn.play()) 
// What should be played
turn = new Turn(0,GET_PLAY,0,0,0); 
console.log(turn.play()) 

turn = new Turn(1,GET_PLAY,0,0,0); 
console.log(turn.play())
console.log("Thinking I am done")

