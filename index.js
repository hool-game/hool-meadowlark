const { component, xml, jid } = require("@xmpp/component")
const debug = require("@xmpp/debug")
const parse = require("@xmpp/xml/lib/parse")
const { nanoid } = require('nanoid/async')
const { Bot, HoolClient } = require('./bot.js')

require("dotenv").config()

const { once, EventEmitter } = require('events')


/**
 * XMPP-Direct Bridge
 *
 * Bridges between an XMPP client (which is expected by the
 * Hool Client Library) and a simple function which can be
 * made to emit events whenever we feel like. The purpose of
 * this is to "sandbox" a HoolClient instance and be able
 * to control its inputs and outputs directly.
 *
 * This object replaces the normal XMPP handler, and intercepts
 * events coming along the way to reroute them through a custom
 * handler (which we get to write).
 */

class XMPPDirectBridge extends EventEmitter {

  start() {
    this.emit('online', {})

    return {
      catch(fn) {
        return
      }
    }
  }

  stop() {
    return
  }

  send(stanza) {
    // We have to override this after construction
    throw 'Message sending not implemented'
  }

  iqCaller() {
    // FIXME: make this work
    return
  }
}

class BotManager extends EventEmitter {

    // xmpp helper functions

    constructor(options,...args) {
      // initial settings go here
      super(...args)

      if (!options) throw 'No options defined'
      if (!options.domain) throw 'Please specify a domain'
      if (!options.service) throw 'Please specify an XMPP service'
      if (!options.password) throw 'Please specify a password!'

      if (!options.botPrefix) options.botPrefix = 'bot'

      this.domain = options.domain
      this.service = options.service
      this.componentTitle = options.componentTitle || 'Hool Bots'
      this.botPrefix = options.botPrefix

      this.xmpp = component({
        service: this.service,
        domain: this.domain,
        password: options.password,
      })

      // keeping track of bots
      // TODO: use external storage
      this.activeBots = []

      // flag to know when we're in the process of shutting down
      this.windingDown = false

      console.log(`Connected as ${this.domain} on ${this.service}`)

      // setup event catching

      this.xmpp.on("error", (err) => {
        console.error(err)
      })

      this.xmpp.on("offline", () => {
        console.log("offline")
      })

      // handle incoming messages

      this.xmpp.on("stanza", async (stanza) => {
        // ignore IQs, since they're handled separately
        if (stanza.is("iq")) return

        // ignore own messages
        var from = stanza.attrs.from ? jid(stanza.attrs.from) : undefined
        if (
          from &&
          from == this.domain
        ) {
          console.log(`ignoring own message: ${stanza}`)
          return
        }

        // incoming message
        if (stanza.is("message")) {

          // handle turns (moves made by user)
          if (
              stanza.getChildren("game").length &&
              stanza.getChild("game").attrs.xmlns == 'http://jabber.org/protocol/mug#user' &&
              stanza.getChild("game").getChildren("invited").length &&
              stanza.getChild("game").getChild("invited").attrs.var == 'http://hool.org/protocol/mug/hool'
            ) {
	      // we got an invite! wheeee

	      let inviteTag = stanza.getChild("game").getChild("invited")
	      let invite = {}

	      invite.from = inviteTag.attrs.from
	      invite.table = stanza.attrs.from
	      invite.variant = stanza.attrs.to // decides what kind of robot to activate!

	      let role = null
	      if (stanza.getChild("game").getChild("item")) {
		role = stanza.getChild("game").getChild("item").attrs.role || null
	      }

	      console.log(`Bot requested by ${invite.from} as ${role} at ${invite.table}. Variant: ${invite.variant}`)

	      await this.activateBot(invite.table, role, invite.variant)
	      return
          }
        }

	// if not handled, fwd to child
	if (stanza.attrs.to != this.domain) {
	  // hunt for matching robot
	  let target = jid(stanza.attrs.to)

	  // if it's the wrong domain, forget it
	  if (target.domain != this.domain) return

	  // now, it all hinges on the local part
	  // TODO: optimise with hash lookups or something

	  let targetBot = this.activeBots.find(b => b.botID == target.local)
	  if (!targetBot) {
	    console.log(`Could not find target bot: ${target.local}. Available bots:`, this.activeBots)

	    // exit on bot's behalf, just in case
	    let shooter = jid(stanza.attrs.from)
	    if (shooter.domain != this.domain) {
	      this.xmpp.send(parse(`<presence from="${target}" to="${shooter}" type="unavailable"><game xmlns='http://jabber.org/protocol/mug'><item affiliation='none' role='none' jid="${target.local}@${target.domain}"/></game></presence>`))
	    }

	    return
	  }

	  targetBot.bot.client.xmpp.emit('stanza', stanza)
	}
      })

      this.xmpp.on("online", async (address) => {
        // Makes itself available
        console.log("online as", address.toString())
      })
    }

    start() {
      if (process.env.NODE_ENV == 'development') {
        debug(this.xmpp, true)
      }

      return this.xmpp.start()
    }

    async stop() {
      this.windingDown = true

      if (this.activeBots) {
	// first, let all the bots wind down
	for (let bot of this.activeBots) {
	  await bot.bot.stop()
	}

	// last bot to wind down will close the connection
      } else {
	// nothing to wait for; disconnect right away
	return this.xmpp.stop()
      }
    }

    async activateBot(table, role, variant=null) {
      // TODO: decide variant

      // pick (or rather, create) a random bot
      let botID = await nanoid()

      if (this.botPrefix) {
	botID = `${this.botPrefix}-${botID}`
      }

      // make it lowercase, because URLs
      botID = botID.toLowerCase()

      if (botID) {
	      // activate the bot
	      let bot = new Bot({
	        jid: `${botID}@${this.domain}`,
	        side: role,
	        table: table,
	        exitOnEndgame: process.env.HOOLBOT_EXIT_ON_ENDGAME || false,
	        client: new HoolClient({
	          service: 'fork',
	          resource: 'meadowlark',
	        })
	      })


	      // Make the client listen properly
	      bot.client.xmpp = new XMPPDirectBridge()
	      let manager = this
	      bot.client.xmpp.send = (stanza) => { manager.xmpp.send(stanza) }

	      bot.client.jid = jid(bot.jid)
	      bot.client.listen(bot.client.xmpp)

	      bot.client.on('exit', () => {
	        // remove from the list
	        // FIXME: make this more efficient
	        this.activeBots = this.activeBots.filter(b => b.botID != botID)

	        console.log(`${botID} exited. Remaining processes: ${this.activeBots.length} and ${this.windingDown ? 'winding down' : 'counting'}`)

	        // send a presence, just in case
	        this.xmpp.send(parse(`<presence from="${botID}@${this.domain}" to="${table}" type="unavailable"><game xmlns='http://jabber.org/protocol/mug'><item affiliation='none' role='none' jid="${botID}@${this.domain}"/></game></presence>`))

	        if (this.windingDown && this.activeBots.length == 0) {
	          this.xmpp.stop()
	        }
	      })

	      this.activeBots.push({
	        botID: `${botID}`,
	        bot: bot,
	        table: table,
	        role: role,
	        variant: variant,
	      })
      } else {
	console.log('No bots available at the current moment')
      }

      return
    }

}

  // respond to bot events
  /*this.transmitter.on('bot', async (bot) => {
    console.log("handle bot:: ",bot)
    let events = await this.handleStartBot(bot)

    // send out response
    for (let event of events) {
      this.emit(event.event, event)
    }
  })*/

if (!module.parent) {
  let manager = new BotManager({
    domain: process.env.HOOLBOT_COMPONENT_DOMAIN,
    service: process.env.HOOLBOT_WEB_SERVICE,
    password: process.env.HOOLBOT_COMPONENT_PASSWORD,
  })

  manager.start()

  process.once('SIGINT', async (code) => {
    console.log("Received disconnect signal; signing off")

    await manager.stop()
    console.log('The robots are gone!')

    process.exit()
  })
}
