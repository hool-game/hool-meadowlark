const { HoolClient } = require('hool-client')
const { EventEmitter } = require('events')
const { jid } = require('@xmpp/client')
const parse = require("@xmpp/xml/lib/parse")

require("dotenv").config()

// config section (change these to change settings)
const SUITS = new Map([
  ['C', 'clubs'],
  ['D', 'diamonds'],
  ['H', 'hearts'],
  ['S', 'spades'],
])

const RANKS = new Map ([
  ['2', 'two'],
  ['3', 'three'],
  ['4', 'four'],
  ['5', 'five'],
  ['6', 'six'],
  ['7', 'seven'],
  ['8', 'eight'],
  ['9', 'nine'],
  ['10', 'ten'],
  ['J', 'jack'],
  ['Q', 'queen'],
  ['K', 'king'],
  ['A', 'ace'],
])

const SIDES = new Map([
  ['N', 'North'],
  ['E', 'East'],
  ['S', 'South'],
  ['W', 'West'],
])

// auto config section (no need to change these)

const HOOLBOT_JID = process.env.HOOLBOT_JID
const HOOLBOT_PASSWORD = process.env.HOOLBOT_PASSWORD
const HOOLBOT_WEB_SERVICE = process.env.HOOLBOT_WEB_SERVICE
const HOOLBOT_TABLE_SERVICE = process.env.HOOLBOT_TABLE_SERVICE
const HOOLBOT_TABLE_NAME = process.env.HOOLBOT_TABLE_NAME
const HOOLBOT_SIDE = process.env.HOOLBOT_SIDE
const HOOLBOT_DELAY = process.env.HOOLBOT_DELAY || 4000
const HOOLBOT_EXIT_ON_ENDGAME = process.env.HOOLBOT_EXIT_ON_ENDGAME || false

// card object type (copied from tablebot)
class Card {
  constructor(v) {
    if (v.rank && v.suit) {
      // objects become strings for further processing
      v = `${v.suit}${v.rank}` // 3 of clubs -> C3
    }

    // convert from string: C3 -> 3 of clubs
    const card_re = new RegExp(
        '^(?<suit>(' +
        [...SUITS.keys()].join('|') +
        '))(?<rank>(' +
        [...RANKS.keys()].join('|') +
        '))$', 'i')

    let r = card_re.exec(v)
    if (!r) {
      // regexp failed; try converting from number
      let cardNum

      try {
        cardNum = Number(v)
      } catch (e) {
        throw TypeError(`Invalid card value: ${v}`)
      }

      if (cardNum < 0) {
        throw TypeError(`Invalid card value: ${v}`)
      } else if (cardNum < 13) {
        this.suit = 'C'
      } else if (cardNum < 26) {
        this.suit = 'D'
      } else if (cardNum < 39) {
        this.suit = 'H'
      } else if (cardNum < 52) {
        this.suit = 'S'
      } else {
        throw TypeError(`Invalid card value: ${v}`)
      }

      this.rank = cardNum % 13 + 2

      /*
       * note: these ranks are different from
       * the reverse conversion because here
       * we've already added 2 (in the to-number
       * conversion, we do this check and only
       * then subtract 2)
       */
      if (this.rank == 11) {
        this.rank = 'J'
      } else if (this.rank == 12) {
        this.rank = 'Q'
      } else if (this.rank == 13) {
        this.rank = 'K'
      } else if (this.rank == 14) {
        this.rank = 'A'
      }

      this.value = `${this.suit}${this.rank}`
    } else {
      // regexp succeeded; let's use that
      this.value = v
      this.suit = r.groups.suit.toUpperCase()
      this.rank = r.groups.rank.toUpperCase()
    }
  }

  toBSON() {
    return this.value
  }

  toNumber() {
    let suitLevel
    if (this.suit == 'C') {
      suitLevel = 0
    } else if (this.suit == 'D') {
      suitLevel = 1
    } else if (this.suit == 'H') {
      suitLevel = 2
    } else if (this.suit == 'S') {
      suitLevel = 3
    } else {
      throw `Invalid suit: ${this.suit}`
    }

    let rankLevel
    if (this.rank == 'J') {
      rankLevel = 9
    } else if (this.rank == 'Q') {
      rankLevel = 10
    } else if (this.rank == 'K') {
      rankLevel = 11
    } else if (this.rank == 'A') {
      rankLevel = 12
    } else {
      // 2 -> 0, 3 -> 1, ..., A -> 12
      rankLevel = Number(this.rank) - 2
    }

    return suitLevel*13 + rankLevel
  }

  static makeDeck() {
    var deck = []
    for (let s of SUITS.keys()) {
      for (let r of RANKS.keys()) {
        deck.push(new this(`${s}${r}`))
      }
    }
    return deck
  }

  /*
   * Decides whether one card is greater or less
   * than another. Compares first by suit and then
   * by rank.
   *
   * returns:
   *   difference between suits if suits are different
   *   difference between ranks if ranks are different
   *   0 if both suit and rank are the same
   *
   * "difference" can be a positive or negative number, depending
   * on whether the first card is of higher or lower value than
   * the second.
   */

  static compare(c1, c2) {
    // compare by suit then rank
    let s1 = [...SUITS.keys()].indexOf(c1.suit)
    let s2 = [...SUITS.keys()].indexOf(c2.suit)

    // return by suit value if different
    if (s1 != s2) {
      return s1 - s2
    }

    // otherwise, suit must be same
    // compare by rank instead
    let r1 = [...RANKS.keys()].indexOf(c1.rank)
    let r2 = [...RANKS.keys()].indexOf(c2.rank)

    return r1 - r2
  }

  static shuffle(deck) {
    return crypto_shuffle(deck)
  }

  // Also known as "sort cards"
  static unshuffle(deck) {
    deck.sort(Card.compare)
    return deck
  }

  /*
   * TODO: reverse the order or create another function
   * which reverses the order before returning. This is
   * because Bridge players sort their cards with the
   * highest on the left, not the lowest.
   */

   static getTrickWinner(cards, trump='NT') {
    if (!['NT', 'C', 'D', 'H', 'S'].includes(trump)) {
      throw `Invalid trump: ${trump}`
    }

    cards = cards.map(c => new Card(c))

     /*
      * our algorithm:
      *
      * - first, filter all trump-suit cards and sort
      *   them by rank from high to low
      * - to that, append leading-suit cards, again
      *   sorted from high to low
      * - that's it: no need to process discards since
      *   they will never win the trick
      *
      * - combine those sorted lists together and take
      *   the rightmost card (which will be a trump card,
      *   if one exists, and a leading-suit card, if not).
      * - since the cards were sorted with highest rank
      *   first, this is the winning card you were looking
      *   for.
      */

     let trumps = []

     if (trump != 'NT') {
       trumps = cards.filter(c => c.suit == trump)
       trumps.sort(Card.compare)
       trumps.reverse() // highest first
     }

     let leads = []
     if (!trumps.length) {
       // we don't have to do this if trumps are there
       leads = cards.filter(c => c.suit == cards[0].suit)
       leads.sort(Card.compare)
       leads.reverse() // highest first
     }

     let alls = [...trumps, ...leads]

     console.log('sort of', alls)
     console.log('wins', alls[0])

     return alls[0]
   }
}


/**
 * Meadowlark
 *
 * Interface for the main backend AI engine. This class contains all
 * the functions necessary to interface with a specific backend.
 */
class Meadowlark {

  constructor(side, system=0) {
    if (!side) throw `Please specify the bot side`
    if (![...SIDES.keys()].includes(side)) throw `Invalid side: "${side}"`

    this.side = side
    this.sideNum = this.constructor.playerToNumber(side)
    this.system = system

    // Set up a unique robot caller
    this.turn = require("./lib/binding.js")

    // initialise the robot
    this.reset()

    console.log(`The side is: ${this.side} (${this.sideNum})`)
  }

  reset() {
    new this.turn(0,99,1,0,0).play()
    new this.turn(0,99,0,0,0).play()

    // set system (0 = no system, 1 = robot system)
    new this.turn(this.sideNum,102,this.system,0,0).play()

    // prepare to accept hand (roll up your sleeves?)
    new this.turn(0,58,0,0,0).play()
  }

  setCards(cards, side=undefined) {
    if (!side) side = this.sideNum

    if(![0, 1, 2, 3].includes(side)) {
      side = this.constructor.playerToNumber(side)
    }
    let turns = []

    for (let card of cards) {
      card = new Card(card).toNumber()
      let turn = new this.turn(side, 9, card, 0, 0)

      turns.push(turn.play())
    }
    return turns
  }

  static parseShare(share) {
    share = share.split(':')

    if (
      share[0] == 'Pattern'
    ) {
      return 'pattern'
    } else if (share[0] == 'hcps') {
      return 'HCP'
    } else if (share[0] == 'Length') {
      // figure out which suit it is from format: 13C

      let suit = share[1][share[1].length-1]
      return suit
    } else {
      console.warn(`Warning: unrecognised share: ${share}`)
      return 'HCP'
      // HOTFIX: don't throw error
      // throw `Unrecognised share: "${share[0]}" (from ${share}). Please update the code to support all share types.`
    }
  }

  getFirstShare(system = 0, turn = undefined){
    if (!turn) turn = this.sideNum

    turn = new this.turn(turn,70,system,0,0);
    let share = turn.play()

    return this.constructor.parseShare(share)
  }

  getSecondShare(system = 0, turn = undefined){
    if (!turn) turn = this.sideNum

    turn = new this.turn(turn,71,system,0,0);
    let share = turn.play()

    return this.constructor.parseShare(share)
  }

  setFirstShare(side, share, value) {
    return this.setShare(side, share, value, 0)
  }

  setSecondShare(side, share, value) {
    return this.setShare(side, share, value, 3)
  }

  setShare(side, share, value, offset=0) {
    side = this.constructor.playerToNumber(side)
    let turns = []

    if (share == 'HCP') {
      turns.push(new this.turn(side, 64+offset, Number(value), 0, 0))
    } else if (share == 'pattern') {
      let shape = value.split(',').map(v => Number(v))
      turns.push(new this.turn(side, 66+offset, shape[0], shape[1], shape[2]))
    } else {
      let shareSide
      if (share == 'C') shareSide = 0
      else if (share == 'D') shareSide = 1
      else if (share == 'H') shareSide = 2
      else if (share == 'S') shareSide = 3

      turns.push(new this.turn(side, 65+offset, shareSide, Number(value), Number(value)))
    }

    let results = []

    for (let turn of turns) {
      results.push(turn.play())

      console.log(results[results.length-1])
    }

    return results
  }

  static parseBid(bid) {
    bid = bid.split(':')

    const bid_re = new RegExp(
      '^(?<level>(1|2|3|4|5|6|7))(?<suit>(C|D|H|S|N))$',
      'i')
    let parsedBid = bid_re.exec(bid[0])

    if (bid[0] == 'pass' || bid[0] == 'P') {
      return {
        type: 'pass',
        level: null,
        suit: null,
      }
    } else if (bid[0] == 'X') {
      return {
        type: 'double',
        level: null,
        suit: null,
      }
    } else if (bid[0] == 'R') {
      return {
        type: 'redouble',
        level: null,
        suit: null,
      }
    } else if (!!parsedBid) {
      return {
        type: 'level',
        level: parsedBid.groups.level,
        suit: parsedBid.groups.suit == 'N' ? 'NT' : parsedBid.groups.suit,
      }
    } else {
      throw `Unrecognised bid type: "${bid[0]}"". Please update your code to support other bid types.`
    }
  }

  setBid(bid, turn){
    turn = this.constructor.playerToNumber(turn)
    if (bid == 'pass'){
      turn = new this.turn(turn,75,0,0,0)

    }
    else if (bid == 'double'){
    console.log("double bid:",bid)
    turn = new this.turn(turn,75,1,0,0)

    }
    else if (bid == 'redouble'){
      console.log("redouble bid:",bid)
      turn = new this.turn(turn,75,2,0,0)

    }
    else{
      console.log("level bid:",bid)
      bid = this.constructor.bidToNumber(bid)
      turn = new this.turn(turn,75,bid,0,0)
    }

    return turn.play()


  // Converting bids to number

  // level suit - level is 1-7  / suit = 0 clubs, 1 diamond, 2 heart , 3 spade,  4 no trump
    // 1C = 10
    // 2C = 20
    // 1D = 11
    // 2D = 21
    // 1H = 12
    // 1S = 13
    // NoTrump - NT / N ?  = 14
    // 0 = Pass
  }

  getBid(side = undefined) {
    if (!side) side = this.sideNum

    new this.turn(0, 89, 0, 0, 0).play()

    // figure out what the current contract is
    let lead = {
      level: null,
      suit: null,
      double: false,
      redouble: false, // we'll never use this, but anyway...
    }

    this.state.bids.forEach((bidRound) => {
      bidRound.forEach((bid) => {
        if (!bid.won) return

        // only pick off winning bids
        if (bid.level) lead.level = bid.level
        if (bid.suit) lead.suit = bid.suit
        if (bid.double) lead.double = true
        if (bid.redouble) lead.redouble = true
      })
    })

    let bidNum = 0 // this is what we tell the bot
    let doubled = 0 // and this too

    if (lead.level && lead.suit) {
      bidNum = this.constructor.bidToNumber(`${lead.level}${lead.suit}`)
      doubled = lead.double ? 1 : 0
    }

    let turn = new this.turn(side,72,bidNum,doubled,0)
    let bid = turn.play()

    return this.constructor.parseBid(bid)
  }

  setContract(winner){
    let level = winner.level
    let suit = winner.suit
    let bid = level + suit
    bid = this.constructor.bidToNumber(bid)
    let side = winner.side
    side = this.constructor.playerToNumber(side)

    // set double and redouble parameter
    let doubles = 0
    if (winner.double) doubles += 1
    if (winner.redouble) doubles += 1

    let turn = new this.turn(0,86,bid,side,doubles)
    return turn.play()

  }

  setCardPlay(card, side, trickRound=null, trickPosition=null) {
    card = new Card(card).toNumber()
    side = this.constructor.playerToNumber(side)

    // set up the empty trick
    if (!this.state.tricks) this.state.tricks = []
    if (!this.state.tricks[0]) this.state.tricks[0] = []

    // compute current trick round
    console.debug('Computing trick round from', this.state ? this.state.tricks : 'nothing')
    if (!trickRound) {
      if (
        this.state &&
        this.state.tricks
      ) {
        trickRound = this.state.tricks.length
      } else {
        trickRound = 1
      }
    }

    if (!trickPosition) {
      if (
        this.state &&
        this.state.tricks &&
        this.state.tricks.length
      ) {
        trickPosition = this.state.tricks[trickRound-1].length
      } else {
        trickPosition = 1
      }
    }

    // both are zero-indexed
    trickRound = trickRound - 1
    trickPosition = trickPosition - 1

    // find out the current

    let turn = new this.turn(side,76,card,trickRound,trickPosition)
    return turn.play()
  }

  getCardPlay(side=undefined, trickRound=null, trickPosition=null){
    if (!side) side = this.sideNum

    if (![0, 1, 2, 3].includes(side)) {
      side = this.constructor.playerToNumber(side)
    }

    // set up the empty trick
    if (!this.state.tricks) this.state.tricks = []
    if (!this.state.tricks[0]) this.state.tricks[0] = []

    // compute current trick round
    console.debug('Computing trick round from', this.state ? this.state.tricks : 'nothing')
    if (!trickRound) {
      if (
        this.state &&
        this.state.tricks
      ) {
        trickRound = this.state.tricks.length
      } else {
        trickRound = 1
      }
    }

    if (!trickPosition) {
      if (
        this.state &&
        this.state.tricks &&
        this.state.tricks.length
      ) {
        trickPosition = this.state.tricks[trickRound-1].length
      } else {
        trickPosition = 1
      }
    }

    // add one since we're counting a trick that's *not happened yet*
    // as opposed to a trick that's already complete!
    trickPosition += 1

    // both are zero-indexed
    trickRound = trickRound - 1
    trickPosition = trickPosition - 1

    let turn = new this.turn(side, 74, 0, trickRound, trickPosition)
    let result = turn.play().split(':')

    if (result[0] != 'Err') {
      return new Card(result[1])
    } else {
      throw `Robot threw error: ${result.join(':')}`
    }
  }

  static playerToNumber(player){
    // numbers are numbers
    if ([0, 1, 2, 3].includes(player)) return player

    switch(player) {

      case 'N':
        return 0
        break;

      case 'S':
        return 2
        break;

      case 'E':
        return 1
        break;

      case 'W':
        return 3
        break;
    }

    throw TypeError(`Invalid player: ${player}`)
  }

  static bidToNumber(bid){
    let output = 0
    console.log("bid:", bid)
    let [level,suit] = bid.split("", 2)
    console.log('the suit is actually', suit)

    if (suit == 'C'){
      output = 0
    }
    else if (suit == 'D'){
      output = 1
    }
    else if (suit == 'H'){
      output = 2
    }
    else if (suit == 'S'){
      output = 3
    }
    else if (suit == 'N'){
      // NT
      output = 4
    }

    console.log("converting bid to number", bid)
    output += level*10
    console.log("this is converted bid:", output)
    return output
  }
}

/**
 * XMPP-Fork Bridge
 *
 * Bridges between an XMPP client (which is expected by the
 * Hool Client Library) and the NodeJS fork() event-based
 * messaging. The latter is used when this bot file is being
 * run as a slave with the final messages being transmitted
 * through an external master process.
 *
 * This object replaces the normal XMPP handler, and intercepts
 * events coming along the way to reroute them through a custom
 * process handler.
 */

class XMPPForkBridge extends EventEmitter {
  constructor(...args) {
    super(...args)

    process.on('message', (msg) => {
      if (msg.type == 'stanza') {
        this.emit('stanza', parse(msg.payload))
      }
    })
  }

  start() {
    this.emit('online', {})

    return {
      catch(fn) {
        return
      }
    }
  }

  stop() {
    return
  }

  send(stanza) {
    process.send({
      type: 'stanza',
      payload: stanza.toString(),
    })
  }

  iqCaller() {
    // FIXME: make this work
    return
  }
}

function wait(milliseconds=HOOLBOT_DELAY, random=0){
    return new Promise(resolve => {
        setTimeout(resolve, milliseconds + Math.random()*random);
    });
}

function validConfiguration(jid, password, webservice){
  if(!jid){
    throw ("ERR:invalid JID")
  }
  if(!password){
    throw("ERR:invalid Password")
  }
  if(!webservice){
    throw("ERR:invalid webservice")
  }
  return true

}

/*
bot = new Bot()

console.log(bot.numberToCard(7))

console.log("newly added functions")
turn = new Turn(0,70,0,0,0); // You have to create this new turn with each change in state
console.log(turn.play()) // This will return a string with the appropriate result
// Info sharing round 2
//turn = new Turn(0,71,0,0,0);
console.log(bot.getFirstShare(0))
//console.log(turn.play())
// Info sharing revise?
turn = new Turn(0,72,0,0,0);
console.log(turn.play())
// Bidding
console.log("Bidding")
turn = new Turn(0,75,0,0,0);
console.log(turn.play())
*/


function nextTurn(prev){
  switch (prev) {

    case 'N':
      return 'E'
      break;

    case 'E':
      return 'S'
      break;

    case 'S':
      return 'W'
      break;

    case 'W':
      return 'N'
      break;

    throw('bad prev turn')
  }
}

function getPartner (side) {
  let sides = [...SIDES.keys()]
  let partnerDistance = Math.floor(sides.length/2)
  return sides[(sides.indexOf(side) + partnerDistance) % sides.length]
}


/**
 * Bot
 *
 * This is what will be initiated in external libraries. It
 * listens for incoming events and respons or passes them on
 * to the main bot engine as appropriate.
 */
class Bot {
  constructor(options) {

    // Which side the bot is playing on
    if (!options.side) throw `Please specify the bot side`
    if (![...SIDES.keys()].includes(options.side)) throw `Invalid side: "${options.side}"`
    this.side = options.side

    // The JID of this bot
    if (!options.jid) throw `The bot needs a JID!`
    this.jid = options.jid

    // The table on which this bot is sitting
    if (!options.table) throw `ON which table is the bot sitting?`
    this.table = options.table

    // Whether to exit when the game ends
    this.exitOnEndgame = options.exitOnEndgame || false

    // Activate the bot
    this.bot = new Meadowlark(this.side)

    this.bot.state = {
      table: this.table,
      side: this.side,
      hands: {},
      shares: {
        N: [],
        E: [],
        S: [],
        W: [],
      },
      bids: [],
      tricks: [],
      dealer: 'N', // as a default
      contract: undefined, // will be set later
  }


    // Which system the (internal) bot is playing by
    this.system = options.system || 0

    // The Hool client object, where we listen for incoming events
    // and send outgoing ones
    if (!!options.client) {
      this.client = options.client
      this.listen(this.client)
    }

  }

  listen(client) {
    client.on('connected', (e) => { console.log('We are now connected, Press control c to disconnect')

    client.joinTable(this.table, this.side)

  })

  client.on('tablePresence', (e) => {
    console.log(`Presence came in: ${JSON.stringify(e)}`)
    console.log(jid(e.user).bare())
    if (jid.equal(
      jid(e.user).bare(),
      jid(this.jid).bare()
    )) {
      console.log('Oh look. Just found myself!')

      if (e.role && ['N', 'E', 'S', 'W'].includes(e.role)) {
        console.log(`I am sitting on ${e.role}`)

        this.bot.state.side = e.role
        this.bot.state.table = e.table
      }
    }
  })

  client.on('tableExit', async (presence) => {
    if (jid.equal(jid(presence.user).bare(), jid(this.jid).bare()) && presence.table == this.table) {
      client.leaveTable(this.table)
      await client.xmppStop()
      await wait(1000)
      process.exit()
    }
  })

  // hool.on('Infoshare', function => check for turns
  client.on('infoshare', async (e) => {
    console.log(`Got share of ${e.type} = ${e.value} from ${e.side}`)
    this.bot.state.shares[e.side].push({
      type: e.type,
      value: e.value,
    })


    // send to the bot (unless it's a self-share)
    if (this.bot.state.side != e.side) {
      if (this.bot.state.shares[e.side].length == 1) {
        console.log(this.bot.setFirstShare(e.side, e.type, e.value))
      } else {
        console.log(this.bot.setSecondShare(e.side, e.type, e.value))
      }
    }

    // if the next turn is ours, make a move
    if (nextTurn(e.side) == this.bot.state.side) {
      console.log(`Our turn to share info`)
      let share

      if (this.bot.state.shares[this.bot.state.side].length <=0) {
        // no shares so far means it's the first one now
        console.log('First share')
        share = this.bot.getFirstShare()

        console.log(`Sharing: ${share}`)
        await wait()
        client.shareInfo(this.bot.state.table, this.bot.state.side, share)
      } else if (this.bot.state.shares[this.bot.state.side].length == 1) {
        // exactly one share means it's time for the second
        console.log('Second share')
        share = this.bot.getSecondShare()

        console.log(`Sharing: ${share}`)
        await wait()
        client.shareInfo(this.bot.state.table, this.bot.state.side, share)
      } else {
        console.log('Oh wait, looks like sharing is already over!')
      }
    }

    // if all infoshares are over, make the first bid
    if (
      ['N', 'E', 'S', 'W']
      .every(s => this.bot.state.shares[s].length == 2)
    ) {
      console.log('Time to make a bid!')
      // randomise to avoid race conditions
      await wait(1000 + (this.bot.constructor.playerToNumber(this.bot.state.side)*2000))

      let bid = this.bot.getBid()
      console.log('The bid is', bid)

      client.makeBid(this.bot.state.table, this.bot.state.side, bid.type, bid.level, bid.suit)
    }
  })

    //hool.on('Presence', e =)
    client.winner = {
        level: null,
        suit: null,
        side: null,
        double: false,
        redouble: false
      }

    client.on('stateUpdate', async (e) => {

      console.log("new state update:",e)

      // handle dealer change
      if (!!e.tableinfo && !!e.tableinfo.dealer) {
        this.bot.state.dealer = e.tableinfo.dealer
      }

      if (!!e.hands){
        console.log('We got some hands!')
        for (let hand of e.hands) {
          if (hand.cards.length) {
            console.log('Received cards:', hand.cards)

            // send to robot if not already set
            if (
              !this.bot.state.hands[hand.side] &&
              ( // at least one of the following should be true:
                !this.bot.state.contract || // contract hasn't been set
                this.bot.state.contract.declarer != getPartner(this.bot.state.side) || // we are not the dummy
                hand.side != getPartner(this.bot.state.side) // we are not getting our partner's cards
              )
            ) {
              console.log(this.bot.setCards(hand.cards.map(c => `${c.suit}${c.rank}`), hand.side))
            }

            // we will save anyway, just to be updated
            this.bot.state.hands[hand.side] = hand.cards.map(c => new Card(c))

            // start sharing if dealer
            console.debug(`We are ${this.bot.state.side} and the dealer is ${this.bot.state.dealer}`)
            if (
              this.bot.state.side == this.bot.state.dealer &&
              hand.side == this.bot.state.side &&
              !this.bot.state.shares[this.bot.state.side].length
            ) {
              let share = this.bot.getFirstShare()

              console.log(`We are the dealer. Playing first share: ${share}`)

              await wait()
              client.shareInfo(this.bot.state.table, this.bot.state.side, share)
            }
          }
        }
      }

      //console.log("bids exist", !!e.bids)

      if(!!e.bidInfo){
        let bidT
        console.log("bid round:", e.bidInfo)

        for (let bid of e.bidInfo){
          // quick compat thing (bid/type inconsistency)
          bid.bid = bid.bid || bid.type

          // skip undefined bids
          if (!bid.bid) continue

          // first, save it
          if (this.bot.state.bids.length == 0) {
            this.bot.state.bids.push([])
          }

          this.bot.state.bids[this.bot.state.bids.length-1].push({
            side: bid.side,
            bid: bid.type,
            level: bid.level,
            suit: bid.suit,
            won: bid.won,
          })

          // figure out whether to make the next round
          if (
            ( // ends at 4 for first round...
              this.bot.state.bids.length == 1 &&
              this.bot.state.bids[0].length == 4
            ) ||
            ( // ...and at 2 for the rest
              this.bot.state.bids.length > 1 &&
              this.bot.state.bids[this.bot.state.bids.length-1].length == 2
            )
            // we dont' worry about the last round, because
            // anyway there's no need to push
          ) {
            this.bot.state.bids.push([])
          }

          // if it's our own one, continue
          if (bid.side == this.bot.state.side) continue

          // if it's not our own bid, send it to the robot
          if (bid.side != this.bot.state.side) {
            console.log(`Telling robot: ${bid.side} plays ${bid.type} ${bid.level||''}${bid.suit||''}`)
            if (bid.type == 'level') {
              console.log('Robot says:',this.bot.setBid(`${bid.level}${bid.suit[0]}`, bid.side))
            } else {
              console.log('Robot says:', this.bot.setBid(bid.type, bid.side))
            }
          }
        }

        // prepare other bid round, just in case


        // find out if it's our turn to play

        /*
         * find out if it's our turn to play
         *
         * logic:
         *  - if it's the first round, play anyway
         *  - for any other round:
         *    - if winner (winning bid) is pass:
         *      - end of bidding; don't play
         *    - if winner is double:
         *      - play if prev winner was ours
         *    - if winner is raise (level):
         *      - play if winner is opponent
         *    - otherwise:
         *      - don't play
         */

        if (
          // first round not yet complete
          this.bot.state.bids.length == 1 ||

          // winner is raise and not by me or partner
          (
            this.bot.state.bids.length >= 2 &&
            this.bot.state.bids[this.bot.state.bids.length-2].some(b => !!b.won) &&
            this.bot.state.bids[this.bot.state.bids.length-2].find(b => !!b.won).bid == 'level' &&
            ![
              this.bot.state.side,
              getPartner(this.bot.state.side),
            ].includes(
              this.bot.state.bids[this.bot.state.bids.length-2].find(b => !!b.won).side
            )
          ) ||

          // winner is double and previous was by me
          (
            this.bot.state.bids.length >= 3 &&
            this.bot.state.bids[this.bot.state.bids.length-2].some(b => !!b.won) &&
            this.bot.state.bids[this.bot.state.bids.length-3].some(b => !!b.won) &&
            this.bot.state.bids[this.bot.state.bids.length-2].find(b => !!b.won).bid == 'double' &&
            this.bot.state.bids[this.bot.state.bids.length-3].find(b => !!b.won).side == this.bot.state.side
          )
        ) {
          // criteria matched: play the bid!

          console.log('Time to make a bid!')
          console.log(JSON.stringify(this.bot.state.bids))

          // randomise to avoid race conditions
          await wait(1000 + (this.bot.constructor.playerToNumber(this.bot.state.side)*2000))

          let bid = this.bot.getBid()
          console.log('The bid is', bid)

          await wait()
          client.makeBid(this.bot.state.table, this.bot.state.side, bid.type, bid.level, bid. suit)
          // HOTFIX: always pass
          //hool.makeBid(bot.state.table, bot.state.side, 'pass')
        } else if (
          this.bot.state.bids[this.bot.state.bids.length-2] &&
          this.bot.state.bids[this.bot.state.bids.length-2].some(b => !!b.won) &&
          ['redouble','pass'].includes(this.bot.state.bids[this.bot.state.bids.length-2].find(b => !!b.won).bid) ||
          this.bot.state.bids[this.bot.state.bids.length-1] &&
          this.bot.state.bids[this.bot.state.bids.length-1].some(b => !!b.won) &&
          ['redouble','pass'].includes(this.bot.state.bids[this.bot.state.bids.length-1].find(b => !!b.won).bid)
        ) {
          console.log('The bidding is over!')
        } else {
          console.log("Can't bid yet. Being patient...")
        }
      }

      if (!!e.contractinfo) {
        e.contractinfo.double = e.contractinfo.double != 'false'
        e.contractinfo.redouble = e.contractinfo.redouble != 'false'

        console.log(`The contract is: ${e.contractinfo.level}${e.contractinfo.suit}${e.contractinfo.double ? ' X' : ''}${e.contractinfo.redouble ? 'X' : ''} by ${e.contractinfo.declarer}`)

        // save it ourselves
        this.bot.state.contract = e.contractinfo

        // send to the bot
        console.log(this.bot.setContract({
          level: e.contractinfo.level,
          suit: e.contractinfo.suit,
          double: e.contractinfo.double,
          redouble: e.contractinfo.redouble,
          side: e.contractinfo.declarer,
        }))

        // play the opening lead if needed
        // play opening lead if needed
        if (
          !!e.contractinfo &&
          nextTurn(e.contractinfo.declarer) == this.bot.state.side &&
          (
            // make sure no cards played yet
            !e.tricks ||
            e.tricks.length == 0 ||
            (
              e.tricks.length == 1 &&
              e.tricks[0].cards.length == 0
            )
          )
        ) {
          console.log(`Leading because ${this.bot.state.side} is after ${e.contractinfo.declarer}`)
          await wait()

          let ourCard = this.bot.getCardPlay()
          console.log(`Leading with: ${ourCard.suit}${ourCard.rank}`)

          await wait()
          client.playCard(this.bot.state.table, this.bot.state.side, `${ourCard.rank}`, ourCard.suit)
        }
      }

      // handle endgame
      if(!!e.score && e.score.running) {
        console.info(`It's endgame, folks`)

        // wait a bit...
        await wait()

        // exit if we're supposed to
        if (this.exitOnEndgame) {
          client.groupChat(this.table, "Good game! See you later")
          client.leaveTable(this.table)
          await client.xmppStop()
          await wait(1000)
          process.exit()
        } else {

          // randomise to avoid race conditions
          await wait(1000 + (this.bot.constructor.playerToNumber(this.bot.state.side)*2000))

          // reset the robots...
          this.bot.reset()

          // ..and the data...
          this.bot.state.hands = {}
          this.bot.state.shares = {
            N: [],
            E: [],
            S: [],
            W: [],
          },
          this.bot.state.tricks = []
          this.bot.state.bids = []
          this.bot.state.contract = undefined // to be set later

          // increment the dealer...
          this.bot.state.dealer = nextTurn(this.bot.state.dealer)

          // ...and then say we're ready to play
          // sorry about the function name; someone else made it and it's
          // not the greatest choice. we need to change that >.<
          client.scoreboard(this.bot.state.table, 'ready', this.bot.state.side)
        }
      }

      // handle host change
      if (e.tableinfo && e.tableinfo.host) {
        console.info(`${e.tableinfo.host} is the new host!`)

        // leave the table if we're the host
        if (e.tableinfo.host == this.jid) {
          console.info(`Wait, I'm the host! I can't handle that! Bye-bye`)

          client.groupChat(this.table, "Wait, I'm the host? I can't handle that!")
          client.groupChat(this.table, "Someone else take over please; I'm leaving")

          client.leaveTable(this.table)

          await client.xmppStop()
        }
      }

    })

    client.on('cardplay', async (e) => {
      console.log(`I(${this.bot.state.side}) see ${e.side} has played ${e.suit}${e.rank}`)

      if (!this.bot.state.tricks.length) {
        this.bot.state.tricks.push([])
      }

      this.bot.state.tricks[this.bot.state.tricks.length-1].push({
        side: e.side,
        rank: e.rank,
        suit: e.suit,
      })

      // if it's not ours or partner-dummy's, send to the bot
      if (this.bot.state.side != e.side &&
        !(
          this.bot.state.contract.declarer == this.bot.state.side &&
          getPartner(this.bot.state.side) == e.side
        )
      ) {
       this.bot.setCardPlay(`${e.suit}${e.rank}`, e.side)
      } else if (
        // if we're the dummy, send own (dummy) cardplay anyway
        this.bot.state.side == e.side &&
        e.side == getPartner(this.bot.state.contract.declarer)
      ) {
        this.bot.setCardPlay(`${e.suit}${e.rank}`, e.side)
      }

      // check for completed trick
      if (this.bot.state.tricks[this.bot.state.tricks.length-1].length >= 4) {
        this.bot.state.tricks.push([])

        // ...and check if we happened to be the winner,
        // in which case we should also make a move

        let lastTrick = this.bot.state.tricks[this.bot.state.tricks.length-2]
        let winningCard = Card.getTrickWinner(lastTrick, this.bot.state.contract.suit)
        let winner = lastTrick.find(c => (
          c.rank == winningCard.rank &&
          c.suit == winningCard.suit
        )).side

        console.log('lastTrick', lastTrick)

        if (
          winner == this.bot.state.side &&
          this.bot.state.contract.declarer != getPartner(this.bot.state.side) && // dummy doesn't try to play, of course
          ( // don't try to play the 14th trick
            this.bot.state.tricks.length < 13 ||
            this.bot.state.tricks[12].length < 4
          )
        ) {
          console.log(`I won the trick! Let me play again.`)

          let ourCard = this.bot.getCardPlay()
          console.log(`We're playing: ${ourCard.suit}${ourCard.rank}`)
          await wait()
          client.playCard(this.bot.state.table, this.bot.state.side, `${ourCard.rank}`, ourCard.suit)
        } else {
          console.log(`I didn't win the trick, ${winner} did with ${winningCard.rank}.${winningCard.suit}`)

          // check if partner-dummy won
          if (
            this.bot.state.contract.declarer == this.bot.state.side &&
            winner == getPartner(this.bot.state.side)
          ) {
            console.log(`Oh, but ${winner} is my partner, the dummy! That means I can still play...`)

            let ourCard

            try {
              ourCard = this.bot.getCardPlay(getPartner(this.bot.state.side))
              console.log(`Dummy is playing: ${ourCard.suit}${ourCard.rank}`)
              await wait()
              client.playCard(this.bot.state.table, getPartner(this.bot.state.side), `${ourCard.rank}`, ourCard.suit)
            } catch(err) {
              console.log(`Dummy was not able to play: ${err}`)
            }
          }
        }

      } else {

        // if the next turn is ours, make a move
        if (
          nextTurn(e.side) == this.bot.state.side &&
          this.bot.state.contract.declarer != getPartner(this.bot.state.side) // not the dummy though
        ) {
          console.log(`Our turn to play!`)

          try {
            let ourCard = this.bot.getCardPlay()
            console.log(`We're playing: ${ourCard.suit}${ourCard.rank}`)
            await wait()
            client.playCard(this.bot.state.table, this.bot.state.side, `${ourCard.rank}`, ourCard.suit)
          } catch (err) {
            console.error(`Couldn't play due to error: ${err}`)
          }
        } else if (
          this.bot.state &&
          this.bot.state.contract &&
          this.bot.state.contract.declarer == this.bot.state.side &&
          nextTurn(e.side) == getPartner(this.bot.state.side)
        ) {
          // if the next turn is partner-dummy's, make move
          await wait(3500)
          console.log("Dummy's turn to play!")

          let ourCard
          try {
            ourCard = this.bot.getCardPlay(getPartner(this.bot.state.side))
            console.log(`We're playing: ${ourCard.suit}${ourCard.rank} (from dummy)`)
            await wait()
            client.playCard(this.bot.state.table, getPartner(this.bot.state.side), `${ourCard.rank}`, ourCard.suit)
          } catch(err) {
            console.log(`Dummy couldn't play: ${err}`)
          }
        }
      }
    })

  }

  async stop() {
    // Leave the table, just in case
    this.client.leaveTable(this.table)
    await this.client.xmppStop()
    await wait(1000)
  }

}

function main() {





if(validConfiguration(HOOLBOT_JID,HOOLBOT_PASSWORD,HOOLBOT_WEB_SERVICE)){
  console.info(`Activating bot: ${HOOLBOT_JID} on ${HOOLBOT_TABLE_NAME} as ${HOOLBOT_SIDE}`)
  hool = new HoolClient({service: HOOLBOT_WEB_SERVICE, resource: 'meadowlark'})
  hool.tableService = HOOLBOT_TABLE_SERVICE

  bot = new Bot({
     jid: HOOLBOT_JID,
     side: HOOLBOT_SIDE,
     table: HOOLBOT_TABLE_NAME,
     exitOnEndgame: HOOLBOT_EXIT_ON_ENDGAME,
  })

  bot.listen(hool)

    // override hool.xmpp with custom object if necessary
    if (HOOLBOT_WEB_SERVICE == 'fork') {
      hool.xmpp = new XMPPForkBridge()
      hool.jid = jid(HOOLBOT_JID)
      hool.listen(hool.xmpp)
    } else {
      hool.xmppConnect(HOOLBOT_JID, HOOLBOT_PASSWORD)
    }

}

    process.once('SIGINT', async (code) => {
      console.log("Off we go")

      if (bot.table) {
        console.log('Exiting from the table, for cleanliness')
        hool.leaveTable(bot.table)
      }

      await hool.xmpp.stop()
      await wait(1000)
      process.exit()
      console.log('component disconnected.')
  })
}

module.exports = {
  HoolClient,
  Meadowlark,
  Card,
  Bot,
}

if (!module.parent) {
  main()

}
