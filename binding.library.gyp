{
  'targets': [
    {
      'target_name': 'hool-native',
      'sources': [ 'src/hool.cc','src/hello.cpp' ],
      'conditions': [[
        'OS=="win"',
        {
            'libraries': ['src/libdds.so'],
            'include_dirs': ['src'],
            'copies': [
            {
                'destination': 'c:/Users/rod.LAPTOP-V8D9A4GN/source/repos/hello/Debug',
                'files': ['src/libdds.so']
            }]
        }]],

      'include_dirs': ["<!@(node -p \"require('node-addon-api').include\")"],
      'dependencies': ["<!(node -p \"require('node-addon-api').gyp\")"],
      'cflags!': [ '-fno-exceptions' ],
      'cflags_cc!': [ '-fno-exceptions' ],
      'xcode_settings': {
        'GCC_ENABLE_CPP_EXCEPTIONS': 'YES',
        'CLANG_CXX_LIBRARY': 'libc++',
        'MACOSX_DEPLOYMENT_TARGET': '10.7'
      },
      'msvs_settings': {
        'VCCLCompilerTool': { 'ExceptionHandling': 1 },
      }
    }
  ]
}
